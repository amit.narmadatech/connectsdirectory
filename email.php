<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 | -------------------------------------------------------------------
 | EMAIL CONFING
 | -------------------------------------------------------------------
 | Configuration of outgoing mail server.
 | */ 


$config['protocol'] = 'smtp';                         			//mail, sendmail, or smtp
//$config['mailpath'] = '/usr/sbin/sendmail';       				//None
$config['smtp_host'] = 'smtp.sendgrid.net';
$config['smtp_user'] = 'jcventures';
$config['smtp_pass'] = 'JCVentures2b$';
$config['smtp_port'] = '587';
$config['smtp_timeout'] = '180';
$config['charset'] = 'iso-8859-1';
$config['mailtype'] = 'html';
$config['wordwrap'] = TRUE;
$config['newline'] = "\r\n";
$config['smtp_crypto'] = $config['email_encryption'] = 'tls';                      //tls or ssl
/* Location: ./system/application/config/mail.php */