$("#loginfrm").validate({
    rules: {
        password: "required",
        email: {
            required: true,
            email: true
        }
    },
    submitHandler: function (form) {
        $.ajax({
            type: "POST",
            url: base_url + 'register/login',
            data: $("#loginfrm").serialize(),
            success: function (respose) {
                var obj = JSON.parse(respose);
                if (obj.status) {
                    $('#success').html(obj.msg).css('color', 'green').show().delay(10000).fadeOut()
                    window.location.href = base_url + "user";
                } else {
                    $('#notice').html(obj.msg).css('color', 'red').show().delay(10000).fadeOut()
                }
            }
        });
    },
    messages: {
        email: {
            required: "Please enter email address",
            email: "Please enter a valid email address"
        },
        password: "Please enter a password",
    }
});


/* Added By Ritesh Joshi 22-04-2017 */
$("a.forgot-password").on('click', function () {
    jQuery("form#forgotpassword").show();
    jQuery("form#loginfrm").hide();
});
$("a.loginfrm").on('click', function () {
    jQuery("form#forgotpassword").hide();
    jQuery("form#loginfrm").show();
});
$("#forgotpassword").validate({rules: {email: {required: true, email: true}}, submitHandler: function () {
    $.ajax({type: "POST", url: base_url + 'register/forgotPassword', data: $("#forgotpassword").serialize(), success: function (respose) {
            var obj = JSON.parse(respose);
            if (obj.status) {
                $('#success_forgot').html(obj.msg).css('color', 'green').show().delay(10000).fadeOut();
            } else {
                $('#notice_forgot').html(obj.msg).css('color', 'red').show().delay(10000).fadeOut()
            }
        }});
}, messages: {email: {required: "Please enter email address", email: "Please enter a valid email address"}}});


