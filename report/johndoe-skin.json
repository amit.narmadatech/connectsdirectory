{
    "your report": [["\n<p>Have you ever wondered what your true skin tone really is? And how much damage the sun will cause to your skin? The skin is the largest sensory organ in the body and the most noticed! There are numerous external factors like the environment and your skin care routine which affect your skin, but underneath that, there is a significant association with the genes that you carry. </p>\n\n<p>About 2 million years ago, early hominids shed their hair and exposed their pale skin to the sun-drenched savanna of Africa. Natural selection favored those with darker skin tone as it protected against the UV radiation that caused cancer. About 50,000 to 100,000 years ago, our ancestors who migrated to Northern Climates did not need the protection and evolved their pale skin back. In regions with less sunshine, there was a natural selection for lighter skin, as it meant that they were able to absorb more sunlight to make vitamin D. </p>\n\n<p>Science has shown that every person is unique and the care required by their skin different, which is why your friend probably tans faster than you when you are at the beach together. Analyzing your genetic data will provide practical information that can be used by your dermatologist to develop a personalized skin care routine.</p>\n\n<p>Some Interesting facts about genes and skin.</p>\n\n<ul><li>8 sites in the human genome are associated with 30% variation in skin pigmentation</li>\n \n<li>81% of acne prevalence is influenced by genes</li> \n\n<li>A first degree relative with acne increases risk by 4 times</li> \n\n<li>The age at which you notice wrinkles and their severity depend on genes</li>\n\n <li>60% of skin aging is influenced by genes</li> \n\n<li>20 to 30% of atopic dermatitis patients have an FLG gene variation</li>\n\n <li>A child with a sibling with psoriasis is 4 to 6 times more likely to get it</li> \n\n<li>The risk of developing varicose veins is 90% for children with affected parents.</li></ul>\n\n<p>Gene variations are partly responsible for skin type. Other factors include environmental triggers, stress and diet.</p>\n\n <p>In this report, we profile genes that are associated with the risk for various skin conditions and signs of skin damage.</p>\n\n <p>We hope this report helps you understand your skin better and you nourish and protect your skin based on your genes, to showcase healthier skin.</p>\n\n"]],
    "how to read": [["<h4 class=\"text-center\" id = \"slabo\"><b>Skin health is a complex interplay between genetics and the environment (lifestyle, diet, activity, stress, etc.). Your genes and the environment that you are exposed to, both play a vital role in your well-being.</b></h4>\n<p>This report is presented in a user friendly language and format. The following tips will help you get the best information value out of the report.</p>\n\n<p><b>1. The word \u201clikely\u201d is used often in the report. What does it mean?</b></p>\n<p>People generally know that high cholesterol can lead to heart conditions. However, there are individuals with high cholesterol who do not develop heart disease. Similarly, smoking can lead to lung disease, but not always. Hence, certain genetic parameters can lead to certain outcomes but other factors may modify the outcome. \u201cLikely\u201d means, it is more likely that one will see the outcome, but other factors may modify it.</p>\n\n<p><b>2. What does the term \u201cmoderate\u201d mean in the report?</b></p>\n\n<p>Moderate implies neither high nor low, rather an intermediate or an average outcome. For example, moderate likelihood of wrinkles is an intermediate level between high and low likelihood. </p>\n\n<p><b>3. How do I know which result is applicable to me?</b></p>\n<p>Only results with a check mark (\"\\u2713\") are applicable to you, the others are not applicable. All possible outcomes are provided in the table to provide a context to your outcome.</p>\n\n<p><b>4. Where did the information contained in the report come from?</b></p>\n<p>The genetic markers that are used in this report are based on scientific studies published in international journals. A list of references is available for you to read on our web blog.</p>\n\n<p><b>5. Some sentences are colored in green and others in red, why?</b></p>\n<p>Low risk for a specific skin condition are indicated in green while high risk are in red. Moderate or Neutral outcomes are indicated in black.</p>\n\n\n\n"]],
    "table1": [{
        "trait": "Glycation",
        "summary": " Genetic variations in susceptibility to skin glycation.",
        "comb": "Moderate: Moderately likely to have increased skin glycation.*lg] Low: Less likely to have increased skin glycation. _sss_*rr] High: Highly likely to have increased skin glycation."
    }, {
        "trait": "Atopic Dermatitis",
        "summary": " Genetic variations in susceptibility to atopic dermatitis.",
        "comb": "Moderate: Moderately likely to have atopic dermatitis. _sss_*lg] Low: Less likely to have atopic dermatitis.*rr] High: Highly likely to have atopic dermatitis."
    }, {
        "trait": "Psoriasis",
        "summary": " Genetic variations in susceptibility to psoriasis.",
        "comb": "Moderate: Moderately likely to have psoriasis. _sss_*lg] Low: Less likely to have psoriasis.*rr] High: Highly likely to have psoriasis."
    }, {
        "trait": "Rosacea",
        "summary": " Genetic variations in susceptibility to rosacea.",
        "comb": "Moderate: Moderately likely to have rosacea.*lg] Low: Less likely to have rosacea. _sss_*rr] High: Highly likely to have rosacea."
    }, {
        "trait": "Ephelides (Freckles)",
        "summary": " Genetic susceptibility to having freckles.",
        "comb": "Moderate: Moderately likely to have freckles. _sss_*lg] Low: Less likely to have freckles.*rr] High: Highly likely to have freckles."
    }, {
        "trait": "Lentigines (Sun Spots)",
        "summary": " Genetic susceptibility to developing sun spots.",
        "comb": "Moderate: Moderately likely to have sun spots.*lg] Low: Less likely to have sun spots. _sss_*rr] High: Highly likely to have sun spots."
    }, {
        "trait": "Tanning Response",
        "summary": " Genetic variations in tanning ability",
        "comb": "Moderate: Moderately likely to get tanned.*lg] Low: Less likely to get tanned easily. _sss_*rr] High: Highly likely to get tanned."
    }, {
        "trait": "Wrinkle And Collagen Degradation",
        "summary": " Genetic tendency for developing premature wrinkles.",
        "comb": "Moderate: Moderately likely to have premature wrinkles. _sss_*lg] Low: Less likely to have premature wrinkles.*rr] High: Highly likely to have premature wrinkles."
    }, {
        "trait": "Stretch Marks (Striae Distensae)",
        "summary": " Genetic tendency for developing stretch marks",
        "comb": "Moderate: Moderately likely to have stretch marks. _sss_*lg] Low: Less likely to have stretch marks.*rr] High: Highly likely to have stretch marks."
    }, {
        "trait": "Varicose Veins",
        "summary": " Genetic tendency for developing varicose veins.",
        "comb": "Moderate: Moderately likely to have varicose veins. _sss_*lg] Low: Less likely to have varicose veins.*rr] High: Highly likely to have varicose veins."
    }, {
        "trait": "Acne",
        "summary": " Genetic tendency for developing severe acne.",
        "comb": "Moderate: Moderately  likely to have acne. _sss_*lg] Low: Less likely to get acne.*rr] High: Highly likely to have acne."
    }, {
        "trait": "Vitamin A Needs",
        "summary": " A genetic tendency to require more Vitamin A due to inefficient metabolism of vitamin A",
        "comb": "Need more: Moderately increase vitamin A intake*lg] Normal: Maintain normal Vitamin A intake*rr] Need more: Significantly increase vitamin A intake _sss_"
    }, {
        "trait": "Vitamin B12 Needs",
        "summary": " A genetic tendency to require more Vitamin B12 due to inefficient metabolism of vitamin B12",
        "comb": "Need more: Moderately increase vitamin B12 intake*lg] Normal: Maintain normal Vitamin B12 intake _sss_*rr] Need more: Significantly increase vitamin B12 intake"
    }, {
        "trait": "Vitamin C Needs",
        "summary": " A genetic tendency to require more Vitamin C due to inefficient metabolism of vitamin C",
        "comb": "Need more: Moderately increase vitamin C intake*lg] Normal: Maintain normal Vitamin C intake*rr] Need more: Significantly increase vitamin C intake _sss_"
    }, {
        "trait": "Vitamin D Needs",
        "summary": " A genetic tendency to require more Vitamin D due to inefficient metabolism of vitamin D",
        "comb": "Need more: Moderately increase vitamin D intake _sss_*lg] Normal: Maintain normal Vitamin D intake*rr] Need more: Significantly increase vitamin D intake"
    }, {
        "trait": "Vitamin E Needs",
        "summary": " A genetic tendency to require more Vitamin E due to inefficient metabolism of vitamin E",
        "comb": "Need more: Moderately increase vitamin E intake*lg] Normal: Maintain normal Vitamin E intake*rr] Need more: Significantly increase vitamin E intake _sss_"
    }, {
        "trait": "Vitamin K Needs",
        "summary": " A genetic tendency to require more Vitamin K due to inefficient metabolism of vitamin K",
        "comb": "Need more: Moderately increase vitamin K intake _sss_*lg] Normal: Maintain normal Vitamin K intake*rr] Need more: Significantly increase vitamin K intake"
    }, {
        "trait": "Antioxidant Needs",
        "summary": " A genetic tendency to require more Antioxidants due to inefficient metabolism of Antioxidants",
        "comb": "Need more: Moderately increase antioxidants intake*lg] Normal: Maintain normal antioxidants intake _sss_*rr] Need more: Significantly increase antioxidants intake"
    }],
    "table2": [{
        "trait": "Glycation",
        "genes": "AGER/RAGE, AGER/RAGE1, GLO11, GLO1",
        "description": "Glucose is the main source of energy for our body and when it is not broken down properly, it binds to collagen in the skin leading to abnormal structural and tissue impairment. This results in the production of advanced glycation products (AGEs) through a process called glycation, which is shown to be associated with accelerated aging. Glycation can lead to hardening of the skin and an inability of the skin to regenerate, leading to wrinkles and laxity. People of certain genetic types are at an increased risk of developing AGE than others and may experience the following symptoms: <b> premature wrinkling, sagging, weak collagen and a lowered ability of the skin to rehabilitate. </b>",
        "outcome": "lg] Low: Less likely to have increased skin glycation. ",
        "recommendation": "You have a low genetic tendency to have increased skin glycation. *Though you have low genetic risk for skin glycation, it is essential to meet the daily requirements of omega-3 fatty acids to prevent skin glycation. *Consumption of Atlantic salmon, anchovies (100-150g), mackerel, walnuts, pecan nuts, hazelnuts(30g) and flax seed powder (ALA) is recommended."
    }, {
        "trait": "Atopic Dermatitis",
        "genes": "IL6R, C11orf30, ACTL9, CLEC16A, IL18RAP, IL13, OVOL1, ZBTB10, RTEL1, ZNF365",
        "description": "Eczema (atopic dermatitis) is a common skin disease that affects a large percentage of the world\u2019s population. Eczema can be caused due to a variety of factors like genetics, environmental factors and abnormalities in immune responses. People of  certain genetic types have a higher risk of being susceptible to eczema and may experience the following symptoms: <b> skin inflammation, itching,  redness and swelling. </b>",
        "outcome": "Moderate: Moderately likely to have atopic dermatitis. ",
        "recommendation": "You have a moderate genetic tendency for atopic dermatitis. *If you have any symptoms of atopic dermatitis, it is essential to follow a diet that is rich in vitamins A and D and zinc as they may reduce the symptoms.* Consume cod liver oil, baked sweet potato (1/2 cup ) with the skin, green leafy vegetables, pink salmon, mackerel, sardines, fortified oils and breakfast cereals, mushrooms, egg yolk, oysters and chickpeas *The main symptom associated with atopic dermatitis is itching and constant scratching which can lead to rashes. *Topical application of creams containing borage oil, evening primrose oil, birch tree extracts, witch hazel extracts can be used for treating the allergy. * Applying coconut oil, shea butter, oatmeal and beeswax is also recommended to reduce dryness of the skin. *Ensure that your skin is well hydrated, have a warm bath (not hot) and apply generous amount of moisturizer soon after the bath.* Harsh soaps or detergents can aggravate the condition, use mild and hypoallergenic soaps instead.* Avoid allergens like dust mites or certain types of food which could increase the intensity of the condition. *High dose UVA1 and narrow band UVB light treatment can be used to treat atopic dermatitis, under the supervision of a dermatologist."
    }, {
        "trait": "Psoriasis",
        "genes": "IL23R, ZNF365, FASLG, IL13 , IL31, FUT2, TNFAIP3, IKBKE, KLRK1, CHUK, TRIM47, UBAC2, IL12B",
        "description": "Psoriasis is a non-contagious chronic skin condition that produces plaques of thickened, scaling skin. It is one of the most baffling and persistent skin disorders. Generalized psoriasis is an inherited autoimmune disease. Men are generally more prone to the condition. Genetics play a major role in the development of psoriasis. People of certain genetic types have a higher risk of being susceptible to psoriasis and may experience the following symptoms: <b> red patches of skin with white or silvery scales, cracked and dry skin, thick nails which may be ridged or pitted, swollen and stiff joints. </b>",
        "outcome": "Moderate: Moderately likely to have psoriasis. ",
        "recommendation": "You have a moderate genetic tendency to have psoriasis. *If you have symptoms of psoriasis, you need to use specially medicated soaps and shampoos to manage the symptoms of psoriasis. Some shampoos have corticosteroids to control inflammation while others may have salicylic acid to remove the white scales.* A dermatologist may also suggest formulas that include vitamin D, tar or retinoids.*Follow a diet that is rich in Vitamins D, E and B12 and omega-3 fatty acids as they may reduce the symptoms and prevent the condition. Include sunflower oil, safflower oil (1 tbsp), almonds, hazelnuts (30g), walnuts, pink salmon, mackerel, sardines, fortified oils and breakfast cereals, mushrooms, egg yolk, hazelnuts (30g) mussels in your diet.* In case of very severe psoriasis, light therapy or prescription drugs may be effective, under the supervision of a dermatologist."
    }, {
        "trait": "Rosacea",
        "genes": "intergenic",
        "description": "Rosacea is a common skin condition which affects the face and neck. The US National Rosacea association has stated that more than 16 million Americans suffer from this condition. This is a chronic condition which cannot be cured but it can be controlled.  People with a lighter skin tone exhibit symptoms of Rosacea more distinctly though it can affect anybody. People of certain genetic types are at a higher risk of being susceptible to rosacea and may experience the following symptoms: <b> facial flushing, redness, pimples, pustules and dilated blood vessels. </b>",
        "outcome": "lg] Low: Less likely to have rosacea. ",
        "recommendation": "You have a low genetic tendency to have rosacea."
    }, {
        "trait": "Ephelides (Freckles)",
        "genes": "IRF42, MC1R1, INTERGENIC, TYR1, ASIP2, intergenic/ 6p25.3, MC1R8, IRF43, IRF44, IRF45, MC1R4, MC1R11, MC1R9, MC1R10, MC1R12, ASIP4, ASIP5",
        "description": "Freckles, also known as ephelides, are small brown spots that are commonly found in the face, neck and other sun exposed areas of the skin. Freckles are harmless and are more common among light skinned people. People of certain genetic types are at a higher risk of being susceptible to freckles and may experience the following symptoms: <b> flat , circular spots of melanin(freckles) on the face. </b>",
        "outcome": "Moderate: Moderately likely to have freckles. ",
        "recommendation": "You have a moderate genetic tendency to have freckles. *if you have freckles, protect yourself from the sun's harsh rays and avoid stepping out between 11:00 am to 4:00p.m.* Cover yourself adequately as freckles are common in sun exposed areas of the skin. *Use a broad spectrum sunscreen with at least an SPF of 30 and 7% Zinc oxide. *Applying creams and serums rich in Vitamin C and E, licorice and hydroquinone compounds may help lighten freckles and reduce further occurrence. *Natural exfoliants like lemon juice and sour cream can also be used* Follow a diet that is rich in antioxidants, vitamin E and vitamin C as they may reduce freckles. *Include mustard spinach, bell peppers, guavas, kiwifruit (100g), sunflower oil, safflower oil (1 tbsp), almonds, hazelnuts (30g), sweet potatoes, and wheat germ in the diet.* Broadband Light (BBL), intense pulse light and Q-switched laser, cryotherapy can be used to remove freckles and sun damage, under the supervision of a dermatologist."
    }, {
        "trait": "Lentigines (Sun Spots)",
        "genes": "MC1R 6, MC1R 1, MC1R, MC1R1, MC1R 3, MC1R 5",
        "description": "Sun spots (Actinic Keratoses), also known as solar lentigines, are darkened spots on the skin, caused by prolonged exposure to UV radiations. They are caused by a local growth of pigment-producing skin cells in response to ultraviolet radiation. Solar lentigines are benign, but they do indicate excessive sun exposure, a risk factor for the development of skin cancer. Solar lentigines most commonly occur in older adults, particularly those who sunburn easily and fail to tan. Women are particularly susceptible.  People of certain genetic types are at a higher risk of being susceptible to sun spots and may experience the following symptoms: <b> red,small, scaly and rough or flat spots on areas of the body exposed to the sun like face, arms, back of the head and forearms. </b>",
        "outcome": "lg] Low: Less likely to have sun spots. ",
        "recommendation": "You have a low genetic tendency to have sun spots."
    }, {
        "trait": "Tanning Response",
        "genes": "ASIP1, ASIP, TYR 1, MC1R1, SLC24A5 , HERC2 , SLC45A21",
        "description": "Tanning is a response to the sun\u2019s ultraviolet radiation resulting in an increased production of melanin. This is an adaptation to protect skin from damage as the increase in melanin darkens the skin. We test for variations in genes that are associated with the production of melanin. Those who have difficulty tanning are at higher risks of sunburn, sun spots, wrinkles, folate loss and melanoma while individuals who tan easily are at risk of vitamin D deficiency as they may derive less vitamin D from sun exposure. People of certain genetic types tan faster than others due to variability in tanning response and may experience the following symptom:  <b> skin color is darkened on sun exposed areas of the skin. </b>",
        "outcome": "lg] Low: Less likely to get tanned easily. ",
        "recommendation": "You have a low genetic tendency to get tanned easily."
    }, {
        "trait": "Wrinkle And Collagen Degradation",
        "genes": "STXBP5L",
        "description": "Wrinkles are a sign of skin aging and are caused by a variety of factors such as genetics, skin pigmentation, dehydration, UV exposure, smoking and alcohol abuse. They are accompanied by the natural aging process and occur when collagen and elastin in the skin become weak and begin to break down. This results in the damage of skin cells. People of certain genetic types have a higher risk of having wrinkles and may experience the following symptoms: <b>  deep furrows or crevices especially around the mouth, eyes and neck. </b>",
        "outcome": "Moderate: Moderately likely to have premature wrinkles. ",
        "recommendation": "You have a moderate genetic tendency to have premature wrinkles. * If you have premature wrinkles, consult a dermatologist. *It is essential that you follow a diet that is rich in antioxidants like vitamins E and C, alpha-linoleic acids, beta carotene, lycopene, omega -3 fatty acids and flavonols as they may reduce and prevent wrinkles.* Consume guavas, blackcurrants, bell peppers, sunflower and safflower oil, red beans, cranberries, kidney beans, tomatoes, water melon, Atlantic Mackerel, salmon fish (100-150gms), chia seeds, flax seeds and raw flax seed oil (1 tbsp) is highly recommended.* Use of sunscreens (with SPF 30+ and UV/IR protection), creams and serums containing Vitamin C and E, Vitamin A and alpha-lipoic acid may reduce and prevent wrinkles.*Wear sunscreen to prevent increased damage to the skin.* Avoid smoking cigarettes as it can accelerate the formation of wrinkles. *Treatments like microdermabrasion will help remove skin layer to gain smoother skin, under the supervision of a dermatologist."
    }, {
        "trait": "Stretch Marks (Striae Distensae)",
        "genes": "HMCN1, TMEM18",
        "description": "Stretch marks, also known as striae distensae, typically appear as bands of parallel lines on the skin. They are caused by a variety of factors such as pregnancy, puberty, genetic factors, rapid weight gain or loss. Being a woman increases your risk of developing stretch marks. Stretch marks are a result of skin stretching and an increase in the hormone cortisone in our system. They are not dangerous and usually disappear over time. People of certain genetic types have a higher risk of developing stretch marks and may experience the following symptoms: <b> streaks of red, pink or purple covering large parts of their body. </b>",
        "outcome": "Moderate: Moderately likely to have stretch marks. ",
        "recommendation": "You have a moderate genetic tendency to have stretch marks.* If you have stretch marks, consult a dermatologist. *Follow a diet that is rich in of Vitamins A, C and E and healthy fatty acids as they may reduce and prevent the occurrence of stretch marks.*Include sweet potato, pumpkin, carrots, mangoes, mustard spinach, bell peppers, guavas, kiwifruit (100g), soybean oil, sunflower seed kernels (1 tbsp), asparagus, wheat germ, Atlantic mackerel, salmon fish (100-150gms) and chia seeds.* Apply olive oil and creams rich in retinoic acid, Vitamin E, Vitamin A, Centella extracts and panthenol.*Percutaneous collagen induction therapy, intense pulse light, microneedling with dermaroller and laser therapy could help lower stretch mark, under the supervision of a dermatologist."
    }, {
        "trait": "Varicose Veins",
        "genes": "MTHFR",
        "description": "Varicose veins occur when your veins become enlarged, dilated, and overfilled with blood. They are dark purple to blue veins under the skin on the back of the legs that often appear twisted and bulged like cords. This condition is very common, especially among women. Some people experience pain, heaviness and itching in the legs. About 10% of the affected people develop skin changes like pigmentation or eczema. It can subsequently impair the quality of life. People of certain genetic types are at a higher risk of developing varicose veins and may experience the following symptoms: <b> misshapen veins, especially on the legs, which may also be accompanied with pain, heaviness or swelling. </b>",
        "outcome": "Moderate: Moderately likely to have varicose veins. ",
        "recommendation": "You have a moderate genetic tendency to have varicose veins. * If you have varicose veins, consult a dermatologist. Here are a few recommendations to reduce varicose veins. *Follow a diet rich in antioxidants, vitamins C and E as they may reduce and prevent varicose veins.* Include pumpkin, carrots, mangoes, mustard spinach, bell peppers, guavas, kiwifruit (100g), soybean oil, sunflower seed kernels (1 tbsp), asparagus and wheat germ in your diet.*Use of creams containing Witch Hazel extracts, grape seed extracts and flavonoids may be effective in reducing varicose veins.*Sclerotherapy, laser and RF induced therapy are the most common procedures used in the treatment of varicose veins, under the supervision of a dermatologist."
    }, {
        "trait": "Acne",
        "genes": "FST, TGFB2, OVOL1",
        "description": "Acne (acne vulgaris) occurs commonly among teenagers and young adults.Adults may also suffer from acne due to hormonal imbalances. Nearly 3 million people suffer from acne with 20% of incidences occurring in adults.This condition happens when dead skin cells and oil from skin clog hair follicles.Genetics, hormones and stress play a role in the risk of acne. People who suffer from acne may have whiteheads, blackheads, pus filled large or tender bumps. People of certain genetic types have a higher risk of developing acne and may experience the following symptoms: <b> uninflamed blackheads, pus-filled pimples or large, red, and tender bumps. </b>",
        "outcome": "Moderate: Moderately  likely to have acne. ",
        "recommendation": "You have a moderate genetic tendency to have acne.* If you have acne, consult a dermatologist. Here are a few recommendations to reduce acne. *Use warm water and mild facial cleanser to keep face clean.* Avoid scrubbing the face harshly.*Avoid eating oily or spicy foods.*Protect your skin from the sun. *Do not squeeze or pick your acne.*Skin resurfacing, laser, chemical peels and dermabrasion can be used to remove damaged layers of the skin, under the supervision of a dermatologist."
    }, {
        "trait": "Vitamin A Needs",
        "genes": "BCMO11",
        "description": "Vitamin A is required for clear vision, healthy skin and enhanced immunity. Animal sources provide Vitamin A in the form of retinol, while some plant sources provide the precursor of Vitamin A in the form of carotenes, which in turn must be converted to retinol. People of certain genetic type need more Vitamin A in their diet due to less efficient conversion of carotenoids to retinol.",
        "outcome": "rr] Need more: Significantly increase vitamin A intake",
        "recommendation": "You may have a genetic tendency to have low vitamin A levels.* Meet your daily requirements for Vitamin A.* Measure serum Vitamin A level, if below normal even after meeting RDA requirements; consult a physician.* Include carrots, sweet potato, pumpkin, green leafy vegetables, parsley, basil, coriander, milk, fish and bell peppers in daily diet."
    }, {
        "trait": "Vitamin B12 Needs",
        "genes": "FUT2, CUBN",
        "description": "Vitamin B12 is actively involved in red blood cell maturity and its deficiency can lead to pernicious anemia and general fatigue. It also helps in the removal of homocysteine from the cells. People of certain genetic type need more Vitamin B12 in their diet due to lower levels in the body.",
        "outcome": "lg] Normal: Maintain normal Vitamin B12 intake ",
        "recommendation": "You may have a genetic tendency for normal vitamin B12 levels.* Meet your daily requirements for Vitamin B12.* Measure serum Vitamin B12 level, if below normal even after meeting RDA requirements; consult a physician.* Vitamin B12 rich foods include fish and seafood.* Also seaweed, eggs, poultry, meat and dairy products provide this nutrient"
    }, {
        "trait": "Vitamin C Needs",
        "genes": "SLC23A1",
        "description": "Vitamin C is a potent antioxidant and is essential for enhanced immunity. People of certain genetic type need more Vitamin C in their diet due to lower levels in the body.",
        "outcome": "rr] Need more: Significantly increase vitamin C intake",
        "recommendation": "You may have a genetic tendency for low vitamin C levels.* Meet your daily requirements for Vitamin C.* Measure serum Vitamin C level, if below normal even after meeting RDA requirements; consult a physician.* Vitamin C rich foods include agathi, cabbage, coriander leaves, drumstick leaves, capsicum, guava, green chillies, orange and broccoli."
    }, {
        "trait": "Vitamin D Needs",
        "genes": "CYP2R1, VDR, CYP27B1, GC, GC1, GC2, NADSYN1",
        "description": "Vitamin D is essential for the absorption of calcium from the intestine and also for enhanced immunity. Our body can synthesize sufficient Vitamin D from cholesterol when the skin is exposed to adequate amounts of sunlight. People of certain genetic type need more Vitamin D in their diet due to its inefficient synthesis in our body.",
        "outcome": "Need more: Moderately increase vitamin D intake",
        "recommendation": "You may have a genetic tendency for moderately low vitamin D levels.* Meet your daily requirements for Vitamin D.* Include calcium rich foods in the diet to improve absorption of vitamin D.* Measure serum Vitamin D level, if below normal even after meeting RDA requirements; consult a physician.* Calcium rich food sources are chia seeds, beans, lentils, almonds, spinach, tofu, milk & milk products, eggs, mushrooms and finger millets."
    }, {
        "trait": "Vitamin E Needs",
        "genes": "CD362, intergenic, TTPA, CD361, CD36",
        "description": "Vitamin E is an antioxidant and it defends our body against free radical damage and protects polyunsaturated fatty acids from oxidation. People of certain genetic type need more Vitamin E in their diet due to inefficient transport and lower plasma levels of Vitamin E.",
        "outcome": "rr] Need more: Significantly increase vitamin E intake",
        "recommendation": "You may have a genetic tendency for low vitamin E levels.* Meet your daily requirements for Vitamin E.* Measure serum Vitamin E level, if below normal even after meeting RDA requirements; consult a physician.* Sunflower seeds, olive oil, wheat germ oil, spinach, avocados, almonds, broccoli and shrimps are rich in vitamin E"
    }, {
        "trait": "Vitamin K Needs",
        "genes": "GGCX, VKORC2",
        "description": "Vitamin K plays an important role in helping blood clotting process and in preventing excessive bleeding. People of certain genetic type need enhanced Vitamin K supplementation to maintain adequate levels in blood.",
        "outcome": "Need more: Moderately increase vitamin K intake ",
        "recommendation": "You may have a genetic tendency for moderately low vitamin K levels.* Meet your daily requirements for vitamin K.* Measure serum vitamin K level, if below normal even after meeting RDA requirements; consult a physician.* Vitamin K rich foods include Brussels sprouts, cabbage, prunes, spring onions and green leafy vegetables."
    }, {
        "trait": "Antioxidant Needs",
        "genes": "PON1, GPX1, SOD21, SOD2, CAT, XRCC1, PON11, NAT1",
        "description": "Antioxidants play a key role in reducing the ill effects of 'free radicals' and thereby preventing premature aging, tissue damage and the onset of chronic diseases. They are present in many vegetables, fruits, cereals, green tea, etc. People of certain genetic type have lower efficiency to defend themselves against free radical damage and hence require more antioxidants in their diet.",
        "outcome": "lg] Normal: Maintain normal antioxidants intake ",
        "recommendation": "You may have a genetic tendency to require moderate antioxidants.* Include foods rich in antioxidants.* Low antioxidant level increases the risk for cardiomyopathy.* Foods rich in antioxidants are purple, red and blue grapes, blueberries, nuts, green leafy vegetables, sweet potato, carrots, whole grains and beans"
    }],
    "table3": "",
    "table4": "",
    "table5": "",
    "disclaimer": [["<p>Xcode provides genetic assessment services for research or investigational use and Xcode\u2019s reports should be interpreted or used exclusively by professional practitioners including but not limited to certified physicians, dieti tians, nutritionists, sports therapists and others in similar profession (\u201cProfessional Practitioners\u201d). Xcode does not provide any direct medical advice to individual patients and this report is to be strictly interpreted by a qualified medical or health services professionals in order to provide relevant medical or healthcare advice, diagnosis or treatment. Genetic information must always be considered in conjunction with other information about your health such as lifestyle, family history, risk factors, biomedical data, diet, nutrition and physical activity among other factors. A single gene mutation is not the only factor that influences the health conditions or outcomes and there are several factors other than your genes such as the environment and lifestyle that may influence the health outcome. You are responsible to ascertain that your Professional Practitioner is qualified to consider the genetic information indicated in this report in conjunction with all other information made available to him/her about you including your family health history, lifestyle, bio-medical data and any other information that you may provide to the Professional Practitioner. Xcode shall not be held responsible for any misinterpretation by your Professional Practitioner of this Report or for any matter arising out of this report.</p> \n      <p>Only full genome sequences are exhaustive. All other forms of genetic tests only provide a limited subset of genetic information that has been found to be relevant to specific conditions. Since this report is not generated by conducting a whole genome sequence test, the results reported are limited to a specific set of mutations known to be associated with specific conditions. Genetic information is also subject to revision based on the latest advances in scientific research. Therefore it is possible that the interpretation of results reported herein may vary or be altered subject to ongoing research. Sometimes, the interpretations may vary from company to company based on which studies are being given a higher preference compared to others.</p>\n      <p>Xcode\u2019s role is limited to providing results of genetic test and providing a broad set of general recommendations. More detailed recommendations that may be specific to you are to be made by qualified Professional Practitioners only. General guidelines provided in our report are for information purpose only and are meant to aid your Professional Practitioner to render the relevant professional or medical advice and treatment. While assessing your genetic parameters and providing the report and recommendations, we do not consider your past or existing health conditions and or any medication taken by you (either in the past or currently), even if you may have provided us with such information. Our report and the recommendations therein are to be acted upon in consultation with a medical or other health and wellness professional practitioner.</p>\n      <p>Your reliance upon the report is solely at your own discretion. As with all health and medical related matters, you should exercise adequate care in using the information provided in this report or on our website. Xcode disclaims any responsibility for any errors and/or omissions by you or other persons either during collection of DNA samples or delivery of the DNA sample to Xcode. We make no warranties of any kind, either express or implied, including, without limitation, the implied warranties of merchantability, fitness for a particular purpose, accuracy and non- infringement. The information in this report is for Research Use Only (RUO) or Investigational Use Only (IUO), meant to assist in further clinical diagnosis or treatment by Professional Practitioners.</p>\n"]],
    "about us": [["<p><strong>About Your Genetic Test</strong>:</p> <p>Your genetic test was performed by Xcode Life Sciences, which is a global pioneer in the field of genetic services. To generate this report by analyzing your saliva sample, Xcode employs some of the best genetic testing platforms in the world such as the Illumina, Affymetrix, Pacific Biosciences platforms. </p> <p>In this test we profiled more than <strong>600,000 genetic markers</strong>. A variety of reports such as Nutrition, Fitness, Allergy, Skin Genetics, Health predisposition, Precision Medicine etc, can be generated using this one-time test. You can avail these reports anytime without having to redo the genetic test, upon request.</p> <p><strong>About the Genetic Testing Laboratory:</strong>:</p> <p>Your DNA analysis is performed in The National Accreditation Board for Testing and Calibration Laboratories (NABL) certified Laboratories. A lab certified by NABL must meet specific standards in quality, which includes rules on qualifications of individuals who perform the test, along with measures to maintain quality standards. </p> <p>Our panel of scientists follow a rigorous approach in curating the reports by analyzing thousands of published scientific studies. Our reports are updated on a regular basis to keep current with the latest in genetic research.</p> <p><strong>About Xcode Life Sciences:</strong></p> <p>Xcode was founded by Dr. Saleem Mohammed and Dr. Abdur Rub in 2011.</p> <p>Dr. Saleem Mohammed holds a PhD in Bioinformatics from University of Nebraska, USA. Previously, he worked as a Scientist in Gene Expression Technology at Monsanto. He received the MIT TR35 award in 2013</a>.</p> <p>Dr. Abdur Rub holds a PhD in Biomedical Sciences from the University of South Florida (USF), USA. Before Xcode, he has held positions as CEO of Cell Diagnostics, Singapore, Vice President of Research, Credo Biomedical, Singapore and Head of In Vitro Diagnostics, A*STAR, Singapore. </p> <p><strong>Accessing your genetic raw data</strong>: </p> <p>Your genetic test produces a raw data file which is approximately 16 MB in size. This file is available to you upon request. </p> <p><br /><br /></p>"]],
    "graphical_version": 0,
    "CFP": "",
    "ES_pie": "",
    "PFC": "",
    "EPF": "",
    "Name": "John Doe",
    "ID": "johndoegmailcom",
    "uploaded_file": "MyHeritage_raw_dna_data.csv",
    "partner": "xcode",
    "package": "Skin"
}
