<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

    
     function __construct() {
        parent:: __construct();

        if (!is_admin()) {
            redirect('Admin');
        }

        if (!check_permission()) {
            //echo "you are not authorized.";die;
            redirect('access_denied');
        }

        $this->load->library('form_validation');
        $this->load->model('admin/transaction_model', 'transaction');
    }

    public function index() {

        if ($this->input->post() && $this->input->post('submit') == 'export') {
            $this->exportReport();
        }
        if ($this->input->post()) {
            $date = $this->input->post('date_filter');
            $date = $this->security->xss_clean($date);
            if (!empty($date)) {
                $this->session->set_userdata('filter_trans_date', $date);
            } else {
                $this->session->unset_userdata('filter_trans_date');
            }
        }
        $date_fliter = false;
        if ($this->input->post('date_filter')) {
            $date_fliter = $this->input->post('date_filter');
        }

        /* ------------------ Pagination Start ------------------------- */
        $config = $this->get_pagination_config();
        $config["base_url"] = site_url("transaction/index");
        $config["total_rows"] = $this->transaction->counts($date_fliter);
        $page = ($this->uri->segment($config["uri_segment"])) ? $this->uri->segment($config["uri_segment"]) : 01;
        $this->pagination->initialize($config);
        $this->data["pagination"] = $this->pagination->create_links();

        /* ------------- Pagination End -------------------------- */
        $this->data['transaction_list'] = $blogs = $this->transaction->get_records($page, $config["per_page"], $date_fliter);
        if ($this->input->post()) {
            $json['status'] = 1;
            $json['mess'] = "success";
            $json['html'] = $this->load->view('admin/transaction/ajax_list', $this->data, true);
            echo json_encode($json);
            die;
        } else {
            $this->load->view('admin/transaction/transaction_list', $this->data);
        }
    }
    
    public function invoice($order_id = null) {
        
        if (empty($order_id)):
            redirect(base_url().'/transaction/index');
        endif;
        
        $this->db->select('orders.*');
        $this->db->from('orders');
        //$this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where(array('orders.id' => $order_id));
        $data['orders'] = $orders = $this->db->get()->row();

        $data['orderDetails'] = "";

        if (!empty($orders) && !empty($orders->id)) {

            $this->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image,tax.sgst_tax,tax.cgst_tax,tax.igst_tax,tax.state_id as pck_state_id,plan.package_id');
            $this->db->from('order_detail');
            $this->db->join('plan', 'order_detail.plan_id = plan.id');
            $this->db->join('tax', 'tax.package_id = plan.package_id');
            $this->db->where('order_detail.order_id', $order_id);
            $data['orderDetails'] = $this->db->get()->result();
        }
        $this->load->view('admin/transaction/invoice_detail', $data);
    }
    
    /* Get Report */

    public function exportReport() {
        if ($this->input->post()) {
            $post = $this->input->post();
         
            $results = $this->transaction->get_all_users_transaction();
          //  $usersData = $results['user'];
           // echo "<pre>";
           // print_r($usersData);
           // die;
            
            $filename = 'transaction_report' . date('Y-m-d H:i:s') . '.csv';
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/csv; ");
            $delimiter = ',';
            $file = fopen('php://output', 'w');
            $arrHeader = array( "name" => "Name",
                                "email" => "Email",
                                "contact_number" => "Contact No",
                                "payment_date" => "Payment date",
                                "state" => "State",
                                "district" => "District",
                                "city" => "City",
                                "zipcode" => "Pin Code",
                                "amount" => "Amount",
                                "cgst" => "CGST",
                                "sgst" => "SGST",
                                "igst" => "IGST",
                                "payment_mode" => "Payment Mode",
                                "payment_status" => "Payment Status",
                                "payment_type" => "Payment Type",
                                "transaction_id" => "Transaction Id",
                                "invoice_id" => "Invoice Id"
                            );
            // Save headings alon
            $usersData = $results['user'];
            $HeadingsArray = array();
            foreach ($arrHeader as $name => $value) {
                $HeadingsArray[] = $value;
            }
            // send the column headers
              fputcsv($file, $HeadingsArray, $delimiter);
           
            $i = 0;
            if (!empty($usersData)) {
                foreach ($usersData as $result) {
                    $valuesArray = [];
                    $valuesArray['name'] = $result['name'];
                    $valuesArray['email'] = $result['email'];
                    $valuesArray['contact_number'] = $result['mobile1'];
                    $valuesArray['payment_date'] =  date('d-m-Y h:m:s', strtotime($result['order_date']));
                    $valuesArray['state'] =   !empty(getState($result['state_id'])) ? getState($result['state_id'])->state_title : '';
                    $valuesArray['district'] = !empty(getDistrict($result['district_id'])) ? getDistrict($result['district_id'])->district_title : '';
                    $valuesArray['city'] = $result['city'];
                    $valuesArray['zipcode'] = $result['pincode'];
                    $valuesArray['amount'] = $result['total_amount'];
                    $valuesArray['cgst'] = $result['cgst'];
                    $valuesArray['sgst'] = $result['sgst'];
                    $valuesArray['igst'] = $result['igst'];
                    $valuesArray['payment_mode'] = $result['payment_mode'];
                    $valuesArray['payment_status'] = $result['payment_status'];
                    $valuesArray['payment_type'] = $result['payment_type'];
                    $valuesArray['transaction_id'] = "'".$result['txnid'];
                    $valuesArray['invoice_id'] = $result['order_number'];
                    fputcsv($file, $valuesArray, $delimiter);
                }
            }
           fclose($file);
            exit;
        }
    }
    function get_pagination_config() {

        $this->load->library('pagination');
        $config["uri_segment"] = 4;
        $config["per_page"] = 20;
        $config["num_links"] = 2;
        $config['use_page_numbers'] = true; // use page numbers, or use the current row number (limit offset)

        $config['full_tag_open'] = '<div class="row col-lg-12 text-right"> <ul class="pagination">';

        $config['full_tag_close'] = '</ul><!--pagination--></div>';

        $config['first_link'] = '&laquo; First';

        $config['first_tag_open'] = '<li class="prev page">';

        $config['first_tag_close'] = '</li>' . "\n";

        $config['last_link'] = 'Last &raquo;';

        $config['last_tag_open'] = '<li class="next page">';

        $config['last_tag_close'] = '</li>' . "\n";

        $config['next_link'] = '&rarr;';

        $config['next_tag_open'] = '<li class="next page">';

        $config['next_tag_close'] = '</li>' . "\n";

        $config['prev_link'] = '&larr;';

        $config['prev_tag_open'] = '<li class="prev page">';

        $config['prev_tag_close'] = '</li>' . "\n";

        $config['cur_tag_open'] = '<li class="active"><a href="">';

        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';

        $config['num_tag_close'] = '</li>' . "\n";

        return $config;
    }

}
