<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tickets extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!is_admin()) {
            redirect('Admin');
        }
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }

    public function index() {

        $data['title'] = 'Tickets List';
        $data['totalticket'] = $this->Comman_model->getTicketCount();
        $data['ticket'] = $this->db->select('*')->from('tbl_tickets')->order_by("tickets_id", "desc")->get()->result();
        $this->load->view('admin/tickets/tickets_list', $data);
    }

    public function addtickets($tickets_id = "") {

        $data['title'] = 'Create Tickets';
        $data['userlist'] = $this->db->select('id,CONCAT(user.firstname, ".", user.lastname) AS name')->from('user')->where(array('status' => 1, 'type' => 4))->get()->result();

        if (!empty($tickets_id)) {

            $data['tickets'] = $this->db->get_where('tbl_tickets', array('tickets_id' => $tickets_id))->row();
        } else {
            $data['tickets'] = "";
        }

        $this->form_validation->set_rules('subject', 'Ticket Subject', 'required');
        $this->form_validation->set_rules('priority', 'Priority', 'required');
        $this->form_validation->set_rules('department', 'Department', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');



        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/tickets/create_tickets', $data);
        } else {

            $created_by = $this->session->userdata('user_id');

            $userid = $this->input->post('user_id');
            $tickets_id = $this->input->post('tickets_id');
            $subject = $this->input->post('subject');
            $priority = $this->input->post('priority');
            $department = $this->input->post('department');
            $message = $this->input->post('message');

            $postdata = array('user_id' => $userid,
                'subject' => $subject,
                'priority' => $priority,
                'departments_id' => $department,
                'body' => $message,
                'created_by' => $created_by,
                'status' => 0,
            );
            $pathinfo = "uploads/tickets";
            $response = $this->Comman_model->ImagesUpload($pathinfo);

            if (empty($response['error'])) {

                if ($response['status']) {
                    $postdata['upload_file'] = $response['file_name'];
                }

                if (!empty($tickets_id)) {

                    $this->db->where('tickets_id', $tickets_id);
                    $this->db->update('tbl_tickets', $postdata);
                    $this->session->set_flashdata('alert_success', 'Tickets updated successfully.');
                } else {

                    $ticket_code = str_pad(mt_rand(0, 999999), 5, '0', STR_PAD_LEFT);

                    $postdata['ticket_code'] = $ticket_code;
                    $id = $this->Comman_model->save('tbl_tickets', $postdata);

                    if ($id) {
                        $this->session->set_flashdata('alert_success', 'Tickets created successfully.');
                    }
                }
                redirect(base_url('tickets/index'));
            } else {
                $this->session->set_flashdata('error', '<div class="alert alert-danger text-center">' . $response['error'] . '500*600</div>');
                redirect('tickets/ticketsdetails/' . $tickets_id);
            }
        }
    }

    public function ticketsdetails($tickets_id = "") {

        $data['title'] = 'View Tickets';

        $data['tickets_id'] = $tickets_id;

        // $data['ticketsconversion'] = $this->db->select('*')->from('tbl_tickets_replies')->where(array('tickets_id'=>$tickets_id))->get()->result();

        $this->db->select('tbl_tickets_replies.*,user.image,user.type,CONCAT(user.firstname, ".", user.lastname) AS name');
        $this->db->from('tbl_tickets_replies');
        $this->db->join('user', 'tbl_tickets_replies.replierid = user.id', 'left');
        $this->db->order_by('tbl_tickets_replies.tickets_replies_id', 'DESC');
        $this->db->where(array('tickets_id' => $tickets_id));
        $data['ticketsconversion'] = $this->db->get()->result();

        $data['tickets'] = $this->db->select('*')->from('tbl_tickets')->order_by("tickets_id", "desc")->get()->result();

        // $data['ticketData'] = $this->Comman_model->getRow('tbl_tickets',array('tickets_id'=>$tickets_id));

        $this->db->select('tbl_tickets.*,CONCAT(user.firstname, ".", user.lastname) AS name');
        $this->db->from('tbl_tickets');
        $this->db->join('user', 'tbl_tickets.user_id = user.id', 'left');
        $this->db->where(array('tickets_id' => $tickets_id));
        $data['ticketData'] = $this->db->get()->row();
        $this->load->view('admin/tickets/view_ticket', $data);
    }

    public function ticketcomment() {

        $userid = $this->session->userdata('user_id');
        $tickets_id = $this->input->post('tickets_id');
        $comment = $this->input->post('comment');

        $ticket = array('replierid' => $userid,
            'comment' => $comment,
            'tickets_id' => $tickets_id,);
        $pathinfo = "uploads/tickets";
        $response = $this->Comman_model->ImagesUpload($pathinfo);

        if ($response['status']) {

            $ticket['attachment'] = $response['file_name'];
        }

        $this->db->insert('tbl_tickets_replies', $ticket);
        $id = $this->db->insert_id();

        redirect('tickets/ticketsdetails/' . $tickets_id);
    }

    public function answerticket() {

        $userid = $this->session->userdata('user_id');
        $tickets_id = $this->input->post('tickets_id');
        $comment = $this->input->post('answer');
        $ticketstatus = $this->input->post('ticketstatus');

        $res = $this->Comman_model->getRow('tbl_tickets', array('tickets_id' => $tickets_id));

        if (!empty($tickets_id) && !empty($res)) {

            $this->Comman_model->update('tbl_tickets', array('tickets_id' => $tickets_id), array('status' => $ticketstatus));

            $ticket = array('replierid' => $userid,
                'comment' => $comment,
                'tickets_id' => $tickets_id,);
            $this->db->insert('tbl_tickets_replies', $ticket);
            $id = $this->db->insert_id();
        }
        redirect(base_url('tickets/index'));
    }

    public function deletetickets($tickets_id = '') {

        $res = $this->Comman_model->getRow('tbl_tickets', array('tickets_id' => $tickets_id));
        if (!empty($tickets_id) && !empty($res)) {
            $this->Comman_model->delete(array('tickets_id' => $tickets_id), 'tbl_tickets');
            $this->session->set_flashdata('success', 'Tickets deleted successfully');
        }
        redirect(base_url('tickets/index'));
    }

}
