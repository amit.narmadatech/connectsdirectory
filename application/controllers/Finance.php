<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Finance class.
 * 
 * @extends CI_Controller
 */
class Finance extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		if(!is_admin()){
            redirect('Admin'); 
        }

		if (!check_permission()) {
            redirect('access_denied');
        }
        
		$this->load->model('comman_model');
		$this->load->model('template_model');
	}

	/**
	 * render finance dashboard.
	 * 
	 * @access public
	 * @return void
	 */
	public function dashboard() {

		/* create the data object */
		$data = new stdClass();

		$this->load->view('admin/finance/finance_dashboard', $data);		
	}


	/**
	 * render request order form.
	 * 
	 * @access public
	 * @return void
	 */
	public function request_order_form($id=null) {

		/* create the data object */
		$data = new stdClass();

		/* if edit  */
		if (($id != null) && ($id > 0)) {
			$data->edit = $this->comman_model->getRow('purchase_request', ['id'=>$id]);
			$data->edit_details = $this->inventory_model->get_request_detail($id);
		}
		
		$data->warehouse_list  = $this->comman_model->getAll('warehouses', []);
		$data->supplier_list  = $this->comman_model->getAll('supplier', []);
		$data->product_list  = $this->comman_model->getAll('products', []);
		
		$purchase_request  = $this->comman_model->getBiggestId('purchase_request', []);
		$biggest_id = ($purchase_request)?$purchase_request[0]->id:0;
		$data->reference_no = "GC-GEN-PO-000".($biggest_id+1);
		//print_r($data->reference_no);die;
		$this->load->view('admin/inventory/request_order_form', $data);		
	}

	/**
	 * insert/update request order.
	 * 
	 * @access public
	 * @return void
	 */
	public function post_request_order_form() {
		
		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');

		/* set validation rules */

		$this->form_validation->set_rules('date', 'date', 'trim|required');
		$this->form_validation->set_rules('reference_no', 'reference_no', 'trim|required');
		$this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
		$this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');
		//$this->form_validation->set_rules('request_status', 'status ', 'trim|required');
		$this->form_validation->set_rules('description', 'Note ', 'trim');
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			$this->request_order_form();
			
		} else {
			
			/* set variables from the form */
			$insertData = array(
				'date' => $this->input->post('date'), 
				'reference_no' => $this->input->post('reference_no'), 
				'supplier_id' => $this->input->post('supplier_id'), 
				//'purchase_status' => $this->input->post('request_status'), 
				'warehouse_id' => $this->input->post('warehouse_id'), 
				'description' => $this->input->post('description'), 
				'created_by' => $this->session->user_id, 
			);

			if(!empty($edit_id)) {
				
				if ($this->comman_model->update('purchase_request',['id'=>$edit_id],$insertData)) {

					$products = $this->input->post('product');
					$quantity = $this->input->post('quantity');
					$rate = $this->input->post('rate');
					$amount = $this->input->post('amount');
					$i = 0;
					$this->comman_model->delete(['request_id'=>$edit_id],'purchase_request_detail');
					foreach ($products as $product) {
						$requestDetail = array(
							'request_id' => $edit_id, 
							'product_id' => $product, 
							'quantity' => $quantity[$i],
							'rate' 	   => $rate[$i], 
							'amount'   => $amount[$i], 
						);
						
						$this->comman_model->save('purchase_request_detail', $requestDetail);
					}
				
					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Request order updated successfully.');
					redirect('inventory/request_order_list');
				}else
				{
					$failed = true;
				}

			}else{
				$insert_id = $this->comman_model->save('purchase_request', $insertData);
				if ($insert_id) {
					
					$products = $this->input->post('product');
					$quantity = $this->input->post('quantity');
					$rate = $this->input->post('rate');
					$amount = $this->input->post('amount');
					$i = 0;
					foreach ($products as $product) {
						$requestDetail = array(
							'request_id' => $insert_id, 
							'product_id' => $product, 
							'quantity' => $quantity[$i],
							'rate' 	   => $rate[$i], 
							'amount'   => $amount[$i], 
							
						);

						$this->comman_model->save('purchase_request_detail', $requestDetail);
					}

					/* START - sending notification */
					$this->load->model('notification_model');
					
					$product = $this->db->from('purchase_request')
					->join('purchase_request_detail','purchase_request_detail.request_id = purchase_request.id')
					->join('products','products.id = purchase_request_detail.product_id')
					->select('purchase_request.reference_no,products.name as product_name,products.code as product_code')
					->where('purchase_request.id',$insert_id)
					->get()->row();

					/* load a template */			
					$tmpdata = array(
						'NTF_PRODUCT_NAME' => $product->product_name,
						'NTF_PRODUCT_CODE' => $product->product_code,
						'NTF_PO_NUMBER' => $product->reference_no,
					 );

					$templateData = get_notification(5,$tmpdata);

					/* sending notification to admin */
					$this->notification_model->add(array(
						'sender_id' => 0,
						'type' => 'admin_broadcast',
						'label' => 'purchase_order_placed',
						'title' => $templateData['title'],
						'body' => $templateData['body'],
						'href' => '',
						'recipient_id' => 0,
					));

					// END - sending notification

					/* creation ok */
					$this->session->set_flashdata('alert_success', 'Request order added successfully.');
					redirect('inventory/request_order_list');	
				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem in creating request order. Please try again.');
				
				redirect('inventory/add_warehouse');
				
			}
			
		}
		
	}
	

}


