<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {

    public function __construct() {

        parent::__construct();
        /* helper function to check user logged in or not */
        if (!is_logged_in()) {
            redirect('/');
        }
        $this->load->model('user_model');
        $this->load->library('dompdf_gen');
        $this->load->library('pagination');
        $this->load->helper('comman');
        $this->load->model('template_model');
        $this->load->model('comman_model');
        $this->load->library('form_validation');
    }
     
    public function index() {
         redirect('user/profile');
        if ($this->checkAuth()) {
            $data['title'] = "Dashboard";
            $this->load->view('user/dashboard', $data);
        } else {
            redirect('/');
        }
    }
    public function activatekit() {
          if ($this->checkAuth()) {
            $data['title'] = "Activate Kit";
            $this->load->view('user/activate_kit', $data);
        } else {
            redirect('/');
        }
    }
    public function kitlist() {
        if (!$this->checkAuth()) {
            redirect('/');
        }
        $data['title'] ="Kit List";
        $user_id = $this->session->user_id;
        $this->db->select('members.*,stock_out.plan_name');
        $this->db->from('members');
        $this->db->join('stock_out', 'members.barcode = stock_out.qr_code');
        $this->db->order_by('members.id', 'DESC');
        $this->db->where('members.user_id', $user_id);
        $data['kitlist'] = $this->db->get()->result();
        
         $this->load->view('user/kit_list', $data);
    }
	 public function myditeplan() {
        if (!$this->checkAuth()) {
            redirect('/');
        }
        
        $data['title'] ="Diet Plan";
        $user_id = $this->session->user_id;
        $this->db->select('diet_plan.*,plan.name as plan_name');
        $this->db->join('plan', 'plan.id = diet_plan.plan_id');
        $this->db->from('diet_plan');
        $this->db->order_by('diet_plan.id', 'DESC');
        $this->db->where(array('diet_plan.user_id'=>$user_id,'diet_plan.status'=>1));
        $data['dietplan'] = $this->db->get()->result();
        $this->load->view('user/diet_plan_list', $data);
    }
    public function profile() {
        if ($this->checkAuth()) {
            $user_id = $this->session->user_id;
            $data['user'] = $this->db->select('id,firstname,lastname,email,contact1,type,image')->from('user')->where('id', $user_id)->get()->row();
            $this->load->view('user/profile', $data);
        } else {
            redirect('/');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/');
    }

    /** check user logged in or not. */
    public function checkAuth() {

        return (isset($this->session->logged_in) && $this->session->logged_in === true) ? true : false;
    }

    public function update_profile() {
        if (!$this->checkAuth()) {
            redirect('/');
        }

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */
        $this->form_validation->set_rules('firstname', 'First name', 'trim|required|alpha_numeric|min_length[2]');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|alpha_numeric|min_length[2]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->profile();
        } else {

            if (!empty($_FILES['userimage']['name'])) {

                if ($this->image_upload()) {
                    $userimage = $this->upload_data['userimage']['file_name'];
                    $oldimgpath = FCPATH . "uploads/userimages/" . $this->input->post('oldimg');
                    if (file_exists($oldimgpath) && (!empty($this->input->post('oldimg')))) {

                        unlink($oldimgpath);

                        $this->session->set_userdata('image', $userimage);
                    }
                } else {
                    return false;
                    $userimage = $this->input->post('oldimg');
                }
            } else {

                $userimage = $this->input->post('oldimg');
            }


            /* set variables from the form */
            $postData = array('firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'contact1' => $this->input->post('mobile'),
                'image' => $userimage,
            );

            $user_id = $this->session->user_id;

            if ($this->user_model->update_user($user_id, $postData)) {

                $userData = array(
                    'user_id' => $user_id,
                    'logged_in' => true,
                    'fullname' => $postData['firstname'] . " " . $postData['lastname'],
                    'contact1' => $this->input->post('mobile'),
                    'image' => $userimage,
                    "type" => 4,
                );

                $this->session->set_userdata($userData);

                $this->session->set_flashdata('alert_success', 'Your profile has been updated successfully.');
                redirect(base_url('user/profile'));
            } else {
                $this->session->set_flashdata('alert_danger', 'There was a problem updating your account. Please try again.');
                $this->profile();
            }
        }
    }

    function image_upload() {
        $upload_dir = 'uploads/userimages/';
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir);
        }

        $config['upload_path'] = $upload_dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = 'userimage_' . substr(md5(rand()), 0, 7);
        $config['overwrite'] = false;
        $config['max_size'] = '5120';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userimage')) {

            /* image upload errors */
            $this->session->set_flashdata('error', $this->upload->display_errors());

            $this->profile();
            return false;
        } else {
            $this->upload_data['userimage'] = $this->upload->data();
            return true;
        }
    }

    public function pickup() {
        
        $data['title']="Pick up";
        $user_id = $this->session->user_id;
        $data['userDetails'] = $this->db->select('user.id,CONCAT(user.firstname," ", user.lastname) AS name,user.email,contact1,billing_address.address,billing_address.pincode')
                                ->from('user')
                                ->join('billing_address', 'user.id = billing_address.user_id')
                                ->where('user.id', $user_id)->get()->row();
        $this->load->view('user/pickup', $data);
    }

    public function post_pickup() 
    {
        $user_id = $this->session->userdata('user_id');
        //$this->form_validation->set_rules('address', 'address', 'required');
        //$this->form_validation->set_rules('contact_person', 'contact person', 'required');
        //$this->form_validation->set_rules('email', 'email', 'required|valid_email');
        //$this->form_validation->set_rules('mobile', 'mobile number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('order_number', 'order number', 'trim');
        $this->form_validation->set_rules('quantity', 'kit quantity', 'required');
        $this->form_validation->set_rules('date', 'pickup date', 'required');
        $this->form_validation->set_rules('time', 'pickup time', 'required');
        $this->form_validation->set_rules('barcode', 'barcode', 'required');
        
        if ($this->form_validation->run() == FALSE) {
           
            $this->pickup();
            
        } else {

            /* set variables from the form */
            $pickupData = array(
                //'address' => $this->input->post('address'), 
                //'contact_person' => $this->input->post('contact_person'), 
                //'email' => $this->input->post('email'), 
                //'mobile' => $this->input->post('mobile'), 
                'pickup_date' => $this->input->post('date'), 
                'pickup_time' => $this->input->post('time'), 
                'quantity' => $this->input->post('quantity'), 
                'order_number' => $this->input->post('order_number'), 
                'barcode' => $this->input->post('barcode'),
                'created_at' => date("Y-m-d H:i:s"),
                'user_id' => $user_id
            );

            if(!empty($edit_id)) {
                
                if ($this->Comman_model->update('pickup_request',['id'=>$edit_id],$pickupData)) {
                
                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Pickup request updated successfully.');
                    $this->session->keep_flashdata('alert_success');
                    
                    redirect('user/pickup');
                }else
                {
                    $failed = true;
                }

            }else{

                if ($this->Comman_model->save('pickup_request', $pickupData)) {
                
                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Pickup request placed successfully.');
                    $this->session->keep_flashdata('alert_success');

                    redirect('user/pickup');   
                }else
                {
                    $failed = true;
                }

            }
            
            if (isset($failed)) {
                
                /* creation failed, this should never happen. send error to the view */
                
                $this->session->set_flashdata('alert_danger', 'There was a problem in creating record. Please try again.');
                $this->session->keep_flashdata('alert_danger');
            
                redirect('user/pickup');
                
            }
        }
    }

    public function notification() {
        $user_id = $this->session->user_id;
        $this->load->model('notification_model');
        $data['notification_list'] = $this->notification_model->customer_notification_list($user_id);
        $this->load->view('user/notification', $data);
    }

    public function myorder() {

        //	$data = new stdClass();
        $user_id = $this->session->user_id;
        $data['title '] = 'My Order';
        $this->db->select('orders.*');
        $this->db->from('orders');
        //$this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where('orders.user_id', $user_id);
        $data['orders'] = $this->db->get()->result();
        //$data->page  = 'user/my_orders';
        //print_r($data);die;
        $this->load->view('user/my_orders', $data);
    }

    public function orderdetails($order_id = "") {
        $user_id = $this->session->user_id;
        $data['title'] = 'My Order';
        $this->db->select('orders.*');
        $this->db->from('orders');
        //$this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where(array('orders.id' => $order_id, 'orders.user_id' => $user_id));
        $data['orders'] = $orders = $this->db->get()->row();
        $data['orderDetails'] = "";
        if (!empty($orders) && !empty($orders->id)) {

            $this->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image,plan.package_id');
            $this->db->from('order_detail');
            $this->db->join('plan', 'order_detail.plan_id = plan.id');
            $this->db->where('order_detail.order_id', $order_id);
            $data['orderDetails'] = $this->db->get()->result();
        }
        $this->load->view('user/order_detail', $data);
    }

    public function downloadinvoice($order_id) {
        
         $data['title'] = "Invoice";
         $user_id = $this->session->user_id;

        $this->db->select('orders.*');
        $this->db->from('orders');
       // $this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where(array('orders.id' => $order_id, 'orders.user_id' => $user_id));
        $data['orders'] = $orders = $this->db->get()->row();

        $data['orderDetails'] = "";

        if (!empty($orders) && !empty($orders->id)) {

            $this->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image,tax.sgst_tax,tax.cgst_tax,tax.igst_tax,tax.state_id as pck_state_id');
            $this->db->from('order_detail');
            $this->db->join('plan', 'order_detail.plan_id = plan.id');
            $this->db->join('tax', 'tax.package_id = plan.package_id');
            $this->db->where('order_detail.order_id', $order_id);
            $data['orderDetails'] = $this->db->get()->result();
        }

        $html = $this->load->view('user/invoice', $data, true);

        $this->load->library('Pdf');
        $this->load->helper('download');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage('P', 'A4');
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('invoice.pdf', 'I');
        // $pdf->Output('invoice.pdf', 'D');
    }

    public function createticket() {
        if (!$this->checkAuth()) {
            redirect('/');
        }

        $this->load->view('user/support');
    }

    public function saveticket() {

        $this->form_validation->set_rules('subject', 'Ticket Subject', 'required');
        $this->form_validation->set_rules('priority', 'Priority', 'required');
        $this->form_validation->set_rules('department', 'Department', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('user/support');
        } else {

            $userid = $this->session->userdata('user_id');
            $subject = $this->input->post('subject');
            $priority = $this->input->post('priority');
            $department = $this->input->post('department');
            $message = $this->input->post('message');
            $ticket_code = str_pad(mt_rand(0, 999999), 5, '0', STR_PAD_LEFT);

            $ticket = array('user_id' => $userid,
                'ticket_code' => $ticket_code,
                'subject' => $subject,
                'priority' => $priority,
                'departments_id' => $department,
                'body' => $message,
                'status' => 0,
            );

            $pathinfo = "uploads/tickets";
            $response = $this->ImagesUpload($pathinfo);

            if ($response['status']) {

                $ticket['upload_file'] = $response['file_name'];
            }

            $this->db->insert('tbl_tickets', $ticket);
            $orderid = $this->db->insert_id();
            $this->session->set_flashdata('success', 'Ticket created successfully');
            redirect(base_url() . 'user/ticket');
        }
    }

    public function ticket() {

        if (!$this->checkAuth()) {
            redirect('/');
        }
         $userid = $this->session->userdata('user_id');
      
       $config = array();
        $config["base_url"] = base_url() . 'user/ticket';
        $total_row = $this->db->count_all("tbl_tickets");
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>' . "\n";
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>' . "\n";
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>' . "\n";
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>' . "\n";
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>' . "\n";

        $this->pagination->initialize($config);
        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 1;
        }
        $data["ticketData"] = $this->Comman_model->ticketpagination($page, $config["per_page"],$userid);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('user/ticket', $data);
    }

    public function ticketview($tickets_id = 0) {
        if (!$this->checkAuth()) {
            redirect('/');
        }

        $userid = $this->session->userdata('user_id');
        $data['userdata'] = $this->session->all_userdata();
        $data['tickets_id'] = $tickets_id;
        //$data['ticketsconversion'] = $this->db->select('*')->from('tbl_tickets_replies')->where(array('tickets_id'=>$tickets_id))->get()->result();

        $this->db->select('tbl_tickets_replies.*,user.image,user.type,CONCAT(user.firstname, ".", user.lastname) AS name');
        $this->db->from('tbl_tickets_replies');
        $this->db->join('user', 'tbl_tickets_replies.replierid = user.id', 'left');
        $this->db->order_by('tbl_tickets_replies.tickets_replies_id', 'DESC');
        $this->db->where(array('tickets_id' => $tickets_id));
        $data['ticketsconversion'] = $this->db->get()->result();


        $data['tickets'] = $this->db->select('*')->from('tbl_tickets')->where(array('user_id' => $userid))->get()->result();
       // echo $this->db->last_query();
       // die;
        $data['ticketData'] = $this->Comman_model->getRow('tbl_tickets', array('tickets_id' => $tickets_id));
        $this->load->view('user/ticketview', $data);
    }

    public function deleteticket($tickets_id = '') {

        $res = $this->Comman_model->getRow('tbl_tickets', array('tickets_id' => $tickets_id));
        if (!empty($tickets_id) && !empty($res)) {
            $this->Comman_model->delete(array('tickets_id' => $tickets_id), 'tbl_tickets');
            $this->session->set_flashdata('alert_success', 'Ticket deleted successfully');
        }
        redirect('user/ticket');
    }

    public function ticketcomment() {

        $userid = $this->session->userdata('user_id');
        $tickets_id = $this->input->post('tickets_id');
        $comment = $this->input->post('comment');

        $ticket = array('replierid' => $userid,
            'comment' => $comment,
            'tickets_id' => $tickets_id,);

        $pathinfo = "uploads/tickets";
        $response = $this->ImagesUpload($pathinfo);

        if ($response['status']) {

            $ticket['attachment'] = $response['file_name'];
        }

        $this->db->insert('tbl_tickets_replies', $ticket);
        $orderid = $this->db->insert_id();

        redirect('user/ticketview/' . $tickets_id);
    }

    public function changepassword() { /* this function for changed password */

        $data['title'] = "changepassword";
        $this->form_validation->set_rules('oldpassword', 'old password', 'required|trim|callback_oldpassword_check');
        $this->form_validation->set_rules('password', 'password', 'required|trim|min_length[6]|matches[cpassword]');
        $this->form_validation->set_rules('cpassword', 'password confirm', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('user/changepassword');
        } else {
            $user_id = $this->session->user_id;
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');

            $this->db->where('id', $user_id);
            $this->db->update('user', array('password' => md5($password)));
            $this->session->set_flashdata('success', 'Password changed successfully');
            $this->load->view('user/changepassword');
        }
    }

    public function oldpassword_check($old_password) {

        $user_id = $this->session->user_id;
        $old_password = md5($old_password);
        $checkoldpassword = $this->db->get_where('user', array('id' => $user_id, 'password' => $old_password))->row();
        if (empty($checkoldpassword)) {
            $this->form_validation->set_message(__FUNCTION__, 'Old password not match');
            return false;
        } else {
            return true;
        }
    }

    function ImagesUpload($pathinfo) {

        $config['upload_path'] = $pathinfo;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB

        if (!empty($_FILES['filename']['name'])) {

            $upload_path = FCPATH . $pathinfo;

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('filename')) {
                return array('status' => false, 'msg' => $this->upload->display_errors());
            } else {

                $upload_data = $this->upload->data();
                return array('status' => true, 'file_name' => $upload_data['file_name']);
            }
        } else {
            return false;
        }
    }
    
    public function post_activate_kit() 
    {
        
        $user_id = $this->session->userdata('user_id');
        $edit_id = $this->input->post('edit_id');
        
        $this->form_validation->set_rules('barcode', 'barcode', 'required|callback_barcode_check');
        $this->form_validation->set_rules('kituser', 'this', 'required');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('gender', 'gender', 'required');
        $this->form_validation->set_rules('dob', 'date of birth', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('mobile', 'mobile number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
       // $this->form_validation->set_rules('question', 'question', 'required');
        if ($this->form_validation->run() == FALSE) {
          
            $this->session->set_flashdata('alert_danger', 'All requires fields can not be empty.');
            $this->activatekit();
            
        }else {
          
            $barcode = $this->input->post('barcode');
            //$chk = $this->db->where('barcode',$barcode)->get('kit_upload')->num_rows();
            $chk = $this->db->where('qr_code',$barcode)->get('stock_out')->num_rows();
            
            if($chk == 0) {
                //$this->session->set_flashdata('alert_danger', 'Invalid barcode !');
                //$this->session->keep_flashdata('alert_danger');
                $this->session->set_userdata('error', 'Invalid barcode.');
                redirect('user/activatekit');
            }

            $dispatch = $this->db->where('qr_code',$barcode)->get('stock_out')->row();

            /* set variables from the form */
            $activationData = array(
                'barcode' => $this->input->post('barcode'), 
                'member_type' => $this->input->post('kituser'), 
                'name' => $this->input->post('name'), 
                'dob' => $this->input->post('dob'), 
                'email' => $this->input->post('email'), 
                'contact' => $this->input->post('mobile'), 
                'gender' => $this->input->post('gender'), 
                'weight' => $this->input->post('weight'), 
                'height' => $this->input->post('height'), 
                'consent' => 1, 
                'consent_for' => $this->input->post('consent_for'), 
                'created_at' => date("Y-m-d H:i:s"),
                'user_id' => $user_id,
                'plan_id' => $dispatch->plan_id,
                'order_id' => $dispatch->order_id,
               // 'plan_id' => 1,
               // 'order_id' => 1, 
            );

            if(!empty($edit_id)) {
                
                if ($this->Comman_model->update('members',['id'=>$edit_id],$activationData)) {
                
                    /* updation ok */
                    //$this->session->set_flashdata('alert_success', 'Record updated successfully.');
                     $this->session->set_userdata('success', 'Record updated successfully.');
                    redirect('user/activatekit');
                }else
                {
                    $failed = true;
                }

            }else{
               $member_id =  $this->Comman_model->save('members', $activationData);
             //  $member_id=1;
                if (!empty($member_id)) {
                    
                    $answerData = array('user_id'=>$user_id,'member_id'=>$member_id); 
                    $question = $this->input->post('question');
                
                    foreach($question as $key=>$questions){

                        if(is_array($questions)){
                          
                            $answer ='';
                            foreach ($questions as $key1=>$questionsArr) {
                              
                                if(is_array($questionsArr)){
                                    
                                    foreach ($questionsArr as $key2=>$value) {
                                        $answer .= $value[$key2].','; 
                                    }
                                }else{
                                      $answer .= $questionsArr.','; 
                                }
                            }
                             $answerData['question'] = $key; 
                             $answerData['answer'] = $answer;
                             $questionnaire_id =  $this->Comman_model->save('questionnaire', $answerData); 
                        }else{
                             $answerData['question'] = $key;
                             $answerData['answer'] = $questions;
                             $questionnaire_id = $this->Comman_model->save('questionnaire', $answerData);  
                        }
                    }

                    /* START - sending notification */
                    $this->load->model('notification_model');
                    
                    $barcode = trim($this->input->post('barcode'));
                    $plan_name = $this->db->where('id',$dispatch->plan_id)->get('plan')->row()->name;
                    $plan_name = empty($plan_name)?'':$plan_name;

                    $order = $this->db->from('orders')
                    ->join('user','user.id = orders.user_id')
                    ->select('CONCAT(user.firstname," ",user.lastname) as fullname,orders.order_number,user.email,user.contact1')
                    ->where('orders.id',$dispatch->order_id)->get()->row();

                    $order_number = empty($order)?'':$order->order_number;
                    $fullname = empty($order)?'':$order->fullname;

                    /* load a template */           
                    $tmpdata = array(
                        'NTF_BARCODE '=> $barcode,
                        'NTF_PLAN'=> $plan_name,
                     );
        
                    $templateData = get_notification(13,$tmpdata);

                    /* sending notification to customer */
                    $this->notification_model->add(array(
                        'sender_id' => 1,
                        'type' => 'customer_notification',
                        'label' => 'kit_activated',
                        'title' => $templateData['title'],
                        'body' => $templateData['body'],
                        'href' => '',
                        'recipient_id' => $user_id,
                    ));

                    $tmpdata1 = array(
                      'NTF_USER_NAME' => $fullname,
                      'NTF_BARCODE' => $barcode,
                      'NTF_ORDER_NUMBER' => $order_number,
                      'NTF_PLAN' => $plan_name,
                    );

                    $templateData1 = get_notification(8,$tmpdata1);

                    /* sending notification to admin */
                    $this->notification_model->add(array(
                        'sender_id' => 0,
                        'type' => 'customer_notification',
                        'label' => 'kit_activated',
                        'title' => $templateData1['title'],
                        'body' => $templateData1['body'],
                        'href' => '',
                        'recipient_id' => 0,
                    ));

                    // END - sending notification

                    // Thankyou email after order   
                     $email = $order->email;
                     $message = $this->load->view('email_template/order_dispatched', '', true);
                     $message = str_replace("{username}",$fullname,$message);
                     $message = str_replace("{planname}",$plan_name,$message);
                     $message = str_replace("{barcode}",$barcode,$message);
                     $subject="Your $plan_name DNA Testing Kit Has Been Dispatched.";
                     $this->comman_model->emailsend($email,$subject,$message);
                     
                     // Send notification mobile app
                     
                    /*$msg = array('user_id'=>$order->user_id,
                                             'notification'=>'Your kit has been dispatched',
                                             'notitify_type'=>'dispatched');
                                             
                    $registrationIds = array($order->device_id);
                    $result = send_android_notification($registrationIds, $msg); */

                    // Sending sms
                    $mobile = $order->contact1;

                    if (!empty($mobile) && (strlen($mobile) == 10)) 
                    {
                        $sms = New Sms();
                        $message = "Dear Customer, your $plan_name DNA Testing Kit has been dispatched successfully. You will receive your ordered kit within 10 business days. \n\nThanks,\nJC Genetics Team";
                        $sms->sendsms($mobile,$message);
                    }

                    /* creation ok */
                    $this->session->set_userdata('success', 'Record added successfully.');
                    //$this->session->set_flashdata('success', 'Record added successfully.');
                    //$this->session->keep_flashdata('success');
                    redirect('user/activatekit');   
                }else
                {
                    $failed = true;
                }
            }
            
            if (isset($failed)) {
                
               // $this->session->set_flashdata('error', 'There was a problem in creating record. Please try again.');
                $this->session->set_userdata('error', 'There was a problem in creating record. Please try again.');
                redirect('user/activatekit');
            }
        }
    }
    function barcode_check($barcode) {
        $this->load->helper('security'); 
        $query =  $this->Comman_model->getRow('members',array('barcode'=>$barcode));
        if (!empty($query)){
            $this->form_validation->set_message('barcode', 'This barcode already used');
            return false;
        }
        else{
            return true;
        }
    }

    function test() {

        // prep the bundle   arrived/received/activited/dispatch
        $msg = array('user_id' => '1',
            'notification' => 'your kit had been activted',
            'notitify_type' => 'arrived');
        $result = send_android_notification($msg);
        echo json_encode($result);
        die;
    }

    public function questionnaire() {

        if ($this->checkAuth()) {
            $data['title'] = 'Questionnaire';
            $this->load->view('user/questionnaire', $data);
        }
    }

    public function getUserDetail()
    {
        //$id = $this->input->post('user_id');
        $id = $this->session->userdata('user_id');

        if (!empty($id)) {
            $result = $this->db->select('email,firstname,lastname,contact1')->where('id',$id)->get('user')->row();
            
            if ($result) {
                echo json_encode(array('status' => true,'result' => $result));die;
            }else{
                
                echo json_encode(array('status' => false));die;
                
            }

        }
    }
    public function checkValidBarcode() {
        $barcode = $this->input->post('barcode');
        //$chk = $this->db->where('barcode', $barcode)->where('is_used', 1)->get('kit_upload')->num_rows();
        $chk = $this->db->where('qr_code',$barcode)->get('stock_out')->num_rows();
        if ($chk > 0) {
            $checkBarcode = $this->db->where('barcode', $barcode)->get('members')->num_rows();
            echo ($checkBarcode > 0) ? 'false' : 'true';
        } else {
            echo 'false';
        }
    }
    public function checkBarcodeAvailability() {
        $barcode = $this->input->post('barcode');
        //$chk = $this->db->where('barcode', $barcode)->where('is_used', 1)->get('kit_upload')->num_rows();
        $chk = $this->db->where('qr_code',$barcode)->get('stock_out')->num_rows();
        if ($chk > 0) {
            $checkBarcode = $this->db->where('barcode', $barcode)->get('members')->num_rows();
            echo ($checkBarcode > 0) ? 'false' : 'true';
        } else {
            echo 'false';
        }
       
    }

    public function checkPincodeAvailability()
    {
        $this->load->library('Delhivery');
        $pincode = $this->input->get('pincode');
        $dlvry = new Delhivery();

        echo $dlvry->check_pincode($pincode);
    }

}
