<?php

class Shipping extends CI_Controller {

    private $error = array();
    private $api_url = 'https://track.delhivery.com/';
    private $token = '573deec75e70c68547dfc668696d26b3bfa20066';
    private $client_name = 'A2ZSALE EXPRESS';

    public function index() {

        /* to start shipping here */
    }

    public function check_pincode() {
        $code = $this->input->get('code');
        $url = $this->api_url . "c/api/pin-codes/json/?";
        $data = array('token' => $this->token, 'filter_codes' => $code);
        //$pincode = json_decode($this->do_get_request($url . http_build_query($data)), true);
        echo $this->do_get_request($url . http_build_query($data));       
    }

    public function create_waybill($count = 1) {
        $url = $this->api_url . "waybill/api/bulk/json/?";
        //$count = isset($this->input->get('number')) ? $this->input->get('number') : 1;
        $data = array('token' => $this->token, 'cl' => $this->client_name, 'count' => $count);
        //print_r($url . http_build_query($data)); 
        $waybill_number = $this->do_get_request($url . http_build_query($data));
        // print_r($waybill_number); 
        $waybill_number = explode(",", str_replace('"', '', $waybill_number));
        $return = is_array($waybill_number) ? $waybill_number[0] : $waybill_number;
        return $return;
    }

    public function add_warehouse() {
        $url = $this->api_url . "api/backend/clientwarehouse/create/";
        $data = array("phone" => 918871374798,
            "city" => "Indore",
            "name" => "vk warehouse",
            "pin" => '452010',
            "address" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial Scheme No. 54, A.B. Road",
            "country" => "India",
            "contact_person" => "Vikas",
            "email" => "vikas.narmadatech@gmail.com",
            "registered_name" => $this->client_name
        );
        $recv = $this->do_post_request($url, $data);
        echo"<pre>";
        print_r($recv);
    }

    public function create_package() {
        
        $url = $this->api_url . "api/cmu/create.json";
        $data = array();
        $waybill = $this->create_waybill();
        //print_r($waybill);
        $send['format'] = "json";
        
        $data['dispatch_date'] = "2018-04-20T10:53:46.789382+00:00";
        $data['dispatch_id'] = 26;
        $data['pickup_location'] = array("add" => "113 Barthal, Dundahera",
            "city" => "Indore",
            "country" => "India",
            "name" => $this->client_name,
            "phone" => "8962132847",
            "pin" => "452001"
        );
        $data['shipments'][] = array("waybill" => $waybill, # unique delhivery tracking number
            "client" => $this->client_name, # client name
            "name" => "Ram Kapoor", # consignee
            "order" => "1234", # client order number
            "products_desc" => "Sony PS3 Super Slim(Black)", # product description
            "order_date" => "2018-04-20 12:00:00", # ISO Format
            "payment_mode" => "Pre­paid", #It can be, Pre­paid, COD, Pickup, REPL or Cash
            "total_amount" => 218.0, # in INR
            "cod_amount" => 0.0, # amount to be collected, required for COD
            "add" => "307 Satguru Parinay ",
            "city" => "Indore",
            "state" => "Madhya Pradesh",
            "country" => "India",
            "phone" => "8770764162",
            "pin" => "452001",
            "return_add" => "sellr address ",
            "return_city" => "indore",
            "return_country" => "india",
            "return_name" => "Sandeep naroliya",
            "return_phone" => "123456790",
            "return_pin" => "452001",
            "return_state" => "madhya predesh",
            "supplier" => "A2Z (India) Pvt Ltd",
            # seller weight and dimensions
            "extra_parameters" => array('dasd'=>1), # optional ­ Extra parameters that need to be send with packages. This should be unicoded dictionary
            "shipment_width" => "200", # optional width of shipment
            "shipment_height" => "100", # optional ­ height of shipment
            "weight" => "650.0 gm", # required
            "quantity" => 1, # quantity of goods, positive integer
            # seller keys(optional)
            "seller_inv" => "1234567890", # seller invoice no.
            "seller_inv_date" => "2018-04-18T09:30:SS+05:30", # ISO format
            "seller_name" => "Village Wine & Spirits", # name of seller
            "seller_add" => "Liquor Shop 1552 Encinitas Blvd Encinitas, CA 92024", # seller address
            "seller_cst" => "seller cst", # seller cst no
            "seller_tin" => "seller tin", # seller tin no
            # extra(optional)
            "consignee_tin" => "consignee tin", # consignee tin no
            "commodity_value" => "commodity value", # commodity value
            "tax_value" => "tax value", # tax value
            "sales_tax_form_ack_no" => "", # Sale Tax Form Acknowledge No.
            "category_of_goods" => "", # Category of goods
            # GST keys
            "seller_gst_tin" => " seller_gst_tin ", #GST TIN # of the seller
            "client_gst_tin" => "client_gst_tin", #GST TIN # of the contracting entity with Delhivery
            "consignee_gst_tin" => "consignee_gst_tin", #GST tin of the consignee in case of B2B shipments
            "hsn_code" => "hsn_code", #Share the Harmonized System of Nomenclature for each product in the package, you can pass more than one HSN here if the quantity is more than 1,using comma seperation.
            "invoice_reference" => "A2Z" #unique invoice reference number
        );

        print_r($data);die;
        
        $send['data'] = json_encode($data);
        /* Send request and get the data */
        $package = json_decode($this->do_post_request($url, http_build_query($send)), true);
        //echo "<pre>"; print_r($package);die;
        //  $this->session->data['ship_package']=$package;
        $slip = $this->packing_slip($waybill);
        echo"<prE>";
        // print_r($slip);die;
        //echo"Slip................";
        $slipdata = json_decode($slip, true);
        print_r($slipdata);die;
        if(empty($slipdata)){
            //die('slipdata not avaialable');
        }

        $this->response->setOutput($this->load->view('shipping/manifest', compact("slipdata", "package"))) ;
        
    }

    public function dtdc()
    {
        $url = "http://dotzot-test.azurewebsites.net/RestService/PushOrderDataService.svc/PushOrderData_PUDO_GST";
        
        $data = array (
          'Customer' => 
                  array (
                    'CUSTCD' => 'CC000100132',
                  ),
          'DocketList' => 
                  array (
                    0 => 
                    array (
                      'AgentID' => '',
                      'AwbNo' => '',
                      'Breath' => '1',
                      'CPD' => '06/60/2018',
                      'CollectableAmount' => '5000',
                      'Consg_Number' => '0012587',
                      'Consolidate_EW' => '123456',
                      'CustomerName' => 'Abhishek',
                      'Ewb_Number' => '876867',
                      'GST_REG_STATUS' => 'Y',
                      'HSN_code' => '02314h03',
                      'Height' => '1',
                      'Invoice_Ref' => 'AB123X001',
                      'IsPudo' => 'N',
                      'ItemName' => 'Iphone X 246 gb black',
                      'Length' => '1',
                      'Mode' => 'C',
                      'NoOfPieces' => '1',
                      'OrderConformation' => 'Y',
                      'OrderNo' => 'AB123X001',
                      'ProductCode' => '00123',
                      'PudoId' => '',
                      'REASON_TRANSPORT' => '',
                      'RateCalculation' => 'N',
                      'Seller_GSTIN' => '123223H2',
                      'ShippingAdd1' => 'Plot number 14 DTDC Express limited',
                      'ShippingAdd2' => 'GOregaon East',
                      'ShippingCity' => 'Mumba',
                      'ShippingEmailId' => 'Abhishek.pandey@dotzot.in',
                      'ShippingMobileNo' => '8433985578',
                      'ShippingState' => 'Maharashtra',
                      'ShippingTelephoneNo' => '0224009038',
                      'ShippingZip' => '400063',
                      'Shipping_GSTIN' => 'H212hf33',
                      'TotalAmount' => '2999',
                      'TransDistance' => '20',
                      'TransporterID' => 'TID0123',
                      'TransporterName' => 'DTDC Courier Cargo',
                      'TypeOfDelivery' => 'Home Delivery',
                      'TypeOfService' => 'Economy',
                      'UOM' => 'Per KG',
                      'VendorAddress1' => 'Plot number 98',
                      'VendorAddress2' => 'Pump House',
                      'VendorName' => 'ABC ltd',
                      'VendorPincode' => '400063',
                      'VendorTeleNo' => '8433985578',
                      'Weight' => '0.150',
                    ),
                  ),
                );
        
        $headers = array
        ('Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,
        'http://dotzot-test.azurewebsites.net/RestService/PushOrderDataService.svc/PushOrderData_PUDO_GST' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($data) );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }
    public function create_package1()
    {
        $url = "http://dotzot-test.azurewebsites.net/RestService/DocketTrackingService.svc/GetDocketTrackingDetails";
        $data = array('DocketNo' => 'I31000110250');
        
        $headers = array
        ('Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($data) );
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;

    }

    public function generate_package() {
        
        $order_product_id = $this->input->get('order_product');
        $order_product = $this->db->getRow(DB_PREFIX.'order_product',['order_product_id'=>$order_product_id]);
        if(empty($order_product)){
            die('invalid order item.');
        }
        $order_detail = $this->db->getRow(DB_PREFIX.'order',['order_id'=>$order_product['order_id']]);
        $query = $this->db->query("Select mss.* from ".DB_PREFIX."ms_seller_setting mss  INNER JOIN ".DB_PREFIX."ms_product msp ON msp.seller_id = mss.seller_id where msp.product_id= ".$order_product['product_id']);
        $manufacturer = array();
        //print_r($query->rows);
        //die;
        if(!empty($query->rows)){
            foreach($query->rows as $add){
                $value = $add['value'];
                if($add['name'] == 'slr_country'){
                    $row =$this->db->getRow(DB_PREFIX.'country',['country_id'=>$add['value']]);
                    //print_r($row);
                    if(isset($row['name'])){
                       $value = $row['name'];
                    }
                }
                $manufacturer[$add['name']]=$value;
               
            }
           // $data = array_column($manufacturer, 'name');
           // $manufacturer = array_combine($data, $query->rows);
        }
        
        $seller_info = $this->db->getRow(DB_PREFIX.'customer',['customer_id'=>$order_detail['customer_id']]);
        $seller_add = $this->db->getRow(DB_PREFIX.'address',['address_id'=>$seller_info['address_id']]);
         //print_r($manufacturer);
       
        $url = $this->api_url . "api/cmu/create.json";
        $data = array();
        $waybill = $this->create_waybill();
        //print_r($waybill);
        $send['format'] = "json";
        $data['dispatch_date'] = "2018-04-20T10:53:46.789382+00:00";
        $data['dispatch_id'] = 26;
//        $data['pickup_location'] = array("add" => isset($manufacturer['slr_address_line1']) ? $manufacturer['slr_address_line1'] : '',
//            "city" => isset($manufacturer['slr_city']) ? $manufacturer['slr_city'] : '',
//            "country" =>isset($manufacturer['slr_country']) ? $manufacturer['slr_country'] : '',
//            "name" => isset($manufacturer['slr_full_name']) ? $manufacturer['slr_full_name'] : '',
//            "phone" => isset($manufacturer['slr_phone']) ? $manufacturer['slr_phone'] : '',
//            "pin" => isset($manufacturer['slr_zip']) ? $manufacturer['slr_zip'] : ''
//        );
         $data['pickup_location'] = array("add" => "113 Barthal, Dundahera",
            "city" => "Indore",
            "country" => "India",
            "name" => $this->client_name,
            "phone" => "8962132847",
            "pin" => "452001"
        );
        
        $data['shipments'][] = array("waybill" => $waybill, # unique delhivery tracking number
            "client" => $this->client_name, # client name
            "name" => $order_detail['firstname'].' '.$order_detail['lastname'], # consignee
            "order" => $order_detail['order_id'], # client order number
            "products_desc" => $order_product['name'], # product description
            "order_date" => $order_detail['date_added'], # ISO Format
            "payment_mode" => "Pre­paid", #It can be, Pre­paid, COD, Pickup, REPL or Cash
            //"total_amount" => $order_product['total'], # in INR
            "total_amount" => 300, # in INR
            "cod_amount" => 0.0, # amount to be collected, required for COD
            "add" => $order_detail['shipping_address_1'],
            "city" => $order_detail['shipping_city'],
            "state" =>'',
            "country" =>$order_detail['shipping_country'],
            "phone" => $order_detail['telephone'],
            "pin" => $order_detail['shipping_postcode'],
            "return_add" => isset($manufacturer['slr_address_line1']) ? $manufacturer['slr_address_line1'] : '',
            "return_city" => isset($manufacturer['slr_city']) ? $manufacturer['slr_city'] : '',
            "return_country" =>isset($manufacturer['slr_country']) ? $manufacturer['slr_country'] : '',
            "return_name" => isset($manufacturer['slr_full_name']) ? $manufacturer['slr_full_name'] : '',
            "return_phone" =>isset($manufacturer['slr_phone']) ? $manufacturer['slr_phone'] : '',
            "return_pin" =>isset($manufacturer['slr_zip']) ? $manufacturer['slr_zip'] : '',
            "return_state" => isset($manufacturer['slr_state']) ? $manufacturer['slr_state'] : '',
            "supplier" => "A2Z (India) Pvt Ltd",
            # seller weight and dimensions
           // "extra_parameters" => array('dasd'=>1), # optional ­ Extra parameters that need to be send with packages. This should be unicoded dictionary
            "shipment_width" => "200", # optional width of shipment
            "shipment_height" => "100", # optional ­ height of shipment
            "weight" => "650.0 gm", # required
            "quantity" => $order_product['quantity'], # quantity of goods, positive integer
            # seller keys(optional)
            "seller_inv" => $order_detail['order_id'], # seller invoice no.
            "seller_inv_date" => "2018-04-18T09:30:SS+05:30", # ISO format
            "seller_name" => $seller_info['firstname'].' '.$seller_info['lastname'], # name of seller
            "seller_add" => $seller_add['address_1'], # seller address
            "seller_cst" => "seller cst", # seller cst no
            "seller_tin" => "seller tin", # seller tin no
            # extra(optional)
            "consignee_tin" => "consignee tin", # consignee tin no
            "commodity_value" => "commodity value", # commodity value
            "tax_value" => "tax value", # tax value
            "sales_tax_form_ack_no" => "", # Sale Tax Form Acknowledge No.
            "category_of_goods" => "", # Category of goods
            # GST keys
            "seller_gst_tin" => " seller_gst_tin ", #GST TIN # of the seller
            "client_gst_tin" => "client_gst_tin", #GST TIN # of the contracting entity with Delhivery
            "consignee_gst_tin" => "consignee_gst_tin", #GST tin of the consignee in case of B2B shipments
            "hsn_code" => "hsn_code", #Share the Harmonized System of Nomenclature for each product in the package, you can pass more than one HSN here if the quantity is more than 1,using comma seperation.
            "invoice_reference" => "A2Z" #unique invoice reference number
        );
        //print_r($data);
        //die;
        $send['data'] = json_encode($data);
        /* Send request and get the data */
        $package = json_decode($this->do_post_request($url, http_build_query($send)), true);
       // print_r($package);
        //  $this->session->data['ship_package']=$package;
        $slip = $this->packing_slip($waybill);
        //echo"<prE>";
       // print_r($package);
        //echo"Slip................";
        $slipdata = json_decode($slip, true);
        //print_r($slipdata);die;
        if(empty($slipdata)){
            //die('slipdata not avaialable');
        }
        $this->response->setOutput($this->load->view('shipping/manifest', compact("slipdata", "package"))) ;
        
    }

    public function create_pickup_request() {
        $url = $this->api_url . "fm/request/new/";
        $data = array('pickup_location' => $this->client_name, 'pickup_time' => "09:43:42", 'pickup_date' => "20180530", 'expected_package_count' => 10);
        $response = $this->do_post_request($url, $data);
        echo'<prE>';
        print_r($response);
    }

    public function packing_slip($waybill) {
        $url = $this->api_url . "api/p/packing_slip?wbns=" . $waybill;
        $response = $this->do_get_request($url);
        //echo"<prE>";
        //print_r($response);
        return $response;
    }

    public function invoice() {
        $data = array("md" => "E", "zn" => "A", "ss" => "DTO", "pt" => "Pre-paid", "cl" => $this->client_name, "token" => $this->token, "format" => "json");
        $url = $this->api_url . "api/kinko/v1/invoice/charges/.json?" . http_build_query($data);
        $response = json_decode($this->do_get_request($url), true);
        echo"<prE>";
        print_r($response);
    }

    public function track_order($waybill, $verbose) {
        $data = array("token" => $this->token, "waybill" => $waybill, "verbose" => $verbose);
        $url = $this->api_url . "api/packages/json/?" . http_build_query($data);
        $response = json_decode($this->do_get_request($url), true);
        echo"<prE>";
        print_r($response);
    }

    public function do_get_request($url, $method = "GET", $data = array()) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: Token " . $this->token,
                "Cache-Control: no-cache"
            )
                )
        );
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $err ? $err : $response;
    }

    public function do_post_request($url, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                //"Authorization: Token " . $this->token,
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $err ? $err : $response;
    }
    
    public function create_manifest(){
        //$waybill = $this->create_waybill();
        //print_r($waybill);
        //die;
        $waybill = $this->input->get('waybill');
      //  $order_no = $this->input->get('order_no');
        $slip = $this->packing_slip($waybill);
        
        $slipdata = json_decode($slip, true);
       /* print_r($slipdata);
        die;*/
        $this->load->view('admin/inventory/manifest', compact('slipdata'));
    }

    public function xcode($filename)
    {
        $string = file_get_contents(base_url('report/'.$filename.'.json'));
        $json_a = json_decode($string, true);
        echo "<pre>";
        //print_r($json_a);die;

        $data = array(
            'your_report' => $json_a['your report'][0][0], 
            'how_to_read' => $json_a['how to read'][0][0], 
            'table1' => $json_a['table1'], 
            'table2' => $json_a['table2'], 
            'table3' => $json_a['table3'], 
            'table4' => $json_a['table4'], 
            'table5' => $json_a['table5'], 
            'disclaimer' => $json_a['disclaimer'][0][0], 
            'graphical_version' => $json_a['graphical_version'], 
            'cfp' => $json_a['CFP'], 
            'es_pie' => $json_a['ES_pie'], 
            'pfc' => $json_a['PFC'], 
            'epf' => $json_a['EPF'], 
            'name' => $json_a['Name'], 
            'tid' => $json_a['ID'], 
            'uploaded_file' => $json_a['uploaded_file'], 
            'partner' => $json_a['partner'], 
            'package' => $json_a['package'], 
        );
echo "<pre>";
        print_r($data);die;

        print_r($json_a['your report'][0][0]);
        print_r($json_a['table1']);
        print_r($json_a['table2']);
        print_r($json_a['how to read'][0][0]);
        foreach ($json_a as $person_name => $person_a) {
            print_r($person_name);echo "<br>";
            //print_r($person_a[$person_name]);echo "<br>";
        }
    }

    public function tracking_slip() {
        //echo '<pre>';
        
        //print_r($seller_add);
       // die;

        $url = $this->api_url . "api/cmu/create.json";
        
            $waybill = $this->create_waybill();
            if (!$waybill) {
                die('waybill not generated.');
            }
                
            $send['format'] = "json";
            $data['dispatch_date'] = "2018-06-03T10:53:46.789382+00:00";
            $data['dispatch_id'] = 26;
//        $data['pickup_location'] = array("add" => isset($manufacturer['slr_address_line1']) ? $manufacturer['slr_address_line1'] : '',
//            "city" => isset($manufacturer['slr_city']) ? $manufacturer['slr_city'] : '',
//            "country" =>isset($manufacturer['slr_country']) ? $manufacturer['slr_country'] : '',
//            "name" => isset($manufacturer['slr_full_name']) ? $manufacturer['slr_full_name'] : '',
//            "phone" => isset($manufacturer['slr_phone']) ? $manufacturer['slr_phone'] : '',
//            "pin" => isset($manufacturer['slr_zip']) ? $manufacturer['slr_zip'] : ''
//        );
            $data['pickup_location'] = array("add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial Scheme No. 54, A.B. Road",
                "city" => "Indore", 
                "country" => "India",
                "name" => 'vk warehouse',
                "phone" => "918871374798",
                "pin" => "452010"
            );


            $data['shipments'][] = array("waybill" => $waybill, # unique delhivery tracking number
                "client" => $this->client_name, # client name
                "name" => 'amit meena', # consignee
                "order" => '587458745', # client order number
                "products_desc" => 'testkit', # product description
                "order_date" => '2018-04-20 12:00:00', # ISO Format
                "payment_mode" => "Pre­paid", #It can be, Pre­paid, COD, Pickup, REPL or Cash
                //"total_amount" => $order_product['total'], # in INR
                "total_amount" => 500, # in INR
                "cod_amount" => 0.0, # amount to be collected, required for COD
                "add" => "house no. 11 new sheetal nagar",
                "city" => "Indore",
                "state" => 'Madhya Pradesh',
                "country" => "India",
                "phone" => "9098794150",
                "pin" => "452010",
                "return_add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial Scheme No. 54, A.B. Road",
                "return_city" => "Indore",
                "return_country" => "India",
                "return_name" => "vikas",
                "return_phone" => "8871374798",
                "return_pin" => "452010",
                "return_state" => "Madhya Pradesh",
                "supplier" => "Jc venture test",
                # seller weight and dimensions
                // "extra_parameters" => array('dasd'=>1), # optional ­ Extra parameters that need to be send with packages. This should be unicoded dictionary
                "shipment_width" => "200", # optional width of shipment
                "shipment_height" => "100", # optional ­ height of shipment
                "weight" => "650.0 gm", # required
                "quantity" => 1, # quantity of goods, positive integer
                # seller keys(optional)
                "seller_inv" => "1234567890", # seller invoice no.
                "seller_inv_date" => "2018-04-18T09:30:SS+05:30", # ISO format
                "seller_name" => "vikas kanik", # name of seller
                "seller_add" => "ujjain", # seller address
                "seller_cst" => "seller cst", # seller cst no
                "seller_tin" => "seller tin", # seller tin no
                # extra(optional)
                "consignee_tin" => "consignee tin", # consignee tin no
                "commodity_value" => "commodity value", # commodity value
                "tax_value" => "tax value", # tax value
                "sales_tax_form_ack_no" => "", # Sale Tax Form Acknowledge No.
                "category_of_goods" => "", # Category of goods
                # GST keys
                "seller_gst_tin" => " seller_gst_tin ", #GST TIN # of the seller
                "client_gst_tin" => "client_gst_tin", #GST TIN # of the contracting entity with Delhivery
                "consignee_gst_tin" => "consignee_gst_tin", #GST tin of the consignee in case of B2B shipments
                "hsn_code" => "hsn_code", #Share the Harmonized System of Nomenclature for each product in the package, you can pass more than one HSN here if the quantity is more than 1,using comma seperation.
                "invoice_reference" => "JCGNTC" #unique invoice reference number
            );
            $send['data'] = json_encode($data);
            //print_r($data);
            //die;
            $package = json_decode($this->do_post_request($url, http_build_query($send)), true);
            /*print_r($package);
            die;*/
        
        $slip = $this->packing_slip($waybill);
        $slipdata = json_decode($slip, true);
        if (empty($slipdata)) {
            die('slipdata not avaialable');
        }
        //print_r($slipdata);
        //die;
        $this->load->view('admin/inventory/manifest', compact("slipdata", "package","order_detail"));
    }

}
