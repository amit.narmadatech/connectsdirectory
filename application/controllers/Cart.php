<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('cart');
        $this->load->library('session');
        $this->load->model('Jcventure_model', 'jcventure');
    }

    public function index() { /* function for  plan list cart */

        $userid = $this->session->userdata('user_id');

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->db->select('cart.*,plan.name,plan.price,plan.id as planid,plan.plan_image,plan.slug');
        $this->db->from('cart');
        $this->db->join('plan', 'cart.plan_id = plan.id');

        if (!empty($userid)) {
            $this->db->where('cart.user_id', $userid);
        } else {

            $session_id = $this->session->userdata('session_id');
            $this->db->where('cart.session_id', $session_id);
        }


        $data['cart'] = $this->db->get()->result();
        $this->load->view('cart', $data);
    }

    public function addtocart() { /* function for add plan in cart */

        $userid = $this->session->userdata('user_id');
        $planid = $this->input->post('planid');

        if (!$this->session->has_userdata('session_id')) {
            $uniqueId = uniqid(rand(), TRUE);
            $this->session->set_userdata("session_id", md5($uniqueId));
        }
        $session_id = $this->session->userdata('session_id');


        if (!empty($userid)) {
            $where = array('user_id' => $userid, 'plan_id' => $planid);
        } else {

            $where = array('session_id' => $session_id, 'plan_id' => $planid);
        }

        $cart = $this->db->select('*')->from('cart')->where($where)->get()->row();


        if (empty($cart)) {

            $cartdata = array('plan_id' => $planid,
                'user_id' => $userid,
                'session_id' => $session_id,
                'qty' => 1);
            $cartid = $this->db->insert('cart', $cartdata);
        } else {

            $cartdata = array('plan_id' => $planid,
                'user_id' => $userid,
                'session_id' => $session_id,
                'qty' => ($cart->qty + 1));

            $this->db->where($where);
            $this->db->update('cart', $cartdata);
        }
        redirect(site_url('/cart'));
    }

    public function removecartitem($cartid) { /* function for remove plan from cart */

        $userid = $this->session->userdata('user_id');
        $session_id = session_id();
        if (!empty($cartid) && $userid) {

            $this->db->where(array('cart_id' => $cartid, 'user_id' => $userid));
            $this->db->delete('cart');
        } else {
            $this->db->where(array('cart_id' => $cartid));
            $this->db->delete('cart');
        }
        redirect(site_url('/cart'));
    }

    public function updatecart() { /* function for update cart */

        $userid = $this->session->userdata('user_id');
        $session_id = session_id();
        $cartqty = $this->input->post('cartqty');

        if (!empty($cartqty)) {

            foreach ($cartqty as $key => $cartqtys) {

                $cartdata = array('qty' => $cartqtys);
                $this->db->where(array('cart_id' => $key));
                $this->db->update('cart', $cartdata);
            }
        }
        redirect(site_url('/cart'));
    }

    public function checkout() { /* function for cart list plan and go for payment */
        //print_r($this->jcventure->getState());die;
        //print_r($this->jcventure->getDistrict(20));die;
        if (!is_logged_in()) {
            redirect('login');
        }
        if (getTotalCount() <= 0) {
            redirect('plan');
        }

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();



        $session_id = session_id();
        $userid = $this->session->userdata('user_id');
        $cartmapping = $this->db->select('*')->from('cart')->where(array('session_id' => $session_id, 'user_id' => NULL))->get()->result();

        if (!empty($cartmapping)) {
            $cartdata = array('user_id' => $userid, 'session_id' => '');
            $this->db->where(array('session_id' => $session_id));
            $this->db->update('cart', $cartdata);
        }

        $data['state'] = $this->jcventure->getState();

        $this->db->select('cart.*,plan.name,plan.price,plan.id as planid,plan.plan_image,plan.package_id');
        $this->db->from('cart');
        $this->db->join('plan', 'cart.plan_id = plan.id');
        $this->db->where('cart.user_id', $userid);
        $data['cart'] = $this->db->get()->result();

        $this->db->select('user.id,user.firstname,user.lastname,user.email,user.contact1,billing_address.address,billing_address.mobile1,billing_address.pincode,billing_address.landmark,billing_address.state_id,billing_address.district_id,billing_address.city');
        $this->db->from('user');
        $this->db->join('billing_address', 'billing_address.user_id = user.id', 'left');
        $this->db->where('user.id', $userid);
        $userdetails = $this->db->get()->row();
        
        $data['userdetails'] = $userdetails;
        if (!empty($userdetails->state_id)) {

            $data['districts'] = $this->jcventure->getDistrict($userdetails->state_id);
        } else {
            $data['districts'] = "";
        }
        //print_r($data);die;
        $this->load->view('checkout', $data);
    }

    public function contactus() { /* function for save contactus list */
        if ($this->input->is_ajax_request()) {

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');

            $data = array('name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message);

            $insertedid = $this->Comman_model->save('contact_us', $data);

            if ($insertedid) {
                $arr = array('status' => true,
                    'id' => $insertedid,
                    'msg' => 'Thank you for contact us. we will reply as soon as possible!');
            } else {
                $arr = array('status' => false,
                    'msg' => 'Some error occurs please try agian');
            }
            echo json_encode($arr);
            die;
        }
    }

    public function getdistrict() {

        $state_id = $this->input->post('state_id');
        
        if (empty($state_id)) {
            $arr = array('status' => false, 'result' => '');
            echo json_encode($arr);
            die;
        }

        $str = '<option value="">Select District</option>';
        $districts = $this->jcventure->getDistrict($state_id);
        if (!empty($districts)) {
            foreach ($districts as $dist) {

                $str .= '<option value="' . $dist->district_id . '">' . $dist->district_title . '</option>';
            }
            $arr = array('status' => true, 'result' => $str);
        } else {
            $arr = array('status' => false, 'result' => '');
        }
        echo json_encode($arr);
        die;
    }

    public function test() {

        $insert_data = array('id' => 1,
            'name' => 'amit',
            'price' => 50,
            'qty' => 1);
        $this->cart->insert($insert_data);
    }

    public function show() {

        $cart = $this->cart->contents();
        echo $this->cart->total_items();
        //  echo  $this->cart->total();
        print_r($cart);
    }

    public function delete() {

        $this->cart->destroy();
    }

    public function getbranch() {

        $DB2 = $this->load->database('db2', TRUE);
        $lat = $this->input->post('latitude');
        $long = $this->input->post('longitude');

         $sql = "SELECT CONCAT(tblcontacts.firstname,' ',tblcontacts.lastname) as name,tbllead_services.package_id,tbllead_services.package_company_id,tblclients.is_franchise,tblclients.is_retailer,tblclients.is_franchise_retailer,tblclients.company,tblclients.address,tblclients.district,tbllead_services.status,tbllead_services.invoice_id,tblclients.userid ,tblclients.ref_code,tblclients.longitude ,tblclients.latitude ,"
                 . "(6371 * acos(cos( radians($lat ) ) * cos( radians( tblclients.latitude ) ) * cos(radians( tblclients.longitude ) - radians($long )) +
            sin(radians($lat)) * sin(radians(tblclients.latitude)) ) ) as distance,com.slug FROM tblclients 
            INNER JOIN tblcontacts  ON tblclients.userid = tblcontacts.userid      
            INNER JOIN tbllead_services  ON tblclients.userid = tbllead_services.customer_id 
            INNER JOIN jcventurescmd.companies AS com  ON com.company_id = tbllead_services.package_company_id 

            INNER JOIN jcventurescmd.packages AS pac ON pac.package_id = tbllead_services.package_id
            where (tbllead_services.invoice_id NOT IN (0) OR tbllead_services.status=1) AND (com.slug ='jcgenetics' OR pac.package_type='1')  HAVING distance <= 20 ORDER BY distance LIMIT 25";
        $result = $DB2->query($sql)->result();
        //echo $DB2->last_query();
      // die;

        $str = '<select name="branch" id="branch" class="form-control"><option value="">Select JC Care</option>';
        if (!empty($result)) {

            foreach ($result as $results) {
                if((!empty($results->is_retailer) || !empty($results->is_franchise_retailer)) && ($results->slug != "jcgenetics")){
                    continue;
                }

                $str .= '<option value="' . $results->userid . '">' . $results->name . ' ' . $results->address.'</option>';
            }
            $status = true;
        } else {

            $status = false;
            $str .= '<option value="">No JC Care available on this PIN code</option>';
        }
        $str .= '</select>';
        $arr = array('status' => $status, "result" => $str);
        echo json_encode($arr);
        die;
    }

    public function applycode() {

        $DB2 = $this->load->database('db2', TRUE);
        $code = $this->input->post('code');

        $this->form_validation->set_rules('code', 'coupan code', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $arr = "";
        if ($this->form_validation->run() == false) {

            $errors = array();
            foreach ($this->input->post() as $key => $value) {
                $errors[$key] = form_error($key);
            }
            $arr = array_filter($errors);
            $arr = array('status' => false, 'message' => 'required input fields are empty', 'response' => array($arr));
        } else {
 
            $this->db->select('tblclients.userid,tblclients.fan_parent_id,tblclients.ref_code,CONCAT(tblcontacts.firstname," ",tblcontacts.lastname) as name,tbllead_services.package_id,tbllead_services.package_company_id,tblclients.is_franchise,tblclients.is_retailer,tblclients.is_franchise_retailer,tblclients.company,tblclients.address,tblclients.district,tbllead_services.status,tbllead_services.invoice_id,tblclients.userid ,tblclients.ref_code,com.slug')
                    ->from('jccare-crm.tblclients')
                    ->join('jccare-crm.tbllead_services', 'tblclients.userid = tbllead_services.customer_id')
                    ->join('jcventurescmd.companies AS com', 'com.company_id = tbllead_services.package_company_id')
                    ->join('jccare-crm.tblcontacts', 'tblclients.userid = tblcontacts.userid')
                    ->join('jcventurescmd.packages AS pac', 'pac.package_id= tbllead_services.package_id','inner')
                    ->where(array('tblclients.ref_code' => $code))
                    ->group_start()
                    ->where('tbllead_services.status', 1)
                    ->or_where_not_in('tbllead_services.invoice_id', 0)
                    ->group_end()
                    ->group_start()
                    ->where('com.slug =','jcgenetics')
                    ->or_where('pac.package_type=','1')
                    ->group_end();
            $result = $this->db->get()->result();

            //echo $this->db->last_query();
            //print_r($result);
           //die;
            /*  $sql = "SELECT CONCAT(tblcontacts.firstname,' ',tblcontacts.lastname) as name,tbllead_services.package_id,tbllead_services.package_company_id,tblclients.is_franchise,tblclients.is_retailer,tblclients.is_franchise_retailer,tblclients.company,tblclients.address,tblclients.district,tbllead_services.status,tbllead_services.invoice_id,tblclients.userid ,tblclients.ref_code,tblclients.longitude ,tblclients.latitude ,(6371 * acos(cos( radians($lat ) ) * cos( radians( $lat ) ) * cos(radians( $long ) - radians($long )) +
              sin(radians($lat)) * sin(radians($lat)) ) ) distance,com.slug FROM tblclients
              INNER JOIN tblcontacts  ON tblclients.userid = tblcontacts.userid
              INNER JOIN tbllead_services  ON tblclients.userid = tbllead_services.customer_id
              INNER JOIN jcventurescmd.companies AS com  ON com.company_id = tbllead_services.package_company_id
              where (tbllead_services.invoice_id NOT IN (0) OR tbllead_services.status=1)  HAVING distance <= 10 ORDER BY distance LIMIT 25";
              $result = $DB2->query($sql)->result();
             */
            $checkStatus = false;
            if (!empty($result)) {

                foreach ($result as $key => $value) {

                    if (!empty($value->is_franchise)) {

                        $arr = array('status' => true, "type" => 'franchise', 'company_id' => $value->userid,'slug'=>$value->slug);
                        $checkStatus = true; 
                    } else if (!empty($value->is_retailer) && $value->slug == "jcgenetics") {

                        $arr = array('status' => true, "type" => 'retailer','company_id' => $value->userid,'slug'=>$value->slug);
                        $checkStatus = true;    
                    } else if (!empty($value->is_franchise_retailer) && !empty($value->fan_parent_id) && $value->slug == "jcgenetics") {

                       $arr = array('status' => true, "type" => 'franchise retailer', 'company_id' => $value->userid,'slug'=>$value->slug);
                       $checkStatus = true; 
                    }
                }
                if(!$checkStatus){
                    $arr = array('status' => false, "message" => 'This code is not valid');   
                }
            } else {
                $arr = array('status' => false, "message" => 'This code is not valid');
            }
        }
        
        echo json_encode($arr);
        die;
    }

    public function checkPincodeAvailability() {
        $pincode = $this->input->post('pincode');
        // Delhivery
        $this->load->library('Delhivery');
        $dlvry = new Delhivery();
        $delhivery_chk = $dlvry->check_pincode($pincode);

        if ($delhivery_chk) {
            $result = array('status' => true, 'company' => 'Delhivery', 'msg' => 'Shipping available');
            echo json_encode($result);
            die;
        }
        //default 
        $result = array('status' => false, 'company' => '', 'msg' => 'Shipping not available');
        echo json_encode($result);
        die;
    }

    public function setTempStateTax()
    {
        $state_id = $this->input->post('state_id');
        $userid = $this->session->userdata('user_id');
        
        $cartlist = $this->db->select('cart.*,plan.name,plan.price,plan.id as planid,plan.plan_image,plan.package_id')
        ->from('cart')
        ->join('plan', 'cart.plan_id = plan.id')
        ->where('cart.user_id', $userid)
        ->get()->result();
        $html = "";$gtotal=0;
        if (!empty($cartlist)) {
            $i=1;
            foreach ($cartlist as $cart) {
                $taxdata = getGstTax($cart->package_id);
                $amt = ($cart->qty * $cart->price);

                if ($state_id == $taxdata['state_id']) 
                {
                    $taxprnct =$taxdata['sgst_tax']+$taxdata['cgst_tax'];
                    $amt = ($cart->qty * $cart->price);
                    $cgst_taxamt =($amt * $taxdata['cgst_tax'])/100;
                    $sgst_taxamt =($amt * $taxdata['sgst_tax'])/100;
                    $taxText = "CGST 9 % <br>SGST 9 % ";
                    $taxAMtText = "(".$cgst_taxamt.")<br>(".$sgst_taxamt.")";
                    $taxamt = ($amt * $taxprnct)/100;
                }else{
                    $taxprnct = $taxdata['igst_tax'];
                    $taxamt = ($amt * $taxprnct)/100;
                    $taxText = 'IGST 18 % ('.$taxamt.')';
                    $taxAMtText = '('.$taxamt.')';
                }

                $netamt = $amt+$taxamt;
                $gtotal += $netamt; 
                if (file_exists("uploads/plan_img/" . $cart->plan_image) && !empty($cart->plan_image)){
                    $img = base_url() . 'uploads/plan_img/' . $cart->plan_image;
                } else {
                    $img = base_url() . 'assets/images/diet-plan.jpg';
                }
                $html .= "<tr>";
                $html .= '<th style="vertical-align: middle;">'.$i++.'</th>';
                $html .= '<td style="vertical-align: middle;"><img src="'.$img.'" class="cart-img">'.$cart->name.'</td>';
                $html .= '<td style="vertical-align: middle;">'.$cart->price.'</td>';
                $html .= '<td style="vertical-align: middle;">'.$cart->qty.'</td>';
                $html .= '<td style="vertical-align: middle;">'.$taxText.'</td>';
                $html .= '<td style="vertical-align: middle;">'.$taxAMtText.'</td>';
                $html .= '<td style="vertical-align: middle;" class="text-right"><i class="fa fa-inr"></i>'.$netamt.'</td>';
                $html .= "</tr>";
            }
        }

        if (!empty($state_id)) {
            $array = ['tempstateid'=>$state_id,'gtotal'=>$gtotal,'htmldata'=>$html];
            $this->session->set_userdata(['tempstateid'=>$state_id]);
            echo json_encode($array);die;
        }
    }

}
