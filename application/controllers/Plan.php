<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Plan extends CI_Controller {

    var $per_page = 10;

    public function __construct() {
        parent::__construct();

        if (!is_admin()) {
            redirect('Admin');
        }
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");
    }

    public function planlist() { /* this function working show plan list  */

        $data['planData'] = $this->db->get('plan')->result();
        $this->load->view('admin/plan/plan_list', $data);
    }

    public function plan($planid = "") { /* this function working add/edit  plan */

        $user_id = $this->session->userdata('user_id');
        $plan_id = $this->input->post('plan_id');
        $package_id = $this->input->post('package_id');

        $data['packages'] = $this->db->select('*')->from('packages')->where(array('status' =>1,'bh_approval' => 1,'ceo_approval' => 1,'cmd_approval' => 1))->get()->result();

        $this->form_validation->set_rules('package_id', 'package_id', 'trim|required|callback_package_exists');
        $this->form_validation->set_rules('price', 'price', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('summary', 'summary', 'required');
        $this->form_validation->set_rules('plan_template', 'plan template', 'required');
        //$this->form_validation->set_message('title', 'title', 'Please enter plan name');
        
        $this->form_validation->set_message('package_id', 'package', 'Select plan package'); 
        $this->form_validation->set_message('price', 'price', 'enter price');
        $this->form_validation->set_message('tagline', 'tagline', 'enter tag line');
        $this->form_validation->set_message('description', 'description', 'enter plan description');
        $this->form_validation->set_message('summary', 'summary', 'enter plan sort description');
        $this->form_validation->set_message('plan_template', 'plan template', 'Select plan template');

        if (empty($plan_id) && !empty($_FILES['file']['name'])) {

            $this->form_validation->set_rules('file', '', 'callback_file_check');
        }


        $directory = APPPATH . 'views/plan_template/';

        //get all php files with a .php extension.
        $arr = array();
        $filetemplatename = glob($directory . "*.php");

        foreach ($filetemplatename as $filetemplatenames) {

            $arr[] = pathinfo($filetemplatenames, PATHINFO_FILENAME);
        }

        $data['plantemplatename'] = $arr;

        if (!empty($planid)) {
            $data['plandata'] = $this->Comman_model->getRow('plan', array('id' => $planid));
        } else {
            $data['plandata'] = "";
        }

        if ($this->form_validation->run() == FALSE) {
          
            $this->load->view('admin/plan/add_plan', $data);
        } else {
            
           $packagesData =  $this->db->select('*')->from('packages')->where(array('id'=>$package_id,'status'=>1))->get()->row();
           
            
           // $title = $this->input->post('title');
          
            //$price = $this->input->post('price');
            $description = $this->input->post('description');
            $sort_summary = $this->input->post('summary');
            $plan_type = $this->input->post('plan_type');
            $tagline = $this->input->post('tagline');
            $plan_template = $this->input->post('plan_template');
           
            $title = !empty($packagesData->title)? $packagesData->title : '' ;
            $price = !empty($packagesData->base_price)? $packagesData->base_price : '';
            $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $title);
            
            $response = $this->uploadImage();

            $post_data = array('name' =>$title,
                'package_id' =>$package_id,
                'tagline' => $tagline,
                'description' => $description,
                'sort_description' => $sort_summary,
                'created_by' => $user_id,
                'plan_type' => isset($plan_type) ? $plan_type : 0,
                'price' =>$price,
                'slug' => $slug,
                'plan_template' => $plan_template,
            );
            
            if ($response['status']) {
                $post_data['plan_image'] = $response['file_name'];
            }


            $plandata = $this->Comman_model->getRow('plan', array('id' => $plan_id));

            if (!empty($plandata)) {

                $this->Comman_model->update('plan', array('id' => $plan_id), $post_data);
                $this->session->set_flashdata('alert_success', 'Plan updated successfully');
            } else {

                $this->db->insert('plan', $post_data);
                $this->session->set_flashdata('alert_success', 'Plan saved successfully');
            }
         
            redirect(site_url('plan/planlist'));
        }
    }
    
    function package_exists($package_id) {
         $this->load->helper('security'); 
         $plan_id = $this->input->post('plan_id');
         if(!empty($plan_id)){
            $where =  array('package_id'=>$package_id,'id !='=>$plan_id);
         }else{
             $where = array('package_id'=>$package_id);
         }
         
       $query =  $this->Comman_model->getRow('plan',$where);
        if (!empty($query)){
            
            $this->form_validation->set_message('package_exists', 'This plan package already exsist');
            return false;
        }
        else{
              return true;
          
        }
    }

    function uploadImage() { /* this function working upload plan images */

        $config['upload_path'] = 'uploads/plan_img/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB

        if (!empty($_FILES['file']['name'])) {

            $upload_path = FCPATH . "uploads/plan_img/";

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {

                return array('status' => false, 'msg' => $this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();
                return array('status' => true, 'file_name' => $upload_data['file_name']);
            }
        } else {
            return false;
        }
    }

    public function file_check() { /* this function check upload images type */

        $config['upload_path'] = FCPATH . "uploads/plan_img/";
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB

        $this->load->helper('security');
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {

            $error = $this->upload->display_errors();
            $this->form_validation->set_message('file', $error);
            return false;
        } else {
            return true;
        }
    }

    public function changePlanStatus($planid = '') { /* this function working change plan status active/inactive  */

        $res = $this->Comman_model->getRow('plan', array('id' => $planid));

        if (!empty($planid) && !empty($res)) {

            if ($res->status) {
                $status = 0;
            } else {
                $status = 1;
            }
            $this->Comman_model->update('plan', array('id' => $planid), array('status' => $status));
            $this->session->set_flashdata('alert_success', 'Plan status change successfully');
        }
        redirect('plan/planlist');
    }

    public function getpackage() {

        $packageid = $this->input->post('packageid');
        if (empty($packageid)) {
            $arr = array('status' => false, 'result' => '');
            echo json_encode($arr);
            die;
        }
        $package = $this->db->select('*')->from('packages')->where('id', $packageid)->get()->row();
        if (!empty($package)) {
            $arr = array('status' => true, 'base_price' => $package->base_price,'description'=>$package->description);
        } else {
            $arr = array('status' => false, 'base_price' => '');
        }
        echo json_encode($arr);
        die;
    }

}
