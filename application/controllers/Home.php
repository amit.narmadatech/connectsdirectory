<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Twilio\Rest\Client;

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->library('sms');
        $this->load->database('narmadas_crm');
        $this->load->model('Jcventure_model', 'jcventure');
           $this->load->library('email');
       // $this->load->helper('security');
    }

    public function index() { 
     
        $data['title'] = "JC Genetics | Best DNA test for health | Ancestry DNA test";
        $data['description'] = "JC Genetics is dedicated to empower health and wellness through your unique genetics information. opt for our wide variety of plans to know your future health";
        $data['keywordes'] = "best treatment for allergies,skin allergy treatment,gluten free diet, gluten free diet benefits,daily workout program, fitness strength training, dna genetic testing kit, dermatology specialists,best workout programs";
       
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $data['planData'] = $this->db->select('*')->from('plan')->where('status', 1)->order_by("id", "asc")->limit(10)->get()->result();
        $data['planlist'] = $this->db->select('*')->from('plan')->where('status', 1)->order_by("display_order", "asc")->get()->result();
        $data['blog'] = $this->db->select('*')->from('blog')->where('status', 1)->order_by("id", "desc")->limit(2)->get()->result();
        $this->load->view('home', $data);
    }

    public function plandetail($slug = "") { /* this function for show plan details */

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $where = array('slug' => $slug, 'status' => 1);
        $plandetails = $this->db->select('*')->from('plan')->where($where)->get()->row();
        
        $data['title'] = !empty($plandetails->meta_title)? $plandetails->meta_title: ''; 
        $data['description'] = !empty($plandetails->meta_description)? $plandetails->meta_description: '';
        $data['keywordes'] = !empty($plandetails->meta_keywordes)? $plandetails->meta_keywordes: '';
        
        $data['allplan'] = $this->db->select('*')->from('plan')->where(array('status' => 1, 'slug !=' => $slug))->order_by("display_order")->get()->result();
        $data['plandetails'] = $plandetails;
        if (!$plandetails) {
            return $this->error();
            // redirect(base_url().'home/error');
        }
        $this->load->view("plan_template/" . $plandetails->plan_template, $data);
        //$this->load->view('plan_detail',$data);
    }

    public function contactus() {
        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'name', ['trim', 'required', 'alpha', 'min_length[2]', 'max_length[16],','xss_clean']);
            $this->form_validation->set_rules('telephone', 'mobile number', ['trim', 'required', 'numeric', 'min_length[10]', 'max_length[13]']);
            $this->form_validation->set_rules('email', 'email', ['trim', 'required', 'valid_email']);
            //$this->form_validation->set_message('subject', ['trim', 'required','xss_clean']);
            $this->form_validation->set_message('message', ['trim', 'required','xss_clean']);
            if ($this->form_validation->run() == false) {

                $errors = array();

                foreach ($this->input->post() as $key => $value) {
                    // Add the error message for this field
                    $errors[$key] = form_error($key);
                }
                $response['errors'] = array_filter($errors); 
                $response['status'] = FALSE;
                echo json_encode($response);
                die;
                
            }else {

                $name = $this->input->post('name');
                $email = $this->input->post('email');
                //$subject = $this->input->post('subject');
                $subject = '';
                $message = $this->input->post('message');
                $mobile = $this->input->post('telephone');
                $message=  strip_tags($message);
                $message =  filter_var ( $message, FILTER_SANITIZE_STRING);

                $data = array('name' => $name,
                    'email' => $email,
                    'subject' => $subject,
                    'mobile' => $mobile,
                    'message' =>$message);
                
                $data = $this->security->xss_clean($data);
                $insertedid = $this->Comman_model->save('contact_us',$this->security->xss_clean($data));
                if ($insertedid) {
                    $arr = array('status' => true,
                        'id' => $insertedid,
                        'msg' => 'Thank you for contact us. we will reply as soon as possible!');
                } else {
                    $arr = array('status' => false,
                        'msg' => 'Some error occurs please try agian');
                }
                echo json_encode($arr);
                die;
            }
        }
    }

    /*public function getcity() {

        $state_id = $this->input->post('state_id');
        if (empty($state_id)) {
            $arr = array('status' => false, 'result' => '');
            echo json_encode($arr);
            die;
        }

        $str = '<option value="">Select city</option>';
        $city = $this->db->select('*')->from('cities')->where('state_id', $state_id)->get()->result();
        if (!empty($city)) {
            foreach ($city as $citys) {

                $str .= '<option value="' . $citys->id . '">' . $citys->name . '</option>';
            }
            $arr = array('status' => true, 'result' => $str);
        } else {
            $arr = array('status' => false, 'result' => '');
        }
        echo json_encode($arr);
        die;
    }*/

    public function getdistrict() {

        $state_id = $this->input->post('state_id');
        
        if (empty($state_id)) {
            $arr = array('status' => false, 'result' => '');
            echo json_encode($arr);
            die;
        }

        $str = '<option value="">Select District</option>';
        $districts = $this->jcventure->getDistrict($state_id);
        if (!empty($districts)) {
            foreach ($districts as $dist) {

                $str .= '<option value="' . $dist->district_id . '">' . $dist->district_title . '</option>';
            }
            $arr = array('status' => true, 'result' => $str);
        } else {
            $arr = array('status' => false, 'result' => '');
        }
        echo json_encode($arr);
        die;
    }

    public function plan() { /* this function for show product page */
        
        
        $data['title'] = "Plan | JC Genetics Plans";
        $data['description'] = "We allow you to know your future health by opting for our plans including, gene fitness, skin genetics, health predisposition, ancestry DNA and so on";
        $data['keywordes'] = "Gene fitness, Gene diet ,prescriptio medicine , skin genetics, ancestry DNA , health Horoscope,   gene allergy, BRCA1 and BRCA2, Methylation to mthfr,Complete genome Analysis";
       

        $data['title'] = "product";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $data['planList'] = $this->db->select('*')->from('plan')->where('status', 1)->order_by("display_order")->get()->result();

        $this->load->view('plan', $data);
    }

    public function genediet() { /* this function for show genediet page */
        $data['title'] = "Gene diet";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('genediet', $data);
    }

    public function ancestrydna() { /* this function for show ancestrydna page */
        $data['title'] = "Ancestry dna";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('ancestrydna', $data);
    }

    public function healthhoroscope() { /* this function for show healthhoroscope page */
        $data['title'] = "Health horoscope";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('healthhoroscope', $data);
    }

    public function precisionmedicine() { /* this function for show precisionmedicine page */
        $data['title'] = "Precision medicine";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('precisionmedicine', $data);
    }

    public function skingenetics() { /* this function for show skingenetics page */
        $data['title'] = "Skin Genetics";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('skingenetics', $data);
    }

    public function aboutus() { /* this function for show aboutus page */
        
        $data['title'] = "About us | JC Jenetics";
        $data['description'] = "We understand the importance of health and wellness in your life and thus, we test, provide personalized diet recommendations as per your genetic profile";
        $data['keywordes'] = "JC Genetics in indore , certified nutrition specialist indore, best diet foods ,good diet plan , natural hygiene diet plan ,allergy therapy";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $data['planList'] = $this->db->select('*')->from('plan')->where('status', 1)->where_in('id',['1','2','3','4','5','6','10','11','12','13'])->order_by("display_order")->get()->result();
        $this->load->view('aboutus', $data);
    }
    public function about() { /* this function for show aboutus page */
           $data['title'] = "About";

           $this->load->library('google');
           $data['GoogleloginURL'] = $this->google->loginURL();

           $this->load->library('facebook');
           $data['facebookAuthUrl'] = $this->facebook->login_url();
           $data['planList'] = $this->db->select('*')->from('plan')->where('status', 1)->where_in('id',['1','2','3','4','5','6','10','11','12','13'])->order_by("display_order")->get()->result();
           $this->load->view('about', $data);
       }
    
    public function ourteam() { /* this function for show aboutus page */ 
        
        $data['title'] = "Team | JC Jenetics";
        $data['description'] = "We understand the importance of health and wellness in your life and thus, we test, provide personalized diet recommendations as per your genetic profile";
        $data['keywordes'] = "JC Genetics in indore , certified nutrition specialist indore, best diet foods ,good diet plan , natural hygiene diet plan ,allergy therapy";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $data['planList'] = $this->db->select('*')->from('plan')->where('status', 1)->where_in('id',['1','2','3','4','5','6','10','11','12','13'])->order_by("display_order")->get()->result();
        $this->load->view('ourteam', $data);
    }

        public function faq() { 
        $data['title'] = "faq";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('faq', $data);
    }

    public function faqs() { /* this function for show faqs page */
        $data['title'] = "faqs";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('faqs', $data);
    }

    public function manual() { /* this function for show faqs page */
        $data['title'] = "manual";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('manual', $data);
    }

    public function howwecare() { /* this function for show howwecare page */
        
        $data['title'] = "How it works | Testing your DNA<";
        $data['description'] = "You can know about your nutritional requirements as per your body type by purchasing our plans. Visit the website to learn more about how the process works";
        $data['keywordes'] = "allergy treatment ,food allergy testing ,best keto diet , vegan nutrition, online dietitian,gym training program, what is the best dna test,precision medicine research ,gene therapy,organic skin care";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('howwecare', $data);
    }

    public function privacypolicy() { /* this function for show Privacy Policy page */
        $data['title'] = "Privacy Policy";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('privacypolicy', $data);
    }


      public function consentpage() { /* this function for show  disclaimer page */
        
        $data['title'] = "consent";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $this->load->view('consentpage', $data);
    }

    public function consent() { /* this function for show  disclaimer page */
        
        $data['title'] = "disclaimer";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $this->load->view('disclaimer', $data);
    }

    public function disclaimers() { /* this function for show  disclaimer for mobile page */
        $data['title'] = "disclaimers";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('disclaimers', $data);
    }

    public function partner() { /* this function for show partner page */
        $data['title'] = "Partner with us";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
    
        $data['state'] = $this->jcventure->getState();

        $this->load->view('partner', $data);
    }

    public function savepartners() { /* function for save partner list */

        if ($this->input->is_ajax_request()) {

            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');
            $mobile = $this->input->post('telephone');
            $state = $this->input->post('state');
            $district = $this->input->post('district');
            
             $message=  strip_tags($message);
             $message =  filter_var ( $message, FILTER_SANITIZE_STRING);

            $data = array('firstname' => $firstname,
                'lastname' => $lastname,
                'subject' => $subject,
                'mobile' => $mobile,
                'state' => $state,
                'district_id' => $district,
                'message' => $message);

            $insertedid = $this->Comman_model->save('partners', $this->security->xss_clean($data));

            if ($insertedid) {
                $arr = array('status' => true, 'id' => $insertedid, 'msg' => 'Thank you for contact us. we will reply as soon as possible!');
            } else {
                $arr = array('status' => false, 'msg' => 'Some error occurs please try agian');
            }
            echo json_encode($arr);
            die;
        }
    }

    public function nutrigeneticscollateral() { /* this function for Nutrigenetics collateral  page */
        $data['title'] = "Nutrigenetics collateral";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('nutrigeneticscollateral', $data);
    }

    public function nutrition() { /* this function for Nutrition   page */
        $data['title'] = "Nutrition";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('nutrition', $data);
    }

    public function genefitness() { /* this function for gene fitness  page */
        $data['title'] = "Gene Fitness";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('genefitness', $data);
    }

    public function geneallergy() { /* this function for gene Allergy  page */
        $data['title'] = "Gene Allergy";

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('geneallergy', $data);
    }

    public function blog() { /* this function for show blog page */
        $data['title'] = "blog";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $keyword = $this->input->get('keyword');
        $archives = $this->input->get('archives');

        $where = array('status' => 1);
        if (!empty($archives)) {

            $where['DATE(blog.created_date)'] = date('Y-m-d', strtotime($archives));
        }
       
        $config = array();
        $config["base_url"] = base_url() . 'home/blog';
        $total_row = $this->Comman_model->getRecordCount('blog', '1');
        $config["total_rows"] = $total_row;
        $config["per_page"] = 6;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = $total_row;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>' . "\n";
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>' . "\n";
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>' . "\n";
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>' . "\n";
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>' . "\n";

        $this->pagination->initialize($config);
        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3));
        } else {
            $page = 1;
        }
          
        $data["blog"] = $this->Comman_model->fetch_pagination($page, $config["per_page"]);
        $data["links"] = $this->pagination->create_links();

        $data['archive'] = $this->db->select('blog.created_date ,MONTHNAME(blog.created_date) as Month,Year(blog.created_date) as Year')->from('blog')->where('status', 1)->order_by("created_date", "desc")->group_by("Month")->get()->result();
        $data['recentblog'] = $this->db->select('*')->from('blog')->where('status', 1)->order_by("id", "desc")->limit('10')->get()->result();
        $this->load->view('blog', $data);
    }

    public function blogdetails($slug = "") { /* this function for show blog detail page */

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $data['title'] = "blogdetail";
        $data['recentblog'] = $this->db->select('*')->from('blog')->where('status', 1)->order_by("id", "desc")->get()->result();
        $blogs = $this->db->select('*')->from('blog')->where(array('status' => 1))->like('slug', $slug)->get()->row();
        if(empty($blogs)){
            redirect(base_url('blogs'));
        }
        $data['blogs'] = $blogs;
        $this->db->select('*');
        $this->db->from('blog_comments');
     //   $this->db->join('user', 'user.id = blog_comments.user_id OR AND blog_comments.user_id =0','left');
        $this->db->where(array('blog_comments.blog_id' => $blogs->id, 'blog_comments.status' => 1));
        $this->db->order_by('id', 'DESC');
        $data['blogcomment'] = $this->db->get()->result();
        $this->load->view('blogdetail', $data);
    }

    public function blogcomment() {

        $user_id = $this->session->userdata('user_id');

//        if (empty($user_id)) {
//            $arr = array('status' => false, 'msg' => 'User not loged in');
//            echo json_encode($arr);
//            die;
//        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', ['trim', 'required', 'min_length[2]']);
        $this->form_validation->set_rules('message', 'message', ['trim', 'required', 'min_length[2]','xss_clean']);
        $this->form_validation->set_rules('email', 'email', ['trim', 'required', 'valid_email']);

        $arr = "";
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $message = $this->input->post('message');
        $blog_id = $this->input->post('blog_id');

        if ($this->form_validation->run() == false) {

            $errors = array();

            foreach ($this->input->post() as $key => $value) {

                $errors[$key] = form_error($key); // Add the error message for this field
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            echo json_encode($response);
            die;
        } else {

             $message=  strip_tags($message);
             $message =  filter_var ( $message, FILTER_SANITIZE_STRING);
            
            $data = array('name' => $name,
                'email' => $email,
                'blog_id' => $blog_id,
                'user_id' => !empty( $user_id) ?  $user_id :0,
                'comment' => $message,);
            $insertedid = $this->Comman_model->save('blog_comments',$this->security->xss_clean($data));
            if ($insertedid) {
                $arr = array('status' => true, 'id' => $insertedid, 'msg' => 'Thank you for comment.it will be approved by admin');
            } else {

                $arr = array('status' => false, 'error' => 'Some error occurs please try agian');
            }
            echo json_encode($arr);
            die;
        }
    }

    public function term() { /* this function for show term and condation page */
        $data['title'] = "term";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('term', $data);
    }


        public function contact() { /* this function for show Carrer page */
        $data['title'] = "contact";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $this->load->view('contact', $data);
    }

    public function career() { /* this function for show Carrer page */
        $data['title'] = "career";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $this->load->view('career', $data);
    }

    public function add_browser_token() {
        if ($this->input->post('bid') != '') {
            $browser_token = $this->input->post('bid');
            $user_id = $this->session->user_id;

            if ($this->session->logged_in) {
                $data = $this->db->where('id', $user_id)->update('user', ['browser_token' => $browser_token]);
            } else {
                $this->session->set_userdata('browser_token', $browser_token);
                $data = false;
            }

            if (!empty($data)) {
                $result = array('status' => true, 'data' => $data);
            } else {
                $result = array('status' => false);
            }

            echo json_encode($result);
            die;
        }
    }

    public function test_curl() {
        $this->load->library('curl');
        echo $this->curl->simple_post(base_url('/home/testing'), array('val' => 2), array(CURLOPT_BUFFERSIZE => 10));
    }

    public function test_form() {
        echo "<form method='post' action='" . base_url("home/test_curl") . "'><input type='submit' value='ok'></form>";
    }

    public function testing() {
        $val = $this->input->post('val');
        echo $val * $val;
    }

    public function error() {

        $data['title'] = "Error";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();
        $this->load->view('error_page', $data);
    }

    public function successmessage() {

        $data['title'] = "Success";
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();
        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('message_success_page', $data);
    }

    /* 	public function testSms()
      {
      $sid = 'ACc8c33099b23767a2b142c65e53088649';
      $token = '42fb04bb5088b66c64536eba6ea9ec4b';
      $client = new Client($sid, $token);

      // Use the client to do fun stuff like send text messages!
      $client->messages->create(
      // the number you'd like to send the message to
      '+918871374798',
      array(
      // A Twilio phone number you purchased at twilio.com/console
      'from' => '+17372043205',
      // the body of the text message you'd like to send
      'body' => 'Hey Vikas! This is testing sms!',
      'statusCallback' => "http://jcgenetics.narmadasoftech.com/home/insertSms"
      )
      );
      } */

    public function getHistory() {
        $sid = 'ACc8c33099b23767a2b142c65e53088649';
        $token = '42fb04bb5088b66c64536eba6ea9ec4b';
        $client = new Client($sid, $token);

        $message = $client->messages("SMb44e9405a62741dab004059f96848678")->fetch();
        echo $message->body;
        /* foreach ($client->messages->read() as $message) {
          echo "<pre>";
          print_r($message->accountSid);
          } */
        //print_r($client->messages);
    }

    public function insertSms() {
        $data = serialize(json_decode($_POST));
        $this->db->insert('test_reports', ['note' => $data]);
    }

    public function testul() {

        // $DB2 = $this->load->database('db2', TRUE); 
        // $data  = $DB2->select('*')->from('tblcontacts')->where('active',1)->get()->result();
        // print_r($data);
        //  $DB3 = $this->load->database('db3', TRUE); 
        //  $commissions  = $DB3->select('*')->from('commissions')->where('status',1)->get()->result();
        // print_r($commissions);
        $this->db->select("tblclients.userid");
        $this->db->from('narmadas_crm.tblclients');
        $this->db->join('narmadas_crm.tblretailerservices', 'tblretailerservices.retailer_id = tblclients.userid');
        $this->db->join('narmadas_crm.tblservices', 'tblservices.serviceid = tblretailerservices.service_id', 'left');
        $this->db->join('narmadas_hrmkiosk.companies', 'companies.company_id = tblretailerservices.company_id');
        $this->db->where('tblclients.userid', 2);
        $this->db->where('companies.is_shipping', 1);
        $query = $this->db->get();
        echo $this->db->last_query();
        die;
    }

    public function mytest() {
        $data = array(
            'method' => 'sms',
            'message' => 'test sms',
            'to' => '8871374798',
            'sender' => 'TRINTY',
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://alerts.trinityservices.co.in/api/v4/?api_key=Ac5ae67bcedf8aa65b8aae3992a525e83',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $data
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $resp = json_decode($resp);
        print_r($resp);
        die;
    }
    public function testemail() {
     
       $this->load->library('email'); 
       $from_email = "rahulnagar8772@gmail.com";
       $to_email = "amit.narmadatech@gmail.com";
         $this->email->from($from_email, 'Your Name'); 
         $this->email->to($to_email);
         $this->email->subject('Email Test'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()){ 
         echo "success";
         }else{ 
            ob_start();
            $error = $this->email->print_debugger();
            print_r($error);die;
         }
    }

}
