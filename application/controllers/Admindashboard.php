<?php

class Admindashboard extends CI_Controller {

    function __construct() {
        parent:: __construct();

        if (!is_admin()) {
            redirect('Admin');
        }

        if (!check_permission()) {
            //echo "you are not authorized.";die;
            redirect('access_denied');
        }
        $this->load->library('form_validation');
    }

    public function index() {

        $data['title'] = 'Dashboard';
        $data['totalcustomer'] = $this->db->get_where('user', array('type' => 4))->num_rows();
        $data['totalorder'] = $this->db->get_where('orders', array('order_status' => 1))->num_rows();
        $totalamount = $this->db->select('SUM(total_amount) as grand_total')->get_where('orders', array('order_status' => 1))->row();
        $data['totalamount'] = ($totalamount) ? $totalamount->grand_total : 0;
        $data['totalkits'] = $this->db->where('is_used', 0)->get('kit_upload')->num_rows();

        $product = $this->db->where('id', 1)->get('products')->row();
        $alert_quantity = ($product) ? $product->alert_quantity : 10;

        $data['quantity_max'] = abs($alert_quantity * 10);
        $set_qty = $data['totalkits'] - $alert_quantity;
        $data['quantity_set'] = ($set_qty <= 0) ? 0 : $set_qty;

        $data['order_list'] = $this->db->from('orders')
                        ->join('user', 'user.id = orders.user_id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2')
                        ->order_by('orders.order_date', 'desc')->limit(5)->get()->result();

        $data['notification_list'] = $this->db
                        ->select('notification.*')
                        ->from('notification')
                        ->where('notification.sender_id', 0)
                        ->where('notification.recipient_id', 0)
                        ->order_by('notification.created_at', 'desc')
                        ->limit(5)
                        ->get()->result();

        $this->load->view('admin/dashboard', $data);
    }

    public function userlist() {

        $data = new stdClass();
        $user_id = $this->session->user_id;
        ;
        $data->user_list = $this->db->from('user')
                        ->select('user.*, designations.name as designation')
                        ->join('designations', 'designations.id = user.designation_id')
                        ->where('type', 1)->where("user.created_by", $user_id)->get()->result();
        //print_r($data->user_list);die;
        $this->load->view('admin/user_list', $data);
    }

    public function user($id = null) {
        /* create the data object */
        $data = new stdClass();

        $user_id = $this->session->user_id;
        $user = $this->db->where('id', $user_id)->get('user')->row();

        /* if edit user  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->db->where('id', $id)->get('user')->row();
        }

        $data->departments = $this->db->where('status', 1)->get('departments')->result();
        $data->designations = $this->db->where('status', 1)->get('designations')->result();

        $data->state_list = $this->db->where('country_id', 101)->get('states')->result();

        if ($user->hierarchy_id == 0) {
            $data->group_list = $this->db->where('status', 1)->get('user_group')->result();
            $data->hierarchy_list = $this->db->where('status', 1)->get('user_hierarchy')->result();
        } else {
            $data->group_list = $this->db->where('status', 1)->where("id >= $user->group_id")->get('user_group')->result();
            $data->hierarchy_list = $this->db->where('status', 1)->where("id > $user->hierarchy_id")->get('user_hierarchy')->result();
        }

        if ($user->hierarchy_id == 1) {
            $data->zone_list = $this->db->where('status', 1)->where('id', 1)->get('user_zones')->result();
        } else {
            $data->zone_list = $this->db->where('status', 1)->get('user_zones')->result();
        }

        $this->load->view('admin/add_user', $data);
    }

    public function saveuser() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('designation_id', 'designation', 'trim|required');
        $this->form_validation->set_rules('firstname', 'first name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('lastname', 'last name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('employment_id', 'employment id', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('contact1', 'mobile number', 'trim|required|regex_match[/^[0-9]{10}$/]|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('contact2', 'mobile number', 'trim|min_length[10]|max_length[10]|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        if (empty($edit_id)) {
            $this->form_validation->set_rules('email', 'email ID', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
        }

        if ($this->form_validation->run() === false) {
            /* validation not ok, send validation errors to the view */
            $this->user();
        } else {

            if (!empty($_FILES['profile_image']['name'])) {

                if ($this->image_upload()) {
                    $profile_image = $this->upload_data['profile_image']['file_name'];
                    $oldimgpath = FCPATH . "uploads/userimages/" . $this->input->post('oldimg');
                    if (file_exists($oldimgpath) && (!empty($this->input->post('oldimg')))) {
                        unlink($oldimgpath);
                        $this->session->set_userdata('image', $profile_image);
                    }
                } else {
                    return false;
                    $profile_image = $this->input->post('oldimg');
                }
            } else {

                $profile_image = $this->input->post('oldimg');
            }

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $userData = array(
                'designation_id' => $this->input->post('designation_id'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'employment_id' => $this->input->post('employment_id'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'contact1' => $this->input->post('contact1'),
                'contact2' => $this->input->post('contact2'),
                'address' => $this->input->post('address'),
                'pincode' => $this->input->post('pincode'),
                'description' => $this->input->post('description'),
                'image' => $profile_image,
                'type' => 1,
                'status' => $status,
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {
                if (empty($this->input->post('password'))) {
                    unset($userData['password']);
                }
                if ($this->db->where('id', $edit_id)->update('user', $userData)) {

                    /* user updation ok */
                    $this->session->set_flashdata('alert_success', 'User profile updated successfully.');
                    redirect('admindashboard/userlist');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->db->insert('user', $userData)) {

                    $this->load->model('notification_model');

                    $this->notification_model->add(array(
                        'sender_id' => 0,
                        'type' => 'admin_broadcast',
                        'label' => 'admin_user_created',
                        'title' => 'NEW BACKEND USER CREATED',
                        'body' => "<p>New backend user has been created. Email is " . $userData["email"] . " .</p>",
                        'href' => '',
                        'recipient_id' => 0,
                    ));

                    /* user creation ok */
                    $this->session->set_flashdata('alert_success', 'User profile created successfully.');
                    redirect('admindashboard/userlist');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* user creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating user. Please try again.');

                redirect('admindashboard/userlist');
            }
        }
    }

    /**
     * user's profile image upload function .
     * 
     * @access public
     * @return void
     */
    function image_upload() {
        $upload_dir = 'uploads/userimages/';
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir);
        }

        $config['upload_path'] = $upload_dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = 'userimage_' . substr(md5(rand()), 0, 7);
        $config['overwrite'] = false;
        $config['max_size'] = '5120';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_image')) {

            /* image upload errors */
            $this->session->set_flashdata('error', $this->upload->display_errors());

            $this->profile();
            return false;
        } else {
            $this->upload_data['profile_image'] = $this->upload->data();
            return true;
        }
    }

    public function group_list() {
        /* create the data object */
        $data = new stdClass();
        $data->group_list = $this->db
                        ->select('user_group.*')
                        ->from('user_group')
                        //->join('user_hierarchy', 'user_hierarchy.id = user_group.hierarchy_id')
                        ->where('user_group.status', 1)
                        ->get()->result();
        //print_r($data->group_list);die;
        $this->load->view('admin/user_group_list', $data);
    }

    public function group($id = null) {
        /* create the data object */
        $data = new stdClass();

        /* if group edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->db->where('id', $id)->get('user_group')->row();
        }

        $data->url_list = $this->db->where('status', 1)->get('page_url')->result();
        $data->hierarchy_list = $this->db->where('status', 1)->get('user_hierarchy')->result();
        $data->zone_list = $this->db->where('status', 1)->get('user_zones')->result();
        $data->department_list = $this->db->where('status', 1)->get('user_designation')->result();
        //print_r($data->group);die;
        $this->load->view('admin/add_user_group', $data);
    }

    public function create_group() {

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */

        /* $this->form_validation->set_rules('hierarchy_id', 'User Hierarchy', 'trim|required');
          $this->form_validation->set_rules('zone_id', 'User Zone', 'trim|required');
          $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
         */
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('redirect_url', 'Redirect to', 'trim|required');
        $this->form_validation->set_rules('permission[]', 'Permission', 'required');


        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->group();
        } else {

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $groupData = array(
                /* 'hierarchy_id'    => $this->input->post('hierarchy_id'),
                  'zone_id'    => $this->input->post('zone_id'),
                  'designation' => $this->input->post('designation'), */
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'redirect_url' => $this->input->post('redirect_url'),
                'permission' => serialize($this->input->post('permission')),
                'status' => $status,
            );

            if (!empty($this->input->post('edit_id'))) {

                if ($this->db->where('id', $this->input->post('edit_id'))->update('user_group', $groupData)) {

                    /* user group creation ok */
                    $this->session->set_flashdata('alert_success', 'User group updated successfully.');
                    redirect('admindashboard/group_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->db->insert('user_group', $groupData)) {

                    /* user group creation ok */
                    $this->session->set_flashdata('alert_success', 'User group created successfully.');
                    redirect('admindashboard/group_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* user group creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating user group. Please try again.');

                redirect('admindashboard/create_group');
            }
        }
    }

    public function department_list() {
        $data = new stdClass();

        $data->departments = $this->db->get('departments')->result();
        $data->designations = $this->db->get('designations')->result();
        //print_r($data->designations);die;

        $this->load->view('admin/department_list', $data);
    }

    public function department($id = null) {

        $data = new stdClass();

        if (($id != null) && ($id > 0)) {
            $data->edit = $this->db->where('id', $id)->get('designations')->row();
            $data->edit->dashboard_id = $this->db->where('link', $data->edit->redirect_url)->get('menus')->row()->id;
        }

        $data->departments = $this->db->where('status', 1)->get('departments')->result();
        $data->designations = $this->db->where('status', 1)->get('designations')->result();
        $data->menus = $this->db->where('status', 1)->get('menus')->result();
        $data->dashboards = $this->db->where('status', 1)->where('is_dashboard', 1)->get('menus')->result();

        $this->load->view('admin/add_department', $data);
    }

    public function post_department() {

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */

        $this->form_validation->set_rules('departments', 'department', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
        $this->form_validation->set_rules('dashboard', 'dashboard', 'trim|required');
        $this->form_validation->set_rules('permission[]', 'Permission', 'required');


        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->department();
        } else {

            if (!empty($this->input->post('department_name'))) {
                $this->db->insert('departments', ['name' => $this->input->post('department_name')]);
                $insert_id = $this->db->insert_id();
            }

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            $redirect_url = $this->db->where('id', $this->input->post('dashboard'))->get('menus')->row()->link;
            /* set variables from the form */
            $groupData = array(
                'department_id' => (!empty($insert_id)) ? $insert_id : $this->input->post('departments'),
                'name' => $this->input->post('designation'),
                //'description' => $this->input->post('description'),
                'redirect_url' => $redirect_url,
                'permissions' => serialize($this->input->post('permission')),
                'status' => $status,
            );

            if (!empty($this->input->post('edit_id'))) {

                if ($this->db->where('id', $this->input->post('edit_id'))->update('designations', $groupData)) {

                    /* user group creation ok */
                    $this->session->set_flashdata('alert_success', 'User group updated successfully.');
                    redirect('admindashboard/department_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->db->insert('designations', $groupData)) {

                    /* user group creation ok */
                    $this->session->set_flashdata('alert_success', 'User group created successfully.');
                    redirect('admindashboard/department_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* user group creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating user group. Please try again.');

                redirect('admindashboard/create_group');
            }
        }
    }

    public function assign_department($id = null) {

        $data = new stdClass();

        if (($id != null) && ($id > 0)) {
            $data->edit = $this->db->select('id,designation_id,hierarchy_id,country_id,zone_id,state_id,district_id,city_id')->where('id', $id)->get('user')->row();
        }

        $data->employee_list = $this->db->where('type', 1)->get('user')->result();

        $data->departments = $this->db->where('status', 1)->get('departments')->result();
        $data->designations = $this->db->where('status', 1)->get('designations')->result();
        $data->country_list = $this->db->where('id', 101)->get('countries')->result();
        $data->state_list = $this->db->where('country_id', 101)->get('states')->result();

        $this->load->view('admin/assign_department', $data);
    }

    public function post_assign_department() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('employee_id', 'employee', 'trim|required');
        $this->form_validation->set_rules('designation_id', 'designation', 'trim|required');


        if ($this->form_validation->run() === false) {
            /* validation not ok, send validation errors to the view */
            $this->assign_department();
        } else {

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $userData = array(
                'designation_id' => $this->input->post('designation_id'),
                'contact2' => $this->input->post('contact2'),
                'country_id' => $this->input->post('country_id'),
                'zone_id' => $this->input->post('zone_id'),
                'state_id' => $this->input->post('state_id'),
                'city_id' => $this->input->post('city_id'),
                'hierarchy_id' => $this->input->post('hierarchy_level'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->db->where('id', $edit_id)->update('user', $userData)) {

                    /* user updation ok */
                    $this->session->set_flashdata('alert_success', 'User department updated successfully.');
                    redirect('admindashboard/userlist');
                } else {
                    $failed = true;
                }
            } else {

                $failed = true;
            }

            if (isset($failed)) {

                /* user creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in assiging user department. Please try again.');

                redirect('admindashboard/userlist');
            }
        }
    }

    public function customerlist() {

        $data = new stdClass();
        $data->customer_list = $this->db
                        ->select('user.*')
                        ->from('user')
                        ->where('user.type', 4)
                        ->order_by("id", "desc")
                        ->get()->result();
        //print_r($data);die;
        $this->load->view('admin/customer_list', $data);
    }

    public function edit_customer($id = null) {
        /* create the data object */
        $data = new stdClass();

        $user_id = $this->session->user_id;
        $user = $this->db->where('id', $user_id)->get('user')->row();

        /* if edit user  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->db->where('id', $id)->get('user')->row();
        }

        $data->departments = $this->db->where('status', 1)->get('departments')->result();
        $data->designations = $this->db->where('status', 1)->get('designations')->result();

        $data->state_list = $this->db->where('country_id', 101)->get('states')->result();

        if ($user->hierarchy_id == 0) {
            $data->group_list = $this->db->where('status', 1)->get('user_group')->result();
            $data->hierarchy_list = $this->db->where('status', 1)->get('user_hierarchy')->result();
        } else {
            $data->group_list = $this->db->where('status', 1)->where("id >= $user->group_id")->get('user_group')->result();
            $data->hierarchy_list = $this->db->where('status', 1)->where("id > $user->hierarchy_id")->get('user_hierarchy')->result();
        }

        if ($user->hierarchy_id == 1) {
            $data->zone_list = $this->db->where('status', 1)->where('id', 1)->get('user_zones')->result();
        } else {
            $data->zone_list = $this->db->where('status', 1)->get('user_zones')->result();
        }

        $this->load->view('admin/edit_customer', $data);
    }

    public function post_edit_customer() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('firstname', 'first name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('lastname', 'last name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('contact1', 'mobile number', 'trim|required|regex_match[/^[0-9]{10}$/]|min_length[10]|max_length[10]');
        //$this->form_validation->set_rules('pincode', 'pincode', 'trim|required');
        //$this->form_validation->set_rules('address', 'address', 'trim|required');
        if (empty($edit_id)) {
            $this->form_validation->set_rules('email', 'email ID', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
        }

        if ($this->form_validation->run() === false) {
            /* validation not ok, send validation errors to the view */
            redirect("admindashboard/edit_customer/$edit_id");
        } else {

            if (!empty($_FILES['profile_image']['name'])) {

                if ($this->image_upload()) {
                    $profile_image = $this->upload_data['profile_image']['file_name'];
                    $oldimgpath = FCPATH . "uploads/userimages/" . $this->input->post('oldimg');
                    if (file_exists($oldimgpath) && (!empty($this->input->post('oldimg')))) {
                        unlink($oldimgpath);
                        $this->session->set_userdata('image', $profile_image);
                    }
                } else {
                    return false;
                    $profile_image = $this->input->post('oldimg');
                }
            } else {

                $profile_image = $this->input->post('oldimg');
            }

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $userData = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                //'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'contact1' => $this->input->post('contact1'),
                'address' => $this->input->post('address'),
                'pincode' => $this->input->post('pincode'),
                'image' => $profile_image,
                'status' => $status,
            );

            if (!empty($edit_id)) {
                if (empty($this->input->post('password'))) {
                    unset($userData['password']);
                }
                if ($this->db->where('id', $edit_id)->update('user', $userData)) {

                    /* user updation ok */
                    $this->session->set_flashdata('alert_success', 'Customer profile updated successfully.');
                    redirect('admindashboard/customerlist');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->db->insert('user', $userData)) {

                    /* user creation ok */
                    $this->session->set_flashdata('alert_success', 'Customer profile created successfully.');
                    redirect('admindashboard/customerlist');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* user creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating customer. Please try again.');

                redirect('admindashboard/customerlist');
            }
        }
    }

    public function memberlist($userid) {

        $data = new stdClass();
        $data->member_list = $this->db
                        ->select('members.*')
                        ->from('members')
                        ->where('members.user_id', $userid)
                        ->order_by("members.created_at", "desc")
                        ->get()->result();
        //print_r($data->member_list);die;
        $this->load->view('admin/member_list', $data);
    }

    public function questionnaire($id) {

        $data = new stdClass();
        $data->member = $this->db->where('id', $id)->get('members')->row();
        $data->question_list = $this->db->where('member_id', $id)->get('questionnaire')->result();
        //print_r($data);die;
        $this->load->view('admin/question_list', $data);
    }

    public function active_kits() {

        /* create the data object */
        $data = new stdClass();

        $data->dispatch_list = $this->db->from('qrcode_mapping')
                        ->join('orders', 'orders.id = qrcode_mapping.order_id')
                        ->join('user', 'user.id = orders.user_id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile1,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode,qrcode_mapping.created_date as dispatch_date,qrcode_mapping.qrcode,qrcode_mapping.is_active,qrcode_mapping.id as map_id')
                        ->order_by('qrcode_mapping.created_date', 'desc')
                        ->get()->result();

        //print_r($data->dispatch_list);die;
        $this->load->view('admin/active_kit_list', $data);
    }

    public function active_kit_detail($id = null) {
        $data['kit'] = $this->db->from('qrcode_mapping')
                        ->join('orders', 'orders.id = qrcode_mapping.order_id')
                        ->join('user', 'user.id = orders.user_id')
                        //->join('orders','orders.user_id = user.id')
                        /* ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile1,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode,states.name as state,cities.name as city,qrcode_mapping.created_date as dispatch_date,qrcode_mapping.qrcode,qrcode_mapping.is_active,qrcode_mapping.id as map_id') */
                        ->where('qrcode_mapping.id', $id)
                        ->order_by('qrcode_mapping.created_date', 'desc')
                        ->get()->row();

        if (empty($data['kit'])) {

            $this->session->set_flashdata('alert_danger', 'Invalid record.');

            redirect('admindashboard/active_kits');
        }
        //print_r($data['kit']);die;
        $this->load->view('admin/active_kit_detail', $data);
    }

    public function orderlist() {

        $data = new stdClass();
        $data->order_list = $this->db->from('orders')
                        ->join('user', 'user.id = orders.user_id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2')
                        ->order_by('orders.order_date', 'desc')->get()->result();
        //print_r($data->order_list);die;
        $this->load->view('admin/order_list', $data);
    }

    public function order_detail($order_id = null) {

        $this->db->select('orders.*');
        $this->db->from('orders');
        //$this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where(array('orders.id' => $order_id));
        $data['orders'] = $orders = $this->db->get()->row();

        $data['orderDetails'] = "";

        if (!empty($orders) && !empty($orders->id)) {

            $this->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image,tax.sgst_tax,tax.cgst_tax,tax.igst_tax,tax.state_id as pck_state_id,plan.package_id');
            $this->db->from('order_detail');
            $this->db->join('plan', 'order_detail.plan_id = plan.id');
            $this->db->join('tax', 'tax.package_id = plan.package_id');
            $this->db->where('order_detail.order_id', $order_id);
            $data['orderDetails'] = $this->db->get()->result();
        }
        $this->load->view('admin/order_detail', $data);
    }

    public function dfdashboard() {
        $data['title'] = 'dashboard';
        $this->load->view('admin/default_dashboard', $data);
    }

    public function dietplanlist() {
        $data['title'] = 'Diet Plan';
        $this->db->select('*');
        $this->db->from('diet_plan');
        $this->db->order_by('diet_plan.id', 'DESC');
        $data['dietplanlist'] = $this->db->get()->result();
        $this->load->view('admin/diet_plan_list', $data);
    }

    public function dietplandetails($diteplan_id = "") {
        $data['title'] = 'Diet Plan Details';
        $this->db->select('*');
        $this->db->from('diet_plan_report');
        $this->db->where('diet_plan_report.diteplan_id', $diteplan_id);
        $data['dietplanlist'] = $this->db->get()->row();
        $this->load->view('admin/dietplan_details', $data);
    }

    public function dietplan() {
        $data['title'] = 'dashboard';
        $this->load->view('admin/add_diet_plan', $data);
    }

    public function send_dietplan() {
        $id = $this->input->post('id');

        $chk = $this->db->where('id', $id)->where('status', 0)->get('diet_plan')->row();

        if ($chk) {

            $attachment = FCPATH . "uploads/dietplan/" . $chk->dietpdf;
            $message = $this->load->view('email_template/dietplan', '', true);
            $message = str_replace("{username}", $chk->name, $message);
            $message = str_replace("{barcode}", $chk->barcode, $message);
            $subject = "Diet Plan for Barcode " . $chk->barcode;
            $this->Comman_model->emailsend($chk->email, $subject, $message, $attachment);
            //$this->Comman_model->emailsend('vikas.narmadatech@gmail.com', $subject, $message, $attachment);
            $this->Comman_model->update('diet_plan', ['id' => $id], ['status' => 1]);
            $result = array('status' => true, 'msg' => "Report sent successfully.");
            echo json_encode($result);
            die;
        } else {
            $result = array('status' => false, 'msg' => "Report already sent.");
            echo json_encode($result);
            die;
        }
    }

    public function getbarcodeinfo() {
        $barcode = $this->input->post('barcode');
        $this->db->select('members.id,members.user_id,members.plan_id,members.name,members.email,members.contact,plan.name as plan_name');
        $this->db->from('members');
        $this->db->join('plan', 'members.plan_id = plan.id');
        $this->db->where('members.barcode', $barcode);
        $barcodeDetails = $this->db->get()->row();
        if (!empty($barcodeDetails)) {
            echo json_encode(array('status' => true, 'data' => $barcodeDetails, 'message' => ''));
        } else {
            echo json_encode(array('status' => false, 'message' => 'This Barcode not is valid'));
        }
        die;
    }

    public function savedietplan() {

        $this->form_validation->set_rules('barcode', 'barcode', 'trim|required');

        if ($this->form_validation->run() === false) {
            $this->dietplan();
        } else {
            $customer_name = $this->input->post('customer_name');
            $customer_email = $this->input->post('customer_email');
            $customer_contact = $this->input->post('customer_contact');
            $meal = $this->input->post('meal');
            $timing = $this->input->post('timing');
            $proitites = $this->input->post('proitites');
            $tips = $this->input->post('tips');
            $indication = $this->input->post('indication');
            $barcode = $this->input->post('barcode');
            $ind_txt = '';
            foreach ($indication as $indc) {
                $ind_txt .= '<li style="font-size: 12px; line-height: 20px; font-family: "Montserrat";">' . $indc . '</li>';
            }

            $data['diteplanData'] = array('user_id' => $this->input->post('customer_id'),
                'plan_id' => $this->input->post('plan_id'),
                'member_id' => $this->input->post('members_id'),
                'barcode' => $this->input->post('barcode'),
                'name' => $this->input->post('customer_name'),
                'email' => $this->input->post('customer_email'),
                'customer_contact' => $this->input->post('customer_contact'),
                'indication' => $ind_txt,
                'note' => $this->input->post('note'),);

            $insertedid = $this->Comman_model->save('diet_plan', $data['diteplanData']);

            if (!empty($insertedid)) {

                $diet_plan = array();
                foreach ($meal as $key => $mealVal) {
                    $answerData = array();
                    $answerData['diteplan_id'] = $insertedid;
                    $answerData['meal'] = $mealVal;
                    $answerData['timing'] = $timing[$key];
                    $answerData['proitites'] = $proitites[$key];
                    $answerData['tips'] = $tips[$key];
                    $diet_plan[$key] = $answerData;
                }
                $this->db->insert_batch('diet_plan_report', $diet_plan);
                $data['diet_plan'] = $diet_plan;
                $pdfhtml = $this->load->view('dietplan', $data, true);
                $this->load->library('Mfpdf');
                $this->mfpdf->SetFont('Montserrat');
                $this->mfpdf->WriteHTML($pdfhtml);
                //$this->mfpdf->Output('invoice.pdf', 'D');
                ob_clean();
                //$pdf->Output('invoice'.rand().'.pdf', 'I');
                $pdfname = 'diet' . $barcode . rand() . '.pdf';
                $pdfpath = FCPATH . 'uploads/dietplan/' . $pdfname;
                $this->mfpdf->Output($pdfpath, 'F');
                $this->Comman_model->update('diet_plan', ['id' => $insertedid], ['dietpdf' => $pdfname]);
                $this->session->set_flashdata('alert_success', 'Diet Plan save successfully.');
                $this->session->keep_flashdata('alert_success');
                redirect('admindashboard/dietplanlist');
            }
        }
    }

}
