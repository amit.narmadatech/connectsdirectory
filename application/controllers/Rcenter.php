<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Rcenter class.
 * 
 * @extends CI_Controller
 */
class Rcenter extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        /* helper function to check user logged in or not */
        if (!is_admin()) {
            redirect('Admin');
        }

        if (!check_permission()) {
            //redirect('access_denied');
        }

        /* echo serialize(array(
          'NTF_USER_NAME' => 'member name',
          'NTF_BARCODE' => 'barcode',
          'NTF_ORDER_NUMBER' => 'order number',
          'NTF_PLAN' => 'plan name'
          ));die; */

        $this->load->model('comman_model');
        $this->load->model('template_model');
    }

    /**
     * render inventory dashboard.
     * 
     * @access public
     * @return void
     */
    public function dashboard() {

        /* create the data object */
        $data = [];

        $data['total_pending_reports'] = $this->db->where('status', 2)->get('collected_kits')->num_rows();
        $data['total_received_reports'] = $this->db->where('status', 0)->get('test_reports')->num_rows();
        $data['total_reports'] = $data['total_pending_reports'] + $data['total_received_reports'];
        //print_r($data);die;
        $this->load->view('admin/report_center/report_center_dashboard', $data);
    }

    /**
     * render upload test report .
     * 
     * @access public
     * @return void
     */
    public function upload_report() {

        /* create the data object */
        $data = [];
        $data['report_list'] = $this->comman_model->getAll('test_reports', ['']);
        $this->load->view('admin/report_center/upload_report', $data);
    }

    /**
     * upload/update request order.
     * 
     * @access public
     * @return void
     */
    public function post_upload_report() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        //$this->form_validation->set_rules('qrcode', 'qrcode', 'trim|required');
        $this->form_validation->set_rules('barcode', 'barcode', 'trim|required|is_unique[test_reports.code]');
        //$this->form_validation->set_rules('report_file', 'test report file', 'trim|required');
        //$this->form_validation->set_rules('prescription_file', 'prescription file ', 'trim|required');
        $this->form_validation->set_rules('note', 'Note ', 'trim');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->upload_report();
        } else {

            $chk = $this->db->where('barcode', $this->input->post('barcode'))->get('collected_kits')->num_rows();

            if ($chk == 0) {
                $this->session->set_flashdata('alert_danger', 'This barcode is not valid.');
                $this->session->keep_flashdata('alert_danger');
                redirect('rcenter/upload_report');
            }

            if (!empty($_FILES['report_file']['name'])) {

                if ($this->image_upload('uploads/report_files/', 'report_file')) {
                    $report_file = $this->upload_data['report_file']['file_name'];
                    $oldreportpath = FCPATH . "uploads/report_files/" . $this->input->post('oldreportfile');
                    if (file_exists($oldreportpath) && (!empty($this->input->post('oldreportfile')))) {

                        unlink($oldreportpath);
                    }
                } else {
                    return false;
                    $report_file = $this->input->post('oldreportfile');
                }
            } else {

                $report_file = $this->input->post('oldreportfile');
            }

            if (!empty($_FILES['report_pdf']['name'])) {

                if ($this->image_upload('uploads/report_files/', 'report_pdf')) {
                    $pdfreport_file = $this->upload_data['report_pdf']['file_name'];
                    $oldpdfreportpath = FCPATH . "uploads/report_files/" . $this->input->post('oldpdfreport');
                    if (file_exists($oldpdfreportpath) && (!empty($this->input->post('oldpdfreport')))) {

                        unlink($oldpdfreportpath);
                    }
                } else {
                    return false;
                    $pdfreport_file = $this->input->post('oldpdfreport');
                }
            } else {

                $pdfreport_file = $this->input->post('oldpdfreport');
            }

            /* set variables from the form */

            $string = file_get_contents(base_url('uploads/report_files/' . $report_file));

            $json_a = json_decode($string, true);

            $insertData = array(
                'code' => $this->input->post('barcode'),
                'file' => $report_file,
                /*'file_data' => $string,
                'your_report' => $json_a['your report'][0][0],
                'how_to_read' => $json_a['how to read'][0][0],
                'table1' => json_encode($json_a['table1']),
                'table2' => json_encode($json_a['table2']),
                'table3' => json_encode($json_a['table3']),
                'table4' => json_encode($json_a['table4']),
                'table5' => json_encode($json_a['table5']),
                'disclaimer' => $json_a['disclaimer'][0][0],
                'graphical_version' => $json_a['graphical_version'],
                'cfp' => $json_a['CFP'],
                'es_pie' => $json_a['ES_pie'],
                'pfc' => $json_a['PFC'],
                'epf' => $json_a['EPF'],
                'name' => $json_a['Name'],
                'tid' => $json_a['ID'],
                'uploaded_file' => $json_a['uploaded_file'],*/
                'pdf_file' => $pdfreport_file,
                //'partner' => $json_a['partner'],
                //'package' => $json_a['package'],
                'created_at' => date('Y-m-d H:i:s'),
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('purchase_request', ['id' => $edit_id], $insertData)) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    $this->comman_model->delete(['request_id' => $edit_id], 'purchase_request_detail');
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'request_id' => $edit_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'amount' => $amount[$i],
                        );

                        $this->comman_model->save('purchase_request_detail', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Request order updated successfully.');
                    redirect('inventory/request_order_list');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('test_reports', $insertData);

                if ($insert_id) {

                    $barcode = trim($this->input->post('barcode'));

                    $chk = $this->comman_model->update('members', ['barcode' => $barcode], ['report_file' => $pdfreport_file]);

                    if ($chk) {

                        // START - sending notification 
                        $this->load->model('notification_model');

                        $product = $this->db->from('members')
                                        ->join('plan', 'plan.id = members.plan_id')
                                        ->join('user', 'user.id = members.user_id')
                                        ->select('plan.name,members.barcode,members.user_id,user.email,user.contact1,CONCAT(user.firstname," ",user.lastname) as fullname')
                                        ->where('members.barcode', $barcode)
                                        ->get()->row();

                        // load a template 			
                        $tmpdata = array(
                            'NTF_BARCODE' => $product->barcode,
                            'NTF_PLAN' => $product->name,
                        );

                        $templateData = get_notification(10, $tmpdata);

                        // sending notification to admin 
                        $this->notification_model->add(array(
                            'sender_id' => 0,
                            'type' => 'customer_notification',
                            'label' => 'report_updated',
                            'title' => $templateData['title'],
                            'body' => $templateData['body'],
                            'href' => '',
                            'recipient_id' => $product->user_id,
                        ));

                        $templateData = get_notification(11, $tmpdata);

                        // sending notification to admin 
                        $this->notification_model->add(array(
                            'sender_id' => 0,
                            'type' => 'admin_broadcast',
                            'label' => 'report_updated',
                            'title' => $templateData['title'],
                            'body' => $templateData['body'],
                            'href' => '',
                            'recipient_id' => 0,
                        ));

                        // END - sending notification
                        // Sending sms
                        $mobile = $product->contact1;
                        if (!empty($mobile) && (strlen($mobile) == 10)) {
                            $sms = New Sms();
                            $message = "Dear ".$product->fullname.",\nyour report for " . $product->name . " Plan and " . $product->barcode . " Barcode has been sent to your Email ID and Mobile App with a FREE subscription plan of 90 days for you.\nYou can also login to your JC Genetics account to check the report. Get in touch with our genetic experts for assistance.\n\nThanks,\nJC Genetics Team";
                            $sms->sendsms($mobile, $message);
                        }

                        // sending email notification

                        $email = $order->user_email;
                        $message = $this->load->view('email_template/report_sent', '', true);
                        $message = str_replace("{username}", $product->fullname, $message);
                        $message = str_replace("{planname}", $product->name, $message);
                        $message = str_replace("{barcode}", $product->barcode, $message);
                        $subject = "Report Sent On Your Website and Mobile App.";
                        $this->comman_model->emailsend($email, $subject, $message);
                        //$this->comman_model->emailsend('amit.narmadatech@gmail.com', $subject, $message);
                    }

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Report uploaded successfully.');
                    $this->session->keep_flashdata('alert_success');
                    redirect('rcenter/report_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in uploading report. Please try again.');

                redirect('rcenter/upload_report');
            }
        }
    }

    /* render customer report list.*/
    public function report_list() {
        $data = [];
        $data['report_list'] = $this->comman_model->getAll('test_reports', ['']);
        $this->load->view('admin/report_center/report_list', $data);
    }
    public function post_request_order_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('date', 'date', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'reference_no', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
        $this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');
        //$this->form_validation->set_rules('request_status', 'status ', 'trim|required');
        $this->form_validation->set_rules('description', 'Note ', 'trim');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->request_order_form();
        } else {

            /* set variables from the form */
            $insertData = array(
                'date' => $this->input->post('date'),
                'reference_no' => $this->input->post('reference_no'),
                'supplier_id' => $this->input->post('supplier_id'),
                //'purchase_status' => $this->input->post('request_status'), 
                'warehouse_id' => $this->input->post('warehouse_id'),
                'description' => $this->input->post('description'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('purchase_request', ['id' => $edit_id], $insertData)) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    $this->comman_model->delete(['request_id' => $edit_id], 'purchase_request_detail');
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'request_id' => $edit_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'amount' => $amount[$i],
                        );

                        $this->comman_model->save('purchase_request_detail', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Request order updated successfully.');
                    redirect('inventory/request_order_list');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('purchase_request', $insertData);
                if ($insert_id) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'request_id' => $insert_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'amount' => $amount[$i],
                        );

                        $this->comman_model->save('purchase_request_detail', $requestDetail);
                    }

                    /* START - sending notification */
                    $this->load->model('notification_model');

                    $product = $this->db->from('purchase_request')
                                    ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                                    ->join('products', 'products.id = purchase_request_detail.product_id')
                                    ->select('purchase_request.reference_no,products.name as product_name,products.code as product_code')
                                    ->where('purchase_request.id', $insert_id)
                                    ->get()->row();

                    /* load a template */
                    $tmpdata = array(
                        'NTF_PRODUCT_NAME' => $product->product_name,
                        'NTF_PRODUCT_CODE' => $product->product_code,
                        'NTF_PO_NUMBER' => $product->reference_no,
                    );

                    $templateData = get_notification(5, $tmpdata);

                    /* sending notification to admin */
                    $this->notification_model->add(array(
                        'sender_id' => 0,
                        'type' => 'admin_broadcast',
                        'label' => 'purchase_order_placed',
                        'title' => $templateData['title'],
                        'body' => $templateData['body'],
                        'href' => '',
                        'recipient_id' => 0,
                    ));

                    // END - sending notification

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Request order added successfully.');
                    redirect('inventory/request_order_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating request order. Please try again.');

                redirect('inventory/add_warehouse');
            }
        }
    }

    function image_upload($upload_dir, $input_field) {
        //$upload_dir = 'uploads/userimages/';
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir);
        }

        $config['upload_path'] = $upload_dir;
        $config['allowed_types'] = '*';
        $config['file_name'] = $input_field . '_' . substr(md5(rand()), 0, 7);
        $config['overwrite'] = false;
        $config['max_size'] = '5120';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($input_field)) {

            /* image upload errors */
            $this->session->set_flashdata('alert_danger', $this->upload->display_errors());

            $this->upload_report();
            return false;
        } else {
            $this->upload_data[$input_field] = $this->upload->data();
            return true;
        }
    }

    /**
     * render purchase order list .
     * 
     * @access public
     * @return void
     */
    public function po_list() {

        /* create the data object */
        $data = new stdClass();
        $data->request_list = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request.*,warehouses.title as warehouse_name,supplier.name as supplier_name,products.name as product_name,purchase_request_detail.quantity as product_qty')
                        ->group_by('purchase_request.id')
                        ->order_by('purchase_request.created_at', 'desc')
                        ->get()->result();

        //print_r($data->request_list);die;
        $this->load->view('admin/report_center/po_list', $data);
    }

    /**
     * render kit scan form.
     * 
     * @access public
     * @return void
     */
    public function scan_kits($id = null) {

        /* create the data object */
        $data = new stdClass();

        $data->scanned_list = $this->comman_model->getAll('xcode_inventory', ['request_id' => $id]);
        //print_r($data->scanned_list);die;

        $data->edit = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->select('purchase_request.*,warehouses.title as w_name,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,warehouses.district_id,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                        ->where('purchase_request.id', $id)
                        ->get()->row();

        $data->po_request_details = $this->db->from('purchase_request_detail')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request_detail.*, products.name as product_name,products.code as product_code,purchase_request_detail.quantity as product_qty')
                        ->where('purchase_request_detail.request_id', $id)
                        ->get()->result();

        //print_r($data);die;

        $this->load->view('admin/report_center/scan_kits', $data);
    }

    /**
     * render po dispatched form.
     * 
     * @access public
     * @return void
     */
    public function dispatch_order($po_no = null) {

        /* create the data object */
        $data = new stdClass();

        if (($po_no != null) && ($po_no > 0)) {

            /* $chk = $this->db->where('id',$po_no)->get('purchase_request')->num_rows();

              if ($chk > 0) {
              $this->session->set_flashdata('alert_danger', "This order No. is already dispatched ! Please check input.");
              redirect("inventory/stock_out");
              } */

            $data->edit = $this->db->from('purchase_request')
                            ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                            ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                            ->select('purchase_request.*,warehouses.title as w_name,warehouses.contact_person as w_contact_person,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,warehouses.district_id,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                            ->where('purchase_request.id', $po_no)
                            ->get()->row();

            $data->edit_details = $this->db->from('purchase_request_detail')
                            ->join('products', 'products.id = purchase_request_detail.product_id')
                            ->select('purchase_request_detail.*, products.name as product_name,products.code as product_code,purchase_request_detail.quantity as product_qty')
                            ->where('purchase_request_detail.request_id', $po_no)
                            ->get()->result();

            //print_r($data);die;
        }
        //print_r($data->order_list);die;
        $this->load->view('admin/report_center/dispatch_order', $data);
    }

    /**
     * render dispatch list.
     * 
     * @access public
     * @return void
     */
    public function dispatch_list() {

        /* create the data object */
        $data = new stdClass();

        $data->dispatch_list = $this->db->order_by('created_at')
                        ->get('xcode_dispatch')->result();

        //print_r($data->dispatch_list);die;
        $this->load->view('admin/report_center/dispatch_order_list', $data);
    }

    public function order_detail($order_id = null) {

        $data = new stdClass();

        $data->order = $this->db->from('orders')
                        ->join('user', 'user.id = orders.user_id')
                        //->join('billing_address', 'billing_address.user_id = user.id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile1,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode,orders.address as bl_address,orders.district_id')
                        ->where('orders.id', $order_id)
                        ->get()->row();

        $data->order_details = $this->db->from('order_detail')
                        ->join('plan', 'plan.id = order_detail.plan_id')
                        ->select('order_detail.*, plan.plan_image, plan.description, plan.sort_description,plan.name as plan_name')
                        ->where('order_detail.order_id', $order_id)
                        ->get()->result();

        //print_r($data);die;
        $this->load->view('admin/order_detail', $data);
    }

    /**
     * render list of sent kits to SANDOR LAB.
     * 
     * @access public
     * @return void
     */
    public function sent_kits_lab($value = '') {
        /* create the data object */
        $data = new stdClass();

        $id = [];
        //$records = $this->db->where_in('status',1)->select('barcodes')->get('collection_dispatch')->result();
        $records = $this->db->where('status', 1)->select('barcodes')->get('collection_dispatch')->result();

        if (!empty($records)) {
            foreach ($records as $rcd) {
                foreach (explode(',', $rcd->barcodes) as $value) {

                    if (!in_array($value, $id)) {
                        array_push($id, $value);
                    }
                }
            }
            $data->kit_list = $this->db->from('collected_kits')
                            ->join('plan', 'plan.id = collected_kits.plan_id')
                            ->select('collected_kits.*,plan.name as plan_name')
                            ->where_in('collected_kits.id', $id)->order_by('collected_kits.created_at', 'desc')
                            ->get()->result();
        }

        $this->load->view('admin/report_center/sent_kits_tolab', $data);
    }

    /**
     * render damage kit entry form.
     * 
     * @access public
     * @return void
     */
    public function damage_entry($order_id = null) {

        /* create the data object */
        $data = new stdClass();

        if (($order_id != null) && ($order_id > 0)) {
            
        }

        $data->damage_list = $this->comman_model->getAll('xcode_damagekits', ['']);
        //print_r($data->order_list);die;
        $this->load->view('admin/report_center/damage_kit_entry', $data);
    }

    /**
     * insert damage kit entry.
     * 
     * @access public
     * @return void
     */
    public function post_damage_entry() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        //$this->form_validation->set_rules('qrcode', 'qrcode', 'trim|required');
        $this->form_validation->set_rules('barcode', 'barcode', 'trim|required|is_unique[test_reports.code]');
        $this->form_validation->set_rules('note', 'Note ', 'trim|required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */

            $this->damage_entry();
        } else {

            $chk = $this->db->where('barcode', $this->input->post('barcode'))->get('xcode_damagekits')->num_rows();

            if ($chk > 0) {
                $this->session->set_flashdata('alert_danger', 'This barcode has already inserted !');

                redirect('rcenter/damage_entry');
            }

            if (!empty($_FILES['damage_img']['name'])) {

                if ($this->image_upload('uploads/damage_img/', 'damage_img')) {
                    $report_file = $this->upload_data['damage_img']['file_name'];
                    $oldreportpath = FCPATH . "uploads/damage_img/" . $this->input->post('oldimg');
                    if (file_exists($oldreportpath) && (!empty($this->input->post('oldimg')))) {
                        unlink($oldreportpath);
                    }
                } else {
                    return false;
                    $report_file = $this->input->post('oldimg');
                }
            } else {

                $report_file = $this->input->post('oldimg');
            }

            /* set variables from the form */

            $insertData = array(
                'barcode' => $this->input->post('barcode'),
                //'qrcode' => $this->input->post('qrcode'), 
                'damage_img' => $report_file,
                'reason' => $this->input->post('note'),
                'created_at' => date('Y-m-d H:i:s'),
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('xcode_damagekits', ['id' => $edit_id], $insertData)) {

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Record updated successfully.');
                    redirect('rcenter/damage_entry');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('xcode_damagekits', $insertData);

                if ($insert_id) {

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Record inserted successfully.');
                    redirect('rcenter/damage_entry');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating record. Please try again.');

                redirect('rcenter/damage_entry');
            }
        }
    }

    /**
     * render list of invoice.
     * 
     * @access public
     * @return void
     */
    public function invoice_list() {
        /* create the data object */
        $data = new stdClass();
        $data->request_list = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request.*,warehouses.title as warehouse_name,supplier.name as supplier_name,products.name as product_name,purchase_request_detail.quantity as product_qty')
                        ->group_by('purchase_request.id')
                        ->order_by('purchase_request.created_at', 'desc')
                        ->get()->result();

        //print_r($data->request_list);die;
        $this->load->view('admin/report_center/invoice_list', $data);
    }

    /**
     * upload/update sealed and signed invoice.
     * 
     * @access public
     * @return void
     */
    public function post_upload_invoice() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');
        /* set validation rules */

        $this->form_validation->set_rules('report_file', 'invoice file', 'trim|required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            redirect('rcenter/invoice_list');
        } else {

            if (!empty($_FILES['report_file']['name'])) {

                if ($this->image_upload('uploads/invoice/', 'report_file')) {
                    $report_file = $this->upload_data['report_file']['file_name'];
                    $oldreportpath = FCPATH . "uploads/invoice/" . $this->input->post('oldreportfile');
                    if (file_exists($oldreportpath) && (!empty($this->input->post('oldinvoicefile')))) {

                        unlink($oldreportpath);
                    }
                } else {
                    return false;
                    $report_file = $this->input->post('oldinvoicefile');
                }
            } else {

                $report_file = $this->input->post('oldinvoicefile');
            }

            /* set variables from the form */

            $insertData = array(
                'file' => $report_file,
                'created_at' => date('Y-m-d H:i:s'),
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('purchase_request', ['id' => $edit_id], $insertData)) {

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Request order updated successfully.');
                    redirect('inventory/request_order_list');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('test_reports', $insertData);

                if ($insert_id) {

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Report uploaded successfully.');
                    redirect('rcenter/upload_report');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in uploading report. Please try again.');

                redirect('rcenter/upload_report');
            }
        }
    }

    /**
     * insert barcode to database.
     * 
     * @access public
     * @return void
     */
    public function post_scan_barcode() {
        if ($this->input->post()) {
            $barcode = trim($this->input->post('barcode'));
            $reqid = trim($this->input->post('reqid'));
            $refno = trim($this->input->post('refno'));
            if (!empty($barcode)) {
                // check requested quantity is filled or not
                $req_qty = countPORequestedKit($reqid);
                $scan_qty = countScannedKit($reqid);
                if ($scan_qty >= $req_qty) {
                    echo json_encode(array('status' => false, 'msg' => 'Requested kit quantity has filled !'));
                    die;
                }

                $result = $this->db->where('barcode', $barcode)->get('xcode_inventory')->num_rows();

                if ($result > 0) {
                    echo json_encode(array('status' => false, 'msg' => "Barcode: $barcode already used !"));
                    die;
                } else {
                    $insertData = array(
                        'request_id' => $reqid,
                        'reference_no' => $refno,
                        'barcode' => $barcode,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $insert_id = $this->comman_model->save('xcode_inventory', $insertData);
                    if ($insert_id) {
                        echo json_encode(array('status' => true, 'insert_id' => $insert_id));
                        die;
                    } else {
                        echo json_encode(array('status' => false));
                        die;
                    }
                }
            }
        }
    }

    /**
     * remove barcode from database.
     * 
     * @access public
     * @return void
     */
    public function remove_barcode() {
        if ($this->input->post()) {
            $id = trim($this->input->post('id'));
            if (!empty($id)) {
                $result = $this->db->where('id', $id)->get('xcode_inventory')->num_rows();

                if ($result > 0) {
                    $this->comman_model->delete(['id' => $id], 'xcode_inventory');
                    echo json_encode(array('status' => true));
                    die;
                } else {

                    echo json_encode(array('status' => false));
                    die;
                }
            }
        }
    }

    /**
     * insert barcodes to database.
     * 
     * @access public
     * @return void
     */
    public function post_scanned_kit() {
        if ($this->input->post()) {
            $failed = [];
            $barcodes = $this->input->post('barcode');
            $size = sizeof($barcodes);
            if ($size > 0) {
                $reqid = trim($this->input->post('request_id'));
                $refno = trim($this->input->post('reference_no'));

                foreach ($barcodes as $barcode) {
                    $chk = $this->comman_model->update('xcode_inventory', ['barcode' => $barcode, 'status' => 0], ['status' => 1]);
                    if ($chk) {
                        
                    } else {
                        array_push($failed, $barcode);
                    }
                }

                $this->session->set_flashdata('alert_success', 'Kits uploaded successfully.');
                $this->session->keep_flashdata('alert_success');
                redirect('rcenter/po_list');
            } else {
                $this->session->set_flashdata('alert_danger', 'Uploading failed.');
                $this->session->keep_flashdata('alert_danger');

                redirect('rcenter/po_list');
            }
        }
    }

    /**
     * render stock out form.
     * 
     * @access public
     * @return void
     */
    public function get_po_by_ponumber($po_no = null) {

        /* create the data object */
        $data = new stdClass();

        if ($po_no != null) {

            $find = $this->db->where('reference_no', $po_no)->get('purchase_request')->row();

            if ($find) {
                redirect("rcenter/dispatch_order/$find->id");
            } else {
                $this->session->set_flashdata('alert_danger', 'No record found! Please check input.');
                redirect("rcenter/dispatch_order");
            }
        } else {
            redirect('rcenter/dispatch_order');
        }
    }

    /**
     * get scanned kit list by po id for AJAX REQUEST.
     * 
     * @access public
     * @return void
     */
    public function getScannedKitList() {
        $pid = $this->input->post('id');

        $result = $this->db->where('request_id', $pid)->get('xcode_inventory')->result();

        //print_r($pid);die;
        if ($result) {
            echo json_encode(array('status' => true, 'result' => $result));
            die;
        } else {
            echo json_encode(array('status' => false));
            die;
        }
    }

    /**
     * update stock / dispatch order.
     * 
     * @access public
     * @return void
     */
    public function post_stock_out() {
        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('shipping_partner', 'shipping partner', 'trim|required');
        $this->form_validation->set_rules('shipping_address', 'shipping address', 'trim|required');
        $this->form_validation->set_rules('edit_id', 'order id ', 'trim|required');
        $this->form_validation->set_rules('po_number', 'po number ', 'trim|required');
        $this->form_validation->set_rules('shipping_address', 'shipping address', 'trim|required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            //$this->stock_out();
            redirect("rcenter/dispatch_order/$edit_id");
        } else {

            $po_number = $this->input->post('po_number');
            $chk = $this->db->where('request_id', $edit_id)->get('xcode_dispatch')->num_rows();
            if ($chk > 0) {

                $this->session->set_flashdata('alert_danger', 'This PO request is already dispatched !');
                $this->session->keep_flashdata('alert_danger');
                redirect("rcenter/po_list");
            }

            $shipping_address = $this->input->post('shipping_address');
            $shipping_partner = $this->input->post('shipping_partner');

            $insertData = array(
                'request_id' => $edit_id,
                'reference_no' => $this->input->post('po_number'),
                'invoice_no' => $this->input->post('invoice_no'),
                'shipping_partner' => $this->input->post('shipping_partner'),
                'tracking_no' => $this->input->post('tracking_no'),
                'shipping_address' => $this->input->post('shipping_address'),
                'required_qty' => $this->input->post('required_qty'),
                'uploaded_qty' => $this->input->post('uploaded_qty'),
                'created_at' => date("Y-m-d H:i:s"),
            );

            $insert_id = $this->comman_model->save('xcode_dispatch', $insertData);

            if ($insert_id) {
                $this->comman_model->update('purchase_request', ['id' => $edit_id], ['purchase_status' => 5]);
                /* creation ok */

                /* START - sending notification */
                $this->load->model('notification_model');

                $product = $this->db
                                ->where('id', $insert_id)
                                ->get('xcode_dispatch')->row();

                /* load a template */
                $tmpdata = array(
                    'NTF_PONO' => $product->reference_no,
                    'NTF_SHIPPING' => $product->shipping_partner,
                    'NTF_TRACKING_NO' => $product->tracking_no,
                    'NTF_ULPD_QTY' => $product->uploaded_qty,
                );

                $templateData = get_notification(12, $tmpdata);

                /* sending notification to admin */
                $this->notification_model->add(array(
                    'sender_id' => 0,
                    'type' => 'admin_broadcast',
                    'label' => 'to_admin_on_xcode_po_dispatch',
                    'title' => $templateData['title'],
                    'body' => $templateData['body'],
                    'href' => '',
                    'recipient_id' => 0,
                ));

                // END - sending notification
                $this->session->set_flashdata('alert_success', 'Order dispatched successfully.');
                redirect('rcenter/dispatch_list');
            } else {
                $failed = true;
            }


            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in dispatching order. Please try again.');

                redirect("rcenter/dispatch_order/$edit_id");
            }
        }
    }

}
