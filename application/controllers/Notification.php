<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Notification class.
 * 
 * @extends CI_Controller
 */
class Notification extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        /* helper function to check user logged in or not */
        $this->load->model('notification_model');
    }

    /**
     * render notification listing.
     * 
     * @access public
     * @return void
     */
    public function index() {

        /* create the data object */
        $data = new stdClass();
        $data->notification_list = $this->notification_model->admin_notification_list();
        //print_r($data->notification_list);die;
        $this->load->view('admin/notification_list', $data);
    }

    /**
     * render user profile.
     * 
     * @access public
     * @return void
     */
    public function get_notification() {

        /* getting logged in user data and sending to profile view */
        $user_id = $this->session->user_id;
        $data = $this->notification_model->get($user_id);

        if (count($data) > 0) {
            $result = array('status' => true, 'data' => $data);
        } else {
            $result = array('status' => false);
        }

        echo json_encode($result);
        die;
    }

    /**   render user profile. * 
     * @access public
     * @return void
     */
    public function get_customer_notification() {

        $user_id = $this->session->user_id;
        if(empty($user_id)){
            return FALSE;
        }
        
        $data = $this->notification_model->get_customer_notification($user_id);

        if (count($data) > 0) {
            $result = array('status' => true, 'data' => $data);
        } else {
            $result = array('status' => false);
        }

        echo json_encode($result);
        die;
    }

    /**
     * render user profile.
     * 
     * @access public
     * @return void
     */
    public function change_status() {

        if ($this->input->post('id') != '') {
            $id = $this->input->post('id');
            $user_id = $this->session->user_id;
            $data = $this->notification_model->set_read($id, $user_id);

            if (!empty($data)) {
                $result = array('status' => true, 'data' => $data);
            } else {
                $result = array('status' => false);
            }

            echo json_encode($result);
            die;
        }
    }

}
