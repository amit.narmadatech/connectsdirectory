<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Inventory class.
 * 
 * @extends CI_Controller
 */
class Inventory extends CI_Controller {

    private $error = array();
    private $api_url = 'https://test.delhivery.com/';
    private $token = '7aab727ceb6313651f6571bacfbad9aacb3173d6';
    private $client_name = 'A2ZSALE EXPRESS';

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        /* helper function to check user logged in or not */
        if (!is_admin()) {
            redirect('Admin');
        }

        if (!check_permission()) {
            redirect('access_denied');
        }
        checkKitQuantityAlert();
        $this->load->model('inventory_model');
        $this->load->model('comman_model');
        $this->load->model('template_model');
        $this->load->library('Delhivery');
        $this->load->library('Dtdc');
    }

    public function mytesst() {
        //$txt = '<Output><ORDER><Succeed>No</Succeed><REQUESTID>R123695</REQUESTID><Reason>Pickup Pincode or location does not exist/not mapped or inactive/not serviceable by INSTACOM.</Reason><Location /><DOCKNO /></ORDER></Output>';
        //print_r(simplexml_load_string($txt));die;
        $data = array(
            'ClientId' => 'INSTACOM',
            'UserName' => 'instauser',
            'Password' => 'insta2013',
            'RequestId' => 'R123695',
            'ConsignorName' => 'vikas kanik',
            'ConsignorAddress1' => 'indore',
            'ConsignorAddress2' => 'indore',
            'MobileNo' => '8871374798',
            'Pincode' => '456001',
            'SkuDescription' => 'test sku',
            'DeclaredValue' => 100,
            'AgentId' => 'TEST',
            'CustomerCode' => 'CC000100132',
            'VendorName' => 'Amit meena',
            'VendorAddress1' => 'indore',
            'VendorAddress2' => 'indore',
            'VendorPincode' => '452010',
            'VendorTeleNo' => '7000091226',
            'TransportMode' => 'Express',
            'ItemChecked' => 'N',
            'DockNo' => '',
        );

        $dtdc = new Dtdc();
        $result = $dtdc->reverse_package($data);
        $xml = simplexml_load_string($result);
        $txt = (string) $xml[0];
        $xml = simplexml_load_string($txt);
        print_r((string) $xml->ORDER->Succeed);
        die;
    }

    /**
     * render inventory dashboard.
     * 
     * @access public
     * @return void
     */
    public function dashboard() {

        /* create the data object */
        $data = [];

        $data['title'] = 'Dashboard';
        $data['po_pending'] = $this->db->get_where('purchase_request', array('purchase_status' => 0))->num_rows();
        $data['po_received'] = $this->db->get_where('purchase_request', array('purchase_status' => 3))->num_rows();

        $ordered_stock = $this->db->from('purchase_request')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->select('SUM(purchase_request_detail.quantity) as total_qty')
                        ->get()->row();

        $received_stock = $this->db->from('stock_purchase')
                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                        ->select('SUM(stock_purchase_details.actual_qty) as total_qty')
                        ->get()->row();

        $data['ordered_stock'] = (empty($ordered_stock)) ? 0 : ($ordered_stock->total_qty) ? $ordered_stock->total_qty : 0;
        $data['received_stock'] = (empty($received_stock)) ? 0 : ($received_stock->total_qty) ? $received_stock->total_qty : 0;

        $data['kit_available'] = $this->db->get_where('kit_upload', array('is_used' => 0))->num_rows();
        $data['kit_dispatched'] = $this->db->get_where('kit_upload', array('is_used' => 1))->num_rows();

        $data['pending_order'] = $this->db->get_where('orders', array('order_status' => 1))->num_rows();
        $data['completed_order'] = $this->db->get_where('orders', array('order_status' => 2))->num_rows();

        $data['order_list'] = $this->db->from('orders')
                        ->join('user', 'user.id = orders.user_id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2')
                        ->order_by('orders.order_date', 'desc')->limit(5)->get()->result();

        $product = $this->db->get('products')->row();
        $alert_quantity = ($product) ? $product->alert_quantity : 10;

        $data['quantity_max'] = abs($alert_quantity * 10);
        $set_qty = $data['kit_available'] - $alert_quantity;
        $data['quantity_set'] = ($set_qty <= 0) ? 0 : $set_qty;

        //print_r($data);die;				
        $this->load->view('admin/inventory/inventory_dashboard', $data);
    }

    /**
     * render request purchase order list .
     * 
     * @access public
     * @return void
     */
    public function request_order_list() {

        /* create the data object */
        $data = new stdClass();
        $data->request_list = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request.*,warehouses.title as warehouse_name,supplier.name as supplier_name,products.name as product_name,purchase_request_detail.quantity as product_qty')
                        ->group_by('purchase_request.id')
                        ->order_by('purchase_request.created_at', 'desc')
                        ->get()->result();
        //$this->session->set_flashdata('alert_success', 'This is testing.');
        //print_r($data->request_list);die;
        $this->load->view('admin/inventory/requested_order_list', $data);
    }

    /**
     * render PO invoice .
     * 
     * @access public
     * @return void
     */
    public function get_po_invoice($id) {
        /* create the data object */
        $data = new stdClass();
        //$data->request_list  = $this->comman_model->getAll('purchase_request', []);
        $data->po_request = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->select('purchase_request.*,warehouses.title as w_name,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                        ->where('purchase_request.id', $id)
                        ->get()->row();

        $data->po_request_details = $this->db->from('purchase_request_detail')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request_detail.*, products.name as product_name,products.code as product_code,purchase_request_detail.quantity as product_qty')
                        ->where('purchase_request_detail.request_id', $id)
                        ->get()->result();

        //print_r($data);die;

        $this->load->view('admin/inventory/po_invoice', $data);
    }

    /**
     * render request order form.
     * 
     * @access public
     * @return void
     */
    public function request_order_form($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('purchase_request', ['id' => $id]);
            if (($data->edit->fh_approval != 0) || ($data->edit->cmd_approval != 0) || ($data->edit->ceo_approval != 0)) {
                $this->session->set_flashdata('alert_danger', 'Request can not be edit now.');
                redirect('inventory/request_order_list');
            }
            $data->edit_details = $this->inventory_model->get_request_detail($id);
        }

        $data->warehouse_list = $this->comman_model->getAll('warehouses', ['status' => 1]);
        $data->supplier_list = $this->comman_model->getAll('supplier', ['status' => 1]);
        $data->product_list = $this->comman_model->getAll('products', ['status' => 1]);

        $purchase_request = $this->comman_model->getBiggestId('purchase_request', []);
        $biggest_id = ($purchase_request) ? $purchase_request[0]->id : 0;
        $data->reference_no = "JC-GEN-PO-000" . ($biggest_id + 1);
        //print_r($data->edit_details);die;

        $this->load->view('admin/inventory/request_order_form', $data);
    }

    /**
     * insert/update request order.
     * 
     * @access public
     * @return void
     */
    public function post_request_order_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('date', 'date', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'reference_no', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
        $this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');
        //$this->form_validation->set_rules('request_status', 'status ', 'trim|required');
        $this->form_validation->set_rules('description', 'Note ', 'trim');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->request_order_form();
        } else {

            /* set variables from the form */
            $insertData = array(
                'date' => $this->input->post('date'),
                'reference_no' => $this->input->post('reference_no'),
                'supplier_id' => $this->input->post('supplier_id'),
                //'purchase_status' => $this->input->post('request_status'), 
                'warehouse_id' => $this->input->post('warehouse_id'),
                'description' => $this->input->post('description'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('purchase_request', ['id' => $edit_id], $insertData)) {
                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $tax_prct = $this->input->post('tax');
                    $tax_amt = $this->input->post('taxamount');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    $this->comman_model->delete(['request_id' => $edit_id], 'purchase_request_detail');
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'request_id' => $edit_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'tax_prct' => $tax_prct[$i],
                            'tax_amt' => $tax_amt[$i],
                            'amount' => $amount[$i],
                        );

                        $this->comman_model->save('purchase_request_detail', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Request order updated successfully.');

                    redirect('inventory/request_order_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->db->where('reference_no', $this->input->post('reference_no'))->get('purchase_request')->num_rows() > 0) {

                    $this->session->set_flashdata('alert_danger', 'This refrence number is already exists.');
                    redirect('inventory/request_order_form');
                }

                $insert_id = $this->comman_model->save('purchase_request', $insertData);
                if ($insert_id) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $tax_prct = $this->input->post('tax');
                    $tax_amt = $this->input->post('taxamount');
                    $i = 0;
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'request_id' => $insert_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'tax_prct' => $tax_prct[$i],
                            'tax_amt' => $tax_amt[$i],
                            'amount' => $amount[$i],
                        );

                        $this->comman_model->save('purchase_request_detail', $requestDetail);
                    }

                    /* START - sending notification */
                    $this->load->model('notification_model');

                    $product = $this->db->from('purchase_request')
                                    ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                                    ->join('products', 'products.id = purchase_request_detail.product_id')
                                    ->select('purchase_request.reference_no,products.name as product_name,products.code as product_code')
                                    ->where('purchase_request.id', $insert_id)
                                    ->get()->row();

                    /* load a template */
                    $tmpdata = array(
                        'NTF_PRODUCT_NAME' => $product->product_name,
                        'NTF_PRODUCT_CODE' => $product->product_code,
                        'NTF_PO_NUMBER' => $product->reference_no,
                    );

                    $templateData = get_notification(5, $tmpdata);

                    /* sending notification to admin */
                    $this->notification_model->add(array(
                        'sender_id' => 0,
                        'type' => 'admin_broadcast',
                        'label' => 'purchase_order_placed',
                        'title' => $templateData['title'],
                        'body' => $templateData['body'],
                        'href' => '',
                        'recipient_id' => 0,
                    ));

                    // END - sending notification
                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Request order added successfully.');
                    $this->session->keep_flashdata('alert_success');
                    redirect('inventory/request_order_list', 'refresh');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating request order. Please try again.');

                redirect('inventory/add_warehouse');
            }
        }
    }

    /**
     * render ware house list .
     * 
     * @access public
     * @return void
     */
    public function warehouse_list() {

        /* create the data object */
        $data = new stdClass();
        $data->warehouse_list = $this->comman_model->getAll('warehouses', []);
        $this->load->view('admin/inventory/warehouse_list', $data);
    }

    /**
     * render add warehouse form .
     * 
     * @access public
     * @return void
     */
    public function add_warehouse($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('warehouses', ['id' => $id]);
        }
        $this->load->model('Jcventure_model', 'jcventure');
        $data->state_list = $this->jcventure->getState();
        $this->load->view('admin/inventory/add_warehouse', $data);
    }

    /**
     * insert/update warehouse info.
     * 
     * @access public
     * @return void
     */
    public function post_warehouse_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('code', 'warehouse code', 'trim|required');
        $this->form_validation->set_rules('contact_person', 'contact person', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|valid_email');
        $this->form_validation->set_rules('contact1', 'mobile number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('contact2', 'mobile number ', 'regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        //$this->form_validation->set_rules('country_id', 'Country', 'trim|required');
        $this->form_validation->set_rules('state_id', 'State', 'trim|required');
        $this->form_validation->set_rules('district_id', 'district', 'trim|required');
        $this->form_validation->set_rules('address', 'address ', 'min_length[15]');
        $this->form_validation->set_rules('pincode', 'pincode ', 'required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            if (empty($edit_id)) {
                $this->add_warehouse();
            } else {
                redirect('inventory/add_warehouse/' . $edit_id);
            }
        } else {

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $billingData = array(
                'title' => $this->input->post('title'),
                'code' => $this->input->post('code'),
                'contact_person' => $this->input->post('contact_person'),
                'email' => $this->input->post('email'),
                'contact1' => $this->input->post('contact1'),
                'contact2' => $this->input->post('contact2'),
                //'country_id' => $this->input->post('country_id'), 
                'state_id' => $this->input->post('state_id'),
                'district_id' => $this->input->post('district_id'),
                'address' => $this->input->post('address'),
                'pincode' => $this->input->post('pincode'),
                'description' => $this->input->post('description'),
                'created_by' => $this->session->user_id,
                'status' => $status,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('warehouses', ['id' => $edit_id], $billingData)) {

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Warehouse updated successfully.');
                    redirect('inventory/warehouse_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->comman_model->save('warehouses', $billingData)) {

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Warehouse added successfully.');
                    redirect('inventory/warehouse_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating warehouse. Please try again.');

                redirect('inventory/add_warehouse');
            }
        }
    }

    /**
     * render supplier list .
     * 
     * @access public
     * @return void
     */
    public function supplier_list() {

        /* create the data object */
        $data = new stdClass();
        $data->supplier_list = $this->comman_model->getAll('supplier', []);
        $this->load->view('admin/inventory/supplier_list', $data);
    }

    /**
     * render add supplier form .
     * 
     * @access public
     * @return void
     */
    public function add_supplier($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('supplier', ['id' => $id]);
        }

        $this->load->model('Jcventure_model', 'jcventure');
        $data->state_list = $this->jcventure->getState();
        $this->load->view('admin/inventory/add_supplier', $data);
    }

    /**
     * insert/update supplier info.
     * 
     * @access public
     * @return void
     */
    public function post_supplier_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('name', 'supplier name', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('contact1', 'mobile number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('contact2', 'mobile number ', 'regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        //$this->form_validation->set_rules('country_id', 'Country', 'trim|required');
        $this->form_validation->set_rules('state_id', 'State', 'trim|required');
        $this->form_validation->set_rules('district_id', 'District', 'trim|required');
        $this->form_validation->set_rules('address', 'address ', 'trim|required|min_length[15]');
        $this->form_validation->set_rules('pincode', 'pincode ', 'required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->add_supplier();
        } else {

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            /* set variables from the form */
            $insertData = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'contact1' => $this->input->post('contact1'),
                'contact2' => $this->input->post('contact2'),
                //'country_id' => $this->input->post('country_id'), 
                'state_id' => $this->input->post('state_id'),
                'district_id' => $this->input->post('district_id'),
                'address' => $this->input->post('address'),
                'pincode' => $this->input->post('pincode'),
                'description' => $this->input->post('description'),
                'created_by' => $this->session->user_id,
                'status' => $status,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('supplier', ['id' => $edit_id], $insertData)) {

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Supplier updated successfully.');
                    redirect('inventory/supplier_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->comman_model->save('supplier', $insertData)) {

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Supplier added successfully.');
                    redirect('inventory/supplier_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating supplier. Please try again.');

                redirect('inventory/add_supplier');
            }
        }
    }

    /**
     * render product list .
     * 
     * @access public
     * @return void
     */
    public function product_list() {

        /* create the data object */
        $data = new stdClass();
        $data->product_list = $this->comman_model->getAll('products', []);
        $this->load->view('admin/inventory/products_list', $data);
    }

    /**
     * render add product form .
     * 
     * @access public
     * @return void
     */
    public function add_product($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('products', ['id' => $id]);
        }

        $data->state_list = $this->comman_model->getAll('states', ['country_id' => 101]);
        $this->load->view('admin/inventory/add_product', $data);
    }

    /**
     * insert/update product info.
     * 
     * @access public
     * @return void
     */
    public function post_product_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('name', 'product name', 'trim|required');
        $this->form_validation->set_rules('code', 'product code', 'trim|required');
        $this->form_validation->set_rules('unit', 'unit', 'trim|required');
        //$this->form_validation->set_rules('default_sale_unit', 'default sale unit', 'trim|required');
        //$this->form_validation->set_rules('default_purchase_unit', 'default purchase unit', 'trim|required');
        $this->form_validation->set_rules('purchase_price', 'purchase price', 'trim|required');
        $this->form_validation->set_rules('tax', 'tax', 'trim|required');
        $this->form_validation->set_rules('selling_price', 'selling price', 'trim|required');
        $this->form_validation->set_rules('alert_quantity', 'alert quantity', 'trim|required');
        /* $this->form_validation->set_rules('opening_stock', 'opening stock', 'trim|required');
          $this->form_validation->set_rules('opening_stock_value', 'opening stock value', 'trim|required'); */


        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->add_product();
        } else {

            $status = $this->input->post('status');
            $status = (empty($status)) ? 0 : 1;

            if (!empty($_FILES['product_image']['name'])) {

                if ($this->image_upload()) {
                    $product_image = $this->upload_data['product_image']['file_name'];
                    $oldimgpath = FCPATH . "uploads/inventory/product/" . $this->input->post('oldimg');
                    if (file_exists($oldimgpath) && (!empty($this->input->post('oldimg')))) {
                        unlink($oldimgpath);
                        $this->session->set_userdata('image', $product_image);
                    }
                } else {
                    return false;
                    $product_image = $this->input->post('oldimg');
                }
            } else {

                $product_image = $this->input->post('oldimg');
            }

            /* set variables from the form */
            $insertData = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'unit' => $this->input->post('unit'),
                'weight' => $this->input->post('weight'),
                //'default_sale_unit' => $this->input->post('default_sale_unit'), 
                //'default_purchase_unit' => $this->input->post('default_purchase_unit'), 
                'purchase_price' => $this->input->post('purchase_price'),
                'tax' => $this->input->post('tax'),
                'selling_price' => $this->input->post('selling_price'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                //'opening_stock' => $this->input->post('opening_stock'), 
                //'opening_stock_value' => $this->input->post('opening_stock_value'), 
                'description' => $this->input->post('description'),
                'image' => $product_image,
                'created_by' => $this->session->user_id,
                'status' => $status,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('products', ['id' => $edit_id], $insertData)) {

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Product updated successfully.');
                    redirect('inventory/product_list');
                } else {
                    $failed = true;
                }
            } else {

                if ($this->comman_model->save('products', $insertData)) {

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Product added successfully.');
                    redirect('inventory/product_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating product. Please try again.');

                redirect('inventory/add_product');
            }
        }
    }

    /**
     * get product by id .
     * 
     * @access public
     * @return void
     */
    public function get_product_by_id() {

        $id = $this->input->post('id');
        /* if edit  */
        if (($id != null) && ($id > 0)) {

            $data = $this->comman_model->getRow('products', ['id' => $id]);

            $arr = array('status' => true, 'data' => $data);
        } else {
            $arr = array('status' => false, 'msg' => 'No product found !');
        }

        echo json_encode($arr);
        die;
    }

    /**
     * get warehouse by id .
     * 
     * @access public
     * @return void
     */
    public function get_warehouse_by_id() {

        $id = $this->input->post('id');
        /* if edit  */
        if (($id != null) && ($id > 0)) {

            $data = $this->comman_model->getRow('warehouses', ['id' => $id]);

            $arr = array('status' => true, 'data' => $data);
        } else {
            $arr = array('status' => false, 'msg' => 'No record found !');
        }

        echo json_encode($arr);
        die;
    }

    /**
     * render manage stock.
     * 
     * @access public
     * @return void
     */
    public function stock_list() {

        /* create the data object */
        $data = new stdClass();

        //$data->stock_list  = $this->comman_model->getAll('stock_purchase', []);
        $data->stock_list = $this->db->from('stock_purchase')
                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                        ->join('supplier', 'supplier.id = stock_purchase.supplier_id')
                        ->join('warehouses', 'warehouses.id = stock_purchase.warehouse_id')
                        ->select('stock_purchase.*,supplier.name as sp_name,warehouses.title as w_name,stock_purchase_details.actual_qty')
                        ->get()->result();

        //print_r($data->stock_list);die;
        $this->load->view('admin/inventory/manage_stock', $data);
    }

    /**
     * render stock in form.
     * 
     * @access public
     * @return void
     */
    public function get_stock_in_form($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('purchase_request', ['id' => $id]);
            $data->edit_details = $this->inventory_model->get_request_detail($id);
        }

        $data->warehouse_list = $this->comman_model->getAll('warehouses', []);
        $data->supplier_list = $this->comman_model->getAll('supplier', []);
        $data->product_list = $this->comman_model->getAll('products', []);

        $stock_purchase = $this->comman_model->getBiggestId('stock_purchase', []);
        $biggest_id = ($stock_purchase) ? $stock_purchase[0]->id : 0;
        $data->reference_no = "STCK000" . ($biggest_id + 1);
        //print_r($data->reference_no);die;
        $this->load->view('admin/inventory/stock_in', $data);
    }

    /**
     * render stock in form.
     * 
     * @access public
     * @return void
     */
    public function stock_in($po_id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($po_id != null) && ($po_id > 0)) {

            $po = $this->db->where('id', $po_id)->get('purchase_request')->row();
            if ($po) {
                if (check_stockin_exists($po->reference_no)) {
                    $this->session->set_flashdata('alert_danger', 'Stock already submitted.');
                    redirect('inventory/request_order_list');
                }
            }

            $data->po_request = $this->db->from('purchase_request')
                            ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                            ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                            ->select('purchase_request.*,warehouses.title as w_name,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,warehouses.district_id,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                            ->where('purchase_request.id', $po_id)
                            ->get()->row();

            $data->po_request_details = $this->db->from('purchase_request_detail')
                            ->join('products', 'products.id = purchase_request_detail.product_id')
                            ->select('purchase_request_detail.*, products.name as product_name,products.code as product_code,purchase_request_detail.quantity as product_qty')
                            ->where('purchase_request_detail.request_id', $po_id)
                            ->get()->result();
        }

        $data->warehouse_list = $this->comman_model->getAll('warehouses', []);
        $data->supplier_list = $this->comman_model->getAll('supplier', []);
        $data->product_list = $this->comman_model->getAll('products', []);

        $stock_purchase = $this->comman_model->getBiggestId('stock_purchase', []);
        $biggest_id = ($stock_purchase) ? $stock_purchase[0]->id : 0;
        $data->reference_no = "STCK000" . ($biggest_id + 1);
        //print_r($data);die;
        $this->load->view('admin/inventory/po_stock_in', $data);
    }

    /**
     * render stock in form.
     * 
     * @access public
     * @return void
     */
    public function get_stock_in_detail($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {

            $data->po_request = $this->db->from('stock_purchase')
                            ->join('warehouses', 'warehouses.id = stock_purchase.warehouse_id')
                            ->join('supplier', 'supplier.id = stock_purchase.supplier_id')
                            ->select('stock_purchase.*,warehouses.title as w_name,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,warehouses.district_id,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                            ->where('stock_purchase.id', $id)
                            ->get()->row();

            $data->po_request_details = $this->db->from('stock_purchase_details')
                            ->join('products', 'products.id = stock_purchase_details.product_id')
                            ->select('stock_purchase_details.*, products.name as product_name,products.code as product_code,stock_purchase_details.quantity as product_qty')
                            ->where('stock_purchase_details.purchase_id', $id)
                            ->get()->result();
        }

        $data->warehouse_list = $this->comman_model->getAll('warehouses', []);
        $data->supplier_list = $this->comman_model->getAll('supplier', []);
        $data->product_list = $this->comman_model->getAll('products', []);

        $stock_purchase = $this->comman_model->getBiggestId('stock_purchase', []);
        $biggest_id = ($stock_purchase) ? $stock_purchase[0]->id : 0;
        $data->reference_no = "STCK000" . ($biggest_id + 1);
        //print_r($data);die;
        $this->load->view('admin/inventory/po_stock_in_details', $data);
    }

    /**
     * insert stock .
     * 
     * @access public
     * @return void
     */
    public function post_stock_in() {
        //echo "<pre>";
        //print_r($_POST);die;
        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');
        /* set validation rules */

        $this->form_validation->set_rules('reference_no', 'reference no', 'trim|required');
        $this->form_validation->set_rules('po_no', 'purchase order no', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
        $this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');
        $this->form_validation->set_rules('description', 'Note ', 'trim');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->stock_in();
        } else {

            /* set variables from the form */
            $insertData = array(
                'purchase_date' => date('D, d M Y'),
                'reference_no' => $this->input->post('reference_no'),
                'request_id' => $this->input->post('request_id'),
                'po_no' => $this->input->post('po_no'),
                'supplier_id' => $this->input->post('supplier_id'),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'description' => $this->input->post('description'),
                'sub_total' => $this->input->post('sub_total'),
                //'tax_percentage' => $this->input->post('tax_prct'), 
                //'tax_amount' => $this->input->post('tax_amt'), 
                //'shipping_charge' => $this->input->post('shipping_charge'), 
                'grand_total' => $this->input->post('grand_total'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('stock_purchase', ['id' => $edit_id], $insertData)) {

                    $products = $this->input->post('product');
                    $request_id = $this->input->post('request_id');
                    $requested_qty = $this->input->post('requested_qty');
                    $received_qty = $this->input->post('received_qty');
                    $damaged_qty = $this->input->post('damaged_qty');
                    $missed_qty = $this->input->post('missed_qty');
                    $actual_qty = $this->input->post('actual_qty');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('current_amt');
                    $i = 0;
                    $this->comman_model->delete(['request_id' => $edit_id], 'stock_purchase_details');
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'purchase_id' => $edit_id,
                            'product_id' => $product,
                            'requested_qty' => $requested_qty[$i],
                            'requested_qty' => $requested_qty[$i],
                            'received_qty' => $received_qty[$i],
                            'damaged_qty' => $damaged_qty[$i],
                            'missed_qty' => $missed_qty[$i],
                            'actual_qty' => $actual_qty[$i],
                            'rate' => $rate[$i],
                            'total_amount' => $amount[$i],
                        );

                        $this->comman_model->save('stock_purchase_details', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Stock updated successfully.');
                    redirect('inventory/stock_list');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('stock_purchase', $insertData);
                if ($insert_id) {

                    $products = $this->input->post('product_id');
                    $request_id = $this->input->post('request_id');
                    $requested_qty = $this->input->post('requested_qty');
                    $received_qty = $this->input->post('received_qty');
                    $damaged_qty = $this->input->post('damaged_qty');
                    $missed_qty = $this->input->post('missed_qty');
                    $actual_qty = $this->input->post('actual_qty');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('current_amt');
                    $i = 0;
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'purchase_id' => $insert_id,
                            'product_id' => $product,
                            'requested_qty' => $requested_qty[$i],
                            'requested_qty' => $requested_qty[$i],
                            'received_qty' => $received_qty[$i],
                            'damaged_qty' => $damaged_qty[$i],
                            'missed_qty' => $missed_qty[$i],
                            'actual_qty' => $actual_qty[$i],
                            'rate' => $rate[$i],
                            'total_amount' => $amount[$i],
                        );

                        $this->comman_model->save('stock_purchase_details', $requestDetail);
                        $this->comman_model->update('purchase_request', ['id' => $request_id], ['purchase_status' => '2']);

                        /* START - sending notification */
                        $this->load->model('notification_model');

                        $product = $this->db->from('stock_purchase')
                                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                                        ->join('products', 'products.id = stock_purchase_details.product_id')
                                        ->join('warehouses', 'warehouses.id = stock_purchase.warehouse_id')
                                        ->select('stock_purchase.po_no,products.name as product_name,products.code as product_code,warehouses.title as warehouse_name,stock_purchase_details.requested_qty,stock_purchase_details.received_qty,stock_purchase_details.damaged_qty,stock_purchase_details.missed_qty,stock_purchase_details.actual_qty')
                                        ->where('stock_purchase.id', $insert_id)
                                        ->get()->row();

                        /* load a template */
                        $tmpdata = array(
                            'NTF_PO_NUMBER' => $product->po_no,
                            'NTF_WAREHOUSE' => $product->warehouse_name,
                            'NTF_REQUEST_QTY' => $product->requested_qty,
                            'NTF_RECEIVE_QTY' => $product->received_qty,
                            'NTF_DAMAGE_QTY' => $product->damaged_qty,
                            'NTF_MISS_QTY' => $product->missed_qty,
                            'NTF_ACTUAL_QTY' => $product->actual_qty,
                        );

                        $templateData = get_notification(6, $tmpdata);

                        /* sending notification to admin */
                        $this->notification_model->add(array(
                            'sender_id' => 0,
                            'type' => 'admin_broadcast',
                            'label' => 'purchase_order_received',
                            'title' => $templateData['title'],
                            'body' => $templateData['body'],
                            'href' => '',
                            'recipient_id' => 0,
                        ));

                        // END - sending notification
                    }

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Stock added successfully.');
                    redirect('inventory/stock_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating stock . Please try again.');

                redirect("inventory/stock_in/$edit_id");
            }
        }
    }

    /**
     * insert/update purchase order.
     * 
     * @access public
     * @return void
     */
    public function post_stock_purchase_form() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('purchase_date', 'date', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'reference no', 'trim|required');
        $this->form_validation->set_rules('po_no', 'purchase order no', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
        $this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');
        $this->form_validation->set_rules('description', 'Note ', 'trim');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->get_stock_in_form();
        } else {

            /* set variables from the form */
            $insertData = array(
                'purchase_date' => $this->input->post('purchase_date'),
                'reference_no' => $this->input->post('reference_no'),
                'po_no' => $this->input->post('po_no'),
                'supplier_id' => $this->input->post('supplier_id'),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'description' => $this->input->post('description'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('stock_purchase', ['id' => $edit_id], $insertData)) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    $this->comman_model->delete(['request_id' => $edit_id], 'stock_purchase_details');
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'purchase_id' => $edit_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'current_quantity' => $quantity[$i],
                            'current_rate' => $rate[$i],
                            'total_amount' => $amount[$i],
                        );

                        $this->comman_model->save('stock_purchase_details', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Stock updated successfully.');
                    redirect('inventory/stock_list');
                } else {
                    $failed = true;
                }
            } else {
                $po_no = $this->input->post('po_no');
                $chk_exist = $this->db->where('po_no', $po_no)->get('stock_purchase')->num_rows();

                if ($chk_exist > 0) {
                    $this->session->set_flashdata('alert_danger', " purchase order number - $po_no is already in stock-in record. Please check input values.");

                    redirect('inventory/get_stock_in_form');
                }


                $insert_id = $this->comman_model->save('stock_purchase', $insertData);
                if ($insert_id) {

                    $products = $this->input->post('product');
                    $quantity = $this->input->post('quantity');
                    $rate = $this->input->post('rate');
                    $amount = $this->input->post('amount');
                    $i = 0;
                    foreach ($products as $product) {
                        $requestDetail = array(
                            'purchase_id' => $insert_id,
                            'product_id' => $product,
                            'quantity' => $quantity[$i],
                            'rate' => $rate[$i],
                            'current_quantity' => $quantity[$i],
                            'current_rate' => $rate[$i],
                            'total_amount' => $amount[$i],
                        );

                        $this->comman_model->save('stock_purchase_details', $requestDetail);
                    }

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Stock added successfully.');
                    redirect('inventory/stock_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating stock . Please try again.');

                redirect('inventory/get_stock_in_form');
            }
        }
    }

    /**
     * delete row on edit po request .
     * 
     * @access public
     * @return void
     */
    public function delete_request_detail() {

        $id = $this->input->post('id');
        if (!empty($id)) {
            $result = $this->db->where('id', $id)->delete('purchase_request_detail');

            if ($result) {
                echo 'true';
                die;
            } else {
                echo 'false';
                die;
            }
        }

        //$data->stock_list  = $this->comman_model->getAll('stock_purchase', []);
        $data->stock_list = $this->db->from('damage_stock')
                        ->join('supplier', 'supplier.id = damage_stock.supplier_id')
                        ->join('warehouses', 'warehouses.id = damage_stock.warehouse_id')
                        ->select('damage_stock.*,supplier.name as sp_name,warehouses.title as w_name')
                        ->get()->result();

        //print_r($data->stock_list);die;
        $this->load->view('admin/inventory/damage_stock_list', $data);
    }

    /**
     * render damage stock list .
     * 
     * @access public
     * @return void
     */
    public function damage_stock_list() {

        /* create the data object */
        $data = new stdClass();

        //$data->stock_list  = $this->comman_model->getAll('stock_purchase', []);
        $data->stock_list = $this->db->from('damage_stock')
                        ->join('supplier', 'supplier.id = damage_stock.supplier_id')
                        ->join('warehouses', 'warehouses.id = damage_stock.warehouse_id')
                        ->select('damage_stock.*,supplier.name as sp_name,warehouses.title as w_name')
                        ->order_by('damage_stock.created_at', 'desc')
                        ->get()->result();

        //print_r($data->stock_list);die;
        $this->load->view('admin/inventory/damage_stock_list', $data);
    }

    /**
     * render damage stock details ajax .
     * 
     * @access public
     * @return void
     */
    public function get_damage_stock_detail() {

        $id = $this->input->post('id');

        /* create the data object */
        $data = new stdClass();

        //$data->stock_list  = $this->comman_model->getAll('stock_purchase', []);
        $data = $this->db->from('damage_stock_details')
                        ->where('damage_stock_details.stock_id', $id)
                        ->get()->result();

        //print_r($data->stock_list);die;
        echo json_encode($data);
        die;
    }

    /**
     * render dispatched kit details ajax .
     * 
     * @access public
     * @return void
     */
    public function get_kits_by_order_id() {

        $id = $this->input->post('id');

        /* create the data object */
        $data = new stdClass();

        $data = $this->db->from('stock_out')
                        ->where('stock_out.order_id', $id)
                        ->get()->result();

        //print_r($data->stock_list);die;
        echo json_encode($data);
        die;
    }

    /**
     * render damage stock entry form.
     * 
     * @access public
     * @return void
     */
    public function damage_stock($id = null) {

        /* create the data object */
        $data = new stdClass();

        /* if edit  */
        if (($id != null) && ($id > 0)) {
            $data->edit = $this->comman_model->getRow('purchase_request', ['id' => $id]);
            $data->edit_details = $this->db->from('stock_purchase')
                            ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                            ->select('stock_purchase_details.damaged_qty')
                            ->where('stock_purchase.id', $id)->get()->row();
        }

        $data->warehouse_list = $this->comman_model->getAll('warehouses', []);
        $data->supplier_list = $this->comman_model->getAll('supplier', []);
        $data->product_list = $this->comman_model->getAll('products', []);

        $stock_purchase = $this->comman_model->getBiggestId('stock_purchase', []);
        $biggest_id = ($stock_purchase) ? $stock_purchase[0]->id : 0;
        $data->reference_no = "STCK000" . ($biggest_id + 1);
        //print_r($data->reference_no);die;
        $this->load->view('admin/inventory/damage_stock', $data);
    }

    /**
     * render stock out detail invoice .
     * 
     * @access public
     * @return void
     */
    public function get_stock_out_invoice($id) {
        /* create the data object */
        $data = new stdClass();
        //$data->request_list  = $this->comman_model->getAll('purchase_request', []);
        $data->po_request = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->select('purchase_request.*,warehouses.title as w_name,warehouses.code as w_code,warehouses.email as w_email,warehouses.contact1 as w_contact1,warehouses.contact2 as w_contact2,warehouses.contact2 as w_pincode,warehouses.address as w_address,warehouses.district_id,supplier.name as s_name,supplier.email as s_email,supplier.contact1 as s_contact1,supplier.contact2 as s_contact2,supplier.address as s_address')
                        ->where('purchase_request.id', $id)
                        ->get()->row();

        $data->po_request_details = $this->db->from('purchase_request_detail')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request_detail.*, products.name as product_name,products.code as product_code,purchase_request_detail.quantity as product_qty')
                        ->where('purchase_request_detail.request_id', $id)
                        ->get()->result();

        //print_r($data);die;

        $this->load->view('admin/inventory/stock_out_details', $data);
    }

    public function pickup_request($id = null) {
        $data = new stdClass();
        if (!empty($id) && ($id > 0)) {
            
        }
        $data->request_list = $this->db
                        ->from('pickup_request')
                        ->join('user', 'user.id = pickup_request.user_id')
                        ->join('billing_address', 'billing_address.user_id = pickup_request.user_id')
                        ->select('pickup_request.status,pickup_request.pickup_date,pickup_request.pickup_time,pickup_request.id,pickup_request.quantity,pickup_request.id,pickup_request.order_number,pickup_request.barcode,pickup_request.created_at,billing_address.address,billing_address.email,billing_address.mobile1,user.firstname,user.lastname')
                        ->where('pickup_request.status', 1)->get()->result();
        //print_r($data);die;
        $this->load->view('admin/inventory/pickup_request', $data);
    }

    /**
     * insert/update damage stock.
     * 
     * @access public
     * @return void
     */
    public function post_damage_stock() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('purchase_date', 'date', 'trim|required');
        $this->form_validation->set_rules('po_no', 'purchase order no', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'supplier ', 'trim|required');
        $this->form_validation->set_rules('warehouse_id', 'warehouse ', 'trim|required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->damage_stock();
        } else {

            /* set variables from the form */
            $insertData = array(
                'po_no' => $this->input->post('po_no'),
                'supplier_id' => $this->input->post('supplier_id'),
                'warehouse_id' => $this->input->post('warehouse_id'),
                'created_by' => $this->session->user_id,
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('damage_stock', ['id' => $edit_id], $insertData)) {

                    $barcodes = $this->input->post('barcode');
                    $reasons = $this->input->post('reason');
                    $i = 0;
                    $this->comman_model->delete(['stock_id' => $edit_id], 'damage_stock_details');
                    foreach ($barcodes as $code) {
                        $requestDetail = array(
                            'stock_id' => $edit_id,
                            'code' => $code,
                            'reason' => $reasons[$i]
                        );

                        $this->comman_model->save('damage_stock_details', $requestDetail);
                    }

                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Stock updated successfully.');
                    redirect('inventory/damage_stock_list');
                } else {
                    $failed = true;
                }
            } else {
                $insert_id = $this->comman_model->save('damage_stock', $insertData);
                if ($insert_id) {

                    $barcodes = $this->input->post('barcode');
                    $reasons = $this->input->post('reason');
                    $i = 0;
                    foreach ($barcodes as $code) {
                        $requestDetail = array(
                            'stock_id' => $insert_id,
                            'code' => $code,
                            'reason' => $reasons[$i]
                        );

                        $this->comman_model->save('damage_stock_details', $requestDetail);
                    }

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Stock added successfully.');
                    redirect('inventory/damage_stock_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating stock . Please try again.');

                redirect('inventory/damage_stock');
            }
        }
    }

    /**
     * user's profile image upload function .
     * 
     * @access public
     * @return void
     */
    function image_upload() {
        $upload_dir = 'uploads/inventory/product';
        if (!is_dir($upload_dir)) {
            mkdir($upload_dir);
        }

        $config['upload_path'] = $upload_dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = 'productimage_' . substr(md5(rand()), 0, 7);
        $config['overwrite'] = false;
        $config['max_size'] = '5120';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('product_image')) {

            /* image upload errors */
            $this->session->set_flashdata('error', $this->upload->display_errors());

            $this->add_product();
            return false;
        } else {
            $this->upload_data['product_image'] = $this->upload->data();
            return true;
        }
    }

    /**
     * user's notification .
     * 
     * @access public
     * @return void
     */
    public function notification() {

        /* create the data object */
        $data = new stdClass();

        $user_id = $this->session->user_id;

        $this->load->model('notification_model');
        $data->notification_list = $this->notification_model->customer_notification_list($user_id);
        //print_r($data->notification_list);die;
        $data->page = 'user/notification';
        $this->load->view('include/user_layout', compact('data'));
    }

    public function upload_kitgroup() {
        $data = new stdClass();
        $this->load->model('Inventory_model');
        $data->kitGroup = $this->Inventory_model->getUploadKitGroup();
        $this->load->view('admin/inventory/manage_upload_kit_group', $data);
    }

    // View dispatched details of order sent form xcode
    public function dispatched_detail($id = null) {
        $data = new stdClass();

        if ($id) {
            $data->detail = $this->db->where('request_id', $id)->get('xcode_dispatch')->row();
            $data->barcode_list = $this->db->where('request_id', $id)->get('xcode_inventory')->result();
        }
        //print_r($data);die;
        $this->load->view('admin/inventory/xcode_dispatched_detail', $data);
    }

    public function kitlist() {
        
    }

    function upload_kitlist() {
        $is_used = empty($this->input->get('st')) ? 0 : $this->input->get('st');
        $data = new stdClass();

        $data->kitGroup = $this->db->where('is_used', $is_used)->get('kit_upload')->result();
        $this->load->view('admin/inventory/kitlist_group_by', $data);
    }

    public function upload_kit($id = null) {

        if (!empty($this->input->post('saveKitUpload'))) {

            $qrcode = $this->input->post('qrcode');
            //$prepared_by = $this->input->post('prepared_by');
            $barcode = $this->input->post('barcode');
            $this->load->model('Inventory_model');
            uploadKitAndGroup($qrcode, $barcode);
        }

        //$data['podetail'] = $this->db->where('request_id',$id)->get('stock_purchase')->row();

        $data['podetail'] = $this->db->from('stock_purchase')
                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                        ->join('supplier', 'supplier.id = stock_purchase.supplier_id')
                        ->join('warehouses', 'warehouses.id = stock_purchase.warehouse_id')
                        ->select('stock_purchase.*,stock_purchase_details.actual_qty')
                        ->where('stock_purchase.request_id', $id)
                        ->get()->row();

        $data['kit_list'] = $this->db->where('request_id', $id)->get('kit_upload')->result();

        //print_r($data);die;
        $this->load->view('admin/inventory/upload_kits', $data);
    }

    /**
     * insert barcode to database.
     * 
     * @access public
     * @return void
     */
    public function post_scan_barcode() {
        if ($this->input->post()) {
            $barcode = trim($this->input->post('barcode'));
            $reqid = trim($this->input->post('reqid'));
            $refno = trim($this->input->post('refno'));
            if (!empty($barcode)) {
                $result = $this->db->where('barcode', $barcode)->get('kit_upload')->num_rows();

                if ($result > 0) {
                    echo json_encode(array('status' => false, 'msg' => "Barcode - $barcode already exist !"));
                    die;
                } else {

                    $check = $this->db->where('barcode', $barcode)->where('request_id', $reqid)->get('xcode_inventory')->num_rows();

                    if ($check == 0) {
                        echo json_encode(array('status' => false, 'msg' => "Barcode - $barcode is exceed !"));
                        die;
                    } else {

                        $insertData = array(
                            'request_id' => $reqid,
                            'barcode' => $barcode,
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $insert_id = $this->comman_model->save('kit_upload', $insertData);
                        if ($insert_id) {
                            echo json_encode(array('status' => true, 'insert_id' => $insert_id));
                            die;
                        } else {
                            echo json_encode(array('status' => false));
                            die;
                        }
                    }
                }
            }
        }
    }

    /**
     * remove barcode from database.
     * 
     * @access public
     * @return void
     */
    public function remove_barcode() {
        if ($this->input->post()) {
            $id = trim($this->input->post('id'));
            if (!empty($id)) {
                $result = $this->db->where('id', $id)->get('kit_upload')->num_rows();

                if ($result > 0) {
                    $this->comman_model->delete(['id' => $id], 'kit_upload');
                    echo json_encode(array('status' => true));
                    die;
                } else {

                    echo json_encode(array('status' => false));
                    die;
                }
            }
        }
    }

    public function uploadKit_excel() {

        $this->load->library('excel');
        $configUpload['upload_path'] = FCPATH . 'uploads/inventory/upload_kit/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000';

        $this->load->library('upload', $configUpload);
        if (!$this->upload->do_upload('userfile')):
            echo $this->upload->display_errors('<p>', '</p>');
        else:
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension = $upload_data['file_ext'];    // uploded file extension
            $objReader = PHPExcel_IOFactory::load(FCPATH . 'uploads/inventory/upload_kit/' . $file_name); // For excel 2007 	  
            $totalrows = $objReader->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
            $objWorksheet = $objReader->setActiveSheetIndex(0);
            $BarCode = array();
            //$BarCode = array();
            for ($i = 2; $i <= $totalrows; $i++) {
                $BarCode[] = $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
                //$BarCode[]= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();						  
            }
            uploadKitAndGroup($BarCode);
        endif;
        unlink('././uploads/inventory/upload_kit/' . $file_name); //File Deleted After uploading in database .			 
        $this->session->set_flashdata('alert_success', 'Kit uploaded successfully.');
        redirect(base_url('inventory/upload_kit'));
    }

    /**
     * render order list .
     * 
     * @access public
     * @return void
     */
    public function order_list() {

        /* create the data object */
        $data = new stdClass();
        $data->order_list = $this->db->from('orders')
                        ->join('user', 'user.id = orders.user_id')
                        //->join('billing_address', 'billing_address.user_id = user.id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode')
                        ->where('orders.order_status', 1)
                        ->order_by('orders.order_date', 'desc')->get()->result();
        //print_r($data->order_list);die;
        $this->load->view('admin/inventory/orders_list', $data);
    }

    /**
     * render dispatch list.
     * 
     * @access public
     * @return void
     */
    public function dispatch_list() {

        /* create the data object */
        $data = new stdClass();

        $data->dispatch_list = $this->db->from('stock_out')
                        ->join('orders', 'orders.id = stock_out.order_id')
                        ->join('user', 'user.id = orders.user_id')
                        //->join('billing_address', 'billing_address.user_id = user.id')
                        ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile1,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode,orders.district_id,stock_out.created_at as dispatch_date,stock_out.id as disid,stock_out.shipping_partner,stock_out.tracking_code')
                        ->order_by('stock_out.created_at', 'desc')
                        ->group_by('stock_out.order_id')
                        ->get()->result();

        //print_r($data->dispatch_list);die;
        $this->load->view('admin/inventory/stock_out_list', $data);
    }

    /**
     * render stock out form.
     * 
     * @access public
     * @return void
     */
    public function stock_out($order_id = null) {

        /* create the data object */
        $data = new stdClass();

        if (($order_id != null) && ($order_id > 0)) {

            $chk = $this->db->where('order_id', $order_id)->get('stock_out')->num_rows();

            if ($chk > 0) {
                $this->session->set_flashdata('alert_danger', "This order No. is already dispatched ! Please check input.");
                redirect("inventory/stock_out");
            }

            $data->edit = $this->db->from('orders')
                            ->join('user', 'user.id = orders.user_id')
                            //->join('billing_address', 'billing_address.user_id = user.id')
                            ->select('orders.*,CONCAT(user.firstname," ",user.lastname) as c_name,user.email,user.contact1,user.contact2,orders.name as bl_name,orders.email as bl_email,orders.mobile1,orders.mobile2,orders.landmark as bl_landmark, orders.locality as bl_locality,orders.pincode,orders.district_id')
                            ->where('orders.id', $order_id)
                            ->get()->row();

            $data->edit_details = $this->db->from('order_detail')
                            ->join('plan', 'plan.id = order_detail.plan_id')
                            ->select('order_detail.*,plan.name as plan_name')
                            ->where('order_detail.order_id', $order_id)
                            ->get()->result();
            //print_r($data->edit_details);die;
        }
        //print_r($data->order_list);die;
        $this->load->view('admin/inventory/stock_out', $data);
    }

    /**
     * render stock out form.
     * 
     * @access public
     * @return void
     */
    public function get_order_by_ordernumber($order_no = null) {

        /* create the data object */
        $data = new stdClass();

        if ($order_no != null) {

            $find = $this->db->where('order_number', $order_no)->or_where('txnid', $order_no)->get('orders')->row();
            if ($find) {
                redirect("inventory/stock_out/$find->id");
            } else {
                $this->session->set_flashdata('alert_danger', 'N record found! Please check input.');
                redirect("inventory/stock_out");
            }
        } else {
            redirect('inventory/stock_out');
        }
    }

    /**
     * update stock / dispatch order.
     * 
     * @access public
     * @return void
     */
    public function post_stock_out() {

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('shipping_partner', 'shipping partner', 'trim|required');
        $this->form_validation->set_rules('order_num', 'order number', 'trim|required');
        $this->form_validation->set_rules('customer_name', 'customer name', 'trim|required');
        $this->form_validation->set_rules('customer_email', 'customer email', 'trim|required|valid_email');
        $this->form_validation->set_rules('customer_contact', 'customer contact number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
        $this->form_validation->set_rules('order_date', 'order date', 'trim|required');

        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('district', 'district', 'trim|required');
        $this->form_validation->set_rules('shipping_address', 'shipping address', 'trim|required');
        $this->form_validation->set_rules('sub_total', 'sub total', 'trim|required');
        $this->form_validation->set_rules('grand_total', 'grand total', 'trim|required');
        $this->form_validation->set_rules('sub_total', 'sub total', 'trim|required');
        $this->form_validation->set_rules('quantity', 'quantity', 'trim|required');
        $this->form_validation->set_rules('edit_id', 'order id ', 'trim|required');

        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            //$this->stock_out();
            redirect("inventory/stock_out/$edit_id");
        } else {

            $qr_codes = $this->input->post('kit');
            $plans = $this->input->post('plan');
            $planid = $this->input->post('planid');
            $plannames = implode(', ', $this->input->post('plan'));

            $prices = $this->input->post('kitamount');
            $shipping_partner = $this->input->post('shipping_partner');
            $status = 0;
            if ($shipping_partner == "manual") {
                $status = 1;
            }
            /* set variables from the form */
            $i = 0;
            foreach ($qr_codes as $code) {

                $insertData = array(
                    'order_id' => $edit_id,
                    'order_number' => $this->input->post('order_num'),
                    'order_date' => $this->input->post('order_date'),
                    'customer_id' => $this->input->post('customer_id'),
                    'plan_id' => trim($planid[$i]),
                    'plan_name' => trim($plans[$i]),
                    'qr_code' => trim($code),
                    'quantity' => 1,
                    'unit_price' => $prices[$i],
                    'tax_amount' => $this->input->post('tax_amount'),
                    'note' => $this->input->post('note'),
                    'shipping_partner' => $this->input->post('shipping_partner'),
                    'shipping_address' => $this->input->post('shipping_address'),
                    'shipping_city' => $this->input->post('city'),
                    'shipping_district' => $this->input->post('district'),
                    'shipping_state' => $this->input->post('state'),
                    'shipping_pincode' => $this->input->post('pincode'),
                    'invoice_amount' => $this->input->post('grand_total'),
                    'customer_name' => $this->input->post('customer_name'),
                    'item_qty' => $this->input->post('quantity'),
                    'customer_email' => $this->input->post('customer_email'),
                    'customer_contact' => $this->input->post('customer_contact'),
                    'created_by' => $this->session->user_id,
                    'status' => $status,
                );

                $insert_id = $this->comman_model->save('stock_out', $insertData);

                $qrcode_map = array(
                    'user_id' => $this->input->post('customer_id'),
                    'plan_id' => $planid[$i],
                    'order_id' => $edit_id,
                    'qrcode' => trim($code),
                );

                $this->comman_model->save('qrcode_mapping', $qrcode_map);

                $i++;

                $this->comman_model->update('kit_upload', ['barcode' => $code], ['is_used' => 1]);
                $this->comman_model->update('orders', ['id' => $edit_id], ['order_status' => 2]);
            }

            if ($insert_id) {

                $shipping_partner = $this->input->post('shipping_partner');

                if ($shipping_partner == "delhivery") {
                    redirect("inventory/delhivery_dispatch/$insert_id");
                } elseif ($shipping_partner == "dtdc") {

                    redirect("inventory/dtdc_dispatch/$insert_id");
                } elseif ($shipping_partner == "manual") {

                    $this->session->set_flashdata('alert_success', 'Stock out successfully.');
                    redirect('inventory/dispatch_list');
                }

                /* START - sending notification */
                /* $this->load->model('notification_model');

                  $order = $this->db->from('stock_out')
                  ->join('orders','orders.id = stock_out.order_id')
                  ->join('user','user.id = orders.user_id')
                  ->select('orders.order_number,user.device_id,user.id as user_id,user.contact1,user.email as user_email,CONCAT(user.firstname," ",user.lastname) as customer_name,stock_out.shipping_address')
                  ->where('stock_out.id',$insert_id)
                  ->get()->row(); */

                /* load a template */
                /* $tmpdata = array(
                  'NTF_ORDER_NUMBER'=> $order->order_number,
                  'NTF_CUSTOMER_NAME'=> $order->customer_name,
                  'NTF_SHIPPING_ADDRESS'=> $order->shipping_address
                  );
                 */
                $templateData = get_notification(7, $tmpdata);

                /* sending notification to admin */
                /* $this->notification_model->add(array(
                  'sender_id' => 0,
                  'type' => 'admin_broadcast',
                  'label' => 'order_dispatched',
                  'title' => $templateData['title'],
                  'body' => $templateData['body'],
                  'href' => '',
                  'recipient_id' => 0,
                  )); */

                // END - sending notification
                // Thankyou email after order	
                /* $email = $order->user_email;
                  $message = $this->load->view('email_template/order_dispatched', '', true);
                  $message = str_replace("{username}",$order->customer_name,$message);
                  $message = str_replace("{planname}",$plannames,$message);
                  $subject="Your $plannames DNA Testing Kit Has Been Dispatched.";
                  $this->comman_model->emailsend($email,$subject,$message);
                 */
                // Send notification mobile app

                /* $msg = array('user_id'=>$order->user_id,
                  'notification'=>'Your kit has been dispatched',
                  'notitify_type'=>'dispatched');

                  $registrationIds = array($order->device_id);
                  $result = send_android_notification($registrationIds, $msg); */

                // Sending sms
                /* $mobile = $order->contact1;

                  if (!empty($mobile) && (strlen($mobile) == 10))
                  {
                  $sms = New Sms();
                  $message = "Dear Customer, your $plannames DNA Testing Kit has been dispatched successfully. You will receive your ordered kit within 10 business days. Track you order at TRACKING_NUMBER or give us a call on +91 731 2441000. \n\nThanks,\nJC Genetics Team";
                  $sms->sendsms($mobile,$message);
                  } */


                /* creation ok */
                $this->session->set_flashdata('alert_success', 'Stock out successfully.');
                redirect('inventory/dispatch_list');
            } else {
                $failed = true;
            }


            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in stock outing. Please try again.');

                redirect("inventory/stock_out/$edit_id");
            }
        }
    }

    /**
     * render purchase approval list .
     * 
     * @access public
     * @return void
     */
    public function approval_list() {

        /* create the data object */
        $data = new stdClass();

        $user_id = $this->session->user_id;
        $data->user = $this->db->where('id', $user_id)->get('user')->row();
        $data->approval_list = $this->db->from('purchase_request')
                        ->join('warehouses', 'warehouses.id = purchase_request.warehouse_id')
                        ->join('supplier', 'supplier.id = purchase_request.supplier_id')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->join('products', 'products.id = purchase_request_detail.product_id')
                        ->select('purchase_request.*,warehouses.title as warehouse_name,supplier.name as supplier_name,products.name as product_name,purchase_request_detail.quantity as product_qty')
                        ->group_by('purchase_request.id')
                        ->order_by('purchase_request.created_at', 'desc')
                        ->get()->result();

        //print_r($data->request_list);die;
        $this->load->view('admin/approval_list', $data);
    }

    /**
     * approve purchase request .
     * 
     * @access public
     * @return void
     */
    public function approve_purchase_request() {

        $request_id = $this->input->post('request_id');

        if (!empty($request_id)) {
            $user_id = $this->session->user_id;
            $user = $this->db->where('id', $user_id)->get('user')->row();
            if ($user->head == 'bh') {
                $this->db->where('id', $request_id)->update('purchase_request', ['bh_approval' => 1]);
            } elseif ($user->head == 'fh') {
                $this->db->where('id', $request_id)->update('purchase_request', ['fh_approval' => 1]);
            } elseif ($user->head == 'ceo') {
                $this->db->where('id', $request_id)->update('purchase_request', ['ceo_approval' => 1]);
            }

            echo json_encode(true);
        }
    }

    /**
     * reject purchase request .
     * 
     * @access public
     * @return void
     */
    public function reject_purchase_request() {

        $request_id = $this->input->post('request_id');
        $reason = $this->input->post('reason');
        $user_id = $this->session->user_id;

        if (!empty($request_id)) {
            $user_id = $this->session->user_id;
            $user = $this->db->where('id', $user_id)->get('user')->row();
            if ($user->head == 'bh') {
                $this->db->where('id', $request_id)->update('purchase_request', ['bh_approval' => 2, 'reject_reason' => $reason, 'rejected_by' => $user_id]);
            } elseif ($user->head == 'fh') {
                $this->db->where('id', $request_id)->update('purchase_request', ['fh_approval' => 2, 'reject_reason' => $reason, 'rejected_by' => $user_id]);
            } elseif ($user->head == 'ceo') {
                $this->db->where('id', $request_id)->update('purchase_request', ['ceo_approval' => 2, 'reject_reason' => $reason, 'rejected_by' => $user_id]);
            }
        }

        $this->session->set_flashdata('alert_success', 'record updated successfully.');

        //print_r($data->request_list);die;
        redirect('inventory/approval_list');
    }

    /**
     * render inventory details report .
     * 
     * @access public
     * @return void
     */
    public function report_inventory_details($id = null) {

        /* create the data object */
        $data = new stdClass();

        $data->product = $this->db->select("CONCAT(name,' [ ',code,' ]') as product")->get('products')->row()->product;
        $data->warehouse = $this->db->select('title')->get('warehouses')->row()->title;
        $data->qty_ordered = $this->db->from('purchase_request')
                        ->join('purchase_request_detail', 'purchase_request_detail.request_id = purchase_request.id')
                        ->select('SUM(quantity) as quantity')->get()->row()->quantity;

        $data->qty_commited = $this->db->from('orders')
                        ->join('order_detail', 'order_detail.order_id = orders.id')
                        ->select('SUM(order_detail.quantity) as quantity')->where('orders.order_status', 1)->get()->row()->quantity;

        $data->qty_in = $this->db->from('stock_purchase')
                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                        ->select('SUM(stock_purchase_details.actual_qty) as quantity')->get()->row()->quantity;

        $data->qty_out = $this->db->from('stock_out')
                        ->get()->num_rows();

        $data->qty_available = $this->db->from('kit_upload')
                        ->where('kit_upload.is_used', 0)->get()->num_rows();

        //print_r($data->qty_in);die;
        $this->load->view('admin/inventory/reports/inventory_details', $data);
    }

    /**
     * render inventory valuation summery report .
     * 
     * @access public
     * @return void
     */
    public function report_inventory_valuation($id = null) {

        /* create the data object */
        $data = new stdClass();

        $data->record_list = $this->db->from('stock_purchase')
                        ->join('stock_purchase_details', 'stock_purchase_details.purchase_id = stock_purchase.id')
                        ->join('products', 'products.id = stock_purchase_details.product_id')
                        ->select("CONCAT(products.name,' [ ',products.code,' ]') as product,SUM(stock_purchase_details.actual_qty) as quantity,SUM(stock_purchase_details.total_amount) as total_amount")->limit(1)->get()->result();

        //print_r($data->record_list);die;
        $this->load->view('admin/inventory/reports/inventory_valuation', $data);
    }

    /**
     * check PO Number is already exist in Stock In table.
     * 
     * @access public
     * @return void
     */
    public function checkPOExists() {

        $po = $this->input->post('po_no');
        $result = $this->comman_model->getRow('stock_purchase', array('po_no' => $po));

        if (!empty($result)) {
            echo 'false';
        } else {
            echo 'true';
        }
        die;
    }

    public function approve_kit_assignment($id = null) {
        if ($id) {
            $req = $this->db->where('id', $id)->get('kit_assignment_request')->row();
            if ($req) {
                $chk = $this->comman_model->update('kit_assignment_request', ['id' => $id], ['inv_approval' => 1]);
                if ($chk) {
                    $this->session->set_flashdata('alert_success', 'Request approved successfully. You can assign kits now.');
                    redirect('inventory/kit_request_list');
                }
            } else {
                $this->session->set_flashdata('alert_danger', 'Invalid record ID !');
                redirect('inventory/kit_request_list');
            }
        }
    }

    /* render kit assign request list */
    public function kit_request_list() {
        $data = new stdClass();
        $data->request_list = $this->comman_model->getAll('kit_assignment_request', ['bh_approval' => 1, 'sm_approval' => 1]);
        $this->load->view('admin/inventory/kit_request_list', $data);
    }

    /* render kit assignment form .*/
    public function kit_assignment($id = null) {

        /* create the data object */
        $data = new stdClass();

        if (($id != null) && ($id > 0)) {

            $result = $this->comman_model->getRow('kit_assignment_request', ['id' => $id]);
         
            if ($result->status == 4) {
                $this->session->set_flashdata('alert_danger', 'This request already processed !');
                redirect('inventory/kit_request_list');
            }
            //if (($result->bh_approval == 1) && ($result->sm_approval == 1) && ($result->inv_approval == 1)) {

                $data->edit = $result;
                if(!empty($result)){
                    $data->total_order_kit =   $this->comman_model->getKitCountJccareRetailer($result->user_id,$result->reference_no);
                    $data->remaingQty  = $this->comman_model->getKitStockJccareRetailer($result->reference_no);
                }
            //} else {
               // $this->session->set_flashdata('alert_danger', 'Some approval are required !');
               // redirect('inventory/kit_request_list');
            //}
        }
        $data->request_list = $this->comman_model->getAll('kit_assignment_request', ['status' => 1]);

        $this->load->view('admin/inventory/assign_kits', $data);
    }

    /* insert/update kit assignment.  */
    public function post_kit_assignment() {

        $request_id = $this->input->post('request_id');
        $chk = $this->db->where('request_id', $request_id)->get('assign_kits')->row();
        if ($chk) {
            $this->session->set_flashdata('alert_danger', 'This request already processed !');
            redirect('inventory/kit_request_list');
        }

        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');

        /* set validation rules */

        $this->form_validation->set_rules('total_qty', 'quantity', 'trim|required');


        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            redirect('inventory/kit_assignment/' . $request_id);
        } else {

            //$status = $this->input->post('status');
            //$status = (empty($status))?0:1;

            /* set variables from the form */
            $insertData = array(
                'reference_no' => $this->input->post('reference_no'),
                'request_id' => $this->input->post('request_id'),
                'assigned_qty' => $this->input->post('total_qty'),
                'description' => $this->input->post('description'),
                'created_at' => date("Y-m-d H:i:s"),
            );

            if (!empty($edit_id)) {

                /* if ($this->comman_model->update('warehouses',['id'=>$edit_id],$insertData)) {

                  $this->session->set_flashdata('alert_success', 'Record updated successfully.');
                  redirect('inventory/kit_request_list');
                  }else
                  {
                  $failed = true;
                  } */
            } else {

                if ($insert_id = $this->comman_model->save('assign_kits', $insertData)) {

                    //$qrcodes = $this->input->post('qrcode');
                    $barcodes = $this->input->post('barcode');
                    //$kitid = $this->input->post('kitid');
                    $date = date("Y-m-d H:i:s");
                    $i = 0;
                    foreach ($barcodes as $barcode) {

                        $insertData1 = array(
                            'parent_id' => $insert_id,
                            //'kit_id' => $kitid[$i], 
                            'barcode' => $barcode,
                            'created_at' => $date,
                        );

                        $this->comman_model->save('assign_kit_details', $insertData1);
                        $this->comman_model->update('kit_upload', ['barcode' => $barcode[$i]], ['is_used' => 1]);
                        $i++;
                    }

                    $this->comman_model->update('kit_assignment_request', ['id' => $request_id], ['	status' => 4]);

                    /* creation ok */
                    $this->session->set_flashdata('alert_success', 'Record added successfully.');
                    redirect('inventory/kit_request_list');
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in adding record. Please try again.');

                redirect('inventory/kit_assignment/' . $request_id);
            }
        }
    }

    public function view_kit_assignment($id = null) {

        /* create the data object */
        $data = new stdClass();

        if (($id != null) && ($id > 0)) {

            $result = $this->comman_model->getRow('assign_kits', ['request_id' => $id]);

            if ($result) {

                $data->assign = $result;
                $data->edit = $this->db->where('id', $result->request_id)->get('kit_assignment_request')->row();
                $data->details = $this->db->where('parent_id', $result->id)->get('assign_kit_details')->result();
            } else {
                $this->session->set_flashdata('alert_danger', 'Invalid record ID !');
                redirect('inventory/kit_request_list');
            }
        }
        $this->load->view('admin/inventory/view_kit_assignment', $data);
    }

    public function get_barcode_detail() {
        if ($this->input->post()) {
            $barcode = trim($this->input->post('barcode'));
            if (!empty($barcode)) {
                $result = $this->db->where('barcode', $barcode)->where('is_used', 0)->get('kit_upload')->row();

                if ($result) {
                    echo json_encode(array('status' => true, 'result' => $result));
                    die;
                } else {
                    echo json_encode(array('status' => false));
                    die;
                }
            }
        }
    }

    public function reject_kit_assignment() {
        if ($this->input->post()) {
            $request_id = $this->input->post('request_id');
            $reason = $this->input->post('reason');

            $chk = $this->comman_model->update('kit_assignment_request', ['id' => $request_id], ['rejected_by' => 'inv_head', 'rejected_reason' => $reason, 'status' => 3]);

            $this->session->set_flashdata('alert_success', 'Request rejected !');
            redirect('inventory/kit_request_list');
        }
    }

    // SHIPPING

    public function addWarehouse() {
        /* $data = array("phone" => 919685095755,
          "city" => "Indore",
          "name" => "JCG Warehouse",
          "pin" => '452010',
          "address" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road",
          "country" => "India",
          "contact_person" => "Yashwant Sharma",
          "email" => "warehousing@jcventures.in",
          "registered_name" => $this->client_name
          ); */

        $dlvry = new Delhivery();
        $status = $dlvry->add_warehouse();

        echo ($status) ? "Warehouse added successfully" : "Error in creating Warehouse!";
    }

    public function delhivery_dispatch($id = null) {
        $data = new stdClass();

        if ($id) {
            $data->edit = $this->db->where('id', $id)->get('stock_out')->row();
            //echo "<pre>"; print_r($data);die;
            $data->wh = $this->db->where('id', 1)->get('warehouses')->row();
            $this->load->view('admin/inventory/delhivery_dispatch', $data);
        }
    }

    public function dtdc_dispatch($id = null) {
        $data = new stdClass();

        if ($id) {
            $data->edit = $this->db->where('id', $id)->get('stock_out')->row();
            //echo "<pre>"; print_r($data);die;
            $data->wh = $this->db->where('id', 1)->get('warehouses')->row();
            $this->load->view('admin/inventory/dtdc_dispatch', $data);
        }
    }

    public function dtdc_generate_package() {
        $order_id = $this->input->post('order_id');
        $dis_id = $this->input->post('dis_id');

        $dis = $this->db->where('id', $dis_id)->get('stock_out')->row();

        $shipData = array(
            0 =>
            array(
                'AgentID' => '',
                'AwbNo' => '',
                'Breath' => '1',
                'CPD' => date('d/m/Y'),
                'CollectableAmount' => (int) 0,
                'Consg_Number' => $dis->order_number,
                'Consolidate_EW' => $dis->order_number,
                'CustomerName' => $dis->customer_name,
                'Ewb_Number' => '876867',
                'GST_REG_STATUS' => 'Y',
                'HSN_code' => '02314h03',
                'Height' => '1',
                'Invoice_Ref' => $dis->order_number,
                'IsPudo' => 'N',
                'ItemName' => 'JC Genetic Salivary Testing Kit',
                'Length' => '1',
                'Mode' => 'P',
                'NoOfPieces' => $dis->item_qty,
                'OrderConformation' => 'Y',
                'OrderNo' => $dis->order_number,
                'ProductCode' => 'GNCT001',
                'PudoId' => '',
                'REASON_TRANSPORT' => '',
                'RateCalculation' => 'N',
                'Seller_GSTIN' => '',
                'ShippingAdd1' => $dis->shipping_address,
                'ShippingAdd2' => '',
                'ShippingCity' => $dis->shipping_city,
                'ShippingEmailId' => $dis->customer_email,
                'ShippingMobileNo' => $dis->customer_contact,
                'ShippingState' => $dis->shipping_state,
                'ShippingTelephoneNo' => $dis->customer_contact,
                'ShippingZip' => $dis->shipping_pincode,
                'Shipping_GSTIN' => '',
                'TotalAmount' => (int) $dis->invoice_amount,
                'TransDistance' => '',
                'TransporterID' => '',
                'TransporterName' => 'DTDC Courier Cargo',
                'TypeOfDelivery' => 'Home Delivery',
                'TypeOfService' => 'Express',
                'UOM' => 'Per KG',
                'VendorAddress1' => '7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road, Indore',
                'VendorAddress2' => '',
                'VendorName' => 'JC Genetics Private Limited',
                'VendorPincode' => '452010',
                'VendorTeleNo' => '9685095755',
                'Weight' => '0.175',
            ),
        );

        $dtdc = new Dtdc();
        $result = json_decode($dtdc->create_package($shipData));
        //print_r($result[0]->DockNo);die;
        if ($result[0]->Succeed == "Yes") {
            $waybill = $result[0]->DockNo;
            $this->comman_model->update('stock_out', ['order_id' => $order_id], ['tracking_code' => $waybill]);
            $result = array('status' => true, 'trackingcode' => $waybill, 'msg' => 'package booked successfully.');
            echo json_encode($result);
            die;
        } else {
            $result = array('status' => false, 'msg' => 'Some error occured. Please try again.');
            echo json_encode($result);
            die;
        }
    }

    public function dtdc_reverse_package() {
        $order_id = $this->input->post('order_id');
        $dis_id = $this->input->post('dis_id');

        $dis = $this->db->where('id', $dis_id)->get('stock_out')->row();
        $data = array(
            'ClientId' => 'INSTACOM',
            'UserName' => 'instauser',
            'Password' => 'insta2013',
            'RequestId' => $dis->order_number,
            'ConsignorName' => $dis->customer_name,
            'ConsignorAddress1' => $dis->shipping_address,
            'ConsignorAddress2' => '',
            'MobileNo' => $dis->customer_contact,
            'Pincode' => $dis->shipping_pincode,
            'SkuDescription' => 'Genetic test kit',
            'DeclaredValue' => 100,
            'AgentId' => 'TEST',
            'CustomerCode' => 'CC000100132',
            'VendorName' => 'JC Genetics Private Limited',
            'VendorAddress1' => '7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road, Indore',
            'VendorAddress2' => '',
            'VendorPincode' => '452010',
            'VendorTeleNo' => '8871374798',
            'TransportMode' => 'Express',
            'ItemChecked' => 'N',
            'DockNo' => '',
        );

        //print_r($data);die;
        $dtdc = new Dtdc();
        $result = $dtdc->reverse_package($data);
        $xml = simplexml_load_string($result);
        $txt = (string) $xml[0];
        $xml = simplexml_load_string($txt);

        if ($xml->ORDER->Succeed == "Yes") {

            $this->comman_model->update('stock_out', ['order_id' => $dis->order_id], ['tracking_code' => $xml->ORDER->DOCKNO]);
            $result = array('status' => true, 'trackingcode' => $xml->ORDER->DOCKNO, 'msg' => $xml->ORDER->Reason);
            echo json_encode($result);
            die;
        } else {
            $result = array('status' => false, 'msg' => $xml->ORDER->Reason);
            echo json_encode($result);
            die;
        }
    }

    public function delhivery_generate_package() {
        $order_id = $this->input->post('order_id');
        $dis_id = $this->input->post('dis_id');

        $dis = $this->db->where('id', $dis_id)->get('stock_out')->row();

        $shipData = array(
            "name" => $dis->customer_name, # consignee
            "order" => $dis->order_number, # client order number
            "order_date" => $dis->order_date, # ISO Format
            "total_amount" => $dis->invoice_amount, # in INR
            "add" => $dis->shipping_address,
            "city" => $dis->shipping_city,
            "state" => $dis->shipping_state,
            "phone" => $dis->customer_contact,
            "pin" => $dis->shipping_pincode,
            "quantity" => $dis->item_qty,
            "payment_mode" => "Pre­paid",
        );

        $pickupData = array("add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road",
            "city" => "Indore",
            "country" => "India",
            "name" => 'JCG Warehouse',
            "phone" => "919685095755",
            "pin" => "452010"
        );

        $dlvry = new Delhivery();
        $result = $dlvry->create_package($order_id, $shipData, $pickupData);
        //print_r($result);die;
        if ($result) {
            $waybill = $result['waybill'];
            $this->comman_model->update('stock_out', ['order_id' => $order_id], ['tracking_code' => $waybill]);
            $result = array('status' => true, 'trackingcode' => $waybill, 'msg' => 'package booked successfully.');
            echo json_encode($result);
            die;
        } else {
            $result = array('status' => false, 'msg' => 'Some error occured. Please try again.');
            echo json_encode($result);
            die;
        }
    }

    public function delhivery_reverse_package() {
        $order_id = $this->input->post('order_id');
        $dis_id = $this->input->post('dis_id');

        $dis = $this->db->where('id', $dis_id)->get('stock_out')->row();

        $shipData = array(
            "name" => $dis->customer_name, # consignee
            "order" => $dis->order_number, # client order number
            "order_date" => $dis->order_date, # ISO Format
            "total_amount" => $dis->invoice_amount, # in INR
            "add" => $dis->shipping_address,
            "city" => $dis->shipping_city,
            "state" => $dis->shipping_state,
            "phone" => $dis->customer_contact,
            "pin" => $dis->shipping_pincode,
            "quantity" => $dis->item_qty,
            "payment_mode" => "Pickup",
        );

        $pickupData = array("add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road",
            "city" => "Indore",
            "country" => "India",
            "name" => 'JCG Warehouse',
            "phone" => "919685095755",
            "pin" => "452010"
        );

        $dlvry = new Delhivery();
        $result = $dlvry->create_package($order_id, $shipData, $pickupData);
        //print_r($result);die;
        if ($result) {
            $waybill = $result['waybill'];
            $this->comman_model->update('stock_out', ['order_id' => $order_id], ['rev_tracking_code' => $waybill]);
            $result = array('status' => true, 'trackingcode' => $waybill, 'msg' => 'package booked successfully.');
            echo json_encode($result);
            die;
        } else {
            $result = array('status' => false, 'msg' => 'Some error occured. Please try again.');
            echo json_encode($result);
            die;
        }
    }

    public function printDelhiverySlip($order_id) {
        if (!empty($order_id)) {

            $dis = $this->db->where('order_id', $order_id)->get('stock_out')->row();
            //print_r($dis);die;
            if (!empty($dis->tracking_code)) {
                $dlvry = new Delhivery();
                $slipdata = json_decode($dlvry->packing_slip($dis->tracking_code), true);

                $this->load->view('admin/inventory/manifest', compact('slipdata'));
            }
        }
    }

    public function pickupRequestDelhivery() {
        /* create the data object */
        $data = new stdClass();
        $edit_id = $this->input->post('edit_id');
        /* set validation rules */

        $this->form_validation->set_rules('pickup_time', 'pickup time', 'trim|required');
        $this->form_validation->set_rules('pickup_date', 'pickup date', 'trim|required');

        if ($this->form_validation->run() === false) {
            /* validation not ok, send validation errors to the view */
            redirect("inventory/delhivery_dispatch/$edit_id");
        } else {

            $insertData = array(
                'pickup' => 1,
                'pickup_date' => $this->input->post('pickup_date'),
                'pickup_time' => $this->input->post('pickup_time'),
                'pickup_location' => 'JCG Warehouse'
            );

            if (!empty($edit_id)) {

                if ($this->comman_model->update('stock_out', ['id' => $edit_id], $insertData)) {
                    /* updation ok */
                    $this->session->set_flashdata('alert_success', 'Pickup request placed successfully.');

                    redirect("inventory/delhivery_dispatch/$edit_id");
                } else {
                    $failed = true;
                }
            }

            if (isset($failed)) {

                /* creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem in creating pickup request. Please try again.');

                redirect("inventory/delhivery_dispatch/$edit_id");
            }
        }
    }

    public function kitdispatch_request() {

        $kit_request_id = $this->input->post('kitrequest_id');
        $shipping_company = $this->input->post('shipping_company');
        $tracking_code = $this->input->post('tracking_code');
        $shipping_date = $this->input->post('shipping_date');
        $shipping_note = $this->input->post('shipping_note');
        $assign_kits_dispatch = array('shipping_company' => $shipping_company,
            'tracking_code' => $tracking_code,
            'shipping_date' => $shipping_date,
            'shipping_note' => $shipping_note,
            'kit_request_id' => $kit_request_id,
        );
        $this->db->insert("assign_kits_dispatch", $assign_kits_dispatch);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {

            $this->db->where('id', $kit_request_id);
            $this->db->update('kit_assignment_request', array('status' => 5));
            $data = array('status' => true, 'msg' => 'kits dispatch request save successfully');
        } else {
            $data = array('status' => false, 'msg' => 'kits dispatch request save successfully');
        }
        echo json_encode($data);
        die;
    }
    
    public function jccareretilerpickup_request() {
       $data['request_list'] = $this->db->select('*')
                                ->from('jccare_pickup_requests')
                                ->where('jccare_pickup_requests.status', 1)->get()->result();
       $this->load->view('admin/inventory/jccareretiler_pickup_request', $data);
    }
    public function jccareretailer_dispatch() {
     
        $request_id = $this->input->post('request_id');
        $courier_company = $this->input->post('shipping_company');
        $tracking_number = $this->input->post('tracking_code');
        $shipping_date = $this->input->post('shipping_date');
        $shipping_note = $this->input->post('shipping_note');
        $dispatchReq = array('courier_company' => $courier_company,
                             'prequest_id' => $request_id,
                             'tracking_number' => $tracking_number,
                             'date' => $shipping_date,
                             'note' => $shipping_note,
                             'status' =>1,
                            );
        $this->db->insert("jccare_pickup_courier_details", $dispatchReq);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $data = array('status' => true, 'msg' =>'Kit request save successfully');
        } else {
            $data = array('status' => false, 'msg' => 'Kit request save successfully');
        }
        echo json_encode($data);
        die;
    }
    public function view_jccareretailer_request($id) {
    
        $result = $this->comman_model->getRow('jccare_pickup_requests',['user_id' => $id]);
        
        if ($result) {
            $data['result'] = $result;
            $data['details'] = $this->db->where('prequest_id',$result->id)->get('jccare_pickup_courier_details')->result();
        } else {
            $this->session->set_flashdata('alert_danger', 'Invalid record ID !');
            redirect('inventory/jccareretilerpickup_request');
        }
            $this->load->view('admin/inventory/view_jccareretailer_request', $data);
    }
    public function getrejectemsg() {
     
        $request_id = $this->input->post('id');
        $result = $this->comman_model->getRow('kit_assignment_request',['id' => $request_id]);
        if (!empty($result->rejected_reason)){
            $data = array('status' => true,'data'=>$result->rejected_reason, 'msg' =>'rejected note message');
        } else {
            $data = array('status' => false,'data'=>"",  'msg' => 'no rejecte message');
        }
        echo json_encode($data);
        die;
    }
    public function dispatched_details() {
        
        $request_id = $this->input->post('id');
        $result = $this->comman_model->getRow('assign_kits_dispatch',['id' => $request_id]);
        if (!empty($result)){
            $data = array('status' => true,'data'=>$result, 'msg' =>'kit dispatch details');
        } else {
            $data = array('status' => false,'data'=>"",  'msg' => 'No Dispatch Details');
        }
        echo json_encode($data);
        die;
    }
    public function addjccareretailer() {
        $contact_person = $this->input->post('contact_person');
        $email = $this->input->post('email');
        $contact_numer = $this->input->post('contact_numer');
        $user_id = $this->input->post('user_id');
        $reference_no = $this->input->post('reference_no');
        $address = $this->input->post('address');
        $state_id = $this->input->post('state_id');
        $district_id = $this->input->post('district_id');
        $city = $this->input->post('city');
        $kit_qty = $this->input->post('kit_qty');
        $pincode = $this->input->post('pincode');
        
        $insertdata = array('user_id' => $user_id,
                             'reference_no' => $reference_no,
                             'quantity' => $kit_qty,
                             'address' => $address,
                             'city' => $city,
                             'district' =>$district_id,
                             'state' =>$state_id,
                             'pincode' =>$pincode,
                             'contact_person' =>$contact_person,
                             'email' =>$email,
                             'contact1' =>$contact_numer,
                            );
        $this->db->insert("kit_assignment_request", $insertdata);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $data = array('status' => true, 'msg' =>'Jcare/Retailer request save successfully');
        } else {
            $data = array('status' => false, 'msg' => 'Please try agian');
        }
        echo json_encode($data);
        die; 
    }
    

}
