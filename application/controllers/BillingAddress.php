<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Billingaddress extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		(!is_logged_in())?redirect('/'):'';
		$this->load->model('billing_address');
		
	}
	
	/**
	 * render add billing address page.
	 * 
	 * @access public
	 * @return void
	 */
	public function index($id = null) {
			
		/* create the data object */
		$data = new stdClass();

		/* if group edit  */
		if (($id != null) && ($id > 0)) {
			$data->edit = $this->db->where('id',$id)->get('billing_address')->row();
		}

		/* getting logged in user data and sending to profile view */
		$user_id = $this->session->user_id;
		$this->load->model('comman_model');
		
		$data->countries  = $this->comman_model->getAll('countries',[]);
		$data->states  = $this->comman_model->getAll('states',[]);
		$data->cities  = $this->comman_model->getAll('cities',['state_id' => '21']);
		//print_r($data->countries);die;
		$data->page  = 'user/billing_address';
		$this->load->view('include/user_layout', compact('data'));	
		
	}
	
	/**
	 * insert/update billing address.
	 * 
	 * @access public
	 * @return void
	 */
	public function add() {
		
		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');

		/* set validation rules */

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('mobile1', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
		$this->form_validation->set_rules('mobile2', 'Mobile Number ', 'regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
		$this->form_validation->set_rules('country_id', 'Country', 'trim|required');
		$this->form_validation->set_rules('state_id', 'State', 'trim|required');
		$this->form_validation->set_rules('city_id', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Shipping Address ', 'min_length[15]');
		$this->form_validation->set_rules('pincode', 'Pincode ', 'required');
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			$this->index();
			
		} else {
			
			/* set variables from the form */
			$billingData = array(
				'name' => $this->input->post('name'), 
				'email' => $this->input->post('email'), 
				'mobile1' => $this->input->post('mobile1'), 
				'mobile2' => $this->input->post('mobile2'), 
				'country_id' => $this->input->post('country_id'), 
				'state_id' => $this->input->post('state_id'), 
				'city_id' => $this->input->post('city_id'), 
				'locality' => $this->input->post('locality'), 
				'address' => $this->input->post('address'), 
				'landmark' => $this->input->post('landmark'), 
				'pincode' => $this->input->post('pincode'), 
				'user_id' => $this->session->user_id, 
				
			);

			if(!empty($edit_id)) {
				
				if ($this->billing_address->update($edit_id,$billingData)) {
				
					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Your billing address updated successfully.');
					redirect('BillingAddress/add');
				}else
				{
					$failed = true;
				}

			}else{

				if ($this->billing_address->insert($billingData)) {
				
					/* creation ok */
					$this->session->set_flashdata('alert_success', 'Your billing address added successfully.');
					redirect('BillingAddress/add');	
				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* user creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem in creating billing address. Please try again.');
				
				redirect('BillingAddress/add');
				
			}
			
		}
		
	}

	/**
	 * render billing address listing.
	 * 
	 * @access public
	 * @return void
	 */
	public function addresslist() {
			
		/* create the data object */
		$data = new stdClass();

		/* getting logged in user id */
		$user_id = $this->session->user_id;
		$this->load->model('comman_model');
		
		$data->addressList  = $this->comman_model->getAll('billing_address',['status' => 1,'user_id' => $user_id]);
		//print_r($data->countries);die;
		$data->page  = 'user/billing_address_list';
		$this->load->view('include/user_layout', compact('data'));	
		
	}
		
	
}
