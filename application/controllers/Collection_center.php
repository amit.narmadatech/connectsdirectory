<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Collection_center class.
 * 
 * @extends CI_Controller
 */
class Collection_center extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		if(!is_admin()){
            redirect('Admin'); 
        }
		$this->load->model('collection_center_model');
		$this->load->model('template_model');
		$this->load->model('comman_model');
		
	}

	/**
	 * render collection center dashboard.
	 * 
	 * @access public
	 * @return void
	 */
	public function dashboard() {

		/* create the data object */
		$data = [];
		$data['kit_received_all'] = $this->db->where('status',1)->get('collected_kits')->num_rows();
		
		$data['kit_received_month'] = $this->db->query("SELECT * FROM collected_kits WHERE MONTH(`created_at`) = MONTH(CURRENT_DATE()) AND YEAR(`created_at`) = YEAR(CURRENT_DATE()) AND status = 1")->num_rows();

		$data['kit_received_week'] = $this->db->query("SELECT * FROM collected_kits WHERE WEEK(`created_at`) = WEEK(CURRENT_DATE()) AND YEAR(`created_at`) = YEAR(CURRENT_DATE()) AND status = 1")->num_rows();

		$data['kit_received_today'] = $this->db->query("SELECT * FROM collected_kits WHERE DATE(`created_at`) = CURDATE() AND status = 1")->num_rows();

		$data['kit_sent_all'] = $this->db->where('status',2)->get('collected_kits')->num_rows();
		
		$data['kit_sent_month'] = $this->db->query("SELECT * FROM collected_kits WHERE MONTH(`created_at`) = MONTH(CURRENT_DATE()) AND YEAR(`created_at`) = YEAR(CURRENT_DATE()) AND status = 2")->num_rows();

		$data['kit_sent_week'] = $this->db->query("SELECT * FROM collected_kits WHERE WEEK(`created_at`) = WEEK(CURRENT_DATE()) AND YEAR(`created_at`) = YEAR(CURRENT_DATE()) AND status = 2")->num_rows();

		$data['kit_sent_today'] = $this->db->query("SELECT * FROM collected_kits WHERE DATE(`created_at`) = CURDATE() AND status = 2")->num_rows();

		//print_r($data);die;
		$this->load->view('admin/collection_center/center_dashboard', $data);		
	}

	/**
	 * render user dashboard.
	 * 
	 * @access public
	 * @return void
	 */
	public function index($id = null) {
		
		$data = new stdClass();

		$this->load->model('comman_model');

		$data->center_list  = $this->comman_model->getAll('collection_center', []);
		$this->load->view('admin/collection_center/collection_center_list', $data);		
	}


	/**
	 * render add collection center form.
	 * 
	 * @access public
	 * @return void
	 */
	public function show_form($id = null) {
		
		$data = new stdClass();

		$this->load->model('comman_model');
		/* if edit  */
		if (($id != null) && ($id > 0)) {
			$data->edit = $this->comman_model->getRow('collection_center', ['id'=>$id]);
		}
		$data->state_list  = $this->comman_model->getAll('states', ['country_id'=>101]);
		$this->load->view('admin/collection_center/add_collection_center', $data);		
	}
	
	/**
	 * insert/create new collection center.
	 * 
	 * @access public
	 * @return void
	 */
	public function add() {

		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');
				
		/* set validation rules */

		$this->form_validation->set_rules('title', 'title', ['trim','required','min_length[5]','max_length[64]']);
		$this->form_validation->set_rules('contact_person', 'contact person', ['trim','required','min_length[2]','max_length[30]']);
		
		if ($edit_id == "") {	
			$this->form_validation->set_rules('email', 'Email', ['trim','required','valid_email','is_unique[collection_center.email]']);
		}
		$this->form_validation->set_rules('contact1', 'contact', ['trim','required','min_length[10]','max_length[10]','regex_match[/^[0-9]{10}$/]']);
		$this->form_validation->set_rules('contact2', 'alternate contact', ['trim','min_length[10]','max_length[10]','regex_match[/^[0-9]{10}$/]']);
		$this->form_validation->set_rules('state_id', 'state', ['trim','required']);
		$this->form_validation->set_rules('city_id', 'city', ['trim','required']);
		$this->form_validation->set_rules('pincode', 'pincode', ['trim','required']);
		$this->form_validation->set_rules('address', 'address', ['trim','required','min_length[10]']);
		
		$this->form_validation->set_message('is_unique', 'The %s is already taken');
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			if ($edit_id == "") { redirect('collection_center'); }else{ redirect('collection_center/show_form/'.$edit_id); }
			//$this->show_form();
		} else {
			
			$status =(string)$this->input->post('status');
			//$status = (empty($status))?0:1;
			/* set variables from the form */
			$insertData = array(
				'title' => $this->input->post('title'), 
				'contact_person' => $this->input->post('contact_person'), 
				'email' => $this->input->post('email'), 
				'contact1' => $this->input->post('contact1'), 
				'contact2' => $this->input->post('contact2'), 
				'state_id' => $this->input->post('state_id'), 
				'city_id' => $this->input->post('city_id'), 
				'pincode' => $this->input->post('pincode'), 
				'address' => $this->input->post('address'), 
				'description' => $this->input->post('description'), 
				'status' => $status,
			);


			if(!empty($edit_id)) {
				
				if ($this->collection_center_model->update($edit_id,$insertData)) {
				
					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Your collection center updated successfully.');
					redirect('collection_center');
				}else
				{
					$failed = true;
				}

			}else{

				$insert_id = $this->collection_center_model->insert($insertData);

				if (!empty($insert_id) && ($insert_id > 0)) {

					/* START - sending notification */
					

					/* creation ok */
					$this->session->set_flashdata('alert_success', "Collection center added successfully.");
					redirect('collection_center');

				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem creating new collection center. Please try again.');
				
				redirect('collection_center');
				
			}
			
		}
		
	}

	/**
	 * render collected samples list.
	 * 
	 * @access public
	 * @return void
	 */
	public function collection_list() {
		
		$is_damage = empty($this->input->get('st'))?0:$this->input->get('st');

		$data = new stdClass();
		
		$data->kit_list = $this->db->from('collected_kits')
			->join('members','members.id = collected_kits.member_id')
			->join('user','user.id = collected_kits.user_id')
			->join('plan','plan.id = collected_kits.plan_id')
			->join('orders','orders.id = members.order_id')
			->select('collected_kits.*,user.id as user_id,user.firstname,user.lastname, user.email, user.contact1,members.id as member_id,members.name as mem_name,members.dob as mem_dob,members.email as mem_email,members.contact as mem_contact,members.gender as mem_gender,members.weight,members.height,members.created_at,plan.name as plan_name,plan.id as plan_id,plan.price as plan_price,plan.sort_description,orders.order_number')
			->where('collected_kits.is_damage',$is_damage)
			->where('collected_kits.status',1)
			->order_by('collected_kits.created_at','desc')
			->get()->result();

		//print_r($data->kit_list);die;
		$this->load->view('admin/collection_center/collected_kit_list', $data);		
	}

	/**
	 * render dispatch samples list.
	 * 
	 * @access public
	 * @return void
	 */
	public function dispatch_list() {
		
		$data = new stdClass();
		
		$data->dispatch_list = $this->db->from('collection_dispatch')
			->order_by('collection_dispatch.created_at','desc')
			->get()->result();

		//print_r($data->kit_list);die;
		$this->load->view('admin/collection_center/dispatch_list', $data);		
	}

	/**
	 * render sample collection entry form.
	 * 
	 * @access public
	 * @return void
	 */
	public function collect_kit($id = null) {
		/*$kit = $this->db->from('qrcode_mapping')
							->join('user','user.id = qrcode_mapping.user_id')
							->join('plan','plan.id = qrcode_mapping.plan_id')
							->join('kit_upload','kit_upload.qrcode = qrcode_mapping.qrcode')
							->join('orders','orders.id = qrcode_mapping.order_id')
							->select('CONCAT(user.firstname," ",user.lastname) as customer_name,plan.name as plan_name,kit_upload.qrcode,orders.order_number,user.id as user_id,user.device_id')
							->where('qrcode_mapping.id',3)
							->get()->row();
		print_r($kit);die;*/
		$data = new stdClass();

		/* if edit  */
		if (($id != null) && ($id > 0)) {
			//$data->edit = $this->db->where('qrcode',$qrcode)->get('kit_upload')->row();
			$data->edit = $this->db->from('members')
							->join('user','user.id = members.user_id')
							->join('plan','plan.id = members.plan_id')
							->join('kit_upload','kit_upload.barcode = members.barcode')
							->join('orders','orders.id = members.order_id')
							->select('user.id as user_id,user.firstname,user.lastname, user.email, user.contact1,members.id as member_id,members.name as mem_name,members.dob as mem_dob,members.barcode,members.email as mem_email,members.contact as mem_contact,members.gender as mem_gender,members.weight,members.height,members.created_at,plan.name as plan_name,plan.id as plan_id,plan.price as plan_price,plan.sort_description,orders.order_number')
							->where('members.id',$id)
							->get()->row();
			$data->plan = $this->db->where('id',$data->edit->plan_id)->get('plan')->row();
		}
		//echo $this->db->last_query();die;
		//print_r($data->plan);die;
		$this->load->view('admin/collection_center/collect_kit', $data);		
	}

	/**
	 * insert/create sample collection entry details.
	 * 
	 * @access public
	 * @return void
	 */
	public function post_collect_kit() {
		
		/* create the data object */
		$data = new stdClass();
		$member_id = $this->input->post('member_id');		
		/* set validation rules */

		$this->form_validation->set_rules('barcode', 'barcode', ['trim','required','is_unique[collected_kits.barcode]']);
		$this->form_validation->set_rules('patient_name', 'patient person', ['trim','required','min_length[2]','max_length[30]']);
		$this->form_validation->set_rules('birthdate', 'birthdate', ['trim','required']);
		$this->form_validation->set_rules('weight', 'weight', ['trim','required']);
		$this->form_validation->set_rules('height', 'height', ['trim','required']);
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			 $this->collect_kit();
			 //redirect('collection_center/collect_kit/'.$member_id);
		} else {
			
			/* set variables from the form */
			$insertData = array(
				'member_id' => $this->input->post('member_id'), 
				'user_id' => $this->input->post('user_id'), 
				'barcode' => $this->input->post('barcode'), 
				'plan_id' => $this->input->post('plan_id'),  
				'patient_name' => $this->input->post('patient_name'),  
				'is_damage' => ($this->input->post('is_damage'))?1:0,  
				'note' => $this->input->post('note'),  
				'created_by' => $this->session->user_id, 
			);

			$insert_id = $this->comman_model->save('collected_kits', $insertData);

			if (!empty($insert_id) && ($insert_id > 0)) {

				$customer = $this->db->where('id',$this->input->post('user_id'))->get('user')->row(); 
			
				$kit = $this->db->from('collected_kits')
							->join('members','members.id = collected_kits.member_id')
							->join('user','user.id = collected_kits.user_id')
							->join('plan','plan.id = collected_kits.plan_id')
							->join('orders','orders.id = members.order_id')
							->select('CONCAT(user.firstname," ",user.lastname) as customer_name,plan.name as plan_name,collected_kits.barcode,orders.order_number,user.id as user_id,user.device_id')
							->where('collected_kits.id',$insert_id)
							->get()->row();
				/* START - sending notification */

				$this->load->model('notification_model');
					
				/* load a template */			
				$tmpdata = array('NTF_USER_NAME' =>$kit->customer_name,
								  'NTF_BARCODE' => $kit->barcode,
								  'NTF_ORDER_NUMBER' =>$kit->order_number,
								  'NTF_PLAN' => $kit->plan_name,
								  );

				//print_r($tmpdata);die;
				$templateData = get_notification(9,$tmpdata);

				/* sending notification to admin */
				$this->notification_model->add(array(
					'sender_id' => 0,
					'type' => 'admin_broadcast',
					'label' => 'purchase_order_placed',
					'title' => $templateData['title'],
					'body' => $templateData['body'],
					'href' => '',
					'recipient_id' => 0,
				));

				// END - sending notification

				// SEND NOTIFICATION TO MOBILE APPS
				if ($kit) {
					$msg = array('user_id'=>$kit->user_id,
					            'notification'=>'Your kit has been received.',
					            'notitify_type'=>'received');
	     
			        $registrationIds = array($kit->device_id);
			        $result = send_android_notification($registrationIds, $msg);
				}
				/* creation ok */
				$this->session->set_flashdata('alert_success', "Record added successfully.");
				$this->session->keep_flashdata('alert_success');
				redirect('collection_center/collection_list');

			}else
			{
				$failed = true;
			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem creating new record. Please try again.');
				
				redirect('collection_center/collect_kit');
				
			}
			
		}
		
	}

	/**
	 * get details of kit by QR code
	 * 
	 * @access public
	 * @return void
	 */
	public function get_detail_by_qrcode($barcode = null) {

		/* create the data object */
		$data = new stdClass();

		if ($barcode != null) {
			$find = $this->db->where('barcode',$barcode)->get('members')->row();
			$chk = $this->db->where('barcode',$barcode)->get('collected_kits')->num_rows();
			//print_r($this->db->last_query());die;
			if ($find) {
					redirect("collection_center/collect_kit/$find->id");	

				if ($chk > 0) {
					$this->session->set_flashdata('alert_danger', 'Barcode already exists in record !');
					redirect("collection_center/collect_kit");
				}else{
					redirect("collection_center/collect_kit/$find->id");	
				}
			}else{
				$this->session->set_flashdata('alert_danger', 'N record found! Please check input.');
				redirect("collection_center/collect_kit");
			}
		}else{
			redirect('collection_center/collect_kit');	
		}
			
	}

	/**
	 * change collected kit status to dispatched to lab
	 * 
	 * @access public
	 * @return void
	 */
	public function change_collected_kit_status()
	{
		$allkits = $this->input->post('allid');
		$shipping_company = $this->input->post('shipping_company');
		$tracking_code = $this->input->post('tracking_code');
		$shipping_date = $this->input->post('shipping_date');
		$shipping_note = $this->input->post('shipping_note');

		$barcodes = implode(',', $allkits);

		$chk = $this->comman_model->save('collection_dispatch',[
			'shipping_company' => $this->input->post('shipping_company'),
			'tracking_code' => $this->input->post('tracking_code'),
			'shipping_date' => $this->input->post('shipping_date'),
			'shipping_note' => $this->input->post('shipping_note'),
			'barcodes' => $barcodes,
		]);

		if($chk)
		{
			$this->db->where_in('id',$allkits)->update('collected_kits',['status'=>'2']);
			echo 'true';
		}

	}

	public function scan_kit()
	{
		if ($this->input->post()) {
			$barcode = trim($this->input->post('barcode'));
			if (!empty($barcode)) {
				$result = $this->db->where('barcode',$barcode)->where('is_used',0)->get('kit_upload')->row();
				
				if ($result) {
					echo json_encode(array('status' => true, 'result' => $result));die;
				}else{
					echo json_encode(array('status' => false));die;
				}

			}
		}
	}

	public function getdispatchedKitList()
	{
		$id = $this->input->post('id');

		$rcd = $this->db->where('id',$id)->get('collection_dispatch')->row();
		
		if($rcd)
		{
			$barcodes = explode(',', $rcd->barcodes);
			$result = $this->db->where_in('id',$barcodes)->get('collected_kits')->result();
			//print_r($result);die;
			if ($result) {
				echo json_encode(array('status' => true,'result' => $result));die;
			}else{
				echo json_encode(array('status' => false));die;
			}
		}
		
		echo json_encode(array('status' => false));die;

	}

}
