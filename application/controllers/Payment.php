<?php

defined('BASEPATH') OR exit('No direct script access allowed');

 require_once(APPPATH . "/third_party/razorpay-php/Razorpay.php");
 use Razorpay\Api\Api;

class Payment extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('Comman_model');
        $this->load->helper('url');
        $this->load->helper('Comman');
        $this->load->model('template_model');
    }

    public function paymentprocess() {
        if (!is_logged_in()) {
            redirect('login');
        }

        $DB3 = $this->load->database('db3', TRUE);
        $DB2 = $this->load->database('db2', TRUE);

        $this->form_validation->set_rules('firstname', 'first name', 'required');
        $this->form_validation->set_rules('lastname', 'last name', 'required');
        $this->form_validation->set_rules('paymenttype', 'paymenttype', 'required');
        $this->form_validation->set_rules('address', 'address', 'required');
        $this->form_validation->set_message('email', 'email', 'Please enter plan name');
        $this->form_validation->set_message('mobile', 'mobile', 'enter price');

        if ($this->form_validation->run() == FALSE) {

            redirect(site_url('/home/checkout'), 'refresh');
        } else {

            $data = array();

            $userid = $this->session->userdata('user_id');

            $customer_name = $this->input->post('firstname');
            $customer_lastname = $this->input->post('lastname');
            $customer_email = $this->input->post('email');
            $customer_mobile = $this->input->post('mobile');
            $customer_address = $this->input->post('address');

            $pincode = $this->input->post('pincode');
            $lanmark = $this->input->post('lanmark');
            $district = $this->input->post('district');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $branch = $this->input->post('branch');
            $couponcode = $this->input->post('couponcode');

            $this->db->select('cart.*,plan.name,plan.price,plan.id as planid,plan.package_id');
            $this->db->from('cart');
            $this->db->join('plan', 'cart.plan_id = plan.id');
            $this->db->where('cart.user_id', $userid);
            $cart = $this->db->get()->result();

            $billingAddress = $this->db->select('*')->from('billing_address')->where('user_id', $userid)->get()->row();

            if (empty($billingAddress)) {

                $billingsumarry = array('email' => $customer_email,
                    'user_id' => $userid,
                    'name' => $customer_name . ' ' . $customer_lastname,
                    'mobile1' => $customer_mobile,
                    'address' => $customer_address,
                    'landmark' => $lanmark,
                    'pincode' => $pincode,
                    'state_id' => $state,
                    'district_id' => $district,
                    'city' => $city,
                    'branch_id' => $branch,
                    'ref_code' => $couponcode
                );
                $this->db->insert('billing_address', $billingsumarry);
            } else {
                $billingsumarry = array('email' => $customer_email,
                    'user_id' => $userid,
                    'name' => $customer_name . ' ' . $customer_lastname,
                    'mobile1' => $customer_mobile,
                    'address' => $customer_address,
                    'landmark' => $lanmark,
                    'pincode' => $pincode,
                    'state_id' => $state,
                    'district_id' => $district,
                    'city' => $city,
                    'branch_id' => $branch,
                    'ref_code' => $couponcode
                );
                $this->db->where(array('user_id' => $userid));
                $this->db->update('billing_address', $billingsumarry);
            }
            $data['email'] = $customer_email;
            $data['name'] = $customer_name . ' ' . $customer_lastname;
            $data['mobile'] = $customer_mobile;
            $data['pincode'] = $pincode;
            $data['branch'] = $branch;
            $data['couponcode'] = $couponcode;

            $total = 0;
            $sgst = 0;
            $cgst = 0;
            $igst = 0;
            $netamt = 0;
            $plan_info = array();

            foreach ($cart as $cartinfo) {

                $plan_info[] = $cartinfo->name;
                
                $taxdata = getGstTax($cartinfo->package_id);
                if ($state == $taxdata['state_id']) 
                {
                    $taxprnct =$taxdata['sgst_tax']+$taxdata['cgst_tax'];

                    $amt = ($cartinfo->qty * $cartinfo->price);
                    $taxamt = ($amt * $taxprnct)/100;
                    
                    $sgst += $taxdata['sgst_tax'];
                    $cgst += $taxdata['cgst_tax'];

                }else{

                    $taxprnct = $taxdata['igst_tax'];
                    $amt = ($cartinfo->qty * $cartinfo->price);
                    $taxamt = ($amt * $taxprnct)/100;

                    $igst += $taxdata['igst_tax'];
                }

                $netamt = $amt+$taxamt;
                $total += $netamt;
            }

            $planinfo = implode(",", $plan_info);

            $userid = $this->session->userdata('user_id');
            $ORDER_ID = $userid . time();
            $data['order_id'] = $ORDER_ID;
            
         //   if(GetIP() == "103.76.252.78"){ 
            
            if($userid == 87 || $userid == 42){
                $data['total_price'] = 1;
            }else{
               $data['total_price'] = $total;  
              
            }
            $data['user_id'] = $userid;
            //var_dump($data);die;

            if ($this->input->post('paymenttype') == 'paytm'):
                $this->paytm($data);
            elseif ($this->input->post('paymenttype') == 'ccavenue'):

                $data['merchant_id'] = 179060;
                $this->load->view('payment/ccavenue', $data);
            elseif($this->input->post('paymenttype') == 'razorpay'): 
                   $this->razorpay($data);
            else:
                $this->session->set_flashdata('error', 'Please Select Valid Payment Method.');
                redirect(site_url('/home/checkout'), 'refresh');
            endif;
        }
    }

    public function paytm($data) {
        if (!is_logged_in()) {
            redirect('login');
        }
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");

        $user_id = $this->session->userdata('user_id');

        // following files need to be included
        require_once(APPPATH . "/third_party/PaytmKit/lib/config_paytm.php");
        require_once(APPPATH . "/third_party/PaytmKit/lib/encdec_paytm.php");
        
//        if (GetIP() == '103.76.252.78'):
//            $amount = 1;
//        endif;
        $checkSum = "";
        $paramList = array();
        // Create an array having all required parameters for creating checksum.
        $paramList["MID"] = PAYTM_MERCHANT_MID;
        $paramList["ORDER_ID"] = $data['order_id'];
        $paramList["CUST_ID"] = $data['user_id'];
        $paramList["INDUSTRY_TYPE_ID"] = 'Retail109';
        $paramList["CHANNEL_ID"] = 'WEB';
        $paramList["TXN_AMOUNT"] = $data["total_price"];
        //$paramList["TXN_AMOUNT"] = 1;
        $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
        $paramList["CALLBACK_URL"] = PAYTM_CALLBACK_URL.http_build_query(['user_id'=>$user_id]);
        /* print_r($paramList);
          die;
         */
        //Here checksum string will return by getChecksumFromArray() function.
        $checkSum = getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY);
        $this->load->view('paytmprocess', ['checkSum' => $checkSum, 'paramList' => $paramList]);
    }

    function paytmcallback() {

        $user_id = $this->input->get('user_id');
        // following files need to be included
        require_once(APPPATH . "/third_party/PaytmKit/lib/config_paytm.php");
        require_once(APPPATH . "/third_party/PaytmKit/lib/encdec_paytm.php");
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = "FALSE";

        $paramList = $_POST;
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
        //Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
        $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

        if ($isValidChecksum == "TRUE") {
            
            $sendData = array(
                'user_id' => $user_id,
                'order_id' => $paramList['ORDERID'],
                'txnid' => $paramList['TXNID'],
                'order_status' => $paramList['STATUS'],
                'payment_status' => $paramList['STATUS'],
                'payment_mode' => $paramList['PAYMENTMODE'],
                'amount' => $paramList['TXNAMOUNT'],
                'payment_type' => "Paytm",
            );
            
            if ($paramList['STATUS'] === "TXN_SUCCESS") {
                $this->saveOrder($sendData);
            } else {
                redirect('payment/paymentcancel');
            }
           
        } else {
            $msg = "<b>Checksum mismatched.</b>";die;
        }
        
    }

    public function paymentcancel() {
        $this->load->view('failure');
    }

    public function paymentsuccessconfirm() {
        $this->load->view('success');
    }

    // CCAvenue callback function 4591
    public function confirm() {

        $this->load->helper('crypto');
        $workingKey = 'B0215404346F00C10C2E0D850B4F2EE2';  //Working Key should be provided here.
        $encResponse = $_POST["encResp"];   //This is the response sent by the CCAvenue Server
        $rcvdString = decrypt($encResponse, $workingKey);  //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        // print_r($decryptValues);
         // die;
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 0)
                $order_id = $information[1];
            if ($i == 1)
                $txnid = $information[1];
            if ($i == 2)
                $order_status = $information[1];
            if ($i == 3)
                $order_status = $information[1];
            if ($i == 5)
                $payment_mode = $information[1];
            if ($i == 10)
                $amount = $information[1];
            if ($i == 26)
                $userId = $information[1];
            if ($i == 27)
                $package_id = $information[1];
            /*if ($i == 28)
                $userId = $information[1];*/
            if ($i == 21)
                $city = $information[1];
        }

        $sendData = array(
            'user_id' => $userId,
            'order_id' => $order_id,
            'txnid' => $txnid,
            'order_status' => $order_status,
            'payment_mode' => $payment_mode,
            'amount' => $amount,
            'city' => $city,
            'payment_type' => "CCAvenue",
        );
        //print_r($sendData);die;
        if ($order_status == "Success") {
            $this->saveOrder($sendData);
        } else {
            redirect('payment/paymentcancel');
        }
    }

    public function saveOrder($data)
    {
        $DB2 = $this->load->database('db2', TRUE);

        $userId = $data['user_id'];
        $userdetails = $this->Comman_model->get_userinfo($userId);

        $mobile1 = !empty($userdetails->mobile1) ? $userdetails->mobile1 : 0;
        $pincode = !empty($userdetails->pincode) ? $userdetails->pincode : 0;
        $district_id = !empty($userdetails->district_id) ? $userdetails->district_id : 0;
        $state = !empty($userdetails->state_id) ? $userdetails->state_id : 0;
        $city = !empty($userdetails->city) ? $userdetails->city : 0;
        $landmark = !empty($userdetails->landmark) ? $userdetails->landmark : 0;
        
        $branch = !empty($userdetails->branch_id) ? $userdetails->branch_id : 0;
        $couponcode = !empty($userdetails->ref_code) ? $userdetails->ref_code : 0;
       
        
        //$order_number = str_pad(mt_rand(0, 999999), 10, '0', STR_PAD_LEFT);
        
        $total_sgst = 0;
        $total_cgst = 0;
        $total_igst = 0;
        $sgst = 0;
        $cgst = 0;
        $igst = 0;
        $plan_info = array();
        $taxArray = [];

        $cartDetails = $this->Comman_model->get_cartinfo($userId);

        foreach ($cartDetails as $cartinfo) {
            
            $plan_info[] = $cartinfo->name;

            $payble_amount = ($cartinfo->qty * $cartinfo->price);

            $gsttax = getGstTax($cartinfo->package_id);

            if (!empty($gsttax) && ($state == $gsttax['state_id'])) {

                $sgst = (($payble_amount * $gsttax['sgst_tax']) / 100);
                $cgst = (($payble_amount * $gsttax['cgst_tax']) / 100);
                $taxprnct = $gsttax['sgst_tax'] + $gsttax['cgst_tax'];
                $taxamt = $sgst + $cgst ;
            }else
            {
                $igst = (($payble_amount * $gsttax['igst_tax']) / 100);
                $taxprnct = $gsttax['igst_tax'];
                $taxamt = $igst;
            }

            $taxArray[$cartinfo->plan_id] = array('tax_prnct' => $taxprnct,'tax_amt' => $taxamt);

            $total_sgst = $total_sgst + $sgst;
            $total_cgst = $total_cgst + $cgst;
            $total_igst = $total_igst + $igst;
        }

        $planinfo = implode(",", $plan_info);

        $order = array(
            'user_id' => $userId,
            'txnid' => $data['txnid'],
            'order_number' => $data['order_id'],
            'name' => $userdetails->billing_name,
            'total_amount' => $data['amount'],
            'discount' => 0,
            'cgst' => $total_cgst,
            'sgst' => $total_sgst,
            'igst' => $total_igst,
            'email' => $userdetails->billing_email,
            'mobile1' => $mobile1,
            'address' => $userdetails->address,
            'landmark' => $landmark,
            'pincode' => $pincode,
            'district_id' => $district_id,
            'city' => $city,
            'state_id' => $state,
            'payment_mode' => $data['payment_mode'],
            'branch_id' => $branch,
            'ref_code' => $couponcode,
            'payment_type' => $data['payment_type'],
            'payment_status' => $data['payment_status'],
            'order_status' => 1
        );

        $this->db->insert('orders', $order);
        $orderid = $this->db->insert_id();


        if (!empty($orderid)) {
            $plannames = '';
            foreach ($cartDetails as $cart) {

                $plannames .= $cart->name . ' ,';

                $orderDetails = array(
                    'order_id' => $orderid,
                    'plan_id' => $cart->plan_id,
                    'plan_price' => $cart->price,
                    'quantity' => $cart->qty,
                    'tax_prct' => $taxArray[$cart->plan_id]['tax_prnct'],
                    'tax_amt' => $taxArray[$cart->plan_id]['tax_amt'],
                );

                $pkgid = $cart->package_id;

                $insertID = $this->Comman_model->save('order_detail', $orderDetails);

                if (!empty($insertID)) {

                    if (!empty($branch) || !empty($couponcode)) {
                        if (!empty($branch)) {

                            $JCCareRetailerDetails = $this->getJCCareRetailer(array('tblclients.userid' => $branch));
                        } else if (!empty($couponcode)) {

                            $JCCareRetailerDetails = $this->getJCCareRetailer(array('tblclients.ref_code' => $couponcode));
                        }

                        if ($JCCareRetailerDetails["status"]) {
                            $result = $this->getCommission($pkgid, $JCCareRetailerDetails);
                            
                            $TDS = $this->Comman_model->getCompanyTdsById($JCCareRetailerDetails['company_id']);
                            
                            $tds_percent="0";
                            $commission_amt= "0";
                            $amt= $result['amount_otc']*$cart->qty ;
                            if(!empty($TDS)){
                                
                                $tds_percent = ($JCCareRetailerDetails['pan_approve']) ? $TDS->tds_for_pan : $TDS->tds_for_no_pan ;
                                $commission_amt = $amt -(( $amt * $tds_percent) / 100);
                            }

                            // OTC 
                            $commissionData = array(
                                'order_id' => $orderid,
                                'ref_code' => $JCCareRetailerDetails['ref_code'],
                                'commission' => $commission_amt,
                                'company_id' => $JCCareRetailerDetails['company_id'],
                                'user_id' => $JCCareRetailerDetails['user_id'],
                                'tds_percent' => $tds_percent,
                                'total' => $result['amount_otc']*$cart->qty,
                                'commission_date' => date("Y-m-d H:i:s")
                            );
                            //print_r($commissionData);die;

                            $DB2->insert('tbljcgeneticscommission', $commissionData);

                            // ORC

                            if ($JCCareRetailerDetails['type'] == "FR") {
                                
                                  $amt_second = $result['amount_orc']*$cart->qty;
                                  $commissionamt_orc = $amt_second - (( $amt_second * $tds_percent) / 100);

                                $orccommissionData = array(
                                    'order_id' => $orderid,
                                    'jccare_id' => $JCCareRetailerDetails['fan_parent_id'],
                                    'retailer_id' => $JCCareRetailerDetails['user_id'],
                                    //'ref_code' => $JCCareRetailerDetails['ref_code'],
                                    //'company_id' => $JCCareRetailerDetails['company_id'],
                                    'commission_amount' => $commissionamt_orc,
                                    'tds_percent' => $tds_percent,
                                    'total' => $result['amount_orc']*$cart->qty,
                                    'commission_date' => date("Y-m-d H:i:s")
                                );

                                $DB2->insert('tblfanorc_jcgenetics', $orccommissionData);
                            }

                              $agent_id = $JCCareRetailerDetails["user_id"];
                              $agent_type = $JCCareRetailerDetails["type"];

                              if(($agent_type == "FR")){
                                  $parent_id = $JCCareRetailerDetails["fan_parent_id"];
                                  $res = $this->Comman_model->getJccareRetailerById($parent_id);
                                  $addedfrom = $res->addedfrom;
                                  $fid = $parent_id;
                                  $rid = $agent_id;
                              }else{
                                  $addedfrom = $JCCareRetailerDetails["addedfrom"];
                                  
                                  if ($agent_type == "F") {
                                      $fid = $agent_id;
                                      $rid = 0;
                                  }elseif ($agent_type == "R") {
                                      $fid = 0;
                                      $rid = $agent_id;
                                  }

                              }
                            /*
                              if (!empty($addedfrom)) {
                                    //$addedfrom = 49;                    
                                    $incentiveData = $this->getIncentive($addedfrom);
                                    $pckIncentive = $this->db->where('package_id',$pkgid)->get('package_incentive')->result();
                                    $incentive = [];
                                    $roles = [];

                                    foreach ($pckIncentive as $key => $value) {
                                        array_push($roles, $value->incentive_id);
                                        $incentive[$value->incentive_id] = array(
                                            'role_id' => $value->incentive_id,
                                            'staffid' => null,
                                            'percentage' => $value->percentage,
                                            'amount' => $value->amount,
                                        );
                                    }

                                    foreach ($incentiveData as $key => $value) {
                                        if (in_array($value->cmd_designation_id, $roles)) {
                                            $incentive[$value->cmd_designation_id]['staffid'] = $value->staffid;
                                        } else {
                                          unset($incentive[$value->cmd_designation_id]['staffid']);
                                        }
                                    }

                                    if (!empty($incentive)) {
                                        foreach ($incentive as $key => $inc) {
                                            
                                            $incData = array(
                                                            'order_id' => $orderid,
                                                            'jccare_id' => $fid,
                                                            'retailer_id' => $rid,
                                                            'staff_id' => $inc['staffid'],
                                                            'insentive_amount' => $inc['amount']*$cart->qty,
                                                            'insentive_date' => date("Y-m-d H:i:s")
                                                        );

                                            $DB2->insert('tblinsentive_jcgenetics', $incData);
                                        }
                                    }

                                  }else{
                                    echo "No sales manager id found !";die;
                                  } 
                                 */ 
                                  
                        }
                    }
                }
            }

            $this->db->where('user_id', $userId);
            $this->db->delete('cart');

            /* START - sending notification */
            $this->load->model('notification_model');

            $user = $this->Comman_model->getRow('user', ['id' => $userId]);

            /* load a template */
            $tmpdata = array(
                'NTF_FIRSTNAME' => $user->firstname,
                'NTF_LASTNAME' => $user->lastname,
                'NTF_EMAIL' => $user->email,
                'NTF_CREATED_AT' => date("D, d-M-Y h:i A", strtotime($user->created_at)),
                'NTF_ORDER_NO' => $data['order_id'],
                'NTF_TXN_NO' => $data['txnid'],
            );

            $templateData = get_notification(3, $tmpdata);

            /* sending notification to customer */
            $this->notification_model->add(array(
                'sender_id' => 0,
                'type' => 'customer_notification',
                'label' => 'order_placed',
                'title' => $templateData['title'],
                'body' => $templateData['body'],
                'href' => '',
                'recipient_id' => $userId,
            ));

            /* load a template */
            $tmpdata1 = array(
                'NTF_FIRSTNAME' => $user->firstname,
                'NTF_LASTNAME' => $user->lastname,
                'NTF_EMAIL' => $user->email,
                'NTF_CREATED_AT' => date("D, d-M-Y h:i A", strtotime($user->created_at)),
                'NTF_ORDER_NO' => $data['order_id'],
                'NTF_TXN_NO' => $data['txnid'],
            );

            $templateData1 = get_notification(4, $tmpdata1);

            /* sending notification to admin */
            $this->notification_model->add(array(
                'sender_id' => 0,
                'type' => 'admin_broadcast',
                'label' => 'order_placed',
                'title' => $templateData1['title'],
                'body' => $templateData1['body'],
                'href' => '',
                'recipient_id' => 0,
            ));

            // END - sending notification

            // payment successful email
            $attachment = $this->invoice($orderid,$userId);

            $message = $this->load->view('email_template/order', '', true);
            $message = str_replace("{username}", $user->firstname, $message);
            $message = str_replace("{txnid}", $data['txnid'], $message);
            $message = str_replace("{planname}", $planinfo, $message);
            $message = str_replace("{amount}", $data['amount'], $message);
            $subject = "Payment Confirmed for Your " . $planinfo . " Plan";
            $this->Comman_model->emailsend($user->email, $subject, $message,$attachment);

            // Thankyou email after order   

            $message = $this->load->view('email_template/thankyou_mail', '', true);
            $message = str_replace("{username}", $user->firstname, $message);
            $message = str_replace("{txnid}", $data['txnid'], $message);
            $message = str_replace("{planname}", $planinfo, $message);
            $message = str_replace("{amount}", $data['amount'], $message);
            $subject = "A Big thanks for choosing us to serve you!";
            $this->Comman_model->emailsend($user->email, $subject, $message);
        }

        // Sending sms ( Payment Confirmation )
        $mobile = trim($mobile1);

        if (!empty($mobile) && (strlen($mobile) == 10)) {
            $sms = New Sms();
            $message = "Dear Customer, your payment of Rs. ".$data['amount']." received for Order ID ".$data['order_id']." Kindly refer Transaction no. ".$data['txnid']." in case of any queries. We will update you soon about dispatch of your $planinfo DNA Testing kit.\n\nThanks,\nJC Genetics Team";
            $sms->sendsms($mobile, $message);
        }
        // Sending sms ( Thankyou mail )
        if (!empty($mobile) && (strlen($mobile) == 10)) {
            $sms = New Sms();
            $message = "Dear Customer, A Big thanks for choosing us to serve you. You have ordered $planinfo DNA Testing kit and your Order ID is ".$data['order_id'].". We will keep you updated for your order status. For further queries, please contact us at " . base_url('/') . ".\n\nThanks,\nJC Genetics Team";
            $sms->sendsms($mobile, $message);
        }
        $this->session->set_flashdata('item', array('status' => true, 'txnid' => $data['txnid'], 'amount' => $data['amount']));
        $this->session->keep_flashdata('item');
        redirect(base_url('payment/paymentsuccessconfirm'), 'refresh');
        
    }

    function getJCCareRetailer($where) {

        $checkJcCareRetailer = $this->Comman_model->get_jccareretailerby_id($where);
        //echo $this->db->last_query();die;
        $checkStatus = false;
        $arr = '';
        if (!empty($checkJcCareRetailer)) {

            foreach ($checkJcCareRetailer as $key => $value) {

                if (!empty($value->is_franchise)) {

                    $arr = array('status' => true, "type" => 'F', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code, 'fan_parent_id' => $value->fan_parent_id, 'addedfrom' => $value->addedfrom ,'pan_approve' => $value->pan_approve);
                    $checkStatus = true;
                } else if (!empty($value->is_retailer) && $value->slug == "jcgenetics") {

                    $arr = array('status' => true, "type" => 'R', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code, 'ref_code' => $value->ref_code, 'fan_parent_id' => $value->fan_parent_id, 'addedfrom' => $value->addedfrom ,'pan_approve' => $value->pan_approve);
                    $checkStatus = true;
                } else if (!empty($value->is_franchise_retailer) && !empty($value->fan_parent_id) && $value->slug == "jcgenetics") {

                    $arr = array('status' => true, "type" => 'FR', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code, 'fan_parent_id' => $value->fan_parent_id, 'ref_code' => $value->ref_code, 'addedfrom' => $value->addedfrom ,'pan_approve' => $value->pan_approve);
                    $checkStatus = true;
                }
            }
        }
        if ($checkStatus)
            return $arr;
        else
            return array('status' => false);
    }

    public function getCommission($package_id, $result) {

        if (!empty($result)) {

            $type = $result['type'];

            if ($type == "F") {
                $commission = $this->db->where('package_id', $package_id)->where('commission_id', 4)->get('package_commission')->row();
                $comamt = array('amount_otc' => $commission->amount);
            } elseif ($type == "R") {
                $commission = $this->db->where('package_id', $package_id)->where('commission_id', 2)->get('package_commission')->row();
                $comamt = array('amount_otc' => $commission->amount);
            } elseif ($type == "FR") {
                $comm_fr = $this->db->where('package_id', $package_id)->where('commission_id', 2)->get('package_commission')->row();
                $comm_f = $this->db->where('package_id', $package_id)->where('commission_id', 3)->get('package_commission')->row();

                $comamt = array('amount_orc' => $comm_f->amount, 'amount_otc' => $comm_fr->amount);
            }

            return $comamt;
        }
    }

    public function getIncentive($saleMID) {
        
        $DB2 = $this->load->database('db2', TRUE);

        $query = "SELECT T2.staffid, T2.firstname,T2.email,T2.report_to,tblroles.name AS desname,tblroles.slug,tblroles.cmd_designation_id
                FROM (
                    SELECT
                        @r AS id,
                        (SELECT @r := report_to FROM tblstaff WHERE staffid = id) AS parent_id,
                        @l := @l + 1 AS lvl
                    FROM
                        (SELECT @r := ".$saleMID.", @l := 0) vars,
                        tblstaff m
                    WHERE @r <> 0) T1
                JOIN tblstaff T2
                ON T1.id = T2.staffid
                LEFT JOIN tblroles ON tblroles.roleid=T2.role
                ORDER BY T1.lvl DESC;";
        $result = $DB2->query($query)->result();

        return ($result)?$result:false;
    }

    public function invoice($order_id,$user_id) {

      //  $this->db->select('orders.*,billing_address.landmark,billing_address.name,billing_address.mobile1,billing_address.email,billing_address.address,billing_address.state_id');
        $this->db->select('orders.*');
        $this->db->from('orders');
       // $this->db->join('billing_address', 'orders.user_id = billing_address.user_id', 'left');
        $this->db->order_by('orders.id', 'DESC');
        $this->db->where(array('orders.id' => $order_id, 'orders.user_id' => $user_id));
        $data['orders'] = $orders = $this->db->get()->row();

        $data['orderDetails'] = "";

        if (!empty($orders) && !empty($orders->id)) {

            $this->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image,tax.sgst_tax,tax.cgst_tax,tax.igst_tax,tax.state_id as pck_state_id');
            $this->db->from('order_detail');
            $this->db->join('plan', 'order_detail.plan_id = plan.id');
            $this->db->join('tax', 'tax.package_id = plan.package_id');
            $this->db->where('order_detail.order_id', $order_id);
            $data['orderDetails'] = $this->db->get()->result();
        }

        $html = $this->load->view('user/invoice', $data, true);
        $pdfFilePath = FCPATH . "uploads/invoice/invoice.pdf";
        $this->load->library('Pdf');
        $this->load->helper('download');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage('P', 'A4');
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($pdfFilePath, 'F'); // save to file because we can
        //  $pdf->Output('invoice.pdf', 'I');
        //  $pdf->Output('invoice.pdf', 'D');
        return $pdfFilePath;
    }

    public function testemail() {
        $data['email'] = "amit.narmadatech@gmail";
        $message = $this->load->view('email_template/order', $data, true);
        $message = str_replace("{username}", 'amit', $message);
        $message = str_replace("{txnid}", "456564", $message);
        $message = str_replace("{amount}", "456564", $message);

        $subject = "order confirmation";
        $this->Comman_model->emailsend('amit.narmadatech@gmail.com', $subject, $message);
    }
    public function razorpay($data) {

        if (!is_logged_in()) {
            redirect('login');
        }
   
       // $api = new Api('rzp_test_1GKEqQtDaAw4l1', 'LL2IoFfInPiq8udCHFMKAhVE');
        //$keyId ="rzp_test_1GKEqQtDaAw4l1";
        
         $api = new Api('rzp_live_HVIRuYG3dHW3kg', '3CsQcEHqEnLv9gAtppDDWWyc');
         $keyId ="rzp_live_HVIRuYG3dHW3kg";
         
         $user_id = $this->session->userdata('user_id');
         $totalAmount = $data['total_price'];
       //  $totalAmount = 1;
         $orderData = [
            'receipt' => $data['order_id'],
            'amount' => (int)($totalAmount * 100), // 2000 rupees in paise
            'currency' => 'INR',
            'payment_capture' => 1 // auto capture
        ];
      
        $razorpayOrder = $api->order->create($orderData);
        $razorpayOrderId = $razorpayOrder['id'];
        $this->session->set_userdata('razorpay_order_id',$razorpayOrderId);
       
        $displayAmount = $amount = $orderData['amount'];
  
       $postdata = [
            "key" => $keyId,
            "amount" => $amount,
            "name" =>$data['name'],
            "description" => "",
            "image" => "https://www.jcgenetics.in/assets/images/logo.png",
            "prefill" => [
                "name" => $data['name'],
                "email" => $data['email'],
                "contact" => $data['mobile'],
            ],
            "notes" => [
                "address" => "Hello World",
                "merchant_order_id" => "12312321",
            ],
            "theme" => [
                "color" => "#F37254"
            ],
            "order_id" => $razorpayOrderId,
        ];
       
       $data['image'] = "https://www.jcgenetics.in/assets/images/logo.png";
       $data['keyId']= $keyId;
       $data['merchant_name']= "JC Genetics";
       $data['display_currency'] = 'INR';
       $data['display_amount'] = $displayAmount;
        $json = json_encode($postdata);
        $this->load->view('payment/razorpay',$data);
    }
    function razorpayconfirm() {
       
      // $api = new Api('rzp_test_YT22uzR9jMPK4P', 'EFJ1t9Kf4sizjh9BV9d9xRdf');
       
       $api = new Api('rzp_live_HVIRuYG3dHW3kg', '3CsQcEHqEnLv9gAtppDDWWyc');
      // $api = new Api('rzp_test_1GKEqQtDaAw4l1', 'LL2IoFfInPiq8udCHFMKAhVE'); 
       
       $razorpay_payment_id = $this->input->post('razorpay_payment_id');
       
       $user_id = $this->session->userdata('user_id');
       if(empty($user_id)){
           
          $user_id = $this->input->post('user_id'); 
       }
       $order_id = $this->input->post('order_id');
       $couponcode = $this->input->post('couponcode');
       $branch = $this->input->post('branch');
       // $razorpay_payment_id = "pay_APFPer337xIVmC";
      //  $payments = $api->payment->all($params);
      $payment = $api->payment->fetch($razorpay_payment_id);
       // Capturing Payment
        $amount = $payment->amount;
        $capture = $payment->capture(array('amount' => $amount));
     
        $status = $capture->status;
      
        if ($status == "captured") {
             
              $sendData = array('user_id' => $user_id ,
                                'order_id' => $order_id,
                                'txnid' => $razorpay_payment_id,
                                'order_status' =>1,
                                'payment_status' =>$status,
                                'payment_mode' => $payment->method,
                                'amount' => ($amount/100),
                                'payment_type' => "Razorpay",);
            $this->saveOrder($sendData);
        } else {
            redirect('payment/paymentcancel');
        }
       
    }
    
   
}
