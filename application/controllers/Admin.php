<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    var $per_page = 10;

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('Comman_model');
		$this->load->helper('url');
		$this->load->helper('cookie');
    }

    public function index() {
        $this->load->view('admin/signin');
    }

    function login() {
	
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');
		
        if ($this->form_validation->run()) {
		
			$rememberme =$this->input->post('rememberme');
			$email =$this->input->post('email');
			$password =$this->input->post('password');
		
            $data = array('email' => $this->input->post('email'),
						  'password' => md5($this->input->post('password')));
			
		    $result = $this->Comman_model->getRow('user', $data);
		
            if (!empty($result)) {
			
			   if(!empty($rememberme)){
			
				$cookie = array('name' =>'rememberme',
							  'value' =>$password,
							  'expire'=> '1209600',  // Two weeks
							  'domain'=> base_url('/'),
							  //'secure' => TRUE,
							  'path' => '/'
					        );
				   set_cookie($cookie);
			}
				$profile_img = (!empty($result->image))?base_url('uploads/userimages/').$result->image:base_url('uploads/person.png');
			    $arr = array('email' => $this->input->post('email'),'user_id' => $result->id,'user_type' =>$result->type,'name' =>$result->firstname,'image' =>$profile_img);

                $this->session->set_userdata($arr);

                $permission_data = $this->Comman_model->getRow('designations', ['id'=>$result->designation_id]);

                redirect($permission_data->redirect_url);
            } else {
                $msgerror = array('login_error' => 'username or password incorrect');
                $this->load->view('admin/signin', $msgerror);
            }
        } else {
            $this->load->view('admin/signin');
        }
    }
    public function recoverpassword() {

        $this->form_validation->set_rules('email', 'email', 'required|valid_email|callback_isEmailExist');

        if ($this->form_validation->run()) {

            $email = $this->input->post('email');

            $result = $this->Comman_model->getRow('user', array('email' => $email));

            // if (!empty($result)) {

            $token = $this->Comman_model->generateRandomString(10);

            $this->Comman_model->update('user', array('email' => $email), array('token' => $token));

            $resetPasswordLink = base_url('admin/resetpass/') . base64_encode($email) . '?token=' . $token;

            $message = $this->load->view('email_template/forgot_password', '', true);
            $message = str_replace("{username}", $email, $message);
            $message = str_replace("{resetPasswordLink}", $resetPasswordLink, $message);

            $subject = "Password Reset Request";
            $this->Comman_model->emailsend($email, $subject, $message);
            $this->session->set_flashdata('success', 'Please check your email.Password reset link send successfully to your Email.');
            $this->load->view('admin/forgot-password');

        } else {

            $this->load->view('admin/forgot-password');
        }
    }

    public function resetpass($email="") {
	
       $token = $this->input->get('token');
       $myemail =  base64_decode($email);
       $result =  $this->Comman_model->getRow('user',  array('token'=>$token,'email'=>$myemail));    
   
       if(!empty($result)){
           $this->Comman_model->update('user',array('email'=>$myemail), array('token'=>'')); 
        }
		//$this->form_validation->set_rules('password', 'Password', 'required');
		//$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required');
		
		$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[8]|matches[cpassword]');
		$this->form_validation->set_rules('cpassword', 'password confirmation', 'required|trim');
	
		if ($this->form_validation->run() == FALSE){
		
			 $this->load->view('admin/reset-password');
		}else{
			$password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            $this->Comman_model->update('user',array('email'=>$myemail), array('password'=> md5($password))); 
			$this->session->set_flashdata('success', 'Password changed successfully.'); 
			$this->load->view('admin/reset-password');
        }
	}
	
	public function isEmailExist($email){
	
		$this->db->select('id');
		$this->db->where('email', $email);
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			return true;
		} else {
		
		    $this->form_validation->set_message('isEmailExist', 'This email does not exist.');    
			return false;
		}
	}
	
    public function logout() {
        $this->session->sess_destroy();
        redirect('admin');
    }
}
