<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Collection_center class.
 * 
 * @extends CI_Controller
 */
class Package extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		if(!is_admin()){
            redirect('Admin'); 
        }

		$this->load->model('package_model');
		$this->load->model('jcventure_model');
		$this->load->model('comman_model');
		
	}

	/**
	 * package list .
	 * 
	 * @access public
	 * @return void
	 */
	public function index($id = null) {
		
		$data = new stdClass();

		$data->package_list  = $this->package_model->get_list([]);
		//print_r($data->package_list);die;
		$this->load->view('admin/package/package_list', $data);		
	}

	public function view_package($id = null) {
		
		if ($id) {
			$data = new stdClass();

			$data->package = $this->package_model->find($id);
			$data->package_product = $this->package_model->get_package_product($id);
			$data->package_expense = $this->package_model->get_package_expense($id);
			$data->package_incentive = $this->package_model->get_package_incentive($id);
			$data->package_commission = $this->package_model->get_package_commission($id);
			//print_r($data);die;
			
			$this->load->view('admin/package/view_package', $data);		
				
		}
	}


	/**
	 * render add package form.
	 * 
	 * @access public
	 * @return void
	 */
	public function add_package($id = null) {
		
		$data = new stdClass();

		/* if edit  */
		if (($id != null) && ($id > 0)) {
			$data->edit = $this->package_model->find($id);
		}

		$data->product_list  = $this->jcventure_model->get_product_list([]);
		$data->commission_list  = array(
									'OTC to JC Care - upon sale of Retailer ID',
									'OTC to Retailer - Sale on Product',
									'ORC to JC Care - on Sales of Proudct via Retailer',
									'OTC to JC Care - on Sale of Product from his outlet'
									);
		$data->incentive_list  = $this->jcventure_model->get_designation_list([]);
		$category_list  = $this->jcventure_model->get_expense_category_list([]);
		$data->expense_list = [];
		foreach ($category_list as $cat) {
			$data->expense_list[$cat->expense_category_id] =  $this->jcventure_model->get_expense_list(['category_id'=>$cat->expense_category_id]);
		}
		//print_r($data);die;
		$this->load->view('admin/package/add_package', $data);		
	}

	/**
	 * insert/create new package.
	 * 
	 * @access public
	 * @return void
	 */
	public function post_add_package() {
		//print_r($_POST);die;
		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');
				
		/* set validation rules */

		$this->form_validation->set_rules('title', 'title', ['trim','required']);
		$this->form_validation->set_rules('price', 'base price', ['trim','required']);
	
		$this->form_validation->set_rules('child_share', 'child company share', ['trim','required']);
		$this->form_validation->set_rules('parent_share', 'parent company share', ['trim','required']);
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			redirect('package/add_package');
			//$this->show_form();
		} else {
			
			//$status =(string)$this->input->post('status');
			//$status = (empty($status))?0:1;
			/* set variables from the form */
			$packageData = array(
				'title' => $this->input->post('title'), 
				'base_price' => $this->input->post('price'), 
				'profit_amount' => $this->input->post('gst_price'), 
				'child_share_prnct' => $this->input->post('child_share'), 
				'parent_share_prnct' => $this->input->post('parent_share'),
				'child_share_amt' => $this->input->post('child_share_amt'), 
				'parent_share_amt' => $this->input->post('parent_share_amt'), 
				'description' => $this->input->post('description'), 
				//'status' => $status,
				'bh_approval' => 1,
				'created_at' => date('Y-m-d H:i:s'),
			);

			if(!empty($edit_id)) {
				
				if ($this->comman_model->update('packages', ['id'=>$edit_id], $packageData)) {

					// delete related data before edit
					$this->comman_model->delete(['package_id'=>$edit_id],'package_product');
					$this->comman_model->delete(['package_id'=>$edit_id],'package_expense');
					$this->comman_model->delete(['package_id'=>$edit_id],'package_incentive');
					$this->comman_model->delete(['package_id'=>$edit_id],'package_commission');

					
					// Insert Package Product

					$productids = $this->input->post('productid');
					$product_qty = $this->input->post('product_qty');
					$productamount = $this->input->post('productamount');
					$productactamt = $this->input->post('productactamt');
					
					$i=0;
					foreach ($productids as $prdid) {
						
						$productData = array(
							'package_id' => $edit_id, 
							'product_id' => $prdid, 
							'quantity' => $product_qty[$i], 
							'amount' => $productamount[$i], 
							'actual_amount' => $productactamt[$i], 
						);

						$this->comman_model->save("package_product",$productData);
						$i++;
					}

					// Insert Package Expense
					
					$expenseids = $this->input->post('expenseid');
					$expense_prnct = $this->input->post('expense_prnct');
					$expenseamount = $this->input->post('expenseamount');
					$category = $this->input->post('category');
					
					$i=0;

					foreach ($expenseids as $expid) {
						
						$expenseData = array(
							'expense_id' => $expid, 
							'package_id' => $edit_id, 
							'category_id' => $category[$i], 
							'percentage' => $expense_prnct[$i], 
							'amount' => $expenseamount[$i], 
						);

						$this->comman_model->save("package_expense",$expenseData);
						$i++;
					}

					// Insert Package Incentive
					
					$incentiveids = $this->input->post('incentiveid');
					$incentive_prnct = $this->input->post('incentive_prnct');
					$incentiveamount = $this->input->post('incentiveamount');
					
					$i=0;

					foreach ($incentiveids as $inctid) {
						
						$expenseData = array(
							'package_id' => $edit_id, 
							'incentive_id' => $inctid, 
							'percentage' => $incentive_prnct[$i], 
							'amount' => $incentiveamount[$i], 
						);

						$this->comman_model->save("package_incentive",$expenseData);
						$i++;
					}

					// Insert Package Commission
					
					$commissionids = $this->input->post('commissionid');
					$commission_prnct = $this->input->post('commission_prnct');
					$commissionamount = $this->input->post('commissionamount');
					
					$i=0;

					foreach ($commissionids as $comid) {
						
						$expenseData = array(
							'package_id' => $edit_id, 
							'commission_id' => $comid, 
							'percentage' => $commission_prnct[$i], 
							'amount' => $commissionamount[$i], 
						);

						$this->comman_model->save("package_commission",$expenseData);
						$i++;
					}

					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Your package updated successfully.');
					redirect('package');
				}else
				{
					$failed = true;
				}

			}else{

				$insert_id = $this->comman_model->save("packages",$packageData);

				if (!empty($insert_id) && ($insert_id > 0)) {

					// Insert Package Product

					$productids = $this->input->post('productid');
					$product_qty = $this->input->post('product_qty');
					$productamount = $this->input->post('productamount');
					$productactamt = $this->input->post('productactamt');
					
					$i=0;
					foreach ($productids as $prdid) {
						
						$productData = array(
							'package_id' => $insert_id, 
							'product_id' => $prdid, 
							'quantity' => $product_qty[$i], 
							'amount' => $productamount[$i], 
							'actual_amount' => $productactamt[$i], 
						);

						$this->comman_model->save("package_product",$productData);
						$i++;
					}

					// Insert Package Expense
					
					$expenseids = $this->input->post('expenseid');
					$expense_prnct = $this->input->post('expense_prnct');
					$expenseamount = $this->input->post('expenseamount');
					$category = $this->input->post('category');
					
					$i=0;

					foreach ($expenseids as $expid) {
						
						$expenseData = array(
							'package_id' => $insert_id, 
							'expense_id' => $expid,
							'category_id' => $category[$i], 
							'percentage' => $expense_prnct[$i], 
							'amount' => $expenseamount[$i], 
						);

						$this->comman_model->save("package_expense",$expenseData);
						$i++;
					}

					// Insert Package Incentive
					
					$incentiveids = $this->input->post('incentiveid');
					$incentive_prnct = $this->input->post('incentive_prnct');
					$incentiveamount = $this->input->post('incentiveamount');
					
					$i=0;

					foreach ($incentiveids as $inctid) {
						
						$expenseData = array(
							'package_id' => $insert_id, 
							'incentive_id' => $inctid, 
							'percentage' => $incentive_prnct[$i], 
							'amount' => $incentiveamount[$i], 
						);

						$this->comman_model->save("package_incentive",$expenseData);
						$i++;
					}

					// Insert Package Commission
					
					$commissionids = $this->input->post('commissionid');
					$commission_prnct = $this->input->post('commission_prnct');
					$commissionamount = $this->input->post('commissionamount');
					
					$i=0;

					foreach ($commissionids as $comid) {
						
						$expenseData = array(
							'package_id' => $insert_id, 
							'commission_id' => $comid, 
							'percentage' => $commission_prnct[$i], 
							'amount' => $commissionamount[$i], 
						);

						$this->comman_model->save("package_commission",$expenseData);
						$i++;
					}

					/* START - sending notification */
					

					/* creation ok */
					$this->session->set_flashdata('alert_success', "package added successfully.");
					redirect('package');

				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem creating new package. Please try again.');
				
				redirect('package');
				
			}
			
		}
		
	}

	public function get_product()
	{
		$id = $this->input->post('product_id');
		if ($id) {
			$product = $this->jcventure_model->find_product($id);
			if ($product) {
				echo json_encode(array('status' => true,'product' => $product));
			}else
			{
				echo json_encode(array('status' => false));
			}
		}
	}

	public function get_expense()
	{
		$id = $this->input->post('expense_id');
		if ($id) {
			$expense = $this->jcventure_model->find_expense($id);
			if ($expense) {
				echo json_encode(array('status' => true,'expense' => $expense));
			}else
			{
				echo json_encode(array('status' => false));
			}
		}
	}

	public function get_incentive()
	{
		$id = $this->input->post('incentive_id');
		if ($id) {
			$incentive = $this->jcventure_model->find_incentive($id);
			if ($incentive) {
				echo json_encode(array('status' => true,'incentive' => $incentive));
			}else
			{
				echo json_encode(array('status' => false));
			}
		}
	}

	public function disable($id=null)
	{
		if ($id) {
			if ($this->comman_model->update('packages', ['id'=>$id], ['status' => 2]))
			{
				$this->comman_model->update('tax', ['package_id'=>$id], ['status' => 2,'to_date' => date('Y-m-d H:i:s')]);

				$this->session->set_flashdata('alert_success', 'Your package updated successfully.');
				redirect('package');
			}
		}
	}


}
