<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class QCode extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		if(!is_admin()){
            redirect('Admin'); 
        }
		$this->load->model('qr_model');
		
	}

	/**
	 * generate random codes.
	 * 
	 * @access public
	 * @return string
	 */
	public function randomString($length, $type = '') {
		// Select which type of characters you want in your random string
		switch($type) {
		case 'num':
		// Use only numbers
		$salt = '1234567890';
		break;
		case 'lower':
		// Use only lowercase letters
		$salt = 'abcdefghijklmnopqrstuvwxyz';
		break;
		case 'upper':
		// Use only uppercase letters
		$salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		break;
		default:
		// Use uppercase, lowercase, numbers, and symbols
		$salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		break;
		}
		$rand = '';
		$i = 0;
		while ($i < $length) { // Loop until you have met the length
		$num = rand() % strlen($salt);
		$tmp = substr($salt, $num, 1);
		$rand = $rand . $tmp;
		$i++;
		}
		return $rand; // Return the random string
	}

	/**
	 * generate QR codes.
	 * 
	 * @access public
	 * @return void
	 */
	public function generate_qrcode($size = 10,$type = 'num') {

		/* create the data object */
		$data = new stdClass();

		/* set variables from the form */
		$qrcode = $this->randomString($size, $type);

		/* set validation rules */
		if($this->qr_model->check_unique_qr($qrcode)){
			$this->session->set_flashdata('alert_danger', 'Please try again.');
			redirect('qcode/generate_qrcode');
		}else{

			$this->load->library('QRcode/ciqrcode');
			
			$imgname = $qrcode.'.png';

			/* configration for QR code */
			$params['data'] = $qrcode;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'uploads/qrcodes/'.$imgname;

			$this->ciqrcode->generate($params); /* genrating QR code and saving to folder uploads/qrcodes */	
			
			/* inserting QR code record to table */
			if ($this->qr_model->create_code($qrcode, $imgname)) {
				
				/* user creation ok */
				$this->session->set_flashdata('alert_success', 'QR code created successfully.');
				
				/*REDIRECT*/
				echo '<img src="'.base_url().'uploads/qrcodes/'.$imgname.'" />';	
				
			} else {

				/* QR code creation failed, this should never happen. send error to the view */
				/* deleting recently generated QR code from folder because some error occured */
				if(file_exists('uploads/qrcodes/'.$imgname)){ 
					unlink('uploads/qrcodes/'.$imgname);
				}
				
				$this->session->set_flashdata('alert_danger', 'There was a problem creating new QR code. Please try again.');
				
				/*REDIRECT*/
			}

		}
		
			
	}


}