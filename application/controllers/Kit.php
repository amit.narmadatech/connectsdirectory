<?php  if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Kit extends CI_Controller {

    var $per_page = 10;

    public function __construct() {
        parent::__construct();
        
        if(!is_admin()){
            redirect('Admin'); 
        }
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library("pagination");
        $this->load->model('Qr_model');
    }
 
    public function kitlist() {

        $data['kitData'] = $this->db
        				->select('kit.*','user.email')
        				->from('kit')
        				->join('user', 'user.id = kit.created_by')
        				->where('kit.status', 1)
        				->order_by('created_at', 'desc')
        				->group_by('group_id')
        				->get()->result();

        $this->load->view('admin/inventory/qrcode_list', $data);
    }

	function uploadImage() {
	
        $config['upload_path'] = 'uploads/plan_img/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB
		
       if (!empty($_FILES['file']['name'])) {
	   
            $upload_path = FCPATH . "uploads/plan_img/";
			
            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }
			
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
            
			    return array('status' => false, 'msg' => $this->upload->display_errors());

            } else {
                $upload_data = $this->upload->data();
               return array('status' => true, 'file_name' => $upload_data['file_name']);
            }
        }else{
		  return false;
		}
	}
	
	public function file_check(){
	
		$config['upload_path'] = FCPATH . "uploads/plan_img/";
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB
	
		$this->load->helper('security');
		$this->load->library('upload', $config);
		
		if (!$this->upload->do_upload('file')){
		
			$error = $this->upload->display_errors();
			$this->form_validation->set_message('file',$error);
			return false;
		}   
		else{
			return true;
		}   
	}

	/**
	 * generate random codes.
	 * 
	 * @access public
	 * @return string
	 */
	public function randomString($length, $type = '') {
		// Select which type of characters you want in your random string
		switch($type) {
		case 'num':
		// Use only numbers
		$salt = '1234567890';
		break;
		case 'lower':
		// Use only lowercase letters
		$salt = 'abcdefghijklmnopqrstuvwxyz';
		break;
		case 'upper':
		// Use only uppercase letters
		$salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		break;
		default:
		// Use uppercase, lowercase, numbers, and symbols
		$salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		break;
		}
		$rand = '';
		$i = 0;
		while ($i < $length) { // Loop until you have met the length
		$num = rand() % strlen($salt);
		$tmp = substr($salt, $num, 1);
		$rand = $rand . $tmp;
		$i++;
		}
		return $rand; // Return the random string
	}

	/**
	 * generate QR codes.
	 * 
	 * @access public
	 * @return void
	 */
	public function generate_qrcode() {

		/* just post request can use this method */
		if ($this->input->server('REQUEST_METHOD') == 'POST'){

			$count    = !empty($this->input->post('quantity'))?$this->input->post('quantity'):1;
			$size    = !empty($this->input->post('size'))?$this->input->post('size'):16;
			$type    = !empty($this->input->post('type'))?$this->input->post('type'):'num';
			$copy    = !empty($this->input->post('copy'))?$this->input->post('copy'):2;
			
			/* create the data object */
			$data = new stdClass();
			$this->load->model('comman_model');	
			$last_id = $this->comman_model->getBiggestId('kit', []);
			$biggest_id = ($last_id)?$last_id[0]->id:0;
			$group_id = $biggest_id+1;

			for ($i=0; $i < $count; $i++) { 

				/* set variables from the form */
				$qrcode = $this->randomString($size, $type);

				/* set validation rules */
				if($this->Qr_model->check_unique_qr($qrcode)){
					$this->session->set_flashdata('alert_danger', 'Please try again.');
					redirect('qcode/generate_qrcode');
				}else{

					$this->load->library('QRcode/ciqrcode');
					
					$imgname = $qrcode.'.png';

					/* configration for QR code */
					$params['data'] = $qrcode;
					$params['level'] = 'H';
					$params['size'] = 10;
					$params['savename'] = FCPATH.'uploads/qrcodes/'.$imgname;

					$this->ciqrcode->generate($params); /* genrating QR code and saving to folder uploads/qrcodes */	
									
					/* inserting QR code record to table */
					$unique_id = $this->randomString(6, 'num');
					
					$user_id = $this->session->userdata('user_id');
					if ($this->Qr_model->create_code($qrcode, $qrcode.$unique_id, $imgname, $user_id, $group_id)) {
						
						/* user creation ok */
						//$this->session->set_flashdata('alert_success', 'QR code created successfully.');
						
						//redirect('kit/kitlist');
						
					} else {

						/* QR code creation failed, this should never happen. send error to the view */
						/* deleting recently generated QR code from folder because some error occured */
						if(file_exists('uploads/qrcodes/'.$imgname)){ unlink('uploads/qrcodes/'.$imgname); }
						
						/*$this->session->set_flashdata('alert_danger', 'There was a problem creating new QR code. Please try again.');
						
						redirect('kit/kitlist');*/
					}

				}
			}

			//$this->session->set_flashdata('alert_success', "Total $i QR code generated successfully.");
			$data->copy = $copy;
       		$data->qrcodes = $this->Comman_model->getAll('kit', array('group_id' => $group_id));

       		$this->load->view('admin/print_qr_template', $data);
					
			//redirect('kit/kitlist');
			
		}else{

			header("HTTP/1.1 401 Unauthorized");
    		exit;

		}
		
	}

	public function print_qrcode_by_group($group_id='')
	{
		$data = array();
		if (!empty($group_id)) {
			$data['copy'] = 2;
       		$data['qrcodes'] = $this->Comman_model->getAll('kit', array('group_id' => $group_id));

       		$this->load->view('admin/print_qr_template', $data);
		}
		return false;
	}

	public function create_printingpage($id=null) {

		if (($id != null) && $id > 0) {
		
	        $data['kitData'] = $this->db
	        				->select('kit.*','user.email')
	        				->from('kit')
	        				->join('user', 'user.id = kit.created_by')
	        				->where('kit.id', $id)
	        				->where('kit.status', 1)
	        				->get()->result();
			//print_r($data['kitData']);die;	        				
	        //return $data['kitData'];
	        $this->load->view('admin/cap_from_qrcode', $data);
		}
    }

    public function my_DOMPDF(){
	  // Load all views as normal
		$this->load->view('aboutus');
		// Get output html
		$html = $this->output->get_output();
		
		// Load library
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf");
	 }

    public function dowload_printingpage($id)
    {

    	$data['qrcode'] = $this->db->select('kit.*','user.email')
					->from('kit')
					->join('user', 'user.id = kit.created_by')
					->where('kit.id', $id)
					->where('kit.status', 1)
					->get()->result();
					
		 // Load all views as normal
	    $this->load->view('admin/cap_from_qrcode',$data);
		
		// Get output html
		$html = $this->output->get_output();
		//Load library
		
		$this->load->library('dompdf_gen');
		
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf");			
    	
    }

    public function verifykitcode()
    {
    	$barcode =  $this->input->post('code');
    	
    	if (!empty($barcode)) {
    	 $row = $this->db->where('is_used',0)->where('barcode',$barcode)->get('kit_upload')->num_rows();
    	 echo json_encode(($row > 0));die;
    	}else{
    		echo json_encode(false);die;
    	}

    }

/*    public function test()
    {
    	print_r(serialize(array(
    		'NTF_USER_NAME' => 'Customer name',
    		'NTF_QRCODE' => 'kit QR code',
    		'NTF_ORDER_NUMBER' => 'Order number',
    		'NTF_PLAN' => 'Plan names' 
    	)));
    }*/
	
	
}
