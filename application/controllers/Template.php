<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Template class.
 * 
 * @extends CI_Controller
 */
class Template extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		(!is_logged_in())?redirect('/'):'';
		$this->load->model('template_model');
		
	}

	public function test()
	{

		print_r(serialize(array(
					  'NTF_ORDER_NUMBER'=> 'Order number.',
					  'NTF_CUSTOMER_NAME'=> 'Customer name.',
					  'NTF_SHIPPING_ADDRESS'=> 'shipping address.'
					)));
	}

	/**
	 * render user profile.
	 * 
	 * @access public
	 * @return void
	 */
	public function add_notification_template()
	{	
		/* create the data object */
		$data = new stdClass();

		/* getting logged in user data and sending to profile view */
		$user_id = $this->session->user_id;
		$data->notification_list = $this->template_model->get_nf_template();
		$this->load->view('admin/notification_template', $data);

	}

	/**
	 * render user profile.
	 * 
	 * @access public
	 * @return void
	 */
	public function get_notification_token()
	{

		$template_id = $this->input->post('id');
		$template = $this->template_model->get_nf_template_by_id($template_id);
		if (count($template) > 0) {
			$token_list = unserialize($template->available_perameter);
			$result = array('status' => true,'title' => $template->title,'body' => $template->body,'data' => $token_list);	
		}else
		{
			$result = array('status' => false);	
		}
		echo json_encode($result);
        die;

	}

	/**
	 * update user profile data .
	 * 
	 * @access public
	 * @return void
	 */
	public function update_notification_template()
	{
		/* create the data object */
		$data = new stdClass();
				
		/* set validation rules */
		$this->form_validation->set_rules('type', 'Notification type', ['trim','required']);
		$this->form_validation->set_rules('title', 'Notification title', 'trim|min_length[15]');
		$this->form_validation->set_rules('body', 'Notification text', 'trim|required|min_length[30]');

		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			$this->add_notification_template();
			
		} else {
		
			/* set variables from the form */
			$template_id = $this->input->post('type');
			$upadteData = array(
				'title' => $this->input->post('title'), 
				'body' => $this->input->post('body'), 
			);
			
			if ($this->template_model->update_notification_template($template_id,$upadteData)) {
				
				/* user creation ok */
				$this->session->set_flashdata('alert_success', 'Notification Template has been updated successfully.');
				redirect('template/add_notification_template');
				
			} else {
				
				/* user creation failed, this should never happen */
				$this->session->set_flashdata('alert_danger', 'There was a problem updating your account. Please try again.');
				
				redirect('template/add_notification_template');
			}
			
		}
		
	}



}
