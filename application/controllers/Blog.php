<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    }
    public function index() {
	
       $data['title'] = 'Blog List';
	   $data['blog'] = $this->db->select('*')->from('blog')->order_by("id","desc")->get()->result();
       $this->load->view('admin/blog/blog_list', $data);
    }
    public function addblog($blog_id=""){
	
	    $data['title'] = 'Add Blog';
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
		
		if(!empty($blog_id)){
		
		 $data['blog'] = $this->db->get_where('blog',array('id' =>$blog_id))->row();
		
		}else{
		  $data['blog'] ="";
		}

        if ($this->form_validation->run() == FALSE) {
		
		    $this->load->view('admin/blog/add_blog', $data);
           
        }else {
			
	        $blogid = $this->input->post('blogid');
            
			$response = $this->uploadImage();
	       
		    $postdata['title'] = $this->input->post('title');
            $postdata['description'] = $this->input->post('description');
            $postdata['slug'] = preg_replace('/[^A-Za-z0-9-]+/', '-',$this->input->post('title'));
            
		    
            if(empty($response['error'])){
			
				if($response['status']) {
					$postdata['blog_img'] = $response['file_name'];
				}
			
                if (!empty($blogid)){
				   
                    $this->db->where('id',$blogid);
				    $this->db->update('blog',$postdata);	
				    //$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Blog updated successfully</div>');

					$this->session->set_flashdata('alert_success', 'Blog updated successfully.');
                
				}else{
				
				   $id = $this->Comman_model->save('blog', $postdata);
				   if($id){
				    
            $this->session->set_flashdata('alert_success', 'Blog created successfully.');
            //$this->session->set_flashdata('alert_success', '<div class="alert alert-success text-center">Blog created successfully</div>');

					}
			       
                }
                 redirect(site_url('blog/index'));
				 
            }else{
		       $this->session->set_flashdata('alert_danger', '<div class="alert alert-danger text-center">'.$response['error'].'500*600</div>');
               redirect(site_url('blog/addblog/' .$blogid)); 
           }
        }
    }

    function uploadImage() {       /* this function working upload images */
	
        $config['upload_path'] = 'uploads/blog/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '10210'; //1 MB
		
       if (!empty($_FILES['file']['name'])) {
	   
            $upload_path = FCPATH . "uploads/blog/";
			
            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }
			
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
            
			    return array('status' => false, 'error' => $this->upload->display_errors());

            } else {
                $upload_data = $this->upload->data();
               return array('status' => true,'error'=>'','file_name' => $upload_data['file_name']);
            }
        }else{
		  return false;
		}
	}
    
    public function changeBlogStatus($blogid='') {
	
        $res = $this->Comman_model->getRow('blog',array('id'=>$blogid));
        if (!empty($blogid) && !empty($res)) {
              if($res->status){
                  $status =0;
              }else{
                  $status =1; 
              }
              $this->Comman_model->update('blog',array('id'=>$blogid),array('status'=>$status));
              $this->session->set_flashdata('alert_success', 'Blog status change successfully');
        }
         redirect('blog/index');
    }
    
    public function deleteblog($blog_id=''){
	
        $res = $this->Comman_model->getRow('blog',array('id'=>$blog_id));
        if (!empty($blog_id) && !empty($res)) {
           $this->Comman_model->delete(array('id'=>$blog_id),'blog');
           $this->session->set_flashdata('alert_success', 'Blog deleted successfully');
        }
        redirect('blog/index');
    }
	public function blogcomment(){
	
	    $data['title'] = 'Blog Comment';
	 	$this->db->select('blog_comments.*,blog.title');
		$this->db->from('blog_comments');
		$this->db->join('blog', 'blog.id = blog_comments.blog_id');
		//$this->db->join('user', 'user.id = blog_comments.user_id');
		$this->db->order_by('id','DESC'); 
		$data['blogcomment'] = $this->db->get()->result();
                $this->load->view('admin/blog/blog_comment_list', $data);	
	}
	 public function changeBlogCommentStatus($blogcommentid='') {
	
        $res = $this->Comman_model->getRow('blog_comments',array('id'=>$blogcommentid));
        if (!empty($blogcommentid) && !empty($res)) {
              if($res->status){
                  $status =0;
              }else{
                  $status =1; 
              }
              $this->Comman_model->update('blog_comments',array('id'=>$blogcommentid),array('status'=>$status));
              $this->session->set_flashdata('alert_success', 'Blog Comment status change successfully');
        }
         redirect('blog/blogcomment');
    }
    
    public function deleteBlogComment($blogcommentid=''){
	
        $res = $this->Comman_model->getRow('blog_comments',array('id'=>$blogcommentid));
        if (!empty($blogcommentid) && !empty($res)) {
           $this->Comman_model->delete(array('id'=>$blogcommentid),'blog_comments');
           $this->session->set_flashdata('alert_success', 'Blog Comment deleted successfully');
        }
        redirect('blog/blogcomment');
    }
    
}
