<?php

defined('BASEPATH')OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('form_validation', 'email');
        $this->load->model('user_model');
        $this->load->model('Comman_model');
        $this->load->model('template_model');
        //load google login library
        $this->load->library('google');
        //load facebook login library
        $this->load->library('facebook');
    }

    public function signup() {
     
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fname', 'first name', ['trim', 'required', 'alpha', 'min_length[2]', 'max_length[16]']);
        $this->form_validation->set_rules('lname', 'last name', ['trim', 'required', 'alpha', 'min_length[2]', 'max_length[16]']);
        $this->form_validation->set_rules('mobile', 'mobile number', ['trim', 'required', 'numeric', 'min_length[10]', 'max_length[13]']);
        $this->form_validation->set_rules('email', 'email', ['trim', 'required', 'valid_email', 'is_unique[user.email]']);
        $this->form_validation->set_rules('password', 'password', ['trim', 'required', 'min_length[6]', 'max_length[15]']);
        $this->form_validation->set_rules('repassword', 'Confirm Password', ['trim', 'required', 'min_length[6]', 'max_length[15]', 'matches[password]']);
        $this->form_validation->set_message('is_unique', 'The %s is already taken');
      //  $this->form_validation->set_message('consent', ['trim', 'required']);

        $arr = "";
        $firstname = $this->input->post('fname');
        $lastname = $this->input->post('lname');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
      //  $consent = $this->input->post('consent');

        if ($this->form_validation->run() == false) {

            $errors = array();

            foreach ($this->input->post() as $key => $value) {
                // Add the error message for this field
                $errors[$key] = form_error($key);
            }
            $response['errors'] = array_filter($errors); // Some might be empty
            $response['status'] = FALSE;
            echo json_encode($response);
            die;
        } else {

            $data = array('firstname' => $firstname,
                'lastname' => $lastname,
                'contact1' => $mobile,
                'email' => $email,
                "type" => 4,
              //  "disclaimer" =>$consent,
                'password' => md5($password),);

            $insertedid = $this->Comman_model->save('user', $data);

            if ($insertedid) {

                /* START - sending notification */
                $this->load->model('notification_model');

                $user = $this->Comman_model->getRow('user', ['id' => $insertedid]);

                /* load a template */
                $tmpdata = array(
                    'NTF_FIRSTNAME' => $user->firstname,
                    'NTF_LASTNAME' => $user->lastname,
                    'NTF_EMAIL' => $user->email,
                    'NTF_CREATED_AT' => date("D, d-M-Y h:i A", strtotime($user->created_at)),
                );

                $templateData = get_notification(1, $tmpdata);

                /* sending notification to customer */
                $this->notification_model->add(array(
                    'sender_id' => 0,
                    'type' => 'customer_notification',
                    'label' => 'customer_created',
                    'title' => $templateData['title'],
                    'body' => $templateData['body'],
                    'href' => '',
                    'recipient_id' => $insertedid,
                ));

                /* load a template */
                $tmpdata1 = array(
                    'NTF_FIRSTNAME' => $user->firstname,
                    'NTF_LASTNAME' => $user->lastname,
                    'NTF_EMAIL' => $user->email,
                    'NTF_CREATED_AT' => date("D, d-M-Y h:i A", strtotime($user->created_at)),
                );

                $templateData1 = get_notification(2, $tmpdata1);

                /* sending notification to admin */
                $this->notification_model->add(array(
                    'sender_id' => 0,
                    'type' => 'admin_broadcast',
                    'label' => 'customer_created',
                    'title' => $templateData1['title'],
                    'body' => $templateData1['body'],
                    'href' => '',
                    'recipient_id' => 0,
                ));

                // END - sending notification 
                // sending email notification 
                $subject = "Successful sign up at JC Genetics";
                $msg = "This is to inform that you have been successfully registered and logged in at  " . base_url('/') . ". Thank you for showing interest in our services and decided to lead a healthy lifestyle.";

                $message = $this->load->view('email_template/email', '', true);
                $message = str_replace("{username}", ucfirst($firstname), $message);
                $message = str_replace("{message}", $msg, $message);
                $this->Comman_model->emailsend($email, $subject, $message);

                // Sending sms

                if (!empty($mobile) && (strlen($mobile) == 10)) {
                    $sms = New Sms();
                    $message = "Dear $firstname, Welcome onboard.\nYou are successfully registered at " . base_url('/') . ".\nWe look forward to providing you our best services always.\n\nThanks,\nJC Genetics Team";
                    $sms->sendsms($mobile, $message);
                }

                $arr = array('status' => true,
                    'id' => $insertedid,
                    'msg' => 'your registration has been successfully! please wait a moment'
                );
            } else {

                $arr = array('status' => false,
                    'error' => 'Some error occurs please try agian');
            }
            echo json_encode($arr);
            die;
        }
    }

    public function login() {
        $data = "";
        $redirect_url = "";
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $redirect_url = $this->input->post('redirect_url');


        if ($email == "" || $password == "") {
            $data = array("status" => false, "msg" => "email or password can not be empty", "error" => array('password' => 'email or password can not be empty'));
        } else {

            $post_data = array('email' => $email, 'password' => md5($password), 'type' => 4);
            $result = $this->Comman_model->getRow('user', $post_data);
            if (!empty($result)) {
                $userData = array(
                    'user_id' => (int) $result->id,
                    'logged_in' => (bool) true,
                    'fullname' => (string) $result->firstname . ' ' . $result->lastname,
                    'image' => (string) $result->image,
                    "email" => (string) $result->email,
                    "type" => $result->type,
                );

                $this->session->set_userdata($userData);

                $session_id = $this->session->userdata('session_id');

                $where = array('session_id' => $session_id, 'user_id' => NULL);
                $cart = $this->db->select('*')->from('cart')->where($where)->get()->result();

                if (!empty($cart)) {

                    foreach ($cart as $carts) {

                        $checkcart = $this->db->select('*')->from('cart')->where(array('user_id' => $result->id, 'plan_id' => $carts->plan_id))->get()->row();

                        if (!empty($checkcart)) {

                            $this->db->where(array('user_id' => $result->id, 'cart_id' => $checkcart->cart_id, 'plan_id' => $checkcart->plan_id,));
                            $this->db->update('cart', array('qty' => ($checkcart->qty + 1)));

                            if ($this->db->affected_rows() >= 0) {

                                $this->db->where(array('cart_id' => $carts->cart_id, 'plan_id' => $carts->plan_id, 'session_id' => $session_id));
                                $this->db->delete('cart');
                            }
                        } else {

                            $cartdata = array('user_id' => $result->id, 'session_id' => '');
                            $this->db->where($where);
                            $this->db->update('cart', $cartdata);
                        }
                    }
                }
                $data = array("status" => true, "msg" => "Login successfully.Please wait a moment.", 'datas' => $userData, 'redirect_url' => $redirect_url);
            } else {

                $data = array("status" => false, "msg" => "Email and password do not match", "error" => array('password' => 'email or password can not be empty'));
            }
        }
        echo json_encode($data);
        die;
    }

    public function checkEmailExists() {
        $email = $this->input->post('email');
        $userdata = $this->Comman_model->getRow('user', array('email' => $email));

        if (!empty($userdata)) {
            echo 'false';
        } else {
            echo 'true';
        }
        die;
    }

    public function email_exists() {
        $this->Comman_model->email_exists();
    }

    function logout() {
        $user_id = $this->session->userdata('userId');
        $this->Comman_model->update('users', array('id' => $user_id), array('is_login' => 0));
        $this->session->sess_destroy();
        redirect(site_url('/'));
    }

    public function varification() {
        $temp_pass = $this->input->get('token');
        $usertoken = $this->input->get('usertoken');
        $result = $this->Comman_model->getRow('user', array('email' => base64_decode($usertoken), 'temp_pass' => $temp_pass));
        if ($result) {
            $result = $this->Comman_model->update('user', array('email' => base64_decode($usertoken), 'temp_pass' => $temp_pass), array('status' => 1, 'temp_pass' => ''));
            redirect(base_url() . 'home');
        }
    }

    public function emailtest() {

        $MyData['insertedid'] = 1;
        $MyData['email'] = "amit.argalon@gmail.com";
        $MyData['randomPassword'] = $this->Comman_model->randomPassword();
        $message = $this->load->view('email_template', $MyData, true);
        $subject = "Confirm email address account activition";
        echo $this->Comman_model->sendmail("amit.argalon@gmail.com", $subject, $message);
    }

    public function resetpassword($token = '') {
        $data = array();
        if (!empty($token)) {
            $row = $this->Comman_model->getRow('users', array('temp_str' => $token));
            if (!empty($row)) {
                $data = array('error' => false, 'token' => $token);
            } else {
                $data = array('error' => true, 'message' => 'Invalid token');
            }
        } else {
            $data = array('error' => true, 'message' => 'Empty token');
        }
        $this->load->view('resetpassword', $data);
    }

    public function saveresetpassword() {
        $password = $this->input->post('password');
        $confirmpassword = $this->input->post('confirmpassword');
        $token = $this->input->post('token');
        $data = array();
        if (!empty($password) && !empty($confirmpassword) && !empty($token)) {
            if ($password == $confirmpassword) {
                $row = $this->Comman_model->getRow('users', array('temp_str' => $token));
                if (!empty($row)) {
                    $this->db->update('users', ['password' => md5($password), 'temp_str' => ''], ['id' => $row->id]);
                    $data = ['status' => true, 'msg' => 'Password reset successfully.'];
                } else {
                    $data = ['status' => false, 'msg' => 'Invalid token.'];
                }
            } else {
                $data = ['status' => false, 'msg' => 'Password and confirm password are not match.'];
            }
        } else {
            $data = ['status' => false, 'msg' => 'Required fields are empty'];
        }
        echo json_encode($data);
    }

    function sendemail($email, $subject, $message) {
        $this->load->library('email');
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $this->email->initialize($config);
        $this->email->from('info@myfitcom.es');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function loginGoogle() {
        //redirect to profile page if user already logged in
        if ($this->session->userdata('logged_in') == true) {
            redirect('/');
        }

        if (isset($_GET['code'])) {    //authenticate user
            $this->google->getAuthenticate();
            $gpInfo = $this->google->getUserInfo(); //get user info from google
            //preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid'] = $gpInfo['id'];
            $userData['firstname'] = $gpInfo['given_name'];
            $userData['lastname'] = $gpInfo['family_name'];
            $userData['email'] = $gpInfo['email'];
            $userData['type'] = 4;

            //insert or update user data to the database
            $this->load->model('user_model');
            $user = $this->user_model->checkUser($userData);

            $userData = array('user_id' => (int) $user->id,
                'logged_in' => (bool) true,
                'fullname' => (string) $userData['firstname'] . ' ' . $userData['lastname'],
                'image' => (string) $user->image,
                "email" => (string) $userData['email'],
                "type" => $userData['type'],);

            //store status & user info in session
            $userid = $this->session->userdata('user_id');

            if (empty($userid)) {

                $session_id = $this->session->userdata('session_id');

                $where = array('session_id' => $session_id, 'user_id' => NULL);
                $cart = $this->db->select('*')->from('cart')->where($where)->get()->result();

                if (!empty($cart)) {

                    foreach ($cart as $carts) {

                        $checkcart = $this->db->select('*')->from('cart')->where(array('user_id' => $user->id, 'plan_id' => $carts->plan_id))->get()->row();

                        if (!empty($checkcart)) {

                            $this->db->where(array('user_id' => $user->id, 'cart_id' => $checkcart->cart_id, 'plan_id' => $checkcart->plan_id,));
                            $this->db->update('cart', array('qty' => ($checkcart->qty + 1)));

                            if ($this->db->affected_rows() >= 0) {

                                $this->db->where(array('cart_id' => $carts->cart_id, 'plan_id' => $carts->plan_id, 'session_id' => $session_id));
                                $this->db->delete('cart');
                            }
                        } else {

                            $cartdata = array('user_id' => $user->id, 'session_id' => '');
                            $this->db->where($where);
                            $this->db->update('cart', $cartdata);
                        }
                    }
                }
            }
            $this->session->set_userdata($userData);
            //redirect to profile page
            redirect('/');
        }

        //google login url
        $data['GoogleloginURL'] = $this->google->loginURL();
        redirect('/');
    }

    public function loginFacebook() {

        $userData = array();

        // Check if user is logged in
        if ($this->facebook->is_authenticated()) {
            echo "test";

            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            print_r($userProfile);
            die;

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['firstname'] = $userProfile['first_name'];
            $userData['lastname'] = $userProfile['last_name'];
            $userData['email'] = !empty($userProfile['email']) ? $userProfile['email'] : '';
            $userData['type'] = 4;

            // Insert or update user data
            $user = $this->user_model->checkUser($userData);

            // Check user data insert or update status
            if (!empty($user)) {
                $userData = array(
                    'user_id' => (int) $user->id,
                    'logged_in' => (bool) true,
                    'fullname' => (string) $userData['firstname'] . ' ' . $userData['lastname'],
                    'image' => (string) $user->image,
                    "email" => (string) $userData['email'],
                    "type" => $userData['type'],
                );

                $data['userData'] = $userData;
                //store status & user info in session

                $userid = $this->session->userdata('user_id');

                if (empty($userid)) {

                    $session_id = $this->session->userdata('session_id');

                    $where = array('session_id' => $session_id, 'user_id' => NULL);
                    $cart = $this->db->select('*')->from('cart')->where($where)->get()->result();

                    if (!empty($cart)) {

                        foreach ($cart as $carts) {

                            $checkcart = $this->db->select('*')->from('cart')->where(array('user_id' => $user->id, 'plan_id' => $carts->plan_id))->get()->row();

                            if (!empty($checkcart)) {

                                $this->db->where(array('user_id' => $user->id, 'cart_id' => $checkcart->cart_id, 'plan_id' => $checkcart->plan_id,));
                                $this->db->update('cart', array('qty' => ($checkcart->qty + 1)));

                                if ($this->db->affected_rows() >= 0) {

                                    $this->db->where(array('cart_id' => $carts->cart_id, 'plan_id' => $carts->plan_id, 'session_id' => $session_id));
                                    $this->db->delete('cart');
                                }
                            } else {

                                $cartdata = array('user_id' => $user->id, 'session_id' => '');
                                $this->db->where($where);
                                $this->db->update('cart', $cartdata);
                            }
                        }
                    }
                }

                $this->session->set_userdata($userData);
                redirect('/');
            } else {
                $data['userData'] = array();

                redirect('/');
            }
        } else {

            //google login url
            $data['facebookAuthUrl'] = $this->facebook->login_url();
        }
        redirect('/');
    }

    /**
     * render login page.
     * 
     * @access public
     * @return void
     */
    public function get_login() {

        redirect(base_url('/'));
        die;
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        $this->load->view('include/header');
        $this->load->view('user/login/login', $data);
    }

    public function register() {
        /* $name = "vikas kanik";
          $string = "My name is $name";
          echo $string;die; */

        $this->load->view('include/header');
        $this->load->view('user/register/register');
    }

    /**
     * insert/create new users.
     * 
     * @access public
     * @return void
     */
    public function create_user() {

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */

        $this->form_validation->set_rules('firstname', 'First Name', ['trim', 'required', 'alpha', 'min_length[2]', 'max_length[16]']);
        $this->form_validation->set_rules('lastname', 'Last Name', ['trim', 'required', 'alpha', 'min_length[2]', 'max_length[16]']);
        $this->form_validation->set_rules('email', 'Email', ['trim', 'required', 'valid_email', 'is_unique[user.email]']);
        $this->form_validation->set_rules('password', 'Password', ['trim', 'required', 'min_length[8]', 'max_length[16]']);
        $this->form_validation->set_rules('password_confirm', 'Confirm Password', ['trim', 'required', 'min_length[8]', 'max_length[16]', 'matches[password]']);
        $this->form_validation->set_message('is_unique', 'The %s is already taken');
        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->register();
        } else {

            /* set variables from the form */
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $insert_id = $this->user_model->create_user($email, $password, $firstname, $lastname);
            if (!empty($insert_id) && ($insert_id > 0)) {

                /* START - sending notification */
                $this->load->model('notification_model');

                $user = $this->Comman_model->getRow('user', ['id' => $insert_id]);

                /* load a template */
                $tmpdata = array(
                    'NTF_FIRSTNAME' => $user->firstname,
                    'NTF_LASTNAME' => $user->lastname,
                    'NTF_EMAIL' => $user->email,
                    'NTF_CREATED_AT' => date("D, d-M-Y h:i A", strtotime($user->created_at)),
                );

                $templateData = get_notification(1, $tmpdata);

                /* sending notification to customer */
                $this->notification_model->add(array(
                    'sender_id' => 0,
                    'type' => 'customer_notification',
                    'label' => 'customer_created',
                    'title' => $templateData['title'],
                    'body' => $templateData['body'],
                    'href' => '',
                    'recipient_id' => $insert_id,
                ));

                /* load a template */
                $tmpdata = array(
                    'NTF_FIRSTNAME' => 'vikas',
                    'NTF_LASTNAME' => 'kanik',
                    'NTF_EMAIL' => 'vikas@gmail.com',
                    'NTF_CREATED_AT' => 'created_at',
                );
                $templateData = get_notification(1, $tmpdata);

                /* sending notification to admin */
                $this->notification_model->add(array(
                    'sender_id' => 0,
                    'type' => 'admin_broadcast',
                    'label' => 'customer_created',
                    'title' => $templateData['title'],
                    'body' => $templateData['body'],
                    'href' => '',
                    'recipient_id' => 0,
                ));

                /* user creation ok */
                $this->session->set_flashdata('alert_success', "Thank you $firstname. You have successfully registered. Please login to continue.");
                redirect('login');
            } else {

                /* user creation failed, this should never happen. send error to the view */

                $this->session->set_flashdata('alert_danger', 'There was a problem creating your new account. Please try again.');

                redirect('login');
            }
        }
    }

    /**     * login function. * @access public * @return void	 */
    public function post_login() {

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */
        $this->form_validation->set_rules('username', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {

            /* validation not ok, send validation errors to the view */
            $this->get_login();
        } else {

            /* set variables from the form */
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            if ($this->user_model->resolve_user_login($username, $password)) {

                $user_id = $this->user_model->get_user_id_from_username($username);
                $user = $this->user_model->get_user($user_id);

                $userData = array(
                    'user_id' => (int) $user->id,
                    'username' => (string) $user->username,
                    'logged_in' => (bool) true,
                    'fullname' => (string) $user->firstname . ' ' . $user->lastname,
                    'image' => (string) $user->image,
                );

                $this->session->set_userdata($userData);

                /* user login ok redirect('user/index') */
                redirect('/');
            } else {

                /* login failed */
                $this->session->set_flashdata('alert_danger', 'Wrong username or password.');

                /* send error to the view */
                $this->get_login();
            }
        }
    }

    public function forgotPassword() {
        $data = "";
        $email = $this->input->post('forgotemail');
        if ($email == "") {
            $data = array("status" => false, "msg" => "Email can not be empty");
        } else {
            $result = $this->Comman_model->getRow('user', array('email' => $email));
            if (!empty($result)) {

                $token = $this->Comman_model->generateRandomString(10);
                $this->Comman_model->update('user', array('email' => $email), array('token' => $token));
                $resetPasswordLink = base_url('register/recoverpassword/') . base64_encode($email) . '?token=' . $token;

                $message = $this->load->view('email_template/forgot_password', '', true);
                $message = str_replace("{username}", ucfirst($result->firstname), $message);
                $message = str_replace("{resetPasswordLink}", $resetPasswordLink, $message);
                $subject = "Forgot Password Request";
                $this->Comman_model->emailsend($email, $subject, $message);


                $data = array("status" => true, "msg" => "Email sent successfully on your registered email id .Please check email for further instructions");
            } else {
                $data = array("status" => false, "msg" => "This email does not exists.");
            }
        }
        echo json_encode($data);
        die;
    }

    public function recoverpassword($email = "") {

        $token = $this->input->get('token');
        $email = base64_decode($email);
        $result = $this->Comman_model->getRow('user', array('token' => $token, 'email' => $email));
        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        if (($this->input->server('REQUEST_METHOD') == 'GET') && (!isset($result->token))) {

            $this->session->set_flashdata('error', 'This url has been expired');
            redirect(base_url() . 'home/error');
        }

        $data['user_id'] = $result->id;
        $data['email'] = $result->email;


        $this->form_validation->set_rules('password', 'password', 'required|trim|matches[cpassword]');
        $this->form_validation->set_rules('cpassword', 'password confirmation', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('reset-password', $data);
        } else {

            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            $user_id = base64_decode($this->input->post('user_id'));

            $this->db->where('id', $user_id);
            $this->db->update('user', array('password' => md5($password), 'token' => ''));
            $this->session->set_flashdata('success', 'Your password changed successfully');
            $this->load->view('reset-password', $data);
            // redirect(base_url().'home/successmessage');
        }
    }

    public function postrecoverpassword() {

        $email = base64_decode($this->input->post('email'));
        $user_id = base64_decode($this->input->post('user_id'));

        $result = $this->Comman_model->getRow('user', array('id' => $user_id));

        $this->load->library('google');
        $data['GoogleloginURL'] = $this->google->loginURL();

        $this->load->library('facebook');
        $data['facebookAuthUrl'] = $this->facebook->login_url();

        if (($this->input->server('REQUEST_METHOD') == 'GET') && (!$result->token)) {

            $this->session->set_flashdata('error', 'This url has been expired');
            redirect(base_url() . 'home/error');
        }

        $data['user_id'] = $result->id;
        $data['email'] = $result->email;


        $this->form_validation->set_rules('password', 'password', 'required|trim|matches[cpassword]');
        $this->form_validation->set_rules('cpassword', 'password confirmation', 'required|trim');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('reset-password', $data);
        } else {

            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            $user_id = base64_decode($this->input->post('user_id'));

            $this->db->where('id', $user_id);
            $this->db->update('user', array('password' => md5($password), 'token' => ''));
            $this->session->set_flashdata('success', 'Your password changed successfully. Click <a href="'.base_url("/").'" >here</a> to login.');
            $this->load->view('reset-password', $data);
            // redirect(base_url().'home/successmessage');
        }
    }

}
