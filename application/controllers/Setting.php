<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Setting class.
 * 
 * @extends CI_Controller
 */
class Setting extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        if (!is_admin()) {
            redirect('Admin');
        }
        if (!check_permission()) {
            $this->access_denied();
        }
        $this->load->model('comman_model');
    }

    /**
     * render admin setting.
     * 
     * @access public
     * @return void
     */
    public function index() {

        /* create the data object */
        $data = new stdClass();

        $key_list = $this->db->select('key,value')->order_by('key')->get('setting')->result();
        $this->load->helper('array');
        $setting_key = [];
        $setting_value = [];

        foreach ($key_list as $key => $value) {
            array_push($setting_key, $value->key);
            array_push($setting_value, $value->value);
        }

        $data->key_list = array_combine($setting_key, $setting_value);
        $this->load->view('admin/add_setting', $data);
    }

    /**
     * render user dashboard.
     * 
     * @access public
     * @return void
     */
    public function get() {

        if ($this->checkAuth()) {
            /* create the data object */
            $data = new stdClass();

            $data->page = 'user/dashboard';
            $this->load->view('include/user_layout', compact('data'));
        } else {
            redirect('login');
        }
    }

    public function tax($id=null)
	{
		$data = new stdClass();

        //$data->state_list  = $this->comman_model->getAll('states', ['country_id'=>101,'id'=>21]);
		$this->load->model('Jcventure_model', 'jcventure');
        $data->state_list  = $this->jcventure->getStateById(22);
		$data->package_list  = $this->comman_model->getAll('packages', []);

		$data->tax_list  = $this->db->from('tax')
							->join('packages','packages.id = tax.package_id')
							->select('tax.*,tax.id as taxid,packages.title as package_title')
							->get()->result();


		//print_r($data->tax_list);die;
		$this->load->view('admin/add_tax', $data);
	}

    public function post_tax() {
		
		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');

		/* set validation rules */

		$this->form_validation->set_rules('state_id', 'state', 'trim|required');
		$this->form_validation->set_rules('package_id', 'package', 'trim|required');
		$this->form_validation->set_rules('from_date', 'start date', 'trim|required');
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			$this->tax();  
			
		} else {
	
			/* set variables from the form */
			$insertData = array(
				'package_id' => $this->input->post('package_id'), 
				'state_id' => $this->input->post('state_id'), 
				'sgst_tax' => $this->input->post('sgst_tax'), 
				'cgst_tax' => $this->input->post('cgst_tax'), 
				'igst_tax' => $this->input->post('igst_tax'),
				'from_date' => $this->input->post('from_date')
			);

			if(!empty($edit_id)) {
				
				if ($this->comman_model->update('tax',['id'=>$edit_id],$insertData)) {
				
					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Tax updated successfully.');
					redirect('setting/tax');
				}else
				{
					$failed = true;
				}

			}else{

				if ($this->comman_model->save('tax', $insertData)) {
				
					/* creation ok */
					$this->session->set_flashdata('alert_success', 'Tax added successfully.');
					redirect('setting/tax');	
				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem in adding tax. Please try again.');
				
				redirect('setting/tax');
				
			}
			
		}
		
	}

    /**
     * set setting data .
     * 
     * @access public
     * @return void
     */
    public function set() {

        $all = $this->input->post();
        //print_r($all);die;
        $columns = array('site_title', 'tag_line', 'site_url', 'site_email', 'google_client_id', 'google_client_secret', 'google_redirect_uri', 'facebook_app_id', 'facebook_app_secret', 'facebook_redirect_url');

        /* create the data object */
        $data = new stdClass();

        /* set validation rules */
        if (array_key_exists('site_title', $all)) {
            $this->form_validation->set_rules('site_title', 'Site Title', 'trim|required');
        }

        if (array_key_exists('tag_line', $all)) {
            $this->form_validation->set_rules('tag_line', 'Tag Line', 'trim|required');
        }

        if (array_key_exists('site_url', $all)) {
            $this->form_validation->set_rules('site_url', 'Site URL', 'trim|required');
        }

        if (array_key_exists('google_client_id', $all)) {
            $this->form_validation->set_rules('google_client_id', 'Google Client ID', 'trim|required');
        }

        if (array_key_exists('google_client_secret', $all)) {
            $this->form_validation->set_rules('google_client_secret', 'Google Client Secret', 'trim|required');
        }

        if (array_key_exists('google_redirect_uri', $all)) {
            $this->form_validation->set_rules('google_redirect_uri', 'Google Redirect URL', 'trim|required');
        }

        if (array_key_exists('facebook_app_id', $all)) {
            $this->form_validation->set_rules('facebook_app_id', 'Facebook App ID', 'trim|required');
        }

        if (array_key_exists('facebook_app_secret', $all)) {
            $this->form_validation->set_rules('facebook_app_secret', 'Facebook App Secret', 'trim|required');
        }

        if (array_key_exists('facebook_redirect_url', $all)) {
            $this->form_validation->set_rules('facebook_redirect_url', 'Facebook Redirect URL', 'trim|required');
        }


        if ($this->form_validation->run() === false) {

            /* validation not ok, send validation errors to the view */
            $this->index();
        } else {

            $this->db->trans_start(); # Starting Transaction
            $this->db->trans_strict(FALSE);

            # Updating data
            foreach ($all as $key => $value) {
                if (in_array($key, $columns)) {
                    $this->db->where('key', $key);
                    $this->db->update('setting', array('value' => $value));
                }
            }

            $this->db->trans_complete(); # Completing transaction

            /* Optional */

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();

                /* setting creation failed, this should never happen */
                $this->session->set_flashdata('alert_danger', 'There was a problem in creating setting key. Please try again.');

                $this->index();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();

                /* setting creation failed, this should never happen */
                $this->session->set_flashdata('alert_success', 'Setting key has been updated successfully.');

                $this->index();
            }
        }
    }

    public function access_denied() {
        $this->load->view('admin/include/error_403');
    }

    public function privacypolicy() {
        $this->load->view('privacypolicy');
    }

    public function emailtmpl($id = '') {
        $data['email'] = $this->Admin_model->get_email_by_id($id);
        $data['emails'] = $this->Admin_model->get_all_emails();
        $data['title'] = 'Edit Email Text';
        $this->load->view('admin/email_view', $data);
    }

    public function updateemail() {
        $email = array();
        $email['subject'] = $this->input->post('subject');
        $email['body'] = $this->input->post('body');
        $email['avl_vars'] = $this->input->post('avl_vars');
        $data['values'] = json_encode($email);
        $data['status'] = 1;
        $id = $this->input->post('id');
        $this->Admin_model->update_email_tmpl($data, $id);
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data updated successfully</div>');
        redirect(site_url('admindashboard/emailtmpl/' . $id));
    }

    public function contactus() { /* function for contactus list */
        $id = $this->session->userdata('user_id');
        $data['contactus'] = $this->db->select('*')->from('contact_us')->order_by('id','DESC')->get()->result();
        $this->load->view('admin/contact_us_list', $data);
    }
    public function deletecontactus($contactus_id = "") { /* function for delete contactus list */

        $res = $this->Comman_model->getRow('contact_us', array('id' => $contactus_id));
        if (!empty($contactus_id) && !empty($res)) {

            $this->db->where(array('id' => $contactus_id));
            $this->db->delete('contact_us');
            $this->session->set_flashdata('alert_success', 'Contact-us deleted from list successfully');
        }
        redirect(site_url('setting/contactus'));
    }
     public function partnerlist() { /* function for partner list */
        $data['partners'] = 'Partner List';
        $data['partners'] = 'Partner List';
        $id = $this->session->userdata('user_id');
        $this->db->select('partners.id,partners.district_id,mobile,subject,message,email,date,CONCAT(firstname,'.', lastname) AS name');
        $this->db->from('partners');
        $this->db->order_by('partners.id','DESC');
        $data['partners'] =  $this->db->get()->result();
        $this->load->view('admin/partner_list', $data);
    }
    
     public function deletepartner($partners_id = "") { /* function for delete contactus list */

        $res = $this->Comman_model->getRow('partners', array('id' => $partners_id));
        if (!empty($partners_id) && !empty($res)) {

            $this->db->where(array('id' => $partners_id));
            $this->db->delete('partners');
          //  $this->session->set_flashdata('success', 'Record deleted successfully');
          $this->session->set_flashdata('alert_success', 'Record deleted successfully.');
        }
        redirect(site_url('setting/partnerlist'));
    }

    public function testimoniallist() { /* this function working show testimonial list  */

        $data['testimonialData'] = $this->db->get('testimonial')->result();
        $this->load->view('admin/testimonial_list', $data);
    }

    public function testimonial($testimonialid = "") { /* this function working add/edit testimonial */

        $user_id = $this->session->userdata('user_id');
        $plan_id = $this->input->post('plan_id');

        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('summary', 'summary', 'required');
        $this->form_validation->set_message('title', 'title', 'Please enter plan name');
        $this->form_validation->set_message('price', 'price', 'enter price');
        $this->form_validation->set_message('description', 'description', 'enter plan description');
        $this->form_validation->set_message('summary', 'summary', 'enter plan sort description');

        if (empty($plan_id) && !empty($_FILES['file']['name'])) {

            $this->form_validation->set_rules('file', '', 'callback_file_check');
        }

        if (!empty($planid)) {
            $data['plandata'] = $this->Comman_model->getRow('plan', array('id' => $planid));
        } else {
            $data['plandata'] = "";
        }

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/add_testimonial', $data);
        } else {

            $title = $this->input->post('title');
            $price = $this->input->post('price');
            $description = $this->input->post('description');
            $sort_summary = $this->input->post('summary');
            $plan_type = $this->input->post('plan_type');

            $response = $this->uploadImage();

            $post_data = array('name' => $title,
                'description' => $description,
                'sort_description' => $sort_summary,
                'created_by' => $user_id,
                'plan_type' => isset($plan_type) ? $plan_type : 0,
                'price' => $price);


            if ($response['status']) {
                $post_data['plan_image'] = $response['file_name'];
            }


            $plandata = $this->Comman_model->getRow('testimonial', array('id' => $plan_id));

            if (!empty($plandata)) {

                $this->Comman_model->update('testimonial', array('id' => $plan_id), $post_data);
                $this->session->set_flashdata('success', 'Testimonial updated successfully');
            } else {

                $this->db->insert('testimonial', $post_data);
                $this->session->set_flashdata('success', 'Testimonial save successfully');
            }
            redirect(site_url('setting/testimoniallist'));
        }
    }

    public function changeTestimonialStatus($testimonialid = '') { /* this function working change Testimonial status active/inactive  */

        $res = $this->Comman_model->getRow('testimonial', array('id' => $testimonialid));

        if (!empty($testimonialid) && !empty($res)) {

            if ($res->status) {
                $status = 0;
            } else {
                $status = 1;
            }
            $this->Comman_model->update('testimonial', array('id' => $testimonialid), array('status' => $status));
            $this->session->set_flashdata('success', 'Testimonial status change successfully');
        }
        redirect('setting/testimoniallist');
    }

}
