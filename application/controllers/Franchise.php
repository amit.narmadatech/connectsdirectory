<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Franchise class.
 * 
 * @extends CI_Controller
 */
class Franchise extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		/* helper function to check user logged in or not */
		if(!is_admin()){
            redirect('Admin'); 
        }

		//$this->load->model('franchise_model');
		$this->load->model('comman_model');
		
	}

	/**
	 * render inventory dashboard.
	 * 
	 * @access public
	 * @return void
	 */
	public function dashboard() {

		/* create the data object */
		$data = new stdClass();

		$this->load->view('admin/franchise/franchise_dashboard', $data);		
	}
		
	/**
	 * render franchise list .
	 * 
	 * @access public
	 * @return void
	 */
	public function franchise_list() {

		/* create the data object */
		$data = new stdClass();
		$data->franchise_list  = $this->comman_model->getAll('franchise', []);
		$this->load->view('admin/franchise/franchise_list', $data);		
	}

	/**
	 * render add franchise form .
	 * 
	 * @access public
	 * @return void
	 */
	public function add_franchise($id = null) {

		/* create the data object */
		$data = new stdClass();
		
		/* if edit  */
		if (($id != null) && ($id > 0)) {
			$data->edit = $this->comman_model->getRow('franchise', ['id'=>$id]);
		}

		$data->state_list  = $this->comman_model->getAll('states', ['country_id'=>101]);
		$this->load->view('admin/franchise/add_franchise', $data);		
	}


	/**
	 * insert/update franchise info.
	 * 
	 * @access public
	 * @return void
	 */
	public function post_franchise_form() {
		
		/* create the data object */
		$data = new stdClass();
		$edit_id = $this->input->post('edit_id');

		/* set validation rules */

		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('contact1', 'mobile number ', 'required|regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
		$this->form_validation->set_rules('contact2', 'mobile number ', 'regex_match[/^[0-9]{10}$/]|max_length[10]|min_length[10]');
		//$this->form_validation->set_rules('country_id', 'Country', 'trim|required');
		$this->form_validation->set_rules('state_id', 'State', 'trim|required');
		$this->form_validation->set_rules('city_id', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'address ', 'min_length[15]');
		$this->form_validation->set_rules('pincode', 'pincode ', 'required');
		
		if ($this->form_validation->run() === false) {
			
			/* validation not ok, send validation errors to the view */
			$this->add_franchise();
			
		} else {

			$status = $this->input->post('status');
			$status = (empty($status))?0:1;
			
			/* set variables from the form */
			$insertData = array(
				'title' => $this->input->post('title'), 
				'name' => $this->input->post('name'), 
				'email' => $this->input->post('email'), 
				'password' => md5($this->input->post('password')), 
				'contact1' => $this->input->post('contact1'), 
				'contact2' => $this->input->post('contact2'), 
				'country_id' => '101', 
				'state_id' => $this->input->post('state_id'), 
				'city_id' => $this->input->post('city_id'), 
				'address' => $this->input->post('address'), 
				'pincode' => $this->input->post('pincode'), 
				'description' => $this->input->post('description'), 
				'created_by' => $this->session->user_id, 
				'status' => $status, 
				
			);

			if(!empty($edit_id)) {
				
				if ($this->comman_model->update('franchise',['id'=>$edit_id],$insertData)) {
				
					/* updation ok */
					$this->session->set_flashdata('alert_success', 'Franchise updated successfully.');
					redirect('franchise/franchise_list');
				}else
				{
					$failed = true;
				}

			}else{

				if ($this->comman_model->save('franchise', $insertData)) {
				
					/* creation ok */
					$this->session->set_flashdata('alert_success', 'Franchise added successfully.');
					redirect('franchise/franchise_list');	
				}else
				{
					$failed = true;
				}

			}
			
			if (isset($failed)) {
				
				/* creation failed, this should never happen. send error to the view */
				
				$this->session->set_flashdata('alert_danger', 'There was a problem in creating franchise. Please try again.');
				
				redirect('franchise/add_franchise');
				
			}
			
		}
		
	}
	

	/**
	 * user's profile image upload function .
	 * 
	 * @access public
	 * @return void
	 */
	function image_upload()
	{
			$upload_dir = 'uploads/userimages/';
			if (!is_dir($upload_dir)) {
			     mkdir($upload_dir);
			}	

			$config['upload_path']   = $upload_dir;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['file_name']     = 'userimage_'.substr(md5(rand()),0,7);
			$config['overwrite']     = false;
			$config['max_size']	 = '5120';

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('userimage')){

				/* image upload errors */
				$this->session->set_flashdata('error', $this->upload->display_errors());

				$this->profile();
				return false;	
			}	
			else{
				$this->upload_data['userimage'] =  $this->upload->data();
				return true;
			}	
	}
	
/**
	 * user's notification .
	 * 
	 * @access public
	 * @return void
	 */

	public function notification() {
		
			/* create the data object */
			$data = new stdClass();

			$user_id = $this->session->user_id;

			$this->load->model('notification_model');
			$data->notification_list =  $this->notification_model->customer_notification_list($user_id);
			//print_r($data->notification_list);die;
			$data->page  = 'user/notification';
			$this->load->view('include/user_layout', compact('data'));
	}
}
