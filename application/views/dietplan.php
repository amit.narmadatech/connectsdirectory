<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>
    Diet Plan
  </title>
  <style type="text/css">
      @page {
        
              /* 'em' 'ex' and % are not allowed; length values are width height */
        margin: 0%; /* <any of the usual CSS values for margins> */
background-color: #E5F6D6; /*(% of page-box width for LR, of height for TB) */
    }
  </style>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,700,800,900" rel="stylesheet"> 
</head>
<body style="margin: 0;">
    
    <table  cellpadding="0" cellspacing="0" style=" background-color:#fff;width: 794px; height: 1245px; border-collapse: collapse; margin:0 auto;">        
        <tr>
             <td>
             	<table style="margin:45px; height: 1132px; background: #E5F6D6; width: 100%; ">
             		<tr>
             			<td style="padding-top: 105px; text-align: center;letter-spacing: 15px;  font-family: 'Montserrat'; font-weight: 500; color: #5e6063; ">
             				GENOTYPED DIET
             			</td>
             		</tr>
             		<tr>
             			<td style="padding-top: 200px; text-align: center; color: #3c3d3a; font-size: 25px; font-family: 'Montserrat'; font-weight: 900;">
             			<b>Your genetic guide to</b>
             		   </td>
             		</tr>
             		<tr>
             			<td style="padding-top: 2px; font-size: 45px; text-align: center; color: #77876a; letter-spacing: 5px; font-family: 'Montserrat'; font-weight: 500;">
             				<b>GENOTYPED DIET</b>
             			</td>
             		</tr>
             		<tr>
                        <td style="padding-top: 35px; font-size: 18px; text-align: center; color: #5e6063; letter-spacing: 5px; font-family: 'Montserrat'; font-weight: 500;">
                            <b><?php echo empty($diteplanData['name'])?'':ucwords($diteplanData['name']); ?></b>
                        </td>
                    </tr>
                    <tr>
             			<td style="padding-top: 0px; font-size: 18px; text-align: center; color: #5e6063; letter-spacing: 15px; font-family: 'Montserrat'; font-weight: 500;">
             				<?php echo empty($diteplanData['barcode'])?'':$diteplanData['barcode']; ?>
             			</td>
             		</tr>
             		<tr>
             			<td style="padding-top: 450px; padding-bottom: 100px;  text-align: center;">
             				<img src="<?php echo FCPATH.'assets/logo.jpg'; ?>">
             			</td>
             		</tr>
             	</table>
             </td>   	
        </tr>
    </table>

    <table cellpadding="0" cellspacing="0" style="width: 794px; border-collapse: collapse; margin:0 auto; background: #E4F6D5;">
    	<tr>
    		<td style="vertical-align: top; padding: 10px 45px;">
                <h4 style=" font-family: 'Montserrat';    font-weight: 500;    color: #77876a;
    font-size: 30px;    margin-bottom: 20px;"><br><b>Recommended For</b></h4><br>
    			<ul>
    				<!-- <li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat';">
    					patient is highly likely to be gluten and lactose intolerant.</li>
    				<li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat'; ">less likely to gain weight with saturated fats, higly likely to gain weith with mufa, less to pufa. macro nutrients needs- vit c, vit a, vit b6 and 12, vit e and d.</li>
    				<li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat'; ">and has risk of heart disease.</li>
    				<li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat'; ">diet is formulated on genotyped data obtained by exome sequencing data and genetic interpritations provided by jcgenetics.
    				</li> -->
                    <?php echo empty($diteplanData['indication'])?'':$diteplanData['indication']; ?>
    			</ul>
    		</td>
    	</tr>
    	<tr>
    		<td style="padding: 45px; vertical-align: top;">
                <h4 style=" font-family: 'Montserrat';    font-weight: 500;    color: #77876a;
    font-size: 30px;    margin-bottom: 20px;" align="center"><center><b>Diet Plan</b></center></h4>
    			<table border="1" cellpadding="0" cellspacing="0"  style="border:solid 1px #000; border-collapse: collapse;width: 794px;">
    				<tr>
    					<th width="15%" style="font-family: 'Montserrat'; font-size: 14px; font-weight: 500; padding: 15px 10px; width: 100px; color: #3c3d3a;background: #6EAD46;"><b>NO OF MEALS TOTAL</b> </th>
    					<th width="15%" style="font-family: 'Montserrat'; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #6EAD46;"><b>MEAL TIMINGS</b></th>
    					<th width="40%" style="font-family: 'Montserrat'; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #6EAD46;"><b>PRIORITIES</b></th>
    					<th width="30%" style="font-family: 'Montserrat'; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #6EAD46;"><b>TIPS</b></th>
    				</tr>
                    <?php if (!empty($diet_plan)) {
                        foreach ($diet_plan as $ditep) { ?>
    				    

                    <tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;"><b><?php echo empty($ditep['meal'])?'':$ditep['meal']; ?></b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px; text-align: center"><?php echo empty($ditep['timing'])?'':$ditep['timing']; ?></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;"><?php echo empty($ditep['proitites'])?'':$ditep['proitites']; ?></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;"><?php echo empty($ditep['tips'])?'':$ditep['tips']; ?></td>
    				</tr>
                            
                    <?php    }
                    } ?>

    				<!-- <tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;background: #A7D08C;"><b>MEAL : 2</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;text-align: center;background: #A7D08C;">8:30 </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;">one apple or pear salad with red sea salt and peeper with almonds,  walnuts and mint leaves. 5 mediterrenian resins  half scoop of isolate protien </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;">finely chopped apples - green or red or pear. add soaked almond or powdered almond with hint of lemon.</td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;"><b>MEAL : 3</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;text-align: center;">10:30 Post Workout</td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;"> one bowl papaya  one scoop isolate  protien or micronized whey</td>
    					<td></td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;background: #A7D08C;"><b>MEAL : 4</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;text-align: center;background: #A7D08C;">11:30 </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;">quinoa khichri/ idli/dosa/moong chilla  </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;">add greated paneer with hint of  garam masala and redchilly powder for taste</td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;"><b>MEAL : 5</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;text-align: center;">2:00</td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;">panner bhurji with peas, white or brownrice pulao with veggies with one bowl of cucumber and mint in hung curd and with one bowl, chickpeas, black gram or lobia alternate days.</td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;">gram or beans to be given altenate days.<br>drink lemmonade within half an hour</td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;background: #A7D08C;"><b>MEAL : 6</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;text-align: center;background: #A7D08C;">5:00</td>
                        <td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;">protien – and papaya 100 gms day -1 and – 2day two pomegranate juice</td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;background: #A7D08C;"></td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;"><b>MEAL : 7</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;text-align: center;">7:00</td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;">three whole bolied eggs with one bowl rice and vegetable soup of your choice. </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px;padding: 5px;">tamato basil, beet root tamato, lauki tamato </td>
    				</tr>
    				<tr>
    					<td style="font-family: 'Montserrat'; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;background: #A7D08C;"> <b>MEAL : 8</b></td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;text-align: center;background: #A7D08C;">9:30</td>
                        <td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;background: #A7D08C;">chena and two dates  </td>
    					<td style="font-family: 'Montserrat'; font-size: 12px; padding: 5px;background: #A7D08C;"></td>
    				</tr> -->
    			</table>
    		</td>
    	</tr>
    	<tr>
    		<td style="padding:5px 45px; vertical-align: top; font-family: 'Montserrat'; font-size: 14px;"><?php echo empty($diteplanData['note'])?'':$diteplanData['note']; ?></td>
        </tr>

			<tr>
            <td style="padding:5px 45px 45px; vertical-align: top; font-family: 'Montserrat'; font-size: 14px;">Happy Dieting.<br>
			Best Wishes.</td>
    	</tr>
    </table>
    
</body>
</html>