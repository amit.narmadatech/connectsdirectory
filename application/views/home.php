<?php  $this->load->view('include/header')?> 
<!-- Slider -->
<div class="tp-banner-container">
  <div class="tp-banner" >
    <ul>  
      <?php 
        if(!empty($planData)){
        foreach($planData as $planDatas){
        if (file_exists("assets/images/slides/".$planDatas->id.".jpg")){ $href = base_url('assets/images/slides/').$planDatas->id.".jpg"; }else{ $href= base_url('assets/images/slides/__1.jpg'); }  
                ?>
	<li data-transition="boxfade" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
        <img src="<?php echo $href; ?>" alt="<?php echo !empty($planDatas->name) ? $planDatas->name : '';?>" />
        <div class="tp-caption sft flat-title-slider style1" data-x="15" data-y="250" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">
          <?php echo !empty($planDatas->name) ? $planDatas->name : '';?></div>
        <?php if (!empty($planDatas->tagline)) { ?>
        <div class="tp-caption sfl flat-content-slider style1" data-x="15" data-y="340" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><i class="fa fa-caret-right" data-unicode="f0da"></i> <?php echo !empty($planDatas->tagline) ? $planDatas->tagline : '';?> </div>  
        <?php } ?>
        <?php if (!empty($planDatas->sort_description)) { ?>
		<div class="tp-caption sfl flat-content-slider content-2 style1" data-x="15" data-y="380" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><i class="fa fa-caret-right" data-unicode="f0da"></i> <?php echo !empty($planDatas->sort_description) ? $planDatas->sort_description : '';?> </div>
        <?php } ?>
        <div class="tp-caption sfl flat-button" data-x="15" data-y="430" data-speed="1000" data-start="2500" data-easing="Power3.easeInOut"><a href="<?php  echo base_url('plandetail/'.$planDatas->slug); ?>">Buy Now <i class="material-icons">&#xE5CC;</i></a></div>
        <div class="tp-caption flat-scroll-btn animated bounce" data-x="585" data-y="990"><i class="icon-mouse icons"></i></div>
      </li>
	<?php } } ?>
    </ul>
  </div>
</div>
    <!-- Entry Page -->
<div class="entry-page">
   <!-- About -->
  <section id="about" class="flat-row row-about">
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-5">
          <div class="flat-divider d6px"></div>
            <div class="singleimages-border">
              <img src="<?php echo base_url();?>assets/images/about.jpg" alt="images">
            </div>
          </div><!-- /.col-md-5 -->
          <div class="col-md-7 col-sm-7">
            <div class="about-wrap padding-left61px">
              <div class="title-box">
              <h4 class="sub-title">Welcome To JC Genetics</h4>
              <h2 class="main-title">A unique plan for the unique you. </h2>

          </div><!-- /.title-box -->
          <p>Your body is made of multiple cells and all the genetic information of body cells are stored in 23 pairs of chromosomes. And these chromosomes are made of DNA. Explore your set of chromosomes today.</p>
          
            <a class="flat-button" href="#contact">Contact us 
            <i class="material-icons">&#xE8D3;</i></a>
          </div><!-- /.about-wrap -->
        </div><!-- /.col-md-7 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section> 
            <!-- Our Advantages -->
  <section class="flat-row row-advantage">
    <div class="container">
      <div class="title-section">
        <h1 class="title" data-text="A">What DNA Can Tell You </h1>
      </div><!-- /.title-section -->
      <div class="row">
        <div class="col-md-6">  
          <div class="row">  
            <div class="flat-tabs advantage bg-white-after">
              <ul class="menu-tab">
                <li class="active"><a href="#" data-item="1" class="activecls">I</a></li>
                <li><a href="#" data-item="2" class="activecls">II</a></li>
                <li><a href="#" data-item="3" class="activecls">III</a></li>
                <li><a href="#" data-item="4" class="activecls">IV</a></li>
                <li><a href="#" data-item="5" class="activecls">V</a></li> 
              </ul><!-- /.menu-tab -->

              <div class="content-tab">
                <div class="content-inner">
                  <div class="title-content">Nutrition strategy</div>
                    <p> Understand your genes to know about sensitivity, body intolerance, food metabolism and nutrient information. Get the best nutritional diet strategy today. </p>
                    <!-- <a class="flat-button" href="#">Read More  <i class="material-icons"> &#xE5CC;</i></a> -->
                </div>

                <div class="content-inner">
                  <div class="title-content">Personalized medication</div>
                    <p> DNA holds the key to understand the way of processing certain medications. Embrace nutrigenomics by understanding your genes and personalize your prescription drugs.</p>
                    <!--  <a class="flat-button" href="#">Read More<i class="material-icons"> &#xE5CC;</i></a> -->
                </div>

                <div class="content-inner">
                  <div class="title-content">Ancestry knowledge edge </div>
                    <p> Explore by unveiling the history of your DNA, ancestral origins, global ancestry percentages, and connect with your DNA family. Reinvent the way you see your ancestry. </p>
                    <!-- <a class="flat-button" href="#">Read More<i class="material-icons"> &#xE5CC;</i></a> -->
                </div>  

                <div class="content-inner">
                  <div class="title-content">Skin health</div>
                    <p> Skin health and radiance originate deep inside and significantly influenced by your genetics. Get all the benefits of radiant and anti-aging skin diets personalized for your genetic makeup.  </p>
                    <!-- <a class="flat-button" href="#">Read More  <i class="material-icons"> &#xE5CC;</i></a> -->
                  </div>

                  <div class="content-inner">
                    <div class="title-content">Future health </div>
                      <p> Your genes induce major diseases in your body. Assess your risk of developing health conditions and eliminate those risks by taking proactive actions. </p>
                                    <!-- <a class="flat-button" href="#">Read More  <i class="material-icons"> &#xE5CC;</i></a> -->
                  </div>                         
                </div><!-- /.content-tab -->
              </div><!-- /.flat-tabs -->
            </div>
          </div><!-- /.col-md-6 -->
        <div class="col-md-6 flat-bg-white">
          <div class="row">
            <img src="<?php echo base_url();?>assets/images/advantage.jpg" id="activeimg_1" class="activeimg" alt="images" style="border: 4px solid #E85222; padding: 15px;" />
            <img src="<?php echo base_url();?>assets/images/advantage1.jpg" id="activeimg_2" class="activeimg" alt="images" style="border: 4px solid #E85222; padding: 15px;display: none;"/>
            <img src="<?php echo base_url();?>assets/images/advantage2.jpg" id="activeimg_3"  class="activeimg" alt="images" style="border: 4px solid #E85222; padding: 15px;display: none;"/>
            <img src="<?php echo base_url();?>assets/images/advantage3.jpg" id="activeimg_4" class="activeimg" alt="images" style="border: 4px solid #E85222; padding: 15px;display: none;"/>
            <img src="<?php echo base_url();?>assets/images/advantage4.jpg" id="activeimg_5" class="activeimg" alt="images" style="border: 4px solid #E85222; padding: 15px;display: none;"/>
          </div>
        </div>
      </div><!-- /.row -->
    </div><!-- /.container -->
    <div class="clearfix"></div>
  </section>

 <!-- Our team -->
  <section id="doctor" class="flat-row row-our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="flat-tabs member">
            <!-- /.menu-tab -->
            <div class="content-tab">
              <div class="content-inner">
                <div class="flat-team style1">
                  <div class="avatar">
                    <div class="singleimages-border">
                        <img src="<?php echo base_url();?>assets/images/1.jpg" alt="image" style="border: 4px solid #E85222; padding: 15px; background: #fff;">
                    </div>
                  </div>                        
                  <div class="content">
                    <h3 class="name">Your future health is just a click away </h3>
                      <p class="position"></p> 
                      <p>We are dedicated to empower wellness with genetics information to impact health positively and improve the quality of life. We will be glad to provide personalized health solutions and make your life healthier. </p>
                  </div> 
                </div><!-- /.flat-team -->
              </div>
            </div><!-- /.content-tab -->
          </div><!-- /.flat-tabs -->
        </div><!-- /.col-md-12-->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section>
            <!-- Our pricing -->
  <section id="pricing" class="flat-row row-pricing">
    <div class="container">                
      <div class="row">
        <div class="col-md-12">
          <div class="title-section desc">
            <h1 class="title" data-text="P">Our <span>Plans</span></h1>
          </div><!-- /.title-section -->
          <div class="flat-pricing-table">
            <?php
              if (!empty($planlist)) {
                foreach ($planlist as $planDataval) {
                if (file_exists("uploads/plan_img/" . $planDataval->plan_image) && !empty($planDataval->plan_image)) {

                  $img = base_url() . 'uploads/plan_img/' . $planDataval->plan_image;
                } else {
                  $img = base_url() . 'assets/images/img_not_available.jpg';
                }
              ?>
              <div class="price-item">
                <div class="pricing-table"> 
                    <?php if ($planDataval->is_discount) { ?>
           			<div class="ribbon"><span><?php echo round($planDataval->discount_prnct); ?>% off</span></div>	
                      <?php } ?>
                  <div class="img">
                        <?php if (($planDataval->is_discount)) { ?>   
                           <a href="javascript:void(0);">
                         <?php }elseif($planDataval->direct_cart){?>
                               <a href="javascript:void(0);">       
                        <?php }else{ ?>
                        <a  href="<?php echo base_url('plandetail/'.$planDataval->slug); ?>"> 
                         <?php } ?>
                          <img src="<?php echo $img; ?>" alt="<?php echo!empty($planDataval->name) ? $planDataval->name : ''; ?>">
                       </a>
                  </div>
                  <div class="wrap-plan">
                      <span class="plan"> 
                        <?php if (($planDataval->is_discount)) { ?>   
                           <a href="javascript:void(0);">
                         <?php }elseif($planDataval->direct_cart){?>
                               <a href="javascript:void(0);">       
                        <?php }else{ ?>
                           <a  href="<?php echo base_url('plandetail/'.$planDataval->slug); ?>">
                         <?php } ?>
                                <?php echo!empty($planDataval->name) ? $planDataval->name : ''; ?>
                          </a>
                      </span> 
                  </div>
                  <div class="description"><?php echo!empty($planDataval->tagline) ? $planDataval->tagline : ''; ?></div>
                    <div class="price">
                      <span class="symbol"><i class="fa fa-inr"></i></span> <span class="price"><?php echo!empty($planDataval->price) ? round($planDataval->price) : ''; ?></span>
                      <?php if ($planDataval->is_discount) { ?>
                        <span class="oldprice"><strong><i class="fa fa-inr"></i> <?php echo round($planDataval->clubbed_amt); ?></strong></span>
                        <?php } ?>
                    </div>                               
                    <div class="cta">
                        
                     <?php if (($planDataval->is_discount) || ($planDataval->direct_cart)) { ?>
                        <form name="addtocart" method="POST" action="<?php echo base_url('cart/addtocart'); ?>">
                            <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                            <input type="hidden" name="planid" value="<?php echo!empty($planDataval->id) ? $planDataval->id : ''; ?>">
                            <button type="submit" class="button">Add to cart</button>
                        </form>
                    <?php }else{ ?>
                         <a class="button" href="<?php echo base_url('plandetail/'.$planDataval->slug); ?>">get started</a>
                     <?php } ?>
                    
                    </div>
                  </div>
                </div>									
              <?php }
            } ?>
          </div>            
        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </section>

  <!-- Appointment -->
  <section id="contact" class="flat-row row-appointment nopad">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="flat-divider d100px"></div>
            <div class="title-section">
              <h1 class="title" data-text="T">Get In <span>Touch</span></h1>
            </div><!-- /.title-section -->

          <div class="flat-appointment">
            <form id="appointmentform" class="appointment-form contact-form for-full-width" method="post">
            <div class="note"></div> 
            <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
            <div class="input-wrap wrap-left fullname">
              <input type="text" value="" tabindex="3" placeholder="Name *" name="name" id="fullname" required>
            </div>
            <div class="input-wrap wrap-right email">
              <input type="text" value="" tabindex="4" placeholder="Email *" name="email" required>
            </div>
            <div class="input-wrap wrap-left telephone">
              <input type="text" value="" tabindex="5" placeholder="Phone *" maxlength="10"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="telephone" id="telephone" required>
            </div>
            <!-- <div class="input-wrap wrap-right subject">
              <input type="text" value="" tabindex="5" placeholder="Subject *" name="subject" id="subject-1" required>
            </div>
 -->		       <div class="textarea-wrap message">
              <textarea class="" tabindex="6" placeholder="Your Message" name="message" id="message" required></textarea>
            </div>
						<div class="alert alert-success text-center" id="conatactusNotitfy" style="display:none" ></div>
                <div class="send-wrap">
                   <button class="flat-button border">Submit <i class="material-icons">send</i></button>
                </div>
            </form><!-- /.appointment-form -->
          </div>
        </div><!-- /.col-md-6-->

        <div class="col-md-6 col-sm-6 wrap-testimonials">  
           <div class="flat-testimonials" data-item="1" data-nav="true" data-dots="false" data-auto="true">
             <div class="flat-testimonials-single"> 
                <blockquote class="whisper"> Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</blockquote>
                <div class="avatar">                            
                   <div class="name">Amit Roy</div>
                </div>
              </div>
              
              <div class="flat-testimonials-single"> 
                <blockquote class="whisper"> Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended. </blockquote>
                <div class="avatar">                            
                  <div class="name">Ritesh Jain</div>
                </div>
              </div>
              <div class="flat-testimonials-single">  
                <blockquote class="whisper">  JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.  </blockquote>
               <div class="avatar">   
                <div class="name">Asha Verma</div>
              </div>
            </div>
          </div><!-- /.flat-testimonials-->
        </div><!-- /.col-md-6-->
      </div><!-- /.row-->
    </div><!-- /.container-fluid -->
  </section>            
  <!-- Contact -->
</div><!-- /.Entry Page --> 

<?php  $this->load->view('include/footer')?> 
<script>	
	$(document).ready(function(){
		
		$('.menu-tab>li>a').click(function(event){
		      $(this).parent().addClass('active').siblings().removeClass('active');
			  $('.activeimg').hide();
			  $('#activeimg_'+$(this).attr('data-item')).show();
		});
	});	
    $("#appointmentform").validate({	
	  /*  errorPlacement: function(error, element) {
			if(element.attr("name") == "email"){
					error.appendTo('#erroremail');
					return;
			}else if(element.attr("name") == "name"){
					error.appendTo('#errorname');
					return;
			}else if(element.attr("name") == "subject"){
					error.appendTo('#errorsubject');
					return;
			}else if(element.attr("name") == "message"){
					error.appendTo('#errormesssage');
					return;
			}else {
				error.insertAfter(element);
			}
		}, */	
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            //subject: "required",
            phone: {
                required: true,
                minlength: 10,
                digits: true
            }
          //  message: "required"
        },
        submitHandler: function () {
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>home/contactus',
                data: $("#appointmentform").serialize(),
                success: function (respose) {
                    console.log(respose);
                    var obj = JSON.parse(respose);
                    if (obj.status) {
			 $('#appointmentform')[0].reset();
                        $('#conatactusNotitfy').html(obj.msg).show().delay(10000).fadeOut();
                    } else {
                        $('#conatactusNotitfy').html(obj.msg).show().delay(10000).fadeOut();
                    }
                }
            });
	    },
        messages: {
            name: "Please enter name",
            email: {
                required: "Please enter email address",
                email: "Please enter a valid email address"
            },
            subject: "Please enter subject",
            phone: {
                required: "Please enter phone number",
            },
            message: "Please enter your message"
        }
    });
</script>