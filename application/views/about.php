<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>JC GENETICS - Towards a better life</title>      
<meta name="description" content="JC GENETICS - Towards a better life">
<meta name="keywords" content=" JC GENETICS - Towards a better life">
<meta name="author" content="www.jcgenetics.in ">     
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Bootstrap  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/bootstrap.css">
<!-- Theme Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/style.css">
<!-- Custom css -->
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/responsive.css">
<!-- Colors -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/colors/color1.css" id="colors">  
<!-- Animation Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/animate.css">
<!-- Favicon and touch icons  -->
<link href="<?php echo base_url();?>assets/icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
<link href="<?php echo base_url();?>assets/icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
<link href="<?php echo base_url();?>assets/icon/favicon.png" rel="shortcut icon">
<!--[if lt IE 9]>     
<script src="<?php echo base_url();?>assets/javascript/html5shiv.js"></script>      
<script src="<?php echo base_url();?>assets/javascript/respond.min.js"></script>    
<![endif]-->        
</head>
<body class="page-template-front-page header-sticky home"> 
<section id="join_us_section">
  <div class="banneroverlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 area_1">
        <h1 class="const_title">About Us</h1>    
      </div>
    </div>
  </div>
</section>
<section id="about-page">
  <div class="container">
        <div class="row">         
            <div class="col-md-12 col-sm-12">
            <h1 class="tagline">JC Genetics - <small> The science of knowing your self.</small></h1>
            <p style="font-style: italic;"><i> "Believe! truth, once found, would be simple as well as pretty."</i></p>

            -James Watson
          
            <br><br>
              
            <p>JC Genetics India is a Private Limited company (CIN: U74999MP2018PTC045167) operating from its head office situated at Indore, MP, India</p>
          
            <p>Our team is taking lead in addressing wellness services with the goal of making nation healthy and disease free with the help of non-invasive salivary DNA testing.</p>
          
            <p>We  specialize in personalized preventive healthcare. We provide a perfect blend of state of art genetic testing, artificial intelligence and human expertise for your better well-being. We are dedicated to empowering physicians, wellness professionals and individuals with the most validated, accurate and actionable genomic information, which in turn positively impacts and improves human health and quality of life.</p>             

            <p>All embedded in our Genes waiting to be uncovered. Every cell in our body contains DNA which can reveal everything about your past present and future. Let’s understand our differences.</p>
           
            <p class="prop"> <strong>Our speciality- </strong> <?php if (!empty($planList)) { foreach ($planList as $plan) { ?><span><?php echo $plan->name; ?></span><?php } }  ?></p>           
    
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/bootstrap.min.js"></script> </body>
</html>