<?php  $this->load->view('include/header')?> 
 <?php $userData = $this->session->all_userdata();?> 
<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 area_1">
                <h2 class="const_title"> Plan Detail</h2>
            </div>
        </div>
    </div>
</section>

<section class="plans">
    <div class="container">
     <form name="addtocart" method="POST" action="<?php echo base_url('cart/addtocart');?>">
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
            <input type="hidden"  name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
             <div class="row">
                    <div class="col-md-6 col-sm-6">
                     <?php
                    if (file_exists("uploads/plan_img/".$plandetails->plan_image) && !empty($plandetails->plan_image)){

                       $img = base_url().'uploads/plan_img/'.$plandetails->plan_image;
                     }else{
                       $img = base_url().'assets/images/diet-plan.jpg';
                     }
                            ?>
                    <div class="plandetail-img">
                            <img src="<?php echo $img;?>" class="img-responsive">
                     </div>
                    <input type="hidden" name="planid" value="<?php echo !empty($plandetails->id)? $plandetails->id : '';?>" readonly>
            </div>
                 <div class="col-md-6 col-sm-6">
                     <h2><?php echo!empty($plandetails->name) ? $plandetails->name : ''; ?></h2>			
                     <h5><?php echo!empty($plandetails->sort_description) ? $plandetails->sort_description : ''; ?></h5>
                     <p><?php echo!empty($plandetails->description) ? $plandetails->description : ''; ?></p>
                     <p class="t2_style editContent">Price: <strong><i class="fa fa-inr"></i> <?php echo!empty($plandetails->price) ? $plandetails->price : ''; ?> /-</strong></p>

                     <button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>			
                 </div>
            </div>
            </form>
    </div>
</section>

<section id="testimonials"> 
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <img src="<?= base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
                <!--Carousel Wrapper-->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <p class="quote-title editContent pix_text">
                                Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>

                            <div class="quote-text editContent pix_text">
                                Amit Roy
                            </div>         
                            <div class="quote-dots">. . .</div>        
                        </div>
                        <div class="item">     
                            <p class="quote-title editContent pix_text">
                                Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>

                            <div class="quote-text editContent pix_text">
                                Ritesh Jain
                            </div>         
                            <div class="quote-dots">. . .</div>  
                        </div>  
                        <div class="item">     
                            <p class="quote-title editContent pix_text">
                                JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>

                            <div class="quote-text editContent pix_text">
                                Asha Verma
                            </div>         
                            <div class="quote-dots">. . .</div>  
                        </div> 
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<?php  $this->load->view('include/footer')?> 
