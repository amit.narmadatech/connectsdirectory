<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		 <div class="row">
 			<div class="col-md-12 area_1">
 				<h2 class="const_title"> Plan Detail</h2>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	 <form name="addtocart">
 		 <div class="row">
 			<div class="col-md-6 col-sm-6">
	 		<h2>Skin Genetics</h2>	
 	 	   <p>We care about your skin health. The radiance and youthfulness of skin originates deep inside in the body. Know about your genes before spending on elaborate skin care regimens. Understand the real you with us.</p>     		
 	 	<div class="plan-pricing">
 	 	<div class="row">
 	 	  <div class="col-sm-8">
			<h6>Skin Genetics DNA kit   </h6>        
		  	  Requires saliva sample for DNA analysis
		  </div>
		  <div class="col-sm-4 text-right">
			<p class="t2_style editContent">Price: <strong>&#x20B9; 25,000</strong></p>
		  </div>
		</div>    	
			<button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
 	 	</div>
 	 </div>

	   <div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service1.png" class="item-icon">
			    
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			            Best Skin Regime			          
			            <p> We read your DNA to share best skin regimen for you. </p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Best skin results</li>
			        <li>No side effects </li>
			        <li>DNA based skin care </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           Personalized Skin Solutions
			          <p>Follow your genetic profile and be beautiful.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Radiant and youthful skin </li>
			        <li>Personal skin care routine </li>
			        <li>Gene guide to healthy skin</li>			      
			       </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			            Way to Healthy Skin
			           <p>Get skin care guidance from our team of genetic counsellors.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Valuable recommendations </li>
			        <li> Measures to align skin care with your genes </li>
			        <li> Right choice for glowing skin </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 	</form>
   </div>
</section>

<section class="plans-content">
 	<div class="container">
 	  <div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 			  <h4>Reveal best of your skin</h4>
 			  <p>We use your saliva sample to understand your DNA, learn about your skin health and recommend best skin regimen for you. Let your genes decide your pathway to radiant skin. </p>
 			  <h4>Beauty recommendations</h4>
 			  <p>Your genes decode the way to be beautiful. Our dedicated team of genetic counselors help you understand your skin health and recommend best beauty regime. Make a difference in your life today.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			  <img src="<?php echo base_url('assets/'); ?>images/skin-banner.png" class="img-responsive center-block">
 	 		</div>
 		</div>
 	  </div>
    </div>
 </section>

 <section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">Make right choice for radiant and youthful skin.</h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Rosacea</h5>
				<p>Exposure to high temperatures and sunlight makes your skin extremely red, prone to pimpled and completely flushed. Skin is likely to be affected by rosacea in this case. We help you take appropriate measures by your gene assessment.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Dry Skin </h5>
				<p>	Dry and cold season makes your skin dry, dull and rough. This test can help you get insights about the prevention of dry skin.  </p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 			    <h5>Deficiency of Vitamin A </h5>
				<p>	Are you facing skin damage premature aging problems? Meeting the required level of Vitamin A protects your skin.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 
 				<h5>Folate deficiency</h5>
				<p>Hyperpigmentation is a very common problem and it occurs due to associated genes in your body. Get away from those dark marks with personalized diet.	</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Contact dermatitis</h5>
				<p>	Don’t you want your skin free from itching blisters, redness, scaly skin. Any substances like cosmetics, jewelry, plants can cause such reactions. Test your genetic profile for dermatitis and follow your skin gene diet.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Generalized psoriasis</h5>
			  <p>This is a common skin disorder and characterized by plaques of thickened, scaly skin. It is more prevalent in men and genetics plays a major role for this skin problem. Ascertain your risks with genetic profile test. </p>
			</div>
		</div> 	

 		<div class="collapse" id="collapseExample">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			 <h5>Vitamin B6 deficiency</h5>
				<p>	Common symptoms of Vitamin B6 deficiency are cracked lips, eruption, ulcers. Ensure your skin protection by matching the required vitamin levels.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Vitamin C deficiency</h5>
				<p>	Avoid bleeding gums and slow skin healing by meeting the required level of Vitamin C and ensure healthy skin and gums.</p>
			</div>
		</div> 		
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Vitamin E deficiency</h5>
				<p>	Deficiency of this vitamin results in pigmentation, dryness and dermatitis. Protect your skin from these problems by understanding your genes.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Tanning response </h5>
				<p>Slow tanning response means deficiency of vitamin D and fast tanning response results in high risks of sunburn, wrinkles, sun spots, melanoma and folate loss.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			 <h5>Skin anti-oxidation capacity</h5>
				<p>	There is always an internal protection of your skin against pollutants, UV rays, toxins. And it is derived from the anti-oxidation capacity of your skin. against UV rays, pollutants and toxins that is conferred by antioxidants.</p>
			</div>
			<div class="col-md-6 col-sm-6">
			<h5>Freckles </h5>
			<p>	Understand your gene predisposition to reduce the genetically inherited skin problems.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Collagen degradation </h5>
				<p> Weakened collagen and elastin in the skin results in damage of skin cells. Nurture your collagen with the right diet. </p>
				</div>
			<div class="col-md-6 col-sm-6">
 			  <h5>Stretch Marks </h5>
				<p> The base of developing stretch marks in women are genes. Know your genetic profile and response.</p>
			</div>
 		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Melanin pigments </h5>
				<p> Response of your skin to UV rays is determined by the melanin pigments and it protects the skin from harmful UV radiations. Genetics has major influence on the skin pigmentation. </p>
				</div>
			<div class="col-md-6 col-sm-6">
 			  <h5>Varicose Veins</h5>
				<p> This condition is accompanied by heaviness and pain in the legs. It is a common condition in women and associated strongly with genes. Test your genetics to prevent this disease before it gets too late.</p>
			</div>
 		</div>
 		</div>
 			<div class="readmore-link">
			<a class="flat-button"  role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> More <i class="material-icons">chevron_right</i>
			</a>
		</div>
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block">
 			</div>
 		</div>
	</div>
</section>

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	<div class="row">
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing5.jpg" alt="plan">
      		<div class="caption">
        		<h4>Ancestry DNA </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing2.jpg" alt="plan">
      		<div class="caption">
        		<h4>Gene Diet </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing3.jpg" alt="plan">
      		<div class="caption">
        		<h4>Health Horoscope </h4>  
        		<p>Know your future health  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing4.jpg" alt="plan">
      		<div class="caption">
        		<h4>Precision Medicine</h4>  
        		<p>Tailor your drug therapy  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		</div>
 	</div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text">Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                    <div class="quote-text editContent pix_text">
                       Amit Roy
                    </div>         
                    <div class="quote-dots">. . .</div>        
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text"> Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>
                       
                    <div class="quote-text editContent pix_text">
                        Ritesh Jain
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                    
                    <div class="quote-text editContent pix_text">
                        Asha Verma
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div> 
 			 </div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
	/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});

$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});
</script>


