<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	</main><!-- #site-content -->
	<footer id="site-footer" role="contentinfo">
	</footer><!-- #site-footer -->
	<!-- js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<?= base_url('assets/admin/js/bootstrap.min.js') ?>"></script>
	<!--<script src="<?= base_url('assets/admin/js/script.js') ?>"></script> -->

	<script type="text/javascript">
		function blockSpecialChar(e)
		{
	        var k;
	        document.all ? k = e.keyCode : k = e.which;
	        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
		}

	</script>

</body>
</html>