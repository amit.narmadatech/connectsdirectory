<?php $this->load->view('admin/include/header1'); ?>
<style type="text/css">
	.read{
		background-color: #F2F5F7;
	}
</style>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
	    <div class="x_title">
	      <h2>Notifications  </h2>
	      <div class="clearfix"></div>
	    </div>
	    <div class="x_content">
	      <div class="dashboard-widget-content">

	        <ul class="list-unstyled timeline widget">
	        	<?php foreach ($notification_list as $nflist) { ?>
                <li>
		            <div id="<?= $nflist->id ?>" class="block single-notification <?php echo (check_notification_isread($nflist->id,$this->session->user_id))?'':'read'; ?>">
		              <div class="block_content">
		                <h2 class="title">
		                                  <a><?= $nflist->title ?></a>
		                              </h2>
		                <div class="byline">
		                  <span><?php echo date('D d M Y h:i a', strtotime($nflist->created_at)); ?></span>
		                </div>
		                <p class="excerpt"><?= $nflist->body ?></p>
		              </div>
		            </div>
		        </li>
                <?php } ?>
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                code: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    number:true,
                    required:true,
                },
                pincode: {
                    number:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
                contact2: {
                    number:true
                }

            },
            messages:{
                title: "The title field is required",
                code: "The warehouse code field is required",
                contact_person: "The contact person field is required",
                email:{
                  email: "The email field must contain a valid email address",
                  required: "The email field is required",
                },
                contact1: {
                  number: "The mobile number field is not in the correct format",
                  required: "The mobile number field is required",
                },
                contact2: {
                  number: "The mobile number field is not in the correct format",
                },
                pincode: {
                  number: "The pincode field is not in the correct format",
                  required: "The pincode field is required",
                },
                state_id: "The state field is required",
                city_id: "The city field is required",
                address: "The address field is required",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });
});
</script>