<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Warehouse</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_warehouse_form'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">


      <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">         
            <label for="title" class=" col-md-3 col-sm-4">Title <span class="required">*</span>
            </label>      
            <div class="col-md-9 col-sm-8">      
              <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="<?= (isset($edit) && (!empty(($edit->title))))?$edit->title:set_value('title'); ?>">
               <span class="text-danger"><?php echo form_error('title'); ?></span> 
          </div>
        </div>

         <div class="col-sm-6 col-xs-12">        
            <label for="code" class=" col-md-3">Warehouse Code <span class="required">*</span>
            </label>           
            <div class="col-md-9">      
              <input type="text" id="code" name="code" class="form-control" placeholder="Enter Code" value="<?= (isset($edit) && (!empty(($edit->code))))?$edit->code:'INDORE-HQ-WH'; ?>">
              <span class="text-danger"><?php echo form_error('code'); ?></span>
          </div>        
          </div>    
         </div>
        </div>
    
     <div class="form-group">  
         <div class="row">           
            <div class="col-sm-6 col-xs-12">                  
            <label for="contact_person" class=" col-md-3">Contact Person <span class="required">*</span>
            </label>         
            <div class="col-md-9">      
              <input type="text" id="contact_person" name="contact_person" class="form-control" placeholder="Enter Contact Person" value="<?= (isset($edit) && (!empty(($edit->contact_person))))?$edit->contact_person:set_value('contact_person'); ?>">           
            <span class="text-danger"><?php echo form_error('contact_person'); ?></span>
            </div>
          </div>
          
           <div class="col-sm-6 col-xs-12">  
            <label for="email" class=" col-md-3">Email <span class="required">*</span>
            </label>    
            <div class="col-md-9">              
              <input type="email" id="email" name="email" class="form-control" placeholder="Enter email ID" value="<?= (isset($edit) && (!empty(($edit->email))))?$edit->email:set_value('email'); ?>">
               <span class="text-danger"><?php echo form_error('email'); ?></span>  
            </div>
          </div>
           </div>
        </div>

          <div class="form-group">
            <div class="row">
             <div class="col-sm-6 col-xs-12"> 
            <label for="contact1" class=" col-md-3">Contact Number<span class="required">*</span>
            </label>          
            <div class="col-md-9">      
              <input type="text" id="contact1" name="contact1" class="form-control" placeholder="Enter contact number" value="<?= (isset($edit) && (!empty(($edit->contact1))))?$edit->contact1:set_value('contact1'); ?>">
              <span class="text-danger"><?php echo form_error('contact1'); ?></span>
            </div>
          </div>
            
          <div class="col-sm-6 col-xs-12">
            <label for="contact2" class=" col-md-3">Alternate Contact Number
            </label>         
            <div class="col-md-9">        
              <input type="text" id="contact2" name="contact2" class="form-control" placeholder="Enter Alternate contact number" value="<?= (isset($edit) && (!empty(($edit->contact2))))?$edit->contact2:set_value('contact2'); ?>">
            <span class="text-danger"><?php echo form_error('contact2'); ?></span>
          </div>
        </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row">
               <div class="col-sm-6 col-xs-12">
               <label for="state_id" class=" col-md-3">State <span class="required">*</span>
                 </label>      
                 <div class="col-md-9">           
                <select name="state_id" id="state_id" class="form-control">
                  <option value="">Select State</option>
                  <?php if (!empty($state_list)) {
                      foreach ($state_list as $state) { ?>
                  <option <?= (isset($edit) && ($edit->state_id == $state->state_id))?'selected':''; ?> value="<?php echo $state->state_id; ?>"><?php echo $state->state_title; ?></option>        
                     <?php }
                  } ?>
              </select>
               <span class="text-danger"><?php echo form_error('state_id'); ?></span>
            </div>
          </div>
        
           
           <div class="col-sm-6 col-xs-12">
            <label for="district_id" class=" col-md-3">District <span class="required">*</span>
            </label>  
            <div class="col-md-9">               
              <select name="district_id" id="district_id" class="form-control">
              <option></option>
              </select>
                <span class="text-danger"><?php echo form_error('district_id'); ?></span>
            </div> 
          </div>

            
          </div>
        </div>

          <div class="form-group">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
            <label for="pincode" class=" col-md-3">Pincode <span class="required">*</span>
            </label>      
            <div class="col-md-9">           
              <input type="text" id="pincode" name="pincode" class="form-control" placeholder="Enter pincode" value="<?= (isset($edit) && (!empty(($edit->pincode))))?$edit->pincode:set_value('pincode'); ?>">           
            <span class="text-danger"><?php echo form_error('pincode'); ?></span>
          </div>        
          </div>
          <div class="col-sm-6 col-xs-12">
            <label for="address" class=" col-md-3">Address <span class="required">*</span>
            </label>     
            <div class="col-md-9">             
              <textarea id="address" name="address" class="form-control" placeholder="Enter address" ><?= (isset($edit) && (!empty(($edit->address))))?$edit->address:set_value('address'); ?></textarea>           
            <span class="text-danger"><?php echo form_error('address'); ?></span>
          </div>
        </div>

        </div>
      </div>

          <div class="form-group">

            <div class="row">
           <div class="col-sm-6 col-xs-12">
            <label for="description" class=" col-md-3">Description 
            </label>
            <div class="col-md-9">      
              <textarea id="description" name="description" class="form-control" placeholder="Enter description" ><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
           
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div> 
        </div>
        <div class="col-sm-6 col-xs-12">
            <label class=" col-sm-3 "> Status<!-- <?=  (isset($edit) && ($edit->status == 1))?'Active':'Inactive'; ?> --></label>
          <div class="col-md-9">      
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?= (isset($edit) && ($edit->status == 1))?'active':''; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-default <?= (isset($edit) && ($edit->status == 0))?'active':''; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
         
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('inventory/warehouse_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getdistrict'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#district_id').html('');      
              $('#district_id').html(response.result); 
              $('#district_id').val('<?php echo (isset($edit))?$edit->district_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                code: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    number:true,
                    required:true,
                },
                pincode: {
                    number:true,
                    required:true,
                },
                state_id: "required",
                district_id: "required",
                address: "required",
                contact2: {
                    number:true
                }

            },
            messages:{
                title: "The title field is required",
                code: "The warehouse code field is required",
                contact_person: "The contact person field is required",
                email:{
                  email: "The email field must contain a valid email address",
                  required: "The email is required",
                },
                contact1: {
                  number: "The mobile number is not in the correct format",
                  required: "The mobile number is required",
                },
                contact2: {
                  number: "The mobile number is not in the correct format",
                },
                pincode: {
                  number: "The pincode is not in the correct format",
                  required: "The pincode field is required",
                },
                state_id: "The state field is required",
                district_id: "The district field is required",
                address: "The address field is required",
            },
           /* highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }*/
        });

    });
});
</script>