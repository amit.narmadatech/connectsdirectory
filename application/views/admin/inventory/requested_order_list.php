<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Purchase Request List</h2>
          <ul class="nav navbar-right panel_toolbox">           
            <li><a href="<?php echo base_url('inventory/request_order_form'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add purchase request</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">         
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Reference No.</th>
              <th>Warehouse</th>
              <th>Supplier</th>
              <th>Item</th>
              <th>Quantity</th>
              <th>Purchase Status</th>
              <th>Created Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
             if(!empty($request_list)){                
                foreach($request_list as $rdlist){ ?>
                <tr>
                    <td><?php  echo !empty($rdlist->reference_no) ? $rdlist->reference_no : '' ?></td>
                    <td><?php  echo !empty($rdlist->warehouse_name) ? $rdlist->warehouse_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->supplier_name) ? $rdlist->supplier_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->product_name) ? $rdlist->product_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->product_qty) ? $rdlist->product_qty : '' ?></td>
                    <td>
                      <?php  if($rdlist->purchase_status ==5){ echo '<a href="'.base_url("inventory/dispatched_detail/$rdlist->id").'" class="label label-success">Dispatched from xcode</a>'; }
                              elseif(($rdlist->cmd_approval == 2) && ($rdlist->fh_approval == 2) && ($rdlist->ceo_approval == 2)){echo 'rejected'; echo $rdlist->reject_reason; } 
                              elseif($rdlist->purchase_status ==2){echo 'received'; } 
                              elseif(($rdlist->cmd_approval == 1) && ($rdlist->fh_approval == 1) && ($rdlist->ceo_approval == 1) && (!check_stockin_exists($rdlist->reference_no))){echo 'approved'; } 
                              else{echo 'pending'; }

                             if ($rdlist->purchase_status ==5) {
                                echo "<a href=".base_url('inventory/stock_in/'.$rdlist->id)." class='btn btn-info btn-xs pull-right'><i class='fa fa-edit'></i> Stock In</a>";
                              } 
                      ?>
                    </td>
                    <td><?php  echo !empty($rdlist->created_at) ?date("D, d M Y",strtotime($rdlist->created_at)): '' ?></td>
                    <td>
                      <a target="_new" href="<?php echo base_url('inventory/get_po_invoice/'.$rdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>
                      <?php if (($rdlist->cmd_approval == 0) && ($rdlist->fh_approval == 0) && ($rdlist->ceo_approval == 0)) { ?>
                      <a href="<?php echo base_url('inventory/request_order_form/'.$rdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>

                      <?php } ?>
              </td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>       
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <script type="text/javascript">
      jQuery(document).ready(function($) {

        $(document).on('click', '.showKitList', function(event) {
            var id = this.id;
            $.post("<?php echo base_url('rcenter/getScannedKitList'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
              var data = JSON.parse(data);
              var txt = '';
              var i = 1;
              $.each(data.result, function(index, val) {
                 txt = '<tr><td>'+i+'</td><td>'+val.barcode+'</td></tr>';
              });

              $('#scanKitArea').append(txt);

            });
        });
        
      });
    </script>