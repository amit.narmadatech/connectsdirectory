<?php
if(!empty($this->session->userdata('issues'))):
	$issuesQrcode = $this->session->userdata('issues');
	$issuesQrcode = json_decode($issuesQrcode);
	foreach($issuesQrcode as $keyId=>$Qrcode):
	?>
	    <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> Barcode <?php  echo $Qrcode;?> is already exists </div>
	<?php
	endforeach;
endif;
$this->session->unset_userdata('issues');
?>