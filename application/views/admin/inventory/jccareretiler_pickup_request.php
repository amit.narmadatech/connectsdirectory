<?php $this->load->view('admin/include/header'); ?>
<!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>JCcare/Retiler Pickup Request List</h2>
               
                <div class="clearfix"></div>
            </div>
            <div class="x_content">         
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Order No.</th>
                            <th>JCcare/Retailer ID</th>
                            <th>Requested By</th>
                            <th>Requested for</th>
                            <th>shipping to</th>
                            <th>Quantity</th>
                            <th>Date Time</th>
                            <th>Status</th>
                            <th>Request Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($request_list)) {
                            $i = 1;
                            foreach ($request_list as $rdlist) {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo!empty($rdlist->order_number) ? $rdlist->order_number : '' ?></td>
                                    <td><?php echo!empty($rdlist->refcode) ? $rdlist->refcode : '' ?> </td> 
                                    <td><?php 
                                         if($rdlist->reftype =='F'){
                                                    echo 'JCCARE';
                                            }else if($rdlist->reftype =='R'){
                                                echo 'Retailer';
                                            }else if($rdlist->reftype =='FR'){
                                                echo 'JCcare Retailer';
                                            } 
                                          ?>
                                    </td> 
                                     <td>
                                        <?php
                                            if($rdlist->type ==1){
                                                    echo '<span class="label label-primary">Order Dispatch From Jccare/Retailer</span>';
                                            }else if($rdlist->type ==2){
                                                echo '<span class="label label-primary">Order Pickup From Jccare/Retailer</span>';
                                            }else if($rdlist->type ==3){
                                                echo '<span class="label label-primary">Damage Return From Jccare/Retailer</span>';
                                            } 
                                        ?>
                                    </td>
                                    <td>
                                        <?php echo!empty($rdlist->contact_person) ? $rdlist->contact_person : '' ?>,</br>
                                        <?php echo!empty($rdlist->contact_number) ? $rdlist->contact_number : '' ?>,</br>
                                        <?php echo!empty($rdlist->address) ? $rdlist->address : '' ?>, <?php echo!empty($rdlist->pincode) ? $rdlist->pincode : '' ?>
                                    </td>
                                    <td><?php echo!empty($rdlist->quantity) ? $rdlist->quantity : '' ?></td>
                                    <td><?php echo!empty($rdlist->pickup_date) ? $rdlist->pickup_date : '' ?> <?php echo!empty($rdlist->pickup_time) ? $rdlist->pickup_time : '' ?></td>
                                    <td>
                                        <?php
                                            if($rdlist->status ==1){
                                                    echo '<span class="label label-info">Pending</span>';
                                            }else if($rdlist->status ==2){
                                                echo '<span class="label label-success">Approved</span>';
                                            }else if($rdlist->status ==3){
                                                echo '<span class="label label-danger">Declined</span>';
                                            }else if($rdlist->status ==4){
                                                echo '<span class="label label-primary">Completed</span>';
                                            } 
                                        ?>
                                    </td>
                                    <td><?php echo!empty($rdlist->created_at) ? date("D, d M Y", strtotime($rdlist->created_at)) : '' ?></td>
                                    <td>
                                        <button type="button" data-id="<?php echo $rdlist->id; ?>" class=" btn btn-primary btn-xs shippingBtn">Shipping</button>
                                         <a href="<?php echo base_url('inventory/view_jccareretailer_request/'.$rdlist->user_id); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
                                    </td>
                                </tr>
                            <?php }
                        } ?>
                    </tbody>
                </table>       
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form name="dispatch_request" id="dispatch_request" method="post">    
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dispatch Form</h4>
                </div>
                <div class="modal-body">
                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />    
                    <div class="form-group">
                        <label for="shipping_company">Courier Company</label>
                        <input type="text" class="form-control" name="shipping_company" id="shipping_company">
                        <input type="hidden" class="form-control" name="request_id" id="request_id">
                    </div>
                    <div class="form-group">
                        <label for="tracking_code">Tracking Code</label>
                        <input  type="text" class="form-control" name="tracking_code" id="tracking_code">
                    </div>
                    <div class="form-group">
                        <label for="shipping_date">Dispatch Date</label>
                          <?php $start_date = date('Y-m-d'); ?>
                        <input  type="date" min="<?php echo $start_date; ?>" class="form-control" name="shipping_date" id="shipping_date">
                    </div>
                    <div class="form-group">
                        <label for="shipping_note">Note</label>
                        <textarea  class="form-control" name="shipping_note" id="shipping_note"></textarea>
                    </div>
                </div>
                <div id="notification" style="display:none"></div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnSubmit">Submit</button> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form> 
        </div>

    </div>
</div>


<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
          $(document).on('click', '.shippingBtn', function (event) {
            $('#request_id').val($(this).attr('data-id'));
            $('#myModal').modal('show');
        });
        
        var validator = $("#dispatch_request").validate({
                rules: {
                    shipping_company: "required",
                    tracking_code: "required",
                    shipping_date: "required",
                    shipping_note: "required",
                },
                submitHandler: function (e) {
                    $('.btnSubmit').prop('disabled', true);
                    $.ajax({
                        url: "<?php echo base_url('inventory/jccareretailer_dispatch') ?>",
                        type: "POST",
                        data: $('#dispatch_request').serialize(),
                        dataType: "json",
                        success: function (response) {
                            $('.btnSubmit').prop('disabled', false);
                            if (response.status == true) {
                                $('#notification').removeClass('alert alert-danger');
                                $('#notification').html(response.msg).addClass('alert alert-success').show().delay(2000).fadeOut("slow", function () {
                                    location.reload();
                                });
                            } else {
                                $('#notification').removeClass('alert alert-success');
                                $('#notification').html(response.msg).addClass('alert alert-danger').show().delay(2000).fadeOut("slow")
                            }
                        }
                    })
                },
                messages: {
                    shipping_company: "Please enter name shipping company",
                    tracking_code: "Please enter tracking code",
                    shipping_date: "Please enter shipping date",
                    shipping_note: "Please enter note",
                }
            });
    });
</script>