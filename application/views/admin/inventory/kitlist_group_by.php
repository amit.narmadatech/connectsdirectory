<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <?php $this->load->view('include/alert'); ?>
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Uploaded Kit</h2>
          <ul class="nav navbar-right panel_toolbox">           
            <li><a href="<?php echo base_url('inventory/upload_kit'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> New Upload Kit</button></a>
            </li>
            <li>
              <?php $is_used = empty($this->input->get('st'))?0:$this->input->get('st'); ?>
                <select class="form-control" id="changeStatus">
                  <option <?php echo ($is_used == 0)?'selected':''; ?> value="0">Available</option>
                  <option <?php echo ($is_used == 1)?'selected':''; ?> value="1">Sold</option>
                </select>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">        
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead> 
            <tr>
              <th width="5%">S.No.</th>
      				<th>Bar Code</th>
              <th>Status</th>
				      <th>Date</th>
			     </tr>
          </thead>
          <tbody>
            <?php 
              if(!empty($kitGroup)){
              $i=1;              
                foreach($kitGroup as $slist){ ?>
            <tr>
              <td><?php  echo $i++; ?></td>
				      <td><?php  echo !empty($slist->barcode) ? $slist->barcode : '' ?></td>              
              <td><?php if($slist->is_used == 1){echo "Sold";}else{echo "Active";} ?></td>
				      <td><?php  echo !empty($slist->created_at) ? date_formate($slist->created_at) : '' ?></td>				 
            </tr>
              <?php } } ?>
            </tbody>
            <tfoot>
              <tr>
                <td></td>
                <td align="center"><b>Total Stock :</b></td>
                <td align="center"><b><?php echo count($kitGroup); ?></b></td>
                <td align="center" ></td>
              </tr>
            </tfoot>
          </table>       
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(document).on('change', '#changeStatus', function(event) {
          var value = this.value;
          window.location.href = '<?php echo base_url("inventory/upload_kitlist"); ?>?st='+value;
        });
      });
    </script>