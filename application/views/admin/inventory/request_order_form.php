<?php $this->load->view('admin/include/header1'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Purchase Request</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_request_order_form'); ?>" > 
         <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
         <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" /> 
         <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

       <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Date 
            </label>
             <div class="col-md-9 col-sm-8"> 
              <input readonly type="text" name="date" id="date" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->date))))?$edit->date:date("D, d M Y"); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('date'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">P.O. No. 
            </label>
            <div class="col-md-9 col-sm-8"> 
              <input readonly type="text" name="reference_no" id="reference_no" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->reference_no))))?$edit->reference_no:$reference_no; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('reference_no'); ?></span>
          </div>
        </div>
      </div>

       <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Supplier <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <select name="supplier_id" id="supplier_id" class="form-control">
                  <?php if (!empty($supplier_list)) {
                      foreach ($supplier_list as $supplier) { ?>
                  <option <?= (isset($edit) && ($edit->supplier_id == $supplier->id))?'selected':''; ?> value="<?php echo $supplier->id; ?>"><?php echo $supplier->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('supplier_id'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Warehouse <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <select name="warehouse_id" id="warehouse_id" class="form-control">
                  <?php if (!empty($warehouse_list)) {
                      foreach ($warehouse_list as $warehouse) { ?>
                  <option <?= (isset($edit) && ($edit->warehouse_id == $warehouse->id))?'selected':''; ?> value="<?php echo $warehouse->id; ?>"><?php echo $warehouse->title; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('warehouse_id'); ?></span>
          </div>
        </div>
      </div>

       <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="description">Note 
            </label>
            <div class="col-md-9 col-sm-8"> 
              <textarea style="height: 110px;" name="description" id="description" class="form-control" placeholder="Enter Request Note"><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <div class="col-sm-3"></div>
             <div class="col-md-9 col-sm-8"> 
                <h3 id="whName"></h3>
                <p><span id="whAddress"></span><span id="whCity"></span></p>
                <p><span id="whState"></span><span id="whPincode"></span></p>
            </div>
          </div>
        </div>
      </div>

          <div class="form-group">
            <div class="row">
            <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Add Item <span class="required">*</span>
            </label>
           <div class="col-md-6 col-sm-6"> 
              <select name="product_list" id="product_list" class="form-control">
                  <?php if (!empty($product_list)) {
                      foreach ($product_list as $product) { ?>
                  <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button id="add_row" class="btn btn-info" type="button">ADD</button>
                </div>
            </div>
          </div>
        </div>
      </div>

          <div class="row clearfix">
            <div class="col-sm-12">
            <H3>Order Items</H3>
                 <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Item (Code-Name)</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Tax</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>
                            <tbody id="item_table">
                            <?php 
                                $total = 0;
                            if (!empty($edit_details)) {
                                foreach ($edit_details as $edetail) { ?>
                                 <tr>
                                     <td></td>
                                     <td><?php echo $edetail->code.'-'.$edetail->name; ?><input type="hidden" name="product[]" value="<?php echo $edetail->product_id;  ?>"></td>
                                     <td style="width:175px;"><input type="number" class="changeQty form-control" id="<?php echo $edetail->id; ?>" value="<?php echo $edetail->quantity; ?>" min="1" name="quantity[]"></td>

                                     <td><?php echo $edetail->rate; ?><input type="hidden" id="rate_<?php echo $edetail->id; ?>" name="rate[]" value="<?php echo $edetail->rate; ?>"></td>
                                     
                                     <td>
                                        <input readonly="" type="number" id="tax_<?php echo $edetail->id; ?>" name="tax[]" class="changeTax form-control col-md-6 valid" style="width:100px;" value="<?php echo $edetail->tax_prct; ?>" aria-invalid="false"><span class="pull-right col-md-6" id="taxAmtText_<?php echo $edetail->id; ?>"><?php echo $edetail->tax_amt; ?></span><input type="hidden" id="taxamount_<?php echo $edetail->id; ?>" name="taxamount[]" value="<?php echo $edetail->tax_amt; ?>">
                                      </td>
                                     
                                     <td>
                                      <span id="amountText_<?php echo $edetail->id; ?>"><i class="fa fa-inr"></i> <?php echo $edetail->amount; ?></span>
                                      <input type="hidden" id="amount_<?php echo $edetail->id; ?>" name="amount[]" value="<?php echo $edetail->amount;$total +=$edetail->amount; ?>"></td>                                     
                                     <td>
                                         <button type="button" class="delete_btn btn btn-inverse btn-xs waves-effect m-t-10" id="<?php echo $edetail->id; ?>">X</button>
                                     </td>
                                 </tr>   
                            <?php  }
                            }
                            ?> 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align="center"><b>Total</b></td>
                                    <td><span id="grandTotalText"><i class="fa fa-inr"></i> <?php  echo $total; ?></span><input type="hidden" id="grand_total" name="grand_total" value="0"></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('inventory/request_order_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="csrfname" value="<?php echo $csrf['name']; ?>">
<input type="hidden" name="csrfhash" value="<?php echo $csrf['hash']; ?>">
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
        


    jQuery(document).ready(function($) {

        function getGrandTotal() {
        var totalGPrice = 0;
          $('input[name="amount[]"]').each( function() {
              totalGPrice += parseFloat(this.value);
          });
              $('#grandTotalText').html(totalGPrice); 
              $('#grand_total').val(totalGPrice);
              alert(totalGPrice);
              //return totalGPrice;
        }

        $('#form_validation').validate({
            submitHandler: function(form) {
                if(!$("input[name='product[]']").val()){
                  alert("Please select a item first !");
                  return false;
                }else{
                  var chk = 0;
                   $('input[name="quantity[]"]').each(function(index, val) {
                      var val=$(this).val();
                      if(isNaN(val) || (val < 1)){
                        chk++;
                      }
                    });

                   if (chk > 0) { alert("Quantity should not be blank or zero !"); $('input[name="quantity[]"]').focus(); }else{
                      $('#submitBtn').attr('disabled', true);
                      form.submit();
                   }

                }
            },
            ignore: [],
            rules: {
                supplier_id: "required",
                warehouse_id: "required",
            },
            messages:{
                supplier_id: "supplier field is required",
                warehouse_id: "warehouse field is required",
            },
           /* highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }*/
        });

  
    var itemArray = new Array();

    $('input[name="product[]"]').each( function() {
      itemArray.push(this.value);
    });

    $("#add_row").click(function(event) {
        if (id == '') { alert('Please first choose item !');$("#product_list").focus();  return false; }
        var id = $("#product_list").val();
        //console.log(jQuery.inArray(id,itemArray));
        $('#add_row').attr('disabled', true);
        if((jQuery.inArray(id,itemArray) == -1)){

            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id,'<?php echo $csrf['name']; ?>' : '<?php echo $csrf['hash']; ?>'},function(response){
                if(response.status){
                  $('#add_row').attr('disabled', false);
                  var taxamt = (parseFloat(response.data.purchase_price)*parseFloat(response.data.tax)/100);
                  var amt = parseFloat(response.data.purchase_price)+taxamt;
                  var html = '<tr id="row_'+response.data.id+'"><td></td><td>'+response.data.code+'-'+response.data.name+'<input type="hidden" name="product[]" value="'+response.data.id+'"></td><td style="width:175px;"><input type="number" autocomplete="off" class="changeQty form-control" id="'+response.data.id+'" value="1" min="1" name="quantity[]"></td><td><span id="rateText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="rate_'+response.data.id+'" name="rate[]" value="'+response.data.purchase_price+'"></td><td><input readonly type="number" id="tax_'+response.data.id+'" name="tax[]" class="changeTax form-control col-md-6" style="width:100px;" value="'+response.data.tax+'"><span class="pull-right col-md-6" id="taxAmtText_'+response.data.id+'">'+taxamt+'</span><input type="hidden" id="taxamount_'+response.data.id+'" name="taxamount[]" value="'+taxamt+'"></td><td><span id="amountText_'+response.data.id+'">'+amt+'</span><input type="hidden" id="amount_'+response.data.id+'" name="amount[]" value="'+amt+'"></td><td><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+response.data.id+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  $('#grandTotalText').html(amt);
                  $('#grand_total').val(amt);
                  console.log(itemArray);
                }else{
                  //console.log(itemArray);
                }
            },"json");
        }else{
          $('#add_row').attr('disabled', false);
        }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id,'<?php echo $csrf['name']; ?>' : '<?php echo $csrf['hash']; ?>'},function(response){
                if(response.status){                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

        var id = $("#warehouse_id").val();

        if (id != "") {
          if(jQuery.inArray(id,itemArray) != 0){
              $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id,'<?php echo $csrf['name']; ?>' : '<?php echo $csrf['hash']; ?>'},function(response){
                  if(response.status){                  
                    $("#whName").html(response.data.title);
                    $("#whAddress").html(response.data.address);
                    //$("#whCity").html(response.data.);
                    //$("#whState").html(response.data.);
                    $("#whPincode").html(response.data.pincode);
                  }else{
                    console.log(response);
                  }
              },"json");
          }
        }

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#grandTotalText').html('');
        $('#grand_total').val('');
        $('#row_'+id).remove();
    });

    $(document).on('change keyup keydown keypress;', '.changeQty', function(event) {
        var id = this.id;
        var taxprnct = parseFloat($('#tax_'+id).val());
        var qty = parseFloat(this.value);
        var rate = parseFloat($('#rate_'+id).val());
        var tax = ((rate * taxprnct)/100)*qty;
        var amt = 0;
        if( (!isNaN(qty)) && (!isNaN(rate))){
            amt = (qty*rate)+tax;
            $('#amountText_'+id).html(amt); 
            $('#amount_'+id).val(amt);
            $('#taxAmtText_'+id).html(tax);
            $('#taxamount_'+id).val(tax);
            getGrandTotal();
           /* var gamt = getGrandTotal();
            $('#grandTotalText').html(gamt); 
            $('#grand_total').val(gamt);*/
        }
    });

    $(document).on('click', '.delete_btn', function(event) {
      var did = this.id;
      if (confirm('Are you sure ?')) {
        $.post("<?php echo base_url('inventory/delete_request_detail'); ?>", {id: did,'<?php echo $csrf['name']; ?>' : '<?php echo $csrf['hash']; ?>'}, function(response) {
          {
          if(response == 'true')
            window.location.reload();
          }
        });
      }
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

});
</script>
