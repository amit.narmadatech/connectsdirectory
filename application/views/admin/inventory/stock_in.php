<?php $this->load->view('admin/include/header1'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Check-in</h2>      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_stock_purchase_form'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <div class="form-group">
         <div class="row">           
           <div class="col-sm-6 col-xs-12">
             <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Date <span class="required">*</span>
            </label>
           <div class="col-md-9 col-sm-8"> 
              <input readonly type="text" name="purchase_date" id="purchase_date" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->date))))?$edit->date:date("D, d M Y"); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('purchase_date'); ?></span>
          </div>
         <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Reference No. <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <input readonly type="text" name="reference_no" id="reference_no" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->reference_no))))?$edit->reference_no:$reference_no; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('reference_no'); ?></span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">           
           <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">P.O. No. <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <input type="text" name="po_no" id="po_no" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->po_no))))?$edit->po_no:set_value('po_no'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('po_no'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Supplier <span class="required">*</span>
            </label>
           <div class="col-md-9 col-sm-8">
              <select name="supplier_id" id="supplier_id" class="form-control">
                  <option></option>
                  <?php if (!empty($supplier_list)) {
                      foreach ($supplier_list as $supplier) { ?>
                  <option <?= (isset($edit) && ($edit->supplier_id == $supplier->id))?'selected':''; ?> value="<?php echo $supplier->id; ?>"><?php echo $supplier->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('supplier_id'); ?></span>
          </div>
        </div>
      </div>

       <div class="form-group">
         <div class="row">           
           <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Warehouse <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8">
              <select name="warehouse_id" id="warehouse_id" class="form-control">
                  <option></option>
                  <?php if (!empty($warehouse_list)) {
                      foreach ($warehouse_list as $warehouse) { ?>
                  <option <?= (isset($edit) && ($edit->warehouse_id == $warehouse->id))?'selected':''; ?> value="<?php echo $warehouse->id; ?>"><?php echo $warehouse->title; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('warehouse_id'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Description
            </label>
           <div class="col-md-9 col-sm-8">
              <textarea id="description" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>
        </div>
      </div>

        <div class="form-group">
         <div class="row">           
           <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Add Product <span class="required">*</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              <select name="product_list" id="product_list" class="form-control">
                  <option></option>
                  <?php if (!empty($product_list)) {
                      foreach ($product_list as $product) { ?>
                  <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                        <button id="add_row" class="btn btn-info waves-effect m-t-10" type="button">ADD</button>
                </div>
            </div>
          </div>

          <div class="row clearfix">
            <div class="col-sm-12">
            <H3>Items</H3>
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Product (Code-Name)</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>
                            <tbody id="item_table">
                            <?php 
                            if (!empty($edit_details)) {
                                foreach ($edit_details as $edetail) { ?>
                                 <tr>
                                     <td></td>
                                     <td><?php echo $edetail->code.'-'.$edetail->name; ?><input type="hidden" name="product[]" value="<?php echo $edetail->id;  ?>"></td>
                                     <td><input type="number" class="changeQty form-control" id="<?php echo $edetail->id; ?>" value="<?php echo $edetail->quantity; ?>" min="1" name="quantity[]"></td>

                                     <td><i class="fa fa-inr"></i> <?php echo $edetail->rate; ?><input type="hidden" id="rate_<?php echo $edetail->id; ?>" name="rate[]" value="<?php echo $edetail->rate; ?>"></td>

                                     <td><i class="fa fa-inr"></i> <?php echo $edetail->amount; ?><input type="hidden" id="amount_<?php echo $edetail->id; ?>" name="amount[]" value="<?php echo $edetail->amount; ?>"></td>
                                     
                                     <td>
                                         <button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="<?php echo $edetail->id; ?>">X</button>
                                     </td>
                                 </tr>   
                            <?php    }
                            }
                            ?> 
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td align="center"><b>Total</b></td>
                                    <td><i class="fa fa-inr"></i> <span id="grandTotalText"></span><input type="hidden" id="grand_total" name="grand_total" value="0"></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-danger" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    $("#add_row").click(function(event) {
        var id = $("#product_list").val();
        if (id == '') { alert('Please first choose product !');$("#product_list").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  var html = '<tr id="row_'+response.data.id+'"><td></td><td>'+response.data.code+'-'+response.data.name+'<input type="hidden" name="product[]" value="'+response.data.id+'"></td><td style="width:175px;"><input type="number" class="changeQty form-control" id="'+response.data.id+'" value="1" min="1" name="quantity[]"></td><td><span id="rateText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="rate_'+response.data.id+'" name="rate[]" value="'+response.data.purchase_price+'"></td><td><span id="amountText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="amount_'+response.data.id+'" name="amount[]" value="'+response.data.purchase_price+'"></td><td><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+response.data.id+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  $('#grandTotalText').html(response.data.purchase_price);
                  $('#grand_total').val(response.data.purchase_price);
                  console.log(html);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
        $('#grandTotalText').html('');
        $('#grand_total').val('');
    });

    $(document).on('change', '.changeQty', function(event) {
        var id = this.id;
        var qty = parseFloat(this.value);
        var rate = parseFloat($('#rate_'+id).val());
        var amt = 0;
        if( (!isNaN(qty)) && (!isNaN(rate))){
            amt = qty*rate;
            $('#amountText_'+id).html(amt); 
            $('#amount_'+id).val(amt);
            $('#grandTotalText').html(amt); 
            $('#grand_total').val(amt);
        }
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
      $('#form_validation').validate({
            submitHandler: function(form) {
                if(!$("input[name='product[]']").val()){
                  alert("Please select a product first !");
                  return false;
                }else{
                  form.submit();
                }
            },
            ignore: [],
            rules: {
                supplier_id: "required",
                warehouse_id: "required",
                po_no:{
                  required: true,
                  remote: {
                    url: '<?php echo base_url('inventory/checkPOExists'); ?>',
                    type: "post",
                  }
                }
            },
            messages:{
                po_no: "",
                po_no:{
                  required: "purchase order no. field is required",
                  remote: "purchase order no. is already in stock-in record"
                },
                supplier_id: "supplier field is required",
                warehouse_id: "warehouse field is required",
            },
            // highlight: function (input) {
            //     $(input).parents('.form-line').addClass('error');
            // },
            // unhighlight: function (input) {
            //     $(input).parents('.form-line').removeClass('error');
            // },
            // errorPlacement: function (error, element) {
            //     $(element).parents('.form-group').append(error);
            // }
        });  

    });

});
</script>
