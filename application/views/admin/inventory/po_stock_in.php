<?php $this->load->view('admin/include/header1'); ?>
<style type="text/css">
  .form-control{ width: 50%; display: inline;}
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Stock IN</h2>       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="ele2" class="b">
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_stock_in'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="request_id" value="<?= isset($po_request)?$po_request->id:''; ?>">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  # <span><?php echo $reference_no; ?></span>
                  <small class="pull-right">Date: <?php echo date("d/m/Y"); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <h4><b>Vendor</b></h4>
                  <address>
                    <strong><?php echo !empty($po_request->s_name)?$po_request->s_name:''; ?></strong>
                    <br><?php echo !empty($po_request->s_address)?$po_request->s_address:''; ?>
                    <br>Phone: <?php echo !empty($po_request->s_contact1)?$po_request->s_contact1:''; ?> <?php echo !empty($po_request->s_contact2)?", ".$po_request->s_contact2:''; ?>
                    <br>Email: <?php echo !empty($po_request->s_email)?$po_request->s_email:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                    <strong><?php echo !empty($po_request->w_name)?$po_request->w_name:''; ?>, [ <?php echo !empty($po_request->w_code)?$po_request->w_code:''; ?> ]</strong>
                      <br><?php echo !empty($po_request->w_address)?$po_request->w_address:''; ?>
                      <br><?php echo !empty(getDistrict($po_request->district_id)) ? getDistrict($po_request->district_id)->district_title : '';  ?>, <?php echo !empty(getState(getDistrict($po_request->district_id)->state_id)) ? getState(getDistrict($po_request->district_id)->state_id)->state_title : ''; ?> <?php echo !empty($po_request->w_pincode)?$po_request->w_pincode:''; ?>
                      <br>Phone: <?php echo !empty($po_request->w_contact1)?$po_request->w_contact1:''; ?> <?php echo !empty($po_request->w_contact2)?", ".$po_request->w_contact2:''; ?>
                      <br>Email: <?php echo !empty($po_request->w_email)?$po_request->w_email:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Ref. Number #<?php echo !empty($reference_no)?$reference_no:''; ?> <input type="hidden" name="reference_no" value="<?php echo $reference_no; ?>"></b>
                <br>
                <b>PO Number #<?php echo !empty($po_request->reference_no)?$po_request->reference_no:''; ?>
                  <input type="hidden" name="po_no" value="<?php echo $po_request->reference_no; ?>">
                  <input type="hidden" name="supplier_id" value="<?php echo $po_request->supplier_id; ?>">
                  <input type="hidden" name="warehouse_id" value="<?php echo $po_request->warehouse_id; ?>">
                </b>
                <br>
                <br>
                <b>PO Date:</b> <?php echo date("d/m/Y",strtotime($po_request->created_at)); ?>
                <br>                
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th width="20%">Item</th>
                      <th>Req. Qty</th>
                      <th>Rec. Qty</th>
                      <th>Damaged Qty</th>
                      <th>Miss. Qty</th>
                      <th>Actual Qty</th>
                      <th>Unit Price</th>
                      <th class="text-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if (!empty($po_request_details)) {
                      $i = 1;$sub_total = 0;
                      foreach ($po_request_details as $detail) { ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $detail->product_name; ?> [ <?php echo $detail->product_code; ?> ]</td>
                      <td><?php echo $detail->quantity; ?><input type="hidden" name="requested_qty[]" id="requested_qty" value="<?php echo $detail->quantity; ?>"></td>
                      <td><input type="number" min="0" value="0" name="received_qty[]" id="received_qty" class="form-control"></td>
                      <td><input type="number" min="0" value="0" name="damaged_qty[]" id="damaged_qty" class="form-control"></td>
                      <td><input type="number" min="0" readonly value="0" name="missed_qty[]" id="missed_qty" class="form-control"></td>
                      <td><input type="number" min="0" readonly value="0" name="actual_qty[]" id="actual_qty" class="form-control"></td>
                      <td><span id="sp_unit_amt"><?php echo $detail->rate; ?><input type="hidden" name="rate[]" value="<?php echo $detail->rate; ?>"></span></td>
                      <td class="text-right"><span id="sp_cur_amt"><?php echo 0;$sub_total += $detail->amount;?></span><input type="hidden" name="current_amt[]" id="current_amt" value="<?php echo $detail->amount; ?>"></td>
                      <input type="hidden" name="product_id[]" value="<?php echo $detail->product_id; ?>">
                    </tr>    
                    <?php  }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
                <!-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;min-height: 115px;">
                  <?php echo $po_request->description; ?>
                </p> -->
                <textarea id="description" name="description" class="text-muted no-shadow form-control" style="margin-top: 10px;height: 115px;" rows="10"></textarea>
              </div>
              <!-- /.col -->
              <div class="col-xs-6">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td class="text-right"><span id="sp_sub_ttl"><?php echo $sub_total=0; ?></span><input type="hidden" name="sub_total" id="sub_total"></td>
                      </tr>
                      <!-- <tr>
                        <th>Tax %:<input type="number" min="0" value="0" name="tax_prct" id="tax_prct" class="form-control pull-right"></th>
                        <td><input type="number" min="0" value="0" name="tax_amt" id="tax_amt" class="form-control"></td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td><input type="number" min="0" value="0" name="shipping_charge" id="shipping_charge" class="form-control"></td>
                      </tr> -->
                      <tr>
                        <th>Total:</th>
                        <td class="text-right"><span id="sp_grd_ttl"><?php echo $grand_total=$sub_total; ?></span><input type="hidden" name="grand_total" id="grand_total"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-6 col-md-offset-5">
                <button type="submit" class="btn btn-primary"><i class="fa fa-right"></i> Submit</button>
                <a href="<?php  echo base_url('inventory/request_order_list'); ?>" class="btn btn-default"><i class="fa fa-right"></i> Cancel</a>
              </div>
            </div>
          </section>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>

<script>
    jQuery(document).ready(function($) {

    $('#print_po').click(function(event) {
      $.print(".b");
    });

    var rem_qty = parseInt($('#requested_qty').val());

    $(document).on('keyup keypress blur change', '#received_qty', function(event) {
      var req_qty = isNaN(parseInt($('#requested_qty').val()))?0:parseInt($('#requested_qty').val());
      var rec_qty = isNaN(parseInt($('#received_qty').val()))?0:parseInt($('#received_qty').val());
      $('#damaged_qty').val(0);
      $('#missed_qty').val(0);
      $('#actual_qty').val(0);
      if ((rec_qty != '') && (!isNaN(rec_qty))) {
        if (rec_qty > req_qty) {
          alert('Received quantity can not be greater then requested quantity !');
          $('#received_qty').val(0);
          $('#received_qty').focus();
        }else{
          rem_qty = parseInt(req_qty - rec_qty);
          $('#missed_qty').val(rem_qty);
          $('#actual_qty').val(rec_qty);
          var unit_amt = parseFloat($('#sp_unit_amt').html());
          var act_qty = parseInt($('#actual_qty').val());
          $('#sp_cur_amt').html(unit_amt*act_qty);
          $('#current_amt').val(unit_amt*act_qty);
          $('#sp_sub_ttl').html(unit_amt*act_qty);
          $('#sp_grd_ttl').html(unit_amt*act_qty);
          $('#sub_total').val(unit_amt*act_qty);
          $('#grand_total').val(unit_amt*act_qty);
        }
      }
    });

    $(document).on('keyup keypress blur change', '#damaged_qty', function(event) {
      var req_qty = isNaN(parseInt($('#requested_qty').val()))?0:parseInt($('#requested_qty').val());
      var rec_qty = isNaN(parseInt($('#received_qty').val()))?0:parseInt($('#received_qty').val());
      var dam_qty = isNaN(parseInt($('#damaged_qty').val()))?0:parseInt($('#damaged_qty').val());
      console.log(dam_qty);
      if (((dam_qty) && (!isNaN(dam_qty))) || (dam_qty == 0)) {
        if (dam_qty > rec_qty) {
          alert('Damaged quantity can not be greater then received quantity !');
          $('#damaged_qty').val(0);
          $('#damaged_qty').focus();
        }else{
          rem_qty -= dam_qty;
         $('#missed_qty').val(req_qty-rec_qty); 
         $('#actual_qty').val(rec_qty-dam_qty);
         var unit_amt = parseFloat($('#sp_unit_amt').html());
         var act_qty = parseInt($('#actual_qty').val());
         $('#sp_cur_amt').html(unit_amt*act_qty);
         $('#current_amt').val(unit_amt*act_qty);
         $('#sp_sub_ttl').html(unit_amt*act_qty);
         $('#sp_grd_ttl').html(unit_amt*act_qty);
         $('#sub_total').val(unit_amt*act_qty);
         $('#grand_total').val(unit_amt*act_qty);
        }
      }
    });

    $('#tax_prct').change(function(event) {
      var sub_total = parseFloat($('#sp_sub_ttl').html());
      var tax_prct = parseFloat($('#tax_prct').val());
      var tax = (sub_total/100)*tax_prct;
      $('#tax_amt').val(tax);
      $('#sp_grd_ttl').html(sub_total+tax);
    });

    $('#shipping_charge').change(function(event) {
      var grand_total = parseFloat($('#sp_grd_ttl').html());
      var shp_chrg = parseFloat($('#shipping_charge').val());
       $('#sp_grd_ttl').html(grand_total+shp_chrg);
    });

/*    $('#missed_qty').blur(function(event) {
      var req_qty = parseInt($('#requested_qty').val());
      var mis_qty = parseInt($('#missed_qty').val());
      if ((mis_qty != '') && (!isNaN(mis_qty))) {
        if (mis_qty > req_qty) {
          alert('Missed quantity can not be greater then requested quantity !');
          $('#missed_qty').val('');
          $('#missed_qty').focus();
        }else{
          rem_qty -= mis_qty;
         $('#actual_qty').val(rem_qty); 
        }
      }
    });*/


    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {

               var chk = 0;
               $('input[name="received_qty[]"]').each(function(index, val) {
                  var val=$(this).val();
                  if(isNaN(val) || (val < 1)){
                    chk++;
                  }
                });

               if (chk > 0) { alert("Quantity should not be blank or zero !"); $('input[name="received_qty[]"]').focus(); }else{
                  form.submit();
               }
            },
            ignore: [],
            rules: {
                name: "required",
                code: "required",
                unit: "required",
                default_sale_unit: "required",
                default_purchase_unit: "required",
                purchase_price: "required",
                selling_price: "required",
                alert_quantity: "required",
                opening_stock: "required",
                opening_stock_value: "required",
            },
            messages:{
                name: "The item name field is required",
                code: "The item code field is required",
                unit: "The unit field is required",
                default_sale_unit: "The default sale unit field is required",
                default_purchase_unit: "The default purchase unit field is required",
                purchase_price: "The purchase price field is required",
                selling_price: "The selling price field is required",
                alert_quantity: "The alert quantity field is required",
                opening_stock: "The opening stock field is required",
                opening_stock_value: "The opening stock value field is required.",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });
});
</script>