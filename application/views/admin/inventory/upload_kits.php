<?php $this->load->view('admin/include/header1'); ?>

 <!-- Datatables -->

<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">

<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">

<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">

<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">



<div class="row">

   <div class="col-md-12 col-sm-12 col-xs-12">

    <?php $this->load->view('admin/inventory/alert/kitUploadAlert'); ?>

	 <div class="x_panel">

        <div class="x_title">

          <h2>Upload Kits</h2>

           <ul class="nav navbar-right panel_toolbox">

            <li>
               <b>PO. No. :</b> <?php echo (isset($podetail) && !empty($podetail->po_no))?$podetail->po_no:''; ?><br>
               <b>Kit Qty. :</b><?php echo (isset($podetail) && !empty($podetail->actual_qty))?$podetail->actual_qty:''; ?>
            </li>

          </ul>

		  <div class="clearfix"></div>

        </div>

		<div class="x_content">

		

		<ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#kitUploadForm">Upload Kits Using Form</a></li>

             <li><a data-toggle="tab" href="#KitUploadExcel">Upload Kit Using Excel File</a></li>

        </ul>

        <div class="tab-content">

            <div id="kitUploadForm" class="tab-pane fade in active">

       

	   		<form id="kitupload" method="post">	
                <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                <input type="hidden" id="request_id" name="request_id" value="<?php echo (isset($podetail))?$podetail->request_id:''; ?>">
                <input type="hidden" id="reference_no" name="reference_no" value="<?php echo (isset($podetail))?$podetail->po_no:''; ?>">
			

			<!-- <div class="col-md-5">

				<div class="form-group">

					<label>Prepared By</label>

					<input type="text" name="prepared_by" value="" class="form-control" />

				</div> 

			</div> -->

		

		

		 <div class="table-responsive">

		 <table id="myTable" class=" table order-list">

             <thead>

              <tr>

                  <td>Bar Code</td>

                  <!-- <td>QR Code</td> -->

             </tr>

             </thead>

             <tbody>
                <?php 
                    if (!empty($kit_list)) {
                        foreach ($kit_list as $kit) { ?>
                <tr id="row_<?php echo $kit->id;  ?>">
                    <td class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" readonly name="barcode[]" value="<?php echo !empty($kit->barcode)?$kit->barcode:''; ?>" />
                        </div>
                    </td>
                    <!-- <td class="col-md-5">
                        <div class="form-group">
                            <input type="text" class="form-control" name="qrcode[]" value="<?php echo !empty($kit->qrcode)?$kit->qrcode:''; ?>" />
                        </div>
                    </td> -->
                    <td class="col-md-2">
                        <!-- <input type="button" id="<?php echo $kit->id; ?>" class="ibtnDel btn btn-md btn-danger" value="Delete"> -->
                    </td>
                </tr>                        
                <?php   }
                    }
                ?>
    </tbody>

    <tfoot>

        <tr>

            <td colspan="5" style="text-align: left;">

                <!--<input type="button" class="btn btn-lg btn-block " id="addrow" value="Add Row" /> -->

            </td>

        </tr>

        </tfoot>

    </table>

</div>

        <div class="col-md-12">

	         <!-- <input type="button" class="btn btn-sm btn-info" id="addrow" value="Add Row" /> -->

	         <!-- <input type="submit" name="saveKitUpload" value="Save" class="btn btn-primary" />	 -->
           <a class="btn btn-primary" href="<?php echo base_url('inventory/upload_kitlist'); ?>">Back</a>

        </div>

    </form>

    </div>

    



	<div id="KitUploadExcel" class="tab-pane fade">

        <div class="form-horizontal">

            <div class="form-group">

                <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Download Sample Excel File: 

                </label>

                <div class="col-md-4 col-sm-6 col-xs-12">

    			 <a href="<?php echo base_url('assets/inventory/inventory_sample.xls') ?>" target="_blank"><img src="<?php echo base_url('assets/images/excel_icon.png'); ?>" width="40px" /></a>

                </div>

		  </div>       

		

		<div class="form-group">								

			<?php echo form_open_multipart('inventory/uploadKit_excel');?>         

            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Excel File:</label>

            <div class="col-md-4 col-sm-6 col-xs-12">                        

			    <input type="file" name="userfile" accept=".xls" />	

            </div>

        </div>	

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

			<input type="submit" value="upload" name="upload" class="btn btn-primary" />

        </div>

			</form>	

			</div>

        </div>

	</div>

</div>



		

		



		

		</div>

     </div>

    </div>

</div>

<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>


<!-- Datatables -->

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>

    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

	

	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>

	<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>

	<script>

	$(document).ready(function () {

    var barcodeArray = new Array();
    $(document).scannerDetection({
      timeBeforeScanTest: 200, // wait for the next character for upto 200ms
      startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
      endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
      avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
      onComplete: function(barcode, qty){ 

        if((jQuery.inArray(barcode,barcodeArray) == -1)){
          var reqid = $('#request_id').val();
          var refno = $('#reference_no').val();
        $.post("<?php echo base_url('inventory/post_scan_barcode'); ?>",{ barcode : barcode,reqid : reqid,refno : refno, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
          if(response.status){

            var newRow = $("<tr id='row_"+response.insert_id+"'>");
            var cols = "";
            cols += '<td id="row_'+response.insert_id+'" class="col-md-5"><div class="form-group"><input type="text" readonly class="form-control" name="barcode[]" id="barcode_'+response.insert_id+'" value="'+barcode+'" /></div></td>';
            /*cols += '<td class="col-md-5"><div class="form-group"><input type="text" class="form-control" id="qrcode_'+response.insert_id+'" name="qrcode[]"/></div></td>';*/
            //cols += '<td class="col-md-2"><input type="button" class="ibtnDel btn btn-md btn-danger" value="Delete" id="'+response.insert_id+'"></td>';
            newRow.append(cols);
            
            barcodeArray.push(barcode);
            $("table.order-list").append(newRow);
            console.log(response);

          }else{
            alert(response.msg);
          }
        },"json");
        }else{
          alert("Barcode - "+barcode+" already exist !");
        }

       } 
    });

    var counter = 0;



    $("#addrow").on("click", function () {

        var newRow = $("<tr>");

        var cols = "";

        //cols += '<td class="col-md-5"><div class="form-group"><input type="text" class="form-control" name="qrcode[]"/></div></td>';

        cols += '<td class="col-md-5"><div class="form-group"><input type="text" class="form-control" name="barcode[]"/></div></td>';

        cols += '<td class="col-md-2"><input type="button" class="ibtnDel btn btn-md btn-danger"  value="Delete"></td>';

        newRow.append(cols);

        $("table.order-list").append(newRow);

        counter++;

		//var qrcode = $(newRow).find('[name="qrcode[]"]');

		var barcode = $(newRow).find('[name="barcode[]"]');

		

		$('#kitupload').bootstrapValidator('addField', barcode);

		$('#kitupload').bootstrapValidator('addField', qrcode);

   });


    $(document).on('click','.ibtnDel', function(event) {
       var id = this.id;

       $.post("<?php echo base_url('inventory/remove_barcode'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(response) 
       {
          var response = $.parseJSON(response);
          if(response.status){
                barcode = $('#barcode_'+id).val();
                barcodeArray.splice( $.inArray(barcode, barcodeArray), 1 );
                $('#row_'+id).remove();
                $('#kitupload').bootstrapValidator('removeField', $('#barcode_'+id));

               // $('#kitupload').bootstrapValidator('removeField', $('#qrcode_'+id));            
          }
       });
    });





});







function calculateRow(row) {

    var price = +row.find('input[name^="price"]').val();



}



function calculateGrandTotal() {

    var grandTotal = 0;

    $("table.order-list").find('input[name^="price"]').each(function () {

        grandTotal += +$(this).val();

    });

 }









$(document).ready(function(){

	

	  $('#kitupload')

        .bootstrapValidator({

			  feedbackIcons: {

                valid: 'glyphicon glyphicon-ok',

                invalid: 'glyphicon glyphicon-remove',

                validating: 'glyphicon glyphicon-refresh'

            },

            fields: {

				'barcode[]': {

                    validators: {

                        notEmpty: {

                            message: 'The option required and cannot be empty'

                        },

                        stringLength: {

                            max: 100,

                            message: 'The option must be less than 100 characters long'

                        }

                    }

                }

            }

        })

});

	

	</script>