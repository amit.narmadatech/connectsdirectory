<html>
    <head>
        <title>pdf html</title>
        <style>
            .custom-btn {
       padding: 10px 33px;
    font-size: 15px;
    color: #fff;
    box-shadow: 1px 1px 11px rgba(98, 129, 157, 0.4);
    border-radius: 40px;
    display: inline-block;
    text-transform: uppercase;
    /* background-color: #20a3f0; */
    background: -moz-linear-gradient(45deg, #3ac0e9 0%, #2d84c5 100%);
    background: linear-gradient(45deg, #3ac0e9 0%, #2d84c5 100%);
    border: none;
    outline: none;
    margin-bottom: 15px;
}
        </style>
    </head>
    <body style="text-align: center;">
        <div><button class="custom-btn" onclick="printDiv()">Print</button></div>
        <div id="slipdata">
        <?php if(!empty($slipdata['packages'])){ 
            //print_r($slipdata);die;
        ?>
        <?php foreach($slipdata['packages'] as $package){ ?>
           
        <table align="center" border="1" cellpadding="0" cellspacing="0" width="560" style="font-family: sans-serif;">
            <tr>
                <td style="padding: 16px 0px; text-align: center; width:50%;"><img src="<?php echo base_url("assets/images/logo.png"); ?>"></td>
                <td style="padding: 16px 0px; text-align: center; width:50%;"><img src="<?php echo $package['delhivery_logo'] ; ?>"></td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="border-bottom:0px; padding: 16px 60px;">
                    <img src="<?php echo $package['barcode'] ; ?>"> 
                </td>
            </tr>	
            <tr>
                <td style="border-top:0px; border-right:0px; font-size: 20px;"><?php echo $package['pin'] ; ?></td>
                <td style="border-top:0px; border-left:0px; font-size: 25px; font-weight: 600; text-align: right;"><?php echo $package['sort_code'] ; ?></td>
            </tr>
            <tr>
                <td style="padding:10px 2px; font-size:16px;">
                    <p style="margin: 0;margin-bottom: 10px;"><b>Shipping Address:</b></p>
                    <p style="margin: 0;margin-bottom: 10px;"><b><?php echo $package['name'] ; ?></b></p>
                    <p style="margin: 0;margin-bottom: 10px;">Phone: <?php echo $package['contact'] ; ?></p>
                    <p style="margin: 0;margin-bottom: 10px;">Address: <?php echo $package['address'] ; ?> 
                        <br><?php echo $package['destination_city'] ; ?>(<?php echo $package['customer_state'] ; ?>)</p>
                    <p style="margin: 0;">PIN: <?php echo $package['pin'] ; ?></p>
                </td>
                <td style="padding:10px 2px; font-size:16px;">
                    <p style="margin: 0;margin-bottom: 10px;"><b>Billing Address:</b></p>
                    <p style="margin: 0;margin-bottom: 10px;"><b><?php echo $package['name'] ; ?></b></p>
                    <p style="margin: 0;margin-bottom: 10px;">Phone: <?php echo $package['contact'] ; ?></p>
                    <p style="margin: 0;margin-bottom: 10px;">Address: <?php echo $package['address'] ; ?> 
                        <br><?php echo $package['destination_city'] ; ?> (<?php echo $package['customer_state'] ; ?>)</p>
                    <p style="margin: 0;">PIN: <?php echo $package['pin'] ; ?></p>
                </td>
                
            </tr>
            <tr>
                <td colspan="2" style="padding:10px 2px; font-size:16px;">
                    <p><b>Sold By</b></p>
                    <p style="margin: 0;margin-bottom: 10px;">Name: <?php echo $package['snm']  ?></p>
                    
                    <p style="margin: 0;">Address:<?php echo $package['sadd']  ?></p>
                </td>
               
            </tr>
            <tr>
                <td colspan="2">
                    <table  cellpadding="0" cellspacing="0" style="width:100%;">
                        <tr>
                            <td style="border-bottom:1.5px solid #444; border-right:1.5px solid #444; padding: 10px 2px;">Product</td>
                            <td style="border-bottom:1.5px solid #444; border-right:1.5px solid #444; padding: 10px 2px; text-align:center;">Price</td>
                            <td style="border-bottom:1.5px solid #444; padding: 10px 2px; text-align:center;">Total</td>
                        </tr>
                        <tr>
                            <td style="padding:10px 2px; font-size:16px; border-bottom:1.5px solid #444; border-right:1.5px solid #444;">
                                <p style="margin: 0;margin-bottom: 10px;"><?php echo $package['prd'] ?></p>
                                
                            </td>
                            <td style="border-bottom:1.5px solid #444; border-right:1.5px solid #444; padding: 10px 2px; text-align:center;">&#x20B9;<?php echo number_format($package['rs'],2)  ?></td>
                            <td style="border-bottom:1.5px solid #444; padding: 10px 2px; text-align:center;">&#x20B9;<?php echo number_format($package['rs'],2)  ?></td>
                        </tr>
                        <tr>
                            <td style="padding:10px 2px; font-size:16px;  border-right:1.5px solid #444;"><b>Total</b></td>
                            <td style=" border-right:1.5px solid #444; padding: 10px 2px; text-align:center;"><b>&#x20B9;<?php echo number_format($package['rs'],2)  ?></b></td>
                            <td style="padding: 10px 2px; text-align:center;"><b>&#x20B9;<?php echo number_format($package['rs'],2)  ?></b></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table  cellpadding="0" cellspacing="0" style="width:100%;">
                        <tr>
                            <td style="padding: 15px 16px;"><img src="<?php echo $package['oid_barcode'];  ?>"></td>
                            <td>Return Address: <?php echo $package['radd']  ?>
                                <?php echo $package['rcty']  ?> - <?php echo $package['rst']  ?> - <?php echo $package['rpin']  ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <?php } ?>
        <?php } ?>
        </div>
    </body>
</html>
<script>
    function printDiv() {
        var printContents = document.getElementById('slipdata').innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
   }
</script>