<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
  .form-control{
    width: 50%;
    display: inline;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Delhivery Dispatch Form</h2>        
        <div class="clearfix"></div>
      </div>
      <input type="hidden" id="dis_id" value="<?php echo $edit->id; ?>">
      <input type="hidden" id="order_id" value="<?php echo $edit->order_id; ?>">
      <div class="x_content">
        <div id="ele2" class="b">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  # <?php echo $edit->order_number; ?>
                  <small class="pull-right">Date: <?php echo date("d/m/Y",strtotime($edit->order_date)); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-3 invoice-col">
                <h4><b>Vendor</b></h4>
                <address>
                    <strong><?php echo $wh->title; ?> [<?php echo $wh->code; ?>]</strong>
                    <br><?php echo $wh->contact_person; ?>
                    <br><?php echo $wh->address; ?>
                    <br><?php echo !empty(getDistrict($wh->district_id)) ? getDistrict($wh->district_id)->district_title : ''; ?>,
                    <?php echo !empty(getState($wh->state_id)) ? getState($wh->state_id)->state_title : ''; ?>
                    <br>Phone: <?php echo $wh->contact1; ?> <?php echo ", ".$wh->contact2; ?>
                    <br>Email: <?php echo $wh->email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                    <strong><?php echo $edit->customer_name; ?></strong>
                    <br><?php echo $edit->shipping_address; ?>
                    <br><?php echo !empty($edit->shipping_city) ? $edit->shipping_city : ''; ?>, <?php echo !empty($edit->shipping_district) ? $edit->shipping_district : ''; ?>, <?php echo !empty($edit->shipping_state) ? $edit->shipping_state : ''; ?> <?php echo $edit->shipping_pincode; ?>
                    <br>Phone: <?php echo $edit->customer_contact; ?>
                   <br>Email: <?php echo $edit->customer_email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                <div id="generatePackageDiv">
                  <?php if (empty($edit->tracking_code)) { ?>
                    <button type="button" class="btn btn-lg btn-primary" id="generatePackage">Generate Package</button><br><br><br><br>
                  <?php }else{ ?>
                    <a target="_blank" href='<?php echo base_url("inventory/printDelhiverySlip/$edit->order_id"); ?>' class="btn btn-success btn-lg">Package Slip</a>
                  <?php } ?>
                </div>

                <div id="reversePickupDiv">
                  <?php if (empty($edit->rev_tracking_code)) { ?>
                    <button type="button" class="btn btn-lg btn-primary" id="reversePickup">Reverse Pickup</button><br><br><br><br>
                  <?php }else{ ?>
                  <button class="btn btn-success">Tracking Code : <?php echo $edit->rev_tracking_code; ?></button>
                  <?php } ?>
                </div>
                
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                <h4><b>Pickup Request</b></h4>
                <!-- <address>
                    <strong><?php echo $edit->customer_name; ?></strong>
                    <br><?php echo $edit->shipping_address; ?>
                    <br><?php echo !empty($edit->shipping_city) ? $edit->shipping_city : ''; ?>, <?php echo !empty($edit->shipping_district) ? $edit->shipping_district : ''; ?>, <?php echo !empty($edit->shipping_state) ? $edit->shipping_state : ''; ?> <?php echo $edit->shipping_pincode; ?>
                    <br>Phone: <?php echo $edit->customer_contact; ?>
                   <br>Email: <?php echo $edit->customer_email; ?>
                </address> -->
                <form method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/pickupRequestDelhivery'); ?>" >
                      <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

                        <div class="form-group">
                          <label for="pickup_date">Pickup Date</label>
                          <input required type="date" class="form-control" id="pickup_date" name="pickup_date">
                        </div>
                        <div class="form-group">
                          <label for="pickup_time">Pickup Time</label>
                          <input required type="time" class="form-control" id="pickup_time" name="pickup_time">
                        </div>
                        <button type="submit" class="btn btn-md btn-primary">Pickup Request</button>
                      

                   </form>
              </div>
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                
                <!-- <a href="<?php echo base_url('inventory/stock_list'); ?>"  class="btn btn-primary pull-right" id="print_po"><i class="fa fa-arrow-left"></i> Back</a> -->
              </div>
            </div>
          </section>
        </div>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>

<script>
    jQuery(document).ready(function($) {

    $(document).on('click', '#generatePackage', function(event) {
        var order_id = $('#order_id').val();
        var dis_id = $('#dis_id').val();
        $('#generatePackage').attr('disabled', true);
        $.post('<?php echo base_url("inventory/delhivery_generate_package"); ?>', {order_id: order_id,dis_id: dis_id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
          $('#generatePackage').attr('disabled', true);
          if (data.status){
            var url =  '<?php echo base_url('inventory/printDelhiverySlip/'); ?>/'+order_id;
            $('#generatePackageDiv').html('<a target="_blank" href="'+url+'" class="btn btn-success btn-lg">Package Slip</a>');
          }else{
            alert(data.msg);
          }

        },"json");

    });

    $(document).on('click', '#reversePickup', function(event) {
        var order_id = $('#order_id').val();
        var dis_id = $('#dis_id').val();
        $('#reversePickup').attr('disabled', true);
        $.post('<?php echo base_url("inventory/delhivery_reverse_package"); ?>', {order_id: order_id,dis_id: dis_id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
          $('#reversePickup').attr('disabled', false);
          if (data.status){
            $('#reversePickupDiv').html('<button class="btn btn-success">'+data.trackingcode+'</button>');
          }else{
            alert(data.msg);
          }

        },"json");

    });

    $('#form_validation').validate({
            submitHandler: function(form) {
               form.submit();
            },
            ignore: [],
            rules: {
                pickup_date: "required",
                pickup_time: "required",
                },
            messages:{
                pickup_date: "Pickup date field is required",
                pickup_time: "Pickup time field is required",
            },
            highlight: function (input) {
                $(input).closest('div').addClass('has-error');
            },
        });

   
});
</script>