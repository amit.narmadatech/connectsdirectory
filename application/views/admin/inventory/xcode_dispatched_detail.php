<?php $this->load->view('admin/include/header1'); ?>

<div class="row">
   <div class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2">
      <div class="print_panel">
        <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0 0 15px; border: solid 1px #000; ">   
         <tr>
          <td style="width: 50%" colspan=""> 
            <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0;">
              <tr>
                <td style="padding: 10px;"> 
                  From
                </td>
              </tr>
              <tr>
                <td style="padding: 10px;"> 
                   Xcode Life Sciences Private Limited <br>
                   5-A, A Block, “Anugraha”, New No 41, Nungambakkam High Road <br>
                   Nungambakkam, Chennai – 600034, Tamilnadu, INDIA               
                </td>
              </tr>
              <tr>
                <td style="padding: 10px;"> 
                  Mobile : XXXXXXXXXXXXXX  <br>
                  Email: test@gmail.com               
                </td>
              </tr>
            </table>
          </td>         
          <td  valign="top" style="border-left: solid 1px #000;"> 
            <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0;">
              <tr>
                <td style="padding:20px 10px; text-align: right;"> 
                  Invoice Date: <?php echo (isset($detail) && !empty($detail->created_at))?date("D d M Y",strtotime($detail->created_at)):''; ?> <br> 
                  Invoice No : <?php echo (isset($detail) && !empty($detail->invoice_no))?$detail->invoice_no:''; ?>
                </td>
              </tr>
              <tr>
                <td style="padding:20px 10px; text-align: right; border-bottom: solid 1px #000; border-top: solid 1px #000; "> 
                  Shipping Company : <?php echo (isset($detail) && !empty($detail->shipping_partner))?$detail->shipping_partner:''; ?> <br> 
                  Tracking No : <?php echo (isset($detail) && !empty($detail->tracking_no))?$detail->tracking_no:''; ?>                 
                </td>
              </tr>
              <tr>
                <td style="padding: 15px 10px; text-align: right;"> 
                  Purchase Order No : <?php echo (isset($detail) && !empty($detail->reference_no))?$detail->reference_no:''; ?>                 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
        <ul class="qr-list">
        <?php 
        	if (!empty($barcode_list)) {
        		foreach ($barcode_list as $barcode) { ?>	
        		<li><?php echo $barcode->barcode; ?></li>
        	<?php	}
        	}
        ?>           
      </ul>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
