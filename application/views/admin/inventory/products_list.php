<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Item List</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?= base_url('inventory/add_product'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Item</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">      
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th width="5%">Sr.No.</th>
                <th>Item</th>
                <th>Code</th>
                <th>Unit</th>
                <th>Purchase Price</th>
                <th>Selling Price</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                if(!empty($product_list)){
                $i=1;                     
                  foreach($product_list as $pdlist){ ?>
                    <tr>
                      <!-- <td><?php if (file_exists("uploads/inventory/product/".$pdlist->image) && !empty($pdlist->image)){ $href = base_url('uploads/inventory/product/').$pdlist->image; }else{ $href= base_url('assets/images/kit.png'); } ?>
                          <img src="<?php echo $href; ?>" class="img-responsive" width="40" height="40">
                      </td> -->
                      <td class="text-center"><?php echo $i++; ?></td>
                      <td><?php  echo !empty($pdlist->name) ? $pdlist->name : '' ?></td>
                      <td><?php  echo !empty($pdlist->code) ? $pdlist->code : '' ?></td>
                      <td><?php  echo !empty($pdlist->unit) ? $pdlist->unit : '' ?></td>
                      <td class="text-right"><?php  echo !empty($pdlist->purchase_price) ? $pdlist->purchase_price : '' ?></td>
                      <td class="text-right"><?php  echo !empty($pdlist->selling_price) ? $pdlist->selling_price : '' ?></td>
                      <td><?php  echo !empty($pdlist->created_at) ?date("D, d M Y",strtotime($pdlist->created_at)): '' ?></td>
                      <td><?php  echo ($pdlist->status == 1)?'Active':'Inactive'; ?></td>
                      <td><a href="<?php echo base_url('inventory/add_product/'.$pdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a></td>
                    </tr>
            <?php } } ?>
          </tbody>
        </table>      
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    