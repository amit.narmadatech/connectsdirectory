<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Damge Stock Summary</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?= base_url('inventory/damage_stock'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> New</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
           <div class="table-responsive">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th></th>
                <th>P.O. No.</th>
                <th>Supplier</th>
                <th>Warehouse</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>


            <tbody>
              <?php 
                $total_qty = 0;
                $i = 1;
               if(!empty($stock_list)){
                foreach($stock_list as $slist){ ?>
                <tr>
                  <td align="center"><?php echo $i++; ?></td>
                  <td><?php  echo !empty($slist->po_no) ? $slist->po_no : '' ?></td>
                  <td><?php  echo !empty($slist->sp_name) ? $slist->sp_name : '-' ?></td>
                  <td><?php  echo !empty($slist->w_name) ? $slist->w_name : '-' ?></td>
                  <td><?php  echo !empty($slist->created_at) ? date("D, d M Y",strtotime($slist->created_at)) : '' ?></td>
                  <td>
                    <button type="button" id="<?php echo $slist->id; ?>" class="btn btn-info btn-xs details" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-eye"></i> View</button>
                  </td>
                </tr>
              <?php } } ?>
            </tbody>

          </table>
        </div>
        </div>
      </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Damage Stock List</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered b">
              <tr>
                <th width="10">#</th>
                <th>Code</th>
                <th>Reason</th>
              </tr>
              <tbody id="detail_div">
                
              </tbody>
              
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="print_btn">Print</button>
          </div>

        </div>
      </div>
    </div>

</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= base_url("assets/js/print/jQuery.print.js"); ?>"></script>
    <script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    
    $('#print_btn').click(function(event) {
      $.print(".b");
    });
    
    $(".details").click(function(event) {
        var id = this.id;
          $.post("<?php echo base_url('inventory/get_damage_stock_detail'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response){
                var rows = '';
                var i =1;
                $.each(response, function(index, val) {
                   rows += '<tr><td>'+i+++'</td><td>'+val.code+'</td><td>'+val.reason+'</td></tr>';
                });
                console.log(rows);
                $('#detail_div').html(rows);
              }else{
                console.log(response);
              }
          },"json");
    });

});
</script>