<?php $this->load->view('admin/include/header1'); ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Stock Out</h2>
        <ul class="nav navbar-right panel_toolbox">

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="form_validation" name="form_validation" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" action="<?= base_url('inventory/post_stock_out'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <input type="hidden" name="customer_id" value="<?= isset($edit)?$edit->user_id:''; ?>">
       
       <div class="row">
        <div class="col-sm-6">            
           <div class="form-group">
             <label class="col-md-4 col-sm-3 col-xs-12" for="order_num">Order Number <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-6 col-xs-12">
               <input type="text" id="order_num" name="order_num" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->order_number))))?$edit->order_number:''; ?>">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <button id="get_stock" class="btn btn-info waves-effect m-t-10" type="button">FIND</button> 
             <a href="<?php echo base_url('inventory/stock_out'); ?>" class="btn btn-default waves-effect m-t-10">RESET</a> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">            
          <div class="form-group">

            <label for="customer_name" class="col-md-4 col-sm-3 col-xs-12">Customer Name <span class="required">*</span></label>
              <div class="col-md-8 col-sm-6 col-xs-12">
                <input type="text" name="customer_name" id="customer_name" value="<?= (isset($edit) && (!empty(($edit->c_name))))?$edit->c_name:set_value('customer_name'); ?>" class="form-control">
              </div>
            </div>
          </div> 
        
          <div class="col-sm-6">            
              <div class="form-group">
                <label for="city" class="col-md-4 col-sm-3 col-xs-12">City <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="city" id="city" value="<?= (isset($edit) && (!empty(($edit->city))))?$edit->city:set_value('city'); ?>" class="form-control">
                </div>
              </div>
            </div>
          </div>
              
         <div class="row">
            <div class="col-sm-6">            
              <div class="form-group">
                <label for="customer_email" class="col-md-4 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="customer_email" id="customer_email" value="<?= (isset($edit) && (!empty(($edit->email))))?$edit->email:set_value('customer_email'); ?>" class="form-control">
                </div>
              </div>
            </div>

            <div class="col-sm-6">            
              <div class="form-group">
                <label for="district" class="col-md-4 col-sm-3 col-xs-12">District <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="district" id="district" value="<?php echo (isset($edit) && !empty(getDistrict($edit->district_id))) ? getDistrict($edit->district_id)->district_title : ''; ?>" class="form-control">
                </div>
              </div>
            </div>
           </div>

          <div class="row">
            <div class="col-sm-6">            
              <div class="form-group">
                <label for="customer_contact" class="col-md-4 col-sm-3 col-xs-12">Contact <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="customer_contact" id="customer_contact" value="<?= (isset($edit) && (!empty(($edit->contact1))))?$edit->contact1:set_value('customer_contact'); ?>" class="form-control">
                </div>
              </div>
            </div>

            <div class="col-sm-6">            
              <div class="form-group">
                <label for="state" class="col-md-4 col-sm-3 col-xs-12">State <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="state" id="state" value="<?php echo (isset($edit) && !empty(getState(getDistrict($edit->district_id)->state_id))) ? getState(getDistrict($edit->district_id)->state_id)->state_title : ''; ?>" class="form-control">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-6">            
              <div class="form-group">
                <label for="order_date" class="col-md-4 col-sm-3 col-xs-12">Order Date <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="order_date" id="order_date" value="<?= (isset($edit) && (!empty(($edit->order_date))))?$edit->order_date:set_value('order_date'); ?>" class="form-control">
                </div>
              </div>
            </div>

            <div class="col-sm-6">            
              <div class="form-group">
                <label for="pincode" class="col-md-4 col-sm-3 col-xs-12">Pincode <span class="required">*</span></label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="pincode" id="pincode" value="<?= (isset($edit) && (!empty(($edit->pincode))))?$edit->pincode:set_value('pincode'); ?>" class="form-control">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
               <div class="col-sm-6">            
                <div class="form-group">  
                  <label for="shipping_partner" class="col-md-4 col-sm-3 col-xs-12">Shipping Partner <span class="required">*</span></label>
                  <div class="col-md-8 col-sm-6 col-xs-12">
                    <select class="form-control" name="shipping_partner" id="shipping_partner">
                       <option value="">Select Shipping Partner </option>
                       <option value="delhivery">Delhivery</option>
                       <option value="dtdc">DTDC</option>
                       <option value="manual">Manual</option>
                    </select>
                  </div>
                 </div>  
                </div>

               <div class="col-sm-6">      
                 <div class="form-group">
                  <label for="shipping_address" class="col-md-4 col-sm-3 col-xs-12">Shipping Address <span class="required">*</span></label>
                 <div class="col-md-8 col-sm-6 col-xs-12">
                      <textarea name="shipping_address" id="shipping_address" rows="2" class="form-control" ><?php echo (isset($edit) && (!empty(($edit->address))))?$edit->address:''; ?><?php echo (isset($edit) && (!empty(($edit->bl_landmark))))?", ".$edit->bl_landmark:''; ?> <?php echo (isset($edit) && (!empty(($edit->bl_locality))))?", ".$edit->bl_locality:''; ?></textarea>
                   </div>
                 </div>
               </div>
             </div>        
          </div>
       </div>
     </div>


        <div class="row clearfix">
          <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <td align="center" width="5">#</td>
                                <td align="center">Plan</td>
                                <td align="center">Kit Bar Code</td>
                                <td align="center">Amount</td>
                            </tr>
                            <tbody id="item_table">
                            <?php 
                            $sub_total = 0;$qty=0;$taxprnct=0;$taxamt=0;
                            if (!empty($edit_details)) {
                              $i=0;
                                foreach ($edit_details as $edetail) { 
                                  $qty += $edetail->quantity;
                                  $taxamt += $edetail->tax_amt;
                                  for ($j=0; $j < $edetail->quantity; $j++) { 
                                ?>
                                 <tr>
                                     <td align="center"><?php echo ++$i; ?></td>
                                     <td align="center"><?php echo $edetail->plan_name; ?>
                                       <input type="hidden" name="plan[]" value="<?php echo $edetail->plan_name; ?>">
                                       <input type="hidden" name="planid[]" value="<?php echo $edetail->plan_id; ?>">
                                     </td>
                                     <td>
                                      <input type="text" name="kit[]" id="kitcode<?php echo $i; ?>" class="form-control kit" value="">
                                     </td>
                                     <td >
                                      <?php 
                                        echo $edetail->plan_price; 
                                        $sub_total += $edetail->plan_price; 
                                      ?>
                                       <input type="hidden" name="kitamount[]" class="form-control" value="<?php echo $edetail->plan_price; ?>">
                                     </td>
                                 </tr>   
                            <?php  }  }
                            }
                            ?> 
                            </tbody>
                                <tr>
                                  <td rowspan="5" colspan="2">
                                    <label class="col-md-12">Note:</label>
                                    <textarea class="form-control" rows="5" name="note" id="note"></textarea>
                                  </td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Sub-Total</b></td>
                                    <td><input readonly type="text" id="sub_total" name="sub_total" class="form-control" value="<?php echo $sub_total; ?>"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Tax</b></td>
                                    <td><input readonly type="text" id="tax_amount" name="tax_amount" class="form-control" value="<?php echo $taxamt; ?>"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Total</b></td>
                                    <td>
                                      <input readonly type="text" id="grand_total" name="grand_total" class="form-control" value="<?php echo $sub_total+$taxamt; ?>">
                                      <input type="hidden" name="quantity" value="<?php echo $qty; ?>">
                                    </td>
                                </tr>

                        </thead>
                    </table>
                </div>
            </div>
        </div>

          <div class="form-group">
           <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
              <button class="btn btn-danger" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {

     
    $(document).on('blur change', '.kit', function(event) {
        var kitcodes = new Array(); 
        var obj = this;
        var code = $.trim(obj.value);
        if(code){
          $('#submitBtn').attr('disabled', true);
          $.post("<?php echo base_url('kit/verifykitcode'); ?>",{ code : code, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              $('#submitBtn').attr('disabled', false);
              var id = $(obj).attr('id'); 

              if(!response){
                 if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Invalid barcode.</span>').insertAfter(obj);
                    $(obj).val('');
                 } 
              }else
              {
                $('input[name="kit[]"]').each(function(index, val) {
                    console.log($(this).id);
                    console.log(val.id);
                    if((val.id != obj.id) && ($(this).val() != "")){
                      kitcodes.push($(this).val());
                    }
                });

                if ($.inArray(code, kitcodes)  == -1) {
                  $(obj).next('span').remove();
                }else{
                  if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Invalid barcode.</span>').insertAfter(obj);
                    
                    $("#"+id).val('');
                 }                  
                }

              }
          },"json");
        }else{
          $(obj).next('span').remove();
        }
        
    });


    $('#get_stock').click(function(event) {
      var onum = $('#order_num').val();
      if (onum != "") { window.location.href="<?php echo base_url('inventory/get_order_by_ordernumber/') ?>"+onum; }else{ alert('Please enter some value !'); $('#order_num').focus(); }
    });;


    $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                $('input[name="kit[]"]').each(function(index, val) {
                  if ($(this).val() == "") { alert('All QR-Code fields are required !'); return false;}else{ form.submit(); }
                });

            },
            ignore: [],
            rules: {
                shipping_address: "required",
                order_num: "required",
                customer_name: "required",
                customer_email: {
                  required:true,
                  email:true
                },
                customer_contact: {
                  required:true,
                  number:true,
                  minlength:10,
                  maxlength:10,
                },
                city: "required",
                district: "required",
                state: "required",
                pincode: {
                  required:true,
                  number:true,
                },
                order_date: "required",
                shipping_partner: "required",
                sub_total: "required",
                grand_total: "required",
                
            },
            messages:{
                title: "The title field is required",
                code: "The warehouse code field is required",
                contact_person: "The contact person field is required",
            },
            highlight: function (input) {
                $(input).closest('div').addClass('has-error');
            },
        });

    });

});
</script>
