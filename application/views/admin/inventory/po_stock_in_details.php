<?php $this->load->view('admin/include/header1'); ?>
<style type="text/css">
  .form-control{
    width: 50%;
    display: inline;
  }
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Stock-IN Details</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="ele2" class="b">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  # <?php echo $po_request->reference_no; ?>
                  <small class="pull-right">Date: <?php echo date("d/m/Y",strtotime($po_request->purchase_date)); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <h4><b>Vendor</b></h4>
                <address>
                    <strong><?php echo $po_request->s_name; ?></strong>
                    <br><?php echo $po_request->s_address; ?>
                    <br>Phone: <?php echo $po_request->s_contact1; ?> <?php echo ", ".$po_request->s_contact2; ?>
                    <br>Email: <?php echo $po_request->s_email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                    <strong><?php echo $po_request->w_name; ?>, [ <?php echo $po_request->w_code; ?> ]</strong>
                    <br><?php echo $po_request->w_address; ?>
                    <br><?php echo !empty(getDistrict($po_request->district_id)) ? getDistrict($po_request->district_id)->district_title : ''; ?>, <?php echo !empty(getState(getDistrict($po_request->district_id)->state_id)) ? getState(getDistrict($po_request->district_id)->state_id)->state_title : ''; ?> <?php echo $po_request->w_pincode; ?>
                    <br>Phone: <?php echo $po_request->w_contact1; ?> <?php echo ", ".$po_request->w_contact2; ?>
                   <br>Email: <?php echo $po_request->w_email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>PO Number #<?php echo $po_request->po_no; ?></b>
                <br>
                <br>
                <b>PO Date:</b> <?php echo date("d/m/Y",strtotime($po_request->created_at)); ?>
                <br>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                 <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th width="20%">Item</th>
                      <th>Req. Qty</th>
                      <th>Rec. Qty</th>
                      <th>Damaged Qty</th>
                      <th>Miss. Qty</th>
                      <th>Actual Qty</th>
                      <!-- <th>Unit Price</th>
                      <th class="text-right">Amount</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if (!empty($po_request_details)) {
                      $i = 1;$sub_total = 0;
                      foreach ($po_request_details as $detail) { ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $detail->product_name; ?> [ <?php echo $detail->product_code; ?> ]</td>
                      <td><?php echo $detail->requested_qty; ?></td>
                      <td><?php echo $detail->received_qty; ?></td>
                      <td><?php echo $detail->damaged_qty; ?></td>
                      <td><?php echo $detail->missed_qty; ?></td>
                      <td><?php echo $detail->actual_qty; ?></td>
                      <!-- <td><span id="sp_unit_amt"><?php echo $detail->rate; ?></span></td>
                      <td  class="text-right"><span id="sp_cur_amt"><?php echo $detail->total_amount; $sub_total += $detail->total_amount; ?></span></td>
                       -->
                    </tr>    
                    <?php  }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
                <!-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;min-height: 115px;">
                  <?php echo $po_request->description; ?>
                </p> -->
               <!--  <textarea readonly id="description" name="description" class="text-muted well well-sm no-shadow form-control" style="margin-top: 10px;height: 115px;width: 545px;" rows="10"><?php // echo $po_request->description; ?></textarea> -->

                <p  id="description" name="description" class="text-muted well well-sm no-shadow" style="margin-top: 10px;min-height: 115px;"><?php echo $po_request->description; ?></p>
              </div>
              <!-- /.col -->
              <!-- <div class="col-xs-6">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td  class="text-right"><span id="sp_sub_ttl"><?php echo $po_request->sub_total; ?></span></td>
                      </tr>
                      <tr>
                        <th>Tax %  <?php echo $po_request->tax_percentage; ?></th>
                        <td><?php echo $po_request->tax_amount; ?></td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td><?php echo $po_request->shipping_charge; ?></td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td class="text-right"> <span id="sp_grd_ttl"><?php echo $po_request->grand_total; ?></span><input type="hidden" name="grand_total" id="grand_total"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div> -->
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                <button class="btn btn-default" id="print_po"><i class="fa fa-print"></i> Print</button>
                <a href="<?php echo base_url('inventory/stock_list'); ?>"  class="btn btn-primary pull-right" id="print_po"><i class="fa fa-arrow-left"></i> Back</a>
              </div>
            </div>
          </section>
        </div>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>

<script>
    jQuery(document).ready(function($) {

    $('#print_po').click(function(event) {
      $.print(".b");
    });

    var rem_qty = parseInt($('#requested_qty').val());
    
    $('#received_qty').blur(function(event) {
      var req_qty = parseInt($('#requested_qty').val());
      var rec_qty = parseInt($('#received_qty').val());
      $('#damaged_qty').val(0);
      $('#missed_qty').val(0);
      $('#actual_qty').val(0);
      if ((rec_qty != '') && (!isNaN(rec_qty))) {
        if (rec_qty > req_qty) {
          alert('Received quantity can not be greater then requested quantity !');
          $('#received_qty').val(0);
          $('#received_qty').focus();
        }else{
          rem_qty = parseInt(req_qty - rec_qty);
          $('#missed_qty').val(rem_qty);
          $('#actual_qty').val(rec_qty);
          var unit_amt = parseFloat($('#sp_unit_amt').html());
          var act_qty = parseInt($('#actual_qty').val());
          $('#sp_cur_amt').html(unit_amt*act_qty);
          $('#sp_sub_ttl').html(unit_amt*act_qty);
          $('#sp_grd_ttl').html(unit_amt*act_qty);
          $('#sub_total').val(unit_amt*act_qty);
          $('#grand_total').val(unit_amt*act_qty);
        }
      }
    });

    $('#damaged_qty').blur(function(event) {
      var req_qty = parseInt($('#requested_qty').val());
      var rec_qty = parseInt($('#received_qty').val());
      var dam_qty = parseInt($('#damaged_qty').val());
      if ((dam_qty != '') && (!isNaN(dam_qty))) {
        if (dam_qty > rec_qty) {
          alert('Damaged quantity can not be greater then received quantity !');
          $('#damaged_qty').val(0);
          $('#damaged_qty').focus();
        }else{
          rem_qty -= dam_qty;
         $('#missed_qty').val(req_qty-rec_qty); 
         $('#actual_qty').val(rec_qty-dam_qty);
         var unit_amt = parseFloat($('#sp_unit_amt').html());
         var act_qty = parseInt($('#actual_qty').val());
         $('#sp_cur_amt').html(unit_amt*act_qty);
         $('#sp_sub_ttl').html(unit_amt*act_qty);
         $('#sp_grd_ttl').html(unit_amt*act_qty);
         $('#sub_total').val(unit_amt*act_qty);
         $('#grand_total').val(unit_amt*act_qty);
        }
      }
    });

    $('#tax_prct').change(function(event) {
      var sub_total = parseFloat($('#sp_sub_ttl').html());
      var tax_prct = parseFloat($('#tax_prct').val());
      var tax = (sub_total/100)*tax_prct;
      $('#tax_amt').val(tax);
      $('#sp_grd_ttl').html(sub_total+tax);
    });

    $('#shipping_charge').change(function(event) {
      var grand_total = parseFloat($('#sp_grd_ttl').html());
      var shp_chrg = parseFloat($('#shipping_charge').val());
      $('#sp_grd_ttl').html(grand_total+shp_chrg);
    });

/*    $('#missed_qty').blur(function(event) {
      var req_qty = parseInt($('#requested_qty').val());
      var mis_qty = parseInt($('#missed_qty').val());
      if ((mis_qty != '') && (!isNaN(mis_qty))) {
        if (mis_qty > req_qty) {
          alert('Missed quantity can not be greater then requested quantity !');
          $('#missed_qty').val('');
          $('#missed_qty').focus();
        }else{
          rem_qty -= mis_qty;
         $('#actual_qty').val(rem_qty); 
        }
      }
    });*/


    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                name: "required",
                code: "required",
                unit: "required",
                default_sale_unit: "required",
                default_purchase_unit: "required",
                purchase_price: "required",
                selling_price: "required",
                alert_quantity: "required",
                opening_stock: "required",
                opening_stock_value: "required",
            },
            messages:{
                name: "The item name field is required",
                code: "The item code field is required",
                unit: "The unit field is required",
                default_sale_unit: "The default sale unit field is required",
                default_purchase_unit: "The default purchase unit field is required",
                purchase_price: "The purchase price field is required",
                selling_price: "The selling price field is required",
                alert_quantity: "The alert quantity field is required",
                opening_stock: "The opening stock field is required",
                opening_stock_value: "The opening stock value field is required.",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });
});
</script>