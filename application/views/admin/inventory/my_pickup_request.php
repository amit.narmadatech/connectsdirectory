 <!-- Datatables -->
<div class="container">
<div class="row">
    <?php echo get_template_part('commission_menu'); ?>	
   <div class="col-md-9">
      <div class="x_panel">
        <div class="x_title">
          <h2>Kit Request List</h2>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">        
          <table id="datatable-buttons" class="table dt-table dataTable table-striped table-bordered">
            <thead>
              <tr>
                <th>Sr. No.</th>
                <th>Ref. No.</th>
                <th>Quantity</th>
                <th>Status</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php 
             if(!empty($request_list)){
                $i =1;
                foreach($request_list as $rqlist){ ?>
                <tr>
                    <td><?php  echo $i++; ?></td>
                    <td><?php  echo !empty($rqlist->reference_no) ? $rqlist->reference_no : '' ?></td>
                    <td><?php  echo !empty($rqlist->quantity) ? $rqlist->quantity : '' ?></td>
                    <td>
                    <?php
                            if ($rqlist->status == 0) {
                                
                              
                            } elseif ($rqlist->status == 1) {
                                echo '<span class="label label-info">Draft</span>';
                              
                            } elseif ($rqlist->status == 2) {
                                echo '<span class="label label-sucess">Approved</span>';
                             
                            } elseif ($rqlist->status == 3) {
                                echo '<span class="label label-danger">Rejected</span>';
                               
                            } elseif ($rqlist->status == 4) {
                                echo '<span class="label label-primary">Assigned</span>';
                              
                            } elseif ($rqlist->status == 5) {
                                echo '<span class="label label-info">Dispatched</span>';
                            }
                        ?>
                    </td>
                    <td><?php  echo !empty($rqlist->created_at) ?date("D, d M Y",strtotime($rqlist->created_at)): '' ?></td>
                </tr>
            <?php } } ?>
            </tbody>
          </table>      
        </div>
      </div>
    </div>

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Kit Details</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered b">
              <tr>
                <th width="10">#</th>
                <th>Plan</th>
                <th>Kit</th>
              </tr>
              <tbody id="detail_div">
                
              </tbody>
              
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="print_btn">Print</button>
          </div>

        </div>
      </div>
    </div>
</div>
</div>
<!--
 Datatables 
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= base_url("assets/js/print/jQuery.print.js"); ?>"></script>-->

    <script type="text/javascript">
    $('#print_btn').click(function(event) {
      $.print(".b");
    });

    
    $(".details").click(function(event) {
        var id = this.id;
          $.post("<?php echo base_url('jcgenetics/get_kits_by_order_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response){
                var rows = '';
                var i =1;
                $.each(response, function(index, val) {
                   rows += '<tr><td>'+i+++'</td><td>'+val.plan_name+'</td><td>'+val.qr_code+'</td></tr>';
                });
                console.log(rows);
                $('#detail_div').html(rows);
              }else{
                console.log(response);
              }
          },"json");
    });
    </script>