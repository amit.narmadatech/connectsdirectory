<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>QR Code List</h2>
          <ul class="nav navbar-right panel_toolbox">           
            <li>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addKitModal"><i class="fa fa-plus"></i> Generate QR Code</button>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">       
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th>Quantity</th>
                <th>Created By</th>
                <th>Created Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                 if(!empty($kitData)){
                    $i=1;
                    foreach($kitData as $kit){ ?>
                    <tr>
                        <td><?= $i++; ?></td>
                        <td><?php  echo !empty($kit->group_id) ? count_qrcode_by_groupid($kit->group_id): '' ?></td>
                        <td><?php  echo !empty($kit->created_by) ? 'admin' : 'Admin' ?></td>
                        <td><?php  echo !empty($kit->created_date) ? date('D, d M Y h:i A',strtotime($kit->created_date)) : '' ?></td>
                        <td><a href="<?php echo base_url('kit/print_qrcode_by_group/'.$kit->group_id);?>" class="btn btn-info"><i class="fa fa-print"></i> Print</a>
                        </td>
                    </tr>
                <?php } } ?>
            </tbody>
          </table>      
        </div>
      </div>
    </div>
</div>
<!-- Small Size -->
<div class="modal fade" id="addKitModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="smallModalLabel">Add Kit</h4>
            </div>
            <form id="kitAddForm" method="post" action="<?= base_url('kit/generate_qrcode'); ?>">
                <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                <div class="modal-body">
                        <label for="quantity">Quantity</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="number" id="quantity" name="quantity" min="1" max="500" class="form-control" placeholder="Enter quantity" value="1" required>
                            </div>
                        </div>
                        <label for="quantity">Number of Copy</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="number" id="copy" name="copy" min="1" max="10" class="form-control" placeholder="Enter number of copy to print" value="2" required>
                            </div>
                        </div>
                        <!-- <label for="type">Type</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select id="type" name="type" class="form-control show-tick">
                                    <option selected value="num">Numeric</option>
                                    <option value="lower">Lower</option>
                                    <option value="upper">Uper</option>
                                </select>
                            </div>
                        </div>
                        <label for="size">QR Code Length</label>
                        <div class="form-group">
                            <div class="form-line">
                                <input type="number" id="size" name="size" min="10" max="30" class="form-control" placeholder="Enter size" value="10" required>
                            </div>
                        </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" id="addKitBtn" class="btn btn-link waves-effect">Submit</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript">
$('#addKitBtn').click(function(event) {
    bootbox.confirm({
        message: "Are you sure ?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log(result);
            if (result){
                $('#kitAddForm').submit();
            }
        }
    });
});
</script>