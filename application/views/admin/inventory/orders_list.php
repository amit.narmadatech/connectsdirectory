<?php $this->load->view('admin/include/header1'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Order List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">          
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Order ID</th>
                <th>Transaction ID</th>
                <th>Date</th>
                <th>Customer</th>
                <th>Email</th>
              
                <th>Contact</th>
                <th>City</th>
                <th>JC Care/Retailer ID</th>
                <th>Total</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
               if(!empty($order_list)){
                  $i = 1;
                foreach($order_list as $odlist){ 
                    
                    $jccareRetailer ="";
                    if(!empty($odlist->branch_id)){
                        
                      $jccareRetailer =  getjccare_by_id($odlist->branch_id);
                    }    
                    ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php  echo !empty($odlist->order_number) ? $odlist->order_number : '' ?></td>
                  <td><?php  echo !empty($odlist->txnid) ? $odlist->txnid : '' ?></td>
                  <td><?php  echo !empty($odlist->order_date) ? date('D d M Y h:i a',strtotime($odlist->order_date)) : '' ?></td>
                  <td><?php  echo !empty($odlist->c_name) ? $odlist->c_name : '-' ?></td>
                  <td><?php  echo !empty($odlist->email) ? $odlist->email : '-' ?></td>

                  <td><?php  echo !empty($odlist->mobile1) ? $odlist->mobile1 : '' ?></td>
                  <td><?php  echo !empty($odlist->city) ? $odlist->city : '-' ?></td>
                  <td>
                      <?php 
                           if(!empty($odlist->ref_code) && empty($jccareRetailer)){
                               echo $odlist->ref_code;
                           }else if (!empty($jccareRetailer['ref_code'])){
                               echo $jccareRetailer['ref_code'];
                           }else{
                               echo "-";
                           }
                     ?>
                  </td>

                  <td><?php  echo !empty($odlist->total_amount) ? $odlist->total_amount : '-' ?></td>
                  <td>
                  <?php  
                    if ($odlist->status == 1) { echo "Pending"; }
                  ?></td>
                  <td>
                    <a class="btn btn-sm btn-primary" href="<?php echo base_url("inventory/stock_out/$odlist->id"); ?>">Process</a>
                  </td>
                </tr>
              <?php } } ?>
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>