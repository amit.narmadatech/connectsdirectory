<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
  .form-control{ width: 50%; display: inline;}
</style>
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-2"></div>
  <div class="col-md-8 col-sm-8 col-xs-8">
    <div class="x_panel">
      <div class="x_title">
          <div class="col-md-6">
               <h2>Assign Stock</h2>  
          </div>
        <div class="col-md-6">
              <button id="<?php echo $edit->id; ?>" type="button" class="btn btn-danger btn-xs rejectBtn pull-right" data-toggle="modal" data-target="#reject_form">Reject</button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="ele2" class="b">
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_kit_assignment'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden"  name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="request_id" value="<?php echo isset($edit)?$edit->id:''; ?>">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  # <span><?php echo isset($edit)?$edit->reference_no:''; ?></span>
                  <small class="pull-right">Date: <?php echo date("d/m/Y"); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <h4><b>CONTACT</b></h4>
                  <address>
                    <strong><?php echo !empty($edit->contact_person)?$edit->contact_person:''; ?></strong>
                    <br>Phone: <?php echo !empty($edit->contact1)?$edit->contact1:''; ?> <?php echo !empty($edit->contact2)?", ".$edit->contact2:''; ?>
                    <br>Email: <?php echo !empty($edit->email)?$edit->email:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                    <strong><?php echo !empty($edit->contact_person)?$edit->contact_person:''; ?></strong>
                      <br><?php echo !empty($edit->address)?$edit->address:''; ?>
                      <br><?php echo !empty($edit->city)?$edit->city:''; ?>, <?php echo !empty($edit->state)?$edit->state:''; ?> <?php echo !empty($edit->pincode)?$edit->pincode:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Ref. Number # <?php echo isset($edit)?$edit->reference_no:''; ?> <input type="hidden" name="reference_no" value="<?php echo isset($edit)?$edit->reference_no:''; ?>"></b>
                <br>
                <b>Requested Quantity :<?php echo isset($edit)? $edit->quantity :''; ?></b>
                <br>
                 <b>Kit Quantity in Account:<?php echo isset($edit) ?  $remaingQty :''; ?></b><br>
                 <b>Order Place Quantity  : <?php echo isset($edit) ?   $total_order_kit :''; ?></b><br>
                 <b>Assign kit Quantity  : <?php echo isset($edit) ? (($total_order_kit + 4) - $remaingQty) :''; ?> </b><br>
                <br>
                <b>Request Date:</b> <?php echo date("d/m/Y",strtotime($edit->created_at)); ?>
                <br>                
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th width="5%">#</th>
                      <!-- <th width="50%">QR CODE</th> -->
                      <th width="35%">BARCODE</th>
                      <th><button type="button" class="btn btn-info btn-xs pull-right" data-id="<?php echo isset($remaingQty) ? (($total_order_kit + 4) - $remaingQty) :''; ?>"  id="addRow">Add Row</button></th>
                    </tr>
                  </thead>
                  <tbody id="kitTbl">
                    
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
               
                <textarea id="description" name="description" class="text-muted no-shadow form-control" style="margin-top: 10px;height: 115px;" rows="10"></textarea>
              </div>
              <!-- /.col -->
              <div class="col-xs-6">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      
                      <tr>
                        <th>Total:</th>
                        <td class="text-right"><span id="total_rows"></span><input type="hidden" name="total_qty" id="total_qty"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-6 col-md-offset-5">
                <button type="submit" class="btn btn-primary"><i class="fa fa-right"></i> Submit</button>
                <a href="<?php  echo base_url('inventory/kit_request_list'); ?>" class="btn btn-default"><i class="fa fa-right"></i> Cancel</a>
              </div>
            </div>
          </section>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" id="reject_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Reject Request</h4>
            </div>
            <form class="form" novalidate="" onsubmit="checkBarcodeUnique();" method="post" action="<?php echo base_url('inventory/reject_kit_assignment'); ?>" >

                <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />    
                <input type="hidden" name="request_id" id="request_id">
                <div class="modal-body">
                    <textarea id="reason" name="reason" required class="form-control" placeholder="Enter rejection reason" ></textarea>
              </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>

<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>

<script>
    jQuery(document).ready(function($) {
      var i=1;
       $(document).on('click','#addRow', function(event) {
           
           var qty = $('#addRow').attr('data-id');
           
           if(qty == 0){
                 bootbox.alert("Kit assign limit exceeded");
                 return false;
            }
           
//           if(qty == 4){
//               bootbox.alert("your account already has  Kit limit");
//                return false;
//            }else if(qty == 0){
//                 bootbox.alert("Kit assign limit exceeded");
//                 return false;
//            }else{
//                 qty = qty-1;
//                 $('#addRow').attr('data-id',qty);
//            }
           
          var txt = '<tr id="row'+i+'"><td class="number">'+i+'</td><td><input type="text" name="barcode[]" onblur="myFunction()" class="form-control"></td><td><button type="button" class="btn btn-danger btn-xs btnRemove" id="'+i+'">X</button></td></tr>';
          $('#kitTbl').append(txt);
          var length = $('#kitTbl tr').length;
          $('#total_rows').html(length);
          $('#total_qty').val(length);
          i++;
           qty = parseInt(qty)- parseInt(1);
           $('#addRow').attr('data-id',qty);
           
           $("td.number").each(function(i,v) {
            $(v).text(i + 1);
           });
        });

        var barcodeArray = new Array();
        $(document).scannerDetection({
          timeBeforeScanTest: 200, // wait for the next character for upto 200ms
          startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
          endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
          avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
          onComplete: function(barcode, qty){ 
           // var barcode = '635416316';

            if((jQuery.inArray(barcode,barcodeArray) == -1)){
            $.post("<?php echo base_url('inventory/get_barcode_detail'); ?>",{ barcode : barcode, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response.status){
                var txt = '<tr id="rowv'+response.result.id+'"><td>'+response.result.id+'</td><td><input readonly type="text" id="barcode_v'+response.result.id+'" name="barcode[]" onblur="myFunction()" class="form-control" value="'+response.result.barcode+'"></td><td><button type="button" class="btn btn-danger btn-xs btnRemove" id="v'+response.result.id+'">X</button></td></tr>';
                barcodeArray.push(response.result.barcode);
                $('#kitTbl').append(txt);
                var length = $('#kitTbl tr').length;
                $('#total_rows').html(length);
                $('#total_qty').val(length);
                console.log(response.result);

              }else{
                console.log(response.status);
              }
            },"json");
            }else{
                 bootbox.alert("Barcode: "+barcode+" already used !");
             // alert("Barcode: "+barcode+" already used !");
            }

           } 
        });

        $(document).on('click','.btnRemove', function(event) {
           var id = this.id;
            var qty = $('#addRow').attr('data-id');
           barcode = $('#barcode_'+id).val();
           barcodeArray.splice( $.inArray(barcode, barcodeArray), 1 );
           $('#row'+id).remove();
           var length = $('#kitTbl tr').length;
            $('#total_rows').html(length);
            $('#total_qty').val(length);
                qty = parseInt(qty) + parseInt(1);
                $('#addRow').attr('data-id',qty);
                
            $("td.number").each(function(i,v) {
                 $(v).text(i + 1);
           });   
        });

       
      
        $(document).on('click', '.rejectBtn', function (event) {
            var id = this.id;
            $('#request_id').val(id);
        });
        
    });
   

    $('#form_validation').validate({
            submitHandler: function(form) {

              var length = $('input[name="barcode[]"]').length;
              if (length < 1) { 
                   bootbox.alert("Please add kits");
              }
              var chk = 0;
               $('input[name="barcode[]"]').each(function(index, val) {
                  var val=$(this).val();
                  if(isNaN(val) || (val < 1)){
                    chk++;
                  }
                });

               if (chk > 0) {  bootbox.alert("Row should not be blank or zero "); }else{
                  form.submit();
               }
            },
            ignore: [],
            rules: {
                total_qty: "required",
               
            },
            messages:{
                total_qty: "Kits are required.",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
    });

    function myFunction(){
        var values = [];
        $('input[name="barcode[]"]').each(function (index, val) {
            var val = $(this).val();
            if(val != ""){
                if ($.inArray(this.value, values) >= 0) {
                       bootbox.alert("Barcode  must be unique");
                       $(this).val('');
                 }else{
                       values.push($(this).val()); 
                 }
            } 
        });
    }
    
</script>