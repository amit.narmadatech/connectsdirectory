<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Stock-in Summary</h2>          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">       
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Reference No.</th>
              <th>P.O. No.</th>
              <th>Supplier</th>
              <th>Warehouse</th>
              <th>Description</th>
              <th>Date</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              <?php 
                $total_qty = 0;
               if(!empty($stock_list)){
                foreach($stock_list as $slist){ ?>
            <tr>
              <td><?php  echo !empty($slist->reference_no) ? $slist->reference_no : '' ?></td>
              <td><?php  echo !empty($slist->po_no) ? $slist->po_no : '' ?></td>
              <td><?php  echo !empty($slist->sp_name) ? $slist->sp_name : '-' ?></td>
              <td><?php  echo !empty($slist->w_name) ? $slist->w_name : '-' ?></td>
              <td><?php  echo !empty($slist->description) ? $slist->description : '-'; ?></td>
              <td><?php  echo !empty($slist->created_at) ? date("D, d M Y",strtotime($slist->created_at)) : '' ?></td>
              <td><?php  echo !empty($slist->actual_qty) ? $slist->actual_qty : '-'; $total_qty += $slist->actual_qty; ?>
                
                <a href="<?php echo base_url('inventory/upload_kit/'.$slist->request_id);?>" class="btn btn-success btn-xs pull-right">Scan Kits</a>
              </td>
              <td>
                <a href="<?php echo base_url('inventory/get_stock_in_detail/'.$slist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>
              </td>
            </tr>
            <?php } } ?>
          </tbody>
            <tfoot>
             <tr>
              <td colspan="5"></td>
              <td><b>Total :</b></td>
              <td colspan="2"><b><?php echo $total_qty; ?></b></td>
            </tr>
          </tfoot>
        </table>      
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>