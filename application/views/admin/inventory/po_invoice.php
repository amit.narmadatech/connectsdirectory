<!DOCTYPE html>
<html>
<head>
	<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible'content='IE=edge,chrome=1'><![endif]-->
<title>Purchase Order</title>      
<meta name="description" content="JC GENETICS - Towards a better life">
<meta name="keywords" content=" JC GENETICS - Towards a better life">
<meta name="author" content="www.jcgenetics.in">     
<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet"> 
</head>
<body style="margin: 0; font-family: 'Montserrat', sans-serif;">

<table cellpadding="0" cellspacing="0" style="width: 900px; border-collapse: collapse; margin:0 auto; ">
	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0;">
			<tr>
 				<td style="padding:15px 30px 15px 15px; width: 35%"> <img src="<?php echo base_url('assets/images/logo.png'); ?>" style="width: 200px;"></td>
 				<td  style="padding:15px 15px 15px 30px; text-align: right; line-height: 25px;">
				 		 JC Genetics India Private Limited <br>
				 		 7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road, Indore (M.P.), India. Pin 452010 <br>
				 		 CIN: U74999MP2018PTC045167 <br>
				 		 GSTIN: 23AAECJ1917G1ZX  Place Of registration: Indore, Madhya Pradesh 

 				</td>
 			 </tr>
		  </table>
		</td>
	</tr>
	<tr>
		<td style="padding: 25px 25px 50px 25px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0; border: solid 1px #000; ">
				<tr>
					<th colspan="4" style="padding: 15px; text-align: center; border-bottom: solid 1px #000; ">PURCHASE ORDER</th>
				</tr>
				<tr>
					<td style="width: 50%; vertical-align: top;" colspan=""> 
						<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0;">
							<tr>
								<td style="padding: 10px;"> 
									TO
								</td>
							</tr>
							<tr>
								<td style="padding: 10px;"> 
									 <?php echo $po_request->s_name; ?> <br>
								</td>
							</tr>
							<tr>
								<td style="padding: 10px;"> 
															
								</td>
							</tr>
						</table>
					</td>					
					<td colspan="3" valign="top" style="border-left: solid 1px #000;"> 
						<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0;">
							<tr>
								<td style="padding:20px 10px; text-align: right;"> 
									Invoice Date: <?php echo date("d-m-Y",strtotime($po_request->created_at)); ?> <br> 
									Invoice No : <?php echo $po_request->reference_no; ?>
								</td>
							</tr>
							<tr>
								<td style="padding:20px 10px; text-align: right; border-bottom: solid 1px #000; border-top: solid 1px #000; "> 
									Kind Attn: Asha Anil									
								</td>
							</tr>
							<tr>
								<td style="padding: 15px 10px; text-align: right;"> 
									Party's GSTIN: 33AAACX0926F1ZP									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding: 10px; border-top: solid 1px #000;">Particular</td>
					<td style="border-left: solid 1px #000; border-top: solid 1px #000; padding: 10px;" >Qty</td>
					<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;">Rate (RS)</td>
					<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;">Amount</td>
				</tr>

				<?php 
                    if (!empty($po_request_details)) {
                      $i = 1;$sub_total = 0;$tax_prnct = 0;$tax_amt = 0;
                      foreach ($po_request_details as $detail) { ?>
                    <tr>
          						<td style="padding: 10px; border-top: solid 1px #000;"><?php echo $detail->product_name; ?> [ <?php echo $detail->product_code; ?> ]</td>
          						<td style="border-left: solid 1px #000; border-top: solid 1px #000; padding: 10px;" ><?php echo $detail->quantity; ?></td>
          						<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;"><?php echo $detail->rate; ?></td>
          						<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;"><?php echo $detail->rate*$detail->quantity;$sub_total += $detail->amount;?></td>
          					</tr> 
                    <?php $tax_prnct += $detail->tax_prct;$tax_amt += $detail->tax_amt;  }
                    }
                 ?>
				
				<tr>
					<td style="padding: 10px; border-top: solid 1px #000;">IGST</td>
					<td style="border-left: solid 1px #000; border-top: solid 1px #000; padding: 10px;" ><?php echo $tax_prnct; ?>%</td>
					<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;"></td>
					<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;"><?php echo $tax_amt; ?></td>
				</tr>
				<tr>
					<td colspan="3" style="padding: 10px; border-top: solid 1px #000; text-align: right;">Grand Total</td>
					
					<td style="padding: 10px; border-left: solid 1px #000; border-top: solid 1px #000;"><?php echo $grand_total=$sub_total; ?></td>
				</tr>
				<tr>
					<td colspan="4" style="padding: 10px; border-top: solid 1px #000;"></td>					
				</tr>
			</table>
		</td>		
	</tr>
	<tr>
		<td style="padding: 5px 25px;">
			GSTIN: 23AAECJ1917G1ZX Place Of registration: Indore, Madhya Pradesh
		<br>
			Our Income Tax PAN number: AAECJ1917G
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 25px 150px;">
			
		
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 25px;">
			Manager Accounts <br> 
			JC Genetics India Pvt. Ltd. <br><br>
		
		</td>
	</tr>
	<tr>
		<td style="padding: 5px 25px 50px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse; margin:0; border:solid 1px #000;">
				<tr>
					<td style="padding:10px;  border-top: solid 1px #000;"> 
						Bank		
					</td>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						HDFC Bank Limited		
					</td>
				</tr>
				<tr>
					<td style="padding: 10px;  border-top: solid 1px #000; "> 
						Address of Bank											
					</td>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						NO:40 Nungambakkam High Road <br>
						Nungambakkam, Chennai-600034, INDIA										
					</td>
				</tr>
				<tr>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						IFSC Code									
					</td>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						HDFC0000082										
					</td>
				</tr>
				<tr>
					<td style="padding: 10px;  border-top: solid 1px #000; "> 
						Account Number								
					</td>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						00822000020998										
					</td>
				</tr>
				<tr>
					<td style="padding: 10px;  border-top: solid 1px #000; "> 
						Account Name								
					</td>
					<td style="padding: 10px;  border-top: solid 1px #000; border-left: solid 1px #000;"> 
						JCGenetics India Private Limited 										
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			JCGenetics India Private Limited Phone:+91 968 509 5752 Email: info@jcgenetics.in website:https://www.jcgenetics.in/
		</td>
	</tr>
</table>

</body>
</html>