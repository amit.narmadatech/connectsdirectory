<?php $this->load->view('admin/include/header1'); ?>
<style type="text/css">
  .form-control{ width: 50%; display: inline;}
</style>
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-2"></div>
  <div class="col-md-8 col-sm-8 col-xs-8" >
    <div class="x_panel">
      
      <div class="x_content">
          
        <div id="ele2" class="b">
          <section class="content invoice" id="outprint">
            <!-- title row -->
            <div class="x_title">
           <div class="col-md-6">
                 <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="image"> 
          </div>
          
         
        <div class="clearfix"></div>
      </div>
            <div class="row">
              <div class="col-xs-12 invoice-header">
                  <h1 style="display: inline-block;width: 100%;">
                    <small class="pull-left"> # <span><?php echo isset($edit)?$edit->reference_no:''; ?></span> </small>
                  <small class="pull-right">Date: <?php echo date("d/m/Y", strtotime($assign->created_at)); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <h4><b>CONTACT</b></h4>
                  <address>
                    <strong><?php echo !empty($edit->contact_person)?ucwords($edit->contact_person):''; ?></strong>
                    <br>Phone: <?php echo !empty($edit->contact1)?$edit->contact1:''; ?> <?php echo !empty($edit->contact2)?", ".$edit->contact2:''; ?>
                    <br>Email: <?php echo !empty($edit->email)?$edit->email:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                    <strong><?php echo !empty($edit->contact_person)?ucwords($edit->contact_person):''; ?></strong>
                      <br><?php echo !empty($edit->address)?$edit->address:''; ?>
                      <br><?php echo !empty($edit->city)?$edit->city:''; ?>, <?php echo !empty($edit->state)?$edit->state:''; ?> <?php echo !empty($edit->pincode)?$edit->pincode:''; ?>
                  </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Ref. Number : <?php echo isset($edit)?$edit->reference_no:''; ?> </b>
                <br>
                <b>Requested Quantity : <?php echo isset($edit)?$edit->quantity:''; ?>
                </b>
                <br>
                <br>
                <b>Request Date : </b> <?php echo date("d/m/Y",strtotime($edit->created_at)); ?>
                <br>                
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th width="5%">#</th>
                      <!-- <th width="50%">QR CODE</th> -->
                      <th width="35%">BARCODE</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if (!empty($details)) {
                      $i=1;
                      foreach ($details as $dt) { ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <!-- <td><?php echo !empty($dt->qrcode)?$dt->qrcode:'-'; ?></td> -->
                          <td><?php echo !empty($dt->barcode)?$dt->barcode:'-'; ?></td>
                        </tr>
                    <?php  }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            
            <div class="row">
                    <div class="col-xs-12">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      
                      <tr>
                          <th class="text-right">Total:</th>
                        <td class="text-right"><span id="total_rows"><?php echo $i-1; ?></span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.row -->

           
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            
          </section>
         <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;height: 115px;">
                  <?php echo !empty($assign->description)?$assign->description:''; ?>
                </p>
              </div>
              <!-- /.col -->
          
              <!-- /.col -->
            </div>
        </div>
      </div>
    </div>
      <div class="col-sm-12 text-center">
      <a href="#" class="btn btn-success btn-small printMe">
        <span class="glyphicon glyphicon-print"></span> Print
      </a>
      </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>

<script>
    $(document).ready(function() {
           $('.printMe').click(function(){
              $("#outprint").print();
            });
    });
</script>