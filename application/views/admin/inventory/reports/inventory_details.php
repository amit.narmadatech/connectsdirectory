<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Inventory Details</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Item Name [ Code ]</th>
                <th>Warehouse Name</th>
                <th>Quantity Ordered</th>
                <th>Quantity Commited</th>
                <th>Quantity IN</th>
                <th>Quantity OUT</th>
                <th>Quantity Available</th>
              </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php  echo !empty($product) ? $product : '' ?></td>
                    <td><?php  echo !empty($warehouse) ? $warehouse : '' ?></td>
                    <td><?php  echo !empty($qty_ordered) ? $qty_ordered : '' ?></td>
                    <td><?php  echo !empty($qty_commited) ? $qty_commited : '' ?></td>
                    <td><?php  echo !empty($qty_in) ? $qty_in : '' ?></td>
                    <td><?php  echo !empty($qty_out) ? $qty_out : '' ?></td>
                    <td><?php  echo !empty($qty_available) ? $qty_available : '' ?></td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Reject</h4>
          </div>
          <form method="post" action="<?php echo base_url('inventory/reject_purchase_request'); ?>">
            <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
          <input type="hidden" name="request_id" id="request_id">
          <div class="modal-body">
            <label for="reason">Description</label>
            <textarea name="reason" class="form-control" required></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default reset" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        
        $('.reject').click(function(event) {
          var id = this.id;
          $('#request_id').val(id);
        });

        $('.reset').click(function(event) {
          $('#request_id').val('');
        });

        $('.approve').click(function(event) {
          var id = this.id;
          bootbox.confirm({
              message: "Are you sure ?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                  if (result){
                      $.post("<?php echo base_url('inventory/approve_purchase_request'); ?>",{ request_id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){

                          window.location.href ="<?php echo base_url('inventory/approval_list'); ?>";
                          
                      },"json");
                  }
              }
          });
      });

      });
    </script>