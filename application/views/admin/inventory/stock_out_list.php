<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Dispatched Stock List</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?= base_url('inventory/stock_out'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Dispatch stock</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">        
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Order No.</th>
                <th>Customer</th>
                <th>Email</th>
                <th>Address</th>
                <th>Shipping</th>
                <th>Dispatch Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
             if(!empty($dispatch_list)){
                
                foreach($dispatch_list as $dplist){ ?>
                <tr>
                    <td><?php  echo !empty($dplist->order_number) ? $dplist->order_number : '' ?></td>
                    <td><?php  echo !empty($dplist->c_name) ? $dplist->c_name : '' ?></td>
                    <td><?php  echo !empty($dplist->email) ? $dplist->email : '' ?></td>
                    <td><?php  echo !empty($dplist->address) ? $dplist->address : '' ?>, <?php  echo !empty($dplist->city) ? $dplist->city : '' ?>, <?php  echo !empty($dplist->state) ? $dplist->state : '' ?> <?php  echo !empty($dplist->pincode) ? $dplist->pincode : '' ?></td>
                    <td><?php  echo !empty($dplist->shipping_partner) ? ucfirst($dplist->shipping_partner) : '' ?> : <?php  echo !empty($dplist->tracking_code) ? ucfirst($dplist->tracking_code) : '' ?></td>
                    <td><?php  echo !empty($dplist->dispatch_date) ?date("D, d M Y",strtotime($dplist->dispatch_date)): '' ?></td>
                    
                    <td>
                      <?php 
                      $url ="";
                        if ($dplist->shipping_partner == "delhivery") {
                         $url = base_url("inventory/delhivery_dispatch/$dplist->disid");
                        }elseif($dplist->shipping_partner == "dtdc"){
                         $url = base_url("inventory/dtdc_dispatch/$dplist->disid");
                        }
                      ?>
                      <?php if($dplist->shipping_partner != "manual"){ ?>
                         <a href="<?php echo $url; ?>" class="btn btn-warning btn-xs"><i class="fa fa-file"></i> Dispatch Form</a>
                   <?php } ?>
                      <a href="<?php echo base_url('admindashboard/order_detail/'.$dplist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-file"></i> View</a>
                      <button type="button" id="<?php echo $dplist->id; ?>" class="btn btn-info btn-xs details" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-eye"></i> Kits</button>
                    </td>
                </tr>
            <?php } } ?>
            </tbody>
          </table>      
        </div>
      </div>
    </div>

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Kit Details</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered b">
              <tr>
                <th width="10">#</th>
                <th>Plan</th>
                <th>Kit</th>
              </tr>
              <tbody id="detail_div">
                
              </tbody>
              
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="print_btn">Print</button>
          </div>

        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= base_url("assets/js/print/jQuery.print.js"); ?>"></script>

    <script type="text/javascript">
    $('#print_btn').click(function(event) {
      $.print(".b");
    });

    
    $(".details").click(function(event) {
        var id = this.id;
          $.post("<?php echo base_url('inventory/get_kits_by_order_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response){
                var rows = '';
                var i =1;
                $.each(response, function(index, val) {
                   rows += '<tr><td>'+i+++'</td><td>'+val.plan_name+'</td><td>'+val.qr_code+'</td></tr>';
                });
                console.log(rows);
                $('#detail_div').html(rows);
              }else{
                console.log(response);
              }
          },"json");
    });
    </script>