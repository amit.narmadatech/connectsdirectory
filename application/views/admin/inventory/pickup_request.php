<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Pickup Request List</h2>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">         
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Sr.No.</th>
              <th>Order No.</th>
              <th>Name</th>
              <th>Email</th>
              <th>Address</th>
              <th>Quantity</th>
              <th>Date Time</th>
              <th>Status</th>
              <th>Created Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
             if(!empty($request_list)){
              $i=1;       
                foreach($request_list as $rdlist){ ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php  echo !empty($rdlist->order_number) ? $rdlist->order_number : '' ?></td>
                    <td><?php  echo !empty($rdlist->firstname) ? $rdlist->firstname : '' ?> <?php  echo !empty($rdlist->lastname) ? $rdlist->lastname : '' ?></td>
                    <td><?php  echo !empty($rdlist->email) ? $rdlist->email : '' ?></td>
                    <td><?php  echo !empty($rdlist->address) ? $rdlist->address : '' ?></td>
                    <td><?php  echo !empty($rdlist->quantity) ? $rdlist->quantity : '' ?></td>
                    <td><?php  echo !empty($rdlist->pickup_date) ? $rdlist->pickup_date : '' ?> <?php  echo !empty($rdlist->pickup_time) ? $rdlist->pickup_time : '' ?></td>
                    <td>
                      <?php echo ($rdlist->status)?'Approved':'Pending'; ?>
                    </td>
                    <td><?php  echo !empty($rdlist->created_at) ?date("D, d M Y",strtotime($rdlist->created_at)): '' ?></td>
                    <td>
                      <input type="hidden" id="barcode_<?php echo $rdlist->id; ?>" name="barcodes" value="<?php echo $rdlist->barcode; ?>">
                      <button type="button" id="<?php echo $rdlist->id; ?>" class="btn btn-info barcodes" data-toggle="modal" data-target="#barcodeModal">Barcode list</button>

                    </td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>       
      </div>
    </div>
  </div>
</div>
 <!-- Modal -->
  <div class="modal fade" id="barcodeModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Barcode List</h4>
        </div>
        <div class="modal-body">
          <ul id="barcodeList">
            
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

    <script type="text/javascript">
      jQuery(document).ready(function($) {

        $(document).on('click', '.showKitList', function(event) {
            var id = this.id;
            $.post("<?php echo base_url('rcenter/getScannedKitList'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
              var data = JSON.parse(data);
              var txt = '';
              var i = 1;
              $.each(data.result, function(index, val) {
                 txt = '<tr><td>'+i+'</td><td>'+val.barcode+'</td></tr>';
              });

              $('#scanKitArea').append(txt);

            });
        });

        $(document).on('click', '.barcodes', function(event) {
          var id = this.id;
          var barcodes = $('#barcode_'+id).val();
          var ar = barcodes.split(',');
          var text = '';
          $.each(ar, function(index, val) {
             text +='<li>'+val+'</li>';
          });
          $('#barcodeList').html(text);
        });
        
      });
    </script>