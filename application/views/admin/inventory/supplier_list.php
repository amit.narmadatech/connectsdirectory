<?php $this->load->view('admin/include/header1'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Supplier List</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?= base_url('inventory/add_supplier'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Supplier</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">       
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              <?php 
               if(!empty($supplier_list)){
                  
                  foreach($supplier_list as $splist){ ?>
                  <tr>
                      <td><?php  echo !empty($splist->name) ? $splist->name : '' ?></td>
                      <td><?php  echo !empty($splist->email) ? $splist->email : '' ?></td>
                      <td><?php  echo !empty($splist->contact1) ? $splist->contact1 : '' ?><?php  echo !empty($splist->contact2) ? ", ".$splist->contact2 : '' ?></td>
                      <td><?php  echo !empty($splist->created_at) ?date("D, d M Y",strtotime($splist->created_at)): '' ?></td>
                      <td><?php  echo ($splist->status == 1)?'Active':'Inactive'; ?></td>
                      <td><a href="<?php echo base_url('inventory/add_supplier/'.$splist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a></td>
                  </tr>
              <?php } } ?>
            </tbody>
          </table>     
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>