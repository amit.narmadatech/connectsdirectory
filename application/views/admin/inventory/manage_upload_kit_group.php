<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <?php $this->load->view('include/alert'); ?>
      <div class="x_panel">
        <div class="x_title">
          <h2>Manage Uploaded Kit Group</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?php echo base_url('inventory/upload_kit'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> New upload Kit</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">       
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Group Id</th>
                <!-- <th>Prepared By</th> -->
                <th>Quantity</th>
                <th>Created By</th>
                <th>Date</th>
			         	<th>View</th>
              </tr>
            </thead>
            <tbody>
              <?php 
               if(!empty($kitGroup)){                  
                foreach($kitGroup as $slist){ ?>
                <tr>
                  <td><?php  echo !empty($slist->group_id) ? $slist->group_id : '' ?></td>
                  <!-- <td><?php  echo !empty($slist->prepared_by) ? $slist->prepared_by : '' ?></td> -->
                  <td><?php  echo !empty($slist->quantity) ? $slist->quantity : '-' ?></td>
                  <td><?php  echo !empty($slist->created_by) ? created_name_by_id($slist->created_by) : '-' ?></td>
                  <td><?php  echo !empty($slist->created_at) ? date_formate($slist->created_at) : '' ?></td>
				  <td><a href="<?php echo base_url('inventory/upload_kitlist').'/'.$slist->group_id; ?>" class="btn btn-info"><i class="fa fa-eye"></i> View Kits</a></td>
                </tr>
              <?php } } ?>
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>