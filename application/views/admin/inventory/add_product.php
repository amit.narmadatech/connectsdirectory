<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Item</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_product_form'); ?>" enctype="multipart/form-data">
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden"  name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <input type="hidden" name="oldimg" value="<?= isset($edit)?$edit->image:''; ?>">
        
       <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">  
            <label for="name" class="col-md-3 col-sm-4 col-xs-12">Item Name <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8">  
              <input type="text" id="name" name="name" class="form-control" placeholder="Enter item name" value="<?= (isset($edit) && (!empty(($edit->name))))?$edit->name:set_value('name'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('name'); ?></span>
          </div>

         <div class="col-sm-6 col-xs-12">  
            <label for="code" class="col-md-3 col-sm-4 col-xs-12">Item code <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8">  
              <input type="text" id="code" name="code" class="form-control" placeholder="Enter code" value="<?= (isset($edit) && (!empty(($edit->code))))?$edit->code:set_value('code'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('code'); ?></span>
          </div>
        </div>
      </div>

       <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="unit" class="col-md-3 col-sm-4 col-xs-12">Unit <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8">  
              <select name="unit" id="unit" class="form-control ">
                  <option></option>
                  <option <?= (isset($edit) && ($edit->unit == 'piece'))?'selected':''; ?> value="piece">Piece (pc)</option>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('unit'); ?></span>
          </div>
          
          <div class="col-sm-6 col-xs-12">
            <label for="weight" class="col-md-3 col-sm-4 col-xs-12">Weight </label>
             <div class="col-md-9 col-sm-8">  
           
            <input type="text" id="weight" name="weight" class="form-control" placeholder="Enter weight ID" value="175 grms">
              <!-- <input type="text" id="weight" name="weight" class="form-control" placeholder="Enter weight ID" value="<?= (isset($edit) && (!empty(($edit->weight))))?$edit->weight:set_value('weight'); ?>"> -->
            </div>
            <span class="text-danger"><?php echo form_error('weight'); ?></span>
          </div>
        </div>
      </div>

      <!-- <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="default_sale_unit" class="col-md-3 col-sm-4 col-xs-12">Default Sell Unit <span class="required">*</span>
            </label>
           <div class="col-md-9 col-sm-8">  
              <select name="default_sale_unit" id="default_sale_unit" class="form-control">
                  <option></option>
                  <option <?= (isset($edit) && ($edit->default_sale_unit == 'meter'))?'selected':''; ?> value="meter">Meter (m)</option>
                  <option <?= (isset($edit) && ($edit->default_sale_unit == 'piece'))?'selected':''; ?> value="piece">Piece (pc)</option>
                  <option <?= (isset($edit) && ($edit->default_sale_unit == 'kilogram'))?'selected':''; ?> value="kilogram">Kilogram (kg)</option>        
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('default_sale_unit'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label for="default_purchase_unit" class="col-md-3 col-sm-3 col-xs-12">Default Purchase Unit <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8"> 
              <select name="default_purchase_unit" id="default_purchase_unit" class="form-control">
                  <option></option>
                  <option <?= (isset($edit) && ($edit->default_purchase_unit == 'meter'))?'selected':''; ?> value="meter">Meter (m)</option>
                  <option <?= (isset($edit) && ($edit->default_purchase_unit == 'piece'))?'selected':''; ?> value="piece">Piece (pc)</option>
                  <option <?= (isset($edit) && ($edit->default_purchase_unit == 'kilogram'))?'selected':''; ?> value="kilogram">Kilogram (kg)</option>        
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('default_purchase_unit'); ?></span>
          </div>
        </div>
      </div> -->

          
        <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="purchase_price" class="col-md-3 col-sm-3 col-xs-12">Purchase Price <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8"> 
              <input type="text" id="purchase_price" name="purchase_price" class="form-control" placeholder="Enter purchase price" value="<?= (isset($edit) && (!empty(($edit->purchase_price))))?$edit->purchase_price:set_value('purchase_price'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('purchase_price'); ?></span>
          </div>       

         <div class="col-sm-6 col-xs-12">
           <label for="selling_price" class="col-md-3 col-sm-3 col-xs-12">Selling Price <span class="required">*</span>
           </label>
            <div class="col-md-9 col-sm-8">
             <input type="text" id="selling_price" name="selling_price" class="form-control" placeholder="Enter selling price" value="<?= (isset($edit) && (!empty(($edit->selling_price))))?$edit->selling_price:set_value('selling_price'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('selling_price'); ?></span>
          </div>
        </div>
      </div>

        <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="alert_quantity" class="col-md-3 col-sm-3 col-xs-12">Alert Quantity <span class="required">*</span>
            </label>
       
        <div class="col-md-9 col-sm-8">
          <div class="row">
             <div class="col-md-5 col-sm-6">
              <input type="text" id="alert_quantity" name="alert_quantity" class="form-control" placeholder="Enter alert quantity" value="<?= (isset($edit) && (!empty(($edit->alert_quantity))))?$edit->alert_quantity:set_value('alert_quantity'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('alert_quantity'); ?></span>
      

          <div class="col-md-7 col-sm-6">
            <div class="row">
            <label for="tax" class="col-md-4 col-sm-3 col-xs-12">Tax (%) <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-8">
              <input type="number" autocomplete="off" min="0" id="tax" name="tax" class="form-control" placeholder="Enter tax (%)" value="<?= (isset($edit) && (!empty(($edit->tax))))?$edit->tax:18; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('tax'); ?></span>
          </div>
        </div>
        </div>
      </div>
      </div>

            <!-- <div class="col-sm-6 col-xs-12">
            <label for="product_image" class="col-md-3 col-sm-3 col-xs-12">Item Image 
            </label>
            <div class="col-md-9 col-sm-8">
              <input type="file" id="product_image" name="product_image" class="form-control hidden" placeholder="Enter item image" value="<?= (isset($edit) && (!empty(($edit->image))))?$edit->image:set_value('product_image'); ?>">
              <label for="product_image" class="btn btn-info ">Change</label>
              <span><?php if (isset($edit) && (!empty($edit->image))) { echo $edit->image; }else{ echo "No file chosen"; } ?></span>
            </div>
            <span class="text-danger"><?php echo form_error('product_image'); ?></span>
          </div> -->
        </div>
      </div>

        <!-- <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="opening_stock" class="col-md-3 col-sm-3 col-xs-12">Opening Stock <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8">
              <input type="text" id="opening_stock" name="opening_stock" class="form-control" placeholder="Enter alert quantity" value="<?= (isset($edit) && (!empty(($edit->opening_stock))))?$edit->opening_stock:set_value('opening_stock'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('opening_stock'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label for="opening_stock_value" class="col-md-3 col-sm-3 col-xs-12">Opening Stock Value <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8">
              <input type="text" id="opening_stock_value" name="opening_stock_value" class="form-control" placeholder="Enter opening stock value" value="<?= (isset($edit) && (!empty(($edit->opening_stock_value))))?$edit->opening_stock_value:set_value('opening_stock_value'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('opening_stock_value'); ?></span>
          </div>
        </div>
      </div> -->

           <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label for="description" class="col-md-3 col-sm-3 col-xs-12">Description 
            </label>
            <div class="col-md-9 col-sm-8">
              <textarea id="description" name="description" class="form-control" placeholder="Enter description" ><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12">Status</label>
            <div class="col-md-9 col-sm-8">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?= (isset($edit) && ($edit->status == 1))?'active':''; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-default <?= (isset($edit) && ($edit->status == 0))?'active':''; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
        </div>
          </div>
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('inventory/product_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<script>
    jQuery(document).ready(function($) {
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                name: "required",
                code: "required",
                unit: "required",
                //default_sale_unit: "required",
                //default_purchase_unit: "required",
                purchase_price: "required",
                tax: "required",
                selling_price: "required",
                alert_quantity: "required",
                //opening_stock: "required",
                //opening_stock_value: "required",
            },
            messages:{
                name: "The item name field is required",
                code: "The item code field is required",
                unit: "The unit field is required",
                //default_sale_unit: "The default sale unit field is required",
                //default_purchase_unit: "The default purchase unit field is required",
                purchase_price: "The purchase price field is required",
                selling_price: "The selling price field is required",
                alert_quantity: "The alert quantity field is required",
                //opening_stock: "The opening stock field is required",
                //opening_stock_value: "The opening stock value field is required.",
            },
          /*  highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }*/
        });

    });
});
</script>