<?php $this->load->view('admin/include/header1'); ?>
  <!-- top tiles -->
  <div class="row">
    <div class="col-sm-6"> 
      <h3>Purchase Request</h3> 
       <div class="row">
         <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="tile-stats bg-green ">
              <div class="icon"><i class="fa fa-first-order"></i></div>
            <div class="count"> <?php echo !empty($po_pending)? $po_pending : 0; ?></div>
              <h3>Total P.O. Pending</h3>
           </div>
         </div>
            <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="tile-stats bg-blue">
                <div class="icon"><i class="fa fa-first-order"></i></div>
                <div class="count"><?php echo !empty($po_received )? $po_received : 0; ?></div>
                <h3>Total P.O. Received</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
         <h3>Available Stock</h3>
        <div class="row">
         <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
           <div class="tile-stats bg-red">
              <div class="icon"><i class="fa fa-inbox"></i>
              </div>
           <div class="count"> <?php echo !empty($ordered_stock)? $ordered_stock : 0; ?></div>
           <h3>Total Stock Ordered</h3>
          </div>
        </div>
        <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="tile-stats bg-orange">
            <div class="icon"><i class="fa fa-inbox"></i>
          </div>
         <div class="count"><?php echo !empty($received_stock )? $received_stock : 0; ?></div>
            <h3>Total Stock Received</h3>
         </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6"> 
      <h3>Available Kits</h3> 
       <div class="row">
          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="tile-stats bg-purple">
             <div class="icon"><i class="fa fa-medkit"></i>
            </div>
           <div class="count"> <?php echo !empty($kit_available)? $kit_available : 0; ?></div>
           <h3>Total Available Kits</h3>
           </div>
         </div>
         <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
           <div class="tile-stats bg-blue-sky">
             <div class="icon"><i class="fa fa-medkit"></i></div>
            <div class="count"><?php echo !empty($kit_dispatched )? $kit_dispatched : 0; ?></div>
           <h3>Total Dispatched Kits</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6"> 
      <h3>Orders </h3> 
        <div class="row">
          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
           <div class="tile-stats bg-pink">
           <div class="icon"><i class="fa fa-shopping-cart"></i></div>
           <div class="count"> <?php echo !empty($pending_order)? $pending_order : 0; ?></div>
           <h3>Total Pending Orders</h3>
          </div>
        </div>
        <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="tile-stats bg-green-light">
           <div class="icon"><i class="fa fa-shopping-cart"></i></div>
           <div class="count"><?php echo !empty($completed_order )? $completed_order : 0; ?></div>
            <h3>Total Completed Orders</h3>
          </div>
        </div>
      </div>       
    </div>
  </div>

    <!-- <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-green">
        <div class="count" style="padding:13px 0px;">Purchase Request</div>
        <h3> </h3>
      </div>
    </div>
   <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-blue">
        <div class="icon"><i class="fa fa-inr"></i>
        </div>
        <div class="count"> <?php echo !empty($po_pending)? $po_pending : 0; ?></div>
        <h3>Total P.O. Pending</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-red">
        <div class="icon"><i class="fa fa-shopping-cart"></i>
        </div>
        <div class="count"><?php echo !empty($po_received )? $po_received : 0; ?></div>
        <h3>Total P.O. Received</h3>
      </div>
    </div> 
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-green">
        <div class="count" style="padding:13px 0px;">Available Stock</div>
        <h3> </h3>
      </div>
    </div>
   <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-blue">
        <div class="icon"><i class="fa fa-inr"></i>
        </div>
        <div class="count"> <?php echo !empty($ordered_stock)? $ordered_stock : 0; ?></div>
        <h3>Total Stock Ordered</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-red">
        <div class="icon"><i class="fa fa-shopping-cart"></i>
        </div>
        <div class="count"><?php echo !empty($received_stock )? $received_stock : 0; ?></div>
        <h3>Total Stock Received</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-green">
        <div class="count" style="padding:13px 0px;">Available Kits</div>
        <h3> </h3>
      </div>
    </div>
   <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-blue">
        <div class="icon"><i class="fa fa-inr"></i>
        </div>
        <div class="count"> <?php echo !empty($kit_available)? $kit_available : 0; ?></div>
        <h3>Total Available Kits</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-red">
        <div class="icon"><i class="fa fa-shopping-cart"></i>
        </div>
        <div class="count"><?php echo !empty($kit_dispatched )? $kit_dispatched : 0; ?></div>
        <h3>Total Dispatched Kits</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-green">
        <div class="count" style="padding:13px 0px;">Orders</div>
        <h3> </h3>
      </div>
    </div>
   <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-blue">
        <div class="icon"><i class="fa fa-inr"></i>
        </div>
        <div class="count"> <?php echo !empty($pending_order)? $pending_order : 0; ?></div>
        <h3>Total Pending Orders</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="tile-stats bg-red">
        <div class="icon"><i class="fa fa-shopping-cart"></i>
        </div>
        <div class="count"><?php echo !empty($completed_order )? $completed_order : 0; ?></div>
        <h3>Total Completed Orders</h3>
      </div>
    </div>     
  </div>-->
  <!-- /top tiles -->
  <br />
  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="x_panel">       
        <div class="x_title">
          <h2>Latest Order </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
           <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Order No.</th>
                <th>Transaction No.</th>
                <th>Status</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (!empty($order_list)) {
                  $i=1;
                  foreach ($order_list as $odlist){ ?>
                <tr>
                  <th scope="row"><?php echo $i++; ?></th>
                  <td><?php echo (!empty($odlist->txnid))?$odlist->txnid:'-'; ?></td>
                  <td><?php echo (!empty($odlist->order_number))?$odlist->order_number:'-'; ?></td>
                  <td><?php if ($odlist->order_status == 1) { echo "Pending"; }elseif ($odlist->order_status == 2) { echo "Dispatched"; }elseif ($odlist->order_status == 3) { echo "Completed"; } ?></td>
                  <td><?php echo (!empty($odlist->order_date))?date("D d M Y h:i a", strtotime($odlist->order_date)):'-'; ?></td>
                </tr>
                    
              <?php  } 
                }
              ?>
              <tr>
                <td colspan="4"></td>
                <td ><a class="label label-info pull-right" href="<?php echo base_url('inventory/order_list'); ?>">View all..</a></td>
              </tr>
            </tbody>
          </table>
        </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_panel tile fixed_height_320">
        <div class="x_title">
          <h2>Kit Stock</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="dashboard-widget-content">
            <ul class="quick-list">
              <li><i class="fa fa-line-chart"></i><a href="#">Stock Status</a></li>
            </ul>

            <div class="sidebar-widget">
                <h4>Kit</h4>
                <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
                <div class="goal-wrapper">
                  <span id="gauge-text" class="gauge-value pull-left">0</span>
                  <span class="gauge-value pull-left"></span>
                  <span id="goal-text" class="goal-value pull-right"></span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('admin/include/footer1'); ?>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    
    if("undefined"!=typeof Gauge) {
        console.log("init_gauge ["+$(".gauge-chart").length+"]"),
        console.log("init_gauge");
        var a= {
            lines:12,
            angle:0,
            lineWidth:.4,
            pointer: {
                length: .75, strokeWidth: .042, color: "#1D212A"
            }
            ,
            limitMax:"false",
            colorStart:"#1ABC9C",
            colorStop:"#1ABC9C",
            strokeColor:"#F0F3F3",
            generateGradient:!0
        }
        ;
        if($("#chart_gauge_01").length)var b=document.getElementById("chart_gauge_01"),
        c=new Gauge(b).setOptions(a);
        if($("#gauge-text").length&&(c.maxValue='<?php echo $quantity_max; ?>', c.animationSpeed=32, c.set('<?php echo $quantity_set; ?>'), c.setTextField(document.getElementById("gauge-text"))), $("#chart_gauge_02").length)var d=document.getElementById("chart_gauge_02"),
        e=new Gauge(d).setOptions(a);
    }
  });
</script>