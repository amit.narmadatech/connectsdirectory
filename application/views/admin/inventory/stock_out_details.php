<?php $this->load->view('admin/include/header1'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Purchase Order</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="ele2" class="b">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  <i class="fa fa-globe"></i> Invoice
                  <small class="pull-right">Date: <?php echo date("d/m/Y",strtotime($po_request->created_at)); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                <h4><b>Vendor</b></h4>
                <address>
                  <strong><?php echo $po_request->s_name; ?></strong>
                  <br><?php echo $po_request->s_address; ?>
                  <br>Phone: <?php echo $po_request->s_contact1; ?> <?php echo ", ".$po_request->s_contact2; ?>
                  <br>Email: <?php echo $po_request->s_email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <h4><b>Ship To</b></h4>
                <address>
                  <strong><?php echo $po_request->w_name; ?>, [ <?php echo $po_request->w_code; ?> ]</strong>
                    <br><?php echo $po_request->w_address; ?>
                    <br><?php echo $po_request->w_city; ?>, <?php echo $po_request->w_state; ?> <?php echo $po_request->w_pincode; ?>
                    <br>Phone: <?php echo $po_request->w_contact1; ?> <?php echo ", ".$po_request->w_contact2; ?>
                    <br>Email: <?php echo $po_request->w_email; ?>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>PO Number #<?php echo $po_request->reference_no; ?></b>
                <br>
                <br>
                <b>Order ID:</b> 4F3S8J
                <br>
                <b>Payment Due:</b> 2/22/2014
                <br>
                <b>Account:</b> 968-34567
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                 <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Product</th>
                      <th>Qty</th>
                      <th>Unit Price</th>
                      <th>Subtotal</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if (!empty($po_request_details)) {
                      $i = 1;$sub_total = 0;
                      foreach ($po_request_details as $detail) { ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $detail->product_name; ?> [ <?php echo $detail->product_code; ?> ]</td>
                      <td><?php echo $detail->quantity; ?></td>
                      <td><?php echo $detail->rate; ?></td>
                      <td><?php echo $detail->amount;$sub_total += $detail->amount;?></td>
                    </tr>    
                    <?php  }
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;min-height: 115px;">
                  <?php echo $po_request->description; ?>
                </p>
              </div>
              <!-- /.col -->
              <div class="col-xs-6">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td><?php echo $sub_total; ?></td>
                      </tr>
                      <tr>
                        <th>Tax </th>
                        <td><?php echo $tax=0; ?></td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td><?php echo $shipping_charge=0; ?></td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td><?php echo $grand_total=$sub_total; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                <button class="btn btn-default" id="print_po"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-primary pull-right"><i class="fa fa-right"></i> Approve</button>
              </div>
            </div>
          </section>
        </div>
      </div>

    </div>
  </div>

</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>

<script>
    jQuery(document).ready(function($) {

    $('#print_po').click(function(event) {
      $.print(".b");
    });;
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                name: "required",
                code: "required",
                unit: "required",
                default_sale_unit: "required",
                default_purchase_unit: "required",
                purchase_price: "required",
                selling_price: "required",
                alert_quantity: "required",
                opening_stock: "required",
                opening_stock_value: "required",
            },
            messages:{
                name: "The product name field is required",
                code: "The product code field is required",
                unit: "The unit field is required",
                default_sale_unit: "The default sale unit field is required",
                default_purchase_unit: "The default purchase unit field is required",
                purchase_price: "The purchase price field is required",
                selling_price: "The selling price field is required",
                alert_quantity: "The alert quantity field is required",
                opening_stock: "The opening stock field is required",
                opening_stock_value: "The opening stock value field is required.",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });
});
</script>