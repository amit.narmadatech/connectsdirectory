<?php $this->load->view('admin/include/header1'); ?>
<style type="text/css">
    .form-control{ width: 50%; display: inline;}
</style>
<div class="row">
    <div class="col-md-2 col-sm-2 col-xs-2"></div>
    <div class="col-md-8 col-sm-8 col-xs-8" >
        <div class="x_panel">
            <div class="x_content">
                <div id="ele2" class="b">
                    <section class="content invoice" id="outprint">
                        <!-- title row -->
                        <div class="x_title">
                          <div class="row">  
                            <div class="col-md-6">
                                <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="image"> 
                            </div>
                            <div class="col-md-6 text-right">
                               <strong> JC-Care/Retailer Pickup Request</strong>
                            </div>
                          </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 invoice-header">
                                <h1 style="display: inline-block;width: 100%;">
                                    <small class="pull-left"> # <span><?php echo!empty($result) ? $result->refcode : ''; ?></span> </small>
                                    <small class="pull-right">Date: <?php echo date("d/m/Y", strtotime($result->created_at)); ?></small>
                                </h1>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                <h4><b>Pickup Address</b></h4>
                                <?php 
                                    $JccareRetailerData =  getJccareRetailerByRefCode(array('tblclients.ref_code' =>$result->refcode));
                                ?>
                                <address>
                                    <strong><?php echo!empty($JccareRetailerData->name) ? $JccareRetailerData->name : ''; ?></strong>
                                    <br>Phone: <?php echo!empty($JccareRetailerData->phonenumber) ? $JccareRetailerData->phonenumber : ''; ?>
                                    <br>Address: <?php echo!empty($JccareRetailerData->address) ? $JccareRetailerData->address : ''; ?>
                                    ,<?php echo!empty($JccareRetailerData->district) ? getDistrict($JccareRetailerData->district)->district_title : ''; ?>
                                    , <?php echo!empty($JccareRetailerData->state) ? getState($JccareRetailerData->state)->state_title : ''; ?>
                                    ,<?php echo!empty($JccareRetailerData->zip) ? $JccareRetailerData->zip : ''; ?>
                                </address>
                            </div>
                            <div class="col-sm-4 invoice-col">
                                <h4><b>Shipping address</b></h4>
                                <address>
                                    <strong><?php echo!empty($result->contact_person) ? ucwords($result->contact_person) : ''; ?></strong>
                                      <br><?php echo!empty($result->contact_number) ? $result->contact_number : ''; ?>
                                    <br><?php echo!empty($result->address) ? $result->address : ''; ?>
                                    <br><?php echo!empty($result->city) ? $result->city : ''; ?>, <?php echo!empty($result->state) ? $result->state : ''; ?> <?php echo!empty($result->pincode) ? $result->pincode : ''; ?>
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-sm-offset-1 invoice-col">
                                <b>Pickup Date : </b> <?php echo date("d/m/Y", strtotime($result->pickup_date)); ?><br>
                                <b>Pickup Time :   <?php echo isset($result) ? $result->pickup_time : ''; ?></b><br>
                                <b>Quantity:   <?php echo isset($result) ? $result->quantity : ''; ?></b><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 table">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Courier Company</th>
                                                <th>Tracking Number</th>
                                                <th>Date</th>
                                                <th>Note</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($details)) {
                                                $i = 1;
                                                foreach ($details as $dispacthDetails) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $i++; ?></td>
                                                        <td><?php echo!empty($dispacthDetails->courier_company) ? $dispacthDetails->courier_company : '-'; ?></td>
                                                        <td><?php echo!empty($dispacthDetails->tracking_number) ? $dispacthDetails->tracking_number : '-'; ?></td>
                                                        <td><?php echo!empty($dispacthDetails->date) ? date("d/m/Y", strtotime($dispacthDetails->date)) : '-'; ?></td>
                                                        <td><?php echo!empty($dispacthDetails->note) ? $dispacthDetails->note : '-'; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($dispacthDetails->status == 1) {
                                                                echo 'Placed';
                                                            } else if ($dispacthDetails->status == 2) {
                                                                echo 'Completed';
                                                            } else if ($dispacthDetails->status == 3) {
                                                                echo 'Declined';
                                                            }
                                                            ?> 
                                                        </td>
                                                    </tr>
                                                <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>