<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Damage Stock Entry</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('inventory/post_damage_stock'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

      <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Date <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <input readonly type="text" name="purchase_date" id="purchase_date" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->date))))?$edit->date:date("D, d M Y"); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('purchase_date'); ?></span>
          </div>

            <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">P.O. No. <span class="required">*</span>
            </label>
             <div class="col-md-9 col-sm-8"> 
              <input type="text" name="po_no" id="po_no" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->reference_no))))?$edit->reference_no:set_value('po_no'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('po_no'); ?></span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Supplier <span class="required">*</span>
            </label>
           <div class="col-md-9 col-sm-8"> 
              <select name="supplier_id" id="supplier_id" class="form-control">
                  <option></option>
                  <?php if (!empty($supplier_list)) {
                      foreach ($supplier_list as $supplier) { ?>
                  <option <?= (isset($edit) && ($edit->supplier_id == $supplier->id))?'selected':''; ?> value="<?php echo $supplier->id; ?>"><?php echo $supplier->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('supplier_id'); ?></span>
          </div>

          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Warehouse <span class="required">*</span>
            </label>
           <div class="col-md-9 col-sm-8"> 
              <select name="warehouse_id" id="warehouse_id" class="form-control">
                  <option></option>
                  <?php if (!empty($warehouse_list)) {
                      foreach ($warehouse_list as $warehouse) { ?>
                  <option <?= (isset($edit) && ($edit->warehouse_id == $warehouse->id))?'selected':''; ?> value="<?php echo $warehouse->id; ?>"><?php echo $warehouse->title; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('warehouse_id'); ?></span>
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">           
          <div class="col-sm-6 col-xs-12">
            <label class="col-md-3 col-sm-3 col-xs-12" for="last-name">Add Product <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-8"> 
              <select name="product_list" id="product_list" class="form-control">
                  <?php if (!empty($product_list)) {
                      foreach ($product_list as $product) { ?>
                  <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>            
          </div>
          <div class="col-sm-2">
                <div class="form-group">
                        <button id="add_row" class="btn btn-info waves-effect m-t-10" type="button">ADD</button>
                </div>
            </div>
        </div>
      </div>

          <div class="row clearfix">
            <div class="col-sm-12">
            <H3>Items</H3>
            
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Code</th>
                                <th>Reason</th>
                                <th></th>
                            </tr>
                            <tbody id="item_table">
                            <?php 
                            if (!empty($edit_details)) {
                                foreach ($edit_details as $edetail) { ?>
                                 <tr>
                                     <td></td>
                                     <td><?php echo $edetail->code.'-'.$edetail->name; ?><input type="hidden" name="product[]" value="<?php echo $edetail->id;  ?>"></td>
                                     <td><input type="number" class="changeQty form-control" id="<?php echo $edetail->id; ?>" value="<?php echo $edetail->quantity; ?>" min="1" name="quantity[]"></td>

                                     <td><?php echo $edetail->rate; ?><input type="hidden" id="rate_<?php echo $edetail->id; ?>" name="rate[]" value="<?php echo $edetail->rate; ?>"></td>

                                     <td><?php echo $edetail->amount; ?><input type="hidden" id="amount_<?php echo $edetail->id; ?>" name="amount[]" value="<?php echo $edetail->amount; ?>"></td>
                                     
                                     <td>
                                         <button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="<?php echo $edetail->id; ?>">X</button>
                                     </td>
                                 </tr>   
                            <?php    }
                            }
                            ?> 
                            </tbody>
                            
                        </thead>
                    </table>
                </div>
            </div>
        </div>


          <div class="form-group">
           <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-danger" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    var i=1;
    $("#add_row").click(function(event) {
        var id = $("#product_list").val();
        if (id == '') { alert('Please first choose product !');$("#product_list").focus();  return false; }
        //if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  var html = '<tr id="row_'+i+'"><td align="center">'+i+'</td><td style="width:350px;"><input type="text" class="form-control" id="code_'+i+'" value="" name="barcode[]"></td><td><textarea class="form-control" id="reason_'+i+'" name="reason[]" style="height:34px;"></textarea></td><td align="center"><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+i+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  i++;
                  console.log(html);
                }else{
                  console.log(response);
                }
            },"json");
       // }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
    });
    

  $(function () {
      $('#form_validation').validate({
            submitHandler: function(form) {
                if(!$("input[name='barcode[]']").val()){
                  alert("Please select a product first !");
                  return false;
                }else{
                  form.submit();
                }
            },
            ignore: [],
            rules: {
                po_no: "required",
                supplier_id: "required",
                warehouse_id: "required",
            },
            messages:{
                po_no: "purchase order no. field is required",
                supplier_id: "supplier field is required",
                warehouse_id: "warehouse field is required",
            },
           /* highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }*/
        });  

    });

});
</script>
