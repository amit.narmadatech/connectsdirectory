<?php $this->load->view('admin/include/header1'); ?>
<!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Kit Request List</h2>
                    <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#addjccareRetailer">Add Jccare/Retailer</button> 
                <div class="clearfix"></div>
            </div>
            <div class="x_content">         
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                        <tr> 
                            <th>Reference No.</th>
                            <th>Name</th>
                            <th>Quantity</th>
                            <th>Address</th>
                            <th>Pin code</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($request_list)) {
                            foreach ($request_list as $rdlist) {
                                ?>
                                <tr>
                                    <td><?php echo!empty($rdlist->reference_no) ? $rdlist->reference_no : ''; ?></td>
                                    <td><?php echo!empty($rdlist->contact_person) ? ucfirst($rdlist->contact_person) : ''; ?></td>
                                    <td><?php echo!empty($rdlist->quantity) ? $rdlist->quantity : ''; ?></td>
                                    <td><?php echo!empty($rdlist->address) ? $rdlist->address : ''; ?> <?php echo!empty($rdlist->city) ? $rdlist->city : ''; ?>, <?php echo!empty($rdlist->state) ? $rdlist->state : ''; ?></td>
                                    <td><?php echo!empty($rdlist->pincode) ? $rdlist->pincode : ''; ?></td>
                                    <td><?php echo!empty($rdlist->created_at) ? date("D, d M Y", strtotime($rdlist->created_at)) : '' ?></td>
                                    <td>
                                        <?php
                                        if ($rdlist->status == 0) {
                                            echo "<span class='label label-primary'>Draft</span>";
                                        } elseif ($rdlist->status == 1) {
                                            echo "<span class='label label-primary'>Pending</span>";
                                        } /*elseif ($rdlist->status == 2) {
                                            echo "<span class='label label-primary'>Approved</span>";
                                        } */ elseif ($rdlist->status == 3) {
                                            echo "<a href='javascript:void(0);' data-id='$rdlist->id' class='label label-danger rejectedCls'>Rejected</a>";
                                        } elseif ($rdlist->status == 4) {
                                            echo "<span class='label label-primary'>Assigned</span>";
                                        } elseif ($rdlist->status == 5) {
                                            echo "<a href='javascript:void(0);' onclick='dispatched_details($rdlist->id)' data-id='$rdlist->id' class='label label-primary DispatchedCls'>Dispatched</a>";
                                        }
                                         elseif ($rdlist->status == 6) {
                                           echo '<span class="label label-success">Received</span>';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                  
                                     <?php /*if (($rdlist->bh_approval == 1) && ($rdlist->sm_approval == 1) && ($rdlist->inv_approval != 1)  ) { ?>
                                            <a <?php echo ($rdlist->inv_approval == 1) ? 'disabled' : ''; ?> onclick="return confirm('Are you sure ?');" href="<?php echo ($rdlist->inv_approval == 1) ? '#' : base_url('inventory/approve_kit_assignment/' . $rdlist->id); ?>" class="btn btn-info btn-xs">Approve</a>
                                      <?php } */ ?>

                                    <?php if (($rdlist->inv_approval == 1 && $rdlist->status != 6 && $rdlist->status != 5)) { ?> 
                                              <a href="<?php echo base_url('inventory/kit_assignment/'.$rdlist->id); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Assign</a>  
                                    <?php } ?>

                                    <?php if ($rdlist->inv_approval == 2) { ?>
                                            <a href="<?php echo base_url('inventory/kit_assignment/'.$rdlist->id); ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Assign</a>  
                                           <button id="<?php echo $rdlist->id; ?>" type="button" class="btn btn-danger btn-xs rejectBtn" data-toggle="modal" data-target="#reject_form">Reject</button>
                                    <?php  } ?>
                                  
                                    <?php if ($rdlist->status == 4) { ?>
                                             <a href="<?php echo base_url('inventory/view_kit_assignment/' . $rdlist->id); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
                                             <button type="button" onclick="dispatched('<?php echo $rdlist->id; ?>')" class="btn btn-info btn-xs btnModal" >Dispatch Kit </button>
                                    <?php } ?>
                                     <?php if ($rdlist->status == 5) { ?>
                                        <a href="<?php echo base_url('inventory/view_kit_assignment/' . $rdlist->id); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
                                    <?php } ?>
                                          <?php if ($rdlist->status == 6) { ?>
                                        <a href="<?php echo base_url('inventory/view_kit_assignment/' . $rdlist->id); ?>" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a>
                                    <?php } ?>
                                    </td>
                                </tr>
                                <?php }
                            } ?>
                    </tbody>
                </table>       
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="reject_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Reject Request</h4>
            </div>
            <form class="form" novalidate="" method="post" action="<?php echo base_url('inventory/reject_kit_assignment'); ?>" >

                <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />    
                <input type="hidden" name="request_id" id="request_id">
                <div class="modal-body">
                    <textarea id="reason" name="reason" required class="form-control" placeholder="Enter rejection reason" ></textarea>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form name="kitdispatch_request" id="kitdispatch_request">    
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dispatch Form</h4>
                </div>
                <div class="modal-body">
                        <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />    
                        <input type="hidden" name="request_id" id="request_id">
                    <div class="form-group">
                        <label for="shipping_company">Courier Company</label>
                            <input type="text" class="form-control" name="shipping_company" id="shipping_company">
                          <input type="hidden" class="form-control" name="kitrequest_id" id="kitrequest_id">
                    </div>
                    <div class="form-group">
                        <label for="tracking_code">Tracking Code</label>
                        <input  type="text" class="form-control" name="tracking_code" id="tracking_code">
                    </div>
                    <div class="form-group">
                        <label for="shipping_date">Dispatch Date</label>
                        <?php $start_date = date('Y-m-d'); ?>
                        <input  type="date" class="form-control" min="<?php echo $start_date; ?>" name="shipping_date" id="shipping_date">
                    </div>
                    <div class="form-group">
                        <label for="shipping_note">Note</label>
                        <textarea  class="form-control" name="shipping_note" id="shipping_note"></textarea>
                    </div>
                </div>
                  <div id="notification" style="display:none"></div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btnSubmit">Submit</button> 
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form> 
        </div>

    </div>
</div>
<div id="myModalDispach" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dispatch Details</h4>
            </div>
            <div class="modal-body">
                <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                <div class="form-group">
                    <p for="shipping_company">Courier Company :  <span id="shippingcompanyID"> </span> </p>
                </div>
                <div class="form-group">
                    <p for="tracking_code">Tracking Code : <span id="trackingcodeID"> </span>  </p>
                </div>
                <div class="form-group">
                    <p for="shipping_date">Dispatch Date : <span id="shippingdateID"> </span></p>
                </div>
                <div class="form-group">
                    <p for="shipping_note">Note : <span id="NoteID"> </p></span>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div id="addjccareRetailer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Jccare/Retailer Request</h4>
            </div>
            <div class="modal-body">
                <form name="addjccareRetailerfrm" id="addjccareRetailerfrm">    
                    <div class="modal-body">
                        <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />    
                        <div class="form-group">
                            <label for="shipping_company">Name</label>
                            <input type="text" class="form-control" name="contact_person" id="contact_person">
                        </div>
                        <div class="form-group">
                            <label for="shipping_company">Email</label>
                            <input type="text" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="shipping_company">Contact Number</label>
                            <input type="text" class="form-control" name="contact_numer" id="contact_numer">
                        </div>
                        <div class="form-group">
                            <label for="shipping_company">User Id</label>
                            <input type="text" class="form-control" name="user_id" id="user_id">
                        </div>
                        <div class="form-group">
                            <label for="tracking_code">Reference No</label>
                            <input  type="text" class="form-control" name="reference_no" id="reference_no">
                        </div>
                        <div class="form-group">
                            <label for="tracking_code">Kit Quantity</label>
                            <input  type="text" class="form-control" name="kit_qty" id="kit_qty">
                        </div>
                        <div class="form-group">
                            <label for="shipping_date">Address</label>
                            <input  type="text" class="form-control"  name="address" id="address">
                        </div>
                        <div class="form-group">
                            <label for="shipping_date">Pincode</label>
                            <input  type="text" class="form-control"  name="pincode" id="pincode">
                        </div>
                        <div class="form-group">
                            <label for="shipping_note">State Id</label>
                              <input  type="text" class="form-control"  name="state_id" id="state_id">
                        </div>
                        <div class="form-group">
                            <label for="shipping_note">District Id</label>
                              <input  type="text" class="form-control"  name="district_id" id="district_id">
                        </div>
                          <div class="form-group">
                            <label for="shipping_note">City</label>
                              <input  type="text" class="form-control"  name="city" id="city">
                        </div>
                    </div>
                    <div id="notification" style="display:none"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btnSubmit">Submit</button> 
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form> 
            </div>
        </div>

    </div>
</div>



<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on('click', '.rejectBtn', function (event) {
            var id = this.id;
            $('#request_id').val(id);
        });
        $(document).on('click', '.rejectedCls', function (event) {
            
            var id= $(this).attr('data-id');
            if(id == ""){
               return false;
            }
            
            $.ajax({
                url: "<?php echo base_url('inventory/getrejectemsg') ?>",
                type: "POST",
                data:{id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                dataType: "json",
                success: function (response) {
                    if (response.status == true) {
                       bootbox.alert(response.data);
                    } else {
                       console.log(response.status);
                    }
                }
            })
        });
        
            var validator = $("#kitdispatch_request").validate({
                rules: {
                    shipping_company: "required",
                    tracking_code: "required",
                    shipping_date: "required",
                    shipping_note: "required",
                },
                submitHandler: function (e) {
                    $('.btnSubmit').prop('disabled', true);
                    $.ajax({
                        url: "<?php echo base_url('inventory/kitdispatch_request') ?>",
                        type: "POST",
                        data: $('#kitdispatch_request').serialize(),
                        dataType: "json",
                        success: function (response) {
                            $('.btnSubmit').prop('disabled', false);
                            if (response.status == true) {
                                $('#notification').removeClass('alert alert-danger');
                                $('#notification').html(response.msg).addClass('alert alert-success').show().delay(2000).fadeOut("slow", function () {
                                    location.reload();
                                });
                            } else {
                                $('#notification').removeClass('alert alert-success');
                                $('#notification').html(response.msg).addClass('alert alert-danger').show().delay(2000).fadeOut("slow")
                            }
                        }
                    })
                },
                messages: {
                    shipping_company: "Please enter name shipping company",
                    tracking_code: "Please enter tracking code",
                    shipping_date: "Please enter shipping date",
                    shipping_note: "Please enter note",
                }
            });
        
        
    });
    function dispatched(id){
        console.log(id);
      $('#kitrequest_id').val(id);
      $('#myModal').modal('show');
      
    }
    
    function dispatched_details(id){
       $.ajax({
            url: "<?php echo base_url('inventory/dispatched_details') ?>",
            type: "POST",
            data:{id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                   $('#shippingcompanyID').html(response.data.shipping_company); 
                   $('#trackingcodeID').html(response.data.tracking_code); 
                   $('#shippingdateID').html(response.data.shipping_date);
                   $('#NoteID').html(response.data.shipping_note); 
                   $('#myModalDispach').modal('show');
                } else {
                   console.log(response.status);
                }
            }
        });
    }
 
 
         $("#addjccareRetailerfrm").validate({
                rules: {
                    contact_person: "required",
                    email: "required",
                    contact_numer: "required",
                    user_id: "required",
                    reference_no: "required",
                    address: "required",
                    state_id: "required",
                    district_id: "required",
                    state_id: "required",
                    state_id: "required",
                    city: "required",
                },
                submitHandler: function (e) {
                    $('.btnSubmit').prop('disabled', true);
                    $.ajax({
                        url: "<?php echo base_url('inventory/addjccareretailer') ?>",
                        type: "POST",
                        data: $('#addjccareRetailerfrm').serialize(),
                        dataType: "json",
                        success: function (response) {
                            $('.btnSubmit').prop('disabled', false);
                            if (response.status == true) {
                                $('#notification').removeClass('alert alert-danger');
                                $('#notification').html(response.msg).addClass('alert alert-success').show().delay(2000).fadeOut("slow", function () {
                                    location.reload();
                                });
                            } else {
                                $('#notification').removeClass('alert alert-success');
                                $('#notification').html(response.msg).addClass('alert alert-danger').show().delay(2000).fadeOut("slow")
                            }
                        }
                    })
                },
                messages: {
//                    contact_person: "Please enter name",
//                    email: "Please enter email",
//                    contact_numer: "Please enter contact_numer",
//                    reference_no: "Please enter jccare/retailer ID ",
                }
            });
 
 
</script>