<?php $this->load->view('admin/include/header'); ?>


<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Customer List</h2>
          <ul class="nav navbar-right panel_toolbox">
           
            <!-- <li><a href="<?= base_url('inventory/add_warehouse'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Customer</button></a>
            </li> -->
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered customer_list">
            <thead>
              <tr>
                <th width="5%">Sr.No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Created date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
               <?php 
                 if(!empty($customer_list)){
                    $i=1;
                    foreach($customer_list as $ulist){ ?>
                    <tr>
                        <td class="text-center"><?php echo $i++; ?></td>
                        <td><?php  echo !empty($ulist->firstname) ? $ulist->firstname : '' ?> <?php  echo !empty($ulist->lastname) ? $ulist->lastname : '' ?></td>
                        <td><?php  echo !empty($ulist->email) ? $ulist->email : '' ?></td>
                        <td><?php  echo !empty($ulist->contact1) ? $ulist->contact1 : '' ?><?php  echo !empty($ulist->contact2) ?", ".$ulist->contact2 : '' ?></td>
                        <td><?php  echo !empty($ulist->created_at) ? date("D d M Y",strtotime($ulist->created_at)) : '' ?></td>
                        <td><?php  echo ($ulist->status == 0) ?'Inactive' : 'Active'; ?></td>
                        <td>
                          <a href="<?php echo base_url('admindashboard/memberlist/'.$ulist->id);?>" class="btn btn-info">Members</a>
                          <a href="<?php echo base_url('admindashboard/edit_customer/'.$ulist->id);?>" class="btn btn-info">Edit</a>
                        </td>
                    </tr>
                <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>

