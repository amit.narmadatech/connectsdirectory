<?php $this->load->view('admin/include/header'); ?>

<div class="row">
    <form class="form-horizontal form-label-left input_mask" id="form_validation" name="form_validation" method="post" action="<?= base_url('admindashboard/create_group'); ?>" >
      <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
      <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
  <div class="col-md-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>User Role and Permissions</h2>
        <div class="clearfix"></div>
      </div>
      
      <div class="x_content">
        <br />

          <!-- <div class="form-group">
            <label for="hierarchy_id" class="control-label col-md-3 col-sm-3 col-xs-12">User Hierarchy</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="hierarchy_id" id="hierarchy_id" class="form-control">
                    <option></option>
                    <?php
                    foreach ($hierarchy_list as $hlist) { ?>
                    <option <?= (isset($edit) && ($edit->hierarchy_id == $hlist->id))?'selected':''; ?> value="<?= $hlist->id ?>"><?= $hlist->title ?></option>
                    <?php } ?>
                </select>
            </div>
            <span class="text-danger"><?php echo form_error('hierarchy_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="zone_id" class="control-label col-md-3 col-sm-3 col-xs-12">User Hierarchy</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="zone_id" id="zone_id" class="form-control">
                    <option></option>
                    <?php
                    foreach ($zone_list as $zlist) { ?>
                    <option <?= (isset($edit) && ($edit->zone_id == $zlist->id))?'selected':''; ?> value="<?= $zlist->id ?>"><?= $zlist->title ?></option>
                    <?php } ?>
                </select>
            </div>
            <span class="text-danger"><?php echo form_error('zone_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="designation" class="control-label col-md-3 col-sm-3 col-xs-12">Department</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="designation" id="designation" class="form-control">
                    <option></option>
                    <?php
                    foreach ($department_list as $dlist) { ?>
                    <option <?= (isset($edit) && ($edit->designation == $dlist->id))?'selected':''; ?> value="<?= $dlist->id ?>"><?= $dlist->title ?></option>
                    <?php } ?>
                </select>
            </div>
            <span class="text-danger"><?php echo form_error('designation'); ?></span>
          </div> -->

          <div class="form-group">
            <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Title </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="title" id="title" class="form-control" placeholder="Enter title" value="<?= (isset($edit) && (!empty(($edit->title))))?$edit->title:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>

          <div class="form-group">
            <label for="redirect_url" class="control-label col-md-3 col-sm-3 col-xs-12">Redirect URL </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <input type="text" name="redirect_url" id="redirect_url" class="form-control" placeholder="Enter redirect url" value="<?= (isset($edit) && (!empty(($edit->redirect_url))))?$edit->redirect_url:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('redirect_url'); ?></span>
          </div>

          <div class="form-group">
            <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Description </label>
            <div class="col-md-9 col-sm-9 col-xs-12">
              <textarea name="description" id="description" placeholder="Enter description" cols="30" rows="5" class="form-control no-resize"><?= (isset($edit) && !empty(($edit->description)))?$edit->description:''; ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= (isset($edit) && ($edit->status == 1))?'Active':'Inactive'; ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'default':'primary'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success">Submit</button>
                <button class="btn btn-primary" type="reset">Reset</button>
            </div>
          </div>
      </div>
    </div>

  </div>

  <div class="col-md-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Permissions </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Select permissions</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <?php
                $permissions = (isset($edit) && (!empty($edit->permission)))?unserialize($edit->permission):array();
                foreach ($url_list as $url) { ?>
                    <div class="">
                        <label>
                          <input class="js-switch" type="checkbox" name="permission[]" value="<?= $url->url ?>" <?= (in_array($url->url, $permissions))?'checked':''  ?> >&nbsp; <?= ucwords($url->title); ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
          </div>
      </div>
    </div>
  </div>
  </form>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                if ($("input[name='permission[]']").is(":checked")) { form.submit(); }else{ alert('Please select atleast one permission');return false; }
            },
            ignore: [],
            rules: {
                title: "required",
                hierarchy_id: "required",
                designation: "required",
            },
            messages:{
                title: "Please select user hierarchy level",
                hierarchy_id: "Please select designation",
                designation: "Please enter group title",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
    });
</script>
