<?php $this->load->view('admin/include/header'); ?>
<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Diet Plan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br><form id="form_validation" name="form_validation" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" action="<?php echo base_url('admindashboard/savedietplan'); ?>">
                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <input type="hidden" name="edit_id" value="<?= isset($edit) ? $edit->id : ''; ?>">
                    <input type="hidden" name="customer_id" id="customer_id" value="" class="form-control">
                    <input type="hidden" name="plan_id" id="plan_id" value="" class="form-control">
                    <input type="hidden" name="members_id" id="members_id" value="" class="form-control">
                    <div class="row">
                        <div class="col-sm-6">            
                            <div class="form-group">
                                <label class="col-md-4 col-sm-3 col-xs-12" for="barcode">Barcode<span class="required">*</span>
                                </label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <input type="text" id="barcode" name="barcode" required="required" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <button id="get_stock" class="btn btn-info waves-effect m-t-10" type="button">FIND</button> 
                                <a href="<?php echo base_url('inventory/stock_out'); ?>" class="btn btn-default waves-effect m-t-10">RESET</a> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">            
                            <div class="form-group">

                                <label for="customer_name" class="col-md-4 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <input readonly="readonly" type="text" name="customer_name" id="customer_name" value="" class="form-control">
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-6">            
                            <div class="form-group">
                                <label for="customer_email" class="col-md-4 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <input  readonly="readonly" type="text" name="customer_email" id="customer_email" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">            
                            <div class="form-group">
                                <label for="order_date" class="col-md-4 col-sm-3 col-xs-12">Plan Name<span class="required">*</span></label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <input type="text"readonly="readonly"  name="plan_name" id="plan_name" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">            
                            <div class="form-group">
                                <label for="customer_contact" class="col-md-4 col-sm-3 col-xs-12">Contact <span class="required">*</span></label>
                                <div class="col-md-8 col-sm-6 col-xs-12">
                                    <input readonly="readonly" type="text" name="customer_contact" id="customer_contact" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <br>
                    <div class="row clearfix">
                        <div class="col-sm-8">
                            <h2>Diet Plan</h2>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-bordered order-list" >
                                    <thead>
                                        <tr style="background-color:#f7f7f7" >
                                            <td> NO OF MEALS</td>
                                            <td >MEALS TIMINGS</td>
                                            <td>PRIORITIES</td>
                                            <td>TIPS</td>
                                            <td>ACTION</td>
                                        </tr>
                                    </thead>    
                                    <tbody id="item_table">
                                        <tr class="number">
                                            <td><input type="text" id="" name="meal[]" class="form-control MEAL" value="MEAL :1 "></td>
                                            <td><input  type="text" id="sub_total" name="timing[]" class="form-control" value=""></td>
                                            <td><textarea class="form-control" name="proitites[]"  size="5">  </textarea></td>
                                            <td><textarea class="form-control" name="tips[]" size="5">  </textarea></td>
                                            <td><button type="button" class="btn btn-primary " id="addrow"><i class="glyphicon glyphicon-plus"></i> Add</button></td>
                                        </tr>
                                    </tbody>  
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h2>Indications</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered" >
                                    <thead>
                                        <tr style="background-color:#f7f7f7" >
                                            <td>TEXT</td>
                                            <td>ACTION</td>
                                        </tr>
                                    </thead>    
                                    <tbody id="tblIndication">
                                        <tr>
                                            <td><input type="text" id="" name="indication[]" class="form-control" ></td>
                                            <td width="5%"><button type="button" class="btn btn-primary addIndBtn" id="addIndication"><i class="glyphicon glyphicon-plus"></i> Add</button></td>
                                        </tr>
                                    </tbody>  
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">            
                            <div class="form-group">
                                <label class="col-md-2 col-sm-1 col-xs-12">Note</label>
                                <div class="col-md-10 col-sm-8 col-xs-12">
                                  <textarea name="note" id="note" style="margin: 0px; height: 96px; width: 388px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                            <button class="btn btn-danger" type="button">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
</div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function ($) {

        $("#addrow").on("click", function () {

            var newRow = $("<tr class='number'>");
            var cols = "";
            cols += '<td><input type="text" class="form-control MEAL" name="meal[]" value=""/></td>';
            cols += '<td><input type="text" class="form-control" name="timing[]" value=""/></td>';
            cols += '<td><textarea class="form-control" name="proitites[]" size="5"></textarea></td>';
            cols += '<td><textarea class="form-control" name="tips[]"  size="5"></textarea></td>';
            cols += '<td><button type="button" class="ibtnDel btn btn-danger " id="addrow"><i class="glyphicon glyphicon-trash"></i></button></td>';
            newRow.append(cols);
            $("table.order-list").append(newRow);
            var counter = 1;
            $(".MEAL").each(function () {
                $(this).val('MEAL : ' + counter);
                counter++;
            });
        });

        $("table.order-list").on("click", ".ibtnDel", function (event) {
            $(this).closest("tr").remove();
            var counter = 1;
            $(".MEAL").each(function () {
                $(this).val('MEAL : ' + counter);
                counter++;
            });
        });

        $(document).on('click', '#get_stock', function (event) {
            var barcode = $('#barcode').val();
            if (barcode == "") {
                bootbox.alert("Please enter valid barcode");
                return false;
            }
            $('#submitBtn').attr('disabled', true);
            $.post("<?php echo base_url('admindashboard/getbarcodeinfo/') ?>", {barcode: barcode, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (response) {
                $('#submitBtn').attr('disabled', false);
                console.log(response);
                console.log(response.data.name);
                if (response.status) {
                    $('#plan_name').val(response.data.plan_name);
                    $('#customer_email').val(response.data.email);
                    $('#customer_contact').val(response.data.contact);
                    $('#customer_name').val(response.data.name);
                    $('#customer_id').val(response.data.user_id);
                    $('#plan_id').val(response.data.plan_id);
                    $('#members_id').val(response.data.id);
                }
            }, "json");
        });

        $(function () {
            $('#form_validation').validate({
                submitHandler: function (form) {
//                    $('input[name="kit[]"]').each(function (index, val) {
//                        if ($(this).val() == "") {
//                            alert('All QR-Code fields are required !');
//                            return false;
//                        } else {
//                            form.submit();
//                        }
//                    });
                    form.submit();


                },
                ignore: [],
                rules: {
                    'meal[]': "required",
                    'timing[]': "required",
                    'proitites[]': "required",
                    'tips[]': "required",
                    barcode: "required",
                    customer_name: "required",
                    plan_name: "required",
                    customer_email: {
                        required: true,
                        email: true
                    },
                    customer_contact: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 10,
                    }
                },
                messages: {
                    barcode: "Please enter valid barcode",
                },
                highlight: function (input) {
                    $(input).closest('div').addClass('has-error');
                },
            });

        });

      $(document).on('click', '#addIndication', function(event) {
          var num = $('#tblIndication  tr').length + 1;
          txt = '<tr id="row_'+num+'"><td><input type="text" id="" name="indication[]" class="form-control"></td><td width="5%"><button type="button" class="btn btn-danger removeIndBtn" id="'+num+'"><i class="glyphicon glyphicon-trash"></i></button></td></tr>';
          $('#tblIndication').append(txt);
      }); 

      $(document).on('click', '.removeIndBtn', function(event) {
         var id = this.id;
         $('#row_'+id).remove(); 
      });

    });
</script>
