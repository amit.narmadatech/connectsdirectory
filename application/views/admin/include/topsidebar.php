<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <span style="font-size: 20px; padding-top: 15px; display: inline-block;">FairConnects Directory</span>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="<?= $this->session->image ?>" alt=""><?= $this->session->name ?>
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?= base_url('admin/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
        <?php if ($this->session->user_id != 86) { ?>
       
        <li role="presentation" class="dropdown">
          <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>
            <span class="badge bg-green" id="notification_count">0</span>
          </a>
          <ul id="notification_list" class="dropdown-menu list-unstyled msg_list" role="menu">
            <li>
              <div class="text-center">
                <a href="<?php echo base_url("notification"); ?>">
                  <strong>See All Notifications</strong>
                  <i class="fa fa-angle-right"></i>
                </a>
              </div>
            </li>
            
          </ul>
        </li>
         <?php } ?>
      </ul>
    </nav>
  </div>
</div>