<?php if ($this->session->flashdata('alert_success')) { ?>
	<script type="text/javascript">
		new PNotify({
                      title: 'Success',
                      text: '<?php echo $this->session->flashdata('alert_success'); ?>',
                      type: 'success',
                      styling: 'bootstrap3',
                      /*stack: {
			          "dir1": "down",
					  "dir2": "left",
					  "firstpos1":800,
					  "firstpos2": 25,
			        }*/
                  });
	</script>
<?php } ?>

<?php if ($this->session->flashdata('alert_danger')) { ?>
	<script type="text/javascript">
		new PNotify({
                      title: 'Failed',
                      text: '<?php echo $this->session->flashdata('alert_danger'); ?>',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
	</script>
<?php } ?>
<?php if ($this->session->flashdata('alert_warning')) { ?>
    <script type="text/javascript">
		new PNotify({
                      title: 'Warning',
                      text: '<?php echo $this->session->flashdata('alert_warning'); ?>',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
	</script>
<?php } ?>
<?php if ($this->session->flashdata('alert_info')) { ?>
    <script type="text/javascript">
		new PNotify({
                      title: 'Info',
                      text: '<?php echo $this->session->flashdata('alert_info'); ?>',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
	</script>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
 	<script type="text/javascript">
		new PNotify({
                      title: 'Error',
                      text: '<?php echo $this->session->flashdata('error'); ?>',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
	</script>
<?php } ?>

<?php if ($this->session->flashdata('alert_non_blocking')) { ?>
  <script type="text/javascript">

    new PNotify({
                    title: 'Alert',
                    type: 'error',
                    text: '<?php echo $this->session->flashdata('alert_non_blocking'); ?>',
                    nonblock: {
                        nonblock: true
                    },
                    styling: 'bootstrap3',
                    //addclass: 'dark',
                    /*stack: {
                      "dir1": "down",
                      "dir2": "left",
                      "firstpos1":'750',
                      "firstpos2": 25,
                    }*/
                });
  </script>
<?php } ?>