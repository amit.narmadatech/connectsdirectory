<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
  <div class="col-sm-6 col-sm-offset-3">
  	<div class="row">
  		<div class="col-sm-12">
  			<div class="x_panel">
  				<div class="x_title">
  					<h2>User Account Detail</h2>
  					<div class="clearfix"></div>
  				</div>
  				<div class="x_content">
  				  <div class="form-group">
              <label class="col-sm-4 control-label">Name</label>
   				   	<div class="col-sm-8">
     				   	 <p class="form-control-static"><?php echo !empty($kit->name)? $kit->name : '';?></p>
              </div>
 				    </div>
 				    <div class="form-group">
   				   	<label class="col-sm-4 control-label">Email</label>
   				     	<div class="col-sm-8">
     				      <p class="form-control-static"><?php echo !empty($kit->email)? $kit->email : '';?></p>
   					    </div>
 				     </div>
 			    	<div class="form-group">
   					 <label class="col-sm-4 control-label">Mobile</label>
   					   <div class="col-sm-8">
     					   <p class="form-control-static"><?php echo !empty($kit->mobile1)? $kit->mobile1 : '';?></p>
   					   </div>
 				   </div>
 			    	<div class="form-group">
   					   <label class="col-sm-4 control-label">Created date</label>
   					   <div class="col-sm-8">
     				   	 <p class="form-control-static">04/14/2018</p>
   				 	 </div>
 			    	</div>
		    	</div>
	     	</div>
	    </div>
    </div>

    <div class="row">
	   <div class="col-xs-12">
	   	<div class="x_panel">
  			<div class="x_title">
  				 <h2>patient Detail</h2>
  				<div class="clearfix"></div>
  			</div>
  			<div class="x_content">				
				<div class="form-group">
   					 <label class="col-sm-4 control-label">Patient Name</label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">Rehana</p>
   					 </div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Gender </label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">male</p>
   					 </div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Birth </label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">04/14/2018</p>
   					 </div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Age </label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">25year</p>
   					 </div>
 				</div>
			</div>		
	
  			<div class="x_title">
  				<h2>Plan Detail</h2>
  				<div class="clearfix"></div>
  			</div>
  			<div class="x_content">		  				
  				<div class="form-group">
   					<label class="col-sm-4 control-label">Name</label>
   					<div class="col-sm-8">
     				 <p class="form-control-static">Skin Genetics</p>
   					</div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Price</label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">50000</p>
   					 </div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Purchase Date</label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">04/9/2018</p>
   					 </div>
 				</div>
 				<div class="form-group">
   					 <label class="col-sm-4 control-label">Dispatch date</label>
   					 <div class="col-sm-8">
     					 <p class="form-control-static">04/14/2018</p>
   					 </div>
 				</div>
			</div>
	
  			<div class="x_title">
  				<h2>kit Detail</h2>
  				<div class="clearfix"></div>
  			</div>
  		  	<div class="x_content">
		      	<div class="form-group">
   			    	<label class="col-sm-4 control-label">Qr Code</label>
   			      	<div class="col-sm-8">
     		       		 <p class="form-control-static"><?php echo !empty($kit->qrcode)? $kit->qrcode : '';?></p>
   			       </div>
 			      	</div>
 				       <div class="form-group">
   				     	 <label class="col-sm-4 control-label">Barcode </label>
   					     <div class="col-sm-8">
                    <p class="form-control-static">04/14/2018</p>
   				     	 </div>
 				     	</div>
			     	</div>
			
  		    	<div class="x_title">
  			     	<h2>Report Detail</h2>
  			    	<div class="clearfix"></div>
            </div>
  		    	<div class="x_content">	
  			      <div class="form-group">
   			      	<label class="col-sm-4 control-label">Genrated Date </label>
   					    <div class="col-sm-8">
     				     	<p class="form-control-static">04/14/2018</p>
                </div>
 			    	</div>
 				     <div class="form-group">
                <label class="col-sm-4 control-label">Sent Date </label>
   					  <div class="col-sm-8">
     					 <p class="form-control-static">04/14/2018</p>
   					  </div>
 				    </div>
 			    </div>
 		 	 </div>
  		</div>
  	</div>
	</div>
</div>		





<?php $this->load->view('admin/include/footer'); ?>