<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2> Contact-us List  </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th >Email</th>
                <th class="numeric">Phone</th>
                <th>Subject</th>
                <th>Message</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
               <?php if (!empty($contactus)) {
                    foreach ($contactus as $contactusVal) { ?>
                        <tr>
                            <td> <?php echo!empty($contactusVal->name) ? ucfirst($contactusVal->name) : ''; ?></td>
                            <td><?php echo!empty($contactusVal->email) ? $contactusVal->email : ''; ?></td>
                             <td><?php echo!empty($contactusVal->mobile) ? $contactusVal->mobile : '';  ?></td>
                            <td><?php echo!empty($contactusVal->subject) ? $contactusVal->subject : ''; ?></td>
                            <td><?php echo!empty($contactusVal->message) ? $contactusVal->message : ''; ?></td>
                            <td><?php echo!empty($contactusVal->created_date) ? date('d-m-Y', strtotime($contactusVal->created_date)) : ''; ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo site_url('setting/deletecontactus/').$contactusVal->id; ?>" class="btn btn-danger a-btn-slide-text deleteuser">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        <span><strong>Delete</strong></span>            
                                    </a>
                                </div>
                            </td>
                        </tr>
                    <?php }
                } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
   <script>
    $(document).ready(function () {
        $('#datatable').DataTable();
        $(document).on("click", ".deleteuser", function (e) {
            var link = $(this).attr("href");
            e.preventDefault();
            bootbox.confirm("Are you sure you want to delete this?", function (result) {
                if (result) {
                    document.location.href = link;
                }
            });
        });
    });

</script> 
    
