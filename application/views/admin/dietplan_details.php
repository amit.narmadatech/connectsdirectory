<?php $this->load->view('admin/include/header'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Diet Plan Details</h2>       
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br>                        


                            <table cellpadding="0" cellspacing="0" style="width: 794px; border-collapse: collapse; margin:0 auto; background: #E4F6D5;">

                                <tr>
                                    <td>
                                        <table style="background: #E5F6D6; width: 704px; ">
                                            <tr>
                                                <td style="padding-top: 30px; text-align: center;letter-spacing: 10px;  font-family: 'Montserrat', sans-serif; font-weight: 500; color: #5e6063; font-size: 14px;">
                                                    GENOTYPED DIET
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 20px; text-align: center; color: #3c3d3a; font-size: 18px; font-family: 'Montserrat', sans-serif; font-weight: 900;">
                                                    Your genetic guide to
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 2px; font-size: 25px; text-align: center; color: #77876a; letter-spacing: 5px; font-family: 'Montserrat', sans-serif; font-weight: 500;">
                                                    GENOTYPED DIET
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 15px; font-size: 14px; text-align: center; color: #5e6063; letter-spacing: 10px; font-family: 'Montserrat', sans-serif; font-weight: 500;">
                                                    105038569481
                                                </td>
                                            </tr>                                   
                                        </table>
                                    </td>       
                                </tr>

                                <tr>
                                    <td style="vertical-align: top; padding: 10px 45px;">
                                        <h4 style=" font-family: 'Montserrat', sans-serif;  font-weight: 500;  color: #77876a; font-size: 20px;  margin-bottom: 20px;">Indications</h4>
                                        <ul>
                                            <li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat', sans-serif;">
                                                patient is highly likely to be gluten and lactose intolerant.</li>
                                            <li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat', sans-serif; ">less likely to gain weight with saturated fats, higly likely to gain weith with mufa, less to pufa. macro nutrients needs- vit c, vit a, vit b6 and 12, vit e and d.</li>
                                            <li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat', sans-serif; ">and has risk of heart disease.</li>
                                            <li style="font-size: 12px; line-height: 20px; font-family: 'Montserrat', sans-serif; ">diet is formulated on genotyped data obtained by exome sequencing data and genetic interpritations provided by jcgenetics.
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 15px; vertical-align: top;">
                                        <h4 style=" font-family: 'Montserrat', sans-serif; font-weight: 500; color: #77876a; font-size: 25px; text-align: center; margin:0px 0 15px">Diet Plan</h4>
                                        <table border="1" cellpadding="0" cellspacing="0"  style="border:solid 1px #000; border-collapse: collapse;">
                                            <tr>
                                                <th style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 500; padding: 15px 10px; width: 100px; color: #3c3d3a;background: #cbe0b9;">NO OF MEALS TOTAL </th>
                                                <th style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #cbe0b9;">MEAL TIMINGS</th>
                                                <th style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #cbe0b9;">PRIORITIES</th>
                                                <th style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 500; padding: 15px 10px; color: #3c3d3a;background: #cbe0b9;">TIPS</th>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 1</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px; text-align: center">Good Morning- 6:30-7:30</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;">amino one scoop  with one lemon squezeed</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;">labrada - naturally fermented</td>
                                            </tr>

                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 2</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;text-align: center;">8:30 </td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">one apple or pear salad with red sea salt and peeper with almonds,  walnuts and mint leaves. 5 mediterrenian resins  half scoop of isolate protien </td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">finely chopped apples - green or red or pear. add soaked almond or powdered almond with hint of lemon.</td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 3</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;text-align: center;">10:30 Post Workout</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;"> one bowl papaya  one scoop isolate  protien or micronized whey</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 4</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;text-align: center;">11:30 </td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">quinoa khichri/ idli/dosa/moong chilla  </td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">add greated paneer with hint of  garam masala and redchilly powder for taste</td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 5</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;text-align: center;">2:00</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">panner bhurji with peas, white or brownrice pulao with veggies with one bowl of cucumber and mint in hung curd and with one bowl, chickpeas, black gram or lobia alternate days.</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">gram or beans to be given altenate days.<br>drink lemmonade within half an hour</td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 6</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;text-align: center;">5:00</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">protien â€“ and papaya 100 gms day -1 and â€“ 2day two pomegranate juice</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;">MEAL : 7</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;text-align: center;">7:00</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">three whole bolied eggs with one bowl rice and vegetable soup of your choice. </td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px;padding: 5px;">tamato basil, beet root tamato, lauki tamato </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 14px; font-weight: 700; padding: 15px 10px; color: #3C3D3A;"> MEAL : 8</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;text-align: center;">9:30</td>
                                                <td style="font-family: 'Montserrat', sans-serif; font-size: 12px; padding: 5px;">chena and two dates  </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   <td style="padding:5px 25px; vertical-align: top; font-family: 'Montserrat', sans-serif; font-size: 14px;">No use of peanut oil pls.</td>
                                </tr>

                                <tr>
                                    <td style="padding:5px 25px 25px; vertical-align: top; font-family: 'Montserrat', sans-serif; font-size: 14px;">Happy Dieting.<br>
                                            Best Wishes.</td>
                                </tr>
                            </table>
                        
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
