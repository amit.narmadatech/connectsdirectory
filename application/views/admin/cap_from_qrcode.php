<!DOCTYPE html> 
<html>
	<head>
	<title>Registration Form</title>
	<style type="text/css">
		body {
		  counter-reset: chapternum figurenum;
		  font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
		  line-height: 1.5;
		  font-size: 11pt;
		}
</style>
</head>
<body style="margin: 0">

	<table cellspacing="0" cellpadding="0" style=" width: 700px; margin:0 auto;  border-collapse: collapse;">
		<tr>

			<td>
			<table cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
				<tr><td style="width:100px;">
					<?php  if (!empty($qrcode)) {
									foreach ($qrcode as $qrcodes) { 
										if (file_exists("uploads/qrcodes/".$qrcodes->image) && !empty($qrcodes->image)){			
										   
										   $img = base_url('uploads/qrcodes/'.$qrcodes->image);
										}else{
											
											$img = '';
										} ?>	
							<img src="<?php echo $img; ?>" width="100" height="100px" style="float: LEFT;"/>
							<?php }	} ?>
					</td> 
					<td>
					 <h1  style="padding-left:100px; font-size:25px;">PATIENT APPLICATION FROM</h1>
					</td>
					</tr>
				<tr><td style="height: 20px"></td> </tr>
				</table>
			
						
			</td>
		</tr>
		
		<tr>
			<td> Today's  Date ______________________</td> 
		</tr>
		
		<tr><td style="height: 20px"></td> </tr>
		 
		 <tr>
			<td style="width: 600px; border:solid 2px #000;">
				
				<table cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%"> 
					<tr>
						<td style="background: #000; color: #fff; text-align: center;">
							<h2 style="margin: 5px 0"> PATIENT INFORMATION</h2></td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Patient's Name (First, middle, last )
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Patient's Address
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							City/State <span style="float: right;">Zip Code</span>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							<table cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
								<tr>
									<td>Date Of Birth</td>
									<td style="width: 100px">Age</td>
									<td style="width: 100px">Sex</td>
									<td style="width: 155px"> Social Security Number </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							<table cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
								<tr>
									<td style="width: 33%">Marital Status</td>
									<td style="width: 33%">Home Phone</td>									
									<td style="width: 33%"> Work Phone </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							<table cellspacing="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
								<tr>
									<td style="width: 25%">Are You</td>
									<td style="width: 25%">Employed</td>									
									<td style="width: 25%"> Retired </td>
									<td style="width: 25%"> Student </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Employer
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Employer's Address (Street)
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							City/State <span style="float: right;">Zip Code</span>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Occupation
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top; ">
							Nearest Relative, friends (Not in Same Household)
						</td>
					</tr>					
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Address <span style="float: right;">Phone</span>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							City/State <span style="float: right;">Zip Code</span>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Reffring Doctor <span style="float: right;">Family Doctor</span>
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height:25px; vertical-align:top;">
							Medication Taking Now
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height: 100px;  vertical-align:top;">
							Allergic to any Medications?
							<input type="checkbox"> yes 
							<input type="checkbox"> No
							<input type="checkbox"> Dont't Know 

							<br><br>
							If yes, Witch once:
						</td>
					</tr>
					<tr>
						<td style=" border-bottom: solid 1px #000; padding: 5px 5px 10px 5px; height: 100px">
							Alergies:
						</td>
					</tr>
				</table>

			</td>

		</tr>

		<tr>
			<td style="padding-top: 10px;">Reason for today's visit (if result of an accident, give detail include date of injury)  ______________________________________________________________________________
			______________________________________________________________________________

			Is this job related injury: __________________________________________________
		 </td>
			
		</tr>

	</table>
	
		<?php  if (!empty($qrcode)) {
				foreach ($qrcode as $qrcodesval) { 
					if (file_exists("uploads/qrcodes/".$qrcodesval->image) && !empty($qrcodesval->image)){			
					   
					   $img = base_url('uploads/qrcodes/'.$qrcodesval->image);
					}else{
						
						$img = '';
					} ?>
				<center>	
				<img src="<?php echo $img; ?>" width="200" height="200px"/>
				</center>		
		<?php }	} ?>
	

</body>
</html>
