<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('admindashboard/post_department'); ?>" >
    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Department</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
          <div class="form-group">
            <label for="departments" class="col-md-4 col-sm-5 col-xs-12">Select Departments <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="departments" id="departments" class="form-control col-md-7 col-xs-12">
                  <option value="new">New Department</option>
                  <?php if (!empty($departments)) {
                      foreach ($departments as $dep) { ?>
                  <option <?= (isset($edit) && ($edit->department_id == $dep->id))?'selected':''; ?> value="<?= $dep->id ?>"><?= $dep->name ?></option>

                  <?php   }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('departments'); ?></span>
          </div>

          <div class="form-group" id="new_div">
            <label for="department_name" class="col-md-4 col-sm-5 col-xs-12">New Department <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="department_name" name="department_name" class="form-control col-md-7 col-xs-12" placeholder="Enter department" value="<?= (isset($edit) && (!empty(($edit->department_name))))?$edit->department_name:set_value('department_name'); ?>" <?php echo (isset($edit) && ($edit->department_id != 0))?'disabled':''; ?> >
            </div>
            <span class="text-danger"><?php echo form_error('department_name'); ?></span>
          </div>

          <div class="form-group">
            <label for="designation" class="col-md-4 col-sm-5 col-xs-12">Designation <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="designation" name="designation" class="form-control col-md-7 col-xs-12" placeholder="Enter designation" value="<?= (isset($edit) && (!empty(($edit->name))))?$edit->name:set_value('name'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('designation'); ?></span>
          </div>

          <div class="form-group">
            <label for="departments" class="col-md-4 col-sm-5 col-xs-12">Redirect Dashboard <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="dashboard" id="dashboard" class="form-control col-md-7 col-xs-12">
                  <option  value="">Select Dashboard</option>
                  <?php if (!empty($dashboards)) {
                      foreach ($dashboards as $dsb) { ?>
                  <option <?= (isset($edit) && ($edit->dashboard_id == $dsb->id))?'selected':''; ?> value="<?= $dsb->id ?>"><?= $dsb->label ?></option>

                  <?php   }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('dashboard'); ?></span>
          </div>

          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12">Status</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'default':'primary'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('admindashboard/department_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>

        
      </div>

    </div>
  </div>
  
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Set Permission</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php $permissions = (isset($edit) && (!empty($edit->permissions)))?unserialize($edit->permissions):array(); ?>
        <ul class="" style="list-style: none;">
          <?php foreach ($menus as $key => $value) {
          if ($value->parent == 0) { ?>
          <li class=""> 
            <input type="checkbox" name="permission[]" class="mymenu <?php echo ($value->is_dashboard == 1)?'dashboard':'';  ?>" value="<?php echo $value->id; ?>" id="<?php echo $value->id;  ?>" parent_id="<?php echo $value->parent;  ?>" <?= (in_array($value->id, $permissions))?'checked':''  ?>>
            <b><?php echo $value->label; ?></b>
              <ul class="" style="list-style: none;">
            <?php 
              foreach ($menus as $key1 => $value1) {
              if (($value1->parent != 0) && ($value1->parent == $value->id)) { ?>
                <li class="">
                  <input type="checkbox" name="permission[]" class="mymenu" value="<?php echo $value1->id; ?>" id="<?php echo $value1->id;  ?>" parent_id="<?php echo $value1->parent;  ?>" <?= (in_array($value1->id, $permissions))?'checked':''  ?>>
                  <b><?php echo $value1->label; ?></b>
            
                  <ul class="" style="list-style: none;">
                  <?php 
                    foreach ($menus as $key2 => $value2) {
                    if (($value2->parent != 0) && ($value2->parent == $value1->id)) { ?>
                      <li>
                        <input type="checkbox" name="permission[]" class="mymenu" value="<?php echo $value2->id; ?>" id="<?php echo $value2->id;  ?>" parent_id="<?php echo $value2->parent;  ?>" <?= (in_array($value2->id, $permissions))?'checked':''  ?> >
                        <b><?php echo $value2->label; ?></b>
                      </li>
                      
                  <?php  }
                  }
                  ?>
                  </ul>
                </li>
            <?php  }
            }
            ?>
            </ul>

            
          </li>
          <?php  }  
          } ?>
        </ul>
      </div>

    </div>
  </div>
  </form>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<script>
    $(document).ready(function($) {
   
      $(document).on('click', '.mymenu', function(event) {
        if (this.checked) {
           $(this).siblings('ul').find("input[type='checkbox']").prop('checked', true);      
        }else{
           $(this).siblings('ul').find("input[type='checkbox']").prop('checked', false);      
        }
      });

      $(document).on('change', '#departments', function(event) {
        if (this.value != "new") {
          $('#department_name').prop('disabled',true);
        }else{
          $('#department_name').prop('disabled',false);
        }
      });

      $(document).on('change', '#dashboard', function(event) {
        if (this.value != "") {
          $('.dashboard').prop('checked',false);
          $('.dashboard[value="' + this.value + '"]').prop('checked',true);
        }
      });
    
  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                if ($("input[name='permission[]']").is(":checked")) { form.submit(); }else{ alert('Please select atleast one permission');return false; }
            },
            ignore: [],
            rules: {
                department_name: "required",
                designation: "required",
                dashboard: "required",
            },
            messages:{
                name: "The department name field is required",
                designation: "The designation name field is required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });
});
</script>