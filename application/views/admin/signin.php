﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Admin Login | JCGENETICS </title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>build/css/custom.min.css" rel="stylesheet">
    <style type="text/css">
      .logo {text-align: center;}
    </style>
</head>

<body class="login login-bg">
  <div class="login_wrapper">
    
     <?php if (isset($login_error)) { ?>
          <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span> </button>
            <?php echo $login_error; ?>
          </div>
        <?php } ?>
      <div class="animate form login_form">
        <section class="login_content">

          <div class="logo">
             <img src="<?= base_url('assets/images/logo.png'); ?>"> 
          </div>
                 


          <form id="sign_in" action="<?php echo base_url('admin/login'); ?>" method="POST">
            <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
            <!--     <h1>ADMIN JC GENETICS</h1> -->
            <div class="form-inner">
              <div class="msg">
                <h1 class="text-center"><i class="fa fa-lock"></i> Please enter your login details.  </h1>
              </div>
                <div class="form-group">    

                  <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" name="email" class="form-control"  placeholder="Email" required autofocus>
                </div>                    
                  
							    <span style="color: red"><?php echo form_error('email'); ?></span>
                </div>                  

                <div class="form-group">     

                  <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input type="password" name="password"  class="form-control"  placeholder="Password" required>
                </div>

                 
							    <span style="color: red"><?php echo form_error('password'); ?></span>
                </div>
                    
                <div class="row">
                  <div class="col-xs-8 text-left">
					              <?php echo $this->input->cookie('remember_me',true); ?>
                  <input type="checkbox" name="rememberme" id="rememberme" <?php if($this->input->cookie('rememberme')!=''){ ?> checked <?php } ?>class="filled-in chk-col-pink">
                  <label for="rememberme">Remember Me</label>
                </div>
                <div class="col-xs-4 text-right">
                  <button type="submit" class="btn btn-warning" type="submit">SIGN IN</button>
                </div>
              </div>
            </div>
            <div class="separator">                        
              <div class="col-xs-12 text-center forgot-link">
                <a href="<?php echo base_url('admin/recoverpassword'); ?>">Forgot Password?</a>
            </div>
          </div>
        </form>  
      </div>
    </section>        
  </div>
<!-- Jquery Core Js -->
<script src="<?php echo base_url('assets/admin/'); ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap Core Js -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/bootstrap/js/bootstrap.js"></script>
    
<!-- Validation Plugin Js -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url('assets/admin/'); ?>js/admin.js"></script>
<!-- <script src="<?php // echo base_url('assets/admin/'); ?>js/pages/examples/sign-in.js"> </script>-->
  </body>
</html>