﻿<!DOCTYPE html>
<html>
<head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Forgot Password  | JCGENOMICS </title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/admin_theme/'); ?>build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login login-bg">
    <div class="login_wrapper">
        
     
        <?php if($this->session->flashdata('success')){?>
            <div class='alert alert-success'> <?= $this->session->flashdata('success'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
        <?php } ?>

       <div class="animate form login_form">
            <section class="login_content">
              <div class="logo text-center">
         <img src="<?= base_url('assets/images/logo.png'); ?>"> 
        </div>

                <form id="forgot_password" name="forgot_password" method="POST">
                  <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div class="form-inner">
                      <div class="msg">
                       <h1 class="text-center">  Forgot Password </h1>
                      </div>
                      <div class="form-group">

                         <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                 <input type="email" name="email" placeholder="Email" class="form-control" required autofocus>  
                </div>
                        
                                             
                       <span style="color: red" id="errormessage"><?php echo form_error('email'); ?></span>
                      </div>
                        <button class="btn btn-block btn-warning" type="submit">SUBMIT</button>
                       <div class="separator">                        
                            <div class="col-xs-12 text-center forgot-link">
                             <a href="<?php echo base_url('admin');?>">Sign In</a>
                        </div>
                      </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
<script src="<?php echo base_url('assets/admin/'); ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>/plugins/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>/plugins/jquery-validation/jquery.validate.js"></script>
<script>
$(function() {
  $("#forgot_password").validate({
  
    errorPlacement: function(error, element) {
       if (element.attr("name") == "email" ){
          $('#errormessage').html('');
          error.appendTo('#errormessage');
        }else{
          error.insertAfter(element);
        }
    },
  
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
        email: "Please enter a valid email address"
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
</body>
</html>