﻿<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Reset Password | JCGENETICS</title>
        <!-- Favicon-->
        <link rel="icon" href="../../favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?php echo base_url('assets/admin/'); ?>/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Animation Css -->
        <link href="<?php echo base_url('assets/admin/'); ?>/plugins/animate-css/animate.css" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="<?php echo base_url('assets/admin/'); ?>/css/style.css" rel="stylesheet">

          <link href="<?php echo base_url('assets/admin_theme/'); ?>build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="fp-page login-bg">

        <?php if ($this->session->flashdata('success')) { ?>
            <div class='alert alert-success'> <?= $this->session->flashdata('success'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
        <?php } ?>
        <div class="fp-box">
            <div class="logo">
                <img src="<?= base_url('assets/images/logo.png'); ?>">
            </div>
            <div class="card">
                <div class="body">
                    <form id="resetpassword" name="resetpassword" method="POST">
                        <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                        <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                        <div class="msg">
                            <h4> Reset Password </h4>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" name="password" id="password" placeholder="Password" class="form-control" required autofocus>
                            </div>
                            <span style="color: red" id="errorpassword"><?php echo form_error('password'); ?></span>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" name="cpassword" id="cpassword" placeholder="Confirm Password" class="form-control" required autofocus>
                            </div>
                            <span style="color: red" id="errorcpassword"><?php echo form_error('cpassword'); ?></span>
                        </div>
                        <button class="flat-btn" type="submit">RESET PASSWORD</button>
                        <div class="row m-t-20 m-b--5 align-center">
                            <a href="<?php echo base_url('admin'); ?>">Sign In</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url('assets/admin/'); ?>/plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>/plugins/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo base_url('assets/admin/'); ?>/plugins/jquery-validation/jquery.validate.js"></script>
    </body>
</html>
<script>
    $(function () {
        $("#resetpassword").validate({

            errorPlacement: function (error, element) {
                if (element.attr("name") == "password") {
                    $('#errormessage').html('');
                    error.appendTo('#errorpassword');
                } else if (element.attr("name") == "cpassword") {
                    error.appendTo('#errorcpassword');
                    return;
                } else {
                    error.insertAfter(element);
                }
            },

            rules: {
                password: {
                    required: true,
                    minlength: 8,
                },
                cpassword: {
                    required: true,
                    minlength: 8,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter your password",
                    minlength: "The Password field must be at least 8 characters in length",
                },
                cpassword: {
                    required: "Please enter confirm password",
                    equalTo: "Confirm password does not match password",
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>


