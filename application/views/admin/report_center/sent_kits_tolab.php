<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <?php $this->load->view('include/alert'); ?>
      <div class="x_panel">
        <div class="x_title">
          <h2>Kit List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Bar Code</th>
                <th>Plan</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
             <?php 
               if(!empty($kit_list)){
                  $i=1;
                  foreach($kit_list as $klist){ ?>
                  <tr>
                      <td width="5%"><?php  echo $i++; ?></td>
                      <td><?php  echo !empty($klist->barcode) ? $klist->barcode : '' ?></td>
                      <td><?php  echo !empty($klist->plan_name) ? $klist->plan_name : '' ?></td>
                      <td><?php  echo !empty($klist->shipping_date) ? date("D, d M Y", strtotime($klist->shipping_date)) : '' ?></td>
                  </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        
        $(document).on('click', '#changeall', function(event) {
            if (this.checked) {
               $('.cid').prop('checked', true);      
            }else{
               $('.cid').prop('checked', false);      
            }
        });

        $(document).on('click', '#submitBtn', function(event) {

          bootbox.confirm({
              message: "Are you sure ?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                  if (result){

                    var allid = [];
                    $.each($("input[name='id[]']:checked"), function(){            
                        allid.push($(this).val());
                    });

                    if (allid.length != 0) {
                      $.post('<?php echo base_url("collection_center/change_collected_kit_status"); ?>', {allid: allid, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
                        if (data == 'true') {

                          alert('Status changed successfully.');
                          window.location.href = "<?php echo base_url('collection_center/collection_list'); ?>";
                        }
                                      
                      });
                    }

                  }
              }
          });
            
        });
      
      });
    </script>