<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
  
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Upload Test Report</h2>
          
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form data-parsley-validate="" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('rcenter/post_upload_report'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden"  name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <input type="hidden" name="oldreportfile" value="<?= isset($edit)?$edit->report_file:''; ?>">
        <input type="hidden" name="oldpdfreport" value="<?= isset($edit)?$edit->report_pdf:''; ?>">
        <!-- <input type="hidden" name="oldprescription" value="<?= isset($edit)?$edit->prescription_file:''; ?>"> -->
        <br>
          <!-- <div class="form-group">
            <label for="qrcode" class="col-md-4 col-sm-5 col-xs-12">QR code <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="qrcode" name="qrcode" class="form-control col-md-7 col-xs-12" placeholder="Enter QR code" value="<?= (isset($edit) && (!empty(($edit->qrcode))))?$edit->qrcode:set_value('qrcode'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('qrcode'); ?></span>
          </div> -->

          <div class="form-group">
            <label for="barcode" class="col-md-4 col-sm-5 col-xs-12">Barcode <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="barcode" name="barcode" class="form-control col-md-7 col-xs-12" placeholder="Enter Barcode" value="<?= (isset($edit) && (!empty(($edit->barcode))))?$edit->barcode:set_value('barcode'); ?>">
            <span class="text-danger"><?php echo form_error('barcode'); ?></span>
            </div>
          </div>

          <div class="form-group">
            <label for="report_file" class="col-md-4 col-sm-5 col-xs-12">Test Report (JSON)<span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="file" id="report_file" name="report_file" class="form-control col-md-7 col-xs-12" placeholder="Enter report_file">
            </div>
            <span class="text-danger"><?php echo form_error('report_file'); ?></span>
          </div>

          <div class="form-group">
            <label for="report_pdf" class="col-md-4 col-sm-5 col-xs-12">Test Report (PDF)<span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="file" id="report_pdf" name="report_pdf" class="form-control col-md-7 col-xs-12" placeholder="Enter PDF Report">
            </div>
            <span class="text-danger"><?php echo form_error('report_pdf'); ?></span>
          </div>

          <div class="form-group" id="new_div">
            <label for="note" class="col-md-4 col-sm-5 col-xs-12">Note
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Enter department"></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('note'); ?></span>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('rcenter/upload_report'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>
        </form>
     </div>
    </div>
  </div>
  
  
</div>
<?php $this->load->view('admin/include/footer'); ?>
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script> -->

<script>
    $(document).ready(function($) {
   
      $(document).on('click', '.mymenu', function(event) {
        if (this.checked) {
           $(this).siblings('ul').find("input[type='checkbox']").prop('checked', true);      
        }else{
           $(this).siblings('ul').find("input[type='checkbox']").prop('checked', false);      
        }
      });

      $(document).on('change', '#departments', function(event) {
        if (this.value != "new") {
          $('#department_name').prop('disabled',true);
        }else{
          $('#department_name').prop('disabled',false);
        }
      });
    
  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                //if ($("input[name='permission[]']").is(":checked")) { form.submit(); }else{ alert('Please select atleast one permission');return false; }
                form.submit();
            },
            ignore: [],
            rules: {
                //qrcode: "required",
                barcode: "required",
                //report_file: "required",
                report_pdf: "required",
            },
            messages:{
                //qrcode: "The QR code field is required",
                barcode: "The Barcode field is required",
                //report_file: "The test report json file is required",
                report_pdf: "The test report pdf file is required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });
});
</script>