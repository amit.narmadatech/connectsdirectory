<?php $this->load->view('admin/include/header1'); ?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Dispatch Form</h2>
        <ul class="nav navbar-right panel_toolbox">

        </ul>
        <div class="clearfix"></div>
      </div>
        <form id="form_validation" name="form_validation" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" action="<?= base_url('rcenter/post_stock_out'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
      <div class="x_content">
        <br>
       
       <div class="row">
        <div class="col-sm-6">            
           <div class="form-group">
             <label class="col-md-4 col-sm-3 col-xs-12" for="order_num">PO Number <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-6 col-xs-12">
               <input type="text" id="order_num" name="po_number" required="required" class="form-control" value="<?= (isset($edit) && (!empty(($edit->reference_no))))?$edit->reference_no:''; ?>">
            </div>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="form-group">
            <button id="get_stock" class="btn btn-info waves-effect m-t-10" type="button">FIND</button> 
             <a href="<?php echo base_url('rcenter/dispatch_order'); ?>" class="btn btn-default waves-effect m-t-10">RESET</a> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">            
          <div class="form-group">

            <label for="customer_name" class="col-md-4 col-sm-3 col-xs-12">Customer</label>
              <div class="col-md-8 col-sm-6 col-xs-12">
                <input type="text" name="customer_name" id="customer_name" value="<?= (isset($edit) && (!empty(($edit->w_contact_person))))?$edit->w_contact_person:set_value('customer_name'); ?>" class="form-control">
              </div>
            </div>
          </div> 

          <div class="col-sm-6">            
            <div class="form-group">  
              <label for="shipping_partner" class="col-md-4 col-sm-3 col-xs-12">Shipping Partner</label>
              <div class="col-md-8 col-sm-6 col-xs-12">
                <input type="text" name="shipping_partner" id="shipping_partner" value="<?php echo set_value('shipping_partner'); ?>" class="form-control">
              </div>
             </div>  
            </div>
          </div>
              
         <div class="row">
            <div class="col-sm-6">            
              <div class="form-group">
                <label for="customer_email" class="col-md-4 col-sm-3 col-xs-12">Email</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="customer_email" id="customer_email" value="<?= (isset($edit) && (!empty(($edit->w_email))))?$edit->w_email:set_value('customer_email'); ?>" class="form-control">
                </div>
              </div>
            </div>

            <div class="col-sm-6">            
              <div class="form-group">
                <label for="tracking_no" class="col-md-4 col-sm-3 col-xs-12">Tracking No.</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="tracking_no" id="tracking_no" value="<?= (isset($edit) && (!empty(($edit->tracking_no))))?$edit->tracking_no:set_value('tracking_no'); ?>" class="form-control">
                </div>
              </div>
            </div>
           </div>

          <div class="row">
            <div class="col-sm-6">            
              <div class="form-group">
                <label for="customer_contact" class="col-md-4 col-sm-3 col-xs-12">Contact</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="customer_contact" id="customer_contact" value="<?= (isset($edit) && (!empty(($edit->w_contact1))))?$edit->w_contact1:set_value('customer_contact'); ?>" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="invoice_no" class="col-md-4 col-sm-3 col-xs-12">Invoice No.</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input type="text" name="invoice_no" id="invoice_no" value="<?= (isset($edit) && (!empty(($edit->order_number))))?$edit->order_number:set_value('invoice_no'); ?>" class="form-control">
                </div>
              </div>
            </div>

            <div class="col-sm-6">            
                 <div class="form-group">
                  <label for="shipping_address" class="col-md-4 col-sm-3 col-xs-12">Shipping Address</label>
                 <div class="col-md-8 col-sm-6 col-xs-12">
                      <textarea name="shipping_address" id="shipping_address" value="" rows="2" class="form-control" ><?= (isset($edit) && (!empty(($edit->w_address))))?$edit->w_address:''; ?><?= (isset($edit) && (!empty(($edit->w_city))))?", ".$edit->w_city:''; ?> <?= (isset($edit) && (!empty(($edit->w_state))))?", ".$edit->w_state:''; ?><?= (isset($edit) && (!empty(($edit->w_pincode))))?", ".$edit->w_pincode:''; ?>
                      </textarea>
                   </div>
                 </div>
            </div>

          </div>

          </div>
          <div class="row clearfix">
          <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered ">
                        <thead>
                            <tr>
                                <td align="center" width="5">#</td>
                                <td align="center" style="width: 55%;">Item</td>
                                <td align="center">Required Qty</td>
                                <td align="center">Uploaded Qty</td>
                                <td align="center">Action</td>
                            </tr>
                            <tbody id="item_table">
                            <?php 
                            $sub_total = 0;
                            if (!empty($edit_details)) {
                              $i=0;
                                foreach ($edit_details as $edetail) {  
                                ?>
                                 <tr>
                                     <td align="center"><?php echo ++$i; ?></td>
                                     <td align="center"><?php echo $edetail->product_name; ?>
                                       <input type="hidden" name="product_id[]" value="<?php echo $edetail->product_id; ?>">
                                     </td>
                                     <td align="center">
                                     <?php echo $edetail->product_qty; ?>
                                     <input type="hidden" id="required_qty" name="required_qty" value="<?php echo $edetail->product_qty; ?>">
                                     </td>
                                     <td align="center"><?php echo countScannedKit($edetail->request_id); ?>
                                     <input type="hidden" id="uploaded_qty" name="uploaded_qty" value="<?php echo countScannedKit($edetail->request_id); ?>">
                                     </td>
                                     <td align="center">
                                      <button type="button" class="btn btn-primary btn-xs showKitList" data-toggle="modal" data-target="#scannedKitList" id="<?php echo $edetail->request_id; ?>">See List</button>
                                     </td>
                                 </tr>   
                            <?php   }
                            }
                            ?> 
                            </tbody>
                                <!-- <tr>
                                  <td rowspan="5" colspan="3">
                                    <label class="col-md-12">Note:</label>
                                    <textarea class="form-control" rows="5" name="note" id="note"></textarea>
                                  </td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Sub-Total</b></td>
                                    <td><input type="text" id="sub_total" name="sub_total" class="form-control" value="<?php echo $sub_total; ?>"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Tax</b></td>
                                    <td><input type="text" id="tax_amount" name="tax_amount" class="form-control" value="0"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Discount</b></td>
                                    <td><input type="text" id="discount_amount" name="discount_amount" class="form-control" value="0"></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Total</b></td>
                                    <td><input type="text" id="grand_total" name="grand_total" class="form-control" value="<?php echo $sub_total; ?>"></td>
                                </tr> -->

                        </thead>
                    </table>
                </div>
            </div>
        </div>

          <div class="form-group">
           <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-danger" type="button">Cancel</button>
            </div>
          </div>

        </form>
     </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="scannedKitList" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Scanned Kit List</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <tr>
              <th width="5%">Sr.No.</th>
              <th>Barcode</th>
            </tr>
            <tbody id="scanKitArea"></tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php $this->load->view('admin/include/footer1'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {

    $( ".kit" ).blur(function() {
        var kitcodes = new Array(); 
        var obj = this;
        var code = $.trim(obj.value);
        if(code){
          $.post("<?php echo base_url('kit/verifykitcode'); ?>",{ code : code, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              
              var id = $(obj).attr('id'); 

              if(!response){
                 if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Please enter right code.</span>').insertAfter(obj);
                 } 
              }else
              {
                $('input[name="kit[]"]').each(function(index, val) {
                    console.log($(this).id);
                    console.log(val.id);
                    if((val.id != obj.id) && ($(this).val() != "")){
                      kitcodes.push($(this).val());
                    }
                });

                if ($.inArray(code, kitcodes)  == -1) {
                  $(obj).next('span').remove();
                }else{
                  if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Please enter right code.</span>').insertAfter(obj);
                    $(obj).val('');
                 }                  
                }

              }
          },"json");
        }else{
          $(obj).next('span').remove();
        }
        
    });

    $(document).on('click', '.showKitList', function(event) {
        var id = this.id;
        console.log(id);
        $.post("<?php echo base_url('rcenter/getScannedKitList'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
          var data = JSON.parse(data);
          var txt = '';
          var i = 1;

          $.each(data.result, function(index, val) {
             txt += '<tr><td>'+i+++'</td><td>'+val.barcode+'</td></tr>';
          });

          $('#scanKitArea').html(txt);

        });
    });


    $('#get_stock').click(function(event) {
      var onum = $('#order_num').val();
      if (onum != "") { window.location.href="<?php echo base_url('rcenter/get_po_by_ponumber/') ?>"+onum; }else{ alert('Please enter some value !'); $('#order_num').focus(); }
    });;


    $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                /*var required_qty = $('#required_qty').val();
                var uploaded_qty = $('#uploaded_qty').val();
                if (required_qty != uploaded_qty) {
                  alert('Uploaded quantity is less then Required quantity !'); return false;
                }else{ 
                  form.submit();
                }*/
                  form.submit();
                
            },
            ignore: [],
            rules: {
                po_number: "required",
                shipping_partner: "required",
                shipping_address: "required",
                
            },
            messages:{
                po_number: "The PO number field is required",
                title: "The title field is required",
                code: "The warehouse code field is required",
                contact_person: "The contact person field is required",
            },
            highlight: function (input) {
                $(input).closest('div').addClass('has-error');
            },
        });

    });

});
</script>
