<?php $this->load->view('admin/include/header1'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
  
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Damage Kit Entry</h2>
          
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form data-parsley-validate="" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('rcenter/post_damage_entry'); ?>" >
        <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />  
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <input type="hidden" name="oldimg" value="<?= isset($edit)?$edit->damage_img:''; ?>">
        <!-- <input type="hidden" name="oldprescription" value="<?= isset($edit)?$edit->prescription_file:''; ?>"> -->
        <br>
          <!-- <div class="form-group">
            <label for="qrcode" class="col-md-4 col-sm-5 col-xs-12">QR code <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="qrcode" name="qrcode" class="form-control col-md-7 col-xs-12" placeholder="Enter QR code" value="<?= (isset($edit) && (!empty(($edit->qrcode))))?$edit->qrcode:set_value('qrcode'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('qrcode'); ?></span>
          </div> -->

          <div class="form-group">
            <label for="barcode" class="col-md-4 col-sm-5 col-xs-12">Barcode <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="barcode" name="barcode" class="form-control col-md-7 col-xs-12" placeholder="Enter Barcode" value="<?= (isset($edit) && (!empty(($edit->barcode))))?$edit->barcode:set_value('barcode'); ?>">
            <span class="text-danger"><?php echo form_error('barcode'); ?></span>
            </div>
          </div>

          <div class="form-group">
            <label for="report_file" class="col-md-4 col-sm-5 col-xs-12">Image <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="file" id="damage_img" name="damage_img" class="form-control col-md-7 col-xs-12">
            </div>
            <span class="text-danger"><?php echo form_error('damage_img'); ?></span>
          </div>

          <div class="form-group" id="new_div">
            <label for="note" class="col-md-4 col-sm-5 col-xs-12">Note
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Enter reason"></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('note'); ?></span>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('admindashboard/department_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>
        </form>
     </div>
    </div>
  </div>
  
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Damage Record List</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th></th>
                <!-- <th>QR Code</th> -->
                <th>Barcode</th>
                <th>Date</th>
                <th>Test Report</th>
              </tr>
            </thead>


            <tbody>
              <?php 
                $i = 1;
               if(!empty($damage_list)){
                foreach($damage_list as $dlist){ ?>
                <tr>
                  <td align="center"><?php echo $i++; ?></td>
                  <td><?php  echo !empty($dlist->barcode) ? $dlist->barcode : '-' ?></td>
                  <!-- <td><?php  echo !empty($dlist->qrcode) ? $dlist->qrcode : '' ?></td> -->
                  <td><?php  echo !empty($dlist->created_at) ? date("D, d M Y",strtotime($dlist->created_at)) : '' ?></td>
                  <td>
                    <?php if (!empty($dlist->damage_img)): ?>
                      <a class="btn btn-success btn-xs" href="<?php echo base_url("uploads/damage_img/$dlist->damage_img"); ?>" download>Download</a>
                    <?php endif ?>
                  </td>
                </tr>
              <?php } } ?>
            </tbody>

          </table>
      </div>

    </div>
  </div>
  
</div>
<?php $this->load->view('admin/include/footer1'); ?>
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
    <script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>
<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script> -->

<script>
    $(document).ready(function($) {
    
     
    $(document).scannerDetection({
        timeBeforeScanTest: 200, // wait for the next character for upto 200ms
        startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
        endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
        avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
        onComplete: function(barcode, qty){
          $('#barcode').val(barcode);
        } 
      });
    
    $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                //if ($("input[name='permission[]']").is(":checked")) { form.submit(); }else{ alert('Please select atleast one permission');return false; }
                form.submit();
            },
            ignore: [],
            rules: {
                //qrcode: "required",
                barcode: "required",
                damage_img: "required",
                note: "required",
            },
            messages:{
                barcode: "The Barcode field is required",
                note: "Damage reason field is required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });

});
</script>