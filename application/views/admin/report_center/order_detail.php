<?php $this->load->view('admin/include/header'); ?>

<style type="text/css">
  .invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Order Detail</h2>
      
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="container">
          <div class="row">
              <div class="col-xs-12">
              <div class="invoice-title">
                <h2>Invoice</h2><h3 class="pull-right">Order # <?php  echo !empty($order->order_number) ? $order->order_number : '' ?></h3>
              </div>
              <hr>
              <div class="row">
                <div class="col-xs-6">
                  <address>
                  <strong>Billed To:</strong><br>
                    <?php  echo !empty($order->c_name) ? $order->c_name : '' ?><br>
                    <?php  echo !empty($order->email) ? $order->email : '' ?><br>
                    <?php  echo !empty($order->contact1) ? $order->contact1 : '' ?><?php  echo !empty($order->contact2) ? ", ".$order->contact2 : '' ?><br>
                  </address>
                </div>
                <div class="col-xs-6 text-right">
                  <address>
                    <strong>Shipped To:</strong><br>
                    <?php  echo !empty($order->bl_name) ? $order->bl_name : '' ?><br>
                    <?php  echo !empty($order->bl_email) ? $order->bl_email : '' ?><br>
                    <?php  echo !empty($order->bl_address) ? $order->bl_address : '' ?><br><?php  echo !empty($order->bl_landmark) ? $order->bl_landmark : '' ?> , 
                    <?php  echo !empty($order->bl_locality) ? $order->bl_locality : '' ?><br>
                    <?php  echo !empty($order->city) ? $order->city : '' ?>,
                    <?php  echo !empty($order->state) ? $order->state : '' ?>,
                    <?php  echo !empty($order->pincode) ? $order->pincode : '' ?>
                    
                  </address>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  <address>
                    <strong>Payment Method:</strong><br>
                    <?php if (!empty($order->payment_mode) && $order->payment_mode == "CC") { echo "Credit Card"; }  ?><br>
                    <strong>Transaction ID #</strong>
                    <?php  echo !empty($order->txnid) ? $order->txnid : '' ?>
                  </address>
                </div>
                <div class="col-xs-6 text-right">
                  <address>
                    <strong>Order Date:</strong><br>
                    <?php  echo !empty($order->order_date) ? date("D d M Y", strtotime($order->order_date)): '' ?><br><br>
                  </address>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-condensed">
                      <thead>
                          <tr>
                            <th>Image</th>
                            <th>Plan Name</th>
                            <th>Quantity</th>
                            <th>Unit Price</th>
                            <th>Amount</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        $total = 0; 
                        foreach ($order_details as $odetail) {                         
                        ?>
                        <tr>
                          <td>
                            <img src="<?php echo base_url("uploads/plan_img/$odetail->plan_image"); ?>" class="img-responsive" width="120" height="80" title="as">
                          </td>
                          <td><?php  echo !empty($odetail->plan_name) ? $odetail->plan_name : '' ?></td>
                          <td><?php  echo !empty($odetail->quantity) ? $odetail->quantity : '' ?></td>
                          <td><?php  echo !empty($odetail->plan_price) ? $odetail->plan_price : '' ?></td>
                          <td><?php  $total += (float)$odetail->quantity * (float)$odetail->plan_price; 
                          echo (float)$odetail->quantity * (float)$odetail->plan_price;
                          ?></td>
                          
                        </tr>
                       <?php } ?>
                        <tr>
                          <td class="thick-line"></td>
                          <td class="thick-line"></td>
                          <td class="thick-line"></td>
                          <td class="thick-line"><strong>Sub-total</strong></td>
                          <td class="thick-line"><?php echo!empty($order->base_amount) ? $order->base_amount : '0.00'; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>Discount</strong></td>
                          <td class="no-line"><?php echo!empty($order->discount) ? '- '.$order->discount : '0.00'; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>Amount</strong></td>
                          <td class="no-line"><?php echo !empty($order->base_amount) ? $order->base_amount - $order->discount : ''; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>CGST</strong></td>
                          <td class="no-line"><?php echo!empty($order->cgst) ? $order->cgst : '0.00'; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>SGST</strong></td>
                          <td class="no-line"><?php echo!empty($order->sgst) ? $order->sgst : '0.00'; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>IGST</strong></td>
                          <td class="no-line"><?php echo!empty($order->igst) ? $order->igst : '0.00'; ?></td>
                        </tr>
                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"></td>
                          <td class="no-line"><strong>Grand Total</strong></td>
                          <td class="no-line"><?php echo!empty($order->total_amount) ? $order->total_amount : ''; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    $("#add_row").click(function(event) {
        var id = $("#product_list").val();
        if (id == '') { alert('Please first choose product !');$("#product_list").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  var html = '<tr id="row_'+response.data.id+'"><td></td><td>'+response.data.code+'-'+response.data.name+'<input type="hidden" name="product[]" value="'+response.data.id+'"></td><td style="width:175px;"><input type="number" class="changeQty form-control" id="'+response.data.id+'" value="1" min="1" name="quantity[]"></td><td><span id="rateText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="rate_'+response.data.id+'" name="rate[]" value="'+response.data.purchase_price+'"></td><td><span id="amountText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="amount_'+response.data.id+'" name="amount[]" value="'+response.data.purchase_price+'"></td><td><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+response.data.id+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  $('#grandTotalText').html(response.data.purchase_price);
                  $('#grand_total').val(response.data.purchase_price);
                  console.log(html);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                    return false;                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
    });

    $(document).on('change', '.changeQty', function(event) {
        var id = this.id;
        var qty = parseFloat(this.value);
        var rate = parseFloat($('#rate_'+id).val());
        var amt = 0;
        if( (!isNaN(qty)) && (!isNaN(rate))){
            amt = qty*rate;
            $('#amountText_'+id).html(amt); 
            $('#amount_'+id).val(amt);
            $('#grandTotalText').html(amt); 
            $('#grand_total').val(amt);
        }
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

    $(function () {
        $('#form_validation1').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    numeric:true,
                    required:true,
                },
                contact2: {
                    numeric:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
            },
            messages:{
                title: "Please enter title",
                hierarchy_id: "Please select designation",
                designation: "Please enter group title",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });

});
</script>
