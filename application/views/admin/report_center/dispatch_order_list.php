<?php $this->load->view('admin/include/header1'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Dispatched Stock List</h2>
          <ul class="nav navbar-right panel_toolbox">            
            <li><a href="<?= base_url('rcenter/dispatch_order'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Dispatch stock</button></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">        
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Sr. No.</th>
                <th>Purchase Order</th>
                <th>Invoice No.</th>
                <th>Shipping Partner</th>
                <th>Tracking Number</th>
                <th>Address</th>
                <th>Dispatch Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
             if(!empty($dispatch_list)){
                $i = 1;
                foreach($dispatch_list as $dplist){ ?>
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php  echo !empty($dplist->reference_no) ? $dplist->reference_no : '' ?></td>
                    <td><?php  echo !empty($dplist->invoice_no  ) ? $dplist->invoice_no  : '' ?></td>
                    <td><?php  echo !empty($dplist->shipping_partner) ? $dplist->shipping_partner : '' ?></td>
                    <td><?php  echo !empty($dplist->tracking_no) ? $dplist->tracking_no : '' ?></td>
                    <td><?php  echo !empty($dplist->shipping_address) ? $dplist->shipping_address : '' ?></td>
                    <td><?php  echo !empty($dplist->created_at) ?date("D, d M Y",strtotime($dplist->created_at)): '' ?></td>
                    
                    <td>
                      <!-- <a href="<?php echo base_url('rcenter/order_detail/'.$dplist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-file"></i> View</a> -->
                      <button type="button" class="btn btn-primary btn-xs showKitList" data-toggle="modal" data-target="#scannedKitList" id="<?php echo $dplist->request_id; ?>">Kit List</button>
                    </td>
                </tr>
            <?php } } ?>
            </tbody>
          </table>      
        </div>
      </div>
    </div>

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Kit Details</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered b">
              <tr>
                <th width="10">#</th>
                <th>Plan</th>
                <th>Kit</th>
              </tr>
              <tbody id="detail_div">
                
              </tbody>
              
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="print_btn">Print</button>
          </div>

        </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="scannedKitList" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Scanned Kit List</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <tr>
              <th width="5%">Sr.No.</th>
              <th>Barcode</th>
            </tr>
            <tbody id="scanKitArea"></tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= base_url("assets/js/print/jQuery.print.js"); ?>"></script>

    <script type="text/javascript">

    jQuery(document).ready(function($) {
      
      $('#print_btn').click(function(event) {
        $.print(".b");
      });
      
      $(".details").click(function(event) {
          var id = this.id;
            $.post("<?php echo base_url('inventory/get_kits_by_order_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response){
                  var rows = '';
                  var i =1;
                  $.each(response, function(index, val) {
                     rows += '<tr><td>'+i+++'</td><td>'+val.plan_name+'</td><td>'+val.qr_code+'</td></tr>';
                  });
                  console.log(rows);
                  $('#detail_div').html(rows);
                }else{
                  console.log(response);
                }
            },"json");
      });

      $(document).on('click', '.showKitList', function(event) {
          var id = this.id;
          $.post("<?php echo base_url('rcenter/getScannedKitList'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
            var data = JSON.parse(data);
            var txt = '';
            var i = 1;
            $.each(data.result, function(index, val) {
               txt += '<tr><td>'+i+++'</td><td>'+val.barcode+'</td></tr>';
            });

            $('#scanKitArea').html(txt);

          });
      });
      
    });


    
    </script>