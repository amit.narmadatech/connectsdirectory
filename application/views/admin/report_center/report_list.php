<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Test Report List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th></th>
                <!-- <th>QR code</th> -->
                <th>Barcode</th>
                <th>Test Report (JSON)</th>
                <th>Test Report (PDF)</th>
                <th>Note</th>
                <th>Date</th>
              </tr>
            </thead>


            <tbody>
              <?php 
                $i = 1;
               if(!empty($report_list)){
                foreach($report_list as $rlist){ ?>
                <tr>
                  <td align="center"><?php echo $i++; ?></td>
                  <!-- <td><?php  echo !empty($rlist->qrcode) ? $rlist->qrcode : '' ?></td> -->
                  <td><?php  echo !empty($rlist->code) ? $rlist->code : '-' ?></td>
                  <td>
                    <?php if (!empty($rlist->file)): ?>
                    <a class="btn btn-success btn-xs" href="<?php echo base_url("uploads/report_files/$rlist->file"); ?>" download>Download</a>  
                    <?php endif ?>
                  </td>
                  <td>
                    <?php if (!empty($rlist->pdf_file)): ?> 
                    <a class="btn btn-success btn-xs" href="<?php echo base_url("uploads/report_files/$rlist->pdf_file"); ?>" download>Download</a></td>
                    <?php endif ?>
                  <td><?php  echo !empty($rlist->note) ? $rlist->note : '-' ?></td>
                  <td><?php  echo !empty($rlist->created_at) ? date("D, d M Y",strtotime($rlist->created_at)) : '' ?></td>
                </tr>
              <?php } } ?>
            </tbody>

          </table>
        </div>
      </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Damage Stock List</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered b">
              <tr>
                <th width="10">#</th>
                <th>Code</th>
                <th>Reason</th>
              </tr>
              <tbody id="detail_div">
                
              </tbody>
              
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="print_btn">Print</button>
          </div>

        </div>
      </div>
    </div>

</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= base_url("assets/js/print/jQuery.print.js"); ?>"></script>
    <script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    
    $('#print_btn').click(function(event) {
      $.print(".b");
    });
    
    $(".details").click(function(event) {
        var id = this.id;
          $.post("<?php echo base_url('inventory/get_damage_stock_detail'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response){
                var rows = '';
                var i =1;
                $.each(response, function(index, val) {
                   rows += '<tr><td>'+i+++'</td><td>'+val.code+'</td><td>'+val.reason+'</td></tr>';
                });
                console.log(rows);
                $('#detail_div').html(rows);
              }else{
                console.log(response);
              }
          },"json");
    });

});
</script>