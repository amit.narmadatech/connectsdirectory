<?php $this->load->view('admin/include/header'); ?>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Purchase Order List</h2>
          <ul class="nav navbar-right panel_toolbox">           
            
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">         
        <table id="datatable-buttons" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Reference No.</th>
              <th>Warehouse</th>
              <th>Supplier</th>
              <th>Product</th>
              <th>Quantity</th>
              <th>Purchase Status</th>
              <th>Created Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
             if(!empty($request_list)){                
                foreach($request_list as $rdlist){ ?>
                <tr>
                    <td><?php  echo !empty($rdlist->reference_no) ? $rdlist->reference_no : '' ?></td>
                    <td><?php  echo !empty($rdlist->warehouse_name) ? $rdlist->warehouse_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->supplier_name) ? $rdlist->supplier_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->product_name) ? $rdlist->product_name : '' ?></td>
                    <td><?php  echo !empty($rdlist->product_qty) ? $rdlist->product_qty : '' ?>
                      <span class="badge pull-right"><?php echo countScannedKit($rdlist->id); ?></span>
                    </td>
                    <td>
                      <?php  
                        if($rdlist->is_paid == 1)
                          { echo 'payment done'; }
                        elseif($rdlist->is_paid == 0)
                          {echo 'waiting for approvals'; } 
                      ?>
                    </td>
                    <td><?php  echo !empty($rdlist->created_at) ?date("D, d M Y",strtotime($rdlist->created_at)): '' ?></td>
                    <td>
                      <a target="_new" href="<?php echo base_url('inventory/get_po_invoice/'.$rdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>
                      <?php 

                      if (($rdlist->is_paid == 1) && ($rdlist->product_qty != countScannedKit($rdlist->id))) { ?>
                      <a href="<?php echo base_url('rcenter/scan_kits/'.$rdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Scan Kits</a>

                      <?php }elseif (($rdlist->product_qty == countScannedKit($rdlist->id)) && checkDispatchPO($rdlist->id)) { ?>
                      <a href="<?php echo base_url('rcenter/dispatch_order/'.$rdlist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Dispatch</a>
                      <?php } ?>
              </td>
            </tr>
            <?php } } ?>
          </tbody>
        </table>       
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>