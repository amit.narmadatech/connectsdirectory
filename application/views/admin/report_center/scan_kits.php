<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
  .form-control{ width: 50%; display: inline;}
</style>
<div class="row">
  <div class="col-md-2 col-sm-2 col-xs-2"></div>
  <div class="col-md-8 col-sm-8 col-xs-8">
    <div class="x_panel">
      <div class="x_title">
        <h2>Scan kits</h2>       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="ele2" class="b">
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('rcenter/post_scanned_kit'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="request_id" id="request_id" value="<?php echo isset($edit)?$edit->id:''; ?>">
          <section class="content invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12 invoice-header">
                <h1>
                  # <span><?php echo isset($edit)?$edit->reference_no:''; ?></span>
                  <small class="pull-right">Date: <?php echo date("d/m/Y"); ?></small>
                </h1>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
               
              
              <div class="col-sm-4 invoice-col">
                <h4><b>SHIP To</b></h4>
                <address>
                    <strong><?php echo !empty($edit->w_name)?$edit->w_name:''; ?> [ <?php echo !empty($edit->w_code)?$edit->w_code:''; ?> ]</strong>
                    <strong><?php echo !empty($edit->contact_person)?$edit->contact_person:''; ?></strong>
                      <br><?php echo !empty($edit->w_address)?$edit->w_address:''; ?>
                      <br><?php echo !empty($edit->w_city)?$edit->w_city:''; ?>, <?php echo !empty($edit->w_state)?$edit->w_state:''; ?> <?php echo !empty($edit->w_pincode)?$edit->w_pincode:''; ?>
                      <br>Phone: <?php echo !empty($edit->w_contact1)?$edit->w_contact1:''; ?> <?php echo !empty($edit->w_contact2)?", ".$edit->w_contact2:''; ?>
                    <br>Email: <?php echo !empty($edit->w_email)?$edit->w_email:''; ?>
                  </address>
              </div>

              <div class="col-sm-4 invoice-col">
                
              </div> 
              
              <div class="col-sm-4 invoice-col">
                <b>Ref. Number # <?php echo isset($edit)?$edit->reference_no:''; ?> <input type="hidden" id="reference_no" name="reference_no" value="<?php echo isset($edit)?$edit->reference_no:''; ?>"></b>
                <br>
                <b>Requested Quantity :<?php echo countPORequestedKit($edit->id); ?>
                </b>
                <br>
                <br>
                <b>Request Date:</b> <?php echo date("d/m/Y",strtotime($edit->created_at)); ?>
                <br>                
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table">
                <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th width="5%">Sr.No.</th>
                      <th width="90%">BARCODE</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="kitTbl">
                    <?php 
                      if (!empty($scanned_list)) {
                        $i=1;
                        foreach ($scanned_list as $sclist) { ?>
                    
                    <tr id="rowv<?php echo $sclist->id; ?>">
                      <td><?php echo $i++; ?></td>
                      <td><input readonly type="text" id="barcode_v<?php echo $sclist->id; ?>" name="barcode[]" class="form-control" value="<?php echo $sclist->barcode; ?>"></td>
                      <td><button type="button" class="btn btn-danger btn-xs btnRemove" id="v<?php echo $sclist->id; ?>">X</button></td>
                    </tr>
                          
                      <?php  }
                      }
                    ?>
                  </tbody>
                </table>
              </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Comments or Special Instructions:</p>
               
                <textarea id="description" name="description" class="text-muted no-shadow form-control" style="margin-top: 10px;height: 115px;" rows="10"></textarea>
              </div>
              <!-- /.col -->
              <div class="col-xs-6">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      
                      <tr>
                        <th>Total:</th>
                        <td class="text-right"><span id="total_rows"></span><input type="hidden" name="total_qty" id="total_qty"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-6 col-md-offset-5">
                <button type="submit" class="btn btn-primary"><i class="fa fa-right"></i> Submit</button>
                <a href="<?php  echo base_url('rcenter/po_list'); ?>" class="btn btn-default"><i class="fa fa-right"></i> Cancel</a>
              </div>
            </div>
          </section>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?= base_url("assets\js\print\jQuery.print.js"); ?>"></script>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>

<script>
    jQuery(document).ready(function($) {
      var i=1;
       $(document).on('click','#addRow', function(event) {
          var txt = '<tr id="row'+i+'"><td>'+i+'</td><td><input type="text" name="barcode[]" class="form-control"></td><td><button type="button" class="btn btn-danger btn-xs btnRemove" id="'+i+'">X</button></td></tr>';
          $('#kitTbl').append(txt);
          var length = $('#kitTbl tr').length;
          $('#total_rows').html(length);
          $('#total_qty').val(length);
          i++;
        });

        var barcodeArray = new Array();
        $(document).scannerDetection({
          timeBeforeScanTest: 200, // wait for the next character for upto 200ms
          startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
          endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
          avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
          onComplete: function(barcode, qty){ 
            var cnt = $('input[name="barcode[]"]').length + 1;
            //var barcode = '635416316';

            if((jQuery.inArray(barcode,barcodeArray) == -1)){
              var reqid = $('#request_id').val();
              var refno = $('#reference_no').val();
            $.post("<?php echo base_url('rcenter/post_scan_barcode'); ?>",{ barcode : barcode,reqid : reqid,refno : refno, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              if(response.status){
                var txt = '<tr id="rowv'+response.insert_id+'"><td>'+cnt+'</td><td><input readonly type="text" id="barcode_v'+response.insert_id+'" name="barcode[]" class="form-control" value="'+barcode+'"></td><td><button type="button" class="btn btn-danger btn-xs btnRemove" id="v'+response.insert_id+'">X</button></td></tr>';
                barcodeArray.push(barcode);
                $('#kitTbl').append(txt);
                var length = $('#kitTbl tr').length;
                $('#total_rows').html(length);
                $('#total_qty').val(length);
                console.log(response);

              }else{
                alert(response.msg);
              }
            },"json");
            }else{
              alert("Barcode: "+barcode+" already used !");
            }

           } 
        });

        $(document).on('click','.btnRemove', function(event) {
           var id = this.id;
           $.post("<?php echo base_url('rcenter/remove_barcode'); ?>", {id: id.split("v")[1], '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(response) 
           {
              var response = $.parseJSON(response);
              if(response.status){
                  barcode = $('#barcode_'+id).val();
                  barcodeArray.splice( $.inArray(barcode, barcodeArray), 1 );
                  $('#row'+id).remove();
                  var length = $('#kitTbl tr').length;
                  $('#total_rows').html(length);
                  $('#total_qty').val(length);              
              }
           });
        });

        $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {

              var length = $('input[name="barcode[]"]').length;
              if (length < 1) { alert("Please add barcodes !"); }else{
                form.submit();
              }
            },
            ignore: [],
            rules: {
              //  total_qty: "required",
               
            },
            messages:{
                total_qty: "Kits are required.",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

      });
        
    });
</script>