<?php $this->load->view('admin/include/header'); ?>
  
  <div class="row">
  	 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  	 	<h2>Summary</h2>
  	 </div>
  </div>
  <!-- top tiles -->
 <div class="row">
    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-green">
        <div class="icon">
        	<i class="fa fa-medkit"></i>
        </div>
        <div class="count"><?php echo empty($total_reports)?0:$total_reports; ?></div>
		    <h3>Total Reports</h3>
      </div>
    </div>
    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-red">
        <div class="icon">
        	<i class="fa fa-medkit"></i>
        </div>
        <div class="count"><?php echo empty($total_pending_reports)?0:$total_pending_reports; ?></div>
        <h3>Pending Reports</h3>
      </div>
    </div>
	 <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="box bg-blue">
        <div class="icon">
        	<i class="fa fa-medkit"></i>
        </div>
        <div class="count"><?php echo empty($total_received_reports)?0:$total_received_reports; ?></div>
        <h3>Total Report Received</h3>
      </div>
    </div>
    
   
  </div>
  <!-- /top tiles -->
  
<?php $this->load->view('admin/include/footer'); ?>   