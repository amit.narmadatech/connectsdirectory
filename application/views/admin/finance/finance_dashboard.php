<?php $this->load->view('admin/include/header'); ?>

 <div class="row">
  	 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  	 	<h2>Summary</h2>
  	 </div>
  </div>
  <!-- top tiles -->
 <div class="row">
    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-green">
        <div class="icon">
        	<i class="fa fa-check"></i>
        </div>
        <div class="count">2,50,00,000</div>
		    <h3>Total Income</h3>
      </div>
    </div>

    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-red">
        <div class="icon"> 
        	<i class="fa fa-clock-o"></i>
        </div>
        <div class="count">5,00,000</div>
        <h3>Total Expences</h3>
      </div>
    </div>

   <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
     <div class="box bg-blue">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count"> 25,00,000</div>
        <h3> Net Profit  </h3>
      </div>
    </div>
  </div>
  <!-- /top tiles -->

  <!-- top tiles -->
 <div class="row">
    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
       <div class="box bg-orange">
        <div class="icon"> 
        	<i class="fa fa-university"></i>
        </div>
        <div class="count">20,00,000</div>
		   <h3>  Accounts Recivable</h3>
      </div>
    </div>

    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-purple">
        <div class="icon"> 
        	<i class="fa fa-money"></i>
        </div>
        <div class="count">2,00,00,000</div>
        <h3>Accounts Payble</h3>
      </div>
    </div>

	 <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-blue-sky">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count">25,000</div>
        <h3> Cash At End  Of Month  </h3>
      </div>
    </div>       
  </div>
  <!-- /top tiles -->
<?php $this->load->view('admin/include/footer'); ?>