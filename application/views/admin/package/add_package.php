<?php $this->load->view('admin/include/header'); ?>
<script src="<?php echo base_url('assets/admin_theme/vendors/jquery/dist/jquery.min.js'); ?>"></script>

<div class="row">
  <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('package/post_add_package'); ?>" >
  <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" /> 
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
  <div class="col-md-6">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Add Package</h2>
         
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br>

            <div class="form-group">
              <label for="title" class="col-md-4 col-sm-5 col-xs-12">Title <span class="required">*</span>
              </label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <input type="text" id="title" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter package title" value="<?= (isset($edit) && (!empty(($edit->title))))?$edit->title:set_value('title'); ?>" >
              </div>
              <span class="text-danger"><?php echo form_error('title'); ?></span>
            </div>

            <div class="form-group">
              <label for="price" class="col-md-4 col-sm-5 col-xs-12">Price <span class="required">*</span>
              </label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <input type="number" id="price" name="price" class="form-control col-md-7 col-xs-12" placeholder="Enter package price" value="<?= (isset($edit) && (!empty(($edit->price))))?$edit->price:set_value('price'); ?>" >
              <span id="priceError" class="text-danger"><?php echo form_error('price'); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="description" class="col-md-4 col-sm-5 col-xs-12">Description 
              </label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <textarea id="description" name="description" class="form-control col-md-7 col-xs-12" placeholder="Enter package description"><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
              </div>
              <span class="text-danger"><?php echo form_error('description'); ?></span>
            </div>

            <!-- <div class="form-group">
              <label class="col-md-4 col-sm-5 col-xs-12">Status</label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <div id="gender" class="btn-group" data-toggle="buttons">
                  <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                    <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                  </label>
                  <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'default':'primary'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                    <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                  </label>
                </div>
              </div>
            </div> -->
          
        </div>

      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Package </h2>
         
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br>
          <table class="table table-bordered">
            <tr>
              <th style="width: 50%;">Base Price</th>
              <td></td>
              <td><span class="pull-right" id="basePriceText">0</span></td>
            </tr>
            <tr>
              <th style="width: 50%;">Amount</th>
              <td></td>
              <td><span class="pull-right" id="gstPriceText">0</span>
                <input type="hidden" name="gst_price" id="gst_price" value="0">
              </td>
            </tr>
            <tr>
              <th style="width: 50%;">Child Company Share</th>
              <td  style="width: 15%"><input type="number" name="child_share" id="child_share" class="form-control" ></td>
              <td><span class="pull-right" id="childCompanyShareText">0</span>
                <input type="hidden" name="child_share_amt" id="child_share_amt" value="0">
              </td>
            </tr>
            <tr>
              <th style="width: 50%;">Parent Company Share</th>
              <td  style="width: 15%"><input type="number" name="parent_share" id="parent_share" class="form-control" ></td>
              <td><span class="pull-right" id="parentCompanyShareText">0</span>
                <input type="hidden" name="parent_share_amt" id="parent_share_amt" value="0">
              </td>
            </tr>
            <tr>
              <th style="width: 50%;">Package Price (MRP)</th>
              <td></td>
              <td><span class="pull-right" id="packagePriceText">0</span>
                <input type="hidden" name="package_price" id="package_price" value="0">
              </td>
            </tr>
            
          </table>

          <div class="ln_solid"></div>

            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo base_url('admindashboard/department_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
              </div>
            </div>
        </div>

      </div>
    </div>

  </div>
  
  
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Item</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

          <div class="form-group">
            <label for="title" class="col-md-3 col-sm-5 col-xs-12">Add Item <span class="required">*</span>
            </label>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <select id="product" name="product" class="form-control col-md-7 col-xs-12" >
                <option value="">Select Item</option>
                <?php 
                if (!empty($product_list)) {
                  foreach ($product_list as $plist) { ?>
                <option value="<?php echo $plist->item_id; ?>"><?php echo $plist->item_title; ?></option>    
                <?php  }
                }
                ?>
              </select>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn btn-info productBtn">ADD</button>
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>
          <div class="ln_solid"></div>
          <div class="row">
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>Item</th>
                <th>QTY</th>
                <th>Amount</th>
                <th>Action</th>
              </tr>
              <tbody id="productDiv">
                
              </tbody>
            </table>
          </div>
          
      </div>

    </div>

    
          <?php 
          if (!empty($expense_list)) {
            $i = 1;
              foreach ($expense_list as $key => $values) {?>
    <div class="x_panel">
      <div class="x_title">
        <h2>Add <?php echo get_expense_category($key)->category; ?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <input type="hidden" name="category_id[]" id="category_<?php echo $i; ?>" value="<?php echo $key; ?>">   
      <div class="x_content">
        <br>
          <div class="form-group">
            <label for="title" class="col-md-3 col-sm-5 col-xs-12">Add <?php echo get_expense_category($key)->category; ?> <span class="required">*</span>
            </label>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <select id="expense<?php echo $i; ?>" name="expense<?php echo $i; ?>" class="form-control col-md-7 col-xs-12" >
                <option value="">Select <?php echo get_expense_category($key)->category; ?></option>
                <?php 
                if (!empty($values)) {
                  foreach ($values as $expense) { ?>
                <option value="<?php echo $expense->expense_item_id; ?>"><?php echo $expense->title; ?></option>    
                <?php  }
                }
                ?>
              </select>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn btn-info expenseBtn<?php echo $i; ?>">ADD</button>
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>
          <div class="ln_solid"></div>

          <div class="row">
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>Expense</th>
                <th>Percentage</th>
                <th>Amount</th>
                <th></th>
                <th>Action</th>
              </tr>
              <tbody id="expenseDiv<?php echo $i; ?>">
                
              </tbody>
            </table>
          </div>    
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
          var expenseArray<?php echo $i; ?> = new Array();

          $(document).on('click', '.expenseBtn<?php echo $i; ?>', function(event) {
            var id = $('#expense<?php echo $i; ?>').val();
            var category = $('#category_<?php echo $i; ?>').val();
            
            $('.expenseBtn<?php echo $i; ?>').attr('disabled', true);
            if((jQuery.inArray(id,expenseArray<?php echo $i; ?>) == -1)){
              $.post('<?php echo base_url("package/get_expense"); ?>', {expense_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
                $('.expenseBtn<?php echo $i; ?>').attr('disabled', false);
                data = JSON.parse(data);
                console.log(data);
                var txt1 = '<tr id="rowExp<?php echo $i; ?>_'+data.expense.expense_item_id+'"><input type="hidden" name="expenseid[]" value="'+data.expense.expense_item_id+'"><td></td><td>'+data.expense.title+'</td><td><input type="number" min="0" id="expenseprnct_'+data.expense.expense_item_id+'" name="expense_prnct[]"  value="0" class="form-control expense_prnct"></td><td><input type="number" min="0" id="expamt_'+data.expense.expense_item_id+'" value="0" class="form-control expense_amount"></td><td><span id="expenseAmtTxt_'+data.expense.expense_item_id+'">0</span><input type="hidden" name="expenseamount[]" value="0" id="expenseamount_'+data.expense.expense_item_id+'"></td><td><button type="button" id="'+data.expense.expense_item_id+'" class="removeExpense btn btn-danger ntn-sm">X</button><input type="hidden" name="category[]" value="'+category+'"></td></tr>';
                $('#expenseDiv<?php echo $i; ?>').append(txt1);
                expenseArray<?php echo $i; ?>.push(data.expense.expense_item_id);
                console.log(expenseArray<?php echo $i; ?>);
                
              });
            }else{
              $('.expenseBtn<?php echo $i; ?>').attr('disabled', false);
            }
          });

          $(document).on('click', '.removeExpense', function(event) {
            var id = this.id;
            expenseArray<?php echo $i; ?>.splice( $.inArray(id, expenseArray<?php echo $i; ?>), 1 );
            $('#rowExp<?php echo $i; ?>_'+id).remove();
          });

      });
    </script>
          <?php $i++;  }
          }
          ?>
          
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Incentive</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

          <div class="form-group">
            <label for="title" class="col-md-3 col-sm-5 col-xs-12">Add Incentive <span class="required">*</span>
            </label>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <select id="incentive" name="incentive" class="form-control col-md-7 col-xs-12" >
                <option value="">Select Incentive</option>
                <?php 
                if (!empty($incentive_list)) {
                  foreach ($incentive_list as $inctlist) { ?>
                <option value="<?php echo $inctlist->designation_id; ?>"><?php echo $inctlist->designation_title; ?></option>    
                <?php  }
                }
                ?>
              </select>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn btn-info incentiveBtn">ADD</button>
            </div>
            <span class="text-danger"><?php echo form_error('incentive'); ?></span>
          </div>
          <div class="ln_solid"></div>
          <div class="row">
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>Incentive</th>
                <th>Percentage</th>
                <th>Amount</th>
                <th></th>
                <th>Action</th>
              </tr>
              <tbody id="incentiveDiv">
                
              </tbody>
            </table>
          </div>
          
      </div>

    </div>

    <div class="x_panel">
      <div class="x_title">
        <h2>Add Commission</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

          <div class="form-group">
            <label for="title" class="col-md-3 col-sm-5 col-xs-12">Add Commission <span class="required">*</span>
            </label>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <select id="commission" name="commission" class="form-control col-md-7 col-xs-12" >
                <option value="">Select Commission</option>
                <?php 
                if (!empty($commission_list)) {
                  $j=1;
                  foreach ($commission_list as $comm) { ?>
                <option value="<?php echo $j++; ?>"><?php echo $comm; ?></option>    
                <?php  }
                }
                ?>
              </select>
            </div>
            <div class="col-md-2">
              <button type="button" class="btn btn-info commissionBtn">ADD</button>
            </div>
            <span class="text-danger"><?php echo form_error('commission'); ?></span>
          </div>
          <div class="ln_solid"></div>
          <div class="row">
            <table class="table table-bordered">
              <tr>
                <th>#</th>
                <th>Commission</th>
                <th>Percentage</th>
                <th>Amount</th>
                <th></th>
                <th>Action</th>
              </tr>
              <tbody id="commissionDiv">
                
              </tbody>
            </table>
          </div>
          
      </div>

    </div>      


  </div>
  </form>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<script>
    $(document).ready(function($) {
      
    var itemArray = new Array();

      $(document).on('click', '.productBtn', function(event) {
        var id = $('#product').val();
        $('.productBtn').attr('disabled', true);
        if((jQuery.inArray(id,itemArray) == -1)){
          $.post('<?php echo base_url("package/get_product"); ?>', {product_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
            $('.productBtn').attr('disabled', false);
            data = JSON.parse(data);
            console.log(data);
            var txt = '<tr id="row_'+data.product.item_id+'"><input type="hidden" name="productid[]" value="'+data.product.item_id+'"><td></td><td>'+data.product.item_title+'</td><td><input type="number" min="1" id="'+data.product.item_id+'" name="product_qty[]"  value="1" class="form-control product_qty"></td><td><span id="prdAmtTxt_'+data.product.item_id+'">'+data.product.item_price+'</span><input type="hidden" name="productamount[]" value="'+data.product.item_price+'" id="productamount_'+data.product.item_id+'"><input type="hidden" name="productactamt[]" value="'+data.product.item_price+'" id="productactamt_'+data.product.item_id+'"></td><td><button type="button" id="'+data.product.item_id+'" class="removeProduct btn btn-danger ntn-sm">X</button></td></tr>';
            $('#productDiv').append(txt);
            itemArray.push(data.product.item_id);
            console.log(itemArray);
          });
        }else{
          $('.productBtn').attr('disabled', false);
        }
      });

      $(document).on('click', '.removeProduct', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
      });

      $(document).on('keyup keypress blur change', '.product_qty', function(event) {
        var id = this.id;
        var qty = this.value;
        var amt = $('#productactamt_'+id).val();
        var result = qty * amt;
        console.log(amt,qty,result);
        $('#prdAmtTxt_'+id).html(result);
        $('#productamount_'+id).val(result);

      });

      /*var expenseArray1 = new Array();

      $(document).on('click', '.expenseBtn1', function(event) {
        var id = $('#expense1').val();
        var category = $('#category_1').val();
        
        $('.expenseBtn1').attr('disabled', true);
        if((jQuery.inArray(id,expenseArray1) == -1)){
          $.post('<?php echo base_url("package/get_expense"); ?>', {expense_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
            $('.expenseBtn1').attr('disabled', false);
            data = JSON.parse(data);
            console.log(data);
            var txt1 = '<tr id="row1_'+data.expense.expense_item_id+'"><input type="hidden" name="expenseid[]" value="'+data.expense.expense_item_id+'"><td></td><td>'+data.expense.title+'</td><td><input type="number" min="0" id="'+data.expense.expense_item_id+'" name="expense_prnct[]"  value="0" class="form-control expense_prnct"></td><td><span id="expenseAmtTxt_'+data.expense.expense_item_id+'">0</span><input type="hidden" name="expenseamount[]" value="0" id="expenseamount_'+data.expense.expense_item_id+'"></td><td><button type="button" id="'+data.expense.expense_item_id+'" class="removeExpense btn btn-danger ntn-sm">X</button><input type="hidden" name="category[]" value="'+category+'"></td></tr>';
            $('#expenseDiv1').append(txt1);
            expenseArray1.push(data.expense.expense_item_id);
            console.log(expenseArray1);
            
          });
        }else{
          $('.expenseBtn1').attr('disabled', false);
        }
      });*/

      /*$(document).on('click', '.removeExpense', function(event) {
        var id = this.id;
        expenseArray1.splice( $.inArray(id, expenseArray1), 1 );
        $('#row1_'+id).remove();
      });*/

      $(document).on('keyup keypress blur change', '.expense_prnct', function(event) {
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var id = this.id.split("_");
        id = id[1];
        var qty = this.value;
        var result = (qty * amt)/100;
        console.log(amt,qty,result);
        $('#expenseAmtTxt_'+id).html(result);
        $('#expenseamount_'+id).val(result);
        $('#expamt_'+id).val(result);
        getPackagePrice();
      });

      $(document).on('keyup keypress blur change', '.expense_amount', function(event) {
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var id = this.id.split("_");
        id = id[1];
        var amount = this.value;
        var result = (amount * 100)/amt;
        console.log(amt,amount,result);
        $('#expenseprnct_'+id).val(result);
        $('#expenseAmtTxt_'+id).html(amount);
        $('#expenseamount_'+id).val(amount);
        getPackagePrice();
      });


      /*var expenseArray2 = new Array();

      $(document).on('click', '.expenseBtn2', function(event) {
        var id = $('#expense2').val();
        var category = $('#category_2').val();
        
        $('.expenseBtn2').attr('disabled', true);
        if((jQuery.inArray(id,expenseArray2) == -1)){
          $.post('<?php echo base_url("package/get_expense"); ?>', {expense_id: id}, function(data) {
            $('.expenseBtn2').attr('disabled', false);
            data = JSON.parse(data);
            console.log(data);
            var txt2 = '<tr id="row2_'+data.expense.expense_item_id+'"><input type="hidden" name="expenseid[]" value="'+data.expense.expense_item_id+'"><td></td><td>'+data.expense.title+'</td><td><input type="number" min="0" id="'+data.expense.expense_item_id+'" name="expense_prnct[]"  value="0" class="form-control expense_prnct"></td><td><span id="expenseAmtTxt_'+data.expense.expense_item_id+'">0</span><input type="hidden" name="expenseamount[]" value="0" id="expenseamount_'+data.expense.expense_item_id+'"></td><td><button type="button" id="'+data.expense.expense_item_id+'" class="removeExpense btn btn-danger ntn-sm">X</button><input type="hidden" name="category[]" value="'+category+'"></td></tr>';
            $('#expenseDiv2').append(txt2);
            expenseArray2.push(data.expense.expense_item_id);
            console.log(expenseArray2);
            
          });
        }else{
          $('.expenseBtn2').attr('disabled', false);
        }
      });

      $(document).on('click', '.removeExpense', function(event) {
        var id = this.id;
        expenseArray2.splice( $.inArray(id, expenseArray2), 1 );
        $('#row2_'+id).remove();
      });

      $(document).on('keyup keypress blur change', '.expense_prnct', function(event) {
        var id = this.id;
        var qty = this.value;
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var result = (qty * amt)/100;
        console.log(amt,qty,result);
        $('#expenseAmtTxt_'+id).html(result);
        $('#expenseamount_'+id).val(result);

      });*/


      var incentiveArray = new Array();

      $(document).on('click', '.incentiveBtn', function(event) {
        var id = $('#incentive').val();
        
        $('.incentiveBtn').attr('disabled', true);
        if((jQuery.inArray(id,incentiveArray) == -1)){
          $.post('<?php echo base_url("package/get_incentive"); ?>', {incentive_id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
            $('.incentiveBtn').attr('disabled', false);
            data = JSON.parse(data);
            console.log(data);
            var txt2 = '<tr id="rowInc_'+data.incentive.designation_id+'"><input type="hidden" name="incentiveid[]" value="'+data.incentive.designation_id+'"><td></td><td>'+data.incentive.designation_title+'</td><td><input type="number" min="0" id="incentiveprnct_'+data.incentive.designation_id+'" name="incentive_prnct[]"  value="0" class="form-control incentive_prnct"></td><td><input type="number" min="0" id="incamt_'+data.incentive.designation_id+'" value="0" class="form-control incentive_amount"></td><td><span id="incentiveAmtTxt_'+data.incentive.designation_id+'">0</span><input type="hidden" name="incentiveamount[]" value="0" id="incentiveamount_'+data.incentive.designation_id+'"></td><td><button type="button" id="'+data.incentive.designation_id+'" class="removeIncentive btn btn-danger ntn-sm">X</button></td></tr>';
            $('#incentiveDiv').append(txt2);
            incentiveArray.push(data.incentive.designation_id);
            console.log(incentiveArray);
            
          });
        }else{
          $('.incentiveBtn').attr('disabled', false);
        }
      });

      $(document).on('click', '.removeIncentive', function(event) {
        var id = this.id;
        incentiveArray.splice( $.inArray(id, incentiveArray), 1 );
        $('#rowInc_'+id).remove();
      });

      $(document).on('keyup keypress blur change', '.incentive_prnct', function(event) {
        var id = this.id.split("_");
        id = id[1];
        var qty = this.value;
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var result = (qty * amt)/100;
        console.log(amt,qty,result);
        $('#incentiveAmtTxt_'+id).html(result);
        $('#incentiveamount_'+id).val(result);
        $('#incamt_'+id).val(result);
        getPackagePrice();
      });

      $(document).on('keyup keypress blur change', '.incentive_amount', function(event) {
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var id = this.id.split("_");
        id = id[1];
        var amount = this.value;
        var result = (amount * 100)/amt;
        console.log(amt,amount,result);
        $('#incentiveprnct_'+id).val(result);
        $('#incentiveAmtTxt_'+id).html(amount);
        $('#incentiveamount_'+id).val(amount);
        getPackagePrice();
      });

      var commissionArray = new Array();

      $(document).on('click', '.commissionBtn', function(event) {
        var id = $('#commission').val();
        var val = $('#commission option:selected').text();
        
        $('.commissionBtn').attr('disabled', true);
        if((jQuery.inArray(id,commissionArray) == -1)){
            $('.commissionBtn').attr('disabled', false);

            var txt2 = '<tr id="rowComm_'+id+'"><input type="hidden" name="commissionid[]" value="'+id+'"><td></td><td>'+val+'</td><td><input type="number" min="0" id="comprnct_'+id+'" name="commission_prnct[]"  value="0" class="form-control commission_prnct"></td><td><input type="number" min="0" id="comamt_'+id+'" value="0" class="form-control commission_amount"></td><td><span id="commissionAmtTxt_'+id+'">0</span><input type="hidden" name="commissionamount[]" value="0" id="commissionamount_'+id+'"></td><td><button type="button" id="'+id+'" class="removeCommission btn btn-danger ntn-sm">X</button></td></tr>';
            $('#commissionDiv').append(txt2);
            commissionArray.push(id);
            console.log(commissionArray);
            
         
        }else{
          $('.commissionBtn').attr('disabled', false);
        }
      });

      $(document).on('click', '.removeCommission', function(event) {
        var id = this.id;
        commissionArray.splice( $.inArray(id, commissionArray), 1 );
        $('#rowComm_'+id).remove();
      });

      
      $(document).on('keyup keypress blur change', '.commission_prnct', function(event) {
        var id = this.id.split("_");
        id = id[1];
        var qty = this.value;
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var result = (qty * amt)/100;
        console.log(amt,qty,result);
        $('#commissionAmtTxt_'+id).html(result);
        $('#commissionamount_'+id).val(result);
        $('#comamt_'+id).val(result);

      });

      $(document).on('keyup keypress blur change', '.commission_amount', function(event) {
        var amt = (isNaN($('#price').val()) || ($('#price').val() == ""))?0:$('#price').val();
        if (amt == 0) {
          $('#priceError').html('The price field is required');
          $('#price').val('').focus();
        }
        var id = this.id.split("_");
        id = id[1];
        var amount = this.value;
        var result = (amount * 100)/amt;
        console.log(amt,amount,result);
        $('#comprnct_'+id).val(result);
        $('#commissionAmtTxt_'+id).html(amount);
        $('#commissionamount_'+id).val(amount);
        getPackagePrice();
      });

      // START- On change of base price

      $(document).on('keyup keypress blur change', '#price', function(event) {
        var val = this.value;
        var amt = (isNaN(val) || (val == ""))?0:val;

        $('#basePriceText').html(amt);

        getPackagePrice();
      });

      // START - calculation of sgst 

      $(document).on('keyup keypress blur change', '#sgst_prnct', function(event) {
        var val = this.value;
        var base_price = $('#price').val();
        var prnct = (isNaN(val) || (val == ""))?0:val;
        var amt = (isNaN(base_price) || (base_price == ""))?0:base_price;

        result = (prnct * amt)/100;

        $('#sgstText').html(result);
        $('#sgst_tax_amount').val(result);
        
      });

      // START - calculation of cgst 

      $(document).on('keyup keypress blur change', '#cgst_prnct', function(event) {
        var val = this.value;
        var base_price = $('#price').val();
        var prnct = (isNaN(val) || (val == ""))?0:val;
        var amt = (isNaN(base_price) || (base_price == ""))?0:base_price;

        result = (prnct * amt)/100;

        $('#cgstText').html(result);
        $('#cgst_tax_amount').val(result);
        
      });

      // START - calculation of igst 

      $(document).on('keyup keypress blur change', '#igst_prnct', function(event) {
        var val = this.value;
        var base_price = $('#price').val();
        var prnct = (isNaN(val) || (val == ""))?0:val;
        var amt = (isNaN(base_price) || (base_price == ""))?0:base_price;

        result = (prnct * amt)/100;

        $('#igstText').html(result);
        $('#igst_tax_amount').val(result);
        
      });

      // START - calculation of child company share 

      $(document).on('keyup keypress blur change', '#child_share', function(event) {
        var val = this.value;
        var gst_price = $('#gst_price').val();
        var prnct = (isNaN(val) || (val == ""))?0:val;
        var amt = (isNaN(gst_price) || (gst_price == ""))?0:gst_price;

        result = (prnct * amt)/100;

        $('#childCompanyShareText').html(result);
        $('#child_share_amt').val(result);
        
      });

      // START - calculation of parent company share 

      $(document).on('keyup keypress blur change', '#parent_share', function(event) {
        var val = this.value;
        var gst_price = $('#gst_price').val();
        var prnct = (isNaN(val) || (val == ""))?0:val;
        var amt = (isNaN(gst_price) || (gst_price == ""))?0:gst_price;

        result = (prnct * amt)/100;

        $('#parentCompanyShareText').html(result);
        $('#parent_share_amt').val(result);
        
      });

      // START - Calculate Package Price 


      function getPackagePrice(){
        var base_amt = $('#price').val();
        var base_amt = (isNaN(base_amt) || (base_amt == ""))?0:base_amt;

        if((base_amt != 0) && (base_amt > 0))
        {
          var product_amt = parseFloat(getAllProductPrice());
          var expense_amt = parseFloat(getAllExpensePrice());
          var incentive_amt = parseFloat(getAllIncentivePrice());
          var commission_amt = parseFloat(getAllCommissionPrice());

          var subtract_amt = product_amt + expense_amt + incentive_amt + commission_amt;
          result = base_amt - subtract_amt;
          console.log('-------');
          console.log(result);

          $('#gstPriceText').html(result);
          $('#gst_price').val(result);

          $('#packagePriceText').html(result);
          $('#package_price').val(result);
        }

      }

      // START - calculation of product/inventory amount 

      function getAllProductPrice(){
          var product_amt = 0;

          $("input[name='productamount[]']").each(function(i, obj) {
              product_amt += parseFloat(obj.value);
          });

          return product_amt;
      }

      // START - On change of product/inventory amount 

      $(document).on('keyup keypress blur change', "input[name='product_qty[]']", function(event) {
        getPackagePrice();
      });


      // START - calculation of Indirect Expense and Overheads amount 

      function getAllExpensePrice(){
          var expense_amt = 0;

          $("input[name='expenseamount[]']").each(function(i, obj) {
              expense_amt += parseFloat(obj.value);
          });

          return expense_amt;
      }

      // START - On change of Indirect Expense and Overheads amount 

      $(document).on('keyup keypress blur change', "input[name='expense_prnct[]']", function(event) {
        getPackagePrice();
      });

      // START - On change of Indirect Expense and Overheads amount 

      $(document).on('keyup keypress blur change', "input[name='expense_prnct[]']", function(event) {
        getPackagePrice();
      });

      // START - calculation of incentive 

      function getAllIncentivePrice(){
          var incentive_amt = 0;

          $("input[name='incentiveamount[]']").each(function(i, obj) {
              incentive_amt += parseFloat(obj.value);
          });
          return incentive_amt;
      }

      // START - On change of Indirect Expense and Overheads amount 

      $(document).on('keyup keypress blur change', "input[name='incentive_prnct[]']", function(event) {
        getPackagePrice();
      });

      // START - calculation of commission 

      function getAllCommissionPrice(){
          var commission_amt = 0;

          $("input[name='commissionamount[]']").each(function(i, obj) {
              commission_amt += parseFloat(obj.value);
          });
          return commission_amt;
      }

      // START - On change of Indirect Expense and Overheads amount 

      $(document).on('keyup keypress blur change', "input[name='commission_prnct[]']", function(event) {
        getPackagePrice();
      });

      // START - On click add product button 

      $(document).on('click', ".productBtn", function(event) {
        getPackagePrice();
      });
      
  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
                //if ($("input[name='permission[]']").is(":checked")) {  }else{ alert('Please select atleast one permission');return false; }
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                price: "required",
                child_share: "required",
                parent_share: "required",

            },
            messages:{
                title: "The package title is required",
                price: "The price field is required",
                child_share: "required",
                parent_share: "required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });
});
</script>