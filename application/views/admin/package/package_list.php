<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Package List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">          
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Base Price</th>
                <th>Date</th>
                <th>Tax</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
               if(!empty($package_list)){
                  $i = 1;
                foreach($package_list as $pcklist){ ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php  echo !empty($pcklist->title) ? $pcklist->title : '' ?></td>
                  <td><?php  echo !empty($pcklist->base_price) ? $pcklist->base_price : '' ?></td>
                  <td><?php  echo !empty($pcklist->created_at) ? date('D d M Y h:i a',strtotime($pcklist->created_at)) : '' ?></td>
                  <td><?php  echo !empty($pcklist->sgst_tax) ? $pcklist->sgst_tax+$pcklist->cgst_tax : '-' ?></td>
                  <td>
                    <?php  if($pcklist->package_status == 1){echo "Active"; }elseif($pcklist->package_status == 2){ echo "Disabled"; }else{ echo "Pending"; } ?>
                  </td>
                  <td>
                    <?php if (empty($pcklist->sgst_tax)): ?>  
                    <a class="btn btn-sm btn-success" href="<?php echo base_url("setting/tax"); ?>">Add Tax</a>
                    <?php endif ?>
                    <a class="btn btn-sm btn-primary" href="<?php echo base_url("package/view_package/$pcklist->pid"); ?>">View</a>
                    <a title="click to disable" onclick="return confirm('Are you sure ? ');" class="btn btn-sm btn-warning" href="<?php echo base_url("package/disable/$pcklist->pid"); ?>">Disable</a>
                  </td>
                </tr>
              <?php } } ?>
            </tbody>
          </table>
       
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>