<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-sm-3 col-md-3 col-lg-3"></div>
  
  <div class="col-sm-6 col-md-6 col-lg-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Package Detail</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <table class="table table-bordered">
          <tr>
            <th>Title</th>
            <td><?php echo !empty($package->title)?ucfirst($package->title):'-'; ?></td>
            <th>Date</th>
            <td><?php echo !empty($package->created_at)?date('D d M Y h:i a',strtotime($package->created_at)):'-'; ?></td>
          </tr>
          <tr>
            <th>Base Price</th>
            <td><?php echo !empty($package->base_price)?$package->base_price:'-'; ?></td><th>Balance</th>
            <td><?php echo !empty($package->profit_amount)?$package->profit_amount:'-'; ?></td>
          </tr>
          <tr>
            <th>Child Company Share (<?php echo $package->child_share_prnct; ?>%)</th>
            <td><?php echo !empty($package->child_share_amt)?$package->child_share_amt:'-'; ?></td>
            <th>Parent Company Share (<?php echo $package->parent_share_prnct; ?>%)</th>
            <td><?php echo !empty($package->parent_share_amt)?$package->parent_share_amt:'-'; ?></td>
          </tr>

          <tr>
            <th>GST</th>
            <td><?php echo $gst = !empty($package->sgst_tax)?$package->sgst_tax+$package->cgst_tax:0; ?> %</td>
            <th>MRP </th>
            <td><?php $tax_amount = ($gst*$package->base_price)/100; echo $tax_amount + $package->base_price; ?></td>
          </tr>
          <tr>
            <th>Description</th>
            <td colspan="3"><?php echo !empty($package->description)?$package->description:'-'; ?></td>
          </tr>
        </table>
          <div class="ln_solid"></div>
        <table class="table table-bordered">
          <tr>
            <th>Sr.No.</th>
            <th>Particulars</th>
            <th>Percentage</th>
            <th>Amount</th>
          </tr>
        <tr>
            <th colspan="5"><b>Product Expense</b></th>
        </tr>
        <?php 
        if (!empty($package_product)) {
          $i=1;
          $tamount = 0;
          foreach ($package_product as $prd) { ?>
          
           <tr>
             <td><?php echo $i++; ?></td>
             <td><?php echo !empty($prd->product_id)?getPckProductById($prd->product_id)->item_title:'-'; ?>
               | QTY [<?php echo !empty($prd->quantity)?$prd->quantity:'-'; ?>]
             </td>
             <td></td>
             <td><?php echo !empty($prd->amount)?$prd->amount:'-';$tamount +=$prd->amount; ?></td>
           </tr> 
        <?php  }
        }
        ?>

        <?php 
        $cat=[]; 
        if (!empty($package_expense)) {
          //$i=1;
          foreach ($package_expense as $exp) { ?>
          <?php if (!in_array($exp->category_id, $cat)){ array_push($cat, $exp->category_id);  ?>
          <tr>
              <td colspan="3"><b class="pull-right">Amount :</b></td>
              <td>- <?php echo $tamount; ?></td>
            </tr>
          <tr>
            <th colspan="5"><b><?php echo get_expense_category($exp->category_id)->category; ?> Expense</b></th>
          </tr>
          <?php $tamount = 0; } ?>
           <tr>
             <td><?php echo $i++; ?></td>
             <td><?php echo !empty($exp->expense_id)?getExpenseById($exp->expense_id)->title:'-'; ?></td>
             <td><?php echo !empty($exp->percentage)?$exp->percentage:'-'; ?> %</td>
             <td><?php echo !empty($exp->amount)?$exp->amount:'-';$tamount +=$exp->amount; ?></td>
           </tr> 
            
        <?php  }

        }
        ?>
        
        <tr>
              <td colspan="3"><b class="pull-right">Amount :</b></td>
              <td> - <?php echo $tamount; ?></td>
        </tr>
        
        <tr>
            <th colspan="5"><b>Incentive </b></th>
        </tr>
        <?php 
        $tamount = 0;
        if (!empty($package_incentive)) {
          //$i=1;
          foreach ($package_incentive as $inctv) { ?>
           <tr>
             <td><?php echo $i++; ?></td>
             <td><?php echo !empty($inctv->incentive_id)?getIncentiveById($inctv->incentive_id)->designation_title:'-'; ?>
             </td>
             <td><?php echo $inctv->percentage; ?> %</td>
             <td><?php echo $inctv->amount; $tamount +=$inctv->amount; ?></td>
           </tr> 
        <?php  }
        }
        ?>

        <tr>
              <td colspan="3"><b class="pull-right">Amount :</b></td>
              <td>- <?php echo $tamount; ?></td>
        </tr>

        <tr>
            <th colspan="5"><b>Commission </b></th>
        </tr>
        <?php 
          $commission_list = array(
              1 => 'OTC to JC Care - upon sale of Retailer ID',
              2 => 'OTC to Retailer - Sale on Product',
              3 => 'ORC to JC Care - on Sales of Proudct via Retailer',
              4 => 'OTC to JC Care - on Sale of Product from his outlet'
              );
        $tamount = 0;
        if (!empty($package_commission)) {
          //$i=1;
          foreach ($package_commission as $comm) { ?>
           <tr>
             <td><?php echo $i++; ?></td>
             <td><?php echo !empty($comm->commission_id)?$commission_list[$comm->commission_id]:'-'; ?>
             </td>
             <td><?php echo $comm->percentage; ?> %</td>
             <td><?php echo $comm->amount;$tamount +=$comm->amount; ?></td>
           </tr> 
        <?php  }
        }
        ?>
        <tr>
              <td colspan="3"><b class="pull-right">Amount :</b></td>
              <td>- <?php echo $tamount; ?></td>
        </tr>
        </table>
      </div>

    </div>
  </div>

  <div class="col-sm-3 col-md-3 col-lg-3"></div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<script>
    $(document).ready(function($) {
    
  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
              form.submit();
            },
            ignore: [],
            rules: {
                state_id: "required",
            },
            messages:{
                state_id: "The state field is required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });
});
</script>