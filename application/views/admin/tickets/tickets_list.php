<?php $this->load->view('admin/include/header'); ?>
<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats bg-green">
      <div class="icon">
        <i class="fa fa-ticket"></i>
      </div>                  
      <div class="count text-center"><?php echo !empty($totalticket->answerdticktes) ? $totalticket->answerdticktes : 0 ;?>          
        </div>
          <h3>Answered Tickets</h3>
      </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats bg-red">
        <div class="icon">
          <i class="fa fa-ticket"></i>
        </div>
       <div class="count text-center"><?php echo !empty($totalticket->inprogresstickets) ? $totalticket->inprogresstickets : 0 ;?></div>
          <h3>In Progress Tickets</h3>
      </div>
    </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats bg-blue">
          <div class="icon">
            <i class="fa fa-folder-open-o"></i>
          </div>
          <div class="count text-center"><?php echo !empty($totalticket->opentickets) ? $totalticket->opentickets : 0 ;?></div>
            <h3>Open Tickets</h3>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats bg-orange">
            <div class="icon">
              <i class="fa fa-folder"></i>
            </div>
            <div class="count text-center"><?php echo !empty($totalticket->closetickets) ? $totalticket->closetickets : 0 ;?></div>
              <h3>Close Tickets</h3>
            </div>
          </div>
        </div>

 <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <?php if($this->session->flashdata('success')){?>
		<div class='alert alert-success'> <?= $this->session->flashdata('success'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
	<?php } ?>
    <div class="x_panel">
      <div class="x_title">
        <h2>Tickets</h2>  
         <a href="<?php echo base_url('tickets/addtickets') ?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add Ticket </a>     
        <div class="clearfix"></div>
      </div>
      <div class="x_content"> 
    <div class="row">
        <div class="col-sm-6">
            <label>
                <select class="form-control input-sm">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </label>
         
        </div>
       
        <div class="col-sm-6 text-right top-search">
            <label> <span class="search-level">Search all columns: </span>
                <input class="form-control input-sm" placeholder="search"  type="search">
            </label>
            </div>
        </div>
		<table id="datatable-buttons" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Ticket Code</th>
					<th>Subject</th>
					<th>Message</th>
					<th>Department</th>
					<th style="width: 180px">Date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			 <?php  if(!empty($ticket)){ 
			 foreach($ticket as $tickets) { ?>
				 <tr>
					<td><span class="label label-success"><?php echo !empty($tickets->ticket_code)? $tickets->ticket_code : '' ;?></span></td>
					<td><a href="<?php echo base_url('tickets/ticketsdetails/'.$tickets->tickets_id);?>"><?php echo !empty($tickets->subject)? $tickets->subject : '' ;?></a></td>
					<td><?php echo !empty($tickets->body)? $tickets->body : '' ;?></td>
					<td><?php echo !empty($tickets->departments_id)? $tickets->departments_id : '' ;?></td>
					<td><?php echo !empty($tickets->created)? date('d-m-Y h:i A', strtotime($tickets->created)) :''; ?></td>
					<td>
					 <?php if(!empty($tickets->status) && ($tickets->status ==1)){ ?>
								 <span class="label label-success">Close</span>
					 <?php }else if(!empty($tickets->status) && ($tickets->status ==2)){ ?>
					 
							<span class="label label-info">Inprogress</span>
							
					 <?php }else if(!empty($tickets->status) && ($tickets->status ==3)){ ?> 
					 
							<span class="label label-primary">Pending</span>
							
					 <?php }else if(!empty($tickets->status) && ($tickets->status ==4)){ ?>
					 
							<span class="label label-info">Answer</span>
					 <?php }else{ ?>
							<span class="label label-danger">Open</span>
					 <?php } ?>	
					</td>
                            <td> 
                                <a href="<?php echo base_url('tickets/addtickets/'.$tickets->tickets_id);?>" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
                                 <a href="javascript:void(0)"  onclick="deletetickets('<?php echo $tickets->tickets_id;?>')" class="btn btn-danger btn-xs">
                                     <i class="fa fa-trash-o"></i>
                                 </a>
                                 <a href="<?php echo base_url('tickets/ticketsdetails/'.$tickets->tickets_id);?>" class="btn btn-info btn-xs">
                                     <span class="fa fa-eye"></span>
                                 </a>
                                   <div class="btn-group">
                                                <button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">Change Status  <span class="caret"></span></button>
                                                <ul class="dropdown-menu animated zoomIn">
                                                <li><a data-id="0" onclick="answertype(this,'<?php echo $tickets->tickets_id ;?>');" href="javascript:void(0)">Open</a></li>
                                                <li><a data-id="1" onclick="answertype(this ,'<?php echo $tickets->tickets_id ;?>');" href="javascript:void(0)">Closed</a></li> 
                                                <li><a data-id="2" onclick="answertype(this ,'<?php echo $tickets->tickets_id ;?>');" href="javascript:void(0)">In_progress</a></li>
                                                <li><a data-id="4" onclick="answertype(this ,'<?php echo $tickets->tickets_id ;?>');"  href="javascript:void(0)">Answered</a></li>
                                           </ul>
                                  </div>
                            </td>
                </tr>
			 <?php } } ?>
	        </tbody>
        </table>
        </div>
      </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<div class="modal fade bs-example-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
	<form name="ticketcommentfrm" id="ticketcommentfrm" action="<?php echo base_url('tickets/answerticket');?>" method="POST" enctype="multipart/form-data">
    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <h4 class="modal-title" id="myModalLabel">Comments</h4> 
        </div>
          <div class="modal-body">  
		   <input type="hidden" name="ticketstatus" id="ticketstatus" value=""/>
		   <input type="hidden" name="tickets_id" id="tickets_id" value=""/>
            <div class="form-group">
                <textarea class="form-control" name="answer" id="answer" rows="6"></textarea>
            </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
	</form>		
    </div>
  </div>
</div>

<?php $this->load->view('admin/include/footer'); ?>
  <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
  <script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script>
<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  
 <script>
 function answertype(obj,tickets_id){
 
	 var id = $(obj).attr("data-id");
	      $('#myModal').modal('toggle');
		  $('#ticketstatus').val(id);
		  $('#tickets_id').val(tickets_id);
	}
	
	function deletetickets(tickets_id){
        bootbox.confirm({
            title: "Delete Tickets",
            message: "Are you sure you want to delete this Tickets",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("tickets/deletetickets"); ?>'+'/'+tickets_id;
                }
            }
        });
    }
	
	$(document).ready(function(){
		$('#ticketreplyfrm').validate({
			rules: {
				comment: "required"
			 
			 },
			submitHandler: function(form){
			   form.submit();
			},
			messages:{
				   	required: "Please enter your messages",
					
			 },
		});
	
	});
	
	
	
</script>
  

