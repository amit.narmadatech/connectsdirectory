<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>New Tickets</h2>       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">    
       <?php //print_r($tickets);   echo $tickets->tickets_id; ?>
	  
    	<form name="ticketreplyfrm" id="ticketreplyfrm" action="<?php echo base_url('tickets/addtickets');?>" method="POST" enctype="multipart/form-data" class="form-horizontal">

        <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
			<div class="row">
         <?php  if(isset($tickets->tickets_id)) { ?>
         <div class="col-sm-6">  
		
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="ticket_code"> Ticket Code <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
			 <input type="text" id="ticket_code" name="ticket_code" value="<?php echo !empty($tickets->ticket_code)? $tickets->ticket_code : ''; ?>" class="form-control">
               <input type="hidden" id="ticket_id" name="ticket_id" value="<?php echo !empty($tickets->tickets_id)? $tickets->tickets_id : ''; ?>" class="form-control">
            </div>            
          </div>
        </div>
         <?php  } ?> 
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="subject ">Subject  <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="subject" name="subject" value="<?php echo !empty($tickets->subject)? $tickets->subject : ''; ?>" class="form-control">
            </div>    
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="ticket_code"> Select User  <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
                <select class="form-control" name="user_id">
				    <option value="2">select user</option> 
                        <?php if(!empty($userlist)){ 
						       foreach ($userlist AS $userlists){ ?>
									<option value="<?php echo $userlists->id ;?>"><?php  echo $userlists->name ?></option>
						<?php } } ?>
                </select>
            </div>            
          </div>
        </div>
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="subject ">Priority   <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <select class="form-control" name="priority">
                    <option value="">select priority</option>                    
                    <option value="1">Low</option>                    
                    <option value="2">Medium</option>
                    <option value="3">High</option> 
                </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12">
             Department   <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
                <select class="form-control" name="department" id="department">
                    <option value="">select department</option> 
                    <option value="Sales">sales</option> 
                    <option value="Marketing">Marketing</option>                   
                </select>
            </div>            
          </div>
        </div>

         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="subject ">Attachment   </label>
           <div class="col-md-6 col-sm-5 col-xs-12">
              <input type="file" name="filename" id="filename" class="form-control">
          </div>
         <!-- <div class="col-md-2 col-sm-2 col-xs-12">
              <input type="button" class="btn btn-info" value="Add">
          </div>-->
		  <div class="clearfix"></div>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-sm-12">   
          <div class="form-group">
            <label class="col-md-2 col-sm-5 col-xs-12">
             Ticket Message   
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
                <textarea class="form-control" name="message" id="message" ><?php echo !empty($tickets->body)? $tickets->body : ''; ?></textarea> 
            </div>            
          </div>
        </div>
    </div>

   <!-- <div class="row">
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="subject ">Select User   </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <select class="form-control" name="department">
                    <option value="Sales">sales</option> 
                    <option value="Marketing">Marketing</option>                   
                </select>
          </div>
          
        </div>
      </div>
    </div>-->
            <div class="ln_solid"></div>
                    <div class="row">
                         <div class="col-xs-12 text-center">
                          <button type="submit" class="btn btn-success">Create Ticket</button>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  
<script src="<?php echo base_url('assets/tinymce/');?>tinymce.min.js"></script>
<script>
  //image link media fullscreen
  tinymce.init({ 
    selector:'textarea',
    height: 300,
    toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent code fontselect fontsizeselect',
    plugins: 'code image link media fullscreen'
  });
</script>