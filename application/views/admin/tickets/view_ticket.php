<?php $this->load->view('admin/include/header'); ?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">   
            <div class="x_title">
                <h2>Tickets</h2>  
                <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#ticketmodal"> 
                    Ticket Details  
                </a>     
                <div class="clearfix"></div>
            </div>   
            <div class="x_content"> 
                <div class="row">
                    <div class="col-sm-4"> 
                        <div class="ticket-box">
                            <h4>All Ticket</h4>
                            <ul>
                                <?php if (!empty($tickets)) { ?>
                                    <?php foreach ($tickets as $ticketDataVal) { ?>
                                        <li class="<?php if (!empty($tickets_id) && ($tickets_id == $ticketDataVal->tickets_id)) { ?> active <?php } ?>"> 
                                            <a href="<?php echo base_url('tickets/ticketsdetails/' . $ticketDataVal->tickets_id); ?>">
                                                <?php echo!empty($ticketDataVal->ticket_code) ? $ticketDataVal->ticket_code : ''; ?>
                                                <div class="pull-right">
                                                    <?php if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 1)) { ?>
                                                        <span class="label label-success">Close</span>
                                                    <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 2)) { ?>

                                                        <span class="label label-info">Inprogress</span>

                                                    <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 3)) { ?> 

                                                        <span class="label label-primary">Pending</span>

                                                    <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 4)) { ?>

                                                        <span class="label label-info">Answer</span>
                                                    <?php } else { ?>
                                                        <span class="label label-danger">Open</span>
                                                    <?php } ?>

                                                </div>
                                                <br>
                                                Client | <?php echo!empty($ticketDataVal->created) ? date('d-m-Y h:i A', strtotime($ticketDataVal->created)) : ''; ?>
                                            </a>
                                        </li>
                                    <?php }
                                } ?>
                            </ul>

                        </div>      
                    </div>       
                    <div class="col-sm-8">
                        <h4 class="ticket-heading">[<?php echo!empty($ticketData->ticket_code) ? $ticketData->ticket_code : ''; ?>] <?php echo!empty($ticketData->subject) ? $ticketData->subject : ''; ?> 

                            <div class="pull-right">
                                <a href="<?php echo base_url('tickets/addtickets/'.$ticketData->tickets_id);?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a href="javascript:void(0)" onclick="deletetickets('<?php echo $ticketData->tickets_id;?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>

                                <div class="btn-group">
                                    <button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">Change Status <span class="caret"></span></button>
                                    <ul class="dropdown-menu animated zoomIn">
                                        <li><a data-id="0" onclick="answertype(this,'<?php echo $ticketData->tickets_id ;?>');" href="javascript:void(0)">Open</a></li>
                                        <li><a data-id="1" onclick="answertype(this ,'<?php echo $ticketData->tickets_id ;?>');" href="javascript:void(0)">Closed</a></li> 
                                        <li><a data-id="2" onclick="answertype(this ,'<?php echo $ticketData->tickets_id ;?>');" href="javascript:void(0)">In_progress</a></li>
                                        <li><a data-id="4" onclick="answertype(this ,'<?php echo $ticketData->tickets_id ;?>');"  href="javascript:void(0)">Answered</a></li>
                                    </ul>
                                </div>
                            </div>

                        </h4>
                        <div class="ticket-detail">
                            <p>  <?php echo!empty($ticketData->body) ? $ticketData->body : ''; ?> </p>
                        </div> 
                        <ul class="list-inline mailbox-attachments">
                            <li>                
                                <span class="mailbox-attachment">                 
                                    <?php if (!empty($ticketData->upload_file) && file_exists("uploads/tickets/" . $ticketData->upload_file)) { ?>
                                        <img src="<?php echo base_url() . 'uploads/tickets/' . $ticketData->upload_file; ?>"  class="img-resopnsive"> 
                                    <?php } ?> 
                                </span>
                            </li>
                        </ul>   
                        <div class="chatreply-btn">
                            <button class="btn btn-info" data-toggle="collapse" data-target="#demo"> <i class="fa fa-reply"></i> Reply Ticket </button>
                        </div>
                        <div id="demo" class="collapse">
                            <form name="ticketreplyfrm" id="ticketreplyfrm" action="<?php echo base_url('tickets/ticketcomment'); ?>" method="POST" enctype="multipart/form-data">
                                <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                                <div class="form-group">
                                    <textarea name="comment" class="form-control" placeholder="Ticket reply"></textarea>
                                    <input type="hidden" name="tickets_id" value="<?php echo!empty($ticketData->tickets_id) ? $ticketData->tickets_id : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <input type="file" name="filename" class="form-control">
                                        </div>    
                                        <!-- <div class="col-sm-4">   
                                                <a class="btn btn-info">Add <i class="fa fa-plus"></i></a>
                                        </div>-->
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Submit </button>
                                </div>
                            </form>
                        </div>
                        <div class="chat">
                            <?php
                            if (!empty($ticketsconversion)) {
                                foreach ($ticketsconversion as $ticketsconversions) {
                                    if (file_exists("uploads/userimages/" . $ticketsconversions->image) && !empty($ticketsconversions->image)) {

                                        $img = base_url() . 'uploads/userimages/' . $ticketsconversions->image;
                                    } else {

                                        $img = base_url() . 'assets/images/profile_big_img.png';
                                    }
                                    ?>
                                    <div class="media"> 
                                        <div class="media-left"> 
                                            <a href="#"> 
                                                <img alt="64x64" class="media-object" style="width: 64px; height: 64px;" src="<?php echo $img; ?>">
                                            </a> 
                                        </div> 
                                        <div class="media-body"> 
                                            <div class="media-heading"> 
                                                <?php echo!empty($ticketsconversions->name) ? $ticketsconversions->name : ''; ?> 
                                                <?php if ($ticketsconversions->type == 4) { ?>
                                                    <span class="label label-success">Client</span>
                                                <?php } else { ?>
                                                    <span class="label label-success">Admin</span>
                                                <?php } ?>
                                                <span class="time"><i class="fa fa-clock-o"></i> <?php echo getTimeAgo($ticketsconversions->time); ?></span>
                                            </div> 
                                            <?php if (!empty($ticketsconversions->attachment) && file_exists("uploads/tickets/" . $ticketsconversions->attachment)) { ?>
                                                <img src="<?php echo base_url() . 'uploads/tickets/' . $ticketsconversions->attachment; ?>" class="img-responsive"> 
                                            <?php } ?> 
                                            <?php echo!empty($ticketsconversions->comment) ? $ticketsconversions->comment : ''; ?>
                                        </div>
                                    </div>
                                <?php }
                            }
                            ?>
                 </div>
                    </div>       
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade bs-example-modal-sm" id="ticketmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
                <h4 class="modal-title" id="mySmallModalLabel">Ticket Detail</h4> 
            </div>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>Ticket No.</td> 
                        <td><?php echo!empty($ticketData->ticket_code) ? $ticketData->ticket_code : ''; ?></td>
                    </tr> 
                    <tr>
                        <td>Reporter</td> 
                        <td><?php echo!empty($ticketData->name) ? $ticketData->name : ''; ?></td>
                    </tr>
                    <tr>
                        <td> Department</td> 
                        <td><?php echo!empty($ticketData->departments_id) ? $ticketData->departments_id : ''; ?></td>
                    </tr>
                    <tr>
                        <td> Status</td> 
                        <td>
                            <?php if (!empty($ticketData->status) && ($ticketData->status == 1)) { ?>
                                <span class="label label-success">Close</span>
                            <?php } else if (!empty($ticketData->status) && ($ticketData->status == 2)) { ?>

                                <span class="label label-info">Inprogress</span>

                            <?php } else if (!empty($ticketData->status) && ($ticketData->status == 3)) { ?> 

                                <span class="label label-primary">Pending</span>

                            <?php } else if (!empty($ticketData->status) && ($ticketData->status == 4)) { ?>
                                <span class="label label-info">Answer</span>
                            <?php } else { ?>
                                <span class="label label-danger">Open</span>
                            <?php } ?>	


                        </td>
                    </tr>
                    <tr>
                        <td>Priority</td> 
                        <td>
                            <?php if (!empty($ticketData->priority) && ($ticketData->priority == 1)) { ?>
                                <span class="label label-info">Low</span>
                            <?php } else if (!empty($ticketData->priority) && ($ticketData->priority == 2)) { ?>
                                <span class="label label-warning">Medium</span>
                            <?php } else if (!empty($ticketData->priority) && ($ticketData->priority == 3)) { ?>
                                <span class="label label-danger">High</span>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Created</td> 
                        <td><?php echo!empty($ticketData->created) ? date('d-m-Y h:i A', strtotime($ticketData->created)) : ''; ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
	<form name="ticketcommentfrm" id="ticketcommentfrm" action="<?php echo base_url('tickets/answerticket');?>" method="POST" enctype="multipart/form-data">
        <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
            <h4 class="modal-title" id="myModalLabel">Comments</h4> 
        </div>
          <div class="modal-body">  
		   <input type="hidden" name="ticketstatus" id="ticketstatus" value=""/>
		   <input type="hidden" name="tickets_id" id="tickets_id" value=""/>
            <div class="form-group">
                <textarea class="form-control" name="answer" id="answer" rows="6"></textarea>
            </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
	</form>		
    </div>
  </div>
</div>



<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script>
    $(document).ready(function () {
        $('#ticketreplyfrm').validate({
            rules: {
                comment: "required"

            },
            submitHandler: function (form) {
                form.submit();
            },
            messages: {
                required: "Please enter your messages",

            },
        });
    });
    
    function deletetickets(tickets_id){
        bootbox.confirm({
            title: "Delete Tickets",
            message: "Are you sure you want to delete this Tickets",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("tickets/deletetickets"); ?>'+'/'+tickets_id;
                }
            }
        });
    }
    function answertype(obj,tickets_id){
 
	 var id = $(obj).attr("data-id");
	      $('#myModal').modal('toggle');
		  $('#ticketstatus').val(id);
		  $('#tickets_id').val(tickets_id);
	}
</script>



