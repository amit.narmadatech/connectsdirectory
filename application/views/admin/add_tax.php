<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('setting/post_tax'); ?>" >
    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
  <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
  <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Tax</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>

          <div class="form-group">
            <label for="package_id" class="col-md-4 col-sm-5 col-xs-12">Select Package <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="package_id" id="package_id" class="form-control col-md-7 col-xs-12">
                  <option value="">Select Package</option>
                  <?php if (!empty($package_list)) {
                      foreach ($package_list as $package) { ?>
                  <option <?= (isset($edit) && ($edit->package_id == $package->id))?'selected':''; ?> value="<?= $package->id ?>"><?= $package->title ?></option>

                  <?php   }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('package_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="state_id" class="col-md-4 col-sm-5 col-xs-12">Select State <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="state_id" id="state_id" class="form-control col-md-7 col-xs-12">
                  <?php if (!empty($state_list)) { ?>
                  <option <?php echo (isset($edit) && ($edit->state_id == $state_list->state_id))?'selected':''; ?> value="<?php echo $state_list->state_id; ?>"><?php echo $state_list->state_title; ?></option>

                  <?php } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('state_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="sgst_tax" class="col-md-4 col-sm-5 col-xs-12">S.G.S.T. <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="number" id="sgst_tax" name="sgst_tax" class="form-control col-md-7 col-xs-12" placeholder="Enter S.G.S.T." value="<?= (isset($edit) && (!empty(($edit->sgst_tax))))?$edit->sgst_tax:set_value('sgst_tax'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('sgst_tax'); ?></span>
          </div>

          <div class="form-group">
            <label for="cgst_tax" class="col-md-4 col-sm-5 col-xs-12">C.G.S.T. <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="number" id="cgst_tax" name="cgst_tax" class="form-control col-md-7 col-xs-12" placeholder="Enter C.G.S.T." value="<?= (isset($edit) && (!empty(($edit->cgst_tax))))?$edit->cgst_tax:set_value('cgst_tax'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('cgst_tax'); ?></span>
          </div>

          <div class="form-group">
            <label for="igst_tax" class="col-md-4 col-sm-5 col-xs-12">I.G.S.T <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="number" id="igst_tax" name="igst_tax" class="form-control col-md-7 col-xs-12" placeholder="Enter I.G.S.T" value="<?= (isset($edit) && (!empty(($edit->igst_tax))))?$edit->igst_tax:set_value('igst_tax'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('igst_tax'); ?></span>
          </div>

          <div class="form-group">
            <label for="from_date" class="col-md-4 col-sm-5 col-xs-12">Start Date<span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="date" id="from_date" name="from_date" class="form-control col-md-7 col-xs-12" value="<?php echo date('Y-m-d'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('from_date'); ?></span>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('admindashboard/department_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>

        
      </div>

    </div>
  </div>

    <div class="col-md-6 col-sm-6 col-xs-6">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tax List</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <div class="table-responsive">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Package</th>
                <th>SGST</th>
                <th>CGST</th>
                <th>IGST</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php 
               if(!empty($tax_list)){
                  $i = 1;
                foreach($tax_list as $tax){ ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php  echo !empty($tax->package_title) ? $tax->package_title : '' ?></td>
                  <td><?php  echo !empty($tax->sgst_tax) ? $tax->sgst_tax : '' ?></td>
                  <td><?php  echo !empty($tax->cgst_tax) ? $tax->cgst_tax : '' ?></td>
                  <td><?php  echo !empty($tax->igst_tax) ? $tax->igst_tax : '' ?></td>
                  <td><?php  echo !empty($tax->from_date) ? date('D d M Y h:i a',strtotime($tax->from_date)) : '' ?></td>
                  <td><?php  echo ($tax->to_date != "0000-00-00 00:00:00") ? date('D d M Y h:i a',strtotime($tax->to_date)) : '-' ?></td>
                  <td>
                  <?php  if($tax->status == 1){echo "Active"; }elseif($tax->status == 2){ echo "Disabled"; }else{ echo "Pending"; } ?></td>
                  
                </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>


  </form>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<script>
    $(document).ready(function($) {
    
  $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
              form.submit();
            },
            ignore: [],
            rules: {
                state_id: "required",
            },
            messages:{
                state_id: "The state field is required",
            },
            highlight: function (input) {
                $(input).parents('.form-group').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-group').removeClass('error');
            },
            
        });

    });
});
</script>