<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
    .show-read-more .more-text{
        display: none;
    }
</style>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      	    <?php if($this->session->flashdata('success')){?>
				<div class='alert alert-success'> <?= $this->session->flashdata('success'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
			<?php } ?>
      <div class="x_panel">
        <div class="x_title">
          <h2>Blog List</h2>
          <ul class="nav navbar-right panel_toolbox">
           
            <li>
				<a href="<?= base_url('blog/addblog'); ?>" class="close-link"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add New Blog</button></a>
			</li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Blog Image</th>
                <th>Blog Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Stauts</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                 <?php 
                     if(!empty($blog)){
                      foreach($blog as $blogs){
						  
						if(!empty($blogs->blog_img) && file_exists("uploads/blog/".$blogs->blog_img)){
					   
							$img = base_url().'uploads/blog/'.$blogs->blog_img;
						}else{
							$img = base_url().'assets/images/img_not_available.png';
						}  
						  
						?>
                      <tr>
                        <td><img src="<?php echo $img; ?>" width="150" height="100"/></td>
                        <td><?php  echo !empty($blogs->title) ? $blogs->title : '' ?></td>
                        <td><?php echo !empty($blogs->description) ? substr(strip_tags($blogs->description),0,250) : '' ?>...</td>
                        <td><?php  echo !empty($blogs->created_date) ?date("D, d M Y",strtotime($blogs->created_date)): '' ?></td>
                        <td>
						
						<?php  if(!empty($blogs->status) && ($blogs->status == 1)){ ?>
						    <button type="button" class="btn btn-success">Active</button>	
						<?php }else{ ?> 
						  <button type="button" class="btn btn-danger">Inactive</button>	
						<?php } ?>
							
						</td>
                        <td>							
						<div class="btn-group">
						<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button" aria-expanded="false">Action <span class="caret"></span></button>
						<ul role="menu" class="dropdown-menu">
							<li>
							   <a href="<?php echo base_url('blog/addblog/'.$blogs->id);?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
							</li>
							<li>
							    <?php  if(!empty($blogs->status) && ($blogs->status == 1)){ ?>
								   <a href="javascript:void(0);" onclick="changestatus('<?php echo $blogs->id;?>')"> <i class="fa fa-times"></i> Inactive</a>
								<?php }else{ ?>
								 <a href="javascript:void(0);" onclick="changestatus('<?php echo $blogs->id;?>')"> <i class="fa fa-check"></i> Active</a>
								<?php } ?>
							</li>
							<li>
							<a href="javascript:void(0);" onclick="deleteblog('<?php echo $blogs->id;?>')"> <i class="fa fa-trash"></i> Delete</a>
						 </li>
						</ul>
					  </div>
					 </td>
                    </tr>
                    <?php } } ?>
                </tbody>
            </table>
          </div>
        </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
  
  <script>
    function changestatus(id){
        bootbox.confirm({
            title: "Change Status",
            message: "Are you sure you want to chage status this blog",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("blog/changeBlogStatus"); ?>'+'/'+id;
                }
            }
        });
    }
	function deleteblog(id){
        bootbox.confirm({
            title: "Delete Blog",
            message: "Are you sure you want to delete status this blog",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("blog/deleteblog"); ?>'+'/'+id;
                }
            }
        });
    }
 </script>
 <script type="text/javascript">
$(document).ready(function(){
	var maxLength = 20;
	$(".show-read-more").each(function(){
		var myStr = $(this).text();
		if($.trim(myStr).length > maxLength){
			var newStr = myStr.substring(0, maxLength);
			var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
			$(this).empty().html(newStr);
			$(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>');
			$(this).append('<span class="more-text">' + removedStr + '</span>');
		}
	});
	$(".read-more").click(function(){
		$(this).siblings(".more-text").contents().unwrap();
		$(this).remove();
	});
});
</script>
