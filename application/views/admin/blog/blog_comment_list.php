<?php $this->load->view('admin/include/header'); ?>
<style type="text/css">
    .show-read-more .more-text{display: none;}
</style>
 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
        <?php if($this->session->flashdata('success')){?>
			<div class='alert alert-success'> 
                <?= $this->session->flashdata('success'); ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
            </div>
		<?php } ?>
    <div class="x_panel">
       <div class="x_title">
        <h2>Blog Comment List</h2>
        <div class="clearfix"></div>
       </div>
       <div class="x_content">
       <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Blog Title</th>
                <th>Name</th>
                <th>Email</th>
                <th>Comment</th>
                <th>Date</th>
                <th>Stauts</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
              if(!empty($blogcomment)){
              foreach($blogcomment as $blogcomments){?>
            <tr>
                <td><?php echo!empty($blogcomments->title) ? $blogcomments->title : ''; ?></td>
                <td><?php echo!empty($blogcomments->name) ? $blogcomments->name : ''; ?></td>
                <td><?php echo!empty($blogcomments->email) ? $blogcomments->email : ''; ?></td>
                <td><?php echo!empty($blogcomments->comment) ? $blogcomments->comment : '' ?></td>
                <td><?php echo!empty($blogcomments->created_date) ? date("D, d M Y", strtotime($blogcomments->created_date)) : '' ?></td>
                <td><?php if (!empty($blogcomments->status) && ($blogcomments->status == 1)) { ?>
                        <button type="button" class="btn btn-success">Approved</button>	
                    <?php } else { ?> 
                        <button type="button" class="btn btn-info">Pending</button><?php } ?>
                </td>
                <td>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button" aria-expanded="false">Action <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu">
                            <li>
                                <?php if (!empty($blogcomments->status) && ($blogcomments->status == 1)) { ?>
                                    <a href="javascript:void(0);" onclick="changestatus('<?php echo $blogcomments->id; ?>')"><i class="fa fa-thumbs-down"></i> Unapproved</a>
                                <?php } else { ?>
                                    <a href="javascript:void(0);" onclick="changestatus('<?php echo $blogcomments->id; ?>')"><i class="fa fa-thumbs-o-up"></i> Approved</a>
                                <?php } ?>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="deleteblog('<?php echo $blogcomments->id; ?>')"><i class="fa fa-trash"></i> Delete</a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php } } ?>
        </tbody>
       </table>
    </div>
   </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<!-- Datatables -->
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script>
    function changestatus(id){
        bootbox.confirm({
            title: "Change Status",
            message: "Are you sure you want to chage status this blog",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("blog/changeBlogCommentStatus"); ?>'+'/'+id;
                }
            }
        });
    }
	function deleteblog(id){
        bootbox.confirm({
            title: "Delete Blog",
            message: "Are you sure you want to delete status this blog",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result){
                    window.location.href='<?php echo base_url("blog/deleteBlogComment"); ?>'+'/'+id;
                }
            }
        });
    }
 </script>
<script type="text/javascript">
$(document).ready(function(){
	var maxLength = 20;
	$(".show-read-more").each(function(){
		var myStr = $(this).text();
		if($.trim(myStr).length > maxLength){
			var newStr = myStr.substring(0, maxLength);
			var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
			$(this).empty().html(newStr);
			$(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>');
			$(this).append('<span class="more-text">' + removedStr + '</span>');
		}
	});
	$(".read-more").click(function(){
		$(this).siblings(".more-text").contents().unwrap();
		$(this).remove();
	});
});
</script>