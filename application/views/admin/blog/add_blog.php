<?php $this->load->view('admin/include/header'); ?>
<script src="<?php echo base_url('assets/tinymce/');?>tinymce.min.js"></script>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <?php if($this->session->flashdata('error')){?>
		<div class='alert alert-danger'><?= $this->session->flashdata('error'); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> </div>
	<?php } ?>
   
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo !empty($blog->id)? 'Update' : 'Add' ;?> Blog</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="form_validation" name="form_validation" method="POST" action="<?php echo base_url('blog/addblog'); ?>" enctype="multipart/form-data" class="form-horizontal form-label-left" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
         <input type="hidden" name="blogid" id="blogid"  value="<?php echo !empty($blog->id)? $blog->id : '' ;?>">
         <div class="row">
          <div class="col-sm-12">   
            <div class="form-group">
              <label class="col-md-2 col-sm-5 col-xs-12" for="first-name">Blog Image <span class="required">*</span>
              </label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <input type="file" name="file" id="file" class="form-control" value="">
              </div>
            <span class="text-danger"><?php echo form_error('file'); ?></span>
          </div>
        </div>
      </div>

      <div class="row">
       <div class="col-sm-12">   
          <div class="form-group">
            <label class="col-md-2 col-sm-5 col-xs-12" for="first-name">Blog Title<span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="title" id="title" placeholder="Title" class="form-control" value="<?php echo !empty($blog->title)? $blog->title : '' ;?>">
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>
        </div>
      </div>

	      <div class="row">
         <div class="col-sm-12">   
           <div class="form-group">
              <label class="col-md-2 col-sm-5 col-xs-12" for="description">Description <span class="required">*</span>
              </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
               <textarea name="description" id="description" class="form-control" placeholder="Enterdescription"><?php echo !empty($blog->description)? $blog->description : '' ;?></textarea>
            </div>
          </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
         </div>
        </div>
        <div class="form-group">
          <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <h3 id="whName"></h3>
              <p><span id="whAddress"></span><span id="whCity"></span></p>
              <p><span id="whState"></span><span id="whPincode"></span></p>
            </div>
          </div>

      	  <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('blog/index'); ?>" class="btn btn-danger" type="button">Cancel</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
	//image link media fullscreen
	tinymce.init({ 
		selector:'textarea',
		height: 300,
		toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent code fontselect fontsizeselect',
		plugins: 'code image link media fullscreen'
	});

      $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
              form.submit();
            },
            ignore: [],
            rules: {
                //file: "required",
                title: "required",
                description: "required",
            },
            messages:{
                file: "Blog image is required",
                title: "The blog title field is required",
                description: "The description field is required",
            },
            highlight: function (input) {
                $(input).closest('div').addClass('has-error');
            },
        });

    });

</script>