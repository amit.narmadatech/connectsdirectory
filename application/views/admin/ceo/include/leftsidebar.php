<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0; text-align: center;">
      <img style="max-height:65px; margin-top: 10px" src="<?= base_url('assets/images/logo.svg'); ?>" >
    </div>

    <div class="clearfix"></div>
    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>ADMIN</h3>
        <!-- <ul class="nav side-menu">
          <li><a href="<?= base_url('admindashboard'); ?>"><i class="fa fa-home"></i> Home</a></li>
          <li><a href="<?= base_url('franchise/franchise_list'); ?>"><i class="fa fa-bank"></i> Franchise List</a></li>
          <li><a><i class="fa fa-codepen"></i> Stock <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?= base_url('inventory/product_list'); ?>">Products</a></li>
              <li><a href="<?= base_url('inventory/request_order_list'); ?>">Purchase Order</a></li>
              <li><a href="<?= base_url('inventory/stock_list'); ?>">Stock IN</a></li>
              <li><a href="<?= base_url('kit/kitlist'); ?>">QR Code</a></li>
              <li>
              <ul class="child_menu">
              <li><a href="<?= base_url('inventory/upload_kit'); ?>">Upload Kits</a></li>
            </ul>
          </li>              
			    <li><a>Kits<span class="fa fa-chevron-down"></span></a>
             <ul class="nav child_menu" style="">
					   <li class="sub_menu">
						  <a href="<?php echo base_url('inventory/upload_kitlist'); ?>">Kits</a>
					 </li>
					 <li><a href="<?php echo base_url('inventory/upload_kit'); ?>">Add New Kit</a></li>
					 <li><a href="<?php echo base_url('inventory/upload_kitgroup'); ?>">Kit Group</a></li>
          </ul>
        </li>		
			  <li><a href="<?= base_url('inventory/order_list'); ?>">Orders</a></li>
			  <li><a href="<?= base_url('inventory/stock_out'); ?>">Stock OUT</a></li>
        <li><a href="<?= base_url('inventory/warehouse_list'); ?>">Ware House</a></li>
        <li><a href="<?= base_url('inventory/supplier_list'); ?>">Supplier</a></li>
		 	 </ul>
      </li>
      <li><a href="<?= base_url('admindashboard/customerlist'); ?>"><i class="fa fa-users"></i> Client List</a>
      </li>
      <li><a href="<?= base_url('plan/planlist'); ?>"><i class="fa fa-list"></i> Plan List</a>
          </li>
          <li><a href="<?= base_url('admindashboard/orderlist'); ?>"><i class="fa fa-shopping-cart"></i> Order List</a>
          </li>
          <li><a><i class="fa fa-edit"></i> User Management <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="<?= base_url('admindashboard/userlist'); ?>">User List</a></li>
              <li><a href="<?= base_url('admindashboard/group_list'); ?>">User Group</a></li>
            </ul>
          </li>
          <li><a href="<?= base_url('collection_center'); ?>"><i class="fa fa-university"></i> Collection Center</a></li>
          <li>
			 <a href="<?= base_url('setting'); ?>"><i class="fa fa-gear"></i> Setting</a>
          </li>
		   <li>
			 <a href="<?php echo base_url('blog/index');?>"> <i class="fa fa-list"></i> Blog List</a>
		   </li>
		    <li>
			 <a href="<?php echo base_url('blog-comment');?>"> <i class="fa fa-list"></i> Blog Comment List</a>
		   </li>
         
        </ul> -->

         <ul class="nav side-menu">
          <?php foreach ($this->db->where('status',1)->order_by('sort')->get('menus')->result() as $key => $value) { 
            if (($value->parent == 0)  && check_permission_by_id($value->id)) {
          ?>
          
          <li>
            <a href="<?php echo ($value->link == "#")?'#':base_url($value->link); ?>"><i class="<?php echo $value->icon; ?>"></i> <?php echo get_menu_name($value->label); ?> <?php if(check_menu_has_parent($value->id)){ ?> <span class="fa fa-chevron-down"></span><?php } ?></a>
            <ul class="nav child_menu">
            <?php  
            foreach ($this->db->where('status',1)->order_by('sort')->get('menus')->result() as $key1 => $value1) {
              if (($value1->parent != 0) && ($value1->parent == $value->id) && check_permission_by_id($value1->id)) {
            ?>
              <li>
                <a href="<?php echo ($value1->link == "#")?'#':base_url($value1->link); ?>"><i class="<?php echo $value1->icon; ?>"></i> <?php echo get_menu_name($value1->label); ?> <?php if(check_menu_has_parent($value1->id)){ ?> <span class="fa fa-chevron-down"></span><?php } ?></a>
                <ul class="nav child_menu">
                  <?php  
                  foreach ($this->db->where('status',1)->order_by('sort')->get('menus')->result() as $key2 => $value2) {
                    if (($value2->parent != 0) && ($value2->parent == $value1->id) && check_permission_by_id($value2->id)) {
                  ?>
                  <li><a href="<?php echo ($value2->link == "#")?'#':base_url($value2->link); ?>"><i class="<?php echo $value2->icon; ?>"></i> <?php echo get_menu_name($value2->label); ?></a></li>
                  <?php  }
                  }
                  ?>
                </ul>
              </li>
            <?php } } ?>
            </ul>
          </li>
            
          <?php } } ?>

        </ul>

      </div>


    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?= base_url('admin/logout'); ?>">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>