<?php $this->load->view('admin/ceo/include/header'); ?>
  
  <div class="row">
  	 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  	 	<h2>Summary</h2>
  	 </div>
  </div>
  <!-- top tiles -->
 <div class="row">
    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-green">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count">2,50,00,000</div>
		    <h3> Amount Recieved from Customers</h3>
      </div>
    </div>
    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-red">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count">5,00,000</div>
        <h3>Amount due to Vendors</h3>
      </div>
    </div>
	 <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="box bg-blue">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count"> 25,00,000</div>
        <h3>Total Revenue  </h3>
      </div>
    </div>
    
   
  </div>
  <!-- /top tiles -->
  <!-- top tiles -->
 <div class="row">
    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
       <div class="box bg-orange">
        <div class="icon"> 
        	<i class="fa fa-inr"></i> 
        </div>
        <div class="count">20,00,000</div>
		   <h3> Total Expenses</h3>
      </div>
    </div>
    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-purple">
        <div class="icon">
        	<i class="fa fa-inr"></i>
        </div>
        <div class="count">2,00,00,000</div>
        <h3>Profit</h3>
      </div>
    </div>
	 <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-blue-sky">
        <div class="icon">
        	<i class="fa fa-users"></i>
        </div>
        <div class="count"> 1,000</div>
        <h3>Total Employee  </h3>
      </div>
    </div>
    
   
  </div>
  <!-- /top tiles -->
  
<?php $this->load->view('admin/ceo/include/footer'); ?>   