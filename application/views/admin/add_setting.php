<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <?php $this->load->view('include/alert'); ?>
    <div class="x_panel">
      <div class="x_title">
        <h2>Setting</h2>
       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('setting/set'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
          <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

          <div class="form-group">
            <label for="site_title" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Title <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="site_title" name="site_title" class="form-control col-md-7 col-xs-12" placeholder="Enter site title" value="<?= (isset($key_list) && (!empty(($key_list['site_title']))))?$key_list['site_title']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('site_title'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="tag_line" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tag Line <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="tag_line" name="tag_line" class="form-control col-md-7 col-xs-12" placeholder="Enter tag line" value="<?= (isset($key_list) && (!empty(($key_list['tag_line']))))?$key_list['tag_line']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('tag_line'); ?></span>
          </div>

          <div class="form-group">
            <label for="site_url" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site URL <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="site_url" name="site_url" class="form-control col-md-7 col-xs-12" placeholder="Enter site URL" value="<?= (isset($key_list) && (!empty(($key_list['site_url']))))?$key_list['site_url']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('site_url'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="site_email" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Email <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="site_email" name="site_email" class="form-control col-md-7 col-xs-12" placeholder="Enter site URL" value="<?= (isset($key_list) && (!empty(($key_list['site_email']))))?$key_list['site_email']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('site_email'); ?></span>
          </div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>

      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('setting/set'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

          <div class="form-group">
            <label for="google_client_id" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Google Client ID <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="google_client_id" name="google_client_id" class="form-control col-md-7 col-xs-12" placeholder="Enter Google Client ID" value="<?= (isset($key_list) && (!empty(($key_list['google_client_id']))))?$key_list['google_client_id']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('google_client_id'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="google_client_secret" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Google Client Secret <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="google_client_secret" name="google_client_secret" class="form-control col-md-7 col-xs-12" placeholder="Enter Google Client Secret" value="<?= (isset($key_list) && (!empty(($key_list['google_client_secret']))))?$key_list['google_client_secret']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('google_client_secret'); ?></span>
          </div>

          <div class="form-group">
            <label for="google_redirect_uri" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Google Redirect URL <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="google_redirect_uri" name="google_redirect_uri" class="form-control col-md-7 col-xs-12" placeholder="Enter Google Redirect URL" value="<?= (isset($key_list) && (!empty(($key_list['google_redirect_uri']))))?$key_list['google_redirect_uri']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('google_redirect_uri'); ?></span>
          </div>
          
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>

      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('setting/set'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

          <div class="form-group">
            <label for="facebook_app_id" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook App ID <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="facebook_app_id" name="facebook_app_id" class="form-control col-md-7 col-xs-12" placeholder="Enter Facebook App ID" value="<?= (isset($key_list) && (!empty(($key_list['facebook_app_id']))))?$key_list['facebook_app_id']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('facebook_app_id'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="facebook_app_secret" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook App Secret <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="facebook_app_secret" name="facebook_app_secret" class="form-control col-md-7 col-xs-12" placeholder="Enter Facebook App Secret" value="<?= (isset($key_list) && (!empty(($key_list['facebook_app_secret']))))?$key_list['facebook_app_secret']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('facebook_app_secret'); ?></span>
          </div>

          <div class="form-group">
            <label for="facebook_redirect_url" class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Facebook Redirect URL <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="facebook_redirect_url" name="facebook_redirect_url" class="form-control col-md-7 col-xs-12" placeholder="Enter Facebook Redirect URL" value="<?= (isset($key_list) && (!empty(($key_list['facebook_redirect_url']))))?$key_list['facebook_redirect_url']:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('facebook_redirect_url'); ?></span>
          </div>
          
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>


    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    $("#add_row").click(function(event) {
        var id = $("#product_list").val();
        if (id == '') { alert('Please first choose product !');$("#product_list").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  var html = '<tr id="row_'+response.data.id+'"><td></td><td>'+response.data.code+'-'+response.data.name+'<input type="hidden" name="product[]" value="'+response.data.id+'"></td><td style="width:175px;"><input type="number" class="changeQty form-control" id="'+response.data.id+'" value="1" min="1" name="quantity[]"></td><td><span id="rateText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="rate_'+response.data.id+'" name="rate[]" value="'+response.data.purchase_price+'"></td><td><span id="amountText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="amount_'+response.data.id+'" name="amount[]" value="'+response.data.purchase_price+'"></td><td><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+response.data.id+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  $('#grandTotalText').html(response.data.purchase_price);
                  $('#grand_total').val(response.data.purchase_price);
                  console.log(html);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
    });

    $(document).on('change', '.changeQty', function(event) {
        var id = this.id;
        var qty = parseFloat(this.value);
        var rate = parseFloat($('#rate_'+id).val());
        var amt = 0;
        if( (!isNaN(qty)) && (!isNaN(rate))){
            amt = qty*rate;
            $('#amountText_'+id).html(amt); 
            $('#amount_'+id).val(amt);
            $('#grandTotalText').html(amt); 
            $('#grand_total').val(amt);
        }
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
    $('#form_validation1').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
      rules: {
                title: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    numeric:true,
                    required:true,
                },
                contact2: {
                    numeric:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
      },
      messages:{
        title: "Please enter title",
        hierarchy_id: "Please select designation",
        designation: "Please enter group title",
      },
      highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
      },
      unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
      },
      errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
      }
        });

    });

});
</script>
