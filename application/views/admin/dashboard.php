<?php $this->load->view('admin/include/header'); ?>
  <!-- top tiles -->
  <div class="row">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a href="<?php echo base_url('admindashboard/customerlist'); ?>">
      <div class="tile-stats bg-green">
        <div class="icon"><i class="fa fa-users"></i>
        </div>
        <div class="count"><?php echo !empty($totalcustomer )? $totalcustomer : 0; ?></div>
		    <h3>Total Customer</h3>
      </div>
      </a>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a href="<?php echo base_url('admindashboard/orderlist'); ?>">
      <div class="tile-stats bg-red">
        <div class="icon"><i class="fa fa-shopping-cart"></i>
        </div>
        <div class="count"><?php echo !empty($totalorder )? $totalorder : 0; ?></div>
        <h3>Total Orders</h3>
      </div>
      </a>
    </div>
	 <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a href="#">
      <div class="tile-stats bg-blue">
        <div class="icon"><i class="fa fa-inr"></i>
        </div>
        <div class="count"> <?php echo !empty($totalamount)? round($totalamount,2) : 0; ?></div>
        <h3>Total Amount</h3>
      </div>
      </a>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <a href="<?php echo base_url('inventory/upload_kitlist'); ?>">
      <div class="tile-stats bg-orange">
        <div class="icon"><i class="fa fa-cubes"></i>
        </div>
        <div class="count"><?php echo !empty($totalkits)? $totalkits : 0; ?></div>
        <h3>Kits</h3>
      </div>
      </a>
    </div>   
  </div>
  <!-- /top tiles -->
  <br />
  <div class="row">
    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="x_panel">       
        <div class="x_title">
          <h2>Latest Sale </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Order No.</th>
                <th>Transaction No.</th>
                <th>Status</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (!empty($order_list)) {
                  $i=1;
                  foreach ($order_list as $odlist){ ?>
                <tr>
                  <th scope="row"><?php echo $i++; ?></th>
                  <td><?php echo (!empty($odlist->txnid))?$odlist->txnid:'-'; ?></td>
                  <td><?php echo (!empty($odlist->order_number))?$odlist->order_number:'-'; ?></td>
                  <td><?php if ($odlist->order_status == 1) { echo "Pending"; }elseif ($odlist->order_status == 2) { echo "Dispatched"; }elseif ($odlist->order_status == 3) { echo "Completed"; } ?></td>
                  <td><?php echo (!empty($odlist->order_date))?date("D d M Y h:i a", strtotime($odlist->order_date)):'-'; ?></td>
                </tr>
                <?php  } 
                }
              ?>
              <tr>
                <td colspan="4"></td>
                <td ><a class="label label-info pull-right" href="<?php echo base_url('admindashboard/orderlist'); ?>">View all..</a></td>
              </tr>
            </tbody>
          </table>
         </div>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="x_panel tile fixed_height_320">
        <div class="x_title">
          <h2>Kit Stock</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="dashboard-widget-content">
            <ul class="quick-list">
              <li><i class="fa fa-line-chart"></i><a href="#">Stock Status</a>
              </li>
              
            </ul>

            <div class="sidebar-widget">
                <h4>Kit</h4>
                <canvas width="150" height="80" id="chart_gauge_01" class="" style="width: 160px; height: 100px;"></canvas>
                <div class="goal-wrapper">
                  <span id="gauge-text" class="gauge-value pull-left">0</span>
                  <span class="gauge-value pull-left"></span>
                  <span id="goal-text" class="goal-value pull-right"></span>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
        <!-- Start to do list -->
        <div class="col-md-5 col-sm-5 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Notification List </h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="">
                <ul class="list-unstyled timeline widget">
                <?php foreach ($notification_list as $nflist) { ?>
                  <li>
                    <div id="<?= $nflist->id ?>" class="block single-notification <?php echo (check_notification_isread($nflist->id,$this->session->user_id))?'':'read'; ?>">
                      <div class="block_content">
                        <h2 class="title"><a><?= $nflist->title ?></a> </h2>
                        <div class="byline">
                          <span><?php echo date('D d M Y h:i a', strtotime($nflist->created_at)); ?></span>
                        </div>
                        <p class="excerpt"><?= $nflist->body ?></p>
                      </div>
                    </div>
                </li>
                <?php } ?>
               </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- End to do list -->        
        <!-- start of weather widget -->
        <!-- <div class="col-md-7 col-sm-7 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Message Board</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="bs-example" data-example-id="simple-jumbotron">
                <div class="jumbotron">
                  <h1>Hello, world!</h1>
                  <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                </div>
              </div>
            </div>
          </div>
        </div> -->
       <!-- end of weather widget -->      
  </div>
<?php $this->load->view('admin/include/footer'); ?>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    
    if("undefined"!=typeof Gauge) {
        console.log("init_gauge ["+$(".gauge-chart").length+"]"),
        console.log("init_gauge");
        var a= {
            lines:12,
            angle:0,
            lineWidth:.4,
            pointer: {
                length: .75, strokeWidth: .042, color: "#1D212A"
            }
            ,
            limitMax:"false",
            colorStart:"#1ABC9C",
            colorStop:"#1ABC9C",
            strokeColor:"#F0F3F3",
            generateGradient:!0
        }
        ;
        if($("#chart_gauge_01").length)var b=document.getElementById("chart_gauge_01"),
        c=new Gauge(b).setOptions(a);
        if($("#gauge-text").length&&(c.maxValue='<?php echo $quantity_max; ?>', c.animationSpeed=32, c.set('<?php echo $quantity_set; ?>'), c.setTextField(document.getElementById("gauge-text"))), $("#chart_gauge_02").length)var d=document.getElementById("chart_gauge_02"),
        e=new Gauge(d).setOptions(a);
    }
  });
</script>  