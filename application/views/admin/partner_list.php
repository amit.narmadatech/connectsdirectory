<?php $this->load->view('admin/include/header1'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Partner List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th >Email</th>
                <th>Phone</th>
                <th>District</th>
                <th>State</th>
                <th>Subject</th>
                <th>Message</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
               <?php if (!empty($partners)) {
                    foreach ($partners as $partnersVal) {
                        ?>
                        <tr>
                            <td> <?php echo!empty($partnersVal->name) ? ucfirst($partnersVal->name) : ''; ?></td>
                            <td><?php echo!empty($partnersVal->email) ? $partnersVal->email : ''; ?></td>
                             <td><?php echo!empty($partnersVal->mobile) ? $partnersVal->mobile : '';  ?></td>
                             <td><?php echo !empty(getDistrict($partnersVal->district_id)) ? getDistrict($partnersVal->district_id)->district_title : ''; ?></td>
                             <td><?php echo !empty(getState(getDistrict($partnersVal->district_id)->state_id)) ? getState(getDistrict($partnersVal->district_id)->state_id)->state_title : ''; ?></td>
                            <td><?php echo!empty($partnersVal->subject) ? $partnersVal->subject : '';  ?></td>
                            <td><?php echo!empty($partnersVal->message) ? $partnersVal->message : ''; ?></td>
                            <td><?php echo !empty($partnersVal->date) ? date('d-m-Y', strtotime($partnersVal->date)) : ''; ?></td>
                            <td>
                                <div class="btn-group">
                                     <a href="<?php echo site_url('setting/deletepartner/').$partnersVal->id; ?>" class="btn btn-danger a-btn-slide-text deleteuser">
                                        <span class="glyphicon glyphicon-trash"></span>
                                         <span><strong>Delete</strong></span>            
                                     </a>
                                </div>
                            </td>
                        </tr>
                    <?php }
                } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer1'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
<script>
    $(document).ready(function () {
        $('#datatable').DataTable();
        $(document).on("click", ".deleteuser", function (e) {
            var link = $(this).attr("href");
            e.preventDefault();
            bootbox.confirm("Are you sure you want to delete this?", function (result) {
                if (result) {
                    document.location.href = link;
                }

            });

        });
    });

</script>