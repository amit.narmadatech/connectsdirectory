<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Assign Department</h2>        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('admindashboard/post_assign_department'); ?>">
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

          <div class="form-group">
            <label for="employee_id" class="control-label col-md-2 col-sm-2 col-xs-12">Employee <span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="employee_id" id="employee_id" class="form-control">
                  <option></option>
                  <?php if (!empty($employee_list)) {
                      foreach ($employee_list as $emp) { ?>
                  <option <?= (isset($edit) && ($edit->id == $emp->id))?'selected':''; ?> value="<?= $emp->id ?>"><?php echo $emp->firstname; ?> <?php  echo $emp->lastname; ?></option>

                  <?php   }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('employee_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="designation_id" class="control-label col-md-2 col-sm-2 col-xs-12">Department / Designation<span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="designation_id" id="designation_id" class="form-control">
                  <option></option>
                  <?php
                  foreach ($departments as $dep) { ?>
                  <optgroup label="<?php echo $dep->name; ?>">
                    <?php foreach ($designations as $desg) { 
                      if($desg->department_id == $dep->id){
                    ?>
                      <option <?= (isset($edit) && ($edit->designation_id == $desg->id))?'selected':''; ?> value="<?= $desg->id ?>"><?= $desg->name ?></option>
                      
                    <?php }} ?>
                  </optgroup>
                  <?php } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('designation_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="country_id" class="control-label col-md-2 col-sm-2 col-xs-12">Country <span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="country_id" id="country_id" class="form-control">
                  <option></option>
                  <?php if (!empty($country_list)) {
                      foreach ($country_list as $country) { ?>
                  <option <?= (isset($edit) && ($edit->country_id == $country->id))?'selected':''; ?> value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
              <div class="checkbox">
                  <label>
                    <input type="radio" <?php if(isset($edit) && ($edit->hierarchy_id == 1)){ echo "checked"; } ?> name="hierarchy_level" value="1" id="country_checkbox"> 
                  </label>
              </div>
            </div>
            <span class="text-danger"><?php echo form_error('country_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="zone_id" class="control-label col-md-2 col-sm-2 col-xs-12">Zones<span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="zone_id" id="zone_id" class="form-control">
                  <option></option>
                  <option <?php if(isset($edit) && ($edit->zone_id == 1)){ echo "selected"; } ?> value="1">East Zone</option>
                  <option <?php if(isset($edit) && ($edit->zone_id == 2)){ echo "selected"; } ?> value="2">West Zone</option>
                  <option <?php if(isset($edit) && ($edit->zone_id == 3)){ echo "selected"; } ?> value="3">North Zone</option>
                  <option <?php if(isset($edit) && ($edit->zone_id == 4)){ echo "selected"; } ?> value="4">South Zone</option>
              </select>
            </div>

            <div class="col-md-1 col-sm-1 col-xs-2">
              <div class="checkbox">
                  <label>
                    <input type="radio" <?php if(isset($edit) && ($edit->hierarchy_id == 2)){ echo "checked"; } ?> name="hierarchy_level" value="2" id="zone_checkbox">  
                  </label>
              </div>
            </div>
            <span class="text-danger"><?php echo form_error('zone_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="state_id" class="control-label col-md-2 col-sm-2 col-xs-12">State <span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="state_id" id="state_id" class="form-control">
                  <option></option>
                  <?php if (!empty($state_list)) {
                      foreach ($state_list as $state) { ?>
                  <option <?= (isset($edit) && ($edit->state_id == $state->id))?'selected':''; ?> value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
              <div class="checkbox">
                  <label>
                    <input type="radio" <?php if(isset($edit) && ($edit->hierarchy_id == 3)){ echo "checked"; } ?> name="hierarchy_level" value="3" id="state_checkbox"> 
                  </label>
              </div>
            </div>
            <span class="text-danger"><?php echo form_error('state_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="city_id" class="control-label col-md-2 col-sm-2 col-xs-12">City <span class="required">*</span>
            </label>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <select name="city_id" id="city_id" class="form-control">
              <option></option>
              </select>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
              <div class="checkbox">
                  <label>
                    <input type="radio" <?php if(isset($edit) && ($edit->hierarchy_id == 5)){ echo "checked"; } ?> name="hierarchy_level" value="5" id="city_checkbox"> 
                  </label>
              </div>
            </div>
            <span class="text-danger"><?php echo form_error('city_id'); ?></span>
          </div>

          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-2">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
      checkCountry();
      checkZone();
      checkState();

      function checkCountry() {
        
        var country_id = $('#country_id').val();
        if(country_id != "") {
          $('#zone_id').prop('disabled', false);
          $('#country_checkbox').prop({'checked': true,'disabled': false});
        }else{
          $('#zone_id').prop('disabled', true);
          //$('#country_checkbox').prop({'checked': false,'disabled': true});
          $('#state_id').prop('disabled', true);
          //$('#zone_checkbox').prop({'checked': false,'disabled': true});
          $('#city_id').prop('disabled', true);
          //$('#state_checkbox').prop({'checked': false,'disabled': true});
          //$('#city_checkbox').prop({'checked': false,'disabled': true});
        }
     
      }

      function checkZone() {
        
        var zone_id = $('#zone_id').val();
        if(zone_id != "") {
          $('#state_id').prop('disabled', false);
          $('#zone_checkbox').prop({'checked': true,'disabled': false});
        }else{
          
        }
     
      }

      function checkState() {
        
        var state_id = $('#state_id').val();
        if(state_id != "") {
          $('#city_id').prop('disabled', false);
          $('#state_checkbox').prop({'checked': true,'disabled': false});
        }else{
          
        }
     
      }

      function checkCity() {
        
        var city_id = $('#city_id').val();
        if(city_id != "") {
          $('#city_checkbox').prop({'checked': true,'disabled': false});
        }else{
          
        }
     
      }

      $(document).on('change', '#country_id', function(event) {
        checkCountry();
      });

      $(document).on('change', '#zone_id', function(event) {
        checkZone();
      });

      $(document).on('change', '#state_id', function(event) {
        checkState();
      });

      $(document).on('change', '#city_id', function(event) {
        checkCity();
      });

        
        $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');
              checkCity();
            }else{
              
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  

});
</script>