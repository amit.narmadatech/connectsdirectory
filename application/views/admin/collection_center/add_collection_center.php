<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Collection Center</h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('collection_center/add'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

           <div class="row">
               <div class="col-sm-6">            
                 <div class="form-group">
            <label for="title" class="col-md-4 col-sm-3 col-xs-12">Title <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
              <input type="text" id="title" name="title" class="form-control" placeholder="Enter title" value="<?= (isset($edit) && (!empty(($edit->title))))?$edit->title:set_value('title'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>
        </div>
     
         <div class="col-sm-6">      
          <div class="form-group">
            <label for="email" class="col-md-4 col-sm-5 col-xs-12">Email <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-5 col-xs-12">  
              <input type="email" id="email" name="email" class="form-control" placeholder="Enter email ID" value="<?= (isset($edit) && (!empty(($edit->email))))?$edit->email:set_value('email'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('email'); ?></span>
          </div>
        </div>
      </div>

        <div class="row">
        <div class="col-sm-6">      
          <div class="form-group">
            <label for="contact1" class=" col-md-4 col-sm-5 col-xs-12">Contact Number<span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="contact1" name="contact1" class="form-control" placeholder="Enter contact number" value="<?= (isset($edit) && (!empty(($edit->contact1))))?$edit->contact1:set_value('contact1'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact1'); ?></span>
          </div>
        </div>
      
         <div class="col-sm-6">      
          <div class="form-group">       
            <label for="contact2" class="col-md-4 col-sm-5 col-xs-12">Alternate Contact No. <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="contact2" name="contact2" class="form-control" placeholder="Enter Alternate contact number" value="<?= (isset($edit) && (!empty(($edit->contact2))))?$edit->contact2:set_value('contact2'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact2'); ?></span>
          </div>
        </div>

        </div>

      <div class="row">
          <div class="col-sm-6">     
          <div class="form-group">
            <label for="state_id" class="col-md-4 col-sm-5 col-xs-12">State <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="state_id" id="state_id" class="form-control">
                  <option></option>
                  <?php if (!empty($state_list)) {
                      foreach ($state_list as $state) { ?>
                  <option <?= (isset($edit) && ($edit->state_id == $state->id))?'selected':''; ?> value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('state_id'); ?></span>
          </div>
        </div>
      
         <div class="col-sm-6">      
          <div class="form-group">
            <label for="city_id" class="col-md-4 col-sm-5 col-xs-12">City <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <select name="city_id" id="city_id" class="form-control">
              <option></option>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('city_id'); ?></span>
          </div>
        </div>
        </div>

       <div class="row">
          <div class="col-sm-6">   
           <div class="form-group">
            <label for="pincode" class="col-md-4 col-sm-5 col-xs-12">Pincode <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="pincode" name="pincode" class="form-control" placeholder="Enter pincode" value="<?= (isset($edit) && (!empty(($edit->pincode))))?$edit->pincode:set_value('pincode'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('pincode'); ?></span>
          </div>
        </div>

         <div class="col-sm-6">       
          <div class="form-group">
            <label for="contact_person" class="col-md-4 col-sm-3 col-xs-12">Contact Person <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
              <input type="text" id="contact_person" name="contact_person" class="form-control" placeholder="Enter contact person" value="<?= (isset($edit) && (!empty(($edit->contact_person))))?$edit->contact_person:set_value('contact_person'); ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact_person'); ?></span>
          </div>
        </div>
      </div>

      <div class="row">
         <div class="col-sm-6">   
          <div class="form-group">
            <label for="address" class="col-md-4 col-sm-5 col-xs-12">Address <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea id="address" name="address" class="form-control" placeholder="Enter address" ><?= (isset($edit) && (!empty(($edit->address))))?$edit->address:set_value('address'); ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('address'); ?></span>
          </div>
        </div>

         <div class="col-sm-6">   
          <div class="form-group">
            <label for="description" class="col-md-4 col-sm-5 col-xs-12">Description <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea id="description" name="description" class="form-control" placeholder="Enter description" ><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:set_value('description'); ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>
        </div>
      </div>

        <div class="row">
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12">Status</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>

                <label class="btn btn-<?= (isset($edit) && ($edit->status == 0))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="<?php echo base_url('collection_center'); ?>" class="btn btn-danger">Cancel</a>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');

            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
        $('#form_validation1').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    numeric:true,
                    required:true,
                },
                contact2: {
                    numeric:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
            },
            messages:{
                title: "Please enter title",
                hierarchy_id: "Please select designation",
                designation: "Please enter group title",
            },
            // highlight: function (input) {
            //     $(input).parents('.form-line').addClass('error');
            // },
            // unhighlight: function (input) {
            //     $(input).parents('.form-line').removeClass('error');
            // },
            // errorPlacement: function (error, element) {
            //     $(element).parents('.form-group').append(error);
            // }
        });

    });
});
</script>