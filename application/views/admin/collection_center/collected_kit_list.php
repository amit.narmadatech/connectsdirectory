<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <div class="col-md-10">  
            <h2>Collected Kit List</h2>
          </div>
          <div class="col-md-2">  
          <?php $is_damage = empty($this->input->get('st'))?0:$this->input->get('st'); ?>
          <select class="form-control" id="changeStatus">
            <option value="">Select Status</option>
            <option <?php echo ($is_damage == 0)?'selected':''; ?> value="0">OK</option>
            <option <?php echo ($is_damage == 1)?'selected':''; ?> value="1">Damaged</option>
          </select>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th><input type="checkbox" id="changeall"></th>
                <th>#</th>
                <th>Order Number</th>
                <th>Bar Code</th>
                <th>Plan</th>
                <th>Customer</th>
                <th>Email</th>
                <th>
                  <button type="button" class="btn btn-info btn-xs btnModal" >Change Status</button>
                </th>
              </tr>
            </thead>

            <tbody>
             <?php 
               if(!empty($kit_list)){
                  $i=1;
                  foreach($kit_list as $klist){ ?>
                  <tr>
                      <td><input type="checkbox" name="id[]" class="cid" value="<?php  echo $klist->id; ?>"></td>
                      <td><?php  echo $i++; ?></td>
                      <td><?php  echo !empty($klist->order_number) ? $klist->order_number : '' ?></td>
                      <td><?php  echo !empty($klist->barcode) ? $klist->barcode : '' ?></td>
                      <td><?php  echo !empty($klist->plan_name) ? $klist->plan_name : '' ?></td>
                      <td><?php  echo !empty($klist->firstname) ? $klist->firstname : '' ?> <?php  echo !empty($klist->lastname) ? $klist->lastname : '' ?></td>
                      <td><?php  echo !empty($klist->created_at) ? date("D, d M Y", strtotime($klist->created_at)) : '' ?></td>
                      <td><?php  if($klist->status == 1){ echo "Received"; }elseif($klist->status == 2){ echo "Sent to Lab"; } ?></td>
                  </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dispatch Form</h4>
      </div>
      <div class="modal-body">
          
          <div class="form-group">
            <label for="shipping_company">Courier Company</label>
            <input required type="text" class="form-control" id="shipping_company">
          </div>
          <div class="form-group">
            <label for="tracking_code">Tracking Code</label>
            <input required type="text" class="form-control" id="tracking_code">
          </div>
          <div class="form-group">
            <label for="shipping_date">Dispatch Date</label>
            <input required type="date" class="form-control" id="shipping_date">
          </div>
          <div class="form-group">
            <label for="shipping_note">Note</label>
            <textarea required class="form-control" id="shipping_note"></textarea>
          </div>
          
          <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        
        $(document).on('click', '#changeall', function(event) {
            if (this.checked) {
               $('.cid').prop('checked', true);      
            }else{
               $('.cid').prop('checked', false);      
            }
        });

        $(document).on('change', '#changeStatus', function(event) {
          var value = this.value;
          window.location.href = '<?php echo base_url("collection_center/collection_list"); ?>?st='+value;
        });

        $(document).on('click', '.btnModal', function(event) {
            
            if ($("input[name='id[]']:checked").length > 0) {
              $('#myModal').modal('show');
            }else{
              
              alert("Please select atleast one checkbox !");return false;
            }
        });

        $(document).on('click', '.btnSubmit', function(event) {
            
            if ($("input[name='id[]']:checked").length > 0) {

              var shipping_company = $('#shipping_company').val(); 
              var tracking_code = $('#tracking_code').val(); 
              var shipping_date = $('#shipping_date').val();
              var shipping_note = $('#shipping_note').val();

              var allid = [];
              $.each($("input[name='id[]']:checked"), function(){            
                  allid.push($(this).val());
              });

              if (allid.length != 0) {
                $.post('<?php echo base_url("collection_center/change_collected_kit_status"); ?>', {allid: allid,shipping_company: shipping_company,tracking_code: tracking_code,shipping_date: shipping_date,shipping_note: shipping_note, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
                  if (data == 'true') {
                    $('#myModal').modal('hide');
                    alert('Status changed successfully.');
                    window.location.href = "<?php echo base_url('collection_center/collection_list'); ?>";
                  }
                                
                });
              }

            }else{
               alert("Please select atleast one checkbox !");return false;
            }              
        });
      
      });
    </script>