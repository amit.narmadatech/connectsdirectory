<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Collect Kit</h2>        
       <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="form_validation" name="form_validation" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" action="<?= base_url('collection_center/post_collect_kit'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="member_id" value="<?= isset($edit)?$edit->member_id:''; ?>">
        <input type="hidden" name="user_id" value="<?= isset($edit)?$edit->user_id:''; ?>">
        <input type="hidden" name="plan_id" value="<?= isset($edit)?$edit->plan_id:''; ?>">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Barcode <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="barcode" name="barcode" required="required" class="form-control col-md-7 col-xs-12" value="<?= (isset($edit) && (!empty(($edit->barcode))))?$edit->barcode:''; ?>">
              <?php echo form_error('barcode', '<span class="text-danger">', '</span>'); ?>
            </div>
            <div class="col-sm-2">
              <div class="form-group">
                <button id="get_info" class="btn btn-info waves-effect m-t-10" type="button">FIND</button> 
                <a href="<?php echo base_url('collection_center/collect_kit'); ?>" class="btn btn-default waves-effect m-t-10">RESET</a> 
              </div>
            </div>
          </div>
          <br><br>

           
          <!-- <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Plan Name <span class="required">*</span> </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
              <input name="title" value="" class="form-control" type="text">
               <span class="text-danger"></span>
            </div>           
          </div> -->
        

        <div class="row clearfix">
          <div class="col-sm-6">   
            <div class="form-group">
              <label class="col-md-4 col-sm-5 col-xs-12" for="">Customer Name</label>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <?php echo isset($edit)?$edit->firstname:''; ?> <?php echo isset($edit)?$edit->lastname:''; ?>
              </div>           
          </div>
        </div>
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="">Plan</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo isset($plan)?$plan->name:''; ?>
            </div> 
          </div>          
        </div>
      </div>

      <div class="row clearfix">
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="">Email</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo isset($edit)?$edit->email:''; ?>
            </div>
          </div>
        </div>
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="">Plan Price</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo isset($edit)?$edit->plan_price:''; ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row clearfix">
        <div class="col-sm-6">   
          <div class="form-group">           
              <label class="col-md-4 col-sm-5 col-xs-12" for="">Contact</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo isset($edit)?$edit->contact1:''; ?>
            </div>
          </div>
        </div>
     
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Is Damage</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
              <!-- <?php echo isset($edit)?$edit->sort_description:''; ?> -->
              <input type="checkbox" name="is_damage" id="is_damage" value="1">
             </div>
          </div>
        </div>
      </div>
  

    <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">          
            <label class="col-md-4 col-sm-5 col-xs-12" for="">Order No.</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo isset($edit)?$edit->order_number:''; ?>
            </div>
          </div>
        </div>
     
        <div class="col-sm-6">   
          <div class="form-group">         
            <label class="col-md-4 col-sm-5 col-xs-12" for="">Note</label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea name="note" id="note" class="form-control"></textarea>
            </div>
          </div>
        </div>
      </div>
  

    <!-- <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Transaction ID</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
              <?php echo (isset($edit) && (!empty(($edit->txnid))))?$edit->txnid:''; ?>
          </div>
        </div>
      </div>
    </div> -->
   

   <hr>
    

     <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Patient's Name</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
             <input type="text" name="patient_name" id="patient_name" class="form-control" value="<?php echo (isset($edit) && !empty($edit->mem_name))?$edit->mem_name:''; ?>">
          </div>
        </div>
      </div>
       <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Gender</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <?php echo (isset($edit) && !empty($edit->mem_gender))?$edit->mem_gender:''; ?>
            <br><span id="errorGender"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Birth Date</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text"  name="birthdate" id="birthdate" class="form-control" value="<?php echo (isset($edit) && !empty($edit->mem_dob))?$edit->mem_dob:''; ?>">
          </div>
        </div>
      </div>
       <!-- <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Age</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="patient_age" data-inputmask="'mask': '99'" id="patient_age" class="form-control">
           </div>
        </div>
      </div> -->
    </div>

    <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Weight</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="weight" id="weight" data-inputmask="'mask': '999'" class="form-control" value="<?php echo (isset($edit) && !empty($edit->weight))?$edit->weight:''; ?>">
          </div>
        </div>
      </div> 
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Height</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="height" id="height" data-inputmask="'mask': '999'" class="form-control" value="<?php echo (isset($edit) && !empty($edit->height))?$edit->height:''; ?>">
          </div>
        </div>
      </div> 
    </div>

    <div class="row clearfix">
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Email</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="mem_email" id="mem_email" class="form-control" value="<?php echo (isset($edit) && !empty($edit->mem_email))?$edit->mem_email:''; ?>">
          </div>
        </div>
      </div> 
      <div class="col-sm-6">   
        <div class="form-group">         
          <label class="col-md-4 col-sm-5 col-xs-12" for="">Contact</label>
          <div class="col-md-8 col-sm-7 col-xs-12">
            <input type="text" name="mem_contact" id="mem_contact" class="form-control" value="<?php echo (isset($edit) && !empty($edit->mem_contact))?$edit->mem_contact:''; ?>">
          </div>
        </div>
      </div> 
    </div>
   
           <br> 
           <br> 
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url('assets/js/'); ?>jquery.scannerdetection.js"></script>
<script>
    jQuery(document).ready(function($) {

    $( ".kit" ).blur(function() {
        var kitcodes = new Array(); 
        var obj = this;
        var code = $.trim(obj.value);
        if(code){
          $.post("<?php echo base_url('kit/verifykitcode'); ?>",{ code : code, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
              
              var id = $(obj).attr('id'); 

              if(!response){
                 if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Please enter right code.</span>').insertAfter(obj);
                 } 
              }else
              {
                $('input[name="kit[]"]').each(function(index, val) {
                    console.log($(this).id);
                    console.log(val.id);
                    if((val.id != obj.id) && ($(this).val() != "")){
                      kitcodes.push($(this).val());
                    }
                });

                if ($.inArray(code, kitcodes)  == -1) {
                  $(obj).next('span').remove();
                }else{
                  if(!$(obj).next('span').hasClass('error')){
                    $('<span class="error">Please enter right code.</span>').insertAfter(obj);
                    $(obj).val('');
                 }                  
                }

              }
          },"json");
        }else{
          $(obj).next('span').remove();
        }
        
    });


    $('#get_info').click(function(event) {
      var qrcode = $('#barcode').val();
      if (qrcode != "") { window.location.href="<?php echo base_url('collection_center/get_detail_by_qrcode/') ?>"+qrcode; }else{ alert('Please enter some value !'); $('#barcode').focus(); }
    });

    $(document).scannerDetection({
      timeBeforeScanTest: 200, // wait for the next character for upto 200ms
      startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
      endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
      avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
      onComplete: function(barcode, qty){ 

        var barcode = '635416316';

        $.post("<?php echo base_url('collection_center/scan_kit'); ?>",{ barcode : barcode, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
          if(response.status){
            alert('ok');
          }else{
            console.log(response.status);
          }
        },"json");

       } 
    });


    $(function () {
        $('#form_validation').validate({
            submitHandler: function(form) {
              form.submit();
            },
              errorPlacement: function(error, element) {
                if(element.attr("name") == "gender"){
                    error.appendTo('#errorGender');
                    return;
                }else {
                  error.insertAfter(element);
                }
            },  
            rules: {
                patient_name: "required",
                gender: "required",
                birthdate: "required",
                note: {
                  required: function(element){
                      return $('input[name=is_damage]').is(':checked');
                  }
                }
            },
            messages:{
                patient_name: "The patient name is required",
                gender: "The gender field is required",
                birthdate: "The birth date field is required",
                patient_age: "The patient age field is required",
            },
            highlight: function (input) {
                $(input).parents('td').addClass('error');
            },
        });

    });

});
</script>
