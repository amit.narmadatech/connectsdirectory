<?php $this->load->view('admin/include/header'); ?>
  
  	<div class="row">
  		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  	 		<h2>Summary</h2>
  		</div>
 	</div>
  <!-- top tiles -->
 <div class="row">

    <div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-green">
       	 <h4>Number of kit received <span class="pull-right"><?php echo $kit_received_all; ?></span></h4>
			<ul>
				<li> <?php echo $kit_received_month; ?>
					<span>Month</span>
				</li>
				<li> <?php echo $kit_received_week; ?>
					<span>Week</span>
				</li>
				<li> <?php echo $kit_received_today; ?>
					<span>Today</span>
				</li>
			</ul>
      </div>
    </div>

    <div class="admin-box  col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="box bg-red">
      	<h4>Number of kit sent <span class="pull-right"><?php echo $kit_sent_all; ?></span></h4>
        	<ul>
				<li> <?php echo $kit_sent_month; ?>
					<span> Month </span>
				</li>
				<li> <?php echo $kit_sent_week; ?>
					<span> Week </span>
				</li>
				<li> <?php echo $kit_sent_today; ?>
					<span>Today</span>
				</li>
			</ul>
      </div>
    </div>

	<div class="admin-box col-lg-4 col-md-4 col-sm-6 col-xs-12">
  	  <div class="box bg-blue">
        <h4>Total sent kit</h4>
        	<span class="total-kit"> <?php echo $kit_sent_all; ?> </span>
      </div>
    </div>
       
  </div>
  <!-- /top tiles -->


<?php $this->load->view('admin/include/footer'); ?>   