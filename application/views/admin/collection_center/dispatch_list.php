<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <?php $this->load->view('include/alert'); ?>
      <div class="x_panel">
        <div class="x_title">
          <h2>Dispatched Sample List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th>Courier Company</th>
                <th>Tracking Code</th>
                <th>Dispatch Date</th>
                <th>Note</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
             <?php 
               if(!empty($dispatch_list)){
                  $i=1;
                  foreach($dispatch_list as $klist){ ?>
                  <tr>
                      <td><?php  echo $i++; ?></td>
                      <td><?php  echo !empty($klist->shipping_company) ? $klist->shipping_company : '' ?></td>
                      <td><?php  echo !empty($klist->tracking_code) ? $klist->tracking_code : '' ?></td>
                      <td><?php  echo !empty($klist->shipping_date) ? date("D, d M Y", strtotime($klist->shipping_date)) : '' ?></td>
                      
                      <td><?php  echo !empty($klist->shipping_note) ? $klist->shipping_note : '' ?></td>
                      <td>
                        <button type="button" class="btn btn-primary btn-xs showKitList" data-toggle="modal" data-target="#scannedKitList" id="<?php echo $klist->id; ?>">Kit List</button>
                      </td>
                      
                  </tr>
              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Dispatch Form</h4>
      </div>
      <div class="modal-body">
          
          <div class="form-group">
            <label for="shipping_company">Courier Company</label>
            <input required type="text" class="form-control" id="shipping_company">
          </div>
          <div class="form-group">
            <label for="tracking_code">Tracking Code</label>
            <input required type="text" class="form-control" id="tracking_code">
          </div>
          <div class="form-group">
            <label for="shipping_date">Dispatch Date</label>
            <input required type="date" class="form-control" id="shipping_date">
          </div>
          <div class="form-group">
            <label for="shipping_note">Note</label>
            <textarea required class="form-control" id="shipping_note"></textarea>
          </div>
          
          <button type="submit" class="btn btn-primary btnSubmit">Submit</button>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="scannedKitList" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Scanned Kit List</h4>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <tr>
              <th width="5%">Sr.No.</th>
              <th>Barcode</th>
            </tr>
            <tbody id="scanKitArea"></tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {

    $(document).on('click', '.showKitList', function(event) {
          var id = this.id;
          $.post("<?php echo base_url('collection_center/getdispatchedKitList'); ?>", {id: id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
            var data = JSON.parse(data);
            var txt = '';
            var i = 1;
            $.each(data.result, function(index, val) {
               txt += '<tr><td>'+i+++'</td><td>'+val.barcode+'</td></tr>';
            });

            $('#scanKitArea').html(txt);

          });
      });
  
  });
</script>