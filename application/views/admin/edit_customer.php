<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add User</h2>       
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('admindashboard/post_edit_customer'); ?>" enctype="multipart/form-data">
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
        <input type="hidden" name="oldimg" value="<?= isset($edit)?$edit->image:''; ?>">

        <div class="row">
          <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="firstname">First Name <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Enter firstname" value="<?= (isset($edit) && (!empty(($edit->firstname))))?$edit->firstname:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('firstname'); ?></span>
          </div>
        </div>
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="lastname">Last Name <span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Enter  lastname" value="<?= (isset($edit) && (!empty(($edit->lastname))))?$edit->lastname:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('lastname'); ?></span>
          </div>
        </div>     
      </div>
          
       <div class="row">
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="email">Email <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
              <input readonly type="email" id="email" name="email" class="form-control" placeholder="Enter email ID" value="<?= (isset($edit) && (!empty(($edit->email))))?$edit->email:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('email'); ?></span>
          </div>
        </div>
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="contact1">Contact Number<span class="required">*</span>
            </label>
           <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="contact1" name="contact1" class="form-control" placeholder="Enter contact number" value="<?= (isset($edit) && (!empty(($edit->contact1))))?$edit->contact1:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact1'); ?></span>
          </div>
        </div>     
      </div>

       <div class="row">
        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="password">Password <span class="required">*</span>
            </label>
             <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="password" id="password" name="password" class="form-control" placeholder="Enter password" value="<?= (isset($edit) && (!empty(($edit->password))))?'':''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('password'); ?></span>
          </div>
        </div>
                
      </div>

       <div class="row">
            <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="profile_image">Profile Image <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="file" name="profile_image" id="profile_image" class="form-control" placeholder="Enter profile image" value="<?= (isset($edit) && (!empty(($edit->profile_image))))?$edit->profile_image:''; ?>" onchange="previewImage(this,'profile_display');">
            </div>
            <span class="text-danger"><?php echo form_error('profile_image'); ?></span>
          </div>
        </div>
         <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12"  for="pincode">Pincode <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <input type="text" id="pincode" name="pincode" class="form-control" placeholder="Enter pincode" value="<?= (isset($edit) && (!empty(($edit->pincode))))?$edit->pincode:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('pincode'); ?></span>
          </div>
        </div>

      </div>
      <div class="row">
            <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12" for="address">Address <span class="required">*</span>
            </label>
            <div class="col-md-8 col-sm-7 col-xs-12">
              <textarea id="address" name="address" class="form-control" placeholder="Enter address" ><?= (isset($edit) && (!empty(($edit->address))))?$edit->address:''; ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('address'); ?></span>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-sm-6">   
          <div class="form-group">
            <label class="col-md-4 col-sm-5 col-xs-12"><?= (isset($edit) && ($edit->status == 1))?'Active':'Inactive'; ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'default':'primary'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-danger" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script>
    jQuery(document).ready(function($) {
        
        $(function () {
          $('#form_validation').validate({
              submitHandler: function(form) {
                  form.submit();
              },
              ignore: [],
              rules: {
                  firstname: "required",
                  lastname: "required",
                  email: {
                      email:true,
                      required:true,
                  },
                  contact1: {
                      number:true,
                      required:true,
                  },
                  /*pincode: {
                      number:true,
                      required:true,
                  },*/
                  //address: "required",
                  password : {
                    minlength : 8
                  }

              },
              messages:{
                  firstname: "The first name field is required",
                  lastname: "The last name field is required",
                  email:{
                    email: "The email field must contain a valid email address",
                    required: "The email field is required",
                  },
                  contact1: {
                    number: "The mobile number field is not in the correct format",
                    required: "The mobile number field is required",
                  },
                  pincode: {
                    number: "The pincode field is not in the correct format",
                    required: "The pincode field is required",
                  },
                  address: "The address field is required",
              },
              // highlight: function (input) {
              //     $(input).parents('.form-line').addClass('error');
              // },
              // unhighlight: function (input) {
              //     $(input).parents('.form-line').removeClass('error');
              // },
              // errorPlacement: function (error, element) {
              //     $(element).parents('.form-group').append(error);
              // }
          });

      });

  

  });
</script>