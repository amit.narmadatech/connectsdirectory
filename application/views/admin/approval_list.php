<?php $this->load->view('admin/include/header'); ?>

 <!-- Datatables -->
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">

      <div class="x_panel">
        <div class="x_title">
          <h2>Approval List</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable-buttons" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Reference No.</th>
                <th>Warehouse</th>
                <th>Supplier</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Created Date</th>
                <th>Status</th>
                <th>View</th>
                <th>Action</th>
              </tr>
            </thead>


            <tbody>
              <?php 
             if(!empty($approval_list)){
                
                foreach($approval_list as $aplist){ ?>
                <tr>
                    <td><?php  echo !empty($aplist->reference_no) ? $aplist->reference_no : '' ?></td>
                    <td><?php  echo !empty($aplist->warehouse_name) ? $aplist->warehouse_name : '' ?></td>
                    <td><?php  echo !empty($aplist->supplier_name) ? $aplist->supplier_name : '' ?></td>
                    <td><?php  echo !empty($aplist->product_name) ? $aplist->product_name : '' ?></td>
                    <td><?php  echo !empty($aplist->product_qty) ? $aplist->product_qty : '' ?></td>
                    <td><?php  echo !empty($aplist->created_at) ?date("D, d M Y",strtotime($aplist->created_at)): '' ?></td>
                    <td>
                      <?php 
                      if ($user->head == 'bh') {
                        if ($aplist->bh_approval == 0) {
                          echo "Pending";
                        }elseif ($aplist->bh_approval == 1) {
                          echo "Approved";
                        }elseif ($aplist->bh_approval == 2) {
                          echo "Rejected";
                        }
                      }elseif ($user->head == 'fh') {
                        if ($aplist->fh_approval == 0) {
                          echo "Pending";
                        }elseif ($aplist->fh_approval == 1) {
                          echo "Approved";
                        }elseif ($aplist->fh_approval == 2) {
                          echo "Rejected";
                        }
                      }elseif ($user->head == 'ceo') {
                        if ($aplist->ceo_approval == 0) {
                          echo "Pending";
                        }elseif ($aplist->ceo_approval == 1) {
                          echo "Approved";
                        }elseif ($aplist->ceo_approval == 2) {
                          echo "Rejected";
                        }
                      } 

                      ?>
                    </td>
                    <td>
                      <a href="<?php echo base_url('inventory/get_po_invoice/'.$aplist->id);?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>
                    </td>
                    <td>
                      <?php if ((($aplist->bh_approval == 0) && ($user->head == 'bh')) || (($aplist->fh_approval == 0) && ($user->head == 'fh')) || (($aplist->ceo_approval == 0) && ($user->head == 'ceo'))) { ?>
                      <button id="<?php echo $aplist->id; ?>" type="button" class="btn btn-success btn-xs approve">Approve</button>
                      <button id="<?php echo $aplist->id; ?>" type="button" class="btn btn-danger btn-xs reject" data-toggle="modal" data-target=".bs-example-modal-sm">Reject</button>
                      <?php } ?>
                    </td>
                </tr>
            <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Reject</h4>
          </div>
          <form method="post" action="<?php echo base_url('inventory/reject_purchase_request'); ?>">
            <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
          <input type="hidden" name="request_id" id="request_id">
          <div class="modal-body">
            <label for="reason">Description</label>
            <textarea name="reason" class="form-control" required></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default reset" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
          </form>

        </div>
      </div>
    </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url('assets/admin_theme/'); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        
        $('.reject').click(function(event) {
          var id = this.id;
          $('#request_id').val(id);
        });

        $('.reset').click(function(event) {
          $('#request_id').val('');
        });

        $('.approve').click(function(event) {
          var id = this.id;
          bootbox.confirm({
              message: "Are you sure ?",
              buttons: {
                  confirm: {
                      label: 'Yes',
                      className: 'btn-success'
                  },
                  cancel: {
                      label: 'No',
                      className: 'btn-danger'
                  }
              },
              callback: function (result) {
                  if (result){
                      $.post("<?php echo base_url('inventory/approve_purchase_request'); ?>",{ request_id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){

                          window.location.href ="<?php echo base_url('inventory/approval_list'); ?>";
                          
                      },"json");
                  }
              }
          });
      });

      });
    </script>