<?php $this->load->view('admin/include/header'); ?>
    <section>
        <!-- Left Sidebar -->
		<?php $this->load->view('admin/include/leftsidebar'); ?>
        <!-- #END# Left Sidebar -->
    </section>    
    <section class="content">
        <div class="container-fluid">
            <!-- START ALERT MESSAGE -->
            <?php $this->load->view('include/alert'); ?>
            <!-- END ALERT MESSAGE -->
            <div class="block-header">
                <h2>NOTIFICATION TEMPLATE</h2>
            </div>
            <!-- Input -->
			 <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2>Change Notification Template</h2>
                                </div>
                                
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <form id="form_validation" name="form_validation" method="post" action="<?= base_url('template/update_notification_template'); ?>" >
                                <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                                    <input type="hidden" name="edit_id" value="<?= isset($group)?$group->id:''; ?>">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="hierarchy_id">Notification Type</label>
                                                <select name="type" id="type" class="form-control">
                                                    <option></option>
                                                    <?php
                                                    foreach ($notification_list as $nflist) { ?>
                                                    <option <?= (isset($group) && ($group->id == $nflist->id))?'selected':''; ?> value="<?= $nflist->id ?>"><?= $nflist->label ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('type'); ?></span>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="title">Title</label>
                                                <input type="text" name="title" id="title" class="form-control" placeholder="Enter title" value="<?= (isset($group) && (!empty(($group->title))))?$group->title:''; ?>">
                                            </div>
                                            <span class="text-danger"><?php echo form_error('title'); ?></span>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="body">Notification Body</label>
                                                <textarea name="body" id="body" placeholder="Enter body" cols="30" rows="5" class="form-control no-resize"><?= (isset($group) && !empty(($group->body)))?$group->body:''; ?></textarea>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('body'); ?></span>
                                        </div>

                                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="demo-switch">
                                            <div class="row clearfix">
                                                <div class="body table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Key</th>
                                                                <th>Description</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="available_tokens">
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </section>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/bootstrap/js/bootstrap.js"></script>
	 <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <!-- Slimscroll Plugin Js -->
   <script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>plugins/node-waves/waves.js"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>js/admin.js"></script>
    <!-- Demo Js -->
    <script src="<?php echo base_url('assets/admin/'); ?>js/demo.js"></script>
</body>
</html>	

<script>
	$(function () {
		$('#form_validation').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
			rules: {
                type: "required",
				title: {
                  required: true,
                  minlength: 15
                },
				body: {
                  required: true,
                  minlength: 30
                },
			},
			messages:{
                type: "Please choose a notification template",
				title: {
                required :"Please enter notification title",
                minlength: "The notification title field must be at least 15 characters in length",               
                },
				body: {
                required :"Please enter notification text",
                minlength: "The notification text field must be at least 30 characters in length",               
                },
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
        });

        $(document).on('change', '#type', function(event) {
            var id = $('#type').val();
            if(id ==''){ return false; }
            $.ajax({
              url:"<?php echo base_url('template/get_notification_token')?>",
              type:"POST",
              data: {id:id},
              dataType:"json",
              success: function(response){
                    if(response.status == true){
                        var token_list = '';    
                        console.log(response);
                        var i = 1;
                        $.each(response.data, function(index, val) {
                              var href = (val.href != '')?val.href:'<?php echo base_url("notification"); ?>';
                              token_list += '<tr><th scope="row">'+i+'</th><td>'+index+'</td><td>'+val+'</td></tr>';
                              i=i+1;
                         });
                                                  
                        $('#available_tokens').html(token_list);
                        $('#title').val(response.title);
                        $('#body').val(response.body);
                    }
                } 
            });
        });


    });
</script>
