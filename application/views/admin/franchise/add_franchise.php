<?php $this->load->view('admin/include/header'); ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Franchise</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="" method="post" id="form_validation" name="form_validation" action="<?= base_url('franchise/post_franchise_form'); ?>" >
          <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
        <input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">

          <div class="form-group">
            <label for="title" class="control-label col-md-3 col-sm-3 col-xs-12">Title <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="title" name="title" class="form-control col-md-7 col-xs-12" placeholder="Enter title" value="<?= (isset($edit) && (!empty(($edit->title))))?$edit->title:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('title'); ?></span>
          </div>

          <div class="form-group">
            <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Contact Person <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="name" name="name" class="form-control col-md-7 col-xs-12" placeholder="Enter name" value="<?= (isset($edit) && (!empty(($edit->name))))?$edit->name:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('name'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="Enter email ID" value="<?= (isset($edit) && (!empty(($edit->email))))?$edit->email:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('email'); ?></span>
          </div>

          <div class="form-group">
            <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" placeholder="Enter password" value="<?= (isset($edit) && (!empty(($edit->password))))?'':'123456'; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('password'); ?></span>
          </div>

          <div class="form-group">
            <label for="contact1" class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="contact1" name="contact1" class="form-control col-md-7 col-xs-12" placeholder="Enter contact number" value="<?= (isset($edit) && (!empty(($edit->contact1))))?$edit->contact1:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact1'); ?></span>
          </div>

          <div class="form-group">
            <label for="contact2" class="control-label col-md-3 col-sm-3 col-xs-12">Alternate Contact No. <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="contact2" name="contact2" class="form-control col-md-7 col-xs-12" placeholder="Enter Alternate contact number" value="<?= (isset($edit) && (!empty(($edit->contact2))))?$edit->contact2:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('contact2'); ?></span>
          </div>
          
          <div class="form-group">
            <label for="state_id" class="control-label col-md-3 col-sm-3 col-xs-12">State <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="state_id" id="state_id" class="form-control">
                  <option></option>
                  <?php if (!empty($state_list)) {
                      foreach ($state_list as $state) { ?>
                  <option <?= (isset($edit) && ($edit->state_id == $state->id))?'selected':''; ?> value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>        
                     <?php }
                  } ?>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('state_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="city_id" class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="city_id" id="city_id" class="form-control">
              <option></option>
              </select>
            </div>
            <span class="text-danger"><?php echo form_error('city_id'); ?></span>
          </div>

          <div class="form-group">
            <label for="pincode" class="control-label col-md-3 col-sm-3 col-xs-12">Pincode <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="pincode" name="pincode" class="form-control col-md-7 col-xs-12" placeholder="Enter pincode" value="<?= (isset($edit) && (!empty(($edit->pincode))))?$edit->pincode:''; ?>">
            </div>
            <span class="text-danger"><?php echo form_error('pincode'); ?></span>
          </div>

          <div class="form-group">
            <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea id="address" name="address" class="form-control col-md-7 col-xs-12" placeholder="Enter address" ><?= (isset($edit) && (!empty(($edit->address))))?$edit->address:''; ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('address'); ?></span>
          </div>

          <div class="form-group">
            <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea id="description" name="description" class="form-control col-md-7 col-xs-12" placeholder="Enter description" ><?= (isset($edit) && (!empty(($edit->description))))?$edit->description:''; ?></textarea>
            </div>
            <span class="text-danger"><?php echo form_error('description'); ?></span>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= (isset($edit) && ($edit->status == 1))?'Active':'Inactive'; ?></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'primary':'default'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 1))?'checked':'';} ?> name="status" id="status" type="radio" value="1"> &nbsp; Active &nbsp;
                </label>
                <label class="btn btn-<?= (isset($edit) && ($edit->status == 1))?'default':'primary'; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input <?php if(!isset($edit)){ echo 'checked'; }else{ echo (isset($edit) && ($edit->status == 0))?'checked':'';} ?> name="status" id="status" type="radio" value="0"> Inactive
                </label>
              </div>
            </div>
          </div>
          <div class="ln_solid"></div>

          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
              <button class="btn btn-primary" type="button">Cancel</button>
            </div>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
        
    $( "#state_id" ).change(function() {
        $.post("<?php echo base_url('home/getcity'); ?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
            if(response.status){
              $('#city_id').html('');      
              $('#city_id').html(response.result); 
              $('#city_id').selectpicker('refresh');
              $('#city_id').val('<?php echo (isset($edit))?$edit->city_id:""; ?>');
              $('#city_id').selectpicker('refresh');
            }else{
              console.log(response.status);
              console.log(response.result);
            }
        },"json");
        
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

  $(function () {
    $('#form_validation1').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
      rules: {
                title: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    numeric:true,
                    required:true,
                },
                contact2: {
                    numeric:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
      },
      messages:{
        title: "Please enter title",
        hierarchy_id: "Please select designation",
        designation: "Please enter group title",
      },
      highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
      },
      unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
      },
      errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
      }
        });

    });

});
</script>