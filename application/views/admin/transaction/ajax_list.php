<?php
$i = 0;
foreach ($transaction_list as $transaction):
    $i++;
    ?>
    <tr>
        <td><?php echo $transaction['fullname']; ?></td>
        <td><?php echo $transaction['mobile1']; ?></td>
        <td><?php echo date('d-m-Y h:m:s', strtotime($transaction['order_date'])); ?></td>
        <td><?php echo $transaction['order_number']; ?></td>
        <td><?php echo $transaction['txnid']; ?></td>
        <td><?php echo $transaction['total_amount']; ?></td>
        <td><?php echo !empty(getState($transaction['state_id'])) ? getState($transaction['state_id'])->state_title : ''; ?></td>
        <td> <?php echo !empty(getDistrict($transaction['district_id'])) ? getDistrict($transaction['district_id'])->district_title : ''; ?></td>
        <td><?php echo !empty($transaction['city']) ? $transaction['city'] : ''; ?></td>
        <td><?php echo $transaction['payment_type']; ?></td>
     <?php
        if ($transaction['status']) {
            $label = 'label-success';
            $status = 'Success';
        }else {
            $label = 'label-primary';
            $status = 'Failure';
        }
        ?>
        <td><span class="label <?php echo $label ?> label-flat label-xs"><?php echo $status ?></span></td>
        <td class="text-right">
            <?php if ($transaction['status']) { ?>
                <a href="<?php echo base_url('transaction/invoice/'.$transaction['id']); ?>" class="btn btn-info btn-flat view-user"> Invoice  </a>
            <?php } ?>
        </td>
    </tr>
<?php endforeach; ?>
<tr>
    <td colspan="11">
        <?php echo $pagination; ?>
    </td>
</tr>    

