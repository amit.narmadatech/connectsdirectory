<!-- Datatable style -->
<?php $this->load->view('admin/include/header'); ?>
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
         <div class="x_title">
        <div class="row">

            <div class="col-md-3">
              <h2>  Transaction Summary </h2>
            </div>
            <div class="col-md-9">
                <form  method="post" action="" id="reportSerachForm"  >
                    <div class="row">
                        <div class="col-sm-8">
                    <div class="form-group">
                       
                        <input type="text" name="date_filter" class="form-control"  id="filter_date" autocomplete="off" value="<?php echo $this->input->post('date_filter') ? $this->input->post('date_filter') : ''; ?>" placeholder="Filter Date" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary searchbtn" name="submit" value="search"><i class="fa fa-filter"></i> Search</button>
                    <button type="submit" class="btn btn-primary exportbtn" name="submit" value="export"><i class="fa fa-download"></i> Export to CSV</button>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                </form>
            </div>
        </div>
            </div>
            </div>
       
          </div>
        </div>
        <div class="x_content">

            <div class="table-responsive">
             <table id="example1" class="table table-bordered table-striped ">
                <thead>
                    <tr>
<!--                        <th>S.NO.</th>
                        <th>User Id</th>-->
                        <th>User Name </th> 
                        <th>Mobile Number</th>  
                        <th>Date</th>
                        <th>Order Number</th>
                        <th>Transaction Id</th>
                        <th>Amount</th>
                        <th>State</th>   
                        <th>District</th>   
                        <th>City</th> 
                        <th>Pay Mode</th> 
                        <th>Pay Status</th>       
                        <th style="width: 150px;" class="text-right">Action</th>
                    </tr>
                </thead>
                <tbody>                
                    <?php $this->load->view('admin/transaction/ajax_list'); ?> 
                </tbody>
            </table>
        </div>
 
        </div>
    </div>
</div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/admin/plugins/daterangepicker/moment.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker.css">  
<script src="<?php echo base_url() ?>assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        //$("#example1").DataTable();
//        $('#filter_date').on('apply.daterangepicker', function (ev, picker) {
//            //ajaxSearch();
//        });

        $('#filter_date').daterangepicker({
            "showDropdowns": true,
            timePicker: true,
            autoUpdateInput: false,

            locale: {
                format: 'YYYY/MM/DD hh:mm:ss',
                cancelLabel: 'Clear'
            }
        });

        $('#filter_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD hh:mm:ss') + ' - ' + picker.endDate.format('YYYY/MM/DD hh:mm:ss'));
        });

    });
</script> 
<script>
    $("#view_users").addClass('active');
    var t_url = '<?php echo base_url('transaction/index') ?>';
    $(function () {
        $("body").on('click', '.searchbtn', function (e) {
            e.preventDefault();
            ajaxSearch();
        });
        $('body').on('click', '.pagination li', function (e) {
            e.preventDefault();
            t_url = $(this).find('a').attr('href');
            ajaxSearch();
        });
        function ajaxSearch() {
            $.ajax({
                url: t_url,
                method: 'post',
                data: $('#reportSerachForm').serialize(),
                onRequest: function () {
                    if (loadMore != 1) {
                        $('#userListContainerLoop').html('<div class="loader"><img src=\"/templates/shaper_varsita/images/vm-loader.svg"></div>');
                    }
                },
                success: function (response) {
                    var data = JSON.parse(response);
                    $('#example1 tbody').html(data.html);
                }
            });
        }
    });
</script>    

