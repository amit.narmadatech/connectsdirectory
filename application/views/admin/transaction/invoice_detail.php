<?php $this->load->view('admin/include/header'); ?>

<style type="text/css">
  .invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
<div class="row">
  <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
    <div class="x_panel">
      <div class="x_title">
        <div class="col-md-6">
            <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="image"> 
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="container">
          <div class="row">
              <div class="col-xs-12">
              <div class="invoice-title">
                <h2>Invoice</h2><h3 class="pull-right">Order # <?php  echo !empty($orders->order_number) ? $orders->order_number : '' ?></h3>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-8 text-left">
               <address>
                   <h4><strong>Shipped To:</strong></h4>
                 <?php  echo !empty($orders->name) ? $orders->name : '' ?><br>
                 <?php  echo !empty($orders->email) ? $orders->email : '' ?><br>
                 <?php  echo !empty($orders->address) ? $orders->address : '' ?> , <?php  echo !empty($orders->landmark) ? $orders->landmark : '' ?> , 
                  <?php echo !empty(getDistrict($orders->district_id)) ? getDistrict($orders->district_id)->district_title : ''; ?>,
                  <?php echo !empty(getState(getDistrict($orders->district_id)->state_id)) ? getState(getDistrict($orders->district_id)->state_id)->state_title : ''; ?>,
                  <?php  echo !empty($orders->city) ? $orders->city : '' ?>
                  <?php  echo !empty($orders->pincode) ? $orders->pincode : '' ?><br>
               </address>
             </div>
                <div class="col-sm-4">
                  <address>
                       <h4><strong>Order Details:</strong></h4>   
                      <strong>Date: </strong>
                      <?php echo!empty($orders->order_date) ? date("D d M Y", strtotime($orders->order_date)) : '' ?><br>
                      <strong>Transaction ID #</strong>
                      <?php echo!empty($orders->txnid) ? $orders->txnid : '' ?><br>   
                      <strong>Payment Mode:</strong>
                      <?php echo!empty($orders->payment_mode) ? $orders->payment_mode : ""; ?><br>
                  </address>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <?php if(!empty($orderDetails)){ ?>
                    
                      <div class="table-responsive">
                    <table class="table table-condensed">
                      <thead>
                          <tr>
                            <th>Image</th>
                            <th>Plan Name</th>
                            <th>Price / Quantity </th>
                            <th>Tax</th> 
                            <th>Tax Amount </th> 
                            <th>Total Amount</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php
                        $total = 0; 
                        foreach ($orderDetails as $odetail) {
                            
                             $amount = ($odetail->quantity * $odetail->plan_price); 
                            ?>
                        <tr>
                          <td>
                            <img src="<?php echo base_url("uploads/plan_img/$odetail->plan_image"); ?>" class="img-responsive" width="120" height="80" title="as">
                          </td>
                          <td><?php  echo !empty($odetail->name) ? $odetail->name : '' ?></td>
                          <td> <i class='fa fa-inr'></i> <?php  echo !empty($odetail->plan_price) ? $odetail->plan_price : '' ?> * <?php  echo !empty($odetail->quantity) ? $odetail->quantity : '' ?></td>
                          <td>
                             <?php 
                             if($orders->state_id == $odetail->pck_state_id) {
                                 
                                    $taxprnct = ($odetail->cgst_tax + $odetail->sgst_tax);
                                    $taxamt = ($amount * $taxprnct)/100;
                                    $total_amount = ($amount+$taxamt);
                                 ?>
                                   CGST  <?php echo $odetail->cgst_tax . '%';?><br> SGST <?php  echo $odetail->sgst_tax .'%';?>
                             <?php }else { 
                                 
                                    $taxprnct = $odetail->igst_tax ;
                                    $taxamt = ($amount * $taxprnct)/100;
                                    $total_amount = ($amount+$taxamt);
                                 ?>
                                   IGST <?php echo $odetail->igst_tax. '%';?>
                             <?php }  ?>
                          </td> 
                          <td><i class='fa fa-inr'></i> <?php echo !empty($taxamt)? $taxamt : 0; ?></td> 
                          <td><i class='fa fa-inr'></i> <?php echo $total_amount; ?></td>
                          
                        </tr>
                       <?php } ?>
                        <tr>
                            <td class="text-right" colspan="5"><strong>Grand Total</strong> &nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><i class='fa fa-inr'></i>  <?php echo!empty($orders->total_amount) ? $orders->total_amount : ''; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                    <?php }else{?>
                      <div class="alert alert-danger">No record found</div> 
                    <?php }?>
                </div>
              </div>
            </div>
          </div>
      </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/include/footer'); ?>
<script>
    jQuery(document).ready(function($) {
    var itemArray = new Array();
    $("#add_row").click(function(event) {
        var id = $("#product_list").val();
        if (id == '') { alert('Please first choose product !');$("#product_list").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_product_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                  var html = '<tr id="row_'+response.data.id+'"><td></td><td>'+response.data.code+'-'+response.data.name+'<input type="hidden" name="product[]" value="'+response.data.id+'"></td><td style="width:175px;"><input type="number" class="changeQty form-control" id="'+response.data.id+'" value="1" min="1" name="quantity[]"></td><td><span id="rateText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="rate_'+response.data.id+'" name="rate[]" value="'+response.data.purchase_price+'"></td><td><span id="amountText_'+response.data.id+'">'+response.data.purchase_price+'</span><input type="hidden" id="amount_'+response.data.id+'" name="amount[]" value="'+response.data.purchase_price+'"></td><td><button type="button" class="removeItem btn btn-inverse btn-xs waves-effect m-t-10" id="'+response.data.id+'">X</button></td>';
                  itemArray.push(response.data.id);
                  $("#item_table").append(html);
                  $('#grandTotalText').html(response.data.purchase_price);
                  $('#grand_total').val(response.data.purchase_price);
                  console.log(html);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $("#warehouse_id").change(function(event) {
        var id = $("#warehouse_id").val();
        if (id == '') { $("#warehouse_id").focus();  return false; }
        if(jQuery.inArray(id,itemArray) != 0){
            $.post("<?php echo base_url('inventory/get_warehouse_by_id'); ?>",{ id : id, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                if(response.status){
                    return false;                  
                  $("#whName").html(response.data.title);
                  $("#whAddress").html(response.data.address);
                  //$("#whCity").html(response.data.);
                  //$("#whState").html(response.data.);
                  $("#whPincode").html(response.data.pincode);
                }else{
                  console.log(response);
                }
            },"json");
        }
    });

    $(document).on('click', '.removeItem', function(event) {
        var id = this.id;
        itemArray.splice( $.inArray(id, itemArray), 1 );
        $('#row_'+id).remove();
    });

    $(document).on('change', '.changeQty', function(event) {
        var id = this.id;
        var qty = parseFloat(this.value);
        var rate = parseFloat($('#rate_'+id).val());
        var amt = 0;
        if( (!isNaN(qty)) && (!isNaN(rate))){
            amt = qty*rate;
            $('#amountText_'+id).html(amt); 
            $('#amount_'+id).val(amt);
            $('#grandTotalText').html(amt); 
            $('#grand_total').val(amt);
        }
    });
    
    var state_id = $("#state_id").val();
    if(state_id){
       $('#state_id').trigger("change");
    }

    $(function () {
        $('#form_validation1').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                title: "required",
                contact_person: "required",
                email: {
                    email:true,
                    required:true,
                },
                contact1: {
                    numeric:true,
                    required:true,
                },
                contact2: {
                    numeric:true,
                    required:true,
                },
                state_id: "required",
                city_id: "required",
                address: "required",
            },
            messages:{
                title: "Please enter title",
                hierarchy_id: "Please select designation",
                designation: "Please enter group title",
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });

    });

});
</script>
