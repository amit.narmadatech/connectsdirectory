<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
	<div class="banneroverlay"></div>
    <div class="container">
    	<div class="row">
    		<div class="col-md-12 area_1">
       		  <h2 class="const_title"> Precision medicine </h2>    
   			</div>
     	</div>
    </div>
</section>
<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h4> THROUGH PHARMACOGENETICS (PGX)</h4>
			<h5>Personalized Medicine is a medical model using molecular profiling technologies to tailor the right therapeutic strategy for the right person at the right time, and determine the predisposition to disease at the population level and to deliver timely and stratified prevention</h5>
			<h5>In the near future, knowing your Drug panel results will be like knowing your blood type</h5>				
		</div>
	</div>
	<h3>ISSUES WITH </h3>
	<h4>ONE - FOR - ALL MEDICINE</h4>		
	<div class="row"> 
		<div class="col-sm-6">
			<h2>4th</h2>
			<p>Adverse drug events are the 4th leading cause of death in America; and genetics are a leading cause of adverse drug events</p>
		</div>
		<div class="col-sm-6">
			<h2>75%</h2>
			<p>Over 75% of patients have detectable variations in their DNA affecting how they metabolize many commonly prescribed medications</p>
		</div>
		<div class="col-sm-6">
			<h2>100+</h2>
			<p>More than 100 drugs carry product label FDA guidance about pharmacogenetic testing</p>
		</div>
		<div class="col-sm-6">
			<h2>25%</h2>
			<p>CYP2D6 is responsible for processing about one quarter of all cytochrome metabolized drugs</p>
		</div>
	</div>
	<h4>PERCENTAGE OF PATIENT POPULATION FOR WHICH A PARTICULAR DRUG IS INEFFECTIVE (ON AVERAGE)</h4>
		<div class="row">
			<div class="col-sm-2">
				Anti-Depressants (SSRIs)
				<h3>38%</h3>
			</div>
			<div class="col-sm-2"> 
				Asthama Drugs
				<h3>40%</h3>
			</div>
			<div class="col-sm-2"> 
				Diabetes Drugs
				<h3>43%</h3>
			</div>
			<div class="col-sm-2"> 
				Arthritis Drugs
				<h3>50%</h3>
			</div>
			<div class="col-sm-2"> 
				Alzhemer’s Drugs
				<h3>70%</h3>
			</div>
			<div class="col-sm-2"> 
				Cancer Drugs
				<h3>75%</h3>
			</div>
		</div>
		<h3>HOW CYTOCHORME P450 GENE VARIANTS </h3>
		<h4>ALTER ENZYME FUNCTION & DRUG META BOLISM</h4>
		<div class="row">
			<div class="col-sm-3">
				<h5>POOR METABOLIZER </h5>
				<p>There are two variants present. Typically one is of the maternal-derived chromosome and a second variant on the pair is of the paternal-derived chromosome. The allele on both chromosomes produce “ loss - of - function ” enzymes. Because it is not metabolized properly, too much medication remains in the blood stream, potentially causing ADRs.</p>
			</div>
			<div class="col-sm-3">
				<h5>INTERMEDIATE METABOLIZER</h5>
				<p>There is only one variant present, located either on
				the maternal-derived chromosome or on the paternal- derived pair chromosome. </p>
				<p>The variant allele produces “loss-of-function” enzymes, while the ordinary allele on the paired chromosome generates normal enzyme.</p>
			</div>
			<div class="col-sm-3">
				<h5>NORMAL METABOLIZER</h5>
				<p>No variant alleles present on either of the patient’s paired chromosomes. </p>
				<p>All enzymes produced by these alleles have normal activity.</p>
			</div>
			<div class="col-sm-3">
				<h5>ULTRA-RAPID METABOLIZER </h5>
				<p>In CYP2C19, the presence of single *17 variant causes a slight increase in CYP2C19 enzyme activity.</p>
				<p>In CYP2D6, the presence of a *2 variant in some patients exists in multiple  copies on the same DNA strand. This results in an increase in total CYP2D6 enzyme activity.</p>
			</div>			
		</div>
		<h3>WHAT IS PHARMACOGENETICS? </h3>
		<div class="row">
			<div class="col-sm-4">
				<p>Pharmacogenetics is the study of genetic variation that gives rise to differing response of drugs.</p>
				<p>When it comes to medications,</p>
				<h5>ONE SIZE DOES NOT FIT ALL </h5>
			</div>
			<div class="col-sm-8"> 
				<p>Blockbuster one-size-fits-all approach to drug development and prescription </p>
				<p>Personalized Prescription enables selection of the right drug and dose</p>
			</div>			
		</div>
		<h4>ADVANTAGES OF <br> PHARMACOGENOMIC TESTING</h4>
		<ul>
			<li>Prescribe a drug regimen with a greater probability of a positive outcome</li>
			<li>Determine a patient's unique drug metabolizing capacity</li>
			<li>Help predict how a patient will respond to a specific drug</li>
			<li>Customize treatment plans for patients</li>
			<li>Suggest safer doses and/or alternatives</li>
			<li>Minimize adverse side effects</li>
			<li>Reduce treatment failures</li>
			<li>Save patient time and money</li>
			<li>Improve patient care</li>
			<li>Prescribe with more confidence</li>
		</ul>
		<h3>WHICH INDIVIDUALS MIGHT BENEFIT FROM <br> PHARMACOGENOMIC TESTING?</h3>
		<ul>
			<li>Those taking multiple medications</li>
			<li>Those whose current prescriptions are not working</li>
			<li>Those on complex drug treatment plans</li>
			<li>Those with previous adverse drug reactions (ADRs) to medications</li>
			<li>Those prescribed any of the FDA-approved drugs where 			pharmacogenomic information is included in black box warning of the drug/medication.</li>
		</ul>
		<h4>WHAT ARE THE POSSIBLE TEST RESULTS ?</h4>
		<ul>
			<li>Extensive (normal) Metabolizer</li>
			<li>Intermediate Metabolizer</li>
			<li>Poor Metabolizer</li>
			<li>Ultra-rapid Metabolizer</li>			
		</ul>
		<h4>THE PHARMACOGENETIC TEST IS HASSLE-FREE, NON-INVASIVE, AND SALIVA-BASED</h4>
	<div class="row genetic-test">
		<div class="col-sm-4"> 
			<div class="heading">Glimepiride</div>
			<ul>
				<li>Glipizide</li>
				<li>Glyburide</li>
				<li>Metformin</li>
				<li>Sulfonylureas</li>
				<li>Tolbutamide</li>
			</ul>
			<div class="heading">Antihypertensive</div>
			<ul>
				<li>Atenolol</li>
				<li>Bisoprolol</li>
				<li>Carvedilol</li>
				<li>Metoprolol</li>
			</ul>
			<div class="heading">Cardiology	</div>
			<ul>
				<li>Atorvastatin</li>
				<li>Clopidogrel</li>
				<li>Lovastatin</li>
				<li>Nifedine</li>
				<li>Simvastatin</li>
				<li>Verapamil</li>
				<li>Warfarin</li>
				<li>Prasugrel</li>
				<li>Aspirin</li>
			</ul>
			<div class="heading">Gastrointestinal</div>
			<ul>
				<li>Esomeprazole</li>
				<li>Omeprazole</li>
				<li>Rabeprazole</li>
			</ul>
			<div class="heading">Immunosuppressant</div>
			<ul>
				<li>Azathiprine</li>
				<li>Cyclosporine</li>
				<li>Mercaptopurine</li>
				<li>Tacrolimus</li>
				<li>Thioguanine</li>
			</ul>
			<div class="heading">Neuropsychiatric</div>
			 <ul>
			 	<li>Alprazolam</li>
			 	<li>Atomoxetine</li>
			 </ul>
		</div>
		<div class="col-sm-4"> 
		 <ul>
			<li>Buspirone</li>
			<li>Clozapine</li>
			<li>Dextroamphetamine</li>
			<li>Diazepam</li>
			<li>Escitalopram</li>
			<li>Eszopiclone</li>
			<li>Paroxetine</li>
			<li>Phenytoin</li>
		</ul>
		<div class="heading">Oncology</div>
		  <ul>
			<li>Tamoxifen</li>			 
		</ul>
		<div class="heading">Pain: opiods</div>	
			<ul>
				<li>Carisoprodol</li>
				<li>Celecoxib</li>
				<li>Codeine</li>
				<li>Cyclobenzaprine</li>
				<li>Fentanyl</li>
				<li>Hydrocodone</li>
				<li>Ibuprofen</li>
				<li>Naproxen</li>
				<li>Oxycodone</li>
				<li>Tizanidine</li>
				<li>Tramadol </li>
			</ul>
			<div class="heading">Psychiatry: antidepressant </div>
			<ul>
				<li>Amitriptyline</li>
				<li>Bupropion</li>
				<li>Doxepin</li>
				<li>Escitalopram</li>
				<li>Fluoxetine</li>
				<li>Fluvoxamine</li>
				<li>Imipramine</li>
				<li>Trazodone(Forsleep)</li>
				<li>Venlafaxine</li>
				<li>Vortioxetine</li>
				<li>Citalopram</li>
				<li>Clomipramine</li>				
			</ul>
		</div>
		<div class="col-sm-4">
			<ul>
				<li>Desipramine</li>
				<li>Desvenlafaxine / venlafaxine (Effexor)</li>
				<li>Duloxetine</li>
				<li>Trimipramine</li>				
			</ul>
			<div class="heading">Psychiatry: antipsychotic</div>
			<ul>
				<li>Aripiprazole</li>
				<li>Benztropine</li>
				<li>Haloperidol</li>
				<li>Perphenazine</li>
				<li>Quetiapine</li>
				<li>Risperidone</li>
				<li>Haloperidol</li>
				<li>Pimozide</li>
				<li>Thioridazine</li>
			</ul>
			<div class="heading">Stabilizers & Anticonvulasants</div>
			<ul>
				<li>Oxcarbazepine</li>
			</ul>
			<div class="heading">Other</div>
			<ul>
				<li>Acetaminophen</li>
				<li>Chlorpheniramine</li>
				<li>Clobazam</li>
				<li>Clonazepam</li>
				<li>Clonidine</li>
				<li>Hydroxyzine</li>
				<li>Hyroxyzine</li>
				<li>Indomethacin</li> 
				<li>Insulin</li>
				<li>Ketoprofen</li>
				<li>Meclizine</li>
				<li>Melatonin</li>
				<li>Meloxicam</li>
				<li>Oxaprozin</li>
				<li>Promethazine</li>
				<li>Raloxifene</li>
				<li>Sirolimus</li>
				<li>Voriconazole</li>
			</ul>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 