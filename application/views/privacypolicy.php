<?php  $this->load->view('include/header')?> 


<section id="join_us_section">
	<div class="banneroverlay"></div>
    <div class="container">
     <div class="row">
        <div class="col-md-12 area_1">
            <h1 class="const_title"> Privacy Policy</h1>    
        </div>
        </div>
    </div>

</section>

<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<ol>
			<li> Users are free to delete their raw data information from their account anytime after their reports are generated. If not, we will take due care to secure your data.</li>

			<li> The security measures and systems are subject to the current state of the art of data protection. However, do note that software and data breaches do occur even at highly secured IT infrastructure, including high data security government/military IT infrastructure. We will not be liable for such malicious breaches or hacking attacks beyond our control.</li>

			<li> Losses or damages due to obsolescence of security systems on account of technological advances does not bind us to any liability, nor does it oblige it to make any refunds, recalls or concessions.</li>

			<li> Access to data, be it registration information, genetic and self-reported information, automated information or data of any other kind will be shared with regulatory authorities if we are bound to comply with the requirements of the law of the land.</li>
		</ol>

 

			
			
			

		</div>
	</div>
</div>
</section>

<?php  $this->load->view('include/footer')?> 