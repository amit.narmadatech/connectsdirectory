<?php $this->load->view('include/header') ?> 
<?php $userData = $this->session->all_userdata(); ?>
<section id="card">
    <div class="container">
        <div class="panel panel-default">
            <h3 class="panel-heading">Cart</h3>
            <div class="panel-body">       
                <div class="row">
                    <!--Grid column-->
                    <div class="col-md-12 col-xl-12">
                        <div class="row setup-content-2" id="step-5">
                            <div class="col-md-12">
                                <?php //print_r($cart); ?>
                                <?php if (!empty($cart)) { ?>

                                    <form name="upcart" id="updatecart" method="POST" action="<?php echo base_url('cart/updatecart') ?>"  onsubmit="return validateForm()">
                                         <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                                         <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                        <div class="table-responsive">
                                            <table class="table card-table" id="myTable">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 40px;">S.No.</th>
                                                        <th>Plan</th>
                                                        <th>&nbsp;</th>
                                                        <th style="width: 100px">Price</th>
                                                        <th style="width: 100px">Qty</th>
                                                        <th class="text-right" width="110px;"> Total</th>
                                                        <th class="text-center" style="width: 190px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $total = 0;
                                                    $item = 1;

                                                    foreach ($cart as $cartVal) {

                                                        $total = ($total + ($cartVal->qty * $cartVal->price));

                                                        if (!empty($cartVal->plan_image) && file_exists("uploads/plan_img/".$cartVal->plan_image)) {

                                                            $img = base_url() . 'uploads/plan_img/' . $cartVal->plan_image;
                                                        } else {
                                                            $img = base_url() . 'assets/images/img_not_available.jpg';
                                                        }
                                                        ?>	
                                                        <tr id="row_<?php echo $cartVal->plan_id; ?>">
                                                            <th><?php echo $item++; ?></th>
                                                            <td style="width: 100px;"><img src="<?php echo $img; ?>" class="cart-img">
                                                            </td>
                                                            <td>
                                                                <a class="plan-name" href="javascript:void(0)"> <?php echo!empty($cartVal->name) ? $cartVal->name : ''; ?></a>
                                                            </td>

                                                            <td><?php echo!empty($cartVal->price) ? '<i class="fa fa-inr"></i> ' . number_format($cartVal->price,2) : ''; ?></td>
                                                            <td> 
                                                                <input type="number" name="cartqty[<?php echo $cartVal->cart_id ?>]" min="1" max="100" step="1" value="<?php echo!empty($cartVal->qty) ? $cartVal->qty : ''; ?>" class="cartqty" style="width: 90px">
                                                            </td>                   
                                                            <td class="text-right"><?php echo!empty($cartVal->price) ? '<i class="fa fa-inr"></i> ' . number_format(($cartVal->qty * $cartVal->price),2) : ''; ?></td>
                                                            <td class="text-center"> <a href="javascript:void(0)" id="removecartItem_<?php echo $cartVal->cart_id ?>" data-id="<?php echo $cartVal->cart_id ?>" onclick="removecartitem('<?php echo $cartVal->cart_id ?>')" class="btn-floating btn-black removecartItem" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons">&#xE872;</i></a></td>
                                                        </tr>
                                                <?php } ?>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td  colspan="3">
<!--                                                            <a href="<?php echo base_url('plan'); ?>" class="flat-button-left border  btn-rounded float-left"><i class="material-icons">&#xE314;</i> Continue Shopping</a>-->
                                                            <a href="<?php echo base_url('/'); ?>" class="flat-button-left border  btn-rounded float-left"><i class="material-icons">&#xE314;</i> Continue Shopping</a>
                                                        </td>
                                                        <td colspan="2" class="text-right" >
                                                            <button class="flat-button border" id="updatecartBtn" type="submit"> Update cart <i class="material-icons">&#xE5D5;</i></button>
                                                        </td>
                                                      <!--  <td  class="text-right">Total: <?php// echo '<i class="fa fa-inr"></i> ' . $total; ?></td>-->
                                                        <td colspan="2" class="text-right" >
                                                        <?php if ((isset($userData['logged_in'])) && (($userData['logged_in'] === true))) { ?>

                                                                <a href="<?php echo base_url('cart/checkout'); ?>" class="flat-button border float-right" type="button">checkout <i class="material-icons">&#xE315;</i></a>	

                                                        <?php } else { ?>

                                                                <a href="#" class="flat-button border float-right float-right" data-toggle="modal" data-url="<?php echo base_url('cart/checkout'); ?>" id="checkoutBtn" data-target="#login-modal">checkout <i class="material-icons">&#xE315;</i></a>

                                                        <?php } ?>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </form>
                                        <?php } else { ?>
                                    <div class="alert alert-danger text-center">Cart is empty<div>	
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>   
            </div>
            </section>
            <!-- End Modal -->
<?php $this->load->view('include/footer') ?> 
            <script src="<?php echo base_url('assets/js/'); ?>/bootbox.min.js"></script>
    <script>
        function removecartitem(id) {
            bootbox.confirm({
                title: "Delete",
                message: "Are you sure you want to delete this plan from your cart",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-info'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        window.location.href = '<?php echo base_url("cart/removecartitem"); ?>' + '/' + id;
                    }
                }
            });
        }
    </script>
            <script>

                $(document).ready(function () {

                    $(document).on('click', '#checkoutBtn', function () {

                        var url = $('#checkoutBtn').attr('data-url');
                        console.log(url);
                        if (url == "") {
                            return false;
                        }
                        $('#redirect_url').val(encodeURIComponent($('#checkoutBtn').attr('data-url')));
                    });


                    var validator = $("#signinForm").validate({

                        errorPlacement: function (error, element) {
                            if (element.attr("name") == "email") {
                                error.appendTo('#emailerror');
                                return;
                            } else if (element.attr("name") == "password") {
                                error.appendTo('#passworderror');
                                return;
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        rules: {
                            password: "required",
                            email: {
                                required: true,
                                email: true
                            }
                        },
                        submitHandler: function (e) {

                            $('#signinBtn').prop('disabled', true);
                            $.ajax({
                                url: "<?php echo base_url('register/login') ?>",
                                type: "POST",
                                data: $('#signinForm').serialize(),
                                dataType: "json",
                                success: function (response) {
                                    $('#signinBtn').prop('disabled', false);
                                    if (response.status == true) {
                                        $('#notification').removeClass('alert alert-danger');
                                        $('#notification').html(response.msg).addClass('alert alert-success').show().delay(1000).fadeOut("slow", function () {
                                            window.location.href = '<?php echo base_url('cart/checkout'); ?>';
                                        });


                                    } else {
                                        console.log(response.error);
                                        validator.showErrors({"password": response.msg});
                                    }
                                }
                            })
                        },
                        messages: {
                            password: "Please enter your password",
                            email: {
                                required: "Please enter your email address",
                                email: "Please enter valid email address",
                            }
                        }
                    });


                    jQuery.validator.addMethod("phoneno", function (phone_number, element) {
                        phone_number = phone_number.replace(/\s+/g, "");
                        return this.optional(element) || phone_number.length > 9 &&
                                phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
                    }, "<br />Please specify a valid phone number");

                    var validator1 = $("#signupForm").validate({

                        errorPlacement: function (error, element) {
                            if (element.attr("name") == "email") {
                                error.appendTo('#erroremail');
                                return;
                            } else if (element.attr("name") == "lname") {
                                error.appendTo('#errorlname');
                                return;
                            } else if (element.attr("name") == "password") {
                                error.appendTo('#errorpassword');
                                return;
                            } else if (element.attr("name") == "repassword") {
                                error.appendTo('#errorrerepassword');
                                return;
                            } else if (element.attr("name") == "fname") {
                                error.appendTo('#errorname');
                                return;
                            } else if (element.attr("name") == "mobile") {
                                error.appendTo('#errormobile');
                                return;
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        rules: {
                            fname: "required",
                            lname: "required",
                            mobile: {
                                required: true,
                                phoneno: true
                            },
                            email: {
                                required: true,
                                email: true,
                                remote: {
                                    url: '<?php echo base_url('register/checkEmailExists'); ?>',
                                    type: "post",
                                }
                            },
                            password: {
                                required: true,
                                minlength: 8,
                            },
                            repassword: {
                                required: true,
                                minlength: 8,
                                equalTo: "#password"
                            }
                        },
                        submitHandler: function (e) {

                            $('#signuppBtn').prop('disabled', true);
                            $.ajax({
                                url: "<?php echo base_url('register/signup') ?>",
                                type: "POST",
                                data: $('#signupForm').serialize(),
                                dataType: "json",
                                success: function (response) {

                                    $('#signuppBtn').prop('disabled', false);
                                    console.log(response.status);
                                    console.log(response.errors);


                                    if (response.status == true) {
                                        $('#notifiy').removeClass('alert alert-danger');
                                        $('#notifiy').html(response.msg).addClass('alert alert-success').show();
                                        //$(signinDiv).hide(); 
                                        //$(signupDiv).show();

                                    } else {

                                        if (response.errors != "") {
                                            validator1.showErrors(response.errors);

                                            /*	var objErrors = {};
                                             $.each(response.errors, function (key, val) {
                                             console.log(key+''+val);
                                             objErrors[val[key]] = val;
                                             }); */
                                            //	validator1.showErrors(objErrors);	
                                        } else {
                                            $('#notifiy').removeClass('alert alert-success');
                                            $('#notifiy').html(response.msg).addClass('alert alert-danger').show();
                                            $('#signupForm')[0].reset();


                                        }
                                    }
                                }
                            });

                        },
                        messages: {
                            fname: "Please enter your first name",
                            lname: "Please enter your last name",
                            mobile: "Please enter your mobile number",

                            email: {
                                required: "Please enter your email address",
                                email: "Please enter valid email address",
                                remote: "This email is already exists",
                            },
                            password: {
                                required: "Please enter your password",
                                minlength: "The Password field must be at least 8 characters in length",
                            },
                            repassword: {
                                required: "Please enter confirm password",
                                equalTo: "Confirm password does not match password",
                            }
                        }
                    });

                    $('.modal').on('hidden.bs.modal', function () {
                        validator.resetForm();
                        validator1.resetForm();
                    });

                    /* $('.cartqty').blur(function(){
                     tmpval = $(this).val();
                     if(tmpval) {
                     $(this).removeClass('errorcls').next('span').remove();
                     } 
                     });
                     */


                });

    function validateForm() {
        var quantity;
        var status = true;

        $('#myTable >tbody >tr').each(function () {
            quantity = $(this).find('.cartqty').val();
            if (!quantity) {
                $(this).find('.cartqty').css('border-color', 'red');
                //  $(this).after('<span style="color:red">quantity not be empty.</span>')
                status = false;
            }
        });
        return status;
    }

 $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
            </script>
