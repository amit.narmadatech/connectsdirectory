<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
  <div class="banneroverlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 area_1">
        <h1 class="const_title">Our Team</h1>    
      </div>
    </div>
  </div>
</section>
<section id="team-page">
  <div class="container">
	  <div class="row">         
      <div class="col-md-6 col-sm-12 col-md-offset-3">
         <div class="row">
           <div class="col-sm-6">
              <div class="team-box">
                <a href="#" data-toggle="modal" data-target="#jcpopup">
                <img src="<?php echo base_url();  ?>assets/images/jc-280x280.jpg">

                <h4 class="team-head">Jeetendra Choudhary</h4>
                 <p class="team-profile">Chairman & Managing Director</p>
               </a>
              </div>
           </div>
           <div class="col-sm-6">
             <div class="team-box">
                <a href="#" data-toggle="modal" data-target="#drpopup">
                <img src="<?php echo base_url();  ?>assets/images/dr-krittika-joshi-280x280.jpg">
                <h4 class="team-head">Dr. Krittika Joshi</h4>
                <p class="team-profile">CEO – JC Genetics</p>
              </a>
            </div>
          </div>
        </div>              
      </div>   
    </div>
  </div>
</section>



<!-- jc sir Modal -->
<div class="modal fade teampopup" id="jcpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Jeetendra Choudhary</h4>
      </div>
      <div class="modal-body">
        <div class="team-image">
          <img src="<?php echo base_url();  ?>assets/images/jc-280x280.jpg" alt="Team member">
        </div>
        <p class="sub-head">Chairman & Managing Director</p>
           <div class="team-deatil">
              <p>One amongst today’s most innovative, dynamic and progressive business leaders, JC comes from a humble and rural background. As a fresh graduate, he made waves early on by inventing an electricity free technology to pull water. His firm belief passion, hard work, persistence and patience, has made JC a well-known technocrat, visionary and self-made entrepreneur. At a very young age, JC along with two visionary partners, sparked the establishment of a nationally known NGO – Network for Information Computer Technology (NICT). Together, the three pioneered the computer literacy movement and enabled government schools across India to impart a life-changing skill development program for students. The NGO was a precursor to an aggressive business – NICT Technologies Pvt. Ltd. that is a name of choice in India’s renowned banks, cellular services, electronic payment systems, schools, state education systems and a lot more. His vision helped the company win the renowned Gyanodaya project and soon thousands of schools were computerized. Also, well-equipped computer labs were established across the state propelling the company into the big league.</p>
              <p><strong>Awards </strong></p>
            <ul>
              <li>In 2018, awarded as ‘Icon of MP’, by the Hon’ble Chief Minister Shri Shivraj Singh Chouhan</li>
              <li>Excellence award by Government of MP in 2017 for contribution in Public Service Delivery Guarantee Act</li>
              <li>The following were awarded to NICT, under JC’s leadership:</li>
              <li>In 2015, awarded 2nd position nationally for Atal Pension Yojana by State Bank of India</li>
              <li>Winner of WSIS Forum 2013 and 2012 by International Telecommunication Union (ITU)</li>
              <li>Winner of Kiosk Banking Financial Inclusion SKOCH Merit Certificate 2011-2012</li>
              <li>MAP IT award for excellence in e-Governance initiatives in MP 2011-12</li>
              <li>Excellence in Economic Development 2011-12</li>
              <li>Certificate of Merit by SKOCH Group 2011</li>
              <li>SKOCH 2010 award for “India bridging Bharat &amp; India” on digital innovation day</li>
              <li>First prize under the category “e-Governance Civil Society/Development Agency Initiative of the Year” in the e-India Awards 2009 for implementation of “Common Service Center Project”</li>
              <li>First prize in CSI National IT Awards 2004-05, for implementation of “Project SHARADA” in primary schools run by the Municipal Corporation of Delhi</li>
          </ul>
        </div>
       </div>
    </div>
  </div>
</div>


<!-- dr kritika Modal -->
<div class="modal fade teampopup" id="drpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Dr. Krittika Joshi</h4>
      </div>
      <div class="modal-body">
        <div class="team-image">
          <img src="<?php echo base_url();  ?>assets/images/dr-krittika-joshi-280x280.jpg">
        </div>
        <p class="sub-head"><strong>Co-Founder & CEO (JC Genetics)</strong></p>
           <div class="team-deatil">
            Dr. Krittika Joshi is an oral and maxillofacial radiologist, with more than 11 years of experience in the field of management and diagnosis of oral pre-cancerous and cancerous lesions. She has many national and international research papers on her name.
            <p>She has a keen inclination towards health and fitness. And has a goal to deliver true wellbeing inside and out to maximum number of lives.
            </p>
          </div>
      </div>
      
    </div>
  </div>
</div>

<?php  $this->load->view('include/footer')?> 