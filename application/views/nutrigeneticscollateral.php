<?php  $this->load->view('include/header')?> 

<section id="join_us_section">
  	<div class="banneroverlay"></div>
    <div class="container">
     	<div class="row">
      		<div class="col-md-12 area_1">
         		<h2 class="const_title">Nutrigenetics collateral</h2>   
      		</div>
      	</div>
    </div>
</section>

<section class="policy">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">

				<h2 class="heading">SAME FOODS, SAME DIET <br>OPPOSITE RESULTS <br>
				 CONFUSED?</h2>

				<h3 class="sub-heading">GENES INFLUENCE EVERYTHING FROM YOUR LOOKS TO THE WAY YOUR BODY PROCESSESS VARIOUS FOODS</h3>
				<h3>Get your genetic assessment today!</h3>
				<h5>Receive a tailor made diet plan based on your unique genetic profile to achieve optimal body weight & health </h5>				
			</div>
		</div>

		<div class="row">			
			<div class="col-sm-4">
				<h4>NUTRITION</h4>
				<ul class="list-unstyled">
					<li>Design a diet plan best suited to optimize your metabolism </li>
					<li>Achieve optimal Body Mass Index </li>
					<li>Boost energy levels, focus and immunity</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>FITNESS</h4>
				<ul class="list-unstyled">
					<li>Determine exercises best suited for your body type</li>
					<li>Enhance functional fitness </li>
					<li>Improve cognition and boost your mood</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>HEALH</h4>
				<ul class="list-unstyled">
					<li>Lower risk of and protect against chronic conditions</li>
					<li>Reduce sickness and absenteeism </li>
					<li>Lower health care cost and Insurance claims </li>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 