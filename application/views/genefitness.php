<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		 <div class="row">
 			<div class="col-md-12 area_1">
 				<h2 class="const_title"> Plan Detail</h2>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	 <form name="addtocart">
 		 <div class="row">
 			<div class="col-md-6 col-sm-6">
	 		<h2>Gene Fitness</h2>	
 	 	   <p>Staying fit has become a top priority for many of us as it provides numerous physical, mental and health benefits. However, many people find it difficult to navigate the spectrum of exercises, routines, videos, blogs, experts and a vast varieties of equipment, supplements, products, programs etc. Though many of these are a fad, and there are genuine scientific advances being made in the domain of human physical performance, that can have a huge impact on your training and fitness results. Test your fitness genes to know about your physical and physiological performance.</p>     		
 	 	<div class="plan-pricing">
 	 	<div class="row">
 	 	  <div class="col-sm-8">
			<h6>Gene Fitness DNA kit   </h6>        
		  	  Requires saliva sample for DNA analysis
		  </div>
		  <div class="col-sm-4 text-right">
			<p class="t2_style editContent">Price: <strong>&#x20B9; 8000</strong></p>
		  </div>
		</div>    	
			<button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
 	 	</div>
 	 </div>

	   <div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service1.png" class="item-icon">
			    
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			           Weight loss and maintenance			          
			           <p> Know your genes and design unique exercise plans </p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Power and Endurance </li>
			        <li>Exercise motivation </li>
			        <li>Aerobic capacity </li>
			        <li>Handgrip strength  </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           Optimum fitness for improved health 
			          <p>Get insights about your fitness genes and body performance</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Lung Capacity</li>
			        <li>Heart Capacity </li>
			        <li>HDL cholesterol level with exercise	</li>		      
			       </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/skin-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           Injury risk awareness
			           <p>Test your genes to know likelihood of injuries. </p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Likelihood of injury</li>
			        <li>Tendon strength </li>
			        <li>Ligament strength </li>
			        <li>Likelihood of fatigue </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 	</form>
   </div>
</section>

<section class="plans-content">
 	<div class="container">
 	  <div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 			  <h4>Know your Fitness Genes</h4>
 			  <p>We use your saliva sample to learn about your body and fitness. Fitness Genes analyses specific genes with specific characteristics associated with fitness like muscle strength, recovery, metabolism, power/endurance profile etc. </p>

			<p>We intuitively recognize that fitness genes explain the difference not only in baseline human physicality but also, influence how different bodies respond to different exercises. Thus, as an individual who is committed to investing a certain number of hours in the gym on a regular basis, it helps to understand the genetic nature of your body.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			  <img src="<?php echo base_url('assets/'); ?>images/skin-banner.png" class="img-responsive center-block">
 	 		</div>
 		</div>
 	  </div>
    </div>
 </section>

 <section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">We help you understand your fitness genes </h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Muscle Fitness </h5>
				<p>The individuals with a specific variant of the ACTN3 gene have muscles which contract powerfully at high speeds because they secrete a specific protein called alpha actinin. The nature of your muscles depends on genetic factors like your ACTN3 gene variant. We provide you genetically optimized nutrition plan featuring many strategies for calorific load, supplementation etc. It will help you get the most from your workouts and enhance your recovery.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Genetic Workout System</h5>
				<p>	Get fit and stay healthy with our customized workout system. Based on your DNA and lifestyle data analysis, we give you a tailored genetic plan to achieve your fitness goal. Your genetic, lifestyle data and body composition goals allow us to create a safe, balanced and customized plan to get the desired physique.     </p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 			    <h5>Lose Weight </h5>
				<p>	Unlock your genetic profile and achieve healthy and sustainable weight loss. We provide genetically personalized diet & exercise to boost your metabolism, change your body composition and reshape your physique. This is a perfect fat shredding system to lose or maintain your weight.</p>
			</div> 			
		</div> 		
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block">
 			</div>
 		</div>
	</div>
</section>

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	<div class="row">
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing5.jpg" alt="plan">
      		<div class="caption">
        		<h4>Ancestry DNA </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing2.jpg" alt="plan">
      		<div class="caption">
        		<h4>Gene Diet </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing3.jpg" alt="plan">
      		<div class="caption">
        		<h4>Health Horoscope </h4>  
        		<p>Know your future health  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing4.jpg" alt="plan">
      		<div class="caption">
        		<h4>Precision Medicine</h4>  
        		<p>Tailor your drug therapy  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		</div>
 	</div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text">Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                    <div class="quote-text editContent pix_text">
                       Amit Roy
                    </div>         
                    <div class="quote-dots">. . .</div>        
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text"> Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>
                       
                    <div class="quote-text editContent pix_text">
                        Ritesh Jain
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                    
                    <div class="quote-text editContent pix_text">
                        Asha Verma
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div> 
 			 </div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
	/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});

$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});
</script>


