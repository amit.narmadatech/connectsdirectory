<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
    <!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
            <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
            <title>JC GENETICS - Towards a better life</title>      
            <meta name="description" content="JC GENETICS - Towards a better life">
                <meta name="keywords" content=" JC GENETICS - Towards a better life">
                    <meta name="author" content="www.jcgenetics.in ">     
                        <!-- Mobile Specific Metas -->
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                            <!-- Bootstrap  -->
                            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/bootstrap.css">
                                <!-- Theme Style -->
                                <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/style.css">
                                    <!-- Custom css -->
                                    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
                                        <!-- Responsive -->
                                        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/responsive.css">
                                            <!-- Colors -->
                                            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/colors/color1.css" id="colors">  
                                                <!-- Animation Style -->
                                                <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/animate.css">
                                                    <!-- Favicon and touch icons  -->
                                                    <link href="<?php echo base_url(); ?>assets/icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
                                                        <link href="<?php echo base_url(); ?>assets/icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
                                                            <link href="<?php echo base_url(); ?>assets/icon/favicon.png" rel="shortcut icon">
                                                                <!--[if lt IE 9]>     
                                                                <script src="<?php echo base_url(); ?>assets/javascript/html5shiv.js"></script>      
                                                                <script src="<?php echo base_url(); ?>assets/javascript/respond.min.js"></script>    
                                                                <![endif]-->        
                                                                </head>
                                                                <body class="page-template-front-page header-sticky home"> 
                                                                    <section id="consent-page">
                                                                        <div class="container">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">		

                                                                                    <h5>Term of Services & Privacy Statement</h5>

                                                                                    <ul class="list-unstyled">
                                                                                        <li> Users are free to delete their raw data information from their account anytime after their reports are generated. If not, we will take due care to secure your data. </li>
                                                                                        <li>The security measures and systems are subject to the current state of the art of data protection. However, do note that software and data breaches do occur even at highly secured IT infrastructure, including high data security government/military IT infrastructure. We will not be liable for such malicious breaches or hacking attacks beyond our control.</li>
                                                                                        <li>Losses or damages due to obsolescence of security systems on account of technological advances does not bind us to any liability, nor does it oblige it to make any refunds, recalls or concessions.</li>
                                                                                        <li>Access to data, be it registration information, genetic and self-reported information, automated information or data of any other kind will be shared with regulatory authorities if we are bound to comply with the requirements of the law of the land.</li>
                                                                                    </ul> 

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.min.js"></script>
                                                                    <script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/bootstrap.min.js"></script> </body>
                                                                </html>