<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 area_1">
                <h1 class="const_title">Reset Password</h1>
            </div>
        </div>
    </div>
</section>

<section class="policy"> 
    <div class="container">
        <div class="row">         
            <div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4 col-sm-offset-3">
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success text-center">
                        <?php echo $this->session->flashdata('success'); ?>
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                <?php } ?>
                <div class="panel panel-default"> 
                    <div class="panel-body">
                        <form name="resetForm" id="resetForm" method="POST" action="<?php echo base_url('register/postrecoverpassword'); ?>"> 
                               <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                                <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                            <div class="form-group">
                                <label for="password">New Password</label>
                                <input type="password" name="password" id="re-password" class="form-control" placeholder="Password">
                                <input type="hidden" name="user_id" value="<?php echo base64_encode($user_id); ?>" class="form-control">
                                <input type="hidden" name="email" value="<?php echo base64_encode($email); ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="cpassword">Confirm Password</label>
                                <input type="password" name="cpassword" id="re-cpassword" class="form-control" placeholder="Confirm Password">
                            </div>
                            <div id="notification" style="display:none"></div>
                            <div class="form-group text-center">
                                <button type="submit" class="flat-btn">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</section>

<?php  $this->load->view('include/footer')?> 
<script>
$('#resetForm').validate({
	rules: {
		password: {
		  required: true,
		  minlength:6,
		},
		cpassword: {
			required: true,
			minlength : 6,					 
			equalTo: "#re-password"
		} 
	 },
	submitHandler: function(form){
		form.submit();
	},
	messages:{
		password:{
			required: "Please enter your password",
			minlength:"The Password field must be at least 8 characters in length",
		},
		cpassword: {
			required: "Please enter confirm password",
			equalTo:"Confirm password does not match password",
		}
	 },
});
</script>
	
