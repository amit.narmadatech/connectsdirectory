<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 <section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 area_1">
 				<h2 class="const_title"> Plan Detail</h2>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	 <form name="addtocart">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
	 		<h2>Ancestry DNA </h2>	 		 	
 	 	    <p>Explore new you by unveiling the history of your DNA. You are as unique as the traces of your family history or ancestors. We help you identify genetic condition, hierarchy, ethnicity, identity and heritage. Know your story of genetic heritage today and re-live the exciting voyage of your genes.</p>     		
 	 	<div class="plan-pricing">
 	 	<div class="row">
 	 	  <div class="col-sm-8">
			<h6>Ancestry DNA kit   </h6>        
		  	  Requires saliva sample for DNA analysis
		  </div>
		  <div class="col-sm-4 text-right">
			<p class="t2_style editContent">Price: <strong>&#x20B9; 25,000</strong></p>
		  </div>
		</div>    	
			<button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
 	 	</div>
 	 </div>

	   <div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/dna-service1.png" class="item-icon">
			   <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			           Composition of Ancestry
			          <p> Know how your DNA breaks out across different regions of the world. </p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Ancestral origins</li>
			        <li>Living place of ancestors before years </li>
			        <li>DNA findings</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/dna-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           DNA Family
			          <p>Connect with people who shares the same DNA as you. </p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <ul>
			         <li><strong>Results include:</strong> </li>
			         <li>Explored identity </li>
			         <li>Discover your DNA Family</li>
			         <li>Ancestry and traits of your DNA relatives </li>			        
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/dna-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			            Haplogroups
			           <p>Explore your ancestry parts to specific group of people from 1000+ years ago.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			         <li><strong>Results include:</strong> </li>
			         <li>Origins of your maternal and paternal ancestors </li>
			         <li>Story of your ancestors</li>
			         <li>Generations of your family </li>			       
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 	</form>
   </div>
</section>

<section class="plans-content">
 	<div class="container">
 		<div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 				<h4>Global composition of ancestry</h4>
 				<p>Know your DNA inheritage proportion around the world. Get detailed geographical report to understand your DNA geographical composition. You might have lives in any country for life but what if your ancestors were from a different geographical base? Know your ancestral geography. </p>
 				<h4>Ethnic population similarity</h4>
 				<p>Compare your DNA with different ethnic groups and know more about the genetic distance between you and your closest ethnic group. Know your identity and ethnicity.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			<img src="<?php echo base_url('assets/'); ?>images/dna-banner.png" class="img-responsive center-block">
 		 </div>
 		</div>
 	</div>
 </div>
</section>

<section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">We encourage you to review your ancestry, health and traits. Find out what your DNA says. </h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Genetic Health Risks </h5>
				<p>With DNA analysis, you can understand more about your genetic health risks. The genes marks are closely associated with many health conditions and risks. Certain factors like genes, lifestyle, environment can play a vital role for occurrence of these health risks. Get insights about your genetic health risks.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Ancestry Details</h5>
				<p>	Your DNA is received from your ancestors. The genetic traces found in DNA can provide information about the living place of your ancestors. Connect with your DNA relatives and continue to find new relatives with your ancestry report. </p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Wellness</h5>
				<p>	DNA plays a vital role in your wellbeing. Find out the relation of your DNA with your food consumption, type of muscles, lactose digestion, caffeine consumption. Knowing your DNA can help you in improving your wellness. </p>
			</div>
 			<div class="col-md-6 col-sm-6"> 
 				<h5>Carrier Status</h5>
				<p>Carrier means the genetic variant being carried for a certain condition. Carriers do not have specific genetic condition but they can pass the genetic variant to their children. Prepare your family for healthy future by understanding your carrier status.	</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Understanding Traits</h5>
				<p>	Your DNA affects your hair color, eye color, skin color, taste preferences and more. The traits of people depend on mixture of internal components of your DNA.  Know your likelihood of having specific characteristics.</p>
			</div>
		</div>
	</div>
</section>

<section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block">
 			</div>
 		</div>
	</div>
</section> 	

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	 <div class="row">
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing2.jpg" alt="plan">
      		<div class="caption">
        		<h4>Gene Diet </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing1.jpg" alt="plan">
      		<div class="caption">
        		<h4>Skin Genetics </h4>  
        		<p>Get personalized diet and nutrition plan   </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>  
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing3.jpg" alt="plan">
      		<div class="caption">
        		<h4>Health Horoscope </h4>  
        		<p>Know your future health  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing4.jpg" alt="plan">
      		<div class="caption">
        		<h4>Precision Medicine</h4>  
        		<p>Tailor your drug therapy  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		</div>
 	</div>
 </div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text">Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                        <div class="quote-text editContent pix_text">
                           Amit Roy
                       </div>         
                       <div class="quote-dots">. . .</div>        
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                         <div class="quote-text editContent pix_text">
                           Ritesh Jain
                      </div>         
                       <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                       
                         <div class="quote-text editContent pix_text">
                           Asha Verma
                      </div>         
                       <div class="quote-dots">. . .</div>  
   				 </div> 
 			 </div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});


$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});

</script>


