<?php if ($this->session->flashdata('alert_success')) { ?>
    <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $this->session->flashdata('alert_success') ?> </div>
<?php } ?>
<?php if ($this->session->flashdata('alert_danger')) { ?>
    <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $this->session->flashdata('alert_danger') ?> </div>
<?php } ?>
<?php if ($this->session->flashdata('alert_warning')) { ?>
    <div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $this->session->flashdata('alert_warning') ?> </div>
<?php } ?>
<?php if ($this->session->flashdata('alert_info')) { ?>
    <div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?= $this->session->flashdata('alert_info') ?> </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
     <?= $this->session->flashdata('error') ?>
 	</div>
<?php } ?>
