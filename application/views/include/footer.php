<!-- Footer -->
<footer class="footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row"> 
                <div class="col-md-3 col-sm-4">  
                    <div class="widget widget_text">
                        <div class="textwidget">
                          <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="images">
                            <p>7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial,
                                Scheme No. 54, A.B. Road, Indore (M.P.), India. Pin 452010
                            </p>
                        </div>
                    </div><!-- /.widget -->  
                </div><!-- /.col-md-4 --> 
                <div class="col-md-3 col-sm-4">  
                    <div class="widget widget_text">
                        <h5 class="widget-title">Contact us</h5>
                        <div class="textwidget">                             
                            <p> 
                                Email:<a href="mailto:krittika.ceo@jcgenetics.com"> info@jcgenetics.in</a><br>
                                Phone: +91 968 509 5752<br>
                                Tel: +91 731 2441000<br>
                                <a href="<?php echo base_url(); ?>">www.jcgenetics.in</a>
                            </p>
                        </div>
                    </div><!-- /.widget -->      
                </div><!-- /.col-md-4 -->
                <div class="col-md-3 col-sm-4">  
                    <div class="widget widget_text"> 
                        <h5 class="widget-title">More Info</h5>
                        <div class="textwidget">  
                            <ul class="features">
                                <li><a href="<?= base_url('partner'); ?>">Partner with us</a></li>
                                <li><a href="<?= base_url('terms-of-use'); ?>">Terms Of Use</a></li>
                                <li><a href="<?= base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                                <li><a href="<?= base_url('faqs'); ?>">FAQs</a></li>
                              <!--   <li> <a href="<?php //echo base_url('career'); ?>"><a href="javascript:void(0);"> Career</a> </li> --> 
                            </ul>
                        </div>
                    </div><!-- /.widget -->   
                </div><!-- /.col-md-4 -->
				<div class="clearfix"></div>
            </div><!-- /.row -->    
        </div><!-- /.container -->
    </div><!-- /.footer-widgets -->
    <!-- Go Top -->
    <a class="go-top style1">
        <i class="zmdi zmdi-long-arrow-up"></i>
    </a>
</footer> 

<!-- Bottom -->
<div class="bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <ul class="list-inline footer-social">
                    <li><a target="_blank" href="https://www.facebook.com/JCGeneticsIndia"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="https://twitter.com/JCGeneticsIndia"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/jcgeneticsIndia"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6 pull-right text-right">
                <img src="<?php echo base_url('assets/'); ?>images/creditcardicons.png" style="height: 25px;">   
            </div><!-- /.col-md-12 -->

            <div class="col-md-6 col-sm-6  col-xs-12 text-center">
                <div class="copyright"> 
                    <p>Copyright ©2018 JC Genetics. All Rights Reserved.</p>
                </div> 
            </div><!-- /.col-md-12 -->      
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>  
</div><!-- /.boxed -->

<!-- Go Top -->
<a class="go-top">
    <i class="zmdi zmdi-long-arrow-up"></i>
</a>   

<!--  register modal -->
<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Register</h4>
            </div>
            <div class="modal-body">
                <form name="signupForm" id="signupForm" method="POST">                  
                    <!--Header-->
                    <!--Body-->
                      <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                        <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div class="form-group">                       
                        <input type="text" name="fname" id="fname" class="form-control" placeholder=" Name">
                    </div>
                    <span id="errorname"></span>

                    <div class="form-group">                     
                        <input type="text" name="lname" id="lname" class="form-control" placeholder="Surname">
                    </div>
                    <span id="errorlname"></span>

                    <div class="form-group">                    
                        <input type="text"  name="email" id="email" class="form-control" placeholder="Email">
                    </div>
                    <span id="erroremail"></span>

                    <div class="form-group">                    
                        <input type="text"  name="mobile"  maxlength="13" onkeyup="if (/\D/g.test(this.value))
                        this.value = this.value.replace(/\D/g, '')" class="form-control" placeholder="Mobile">
                    </div>
                    <span id="errormobile"></span>  

                    <div class="form-group">                     
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    </div>
                    <span id="errorpassword"></span>

                    <div class="form-group">                      
                        <input type="password" name="repassword" id="repassword" class="form-control" placeholder="Confirm Password">                     
                    </div>
                    <span id="errorrerepassword"></span>

                   <!-- <div class="checkbox">
                        <label>
                            <input type="checkbox" name="consent" id="consent" value="1" class="form-check"><a href="<?php //echo base_url('consent'); ?>" target="_blank">Consent</a>
                        </label>
                    </div>-->
                      <span id="errorconsent"></span>
                    <div id="notifiy"></div>
                    <div class="form-group text-center">
                        <button type="submit" id="SignupBtn" class="flat-btn" >Register</button>
                    </div>                  
                 <!--  
                   <div class="modal-footer text-center">
                     <p class="text-center"> Already a Member? <a href="javascript:void(0)" class="blue-text" id="signinBtnDiv">Sign in</a></p>
                    </div>
                 -->
                </form>
            </div>     
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Login modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login</h4>
            </div>
            <div class="modal-body">
                <form name="signinForm" id="signinForm" method="POST">
                    <!--Body-->
                      <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                     <input type="hidden"  name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div class="form-group">                     
                        <input type="text" name="email" id="email1" class="form-control" placeholder="Your Email">
                    </div>
                    <span id="emailerror"></span>
                    <input type="hidden" name="redirect_url" id="redirect_url" value="" readonly>
                    <div class="form-group">
                        <input type="password" name="password" id="Form-pass1" class="form-control" placeholder="Your Password">
                    </div>
                    <span id="passworderror"></span>
                    <div id="notification" style="display:none"></div>
                    <div class="row">
                        <div class="col-sm-5 col-xs-6">
                            <div class="form-group">
                                <button type="submit" id="SigninBtn" class="flat-button-left btn-sm">Login <i class="material-icons">https</i></button>
                            </div>
                        </div>             
                        <div class="col-sm-7 col-xs-6 text-right">              
                            <a href="javascript:void(0)" class="forgot-text pull-right" id="forgotpasswordDiv">Forgot Password</a>
                        </div>
                    </div>
                    <div class="form-group  social-in text-center">
                        <p>Or Sign Up with</p>
                        <!--Facebook-->
                    <!-- <a href="<?php //echo $facebookAuthUrl ?>"><i class="fa fa-facebook-square"></i></a> -->
                                                  <!-- <a href="#"><i class="fa fa-facebook-square"></i></a> -->
                        <!--Google +-->
                         
                     <?php 
                            $userData = $this->session->all_userdata();
                        if (empty($userData['logged_in'])) { ?>   
                        <a href="<?php echo $GoogleloginURL ?>"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <?php } ?>
                    </div>					
                    <div class="clearfix"></div>                  
                </form>
               <!-- <div class="modal-footer">
                    <p class="text-center"> Not a Member?  <a href="javascript:void(0)" class="blue-text" id="registerDiv">Sign up</a></p>
                </div>-->
            </div>
        </div>
    </div>
</div>

<!--  Forgot Password Modal -->
<div class="modal fade" id="forgotpassword-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <form name="forgotform" id="forgotform" method="POST"> 
                <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div class="form-group">
                        <label for="email1">Your email</label>
                        <input type="text" name="forgotemail" id="forgotemail" class="form-control">
                    </div>
                    <div id="notification" style="display:none"></div>
                    <div class="form-group text-center">
                        <button type="submit" id="forgotpassBtn" class="flat-btn">Submit</button>
                    </div>
                </form>
                <div class="modal-footer">
                    <div id="forgotID" style="margin-top: 10px;" class="text-center" style="display:none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Javascript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/bootstrap.min.js"></script>  
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/imagesloaded.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery-countTo.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery-waypoints.js"></script>  
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery-ui-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery-validate.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.easing.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/owl.carousel.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/main.js"></script>
<!-- Revolution Slider -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/slider.js"></script>    
<script type="text/javascript">
                function blockSpecialChar(e) {
                    var k;
                    document.all ? k = e.keyCode : k = e.which;
                    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
                }
</script>
<!-- <script src="https://www.gstatic.com/firebasejs/3.7.2/firebase.js"></script>
 --><script>
        $(document).ready(function () {
            $('#signinBtnDiv').on('click', function () {
                $('#register-modal').modal('hide');
                $('#forgotpassword-modal').modal('hide');
                $('#login-modal').modal('show');
            });
            $(document).on('click', '#registerDiv', function () {
                $('#login-modal').modal('hide');
                $('#forgotpassword-modal').modal('hide');
                $('#register-modal').modal('show');
            });
            $('#forgotpasswordDiv').on('click', function () {
                $('#login-modal').modal('hide');
                $('#register-modal').modal('hide');
                $('#forgotpassword-modal').modal('show');
            });
            var validator = $("#signinForm").validate({
                /*errorPlacement: function(error, element) {
                 if(element.attr("name") == "email"){
                 error.appendTo('#emailerror');
                 return;
                 }else if(element.attr("name") == "password"){
                 error.appendTo('#passworderror');
                 return;
                 }else {
                 error.insertAfter(element);
                 }
                 }, */
                rules: {
                    password: "required",
                    email: {
                        required: true,
                        email: true
                    }
                },
                submitHandler: function (e) {
                    $('#signinBtn').prop('disabled', true);
                    $.ajax({
                        url: "<?php echo base_url('register/login') ?>",
                        type: "POST",
                        data: $('#signinForm').serialize(),
                        dataType: "json",
                        success: function (response) {
                            $('#signinBtn').prop('disabled', false);
                            if (response.status == true) {
                                $('#notification').removeClass('alert alert-danger');
                                $('#notification').html(response.msg).addClass('alert alert-success').show().delay(1000).fadeOut("slow", function () {
                                    if (response.redirect_url) {
                                        window.location.href = decodeURIComponent(response.redirect_url);
                                    } else {
                                        window.location.href = '<?php echo base_url('profile'); ?>';
                                        //window.location.href='<?php echo base_url('user/index'); ?>';
                                    }
                                    //window.location.href='<?php echo base_url('cart/checkout'); ?>';
                                });
                            } else {
                                console.log(response.error);
                                validator.showErrors({"password": response.msg});
                            }
                        }
                    })
                },
                messages: {
                    password: "Please enter your password",
                    email: {
                        required: "Please enter your email address",
                        email: "Please enter valid email address",
                    }
                }
            });
            jQuery.validator.addMethod("phoneno", function (phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                        phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
            }, "<br />Please specify a valid phone number");
            var validator1 = $("#signupForm").validate({
                errorPlacement: function(error, element) {
                    if(element.attr("name") == "consent"){
                            error.appendTo('#errorconsent');
                           /* return;
                           }else if(element.attr("name") == "lname"){
                           error.appendTo('#errorlname');
                           return;
                           }else if(element.attr("name") == "password"){
                           error.appendTo('#errorpassword');
                           return;
                           }else if(element.attr("name") == "repassword"){
                           error.appendTo('#errorrerepassword');
                           return;
                           }else if(element.attr("name") == "fname"){
                           error.appendTo('#errorname');
                           return;
                           }else if(element.attr("name") == "mobile"){
                           error.appendTo('#errormobile');
                           return;*/
                    }else {
                        error.insertAfter(element);
                    }
                },	
                rules: {
                    fname: "required",
                    lname: "required",
                   // consent: "required",
                    mobile: {
                        required: true,
                        phoneno: true
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: '<?php echo base_url('register/checkEmailExists'); ?>',
                            type: "post",
                            data: { email: $('#email').val(),'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                        }
                    },
                    password: {
                        required: true,
                        minlength: 6,
                    },
                    repassword: {
                        required: true,
                        minlength: 6,
                        equalTo: "#password"
                    }
                },
                submitHandler: function (e) {
                    $('#signuppBtn').prop('disabled', true);
                    $.ajax({
                        url: "<?php echo base_url('register/signup') ?>",
                        type: "POST",
                        data: $('#signupForm').serialize(),
                        dataType: "json",
                        success: function (response) {
                            $('#signuppBtn').prop('disabled', false);
                            console.log(response.status);
                            console.log(response.errors);
                            if (response.status == true) {
                                $('#notifiy').removeClass('alert alert-danger');
                                //$('#notifiy').html(response.msg).addClass('alert alert-success').show();
                                $('#notifiy').html(response.msg).addClass('alert alert-success').show().delay(2000).fadeOut("slow", function () {
                                    $('#signupForm')[0].reset();
                                    $('#register-modal').modal('hide');
                                    $('#login-modal').modal('show');
                                });
                            } else {
                                if (response.errors != "") {
                                    validator1.showErrors(response.errors);
                                    /*	var objErrors = {};
                                     $.each(response.errors, function (key, val) {
                                     console.log(key+''+val);
                                     objErrors[val[key]] = val;
                                     }); */
                                    //	validator1.showErrors(objErrors);	
                                } else {
                                    $('#notifiy').removeClass('alert alert-success');
                                    $('#notifiy').html(response.msg).addClass('alert alert-danger').show();
                                    $('#signupForm')[0].reset();
                                }
                            }
                        }
                    });
                },
                messages: {
                    fname: "Please enter your first name",
                    lname: "Please enter your last name",
                    mobile: "Please enter your mobile number",
                    consent: "Please select consent",
                    email: {
                        required: "Please enter your email address",
                        email: "Please enter valid email address",
                        remote: "This email is already exists",
                    },
                    password: {
                        required: "Please enter your password",
                        minlength: "The Password field must be at least 8 characters in length",
                    },
                    repassword: {
                        required: "Please enter confirm password",
                        equalTo: "Confirm password does not match password",
                    }
                }
            });
            var validator2 = $('#forgotform').validate({
                rules: {
                    forgotemail: {
                        required: true,
                        email: true,
                    }
                },
                submitHandler: function (form) {
                    $('#forgotpassBtn').prop('disabled', true);
                    $.post("<?php echo base_url('register/forgotPassword'); ?>", $('#forgotform').serialize(), function (response) {
                        $('#forgotpassBtn').prop('disabled', false);
                        var obj = $.parseJSON(response);
                        if (obj.status == true) {
                            $('#forgotID').removeClass('alert alert-danger');
                            $('#forgotID').html(obj.msg).addClass('alert alert-success').show().delay(3000).fadeOut("slow", function () {
                                $('#forgotform')[0].reset();
                                $('#forgotpassword-modal').modal('hide');
                            });
                        } else {
                            $('#forgotID').removeClass('alert alert-success');
                            $('#forgotID').html(obj.msg).addClass('alert alert-danger').show();
                        }
                    });
                },
                messages: {
                    forgotemail: {
                        required: "Please enter your email address",
                        email: "Please enter valid email address",
                    }
                },
            });
            $('.modal').on('hidden.bs.modal', function () {
                validator.resetForm();
                validator1.resetForm();

            });


            var ids = [];
            var notf_lst = '';
            (function checkForNotification() {
                $.ajax({
                    url: "<?php echo base_url('notification/get_customer_notification') ?>",
                    type: "GET",
                    data: {},
                    dataType: "json",
                    success: function (response) {

                        if (response.status == true) {
                            $("#notification_count").html(response.data.length);

                            console.log(response);
                            $.each(response.data, function (index, val) {
                                if (index === 5) {
                                    return false;
                                }
                                if (jQuery.inArray(val.id, ids) < 0) {
                                    ids.push(val.id);
                                    var href = (val.href != '') ? val.href : '<?php echo base_url("notification"); ?>';
                                    notf_lst += '<li id="notification_' + val.id + '"><a class="single-notification  " id="' + val.id + '">';
                                    notf_lst += '<span><span class="time">' + val.formatted_date + '</span></span><br><span class="message"><i class="fa fa-info"></i> &nbsp;&nbsp;&nbsp;' + val.title + '</span>';
                                    notf_lst += '</a></li>';
                                }
                            });

                            $('#notification_list').prepend(notf_lst);
                            notf_lst = '';
                        }
                    }
                });
                var chk_login = '<?php echo $this->session->user_id; ?>';
                if (chk_login) {
                    setTimeout(checkForNotification, 1000);
                }
            }());

            $(document).on('click', '.single-notification', function (event) {
                var notification_id = this.id;
                $.ajax({
                    url: "<?php echo base_url('notification/change_status') ?>",
                    type: "POST",
                    data: {id: notification_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                    dataType: "json",
                    success: function (response) {

                        if (response.status == true) {
                            $("#notification_" + response.data.id).remove();
                            $("#notification_count").html(parseInt($("#notification_count").html()) - 1);
                            var href = (response.data.href == '') ? '<?php echo base_url("user/notification"); ?>' : '<?php echo base_url(); ?>/' + href;
                            window.location.href = href;
                        }
                    }
                });
            });

        });

</script> 

</body>
</html>