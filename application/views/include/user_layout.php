<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>
     <!-- header ends -->
</header> 
  
  <main id="site-content" role="main">
    
    <?php if (isset($this->session)) : ?>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
           
          </div>
        </div><!-- .row -->
      </div><!-- .container -->
    <?php endif; ?>

<!-- START - this is main section  -->

<?php $this->load->view($data->page, $data); ?>


<!-- END - this is main section  -->
</main>


<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Footer -->
    <footer class="footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row"> 
                    <div class="col-md-4 col-sm-4">  
                        <div class="widget widget_text">
                            <div class="textwidget">
                                <img src="<?php echo base_url();?>assets/images/logo-footer.png" alt="images">
                                <p> 7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial,
                                         Scheme No. 54, A.B. Road, Indore (M.P.), India. Pin 452010
                                </p>
                            </div>
                        </div><!-- /.widget -->  
                    </div><!-- /.col-md-4 --> 

                     <div class="col-md-4 col-sm-4">  
                        <div class="widget widget_text">
                            <h5 class="widget-title">Contact us</h5>
                             <div class="textwidget">                             
                               <p> Email:<a href="mailto:krittika.ceo@jcgenetics.com"> krittika.ceo@jcgenetics.com</a><br>
                                   Phone: +91 871 882 0561<br>
                                   Tel: +91 731 2441000<br>
                                   <a href="<?php echo base_url();?>">www.jcgenetics.in</a>
                                </p>
                            </div>
                        </div><!-- /.widget -->      
                    </div><!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">  
                        <div class="widget widget_text"> 
                            <h5 class="widget-title">More Info</h5>
                            <div class="textwidget">  
                                <ul class="features">
                                   <li><a href="<?= base_url('partner'); ?>">Partner with us</a></li>
                                   <li><a href="<?= base_url('terms-of-use'); ?>">Terms Of Use</a></li>
                                   <li><a href="<?= base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                                   <li><a href="<?php echo base_url('career'); ?>">Career</a>
                                </ul>
                            </div>
                        </div><!-- /.widget -->   
                    </div><!-- /.col-md-4 -->
                </div><!-- /.row -->    
            </div><!-- /.container -->
        </div><!-- /.footer-widgets -->
            <!-- Go Top -->
            <a class="go-top style1">
                <i class="zmdi zmdi-long-arrow-up"></i>
            </a>
        </footer> 

    <!-- Bottom -->
      <div class="bottom">
        <div class="container">
          <div class="row">
              <div class="col-md-6 col-sm-6">
                <ul class="list-inline footer-social">
                  <li><a target="_blank" href="https://www.facebook.com/JCGenetic/"><i class="fa fa-facebook"></i></a></li>
                  <li><a target="_blank" href="https://twitter.com/JCGenetics"><i class="fa fa-twitter"></i></a></li>
                  <li><a target="_blank" href="https://www.instagram.com/JCGenetics/"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>

              <div class="col-md-6 col-sm-6 text-right">
                <div class="copyright"> 
                    <p>Copyright ©2018 JC Genetics. All Rights Reserved.</p>
                </div> 
               </div><!-- /.col-md-12 -->
              </div><!-- /.row -->
            </div><!-- /.container -->
        </div>  
    </div><!-- /.boxed -->

    <!-- Go Top -->
    <a class="go-top">
        <i class="zmdi zmdi-long-arrow-up"></i>
    </a>   

           
    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/bootstrap.min.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/imagesloaded.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery-countTo.js"></script> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery-waypoints.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery-ui-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery-validate.js"></script> 

  
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/owl.carousel.js"></script>
    <!--   <script type="text/javascript" src="javascript/jquery.mb.YTPlayer.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="javascript/gmap3.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/jquery.fitvids.js"></script> -->  
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/main.js"></script>
    <!-- Revolution Slider -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/javascript/slider.js"></script>


  <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

   <script>
    (function($){
      $(window).on("load",function(){        
        $("#notify").mCustomScrollbar({        
        theme:"dark-thin"   
        });
        
      });
    })(jQuery);
  </script> 


    <script type="text/javascript">
      function previewImage(input,id)
      {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('#'+id).attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
      }


    </script> 

     <script type="text/javascript">
        jQuery(document).ready(function($) {
            var ids = [];
            var notf_lst = '';
            (function checkForNotification() {
               
               $.ajax({
                  url:"<?php echo base_url('notification/get_customer_notification')?>",
                  type:"GET",
                  data: {},
                  dataType:"json",
                  success: function(response){
                        
                        if(response.status == true){
                            $("#notification_count").html(response.data.length);
                                
                            console.log(response);
                            $.each(response.data, function(index, val) {
                                if(jQuery.inArray(val.id, ids)  < 0){
                                  ids.push(val.id);
                                  var href = (val.href != '')?val.href:'<?php echo base_url("notification"); ?>';
                                  //notf_lst += '<li id="notification_'+val.id+'"><a class="single-notification  waves-effect waves-light" id="'+val.id+'"><b>'+val.title+'</b><p>'+val.body+'</p></a></li>';
                                  notf_lst += '<div id="mCSB_1" class="mCustomScrollBox mCS-dark-thin mCSB_vertical mCSB_inside notification_'+val.id+'" style="max-height: 155.256px;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr"><li><a id="'+val.id+'" class="single-notification waves-effect waves-light"><b>'+val.title+'</b><p>'+val.body+'</p></a></li></div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-dark-thin mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; top: 0px; height: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div>'
                                }
                             });
                                                      
                            $('#notify').append(notf_lst);
                            notf_lst ='';
                        }
                    } 
                });

               setTimeout(checkForNotification, 1000);
            }());

            $(document).on('click', '.single-notification', function(event) {
                var notification_id= this.id;
                 $.ajax({
                  url:"<?php echo base_url('notification/change_status')?>",
                  type:"POST",
                  data: { id :notification_id },
                  dataType:"json",
                  success: function(response){
                        
                        if(response.status == true){
                            $(".notification_"+response.data.id).remove();
                            $("#notification_count").html(parseInt($("#notification_count").html())-1);
                            var href = (response.data.href == '') ?'<?php echo base_url("notification"); ?>':'<?php echo base_url(); ?>/'+href;
                            //window.location.href = href;
                        }
                    } 
                });
            });
            
        });
    </script>
  </body>
</html>