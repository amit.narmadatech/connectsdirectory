<?php
    if (($this->session->logged_in) && (isset($this->session->browser_token))) {
set_browser_id($this->session->browser_token);
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->
<head>
<!-- Basic Page Needs -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible'content='IE=edge,chrome=1'><![endif]-->
<title><?php echo !empty($title)? $title :'FC Directory'; ?></title>      
<meta name="description" content="<?php echo !empty($description)? $description :'FC Directory'; ?>">
<meta name="keywords" content="<?php echo !empty($keywordes)? $keywordes :'FC Directory'; ?>">
    
<meta name="author" content="www.fcconnectsdirectory.in"> 
<meta name="google-site-verification" content="uKplgvdlsffyyx1L-RgVJCScKHpRNZaeQJuApDvVX1k" /> 
<a href="https://www.jcgenetics.in" hreflang="en"></a>
<!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Bootstrap  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/material-design-iconic-font.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/material-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/simple-line-icons.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/revolution-slider.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/shortcodes.css">
<!-- Theme Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/style.css">
<!-- Custom css -->
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/responsive.css">
<!-- Colors -->
<!-- <link rel="stylesheet" type="text/css" href="<?php // echo base_url(); ?>assets/stylesheets/colors/color1.css" id="colors">  --> 
<!-- Animation Style -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/animate.css"> -->
<!-- Favicon and touch icons  -->
<link href="<?php echo base_url(); ?>assets/icon/favicon-48.png" rel="apple-touch-icon-precomposed" sizes="48x48">
<link href="<?php echo base_url(); ?>assets/icon/favicon-32.png" rel="apple-touch-icon-precomposed">
<link href="<?php echo base_url(); ?>assets/icon/favicon-16.png" rel="shortcut icon">
<!--[if lt IE 9]>     
<script src="<?php echo base_url(); ?>assets/javascript/html5shiv.js"></script>      
<script src="<?php echo base_url(); ?>assets/javascript/respond.min.js"></script>    
<![endif]-->  


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117753239-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117753239-1');
</script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TMBJX2K');</script>

</head>
<body class="page-template-front-page header-sticky home">
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMBJX2K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>  
    
<?php $userData = $this->session->all_userdata(); ?>
<div class="loader"> <span class="loader1 block-loader"></span> <span class="loader2 block-loader"></span> <span class="loader3 block-loader"></span> </div>
<!-- Boxed -->
<div class="boxed">
<!-- Header -->
<header id="header" class="header style3 clearfix">
    <div class="header-inner">
        <div class="container">
        <div id="logo" class="logo"> 
            <a href="<?= base_url('/'); ?>">
              <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="image"> 
            </a> 
        </div>
<!-- /.logo -->
 <?php //echo $this->uri->segment(1); ?>  
        <div class="nav-wrap">
            <div class="btn-menu open"></div>
            <!-- //mobile menu button -->
            <nav id="mainnav" class="mainnav">
                <ul class="menu">
                <?php
                    $navMenu[] = array('url' => base_url('/'), 'title' => 'Home');
                    $navMenu[] = array('url' => base_url('about-us'), 'title' => 'About us');
                    $navMenu[] = array('url' => base_url('our-team'), 'title' => 'Team');
                    $navMenu[] = array('url' => base_url('plan'), 'title' => 'Shop Now');
                    $navMenu[] = array('url' => base_url('how-it-works'), 'title' => 'How It Works');
                    $navMenu[] = array('url' => base_url('blogs'), 'title' => 'Blogs');
                   // $navMenu[] = array('url' => base_url('home/contact'), 'title' => 'Contact');
                        foreach ($navMenu as $key => $value):
                        $class = '';
                           if (base_url(uri_string()) == $value['url']) {
                           $class = 'active';
                    }
                ?>
                    <li class="<?php echo $class; ?>">
                        <a href="<?php echo $value['url']; ?>">
                            <?php echo $value['title']; ?>
                        </a>
                    </li>
            <?php endforeach; ?>

                </ul>
                <!-- /.menu -->
            </nav>
<!-- /.mainnav -->

<div class="language-wrap">
<div class="btn-menu-active">
<?php if ((isset($userData['logged_in'])) && (($userData['logged_in'] === true))) { ?>
        <a  href="<?= base_url('/user/notification'); ?>">
            <i class="material-icons">notifications</i>
            <span id="notification_count" class="badge badge-info"> 0</span>
        </a>
<?php } ?>
    <a href="<?php echo base_url('/cart') ?>">
        <i class="material-icons">&#xE547;</i>
        <span class="badge badge-info">    
            <?php
            $totalcount = getTotalCount();
            echo!empty($totalcount) ? $totalcount : 0;
            ?>                   
        </span>
    </a> 
</div>
<ul class="language">
    <?php
    if ((isset($userData['logged_in'])) && (($userData['logged_in'] === true))) {
        $img = "";
        $url = parse_url($this->session->image);
        if (isset($url['scheme']) == 'https' || isset($url['scheme']) == 'http') {
            $img = $this->session->image;
        } else if (!empty($this->session->image) && file_exists("uploads/userimages/" . $this->session->image)) {

            $img = base_url() . 'uploads/userimages/' . $this->session->image;
        } else {
            $img = base_url('assets/images/avtar.jpg');
        }
        ?>
        <li class="dropdown user-pro">
            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                <span class="user-img"> <img src="<?php echo $img; ?>"></span>
                <span class="user-name"> <?= $this->session->fullname ?> </span>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="<?= base_url('user/profile') ?>"><i class="fa fa-user" data-unicode="f007"></i> My Account</a></li>
                <li > <a href="<?php echo base_url('changepassword'); ?>" class="blue-text"><i class="fa fa-lock"></i> Change Password</a></li>
                <li><a class="dropdown-item" href="<?= base_url('logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            </ul>
        </li>
    <?php } else { ?>
        <li class="current"><a href="#" data-toggle="modal" data-target="#register-modal">Register</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
    <?php } ?>
            </ul>
<!-- /.language -->
        </div>
<!-- /.language-wrap -->

        </div>
<!-- /.nav-wrap -->

</div>
    </div>
<!-- /.header-inner -->
</header>
<!-- /.header -->
<script>
var base_url = '<?php echo base_url(); ?>';
</script>