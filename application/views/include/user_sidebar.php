<?php 
	$imgpath = (isset($this->session->image) && (!empty($this->session->image)))?base_url('uploads/userimages/'.$this->session->image):base_url('uploads/userimages/default.jpeg'); 
?>
	<div class="profile-sidebar">
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
			<ul class="list-unstyled">
		<?php if ($_SESSION['logged_in'] === true) : ?>
	              <li class="active"><a href="<?php echo base_url('userdashbord') ?>">Dashboard</a></li>
	              <li><a href="<?php echo base_url('user/profile') ?>">My Account</a></li>
	              <li><a href="<?php echo base_url('myorder') ?>">My Orders</a></li>
	              <li><a href="<?php echo base_url('user/notification') ?>"> Notification</a></li>
	              <li><a href="<?php echo base_url('user/ticket') ?>"> Ticket</a></li>		             
	              <li><a href="<?php echo base_url('logout') ?>">Logout</a></li>
	            <?php else : ?>
	              <li><a href="<?php echo base_url('register') ?>">Register</a></li>
	              <li><a href="<?php echo base_url('login') ?>">Login</a></li>
	            <?php endif; ?>						
			</ul>
		</div>
		<!-- END MENU -->
	</div>			   
