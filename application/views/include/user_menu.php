<!-- SIDEBAR MENU -->
<div class="user-menu">
	<ul class="list-unstyled">
	 <?php 
		$navMenuLink = array();
 		//$navMenuLink[] = array( 'title' =>'Dashboard','url'=>base_url('user/index')); 
                $navMenuLink[] = array( 'title' =>'My Account','url'=>base_url('user/profile'));
                $navMenuLink[] = array( 'title' =>'My Orders','url'=>base_url('myorder'));
 		$navMenuLink[] = array( 'title' =>'Activate Kit','url'=>base_url('user/activatekit')); 
 	        $navMenuLink[] = array( 'title' =>' My Kit List','url'=>base_url('user/kitlist'));
 	        $navMenuLink[] = array( 'title' =>' My DitePlan','url'=>base_url('user/myditeplan'));
 		$navMenuLink[] = array( 'title' =>'Notifications','url'=>base_url('user/notification'));
 	 	$navMenuLink[] = array( 'title' =>'Pickup Request','url'=>base_url('user/pickup'));
 	 	$navMenuLink[] = array( 'title' =>'Tickets','url'=>base_url('user/ticket'));
 	 	//$navMenuLink[] = array( 'title' =>'Questionnaire','url'=>base_url('user/questionnaire'));
		//$navMenuLink[] = array( 'title' =>'Logout','url'=>base_url('logout'));
	 ?>
		<?php if ($_SESSION['logged_in'] === true) : ?>
			<?php foreach($navMenuLink as $Menukey=>$MenuVal): ?>
				<?php 
					$class = '';
						if(base_url(uri_string()) ==$MenuVal['url']): 
						$class = "active";
					endif;	
				?>		   
		<li class="<?php echo $class; ?>"><a href="<?php echo $MenuVal['url']; ?>"><?php echo $MenuVal['title']; ?></a></li>
		<?php endforeach; ?>			  
	<?php else : ?>
		<li><a href="<?= base_url('register') ?>">Register</a></li>
		<li><a href="<?= base_url('login') ?>">Login</a></li>
	<?php endif; ?>	

		<!--<li class="dropdown">
  			<a id="dLabel" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    			Dropdown Link
    		<span class="caret"></span>
 			 </a>
		    <ul class="dropdown-menu" aria-labelledby="dLabel">
			   <li><a href="#">Sub Menu</a></li>
			   <li><a href="#">Sub Menu</a></li>
			   <li><a href="#">Sub Menu</a></li>
		    </ul>
		</li>	-->				
	</ul>
</div>
<!-- END MENU -->	