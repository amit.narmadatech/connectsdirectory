<?php  $this->load->view('include/header')?> 


<section id="join_us_section">
	<div class="banneroverlay"></div>
     <div class="container">
      <div class="row">
        <div class="col-md-12 area_1">
            <h2 class="const_title">  Gene and Health </h2>    
        </div>
        </div>
    </div>
</section>

<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="heading">YOUR GENES YOUR HEALTH</h2>
			<h3 class="sub-heading">Genes play a role in who we are and how we perform.</h3>
			<h3>Armed with next level of  information you can optimize your nutrition and fitness regimen to achieve peak results.</h3>				
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4>Get your genetic assessment today</h4>
				<ul class="list-inline assist-list">
					<li> <span class="circule-blue">Fitness </span> </li>
					<li> +</li>
					<li> <span class="circule-blue">Nutrition</span> </li>
					<li> +</li>
					<li> <span class="circule-blue">Lifestyle </span></li>
					<li> = </li>
					<li><span class="circule-red">Your Present and Future Performance </span></li>
				</ul>
			</div>
		</div>	

		<h3>Our Products and their Benefits</h3>
		<h4>Assessing parameters</h4>

 		<div class="row">
			<div class="col-sm-4">
				<h4>NUTRITION </h4>
				<ul class="list-unstyled">
					<li> Eating Behaviour </li>
					<li> Carbohydrate Sensitivity </li>
					<li> Saturated Fat Sensitivity </li>
					<li> Polyunsaturated Fat Sensitivity </li>
					<li> Monounsaturated Fat Sensitivity </li>
					<li> Vitamin A, D, E, B6, B9, B12 & C Requirement</li>
					<li> Iron, Omega3 Requirement</li>
					<li> Antioxidant Requirement </li>
					<li> Caffeine Sensitivity </li>
					<li> Salt Sensitivity </li>
					<li> Gluten Intolerance Risk</li>
					<li> Lactose Intolerance Risk</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>FITNESS </h4>
				<ul class="list-unstyled">
					<li> Aerobic Ca pa city (VO2 max) </li>
					<li>Endurance and Power Profile Anaerobic and/or L actate Treshold </li>
					<li>Exercise and Fa t Loss </li>
					<li>Fle xibility and Performance </li>
					<li>Injury Risk and Repair Ability </li>
					<li>Muscle Fatigue Resistance </li>
					<li>Resistance Training and Muscle Building </li>
					<li>Capacity </li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>HEALTH </h4>
				<ul class="list-unstyled">
					<li> Obesity </li>
					<li>Type II Diabetes</li>
					<li>Cardiovascular Diseases</li>
					<li>Hypertension</li>
				</ul>
			</div>
		</div>	

		<h4>Benefits</h4>
 		<div class="row">
			<div class="col-sm-4">
				<h4>NUTRITION </h4>
				<ul class="list-unstyled">
					<li> Design a diet plan best suited to optimize your metabolism </li>
					<li> Achieve optimal Body Mass Index </li>
					<li> Boost energy levels, focus and immunity </li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>FITNESS </h4>
				<ul class="list-unstyled">
				  <li> Determine exercises best suited for your body type </li>
				  <li>Enhance functional fitness </li>
				  <li>Improve cognition and boost your mood </li>	
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>HEALTH </h4>
				<ul class="list-unstyled">
					<li>Lower risk of and protect against chronic conditions </li>
					<li>Reduce sickness and absenteeism </li>
					<li>Lower health care cost and Insurance claims </li>
				</ul>
			</div>
		</div>
		<h3>Why we are the best at what we do</h3>
 	<div class="row">
 		<div class="col-sm-3 text-center">
 			<h4>75%</h4>
 			found their Genetic Report very useful & 25% useful
 		</div>
 		<div class="col-sm-3 text-center">
 			<h4>90%</h4>
 			are motivated to follow their genetically appropriate diet
 		</div>
 		<div class="col-sm-3 text-center">
 			<h4>100%</h4>
 			found genetic diet recommended actions helpful
 		</div>
 		<div class="col-sm-3 text-center">
 			<h4>90%</h4>
 		will recommend the gene based wellness program to others 
 		</div>
 	</div>

 	<h3>Simple and Hassle Free, Non-Invasive Saliva Test</h3>
		<div class="row">
			<div class="col-sm-4">
				Collect saliva before brushing in the morning
			</div>
			<div class="col-sm-4">
				Spit directly into the funnel until the black line on the tube
			</div>
			<div class="col-sm-4">
				Twist the yellow gel tube and add the gel into the saliva tube
			</div>
			<div class="col-sm-4">
				Unscrew the funnel and close the tube with the cap
			</div>
			<div class="col-sm-4">
				Complete the lifestyle questionnaire
			</div>
			<div class="col-sm-4">
				Sign the declaration form and call +91 731 2441000 to schedule
a free reverse pick up
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4>About JCGenetics</h4>
				<p>JCGenetics is a pioneer, and a leading personal genomics company focussed on enabling personalized preventive healthcare. We are dedicated to empowering physicians, wellness professionals and customers with the most validated, accurate and actionable genomic information to positively impact and improve their health and quality of life.</p>
			</div>
		</div>

</div>
</div>
	</div>
</section>

<?php  $this->load->view('include/footer')?> 