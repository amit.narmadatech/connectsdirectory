<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		 <div class="row">
 			<div class="col-md-12 area_1">
 				<h2 class="const_title"> Plan Detail</h2>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	 <form name="addtocart">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
	 	 	 <h2>Gene Diet</h2>	
 	 	      <p>Check your genetic food metabolism, Macro nutrients Micro nutrients, Intolerances and Sensitivities. Learn about the best suited foods for you and customize your diet and nutritional recommendations.</p>
 	 	<div class="plan-pricing">
 	 	<div class="row">
 	 	  <div class="col-sm-8">
			<h6>Gene Diet DNA kit   </h6>        
		  	  Requires saliva sample for DNA analysis
		  </div>
		  <div class="col-sm-4 text-right">
			<p class="t2_style editContent">Price: <strong>&#x20B9; 25,000</strong></p>
		  </div>
		</div>    	
			<button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
 	 	</div>
 	 </div>

	<div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/diet-service1.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			           Wellness
			          <p> Learn how your genomes play a vital role in your nutritional and lifestyle choices.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Genetic weight </li>
			        <li>Weight Tendency </li>
			        <li>Metabolism </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/diet-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Intolerance and Sensitivity
			          <p>Know more about your body intolerances and sensitivities.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Deep Sleep </li>
			        <li>Lactose intolerance </li>
			        <li>Blood pressure sensitivity </li>
			        <li>Gluten sensitivity</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/diet-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           Nutritional Information
			           <p>Get better insights about vitamin, minerals and other needs of your body.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Vitamin and Calcium </li>
			        <li> Iron and phosphate </li>
			        <li> Caffeine consumption </li>
			        <li> Antioxidants </li>
			        <li> Fiber intake </li>
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 	</form>
   </div>
</section>

 <section class="plans-content">
 	<div class="container">
 		<div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 				<h4>Perfect gene diet at your fingertips.</h4>
 				<p>We use your saliva sample to learn about your diet, health and lifestyle. We don’t guide you for your diet and nutrition but it’s your own genes. Let your genetic information change your lifestyle to better. </p>
 				<h4>Change your outlook on dieting.</h4>
 				<p>After the test, actionable report is provided to you in form of simple suggestions. Gene Diet help to check your genetic predisposition to food and nutrient needs, sensitivities and intolerances to personalize your diet and nutritional recommendations.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			<img src="<?php echo base_url('assets/'); ?>images/diet-banner.png" class="img-responsive center-block">
 		</div>
 	  </div>
 	</div>
 </div>
</section>

 <section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">We help you understand your body with Gene Diet program.</h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Tendency to overeat </h5>
				<p>Tendency to over-consume food can be due to unregulated hormones and not only because of less self-control.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Tendency to overeat sweets</h5>
				<p>	It can be due to low sensitivity to sweet taste. So, you are likely to consume more sweet foods.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 				 <h5>Tendency to gain weight </h5>
				<p>	Not everyone gains weight because of consuming junk food but it can be your genes that affects metabolism.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 
 				<h5>Tendency to lose weight</h5>
				<p>Your consumption of protein does not ensure weight gain because some people experience good weight maintenance with protein diet but not everyone experiences the same.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Tendency to regain weight</h5>
		  	  <p>Some people loses weight with diet plan efforts but cannot maintain that weight because they tend to regain the weight in no time.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Vitamin needs</h5>
			 <p>Food nutrients are converted to vitamins and this work is done by enzymes, which are produced by genes. So, of course genes influence the vitamin levels in your body. It can be Vitamin A, Vitamin D, Vitamin E, Vitamin B6, Vitamin B9, Vitamin B12, Vitamin C. </p>
			</div>
		</div>	
 		<div class="collapse" id="collapseExample">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Iron needs</h5>
			  <p>Lack of iron in body leads to weakness, dizziness, fatigue. Optimize your iron intake by knowing your genes.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			  <h5>Calcium needs</h5>
			  <p>This mineral is harmful if it is deficient or excessive in body. Find out the need of calcium in your body according to your genes.</p>
			</div>
		</div> 		
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Antioxidant needs</h5>
				<p>	Know the requirements of antioxidants in your body as excess of it can harm your body cells.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			  <h5>Blood pressure sensitivity with salt intake </h5>
			  <p>Some people have tendency to increased blood pressure due to high salt consumption but blood pressure reduces on low salt diet. Know the optimized consumption for you.</p>
			</div>
		</div>		 		
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Caffeine metabolism </h5>
			  <p>Caffeine need to be clear from the body to avoid its negative effects. Some individuals are fast caffeine metabolizers but the slow metabolizers have increased risk of heart diseases.</p>
			</div>
			<div class="col-md-6 col-sm-6">
			 <h5>Lactose intolerance </h5>
			 <p>Know your milk and milk products digestion capability for better body and health.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			  <h5>Gluten sensitivity </h5>
			  <p>Know about your sensitivity to gluten to understand about your regular issues of bloating and diarrhea.</p>
			</div>
 		 </div>
 		</div>
 		<div class="readmore-link">
		 <a class="flat-button"  role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> More <i class="material-icons">chevron_right</i></a>
		</div>
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block">
 			</div>
 		</div>
	</div>
</section>
 	

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	<div class="row">
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing5.jpg" alt="plan">
      		<div class="caption">
        		<h4>Ancestry DNA </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing1.jpg" alt="plan">
      		<div class="caption">
        		<h4>Skin Genetics </h4>  
        		<p>Holistic approach to radiant and youthful skin  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p> 
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing3.jpg" alt="plan">
      		<div class="caption">
        		<h4>Health Horoscope </h4>  
        		<p>Know your future health  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing4.jpg" alt="plan">
      		<div class="caption">
        		<h4>Precision Medicine</h4>  
        		<p>Tailor your drug therapy  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 	</div>
 </div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text"> Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                        <div class="quote-text editContent pix_text">
                           Amit Roy
                       </div>         
                       <div class="quote-dots">. . .</div>        
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                    <div class="quote-text editContent pix_text">
                        Ritesh Jain
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                       
                    <div class="quote-text editContent pix_text">
                        Asha Verma
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				</div> 
 			</div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */
equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}
$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});
$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});
</script>


