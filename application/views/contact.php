<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
	<div class="banneroverlay"></div>
    <div class="container">
     	<div class="row">
      		<div class="col-md-12 area_1">
          		<h1 class="const_title"> Contact Us  </h1>    
       		</div>
        </div>
    </div>
</section>
<section class="contact-page">
	<div class="container">

		<div class="row">
			<div class="col-md-6 col-sm-6 address">
        		<div class="address-box">
        			<i class="fa fa-location-arrow"></i>
        			<h6> Address</h6>
        			<p>7th Floor, 714-716, Princess Business Skypark, <br> A.B. Road, Indore (M.P.), India. <br> Pin 452010 </p>
      	 		 </div>
      		</div>      

   		 	<div class="col-md-6 col-sm-6 address">
      		  	<div class="address-box">
        			<i class="fa fa-envelope"></i>
        			<h6> Email</h6>
        			<a href="mailto:info@jcgenetics.in">info@jcgenetics.in</a>
      		    </div>
     	    </div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">   
			 	<div class="contact-heading">
				 <h2>Connect with us</h2>       
				 <p>Reach out to our team for immediate assistance and support for all your inquiries</p>
				</div>

          <div class="flat-appointment">
            <form id="appointmentform" class="appointment-form contact-form for-full-width" method="post" action="./contact/appoinment-process.php">
            <div class="note"></div> 
            <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
            <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
            <div class="input-wrap wrap-left fullname">
              <input type="text" value="" tabindex="3" placeholder="Name *" name="name" id="fullname" required>
            </div>
            <div class="input-wrap wrap-right email">
              <input type="text" value="" tabindex="4" placeholder="Email *" name="email" required>
            </div>
            <div class="input-wrap wrap-left telephone">
              <input type="text" value="" tabindex="5" placeholder="Phone *" maxlength="10"  onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" name="telephone" id="telephone" required>
            </div>
            <div class="input-wrap wrap-right subject">
              <input type="text" value="" tabindex="5" placeholder="Subject *" name="subject" id="subject-1" required>
            </div>
		       <div class="textarea-wrap message">
              <textarea class="" tabindex="6" placeholder="Your Message" name="message" id="message" required></textarea>
            </div>
						<div class="alert alert-success text-center" id="conatactusNotitfy" style="display:none" ></div>
                <div class="send-wrap">
                   <button class="flat-button border">Submit <i class="material-icons">send</i></button>
                </div>
            </form><!-- /.appointment-form -->
          </div>
        </div><!-- /.col-md-8-->

        
	</div>
 </div>



</section>
<?php  $this->load->view('include/footer')?> 