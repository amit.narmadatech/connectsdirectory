<!-- Mobile Specific Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Bootstrap  -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/bootstrap.css">
<!-- Theme Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/style.css">
<!-- Custom css -->
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/responsive.css">
<!-- Colors -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/colors/color1.css" id="colors">  
<!-- Animation Style -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/animate.css">
<!-- Favicon and touch icons  -->
<link href="<?php echo base_url();?>assets/icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
<link href="<?php echo base_url();?>assets/icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
<link href="<?php echo base_url();?>assets/icon/favicon.png" rel="shortcut icon">
<!--[if lt IE 9]>     
<script src="<?php echo base_url();?>assets/javascript/html5shiv.js"></script>      
<script src="<?php echo base_url();?>assets/javascript/respond.min.js"></script>    
<![endif]-->        
</head>
<body class="page-template-front-page header-sticky home"> 

<section class="disclaimer-page">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">   
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
           
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

          </div>
      </div>
  </div>
</section>

<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/bootstrap.min.js"></script> </body>
</html>
