<!DOCTYPE html>
   <!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
   <!--[if (gte IE 9)|!(IE)]><!-->
   <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <!--<![endif]-->
   <head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>JC GENETICS - Towards a better life</title>      
    <meta name="description" content="JC GENETICS - Towards a better life">
    <meta name="keywords" content=" JC GENETICS - Towards a better life">
    <meta name="author" content="www.jcgenetics.in ">     
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/bootstrap.css">
    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/style.css">
    <!-- Custom css -->
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
        <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/responsive.css">
        <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/colors/color1.css" id="colors">  
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/stylesheets/animate.css">
    <!-- Favicon and touch icons  -->
    <link href="<?php echo base_url();?>assets/icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="<?php echo base_url();?>assets/icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="<?php echo base_url();?>assets/icon/favicon.png" rel="shortcut icon">
    <!--[if lt IE 9]>     
       <script src="<?php echo base_url();?>assets/javascript/html5shiv.js"></script>      
       <script src="<?php echo base_url();?>assets/javascript/respond.min.js"></script>    
   <![endif]-->       
</head> 
<body class="page-template-front-page header-sticky home">
<section id="page">
   <div class="container">    
        <div class="row">         
            <div class="col-md-12">
               <img class="img-responsive" src="<?php echo base_url();?>assets/images/instruction-mobile.jpg">

            </div>
       </div>
    </div>
</section>