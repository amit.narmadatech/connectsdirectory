<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
    	<div class="row">
       		<div class="col-md-12 area_1">
            	<h1 class="const_title"> Blog Details</h1>    
        	</div>
        </div>
    </div>
</section>
<section id="blog-page">
    <div class="container">
   		<div class="row">        
       		<div class="col-sm-8 ">          
        		<div class="post entry">
           			<!-- <h2 class="entry-title"><a href="<?php //echo base_url('home/blogdetails/'.$blogs->slug); ?>"><?php // echo !empty($blogs->title) ? $blogs->title : '' ?></a></h2>   -->   
                    <h2 class="entry-title"><?php  echo !empty($blogs->title) ? $blogs->title : '' ?></h2>

           			<div class="blog-img">			
						<?php if(!empty($blogs->blog_img) && file_exists("uploads/blog/".$blogs->blog_img)){
						$img = base_url().'uploads/blog/'.$blogs->blog_img;
						}else{
							$img = base_url().'assets/images/img_not_available.jpg';
						} ?>			
         	 			<img src="<?php echo $img; ?>" class="img-responsive " alt="<?php  echo !empty($blogs->title) ? $blogs->title : '' ?>">
     				</div>
           			 <div class="blog-description"><p> <?php echo !empty($blogs->description) ? $blogs->description : ''; ?></p></div>
          			<div class="entry-meta"> 
               			<span class="date"><?php echo !empty($blogs->created_date)?date("D, d M Y",strtotime($blogs->created_date)): '' ?></span>
              			<span class="author"><a href="#">Admin</a></span>
             		  <!--<span class="comment"><a href="#">0</a></span><span class="vote"><a href="#">0</a></span>-->
          			</div>
         			<?php
		  			   if(!empty($blogcomment)){
						foreach($blogcomment as $blogcomments){   
                                                    $userImage =  getUserImage($blogcomments->user_id);
                                                     
						 if (!empty($userImage) && file_exists("uploads/userimages/" .$userImage)) {
                                                        $img = base_url() . 'uploads/userimages/'.$userImage;
                                                    } else {
                                                        $img = base_url() . 'assets/images/avtar.jpg';
                                                    }
                                                 ?>
					<div class="media">
					  	<div class="media-left">
						 <a href="#">
                                                      <img class="media-object" src="<?php echo $img; ?>" alt="user images" style="width: 64px; height: 64px;">
                                                  </a>
					 	</div>
					 	<div class="media-body">
						<h6 class="media-heading"> <?php echo !empty($blogcomments->name)? $blogcomments->name :''; ?></h6>
					     <?php echo !empty($blogcomments->comment)? $blogcomments->comment :''; ?>
					 	</div>
					</div> 
				<?php }  }?>			
				<?php 
			  		 $userData = $this->session->all_userdata();	
                               ?>			
          		 <h2 class="post-heading">Post A Comment</h2>
                         <form name="blogcommentfrm" id="blogcommentfrm" method="POST" action="">
                              <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                               <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                             <div class="row">
                                 <div class="col-sm-6"> 
                                     <div class="form-group">
                                         <input type="text" name="name" value="<?php echo !empty($userData['fullname']) ? $userData['fullname'] : ''; ?>" class="form-control" placeholder="Name">
                                         <input type="hidden" data-id="<?php echo !empty($blogs->id) ? $blogs->id : '' ?>" name="blog_id" value="<?php echo!empty($blogs->id) ? $blogs->id : '' ?>" class="form-control">
                                     </div>
                                 </div>
                                 <div class="col-sm-6"> 
                                     <div class="form-group">
                                         <input type="email" name="email" value="<?php echo !empty($userData['email']) ? $userData['email'] : ''; ?>" class="form-control" placeholder="E-mail">
                                     </div>
                                 </div>               
                                 <div class="col-sm-12">
                                     <div class="form-group">
                                         <textarea name="message" class="form-control" placeholder="Type here message" rows="5"></textarea>
                                     </div>
                                 </div>
                                 <div class="col-sm-12 text-right">
                                     <button type="submit" id="sendBtn" class="flat-button border">Post Comment <i class="material-icons">send</i></button>
                                 </div>
                             </div>
                        
                         </form>	  
       		    </div>
    		</div>
                    <div class="col-sm-4">
                        <div class="bolg-sidebar">
                            <h4 class="title"><span>Recent Post</span></h4>
                            <ul>
                                <?php
                                if ($recentblog) {
                                    foreach ($recentblog as $recentblogs) {
                                        ?>
                                        <li><a href="<?php echo base_url('home/blogdetails/' . $recentblogs->slug); ?>"><?php echo!empty($recentblogs->title) ? $recentblogs->title : '' ?></a> </li>
                                    <?php }
                                }
                                ?>
                            </ul>         
                        </div>
                    </div>
                         <div id="notify" style="margin-top: 10px;" class="text-center" style="display:none"></div>
    		</div>
   		</div>
 	</div>
</section>
<?php  $this->load->view('include/footer');?>

<script>
	$(document).ready(function(){
	    $('#blogcommentfrm').validate({
		      rules: {
                                name: "required",
                                message: "required",
                                email:{
                                        required: true,
                                        email: true	,
                                }
				},
			submitHandler: function(form){
			
			   $('#sendBtn').prop('disabled', true);
			   $.post( "<?php echo base_url('home/blogcomment'); ?>",$('#blogcommentfrm').serialize(),function(response ) {
				    $('#sendBtn').prop('disabled', false);	
					 var obj = jQuery.parseJSON(response);
					if(obj.status){
					
						$('#notify').removeClass('alert alert-danger');
						$('#notify').html(obj.msg).addClass('alert alert-success').show().delay(3000).fadeOut("slow", function() {});
						
					}else{
						$('#notify').removeClass('alert alert-success');
						$('#notify').html(obj.msg).addClass('alert alert-danger').show().delay(3000).fadeOut("slow", function() {});
					}
				});
			},
			messages:{
					name:"Please enter your name",
					message:"Please enter your message",
				    email: {
					  required: "Please enter your email address",
					  email: "Please enter valid email address",
					}
			 },
		}); 
	});
	  
	
</script>



