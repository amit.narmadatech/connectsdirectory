<?php  $this->load->view('include/header')?> 


<section id="join_us_section">
	<div class="banneroverlay"></div>
     <div class="container">
      <div class="row">
        <div class="col-md-12 area_1">
            <h2 class="const_title"> Lactose intolerance </h2>    
        </div>
      </div>
    </div>
</section>

<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		 <h4>3 OUT OF 4 INDIANS ARE MILK INTOLERANT</h4>		
		 <p>We love milk but only 18% of us can digest it. Lactose intolerance is very common in India. Our body stops producing Lactase enzyme around late childhood.</p>
		  <span class="pull-right">Sanjay Gandhi P G I of medical sciences</span>
		<div class="clearfix"></div>
		 <h3>THESE ARE THE SYMPTOMS OF LACTOSE INTOLERANCE</h3>				
		</div>
	</div>

	<h4>THESE ARE THE SYMPTOMS OF LACTOSE INTOLERANCE</h4>
		<div class="row">
			<div class="col-sm-3">
				<div class="bg-info four-box">
					Bloating
			   </div>
			</div>
			<div class="col-sm-3"> 
				<div class="bg-primary four-box">
					Abdominal discomfort 
		  	  </div>
			</div>
			<div class="col-sm-3"> 
				<div class="bg-warning four-box">
					Diarrhea
				</div>
			</div>
			<div class="col-sm-3">
				<div class="bg-success four-box">
					Vomiting 
			  </div>
			</div>
		</div>

		<ul>
			<li>35% of individuals suffer from abdominal pain - Institute of public health.</li>
			<li>31% from gastrointestinal infections - Institute of public health.</li>
			<li>88% of south indians are lactose intolerant - SGPGI</li>
		</ul>
		<h4>TAKE THE JCGENETICS LACTOSE INTOLERANCE TEST TODAY TO AVOID SYMPTOMS.</h4>
	</div>
</section>

<?php  $this->load->view('include/footer')?> 