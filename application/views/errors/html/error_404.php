<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$base_url = load_class('Config')->config['base_url'];

?>
<?php  $this->load->view('include/header')?> 
<style type="text/css">
#error-page { padding: 120px 0; text-align: center; }
#error-page h1 {color: #E44157; font-size: 120px; margin-bottom: 35px;line-height: 1 }
#error-page h3 { margin-bottom: 25px; line-height: 1 }
</style>
<section id="error-page"> 
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">					
				<h1>404 </h1>
				<h3>OOPS! The Page You Are Looking Can't Be Found</h3>
				<a href="<?php echo base_url('home'); ?>" class="flat-button ">Back To Home <i class="fa fa-home"> </i></a>	
			</div>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 
</body>
</html>
