<?php  $this->load->view('include/header')?> 
<link href="<?php echo base_url();?>assets/css/jquery.steps.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/bootstrap-tagsinput.css" rel="stylesheet">
<?php $userData = $this->session->all_userdata();?>
<style type="text/css">
	.bootstrap-tagsinput {width: 100%;}
	.label {line-height: 2 !important;}
</style>
<section id="checkout">
    <div class="container">
        <div class="card">
            <div class="card-body">

                <form id="checkoutfrm" name="checkoutfrm" method="POST" action="<?php echo base_url('payment/paymentprocess'); ?>">
                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                     <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div>
                        <h3>Customer Information</h3>
                        <section>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">      
                                        <label for="name">Name *</label>     
                                        <input id="name" name="firstname" value="<?php echo!empty($userdetails->firstname) ? $userdetails->firstname : ''; ?>" type="text" class="required form-control">	
                                    </div>
                                    <span id="fnameerror"></span>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">    
                                        <label for="surname">Surname *</label>    
                                        <input id="surname" name="lastname" value="<?php echo!empty($userdetails->lastname) ? $userdetails->lastname : ''; ?>" type="text" class="required form-control">		         
                                    </div>
                                    <span id="lnameerror"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">         
                                    <div class="form-group">  
                                        <label for="email">Email *</label>             
                                        <input name="email" value="<?php echo!empty($userdetails->email) ? $userdetails->email : ''; ?>" type="text" class="required email form-control">
                                    </div>
                                    <span id="emailerror"></span>
                                </div>
                                <div class="col-sm-6"> 
                                    <div class="form-group">  	
                                        <label for="mobile">Mobile Number *</label>     
                                        <input id="mobile" name="mobile" value="<?php echo!empty($userdetails->contact1) ? $userdetails->contact1 : ''; ?>" type="text" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="13" class="required form-control">	
                                    </div>
                                    <span id="mobileerror"></span>
                                </div>
                            </div>
                        </section>
                        <h3>Shipping Address</h3>
                        <section>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address" data-error="wrong" data-success="right">Address</label>				
                                    <textarea id="address" name="address" type="text" required="required" class="md-textarea validate form-control"><?php echo!empty($userdetails->address) ? $userdetails->address : ''; ?></textarea>
                                </div>
                                <span id="addresserror"></span>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <select id="state" name="state" required="required" class="form-control validate">
                                                <option value="">Select State</option>
                                                <?php if (!empty($state)) {
                                                    foreach ($state as $states) {
                                                        ?>
                                                        <option value="<?php echo $states->state_id; ?>" <?php if (!empty($userdetails->state_id) && ($userdetails->state_id == $states->state_id)) { ?> selected <?php } ?> ><?php echo $states->state_title; ?></option>
                                            <?php }
                                        } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">	
                                            <label>District</label>
                                            <select id="district" name="district" required="required" class="form-control validate">
                                                <option value="">Select District</option>
                                                <?php if (!empty($districts)) {
                                                    foreach ($districts as $district) {
                                                        ?>
                                                        <option value="<?php echo $district->district_id; ?>" <?php if (!empty($userdetails->district_id) && ($userdetails->district_id == $district->district_id)) { ?> selected <?php } ?>><?php echo $district->district_title; ?></option>
                                                <?php }
                                            } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">					
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">    
                                            <label for="city" data-error="wrong" data-success="right">City</label>   
                                            <input id="city" name="city" value="<?php echo!empty($userdetails->city) ? $userdetails->city : ''; ?>"  type="text" required="required" class="form-control validate">   
                                        </div>
                                        <span id="cityerror"></span>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">	
                                            <label for="lanmark" data-error="wrong" data-success="right">Landmark</label>	
                                            <input id="lanmark" name="lanmark" value="<?php echo!empty($userdetails->landmark) ? $userdetails->landmark : ''; ?>"  type="text" required="required" class="form-control validate">	
                                        </div>
                                        <span id="lanmarkerror"></span>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">		
                                                    <label for="pincode" data-error="wrong" data-success="right">Pincode</label>
                                                    <input id="pincode" name="pincode" value="<?php echo!empty($userdetails->pincode) ? $userdetails->pincode : ''; ?>" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" type="text" required="required" maxlength="6" class="form-control validate">
                                                </div>
                                                <span id="pincodeerror"></span>
                                            </div>
                                            <div class="col-md-4 col-sm-4">  
                                                <div class="form-group">
                                                    <div class="branch-button">
                                                        <button type="button" class="flat-button border" id="checkPincodeAvailability">Check Pincode <i id="pinbtn" class="material-icons">send</i></button>
                                                        <br><span id="pinText"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4">  
                                                <div class="form-group">
                                                    <div class="branch-button">
                                                        <button type="button" class="flat-btn" onclick="GetLocation()" name="serachBtn" id="branchBtn">Search Near By JC Care/Retailer</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group" id="branchlist">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>				
                            </div>			
                        </section>
                        <h3>Order Summary</h3>
                        <section>
                            <div class="col-md-12">
                            <?php if (!empty($cart)) { ?>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Plan</th>
                                                    <th>Unit Price</th>
                                                    <th>Qty</th>
                                                    <th>Tax</th>
                                                    <th>Tax Amount</th>
                                                    <th class="text-right" style="width: 120px;">Total Price</th>
                                                </tr>
                                            </thead>
                                            <tbody id="orderSummaryDiv">
                                                <?php
                                                $total = 0;
                                                $item = 1;
                                                $tax=0;
                                                $tax_amount=0;
                                                $total_amount =0;
                                                foreach ($cart as $cartVal) {

                                                            if (file_exists("uploads/plan_img/" . $cartVal->plan_image) && !empty($cartVal->plan_image)) {
                                                                $img = base_url() . 'uploads/plan_img/' . $cartVal->plan_image;
                                                            } else {
                                                                $img = base_url() . 'assets/images/diet-plan.jpg';
                                                            }

                                                            $state_id = $this->session->tempstateid;
                                                            $tax = getGstTax($cartVal->package_id);
                                                            $amt = ($cartVal->qty * $cartVal->price);

                                                            if ($state_id == $tax['state_id']) 
                                                            {
                                                                $taxprnct =$tax['sgst_tax']+$tax['cgst_tax'];
                                                                $cgst_taxamt = ($amt * $tax['cgst_tax'])/100;
                                                                $sgst_taxamt = ($amt * $tax['sgst_tax'])/100;
                                                                $taxamt = ($amt * $taxprnct)/100;

                                                               $tax =" CGST " .$tax['cgst_tax'].'%'."<br> SGST ".$tax['sgst_tax'].'%'; 
                                                               $tax_amount = '<i class="fa fa-inr"></i>'. $cgst_taxamt ."<br><i class='fa fa-inr'></i> ".$sgst_taxamt ;
                                                            }else{
                                                                $taxprnct = $tax['igst_tax'];

                                                                 $tax ="IGST " .$tax['igst_tax'].'%'; 
                                                                 $taxamt = ($amt * $taxprnct)/100;
                                                                 $tax_amount = '<i class="fa fa-inr"></i>'.$taxamt;
                                                            }
                                                            
                                                            $total_amount = ($amt+$taxamt);
                                                            $total += $total_amount;
                                                    ?>			
                                                    <tr>
                                                        <th style="vertical-align: middle;"><?php echo $item++; ?></th>
                                                        <td style="vertical-align: middle;"><img src="<?php echo $img; ?>" class="cart-img"><?php echo!empty($cartVal->name) ? $cartVal->name : ''; ?></td>
                                                        <td style="vertical-align: middle;"><?php echo!empty($cartVal->price) ? number_format(round($cartVal->price),2) : ''; ?></td>
                                                        <td style="vertical-align: middle;"><?php echo!empty($cartVal->qty) ? $cartVal->qty : ''; ?></td>
                                                        <td style="vertical-align: middle;"><?php echo $tax; ?></td>
                                                        <td style="vertical-align: middle;"><?php echo $tax_amount;?> </td>
                                                        <td style="vertical-align: middle;" class="text-right"><?php echo!empty($total_amount) ? '<i class="fa fa-inr"></i> ' . number_format($total_amount,2) : ''; ?></td>
                                                    </tr>
                                            <?php } ?>		
                                            </tbody>
                                            <tfoot>								
                                                <tr>							
                                                    <td style="vertical-align: middle;" colspan="3" class="text-right retailer-box">
                                                        <div style="display: inline-block;">
                                                            <strong>JC Care/Retailer ID</strong>
                                                            <strong>
                                                                <input type="text" name="couponcode" id="code" onblur="/*freez(this)*/" class="form-control" value="" placeholder="Optional"  style="width:250px; display: inline-block;">
                                                                <button type="button" onclick="codeapply()"  id="codeapplyBtn" name="codeapplyBtn"> Apply</button>
                                                                <span id="codeerror"></span>
                                                            </strong>
                                                        </div>
                                                    </td>									
                                                    <td class="text-right discountCls" colspan="3"> <strong>Total Payable Amount :</strong></td>
                                                    <td  class="text-right">
                                                        <strong id="payamount" data-item="<?php echo $total; ?>"><?php echo '<i class="fa fa-inr"></i> ' . number_format($total,2); ?></strong>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-danger">Cart is empty<div>	
                                <?php } ?>
                                    </div>
                                    </section>
                                    <h3>Make a Payment</h3>
                                    <section>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group">
                                                  <!--  <div class="form-group">
                                                        <label class="radio-inline" for="paymenttype" data-error="wrong" data-success="right"> 
                                                        <input type="radio" id="paymenttype" name="paymenttype" value="payu"/>PayUMoney</label>
                                                    </div>-->
                                                  <h6>Please Select Payment Gateway</h6>



                                                  <div class="form-group payment-option">
                                                    <label class="radio-inline" data-error="wrong" data-success="right">
                                                            <input type="radio" name="paymenttype" value="ccavenue" checked="checked"/> <span class="payment-img"><img src="<?php echo base_url();?>assets/images/cc.png" alt="images"></span>
                                                        </label>
                                                          <label class="radio-inline" data-error="wrong" data-success="right"> 
                                                        <input type="radio" name="paymenttype" value="paytm"/> <span class="payment-img"><img src="<?php echo base_url();?>assets/images/paytm.png" alt="images"> </span></label>
                                                 
                                                        <label class="radio-inline" data-error="wrong" data-success="right"> 
                                                        <input type="radio" id="razorpay" name="paymenttype" value="razorpay"/><span class="payment-img"><img src="<?php echo base_url();?>assets/images/razorpay.png"></span></label>
                                                    </div>
                                                     
                                                    <!--  <div class="form-group">
                                                        <label class="radio-inline" data-error="wrong" data-success="right">
                                                            <input type="radio" name="paymenttype" value="ccavenue" checked="checked"/> <img src="<?php echo base_url();?>assets/images/ccavenue.jpg" alt="images">
                                                        </label>                                                      
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="radio-inline" data-error="wrong" data-success="right"> 
                                                        <input type="radio" name="paymenttype" value="paytm"/><img src="<?php echo base_url();?>assets/images/paytm.png" alt="images"> </label>
                                                    </div> -->
                                                </div>
                                            </div>
                                    </section>
                                </div>
                                </form>

                            </div>
                    </div>
            </div>
</section>
<?php  $this->load->view('include/footer') ?> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDI_PFMxsKHki7wrzSl-udZI4cqva7zTM"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/bootstrap-tagsinput.min.js');?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.min.js') ?>"></script>
<script type="text/javascript">
// Tooltips Initialization
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

		jQuery.validator.addMethod("noSpace", function(value, element) { 
		  return value.indexOf(" ") < 0 && value != ""; 
		}, "No space please and don't leave it empty");	
        	  
		var form = $("#checkoutfrm");
		form.validate({
			errorPlacement: function(error, element) {
	          if(element.attr("name") == "firstname"){
	              error.appendTo('#fnameerror');
	              return;
	          }else if(element.attr("name") == "lastname"){
	              error.appendTo('#lnameerror');
	              return;
	          }else if(element.attr("name") == "email"){
	              error.appendTo('#emailerror');
	              return;
	          }else if(element.attr("name") == "mobile"){
	              error.appendTo('#mobileerror');
	              return;
	          }else if(element.attr("name") == "address"){
	              error.appendTo('#addresserror');
	              return;
	          }else if(element.attr("name") == "pincode"){
	              error.appendTo('#pincodeerror');
	              return;
	          }else if(element.attr("name") == "lanmark"){
	              error.appendTo('#lanmarkerror');
	              return;
	          }else if(element.attr("name") == "state"){
	              error.appendTo('#stateerror');
	              return;
	          }else if(element.attr("name") == "city"){
	              error.appendTo('#cityerror');
	              return;
	          }else {
	            error.insertAfter(element);
	          }
	          },
			rules: {
				 firstname: {
					required: true,
					noSpace: true,
					minlength: 2,
					maxlength:16
				},
				lastname: {
					required: true,
					noSpace: true,
					minlength: 2,
					maxlength:16
				},
				mobile: "required",
				address: {
					required: true,
				},
				city: "required",
				state: "required",
				pincode: "required",
				landmark: "required",
				email:{
					required: true,
					email: true	
				}
			},
			messages: {
					firstname: {
						required: "Please enter first name",
						maxlength:"max length 16 characters",
						minlength: "Your first name must consist of at least 2 characters"
					},
					lastname: {
						required: "Please enter last name",
						maxlength:"max length 16 characters",
						minlength: "Your last name must consist of at least 2 characters"
					},
					mobile: "Please enter your mobile number",
					address: "Please enter your address",
					state: "select your state",
					city: "select your city",
					pincode: "Please enter your pincode",
					landmark: "Please enter your near landmark",
					email: { 
					required :"Please enter your email address",
					email:  "Please enter valid email address",
				}
			}
		});

		form.children("div").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			onStepChanging: function (event, currentIndex, newIndex)
			{
				form.validate().settings.ignore = ":disabled,:hidden";
				return form.valid();
			},
			onFinishing: function (event, currentIndex)
			{
				form.validate().settings.ignore = ":disabled";
				return form.valid();
			},
			onFinished: function (event, currentIndex)
			{
				$('#checkoutfrm').submit()
			}
		});

		$( "#state" ).change(function() {
                    $.post("<?php echo base_url('home/getdistrict')?>",{ state_id : this.value,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
                    if(response.status){
                      $('#district').html('');		
                      $('#district').html(response.result);		
                    }else{
                      console.log(response.status);
                      console.log(response.result);
                    }
                    },"json");			
		});		
		var state_id = $("#state").val();
		if(state_id){
		 //  $('#state').trigger("change");
		}

        var state_id = $('#state').val();
        if(state_id != '')
        {
            $.post('<?php echo base_url("cart/setTempStateTax"); ?>', {state_id: state_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) 
            {
                console.log(data.tempstateid);    
            },"json");

        }
        $(document).on('change', '#state', function(event) {
            var state_id = $('#state').val();
            $.post('<?php echo base_url("cart/setTempStateTax"); ?>', {state_id: state_id,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) 
            {
                console.log(data);
                $('#orderSummaryDiv').html(data.htmldata);    
                $('#payamount').html("<i class='fa fa-inr'></i>"+data.gtotal);    
            },"json");
        });

    });		
</script>   
<script type="text/javascript">
   
    function GetLocation() {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById("pincode").value;			
			if(address == ""){
			    alert('Please enter pincode');  
			}			
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    console.log("Latitude: " + latitude + "\nLongitude: " + longitude);
					getbranch(latitude , longitude);
                } else {
                    alert("Request failed.")
                }
            });
        };
     	
    function getbranch(latitude , longitude){	
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('cart/getbranch'); ?>',
            data: {latitude:latitude,longitude:longitude, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType:"json",
            success: function (response) {
             console.log(response.result);
             $('#branchlist').html(response.result);
            }
        });
    }	
    function codeapply(){	
        var code = $('#code').val();	
        if($.trim(code)== ""){
             $('#code').css('border-color','red');
             $('#codeerror').css('color','red').html('Please enter your code');
             return false;    
         }	
    $.ajax({
	    type: "POST",
	    url: '<?php echo base_url('cart/applycode'); ?>',
            data: {code:code ,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
	    dataType:"json",
	    success: function (response) {
                if(response.status){				
                       $('#codeerror').html('');
                       $('#code').css('border-color','').html('');
                        $('#codeerror').css('color','green').html('Code apply successfully.').fadeIn().delay(3000).fadeOut();
                }else{				  
                  $('#code').val('');
                  $('#code').css('border-color','red');
                  $('#codeerror').css('color','red').html(response.message);
                }
	    }
	});
    }
    function freez(obj){	
	    if(obj.value==''){		
	      var totalPrice = $('#ToatalAmt').attr('data-item');
		    $('#discount_row').html('');
			$('#payamount').html('₹ '+ totalPrice);
		} 
	}

    jQuery(document).ready(function($) {
        
        $(document).on('click', '#checkPincodeAvailability', function(event) {
        var pincode = $('#pincode').val();
           $.post('<?php echo base_url('cart/checkPincodeAvailability'); ?>', {pincode: pincode,'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
               if (data.status) {
                $('#pinbtn').html('done');
                $('#pinText').html(data.msg).css("color", "green").fadeIn().delay(3000).fadeOut();
               }else{
                    $('#pinbtn').html('send');
                     $('#pinText').html(data.msg).css("color", "red").fadeIn().delay(3000).fadeOut();
               }
           },"json");
        });

    });	
</script>