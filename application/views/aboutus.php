<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
  <div class="banneroverlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 area_1">
        <h1 class="const_title">About Us</h1>    
      </div>
    </div>
  </div>
</section>
<section id="about-page">
  <div class="container">
	  <div class="row">         
      <div class="col-md-12 col-sm-12">
        <h1 class="tagline">JC Genetics - <small> The science of knowing your self.</small></h1>
        <p style="font-style: italic;"><i> "Believe! truth, once found, would be simple as well as pretty."</i></p>
          
        -James Watson
          
        <br><br>
              
        <p>JC Genetics India is a Private Limited company (CIN: U74999MP2018PTC045167) operating from its head office situated at Indore, MP, India</p>
          
        <p>Our team is taking lead in addressing wellness services with the goal of making nation healthy and disease free with the help of non-invasive salivary DNA testing.</p>
          
        <p> We  specialize in personalized preventive healthcare. We provide a perfect blend of state of art genetic testing, artificial intelligence and human expertise for your better well-being. We are dedicated to empowering physicians, wellness professionals and individuals with the most validated, accurate and actionable genomic information, which in turn positively impacts and improves human health and quality of life. </p>            
             
        <p>All embedded in our Genes waiting to be uncovered. Every cell in our body contains DNA which can reveal everything about your past present and future. Let’s understand our differences.</p>
           
           
        <p class="prop"> <strong>Our speciality- </strong>  <?php if (!empty($planList)) { foreach ($planList as $plan) { ?><a href="<?php echo base_url("home/plandetail/$plan->slug"); ?>"><?php echo $plan->name; ?></a><?php } }  ?></p>          
            
<!--        
        <h2>“We help you catch disease before it catches you”</h2>
       	<p>Most of us must have uttered at one time in life that “It’s in my genes”. For some people, it means that eating a lot without gaining an ounce of weight, while for some others, it means an increased risk of a particular disease due to hereditary reasons.</p>
        <p>With the ever increasing risks to our body, taking routine treatment plan is not really enough. And every one is aware of numerous unsuccessful attempts to prevent obesity, metabolic, cardiac diseases. It is likely because making one size fits all plans often miss the mark.</p>
      -->
<!--        <p>JC Genetics is taking lead in addressing wellness services with the goal of making nation disease free with the help of non-invasive DNA testing process. We can help you understand yourself better and to have a long lasting impact on the way of prevention of a variety of diseases. It prevents and treat chronic diseases by understanding the present genotype, predicting the individual risks, and providing customization of nutrition therapy</p>
        <p>We test, understand, and counsel for your better health. Our prime purpose is to provide personalized diet and health solutions according to your genetic profile. We believe in providing good health and making nation fit.</p>
    -->
      </div>   
    </div>
  </div>
</section>


<section class="about-midcontent"> 
   <div class="container">
     <h2 class="text-center">Our Certifications</h2>
    <div class="row "> 
      <div class="col-md-12 col-sm-12 text-center">
        <ul class="certificate-list">
          <li>
             <img src="<?php echo base_url('assets/') ?>images/certificate1.jpg">
          </li>
          <li>     
           <img src="<?php echo base_url('assets/') ?>images/nablcertificate.jpg">
          </li>          
        </ul>  
      </div>
    </div>
    </div>
  </section>


  <section class="about-midcontent"> 
   <div class="container">
     <h2 class="text-center">Our Partners</h2>
    <div class="row "> 
      <div class="col-md-12 col-sm-12 text-center">
        <div class="wrap-team our-partner">
          <div class="flat-team-olw" data-item="5" data-nav="false" data-dots="false" data-auto="false">
            <div class="flat-team ">                            
              <img src="<?php echo base_url('assets/') ?>images/SLSPL_Logo.png">
            </div> 
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>




<!--
<section> 
   <div class="container">
    <div class="row about-midcontent">                    
      <div class="col-md-6 col-sm-6 mid-content ">
        <h2>Vision</h2>
        <p>Our vision is to make clinical genetic testing available and  affordable to you and your family. And to provide power of personalized medicine and genetics to guide them for their clinical and treatment choices. </p>
      </div> 
      <div class="col-md-6 col-sm-6">
        <img src="<?php echo base_url('assets/images/') ?>Vision.png" class="img-responsive center-block"> 
      </div>           
    </div>
  </div>
</section>
<section class="about-mission">
  <div class="container">
    <div class="row about-midcontent"> 
      <div class="col-md-6 col-sm-6 mid-content pull-right"> 
        <h2>Mission</h2>
        <p>Our mission is empowering individuals globally with knowledge about their genes and provide easy access of health care products and wellness services at affordable prices through our reliable, efficient, secure and technology driven distribution solutions and helping everyone move forward towards healthy and disease free future.</p>
      </div> 
      <div class="col-md-6 col-sm-6"> 
        <img src="<?php echo  base_url('assets/images/') ?>Mission.png" class="img-responsive center-block">  
      </div>         
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row about-midcontent">         
      <div class="col-md-6 col-sm-6 mid-content">
        <h2>Our Values</h2>                
        <ul class="why-us">
          <li>Bringing health preservations to new horizon.</li>
          <li>Empower wellness with the most validated actionable genomic information.</li>
          <li>Improve overall health and lifestyle to improve the quality of life.</li>
        </ul>
      </div>    
      <div class="col-md-6 col-sm-6">
        <img src="<?php //echo  base_url('assets/images/') ?>Values.png" class="img-responsive center-block">                
      </div>
    </div>
  </div>
</section>-->
<?php  $this->load->view('include/footer')?> 