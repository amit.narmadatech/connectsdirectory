<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php  $this->load->view('include/header')?> 

<section id="error-page"> 
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">					
				<h1>404 </h1>
				<h3>OOPS! The Page You Are Looking Can't Be Found</h3>
				 <p style="font-size: 18px;"><?php  if($this->session->flashdata('error')) {  ?>
					<?php echo $this->session->flashdata('error'); ?>
				 <?php } ?></p>			
				<a href="<?php echo base_url('home'); ?>" class="flat-button ">Back To Home <i class="fa fa-home"> </i></a>	
			</div>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 
</body>
</html>