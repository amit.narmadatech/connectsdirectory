<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
	<div class="banneroverlay"></div>
    <div class="container">
    	<div class="row">
       		<div class="col-md-12 area_1">
          		<h1 class="const_title"> Terms Of Use</h1>    
        	</div>
        </div>
    </div>
</section>
<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		
			<p>These terms and conditions (“T&C”) are between you and JC Genetics Pvt Ltd (“JC Genetics”, “we”, “our” or “us”) and governs your use of our services. You are availing our Services under the following T&Cs and any operating policies including the legal and privacy policy that may be published by JC Genetics from time to time. By your use of these Services and providing us with the registration and order form you agree to be bound by these T&C.</p>

			<h5> TERMS OF USE</h5>			
			<p>“Sample” or “DNA sample” shall mean saliva or any other biological sample accepted by JC Genetics for DNA or genetic analysis. “Genetic Assessment” or “Genetic Test” shall refer to any type of genetic analysis or pharmacogenetic analysis. JC Genetics may carry out such Genetic Test using the methods it deems fit and appropriate.</p>

			<p>“Services” will refer to the genetic assessment services performed by JC Genetics. You acknowledge and agree that Genetic Assessments are not yet considered as medically necessary (except those prescribed by your physician) and are purely undertaken at your discretion for information purposes only and not for clinical or medical purposes.</p>

			<p>You shall provide the DNA sample in the manner prescribed by JC Genetics and must be given in the tube provided for collection of sample in the product kit provided to you as part of the Services. You shall not damage the tube provided with the product kit and shall provide the sample in the manner as prescribed by JC Genetics.</p>

			<p>You agree that JC Genetics reserves the right to reject and destroy, without any further notice to you, any DNA sample provided by you which is not in the tube provided by JC Genetics in its product kit; is damaged or compromised during transit; or is sent without a complete registration and order form.</p>

			<h5>REPORT</h5>
			<p>You agree and acknowledge that the Genetic Test report (“Report”) to be provided by JC Genetics is not a diagnostic report and shall only be read and interpreted by a physician or qualified healthcare professional. You further acknowledge that the Report to be generated is only for your information and shall not be used in any court of law or any other legal or statutory proceeding. You shall expressly inform JC Genetics prior to submission of the DNA samples in case such Genetic Test is being conducted for any legal purposes and shall furnish all such documents as may be required by JC Genetics for verification and confirmation.</p>

			<p>JC Genetics or its channel partner shall supply the Report only to you i.e. the person who has provided the registration and order form and the DNA sample to JC Genetics or your legal representative or authorized person where such authorized person shall be required to provide the letter of authority satisfactory to JC Genetics or the channel partner, as the case maybe.</p>



			<h5>REPRESENTATIONS AND WARRANTIES</h5>

			<p>	You hereby represent, warrant and agree that: You have all the rights, and are authorized to possess and submit the DNA sample/data to JC Genetics for the purpose of generating the Genetic Test reports. By providing the DNA sample/data to JC Genetics, you do not infringe the rights or privileges of any third party.</p>


			<h5>DISCLAIMER</h5>

			<p>JC Genetics does not give medical advice nor provide medical or diagnostic services. The Report is only indicative and for information purposes only and is not a substitute for the professional judgment of any health care professional in diagnosing and treating patients. Your reliance upon the Report is solely at your own discretion. With regard to all health and medical related matters, you should exercise adequate care in using the information provided on our website or the Report. JC Genetics shall not be responsible for any findings in the Report. JC Genetics disclaims any responsibility for any errors and/or omissions by the sampler or agent either during collection of DNA samples or report generation or delivery.</p>

			<p>JC Genetics makes no warranties of any kind, either express or implied, including, without limitation, the implied warranties of merchantability, fitness for a particular purpose, accuracy and non-infringement.</p>

			<p>Not with standing the other terms of these T&C (A) JC Genetics will not be liable for any loss or any direct, indirect, consequential, exemplary, incidental, special or punitive damages relating to our services or any other information or content in respect of our services or otherwise in connection with your use of our services or due to any malpractice, even if we have been advised of the possibility of such damages, and (B) Our total cumulative liability in connection with our services and report and any other information and content in respect of our services and otherwise in connection with these t&c will at all times be limited to indian rupees one thousand (INR 1000/-). The existence of more than one claim will not enlarge this limit. In no event will our suppliers have any liability under or resulting from these T&C. You understand and agree that the foregoing limitation of liability is an agreed allocation of risk and an essential part of these T&C. You understand and agree that without your agreement to this limitation of liability provisions, we would not have provided the services to you as specified in these T&C. You agree that any such claim will be accepted only if made within thirty (30) days of the genetic test date.</p>
			
			<h5>CONFIDENTIALITY</h5>

			<p>Your personal information/data including your Report available with JC Genetics for the purpose of providing Services will be treated as strictly confidential in accordance with the privacy policy of JC Genetics and applicable laws and regulations.</p>

			<h5>INDEMNIFICATION</h5>
			<p>You agree to indemnify and hold JC Genetics, its subsidiaries, affiliates, directors, officers, agents, and employees harmless from any and all losses, damages, liabilities, claims, demands, costs, or expenses, including reasonable attorneys’ fees, relating to your: (a) use of our Services; (b) breach of these T&C; (c) breach or inaccuracy of any representation or warranty made under these T&C; or (d) violation of any applicable law or any right of any third party.</p>
			<h5>DISPUTE RESOLUTION</h5>
			<p>These T&C shall be governed by and construed in accordance with the laws of India. Any dispute or issue relating to these T&C or any transaction made pursuant thereto shall be submitted to the exclusive jurisdiction of the courts of Chennai, India.</p>

			<h5>GENERAL</h5>
			<p>JC Genetics may, in its sole and absolute discretion, revise these T&C from time to time. JC Genetics will post such revised T&C on its website from time to time. If you object to any such changes, your sole recourse shall be to cease availing Services of JC Genetics. Continued use of the Services following notice of any such changes shall constitute your acknowledgement of such changes and agreement to be bound by the revised terms and conditions.
			</p>

			<p>These T&C, together with any additional terms to which you agree when using JC Genetics’s Services, constitute the entire and exclusive and final statement of the agreement between you and JC Genetics with respect to the subject matter hereof, superseding any prior agreements or negotiations between you and JC Genetics with respect to such subject matter. The failure of JC Genetics to exercise or enforce any right or provision of these T&C shall not constitute a waiver of such right or provision. If any provision of these T&C is found by a court of competent jurisdiction to be invalid, you nevertheless agree that the court should endeavour to give effect to the intentions of JC Genetics and you as reflected in the provision, and that the other provisions of these T&C remain in full force and effect. The clause titles in these T&C are for convenience only and have no legal or contractual effect. These T&C shall remain in full force and effect notwithstanding any termination of your use of the JC Genetics’s Services. These T&C will be interpreted without application of any strict construction in favour of or against you or JC Genetics.</p>

			<h5>COMMUNICATION</h5>
			<p>	If you have any questions about these T&C or other concerns, you can contact us at +91 731 2441000. <a href="mailto:info@jcgenetics.in">info@jcgenetics.in</a>. </p>
			
		
			</div>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 