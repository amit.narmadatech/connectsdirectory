<?php $this->load->view('include/header') ?> 
<?php $userData = $this->session->all_userdata(); ?>

<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 area_1">
                <h1 class="const_title"> FAQs </h1>
            </div>
        </div>
    </div>
</section>
<section id="page-faq">
  <div class="container">    
    <div class="row">         
      <div class="col-md-12">   

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="fa fa-plus-circle"></i> What is genetic testing?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p>Genetic testing is a process to understand your genes, which are DNA instructions inherited from parents. Genetic tests are conducted to identify the increased health risks and guide in choosing right treatments or assess treatment responses. </p>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="fa fa-plus-circle"></i>  What I will learn from genetic testing?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <ul>
          <li> Diagnosis of diseases </li>
          <li> Identification of gene changes </li>
          <li> Determination of disease severity </li>
          <li> Identification of gene that can be passed on to children </li>
          <li> Identification of increased risks to develop certain disease </li>
          <li> Providing guidance to doctors for customized treatment or medicines</li>
        </ul>
      </div>
    </div>
  </div>
 
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="fa fa-plus-circle"></i>  Why should I opt for genetic test?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <p> Genetic test results provide personal genetic data that can help you to make informed health decisions like dietary habits, fitness goals, nutrition, weight management, treatment of diseases that are best for your body.</p>
      </div>
    </div>
  </div>


  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingfour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour"> <i class="fa fa-plus-circle"></i>  How do I decide about the suitable genetic test plan?
        </a>
      </h4>
    </div>
    <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
      <div class="panel-body">
       <p> There are various reasons people might consider to get the genetic testing. Doctors can also suggest genetic test, if anyone in the family of patient have certain disease patterns. Our genetic counselor can help you decide the best plan for you according to your requirements.</p>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingfive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive"> <i class="fa fa-plus-circle"></i>  How does genetic testing work?
        </a>
      </h4>
    </div>
    <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
      <div class="panel-body">
       <p> Genetic testing looks at the specific gene or DNA code to provide information about the gene(s) and it is performed on your saliva sample. Results are usually sent within 3-5 weeks. </p>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingsix">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapsesix"> <i class="fa fa-plus-circle"></i>  Who is a genetic counsellor?
        </a>
      </h4>
    </div>
    <div id="collapsesix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsix">
      <div class="panel-body">
       <p>Genetic counsellors are our team members. They are trained individuals and specializes in helping individuals understand and manage their health in best possible manner. </p>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingseven">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseseven" aria-expanded="false" aria-controls="collapseseven"> <i class="fa fa-plus-circle"></i>  What will a genetic counsellor do?
        </a>
      </h4>
    </div>
    <div id="collapseseven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingseven">
      <div class="panel-body">
       <ul>
          <li>Genetic counsellor will: </li>
          <li>Confirm your family and medical history </li>
          <li>Explain the causes of hereditary diseases </li>
          <li>Discuss the gene testing process</li>
          <li>Determine if you are at risk of any hereditary disease </li>
          <li> Allow you to make an informed decision for your better health </li>
          <li> Provide information about gene testing and overview of results </li>
        </ul>
      </div>
    </div>
  </div>

   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingeight">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseeight" aria-expanded="false" aria-controls="collapseeight"> <i class="fa fa-plus-circle"></i> How do I understand my genetic test results? 
        </a>
      </h4>
    </div>
    <div id="collapseeight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingseeight">
      <div class="panel-body">
       <p> You can review the test results on your own. You can also consult with our genetic counsellor who will help you to analyze results, highlight the sections of test and guide you to reach your goals. We will also provide recommendations on the basis of your test results.</p>
      </div>
    </div>
  </div>

   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingnine">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsenine" aria-expanded="false" aria-controls="collapsenine"> <i class="fa fa-plus-circle"></i>  Do you need any doctor referral for genetic testing? 
        </a>
      </h4>
    </div>
    <div id="collapsenine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsenine">
      <div class="panel-body">
       <p>  No, usually there is no requirement of any referral.</p>
      </div>
    </div>
  </div>
</div>


  <!-- <h5> What is genetic testing?</h5>
   <p> Genetic testing is a process to understand your genes, which are DNA instructions inherited from parents. Genetic tests are conducted to identify the increased health risks and guide in choosing right treatments or assess treatment responses. </p>

  <h5> What I will learn from genetic testing?</h5>
   <p> Genetic tests will help in: </p>
     <ul>
      <li> Diagnosis of diseases </li>
      <li> Identification of gene changes </li>
      <li> Determination of disease severity </li>
      <li> Identification of gene that can be passed on to children </li>
      <li> Identification of increased risks to develop certain disease </li>
      <li> Providing guidance to doctors for customized treatment or medicines</li>
    </ul>

    <h5> Why should I opt for genetic test?</h5>
    <p> Genetic test results provide personal genetic data that can help you to make informed health decisions like dietary habits, fitness goals, nutrition, weight management, treatment of diseases that are best for your body.</p>

    <h5> How do I decide about the suitable genetic test plan?</h5>
    <p> There are various reasons people might consider to get the genetic testing. Doctors can also suggest genetic test, if anyone in the family of patient have certain disease patterns. Our genetic counselor can help you decide the best plan for you according to your requirements.</p>

    <h5> How does genetic testing work?</h5>
    <p> Genetic testing looks at the specific gene or DNA code to provide information about the gene(s) and it is performed on your saliva sample. Results are usually sent within 3-5 weeks. </p>
           
    <h5>Who is a genetic counsellor?</h5>
    <p>Genetic counsellors are our team members. They are trained individuals and specializes in helping individuals understand and manage their health in best possible manner. </p>

    <h5> What will a genetic counsellor do?</h5>
      <ul>
        <li>Genetic counsellor will: </li>
        <li>Confirm your family and medical history </li>
        <li>Explain the causes of hereditary diseases </li>
        <li>Discuss the gene testing process</li>
        <li>Determine if you are at risk of any hereditary disease </li>
        <li> Allow you to make an informed decision for your better health </li>
        <li> Provide information about gene testing and overview of results </li>
      </ul>  

    <h5> How do I understand my genetic test results? </h5>
      <p> You can review the test results on your own. You can also consult with our genetic counsellor who will help you to analyze results, highlight the sections of test and guide you to reach your goals. We will also provide recommendations on the basis of your test results.</p>

      <h5> Do you need any doctor referral for genetic testing? </h5>
      <p> No, usually there is no requirement of any referral.</p>         -->    
      </div>
    </div> 
  </div>
</section>
<?php $this->load->view('include/footer') ?> 
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
          $(this).siblings(".panel-heading").find(".fa").addClass("fa-minus-circle").removeClass("fa-plus-circle");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
          $(this).parent().find(".fa").removeClass("fa-plus-circle").addClass("fa-minus-circle");
        }).on('hide.bs.collapse', function(){
          $(this).parent().find(".fa").removeClass("fa-minus-circle").addClass("fa-plus-circle");
        });
    });
</script>