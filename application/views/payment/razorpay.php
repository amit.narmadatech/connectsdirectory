<head>
    <meta name="viewport" content="width=device-width">
</head>
<form action="<?php echo base_url('payment/razorpayconfirm')?>" method="POST">
<script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="<?php echo $keyId; ?>"
    data-amount="<?php echo $display_amount; ?>"
    data-buttontext="Pay with Razorpay"
    data-name="<?php echo $name; ?>"
    data-description=""
    data-image="<?php echo $image; ?>"
    data-prefill.name="<?php echo $name ?>"
    data-prefill.email="<?php echo $email ?>"
    data-prefill.contact="<?php echo $mobile ?>"
    data-theme.color="#F37254"
></script>
<input type="hidden" value="<?php echo $user_id ?>" name="user_id">
<input type="hidden" value="<?php echo $branch ?>" name="branch">
<input type="hidden" value="<?php echo $couponcode ?>" name="couponcode">
<input type="hidden" value="<?php echo $order_id ?>" name="order_id">
</form>
  <script src="<?php echo  base_url("assets/js/");?>jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
    jQuery(function(){
        
           jQuery(".razorpay-payment-button").trigger("click");
           jQuery(".razorpay-payment-button").click();
           $('.razorpay-payment-button').hide();
           
            var options = {
                    "key": "<?php echo $keyId ;?>",
                    "amount": "<?php echo $display_amount ;?>",
                    "name": "<?php echo $name ;?>",
                    "description": "",
                    "image": "<?php echo $image ;?>",
                    "handler": function (response){
                        alert(response.razorpay_payment_id);
                    },
                    "modal": {
                        "ondismiss": function(){
                            alert(1);
                        }
                    }
            };
         var rzp1 = new Razorpay(options);
       });
    </script>