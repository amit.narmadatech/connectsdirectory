<html>
    <head>
        <script>
            window.onload = function () {
                var d = new Date().getTime();
                document.getElementById("tid").value = d;
            };
        </script>
    </head>
    <body>
        <form method="POST" name="customerData" id="payuindia_payment_form" action="<?php echo base_url('ccavenue/ccavRequestHandler.php'); ?>">
            <input type="hidden" name="tid" id="tid" readonly />
            <input type="hidden" name="merchant_id" value="<?php echo $merchant_id; ?>" />
            <input type="hidden" name="order_id" value="<?php echo $order_id; ?>"/>
            <input type="hidden" name="amount" value="<?php echo $total_price; ?>"/>
            <!-- <input type="hidden" name="amount" value="1.0" /> -->
            <input type="hidden" name="billing_name" value="<?php echo $name; ?>"/>
            <input type="hidden" name="billing_zip" value="<?php echo $pincode; ?>"/>
            <input type="hidden" name="billing_country" value="India"/>
            <input type="hidden" name="billing_state" value="Madhya Pradesh"/>
            <input type="hidden" name="billing_city" value="Indore"/>
            <input type="hidden" name="billing_address" value="vijay nagar indore"/>
            <input type="hidden" name="billing_email" value="<?php echo $email; ?>"/>
            <input type="hidden" name="billing_tel" value="<?php echo $mobile; ?>"/>
            <input type="hidden" name="currency" value="INR"/> 
            <input type="hidden" name="merchant_param1" value="<?php echo $user_id; ?>" />
            <input type="hidden" name="merchant_param2" value="<?php echo $branch; ?>" />
            <input type="hidden" name="merchant_param3" value="<?php echo $couponcode; ?>" />
            <input type="hidden" name="redirect_url" value="<?php echo base_url('payment/confirm'); ?>"/>
            <input type="hidden" name="cancel_url" value="<?php echo base_url('payment/paymentcancel'); ?>"/>
            <input type="hidden" name="language" value="EN"/>
            <!-- <input type="submit" value="CheckOut"> -->
            <div class="main_body" id="loadingimage" style="display: block;">
                <div class="center" style="text-align: center;font-size: 30px;">Be patient! <br>We are redirecting to you on payment gateway.<br> Please don\'t refresh page.</div>
                <img style="margin: 0 auto; display:block;" src="<?php echo base_url('assets/images/payment-gateway-loader.gif'); ?>" />
            </div>	
            <script type="text/javascript">
                //document.getElementsByClassName("submit_payuindia_payment_form")[0].submit();
                setTimeout(function () {
                    document.getElementById("payuindia_payment_form").submit();
                }, 500);
            </script>
        </form>
    </body>
