<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('include/header') ?> 
<section class="sucesspage">
    <div class="container">
        <div class="row">         
            <div class="col-md-8 col-md-offset-2">
                <div class="success-content text-center">
                    <div class="text-center sucess-header">
                        <i class="fa fa-check-circle green-text" aria-hidden="true"></i>
                        <h4 class="green-text">Transaction Successful </h4>
                    </div>        		 
                    <?php
                    if ($this->session->flashdata('item')) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <p>Thank You. Your order status is successful</br>
                            Your Transaction ID for this transaction is <strong><?php echo!empty($message['txnid']) ? $message['txnid'] : ''; ?></strong></br>
                            We have received a payment of Rs.<span><?php echo!empty($message['amount']) ? $message['amount'] : ''; ?></span> Your Kit  will be dispatched soon.</p>
                    <?php }
                    ?>
                    <p class="text-center btn-sucesspage">
                        <a href="<?php echo base_url('myorder'); ?>" class="flat-btn">
                            Click here for order list </a>
                    </p>        	
                </div>            
            </div>      
        </div>
    </div>
</section>
<?php $this->load->view('include/footer') ?> 