<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
    </div>
    <div class="col-sm-8 col-md-9">
        <div class="user-content">
            <?php $this->load->view('include/alert'); ?>	
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">			
                    <div class="page-header">
                        <h1>Pickup Request</h1>
                    </div>
                    <form id="pickupForm" name="pickupForm" method="post" action="<?= base_url('user/post_pickup'); ?>">
                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                    <input type="hidden" id="token" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                        <!-- <div class="row">
                            <div class="col-sm-12">	
                                <div class="form-group">			
                                    <label for="firstname">Address</label>		
                                    <textarea class="form-control" id="address" name="address"><?php echo (!empty($userDetails->address)) ? $userDetails->address :''; ?></textarea>				
                                    <?php echo form_error('address', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                             <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="contact_person">PinCode</label>          
                                    <input type="text" class="form-control" id="pincode" name="pincode"  value="<?php echo (!empty($userDetails->pincode)) ? $userDetails->pincode : set_value('pincode') ?>">
                                    <?php echo form_error('contact_person', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div> -->
                        <!-- <div class="row">
                            <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="contact_person">Contact Name</label>          
                                    <input type="text" class="form-control" id="contact_person" name="contact_person"  value="<?= (isset($userDetails) && (!empty($userDetails->name))) ? $userDetails->name : set_value('contact_person') ?>">

                                    <?php echo form_error('contact_person', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">	
                                <div class="form-group">		
                                    <label for="email">Email</label>			
                                    <input type="email" class="form-control" name="email" value="<?php echo (!empty($userDetails->email)) ? $userDetails->email : set_value('pincode') ?>">
                                    <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                            
                        </div> -->
                        
                        <div class="row">
                            <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="date">Pickup Date</label>
                                    <?php $start_date = date('Y-m-d'); ?>                  
                                    <input type="date" class="form-control" id="date" name="date" min="<?php echo $start_date; ?>">
                                    <?php echo form_error('date', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="time">Preferred Time</label>                  
                                    <select class="form-control" id="time" name="time">
                                        <option value="morning">Morning</option>
                                        <option value="afternoon">Afternoon</option>
                                        <option value="evening">Evening</option>
                                    </select>

                                    <?php echo form_error('time', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="order_number">Order Number</label>                  
                                    <input type="text" class="form-control" id="order_number" name="order_number" >

                                    <?php echo form_error('order_number', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">  
                                <div class="form-group">    
                                    <label for="quantity">Number of kits</label>                  
                                    <input type="number" min="1" class="form-control" id="quantity" name="quantity" value="1">

                                    <?php echo form_error('quantity', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">  
                                <div class="form-group">    
                                    <label for="barcode">Barcode (use comma for seprating multiple barcodes)</label>
                                    <input type="text" class="form-control" id="barcode" name="barcode" >                

                                    <?php echo form_error('barcode', '<div class="text-danger">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="flat-button border" value="Submit">Submit <i class="material-icons">send</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
<?php $this->load->view('include/footer'); ?>

<script type="text/javascript">
    $(document).ready(function($) {

        $('#pickupForm').validate({
            submitHandler: function(form) {
                form.submit();
            },
            ignore: [],
            rules: {
                //address: "required",
                //contact_person: "required",
                mobile: {
                    number: true,  // <-- no such method called "matches"!
                    minlength:10,
                    maxlength:10
                },
                quantity: "required",
                date: "required",
                time: "required",
                //order_number: "required",
                barcode: "required",
                /*email: {
                    required: true,
                    email: true
                }*/
            },
            messages:{},
          /*  highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }*/
        });
        

    });
</script>