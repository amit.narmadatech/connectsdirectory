<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">
                    <?php $this->load->view('include/alert'); ?>	
                    <div class="row">
                        <?php $oldimg = (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>	

                        <div class="col-md-12 col-sm-12 col-xs-12">			
                            <div class="page-header">
                                <h1>Profile</h1>
                            </div>
                            <form method="post" enctype="multipart/form-data" action="<?= base_url('user/update_profile'); ?>">
                                  <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                                  <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                <input type="hidden" name="oldimg" value="<?php echo (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>">

                                <div class="row">
                                    <div class="col-sm-6">	
                                        <div class="form-group">			
                                            <label for="firstname"> Name</label>		
                                            <input type="text" class="form-control" id="firstname" name="firstname"  value="<?= (isset($user) && (!empty($user->firstname))) ? $user->firstname : set_value('firstname') ?>">					
                                            <?php echo form_error('firstname', '<div class="text-danger">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">	
                                        <div class="form-group">	
                                            <label for="lastname">Surname</label>			
                                            <input type="text" class="form-control" id="lastname" name="lastname"  value="<?= (isset($user) && (!empty($user->lastname))) ? $user->lastname : set_value('lastname') ?>">

                                            <?php echo form_error('lastname', '<div class="text-danger">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">	
                                        <div class="form-group">		
                                            <label for="email">Email</label>			
                                            <input readonly type="email" class="form-control" id="email" name="email" value="<?= (isset($user) && (!empty($user->email))) ? $user->email : set_value('email') ?>">

                                            <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">	
                                        <div class="form-group">	
                                            <label for="mobile">Mobile</label>					
                                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo!empty($user->contact1) ? $user->contact1 : ''; ?>">

                                            <?php echo form_error('mobile', '<div class="text-danger">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">	
                                        <div class="form-group">		
                                            <label for="userimage">Profile Image</label>			
                                            <input type="file" class="" id="userimage" name="userimage" onchange="previewImage(this, 'userimage');" >
                                            <?php echo form_error('userimage', '<div class="text-danger">', '</div>'); ?>
                                        </div>	
                                    </div>
                                </div>	
                                <div class="form-group">
                                    <button type="submit" class="flat-button border" value="Submit">Submit <i class="material-icons">send</i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('include/footer'); ?>