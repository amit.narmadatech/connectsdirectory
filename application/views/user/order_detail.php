<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('include/header'); ?>
<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
    </div>
     <div class="col-sm-8 col-md-9">
        <div class="user-content">
            <?php $this->load->view('include/alert'); ?>
            <div class="page-header">
                <h1>Order Detail </h1>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php if (!empty($orders)) { ?>		  
                        <div class="row order-detail">
                            <!--Grid column-->
                            <div class="col-md-12">
                                <div class="well">                        
                                    <div class="row">
                                        <div class="col-sm-4"> 
                                            <h4>Order Details</h4>
                                            <p> <strong>GSTIN  :</strong> 23AAECJ1917G1ZX</p>
                                            <p><strong>Order Number  :</strong> <?php echo!empty($orders->order_number) ? $orders->order_number : ''; ?></p>
                                            <p><strong>Order Date & Time : </strong> <?php echo!empty($orders->order_date) ? date('d-m-Y h:i A', strtotime($orders->order_date)) : ''; ?> </p>
                                            <p><strong>Payment Mode :</strong> <?php echo!empty($orders->payment_mode) ? $orders->payment_mode : ''; ?></p> 
                                        </div>
                                        <div class="col-sm-5">
                                            <h4>Shipping Address </h4>
                                            <strong><?php echo!empty($orders->name) ? $orders->name : ''; ?></strong>
                                            <p>
                                                <?php echo!empty($orders->address) ? $orders->address : ''; ?>,
                                                <?php echo!empty($orders->landmark) ? $orders->landmark : ''; ?>,
                                                <?php echo !empty(getDistrict($orders->district_id)) ? getDistrict($orders->district_id)->district_title : ''; ?>,
                                                <?php echo !empty(getState(getDistrict($orders->district_id)->state_id)) ? getState(getDistrict($orders->district_id)->state_id)->state_title : ''; ?>
                                            </p>								
                                            <p><?php echo!empty($orders->mobile1) ? $orders->mobile1 : ''; ?></p>
                                            <p><?php echo!empty($orders->email) ? $orders->email : ''; ?></p>
                                        </div>
                                        <div class="col-sm-3">
<!--                                            <h4>Manage Order</h4>-->
                                            <a target="_blank" href="<?php echo base_url('user/downloadinvoice/' . $orders->id); ?>" class="flat-button border"> Invoice <i class="fa fa-download"></i></a>
                                        </div>
                                    </div>      
                                </div>                        
                            </div>
                        </div>

                        <div class="row">
                            <!--Grid column-->
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Plan</th>
                                                <th>Plan Name</th>
                                                <th>Price/Qty </th>
                                                <th>Tax </th>
                                                <th>Tax Amount </th>
                                                <th class="text-right">Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                         //   print_r($orders);
                                                $item = 1;
                                                $tax=0;
                                                $tax_amount =0;
                                                $total_amount =0;
                                            if (!empty($orderDetails)) {
                                                foreach ($orderDetails as $orderDetailsval) {
                                                    
                                                    if (file_exists("uploads/plan_img/" . $orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image)) {

                                                        $img = base_url() . 'uploads/plan_img/' . $orderDetailsval->plan_image;
                                                    } else {
                                                        $img = base_url() . 'assets/images/img_not_available.jpg';
                                                    }
                                                    
                                                        $amount = ($orderDetailsval->quantity * $orderDetailsval->plan_price);
                                                        $tax = getGstTax($orderDetailsval->package_id);
                                                       
                                                        if ($orders->state_id == $tax['state_id']) {
                                                            
                                                                $taxprnct =$tax['sgst_tax']+$tax['cgst_tax'];
                                                                $cgst_taxamt =($amount * $tax['cgst_tax'])/100;
                                                                $sgst_taxamt =($amount * $tax['sgst_tax'])/100;
                                                                $taxamt = ($amount * $taxprnct)/100;
                                                                $total_amount = ($amount+$taxamt);

                                                             $tax = " CGST " . $tax['cgst_tax'] . '%' . "<br> SGST " . $tax['sgst_tax'] . '%';
                                                             $tax_amount = '<i class="fa fa-inr"></i> ' . $cgst_taxamt . "<br><i class='fa fa-inr'></i> " . $sgst_taxamt;
                                                         } else {
                                                             
                                                                 $taxprnct = $tax['igst_tax'];
                                                                 $tax ="IGST " .$tax['igst_tax'].'%'; 
                                                                 $taxamt = ($amount * $taxprnct)/100;
                                                                 $total_amount = ($amount+$taxamt);
                                                                 $tax_amount = '<i class="fa fa-inr"></i> '.$taxamt;
                                                         }
                                                    ?>      
                                                    <tr>
                                                        <td>           
                                                            <img src="<?php echo $img; ?>" class="img-responsive" width="120" height="80">
                                                        </td>
                                                        <td> 
                                                            <p> <?php echo!empty($orderDetailsval->name) ? $orderDetailsval->name : ''; ?> </p>
                                                        </td>
                                                        <td> 
                                                            <p> <i class="fa fa-inr"></i> <?php echo!empty($orderDetailsval->plan_price) ? $orderDetailsval->plan_price : ''; ?>
                                                                  X <?php echo!empty($orderDetailsval->quantity) ? $orderDetailsval->quantity : ''; ?> 
                                                            </p>
                                                        </td> 
                                                        <td><?php echo $tax ;?></td> 
                                                        <td><?php echo $tax_amount;?> </td> 
                                                         <td class="text-right"> 
                                                             <p class="pull-right"><i class="fa fa-inr"></i> <?php echo!empty($total_amount) ? $total_amount : ''; ?></p>
                                                        </td>
                                                    </tr>                 
                                            <?php }
                                        } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" class="text-right">Grand Total :</td>
                                                <td class="text-right"> <i class="fa fa-inr"></i> <?php echo!empty($orders->total_amount) ? $orders->total_amount : ''; ?></td>
                                            </tr>
                                        </tfoot>
                                    </table> 
                                </div>
                        <?php } else { ?>
                                <div class="alert alert-danger text-center">No order details available</div>	   
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>
</section>
<?php $this->load->view('include/footer'); ?>