<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('include/header'); ?>
<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">		  	
                    <div class="page-header">

                        <h1>My Tickets <a href="<?= base_url('create-ticket') ?>" class="flat-button border pull-right">Add Ticket <i class="material-icons">add</i></a></h1>
                    </div>		 
                    <div class="table-responsive"> 
                        <?php if (!empty($ticketData)) { ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 115px;">Ticket Code</th>
                                        <th>Subject</th>
                                        <th>Date</th> 
                                        <th>Priority</th>
                                        <th>Department</th>
                                        <th>Status</th>
                                        <th style="width: 90px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ticketData as $ticketDataVal) { ?>		
                                        <tr>
                                            <td><?php echo!empty($ticketDataVal->ticket_code) ? $ticketDataVal->ticket_code : ''; ?></td>
                                            <td><a class="ticket-subject" href="<?= base_url('user/ticketview/' . $ticketDataVal->tickets_id) ?>"><?php echo!empty($ticketDataVal->subject) ? $ticketDataVal->subject : ''; ?></a></td>
                                            <td> <?php echo!empty($ticketDataVal->created) ? date('d-m-Y h:i A', strtotime($ticketDataVal->created)) : ''; ?></td>
                                            <td>
                                                <?php if (!empty($ticketDataVal->priority) && ($ticketDataVal->priority == 1)) { ?>
                                                    <span class="label label-info">Low</span>
                                                <?php } else if (!empty($ticketDataVal->priority) && ($ticketDataVal->priority == 2)) { ?>
                                                    <span class="label label-warning">Medium</span>
                                                <?php } else if (!empty($ticketDataVal->priority) && ($ticketDataVal->priority == 3)) { ?>
                                                    <span class="label label-danger">High</span>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo!empty($ticketDataVal->departments_id) ? $ticketDataVal->departments_id : ''; ?></td>
                                            <td>	
                                                <?php if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 1)) { ?>
                                                    <span class="label label-success">Close</span>
                                                <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 2)) { ?>	
                                                    <span class="label label-info">Inprogress</span>
                                                <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 3)) { ?> 
                                                    <span class="label label-primary">Pending</span>	
                                                <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 4)) { ?>
                                                    <span class="label label-info">Answer</span>
                                                <?php } else { ?>
                                                    <span class="label label-danger">Open</span>
                                                <?php } ?>								
                                            </td>
                                            <td>
                                                <a href="<?= base_url('user/ticketview/' . $ticketDataVal->tickets_id) ?>" data-toggle="tooltip" data-placement="top" title="view" class="btn-floating btn-warning"><i class="material-icons">&#xE417;</i></a>
                                                <a href="javascript:void(0)" onclick="deleteticket('<?php echo $ticketDataVal->tickets_id; ?>')" class="btn-floating btn-black deleteuser" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons">&#xE872;</i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>					
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="7"> 
                                            <nav aria-label="Page navigation">
                                                <?php echo $links; ?>
                                            </nav>
                                        </td>
                                    </tr>
                                </tfoot>						
                            </table>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                <strong>Info!</strong> NO TICKETS AVAILABLE.
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootbox.min.js"></script>
<script>
                                            function deleteticket(id) {
                                                bootbox.confirm({
                                                    title: "Delete Ticket",
                                                    message: "Are you sure you want to delete this ticket",
                                                    buttons: {
                                                        confirm: {
                                                            label: 'Yes',
                                                            className: 'btn-success'
                                                        },
                                                        cancel: {
                                                            label: 'No',
                                                            className: 'btn-danger'
                                                        }
                                                    },
                                                    callback: function (result) {
                                                        if (result) {
                                                            window.location.href = '<?php echo base_url("user/deleteticket"); ?>' + '/' + id;
                                                        }
                                                    }
                                                });
                                            }

 $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>