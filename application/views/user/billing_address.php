<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
  <div class="container inner-container">
  	<div class="row">
            <div class="col-sm-4 col-md-3">

        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
</div>
 <div class="col-sm-8 col-md-9">

    <div class="user-content">
	<?php $this->load->view('include/alert'); ?>
	
	<div class="row">
		<?php 
			$imgpath = (isset($edit) && (!empty($edit->image)))?base_url('uploads/userimages/'.$edit->image):base_url('uploads/userimages/default.jpeg'); 
			$oldimg = (isset($edit) && (!empty($edit->image)))?$edit->image:''; 
		?>
	
		
		
		<div class="col-md-12 col-sm-12 col-xs-12">
		 
			<div class="page-header">
				<h1>BIlling Adderss</h1>
			</div>

		<form method="post" enctype="multipart/form-data" action="<?= base_url('BillingAddress/add'); ?>" >
			<input type="hidden" name="edit_id" value="<?= isset($edit)?$edit->id:''; ?>">
			
			<div class="row">
			 <div class="col-md-6">
				<div class="form-group md-form">					
					<input type="text" class="form-control" id="name" name="name"  value="<?= (isset($edit) && (!empty($edit->name)))?$edit->name:set_value('name') ?>">
					<label for="name">Name</label>
					<?php echo form_error('name', '<div class="text-danger">', '</div>'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group md-form">					
					<input type="email" class="form-control" id="email" name="email"  value="<?= (isset($edit) && (!empty($edit->email)))?$edit->email:set_value('email') ?>">
					<label for="email">Email</label>
					<?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
				</div>
			   </div>
		      </div>

		 	 <div class="row">
				<div class="col-md-6">
			   	  <div class="form-group md-form">					
					<input type="text" class="form-control" id="mobile1" name="mobile1"  value="<?= (isset($edit) && (!empty($edit->mobile1)))?$edit->mobile1:set_value('mobile1') ?>">
					<label for="mobile1">Mobile Number</label>
					<?php echo form_error('mobile1', '<div class="text-danger">', '</div>'); ?>
				</div>
			  </div>
			  <div class="col-md-6">
				<div class="form-group md-form">						
					<input type="text" class="form-control" id="mobile2" name="mobile2" value="<?= (isset($edit) && (!empty($edit->mobile2)))?$edit->mobile2:set_value('mobile2') ?>">
					<label for="mobile2">Alternate Number</label>
					<?php echo form_error('mobile2', '<div class="text-danger">', '</div>'); ?>
				 </div>
			   </div>
			 </div>
			
			<div class="row">
			  <div class="col-md-6">
				<div class="form-group">
				   <label for="country_id">Country</label>
					<select name="country_id" id="country_id" class="form-control">
                        <option></option>
                        <?php
                        foreach ($countries as $country) { ?>
                        <option <?= (isset($edit) && ($edit->country_id == $country->id))?'selected':''; ?> value="<?= $country->id ?>"><?= $country->name ?></option>
                        <?php } ?>
                    </select>
					<?php echo form_error('country_id', '<div class="text-danger">', '</div>'); ?>
				 </div>
				</div>
				<div class="col-md-6">
				 <div class="form-group">	
				   <label for="state_id">State</label>				
					<select name="state_id" id="state_id" class="form-control">
                        <option></option>
                        <?php
                        foreach ($states as $state) { ?>
                        <option <?= (isset($edit) && ($edit->state_id == $state->id))?'selected':''; ?> value="<?= $state->id ?>"><?= $state->name ?></option>
                        <?php } ?>
                    </select>                    
				  	 <?php echo form_error('state_id', '<div class="text-danger">', '</div>'); ?>
				   </div>
				 </div>
				</div>
				<div class="row">
				  <div class="col-md-6">
					<div class="form-group ">
				  	 <label for="city_id">City</label>
					 <select name="city_id" id="city_id" class="form-control">
                        <option></option>
                        <?php
                        foreach ($cities as $city) { ?>
                        <option <?= (isset($edit) && ($edit->city_id == $city->id))?'selected':''; ?> value="<?= $city->id ?>"><?= $city->name ?></option>
                        <?php } ?>
                    </select>
					<?php echo form_error('city_id', '<div class="text-danger">', '</div>'); ?>
				 </div>
				</div>				
			   <div class="col-md-6">
				 <div class="form-group md-form">					
					<input type="text" class="form-control" id="locality" name="locality" value="<?= (isset($edit) && (!empty($edit->locality)))?$edit->locality:set_value('locality') ?>">
					<label for="locality">Locality</label>
					<?php echo form_error('locality', '<div class="text-danger">', '</div>'); ?>
				  </div>
				</div>
			 </div>

			<div class="md-form">					
				<textarea class="md-textarea"  id="address" name="address" ><?= (isset($edit) && (!empty($edit->address)))?$edit->address:set_value('address') ?></textarea>
				<label for="address">Address</label>
				<?php echo form_error('address', '<div class="text-danger">', '</div>'); ?>
			</div>

				<div class="row">
				 <div class="col-md-6">
				  <div class="form-group md-form">					
					<input type="text" class="form-control" id="landmark" name="landmark"  value="<?= (isset($edit) && (!empty($edit->landmark)))?$edit->landmark:set_value('landmark') ?>">
					<label for="landmark">Landmark</label>
					<?php echo form_error('landmark', '<div class="text-danger">', '</div>'); ?>
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group md-form">						
					<input type="number" class="form-control" id="pincode" name="pincode" value="<?= (isset($edit) && (!empty($edit->pincode)))?$edit->pincode:set_value('pincode') ?>">
					<label for="pincode">Pincode</label>
					<?php echo form_error('pincode', '<div class="text-danger">', '</div>'); ?>
			 	 </div>
			    </div>
			  </div>
				<div class="form-group">
					<input type="submit" class="btn btn-info" value="Submit">
				</div>
			</form>
		</div>
	 </div>
   </div><!-- .row -->
 </div><!-- .container -->
</div>
</div>
</section>