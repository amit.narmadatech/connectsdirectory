<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>
    invoice
  </title>
</head>
<body style="margin: 0; padding: 200px;">
    <div style="width:540px;">
        <table cellpadding="0" cellspacing="0" style="width: 540px; border-collapse: collapse; margin:0 auto; border: solid 2px #A2A2A2;">
            <thead>
                <tr>
                    <th style="background: #EEE; text-align: left; border-bottom: solid 2px #A2A2A2; vertical-align: middle;" ><br><br>
                     <img src="<?php echo base_url('/assets/images/logo.png'); ?>" width="100px" /><br>
                     <span style="font-family: helvetica; font-size:8pt;">7th Floor, 714-716, Princess Business Skypark,</span><br>
                    <span style="font-family: helvetica; font-size:8pt;">PU-3 Commercial, Scheme No. 54, A.B. Road,</span><br>
                    <span style="font-family: helvetica; font-size:8pt;">Indore (M.P.), India. Pin 452010</span><br>
                    <span style="font-family: helvetica; font-size:8pt;">GSTIN: 23AAECJ1917G1ZX </span><br>
                    <span style="font-family: helvetica; font-size:8pt;">Place of registration: Indore, Madhaya predesh</span><br>
                    </th>               
                    <th style="background: #EEE; color: #000; font-size: 16pt;font-weight: bolder; text-align: right; font-family: helvetica; border-bottom: solid 2px #A2A2A2; vertical-align: middle; line-height: 40px"><br>
                            INVOICE &nbsp; 
                    </th>
                </tr>
                
            </thead>
            <tbody>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="3" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                            <tr>
                                <td style="font-family: helvetica; font-size:10pt;"><strong>SHIP TO</strong></td>                           
                            </tr>
                            <tr>
                               <td style="font-family: helvetica; font-size:10pt; text-transform: capitalize;"><?php echo!empty($orders->name) ? $orders->name : ''; ?></td>
                            </tr>
                            <tr>
                               <td style="font-family: helvetica; font-size:8pt;"><?php echo!empty($orders->address) ? $orders->address : ''; ?>, <?php echo!empty($orders->landmark) ? $orders->landmark : ''; ?>, <?php echo !empty(getDistrict($orders->district_id)) ? getDistrict($orders->district_id)->district_title : ''; ?>, <?php echo !empty(getState(getDistrict($orders->district_id)->state_id)) ? getState(getDistrict($orders->district_id)->state_id)->state_title : ''; ?>   </td>
                            </tr>                             
                            <tr>
                                <td style="font-family: helvetica; font-size:8pt;">+91 <?php echo!empty($orders->mobile1) ? $orders->mobile1 : ''; ?> </td> 
                            </tr>
                            <tr>
                                <td style="font-family: helvetica; font-size:8pt;" colspan="2"><?php echo!empty($orders->email) ? $orders->email : ''; ?> </td>  
                              </tr>
                            </tbody>
                        </table>
                    </td>   
                    <td>
                        <table cellpadding="0" cellspacing="5" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td style="text-align: right; font-family: helvetica; font-size:8pt;">DATE : </td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: left;"><?php echo!empty($orders->order_date) ? date('d-m-Y', strtotime($orders->order_date)) : ''; ?>  &nbsp; </td>
                                </tr>
                                <tr>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">ORDER NO. : </td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: left;"><?php echo!empty($orders->order_number) ? $orders->order_number : ''; ?>   &nbsp; </td>
                                </tr>
                                
                                <tr>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">TXN NO. : </td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: left;"><?php echo!empty($orders->txnid) ? $orders->txnid : ''; ?>   &nbsp;  </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    
                    </tr>
                    <tr>
                        <td style="width: 100%;" colspan="2"><table cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                        <tr>
                            <td style="font-family: helvetica; width: 50px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">SR. No. </td>
                            <td style="font-family: helvetica; width: 150px;  font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">PLAN NAME </td>
                            <td style="font-family: helvetica; width: 70px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">PRICE/QTY </td>
                            <td style="font-family: helvetica; width: 70px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">TAX  </td>
                            <td style="font-family: helvetica; width: 100px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">TAX AMOUNT  </td>
                            <td style="font-family: helvetica; width: 100px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; text-align: right;">TOTAL AMOUNT </td>
                        </tr><?php
				$amount =0 ;
                                if (!empty($orderDetails)) {
                                  $i=1;$gtotal = 0;
                                    foreach ($orderDetails as $orderDetailsval) {
                                      $taxamt=0;
					  $amount = ($amount+($orderDetailsval->quantity*$orderDetailsval->plan_price));	
									 	
                                    ?>
                                <tr>
                                   <td style="font-family: helvetica; width: 50px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">
                                        <?php echo $i++; ?>
                                    </td>
                                    <td style="font-family: helvetica; width: 150px;  font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">

                                        <?php echo!empty($orderDetailsval->name) ? $orderDetailsval->name : ''; ?>
                                    </td>
                                    <td style="font-family: helvetica; width: 70px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">
                                         <?php echo!empty($orderDetailsval->plan_price) ? $orderDetailsval->plan_price : ''; ?> * <?php echo!empty($orderDetailsval->quantity) ? $orderDetailsval->quantity : ''; ?>
                                    </td>
                                    <td style="font-family: helvetica; width: 70px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px; text-align: right;">
                                        <?php 
                                              if ($orders->state_id == $orderDetailsval->pck_state_id) { ?>
                                                <span style="line-height:15px;">CGST <?php echo $orderDetailsval->cgst_tax; ?> % <br>SGST <?php echo $orderDetailsval->sgst_tax; ?> %
                                                </span>
                                             <?php }elseif(!empty($orders->igst)){ ?>
                                                <span style="line-height:15px;">IGST <?php echo $orderDetailsval->igst_tax; ?> %</span>
                                              <?php }
                                         ?>
                                   </td>
                                  <td style="font-family: helvetica; width: 100px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px; text-align: right;">
                                        <?php 
                                              if ($orders->state_id == $orderDetailsval->pck_state_id) { ?>
                                                <span style="line-height:15px;">Rs <?php 
                                                  echo (($orderDetailsval->cgst_tax * $orderDetailsval->plan_price)/100)*$orderDetailsval->quantity;$taxamt += (($orderDetailsval->cgst_tax * $orderDetailsval->plan_price)/100)*$orderDetailsval->quantity; ?> <br>Rs
                                                  <?php 
                                                  echo (($orderDetailsval->sgst_tax * $orderDetailsval->plan_price)/100)*$orderDetailsval->quantity;$taxamt += (($orderDetailsval->sgst_tax * $orderDetailsval->plan_price)/100)*$orderDetailsval->quantity; ?>
                                                </span>
                                             <?php }elseif(!empty($orders->igst)){ ?>
                                                <span style="line-height:15px;">Rs <?php 
                                                  echo (($orderDetailsval->igst_tax * $orderDetailsval->plan_price)/100)*$orderDetailsval->quantity; ?></span>
                                              <?php }
                                         ?>
                                   </td>
                                   <td style="font-family: helvetica; width: 100px; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; text-align: right; line-height:40px;">
                                      <i class="fa fa-inr"></i>&nbsp;<?php  $gtotal +=!empty($orderDetailsval->plan_price) ? ($orderDetailsval->quantity*$orderDetailsval->plan_price)+$taxamt : '0.0';
                                      echo !empty($orderDetailsval->plan_price) ? ($orderDetailsval->quantity*$orderDetailsval->plan_price)+$taxamt : '0.0';
                                       ?>
                                    </td>
                                </tr><?php }
                                    } ?>

			                           	<tr>
                                   <td colspan="4" style="border-top:solid 2px #A2A2A2;"></td>
                                   <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                         Sub-Total:  </td>
                                   <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px;">
                                         Rs &nbsp;<?php echo!empty($gtotal) ? $gtotal : '0.00'; ?> &nbsp;&nbsp;
                                    </td>
                                 </tr>
                                
                                <tr>
                                    <td colspan="4"></td>
                                   <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                  Grand Total:  </td>
                                  <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px; border-top:solid 2px #A2A2A2;">
                                   	Rs <?php echo!empty($orders->total_amount) ? $orders->total_amount : ''; ?> &nbsp;&nbsp;
                                  </td>
                              </tr>
                          </table> 
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>