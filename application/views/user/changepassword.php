<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success text-center">
                <?php echo $this->session->flashdata('success'); ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
        <?php } ?>
   
                    <div class="page-header"><h1>Change Password</h1></div>
                    <div class="panel-body">
                        <form id="resetpasswordfrm" name="resetpasswordfrm" method="POST" action="">
                             <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                            <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                            <div class="form-group <?php if (form_error('oldpassword')) echo 'has-error'; ?>">
                                <label for="new-password">Old Password</label>
                                <input type="Password" id="oldpassword" name="oldpassword" class="form-control" placeholder="Old Password">
                                <span style="color:#a94442"><?php echo form_error('oldpassword'); ?></span>
                            </div>
                            <div class="form-group <?php if (form_error('oldpassword')) echo 'has-error'; ?>"">
                                <label for="new-password">New Password</label>
                                <input type="Password" id="new-password" name="password" class="form-control" placeholder="New Password">
                                <span style="color:#a94442"><?php echo form_error('password'); ?></span>
                            </div>
                            <div class="form-group <?php if (form_error('oldpassword')) echo 'has-error'; ?>"">
                                <label for="re-password">Retype Password</label>
                                <input type="Password" id="re-password" name="cpassword" class="form-control" placeholder="Retype Password">
                                <span style="color:#a94442"><?php echo form_error('cpassword'); ?></span>
                            </div>
                            <button type="submit" class="flat-button border">Change Password <i class="material-icons">send</i></button>
                        </form>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</section>
<?php $this->load->view('include/footer') ?> 
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(function () {
            $('#resetpasswordfrm').validate({
                submitHandler: function (form) {
                    form.submit();
                },
                ignore: [],
                rules: {
                    oldpassword: "required",
                    password: {
                        required: true,
                        minlength: 6,
                    },
                    cpassword: {
                        required: true,
                        minlength: 6,
                        equalTo: "#new-password"
                    }

                },
                messages: {
                    password: {
                        required: "Please enter your password",
                        minlength: "The Password field must be at least 6 characters in length",
                    },
                    repassword: {
                        required: "Please enter confirm password",
                        equalTo: "Confirm password does not match password",
                    }
                },
                /* highlight: function (input) {
                 $(input).parents('.form-line').addClass('error');
                 },
                 unhighlight: function (input) {
                 $(input).parents('.form-line').removeClass('error');
                 },
                 errorPlacement: function (error, element) {
                 $(element).parents('.form-group').append(error);
                 }*/
            });

        });
    });
</script>