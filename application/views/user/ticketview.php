<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<?php $this->load->view('include/header'); ?>

<section class="content">

    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">
                    <div class="page-header">
                        <h1> Ticket 
                            <!--<button class="flat-button border pull-right"  data-toggle="modal" data-target="#ticketmodal">Ticket Details <i class="material-icons">remove_red_eye</i></button>-->
                        </h1>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="ticket-box">
                                <h4>All Ticket</h4>
                                <ul>
                                    <?php if (!empty($tickets)) { ?>
                                    
                                     <?php //print_r($tickets);?>
                                    
                                        <?php foreach ($tickets as $ticketDataVal) { ?>
                                            <li class="<?php if (!empty($tickets_id) && ($tickets_id == $ticketDataVal->tickets_id)) { ?> active <?php } ?>"> 
                                                <a href="<?php echo base_url('user/ticketview/' . $ticketDataVal->tickets_id); ?>">
                                                    <?php echo!empty($ticketDataVal->ticket_code) ? $ticketDataVal->ticket_code : ''; ?>
                                                    <div class="pull-right">
                                                        <?php if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 1)) { ?>
                                                            <span class="label label-success">Close</span>
                                                        <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 2)) { ?>

                                                            <span class="label label-info">Inprogress</span>

                                                        <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 3)) { ?> 

                                                            <span class="label label-primary">Pending</span>

                                                        <?php } else if (!empty($ticketDataVal->status) && ($ticketDataVal->status == 4)) { ?>

                                                            <span class="label label-info">Answer</span>
                                                        <?php } else { ?>
                                                            <span class="label label-danger">Open</span>
                                                        <?php } ?>

                                                    </div>
                                                    <br>
                                                    <span class="ticket-date">Client | <?php echo!empty($ticketDataVal->created) ? date('d-m-Y h:i A', strtotime($ticketDataVal->created)) : ''; ?></span>
                                                </a>
                                            </li>
                                        <?php }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <h4 class="ticket-heading">[<?php echo!empty($ticketData->ticket_code) ? $ticketData->ticket_code : ''; ?>] <?php echo!empty($ticketData->subject) ? $ticketData->subject : ''; ?> </h4>
                            <div class="ticket-detail">
                                <p> <?php echo!empty($ticketData->body) ? $ticketData->body : ''; ?> </p>
                            </div>
                            <ul class="list-inline mailbox-attachments">
                                <li>
                                    <span class="mailbox-attachment">
                                        <?php if (!empty($ticketData->upload_file) && file_exists("uploads/tickets/" . $ticketData->upload_file)) { ?>
                                            <img src="<?php echo base_url() . 'uploads/tickets/' . $ticketData->upload_file; ?>" class=""> 
                                        <?php } ?>
                                    </span>
                                </li>
                            </ul>
                            <div class="chatreply-btn">
                                <button class="flat-button border" data-toggle="collapse" data-target="#demo">Reply Ticket <i class="material-icons">reply</i></button>
                            </div>

                            <div id="demo" class="collapse">
                                <form name="ticketreplyfrm" id="ticketreplyfrm" action="<?php echo base_url('user/ticketcomment'); ?>" method="POST" enctype="multipart/form-data">
                                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                                    <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                                    <div class="form-group">
                                        <textarea name="comment" class="form-control" placeholder="Ticket reply"></textarea>
                                        <input type="hidden" name="tickets_id" value="<?php echo!empty($ticketData->tickets_id) ? $ticketData->tickets_id : ''; ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <input type="file" name="filename" class="">
                                            </div>    
                                            <!-- <div class="col-sm-4">   
                                                    <a class="flat-button border">Add <i class="material-icons">add</i></a>
                                             </div>-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="flat-button border">Submit <i class="material-icons">send</i></button>
                                    </div>
                                </form>
                            </div>
                            <div class="chat">
                                <?php
                                if (!empty($ticketsconversion)) {
                                    foreach ($ticketsconversion as $ticketsconversions) {

                                        if (file_exists("uploads/userimages/" . $ticketsconversions->image) && !empty($ticketsconversions->image)) {

                                            $img = base_url() . 'uploads/userimages/' . $ticketsconversions->image;
                                        } else {

                                            $img = base_url() . 'assets/images/profile_big_img.png';
                                        }
                                        ?>
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                <a href="#"> 
                                                    <img alt="64x64" class="media-object" style="width: 64px; height: 64px;" src="<?php echo $img; ?>">
                                                </a> 
                                            </div> 
                                            <div class="media-body"> 
                                                <div class="media-heading">
                                                    <?php echo!empty($ticketsconversions->name) ? $ticketsconversions->name : ''; ?> 
                                                    <?php if ($ticketsconversions->type == 4) { ?>
                                                        <span class="label label-success">Client</span>
                                                    <?php } else { ?>
                                                        <span class="label label-success">Admin</span>
                                                    <?php } ?>
                                                    <span class="time"><i class="material-icons">alarm </i><?php echo getTimeAgo($ticketsconversions->time); ?></span></div> 

                                                <?php echo!empty($ticketsconversions->comment) ? $ticketsconversions->comment : ''; ?>

                                                <?php if (!empty($ticketsconversions->attachment) && file_exists("uploads/tickets/" . $ticketsconversions->attachment)) { ?>
                                                    <img src="<?php echo base_url() . 'uploads/tickets/' . $ticketsconversions->attachment; ?>" class="img-responsive"> 
                                                <?php } ?> 

                                            </div>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
</section>


<div class="modal fade bs-example-modal-sm" id="ticketmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> 
                <h4 class="modal-title" id="mySmallModalLabel">Ticket Detail</h4> 
            </div>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td>Reporter</td> 
                        <td>Mr Vikas</td>
                    </tr>
                    <tr>
                        <td> Department</td> 
                        <td>Technical</td>
                    </tr>
                    <tr>
                        <td> Status</td> 
                        <td>Open</td>
                    </tr>
                    <tr>
                        <td>Priority</td> 
                        <td>High</td>
                    </tr>
                    <tr>
                        <td>Created</td> 
                        <td>2018-03-15 00:36:28</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('include/footer'); ?>
<script>

    $(document).ready(function () {
        $('#ticketreplyfrm').validate({
            rules: {
                comment: "required"

            },
            submitHandler: function (form) {
                form.submit();
            },
            messages: {
                required: "Please enter your messages",

            },
        });
    });

</script>
