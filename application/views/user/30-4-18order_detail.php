<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('include/header'); ?>
<section class="content">
  <div class="container inner-container">
      <!-- USER MENU -->
       <?php $this->load->view('include/user_menu'); ?>
      <!-- END USER MENU -->
    <div class="user-content">
	   <?php $this->load->view('include/alert'); ?>
      <div class="page-header">
        <h1>Order Detail </h1>
      </div>
	   <div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
		   <?php if(!empty($orders)){ ?>		  
		  	<div class="row order-detail">
                <!--Grid column-->
                <div class="col-md-12">
                    <div class="well">                        
                        <div class="row">
                            <div class="col-sm-4"> 
                                <h4>Order Details</h4>
                                <p>Order Number  : <?php echo !empty($orders->order_number)? $orders->order_number : ''; ?></p>
                                <p>Order Date & Time : <?php echo !empty($orders->order_date)? date('d-m-Y h:i A', strtotime($orders->order_date)) :''; ?> </p>
                               <!-- <p>Total Amount :  &#8377; <?php //echo !empty($orders->total_amount)? $orders->total_amount:''; ?></p>-->
                                <p>Payment Mode : <?php echo !empty($orders->payment_mode)? $orders->payment_mode:''; ?></p> 
							    <p>Payment Status : successful</p>
                            </div>
                        <div class="col-sm-5">
                         <h4>Address </h4>
                            <strong><?php echo !empty($orders->name)? $orders->name :''; ?></strong>
                            <p>
                            <?php echo !empty($orders->address)? $orders->address :''; ?>,
                            <?php echo !empty($orders->landmark)? $orders->landmark :''; ?>,
                            <?php echo !empty($orders->city_name)? $orders->city_name :''; ?>,
                            <?php echo !empty($orders->state_name)? $orders->state_name :''; ?>
                            </p>								
							<p><?php echo !empty($orders->mobile1)? $orders->mobile1 :''; ?></p>
                            <p><?php echo !empty($orders->email)? $orders->email :''; ?></p>
                        </div>
                        <div class="col-sm-3">
                            <h4>Manage Order</h4>
							<a href="<?php echo base_url('user/downloadinvoice/'.$orders->id); ?>" class="flat-button border"> Invoice <i class="fa fa-download"></i></a>
                        </div>
                    </div>      
                </div>                        
            </div>
        </div>

        <div class="row">
            <!--Grid column-->
            <div class="col-md-12">
                <div class="panel panel-default">  
                   <!-- <div class="panel-header">  1</div>  -->
             	<div class="panel-body"> 	
              		<div class="row order-box">
						<div class="col-sm-3">  
							Plan
						</div>
						<div class="col-sm-3"> 
							Plan Name
						</div>
						<div class="col-sm-3"> 
							 Price/Qty 
						</div>
						 <div class="col-sm-3 text-right"> 
							Total 
						</div>
					</div>
					<?php  	$amount =0 ; //$orderDetails = getOrderDetails($ordersVal->id);									
						if(!empty($orderDetails)){
										
							 foreach ($orderDetails as $orderDetailsval) {
										   	if (file_exists("uploads/plan_img/".$orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image)){
											
								 $img = base_url().'uploads/plan_img/'.$orderDetailsval->plan_image;
							}else{
							 $img = base_url().'assets/images/diet-plan.jpg';
							}
							$amount = ($amount+($orderDetailsval->quantity*$orderDetailsval->plan_price));
											?>			
							<div class="row order-box">
								<div class="col-sm-3">           
									<img src="<?php echo $img;?>" class="img-responsive" width="120" height="80">
								</div>
								<div class="col-sm-3"> 
									<p> <?php echo !empty($orderDetailsval->name)? $orderDetailsval->name :''; ?>	</p>
								</div>
								<div class="col-sm-3"> 
									<p>  &#8377; <?php echo !empty($orderDetailsval->plan_price)? $orderDetailsval->plan_price :''; ?>
									X <?php echo !empty($orderDetailsval->quantity)? $orderDetailsval->quantity :''; ?> 
								  </p>
								</div>		
													
								<div class="col-sm-3 text-right">
									<p>&#8377;  <?php echo!empty($amount) ? $amount : '0.00'; ?> <?php //echo !empty($orders->total_amount)? $orders->total_amount :''; ?></p>
								</div> 
							</div>											
						<?php  } }  ?>
                  </div>
                  <div class="panel-footer grey lighten-4">
                    <div class="row">
                      <div class="col-sm-3 col-xs-6">
                         Order on : <?php echo !empty($orders->order_date)? date('d-m-Y', strtotime($orders->order_date)) :''; ?>
                      </div>
					   <div class="col-sm-3 col-xs-6">
                          <p>Total : &#8377; <?php echo!empty($amount) ? $amount : '0.00'; ?> </p>
                      </div>
					   <div class="col-sm-3 col-xs-6">
                           <p>Discount : <?php echo !empty($orders->discount)? $orders->discount :' 0.00'; ?> </p>
                      </div>
                      <div class="col-sm-3 col-xs-6 text-right">			   
					   <p>Sub Total : &#8377; <?php echo !empty($orders->total_amount)? $orders->total_amount :''; ?> </p>
                      </div>
                  </div>
                </div>
              </div>         
            </div>
		   <?php }else{ ?>
	           <div class="alert alert-danger text-center">No order details available</div>	   
		   <?php } ?>
	      </div>
        </div>
      </div>
	 </div>
	</div>	
</section>
<?php $this->load->view('include/footer'); ?>