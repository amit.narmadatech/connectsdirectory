<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>
<section class="content">
  <div class="container inner-container">
        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
    <div class="user-content">	
	<?php $this->load->view('include/alert'); ?>
	 <div class="page-header">
                <h1>My Orders</h1>
            </div>
     </div>

		<div class="row">
			<div class="col-sm-12">
		<?php 
			   if (!empty($orders)) {
					foreach ($orders as $ordersVal) { 
					
						$orderDetails = getOrderDetails($ordersVal->id);
						if(!empty($orderDetails)){  ?>
						
					 <div class="table-responsive">
								<table class="table table-bordered card-table">
									<thead>
										<tr>
										<th> Order Id </th>
										<th> Plan Name</th>
										<th> Order Date </th>
										<th> Price/Qty </th>									
										<th> Payment status  </th>
										<th> Oreder Status </th>
										<th> Action </th>
									</tr>
									</thead>
										
									<tr>
								<?php  
									
									   foreach ($orderDetails as $orderDetailsval) {
							
											if (file_exists("uploads/plan_img/".$orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image)){
											
											   $img = base_url().'uploads/plan_img/'.$orderDetailsval->plan_image;
											 }else{
											   $img = base_url().'assets/images/diet-plan.jpg';
											 }
											?>
										<td> <?php echo $ordersVal->order_number ;?> </td>
										<td><a href="<?php echo base_url('user/orderdetails/'.$ordersVal->id) ?>"><img src="<?php echo $img;?>" class="img-responsive" width="120" height="80" style="width:60px; height: 40px; float: left; margin-right: 5px;"></a>
											<a href="<?php echo base_url('user/orderdetails/'.$ordersVal->id) ?>"><?php echo !empty($orderDetailsval->name)? $orderDetailsval->name :''; ?></a>

										<!--  <img src="<?php echo base_url('assets/') ?>images/diet-plan.jpg" style="width:80px; height: 60px; float: left; margin-right: 5px;"> Skin Genetics --></td>
										<td><?php echo !empty($ordersVal->order_date)? date('d-m-Y h:i A', strtotime($ordersVal->order_date)) :''; ?> <br>
											
										</td>
										<td> <i class="fa fa-inr"></i> <?php echo !empty($orderDetailsval->plan_price)? $orderDetailsval->plan_price :''; ?> X
											<?php echo !empty($orderDetailsval->quantity)? $orderDetailsval->quantity :''; ?>
										</td>
										
										<td>  successful  </td>
										<td> Your kit estimated delivery date <?php echo date('d-m-Y', strtotime("+1 week")); ?>  </td>
										<td> <a href="<?php echo base_url('user/orderdetails/'.$ordersVal->id) ?>" class="btn-floating btn-info"> <i class="material-icons">remove_red_eye</i></a>
											
										</td>
									
									</tr>
									<?php  }   ?>
									<tfoot>
									<tr>
										<td colspan="6" class="text-right">
											<strong>Total</strong>
										</td>
										<td>
											<strong><i class="fa fa-inr"></i> <?php echo $ordersVal->total_amount; ?> </strong>
										</td>
									</tr>
								</tfoot>
							  </table>
							</div>
								
						      <?php  }
						}
				}else{ ?>
				
				    <div class="alert alert-info">
						<strong>Info!</strong> NO order available.
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					</div>
				
				<?php }?>
							
						</div>
						</div>

	  </div>
	</div>
 </div>
 <?php $this->load->view('include/footer'); ?>