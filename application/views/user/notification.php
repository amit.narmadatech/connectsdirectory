<?php $this->load->view('include/header'); ?>

<section class="content">
    <div class="container inner-container">
    	<div class="row">
            <div class="col-sm-4 col-md-3">
		<!-- USER MENU -->
		<?php $this->load->view('include/user_menu'); ?>
		<!-- END USER MENU -->
	</div>
	<div class="col-sm-8 col-md-9">
 		<div class="user-content">
			<?php $this->load->view('include/alert'); ?>
			<div class="page-header">
				<h1>Notification</h1>
			</div>
			
			<div class="row">
			<?php 
				$oldimg = (isset($user) && (!empty($user->image)))?$user->image:''; 
			?>	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="notice-content">
					<?php foreach ($notification_list as $nflist) { ?>
						<div class="card">
							<div class="card-body">
								<a class="single-notification <?php echo (check_notification_isread($nflist->id,$this->session->user_id))?'':'read'; ?>" id="<?= $nflist->id ?>"><b><?= $nflist->title ?></b></a>
                                                                <p><?php echo $nflist->body ?> &nbsp;<br><span><b><?php echo date('F d, Y h:i:s A', strtotime($nflist->created_at)); ?></b></span></p>
                                                  	</div>
						</div>
					<?php } ?>			
					</div>
	 			</div>
			</div>
  		</div><!-- .row -->
  	</div>
  </div>
	</div><!-- .container -->
</section>	
<?php $this->load->view('include/footer'); ?>