<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="login-page">
  <div class="container">
		<?php $this->load->view('include/alert'); ?>
    <div class="card-login  box-shadow">
    <di class="row"> 
     <div class="col-md-8 col-md-offset-3">


    <div class="row">
      
		  <div class="col-md-6 col-sm-6 col-md- login-left">
        <div class="login-form">
        <form method="post" action="<?= base_url('register/post_login'); ?>">    
		    	       
             <h3 class="my-0  text-center">Sign In</h3>
            <form method="post" id="signinForm" action="<?= base_url('register/post_login'); ?>">        

              <fieldset>                
                <div class="form-group">
                  <!-- <i class="fa fa-envelope prefix grey-text"></i>   -->                                 
									<input type="email" class="form-control" id="username" name="username" value="<?= set_value('username') ?>"  placeholder="Email">
                 
										<?php echo form_error('username', '<div class="text-danger">', '</div>'); ?>
                 </div>
                 <span id="emailerror"></span>
                 
                <div class="form-group">                  
                 <!--   <i class="fa fa-lock prefix grey-text"></i> -->
									<input type="password" class="form-control" id="password" name="password" placeholder="Password">
                 
									<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
                 </div>
                 <span id="passworderror"></span>

                 <div class="form-group">
                  
                    <input type="checkbox" id="checkbox">
                    <label for="checkbox" class="grey-text">Remember me</label>
                    

                    <a href="#" class="pull-right blue-text">Forgot Password?</a>
                </div>

               <div class="form-group">           
                 <button type="submit" class="flat-button border">Login <i class="material-icons">send</i></button>
                 </div>
       

          <!--  <div class="row my-3 d-flex justify-content-center">
              <a href="<?php echo $GoogleloginURL; ?>" class="btn btn-blue btn-rounded waves-effect waves-light"><i class="fa fa-google-plus "></i></a>
              <a href="<?php echo $facebookAuthUrl; ?>" class="btn btn-blue btn-rounded waves-effect waves-light"><i class="fa fa-facebook "></i></a>
            </div> -->
                     
               <div class="form-group">

                    <p class="font-small text-center grey-text">Don't have an account? <a href="<?= base_url('signup'); ?>" class="blue-text ml-1"> Create your account</a></p>

                </div>
              </div>
          </div>
       <div class="col-md-6 col-sm-6 login-right">
        

          <div class="row d-flex justify-content-center">
                <h2 class="text-center mb-0 pb-2">Sign up with</h2>
            </div>

            <div class="row pb-2">
                <!--Facebook-->
                <div class="col-sm-12 text-center">
                <a href="<?php echo $facebookAuthUrl; ?>" target="_blank" class="button facebook"><span><i class="fa fa-facebook" aria-hidden="true"></i></span><p>Facebook</p></a>

                <!-- <a class="icons-sm fb-ic" href="<?php echo $facebookAuthUrl; ?>" ><span><i class="fa fa-facebook-square  fa-lg"> </i></span></a> -->
                <!--Twitter-->
               <!--  <a class="icons-sm tw-ic"><i class="fa fa-twitter white-text fa-lg"> </i></a> -->
              
                <!--Google +-->
                <a href="<?php echo $GoogleloginURL; ?>" target="_blank" class="button google-plus"><span><i class="fa fa-google-plus" aria-hidden="true"></i></span><p>Google +</p></a>

                <!-- <a class="icons-sm gplus-ic" href="<?php echo $GoogleloginURL; ?>"><span><i class="fa fa-google-plus-square fa-lg" aria-hidden="true"></i></span></a> -->
            </div>
          </div>
      </div>
  </fieldset>
        </div>
      	</div>
        </form>
			</div>
    </div>
</div>
		</div>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 
<script>
    var validator =   $("#signinForm").validate({
    
          errorPlacement: function(error, element) {
          if(element.attr("name") == "username"){
              error.appendTo('#emailerror');
              return;
          }else if(element.attr("name") == "password"){
              error.appendTo('#passworderror');
              return;
          }else {
            error.insertAfter(element);
          }
          },
            rules: {
          password: "required",
          username:{
            required: true,
            email: true 
          }
        },
        submitHandler: function(e){
          this.submit();
        },
        messages: {
           password: "Please enter your password",
           username: { 
            required :"Please enter your email address",
             email:  "Please enter valid email address",
           }
        }
    });
</script>