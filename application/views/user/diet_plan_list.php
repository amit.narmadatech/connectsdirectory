<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('include/header'); ?>
<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">	
                    <div class="page-header">
                        <h1>Diet Plan</h1>
                    </div>
                    <?php $oldimg = (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>
                    <div class=" clearfix">
                        <div class="table-responsive">
                            <?php if (!empty($dietplan)) { ?>      
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Barcode </th> 
                                            <th>Plan Name</th>
                                            <th>Name</th>
                                            <th>Email</th>                                       
                                            <th>Diteplan Date</th>
                                            <th>Report</th>
                                        </tr>
                                    </thead>

                                    <?php
                                    foreach ($dietplan as $dietplans) {
                                        if (!empty($dietplans->dietpdf)) {

                                            $rurl = base_url().'uploads/dietplan/'.$dietplans->dietpdf;
                                        } else {
                                            $rurl = 'javascript:void(0)';
                                        }
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td><?php echo!empty($dietplans->barcode) ? $dietplans->barcode : ''; ?></td>
                                                <td><?php echo!empty($dietplans->plan_name) ? $dietplans->plan_name : ''; ?></td>
                                                <td><?php echo!empty($dietplans->name) ? $dietplans->name : ''; ?></td>
                                                <td><?php echo!empty($dietplans->email) ? $dietplans->email : ''; ?></td>
                                                <td><?php echo!empty($dietplans->created_date) ? date('j F Y, g:i a', strtotime($dietplans->created_date)) : ''; ?></td>
                                                <td>
                                                    <a download href="<?php echo $rurl;?>"> <i class="fa fa-file-pdf-o" style="font-size:48px;color:red"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    <?php } ?>
                                </table>
                            <?php } else { ?>
                                <div class="alert alert-danger text-center">
                                    No record available.
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            <?php } ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
<?php $this->load->view('include/footer'); ?>
