<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<link href="<?php echo base_url(); ?>assets/css/jquery.steps.css" rel="stylesheet">

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">	
                    <div class="page-header">
                        <h1>DashBoard</h1>
                    </div>
                    <?php $oldimg = (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>
                      <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <h4 class="panel-heading">Resent Order</h4>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Plan</th>
                                            <th>Price</th>
                                            <th>Qty </th>
                                            <th>Order id</th>
                                            <th>Payment  Status</th>               
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Skin Genetics</td>
                                            <td><i class="fa fa-inr"></i> 9999</td>
                                            <td>1</td>
                                            <td>61529150145</td>
                                            <td>Successful</td>
                                                                                       
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Gene Diet</td>
                                            <td><i class="fa fa-inr"></i> 9999</td>
                                            <td>1</td>
                                            <td>61529150145</td>
                                            <td>Successful</td>                                         
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Health Horoscope</td>
                                            <td><i class="fa fa-inr"></i> 9999</td>
                                            <td>1</td>
                                            <td>61529150145</td>
                                            <td>Successful</td>
                                       </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Precision Medicine</td>
                                            <td><i class="fa fa-inr"></i> 9999</td>
                                            <td>1</td>
                                            <td>61529150145</td>
                                            <td>Successful</td>                                     
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Ancestry DNA</td>
                                            <td><i class="fa fa-inr"></i> 9999</td>
                                            <td>1</td>
                                            <td>61529150145</td>
                                            <td>Successful</td>                                      
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->     
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-default">
                        <h4 class="panel-heading">My Kit List</h4>
                     <div class="panel-body">                         
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Barcode </th> 
                                        <th>Name</th>
                                        <th>Date of birth</th>
                                        <th>Gender</th>
                                        <th>Plan Name </th>                                       
                                        <th>Date</th>
                                        <th>Report</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>105652463316</td>
                                        <td>vikas</td>
                                        <td>13-03-1994</td>
                                        <td>Male</td>
                                        <td>Gene Fitness</td>
                                        <td>June 11, 2018 03:41:28 PM</td>
                                        <td class="text-center"><a target="_blank" href="#" class="btn-floating btn-warning">  <i class="fa fa-download"></i></a></td>
                                     </tr>
                                    <tr>
                                        <td>105652463316</td>
                                        <td>vikas</td>
                                        <td>13-03-1994</td>
                                        <td>Male</td>
                                        <td>Gene Fitness</td>
                                        <td>June 11, 2018 03:41:28 PM</td>
                                        <td class="text-center"><a target="_blank" href="#" class="btn-floating btn-warning">  <i class="fa fa-download"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>105652463316</td>
                                        <td>vikas</td>
                                        <td>13-03-1994</td>
                                        <td>Male</td>
                                        <td>Gene Fitness</td>
                                        <td>June 11, 2018 03:41:28 PM</td>
                                        <td class="text-center"><a target="_blank" href="#" class="btn-floating btn-warning">  <i class="fa fa-download"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>105652463316</td>
                                        <td>vikas</td>
                                        <td>13-03-1994</td>
                                        <td>Male</td>
                                        <td>Gene Fitness</td>
                                        <td>June 11, 2018 03:41:28 PM</td>
                                        <td class="text-center"><a target="_blank" href="#" class="btn-floating btn-warning">  <i class="fa fa-download"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>105652463316</td>
                                        <td>vikas</td>
                                        <td>13-03-1994</td>
                                        <td>Male</td>
                                        <td>Gene Fitness</td>
                                        <td>June 11, 2018 03:41:28 PM</td>
                                        <td class="text-center"><a target="_blank" href="#" class="btn-floating btn-warning">  <i class="fa fa-download"></i></a></td>
                                    </tr>
                                </tbody>                                  
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>



            </div>
        </div>
    </div>
</div><!-- .container -->

<?php $this->load->view('include/footer'); ?>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.min.js') ?>"></script>
<!--<script type="text/javascript">
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });
</script> -->
<script type="text/javascript">
// Tooltips Initialization
    $(document).ready(function () {

        $(document).on('click', '.kituser', function (event) {
            if (this.value == 'self') {
                $.post('<?php echo base_url("user/getUserDetail"); ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                    $('#name').val(data.result.firstname);
                    $('#namereview,#nameID').html(data.result.firstname);
                    $('#emailID').val(data.result.email);
                    $('#mobile').val(data.result.contact1);
                }, "json");
            } else if (this.value == 'other') {
                $('#name').val('');
                $('#emailID').val('');
                $('#mobile').val('');
            }
        });
    
    $(document).on('click', '.gender', function (event) {
        $('#sexID,#sexreview').html(this.value);
    });


        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "No space please and don't leave it empty");
        
//        $.validator.addMethod('atLeastOneSeat', function(value, element, params) {
//             var seats = $('radio.group-required').filter(function() {
//            return $(this).val() != '0';
//        });
//           return seats.length > 0;
//        }, 'Please select at least one seat');
//
//        $.validator.addClassRules('group-required', {atLeastOneSeat: true});

        var form = $("#kitActivationfrm");
        form.validate({
            errorPlacement: function (error, element) {
                if (element.attr("name") == "barcode") {
                    error.appendTo('#barcodeerror');
                    return;
                } else if (element.attr("name") == "kituser") {
                    error.appendTo('#kitusererror');
                    return;
                } else if (element.attr("name") == "gender") {
                    error.appendTo('#gendererror');
                    return;
                } else if (element.attr("name") == "email") {
                    error.appendTo('#emailerror');
                    return;
                } else if (element.attr("name") == "mobile") {
                    error.appendTo('#mobileerror');
                    return;
                } else if (element.attr("name") == "dob") {
                    error.appendTo('#doberror');
                    return;
                } else if (element.attr("name") == "consent_for") {
                    error.appendTo('#consenterror');
                    return;
                } else if (element.attr("name") == "pincode") {
                    error.appendTo('#pincodeerror');
                    return;
                }else if (element.attr("name") == "question[1]") {
                    error.appendTo('#question_1_error');
                    return;
                }else if (element.attr("name") == "question[2]") {
                    error.appendTo('#question_2_error');
                    return;
                } else if (element.attr("name") == "question[3]") {
                    error.appendTo('#question_3_error');
                    return;
                }else if (element.attr("name") == "question[3a]") {
                    error.appendTo('#question_3a_error');
                    return;
                } else if (element.attr("name") == "question[4]") {
                    error.appendTo('#question_4_error');
                    return;
                } else if (element.attr("name") == "question[5]") {
                    error.appendTo('#question_5_error');
                    return;
                } else if (element.attr("name") == "question[6]") {
                    error.appendTo('#question_6_error');
                    return;
                } else if (element.attr("name") == "question[7]") {
                    error.appendTo('#question_7_error');
                    return;
                }  else if (element.attr("name") == "question[8]") {
                    error.appendTo('#question_8_error');
                    return;
                } else if (element.attr("name") == "question[9]") {
                    error.appendTo('#question_9_error');
                    return;
                } else if (element.attr("name") == "question[10]") {
                    error.appendTo('#question_10_error');
                    return;
                } else if (element.attr("name") == "question[11]") {
                    error.appendTo('#question_11_error');
                    return;
                } else if (element.attr("name") == "question[12]") {
                    error.appendTo('#question_12_error');
                    return;
                } else if (element.attr("name") == "question[13]") {
                    error.appendTo('#question_13_error');
                    return;
                }else if (element.attr("name") == "question[13b]") {
                    error.appendTo('#question_13b_error');
                    return;
                }else if (element.attr("name") == "question[14]") {
                    error.appendTo('#question_14_error');
                    return;
                }else if (element.attr("name") == "question[15]") {
                    error.appendTo('#question_15_error');
                    return;
                }else if (element.attr("name") == "question[16]") {
                    error.appendTo('#question_16_error');
                    return;
                }else if (element.attr("name") == "question[17]") {
                    error.appendTo('#question_17_error');
                    return;
                }else if (element.attr("name") == "question[18]") {
                    error.appendTo('#question_18_error');
                    return;
                }else if (element.attr("name") == "question[19]") {
                    error.appendTo('#question_19_error');
                    return;
                }else if (element.attr("name") == "question[19][fruits][]" || element.attr("name") == "question[19][milk][]") {
                     $('#question_19_error').html('');
                     error.appendTo('#question_19_error');
                    return;
                } else if (element.attr("name") == "question[20][stress][]") {
                    $('#question_20_error').html('');
                    error.appendTo('#question_20_error');
                    return;
                } else if (element.attr("name") == "question[21][alcohol][]") {
                    $('#question_21_error').html('');
                    error.appendTo('#question_21_error');
                    return;
                }else {
                    error.insertAfter(element);
                }
            },
            rules: {
                name: {
                    required: true,
                    noSpace: true,
                    minlength: 2,
                    maxlength: 16
                },
                barcode: "required",
                mobile: "required",
                pincode: "required",
                consent_for: "required",
                dob: "required",
                 'question[1]': {
                    required: true
                  },
                  'question[2]': {
                    required: true
                  },
                   'question[3]': {
                    required: true
                  },
                   'question[3a]': {
                    required: true
                  },
                   'question[4]': {
                    required: true
                  },
                   'question[5]': {
                    required: true
                  },
                   'question[6]': {
                    required: true
                  },
                  'question[7]': {
                    required: true
                  },
                  'question[8]': {
                    required: true
                  },
                  'question[9]': {
                    required: true
                  },
                  'question[10]': {
                    required: true
                  },
                  'question[11]': {
                    required: true
                  },
                  'question[12]': {
                    required: true
                  },
                  'question[13]': {
                    required: true
                  },
                  'question[13b]': {
                    required: true
                  },
                  'question[14]': {
                    required: true
                  },
                  'question[15]': {
                    required: true
                  },
                  'question[16]': {
                    required: true
                  },
                  'question[17]': {
                    required: true
                  },
                  'question[18]': {
                    required: true
                  },
                  'question[19][fruits][]': {
                     require_from_group: [1, ".group-required"]
                  },
                  'question[20][stress][]': {
                    require_from_group: [1, ".group-required"]
                  },
                  'question[21][alcohol][]': {
                    require_from_group: [1, ".group-required"]
                  },
                  
                kituser: {
                    required: true
                },
                gender: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name",
                    maxlength: "max length 16 characters",
                    minlength: "Your first name must consist of at least 2 characters"
                },
                barcode: "Please enter kit barcode",
                mobile: "Please enter your mobile number",
                pincode: "Please enter your pincode",
                kituser: "Please select use kit",
                dob: "Date of birth field is required",
                gender: "Please select gender",
                email: {
                    required: "Please enter your email address",
                    email: "Please enter valid email address",
                },
                'question[1]': {
                    required: "This question requires an answer",
                },
                'question[2]': {
                    required: "This question requires an answer",
                },
                 'question[3]': {
                    required: "This question requires an answer",
                },
                 'question[3a]': {
                    required: "This question requires an answer",
                },
                 'question[4]': {
                    required: "This question requires an answer",
                },
                 'question[5]': {
                    required: "This question requires an answer",
                },
                 'question[6]': {
                    required: "This question requires an answer",
                },
                 'question[7]': {
                    required: "This question requires an answer",
                },
                 'question[8]': {
                    required: "This question requires an answer",
                },
                 'question[9]': {
                    required: "This question requires an answer",
                },
                 'question[10]': {
                    required: "This question requires an answer",
                },
                 'question[11]': {
                    required: "This question requires an answer",
                },
                 'question[12]': {
                    required: "This question requires an answer",
                },
                 'question[13]': {
                    required: "This question requires an answer",
                },
                 'question[14]': {
                    required: "This question requires an answer",
                },
                 'question[15]': {
                    required: "This question requires an answer",
                },
                 'question[16]': {
                    required: "This question requires an answer",
                },
                 'question[17]': {
                    required: "This question requires an answer",
                },
                 'question[18]': {
                    required: "This question requires an answer",
                },
                 'question[19][fruits][]': {
                    required: "This question requires an answer",
                },
                 'question[20][stress][]': {
                    required: "This question requires an answer",
                },
                 'question[21][alcohol][]': {
                    required: "This question requires an answer",
                },
            }
        });

        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
             
                if(currentIndex == 0 || currentIndex == 1 || currentIndex == 2){
                    $('#barcodereview').html($('#barcode').val());    
                    $('#birthdateID,#dobreview').html($('#dob').val());    
                    $('#sexID,#sexreview').html($('.gender').val());    
                }
                
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                //alert(1);
                $('#kitActivationfrm').submit()
            }
        });

    });
</script>   
