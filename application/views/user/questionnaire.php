<?php $this->load->view('include/header'); ?>
<div class="container inner-container">
  <div class="row">
    <div class="col-sm-4 col-md-3">
      <!-- USER MENU -->
      <?php $this->load->view('include/user_menu'); ?>
      <!-- END USER MENU -->
    </div>
    <div class="col-sm-8 col-md-9">
      <div class="user-content questionnaire-page">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="page-header">
              <h1>
                Questionnaire
              </h1>
            </div>
            <form>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Name</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Date of Birth</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Gender</label><br />
                    <label class="radio-inline"><input type="radio"
                    name="gen" id="" value="" /> Male</label>
                    <label class="radio-inline"><input type="radio"
                    name="gen" id="" value="" /> Female</label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Height(cm)</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Weight (Kg)</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Highest Edu.
                    Qua.</label> <input type="text" class=
                    "form-control" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Email Id</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="firstname">Mobile No.</label>
                    <input type="text" class="form-control" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q1. Which of the following best
                    describes your food pattern?</strong>
                  </div>
                  <ul>
                    <li align="center">
                      <input type="radio" name="q1" /> Vegetarian
                      (plant food, dairy products)
                    </li>
                    <li align="center">
                      <input type="radio" name="q1" />
                      Ova-vegetarian (plant food, dairy products,
                      egg)
                    </li>
                    <li align="center">
                      <input type="radio" name="q1" />
                      Non-vegeterian (meat,fish,chicken)
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q2. Define your eating habit by chosing
                    an appopriate option.</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q2" /> I always
                      follow proper meal timing and I choose healty
                      foods
                    </li>
                    <li>
                      <input type="radio" name="q2" /> Most of the
                      timing I eat healty foods and stick to my
                      reguler meal timing
                    </li>
                    <li>
                      <input type="radio" name="q2" /> I Sometimes
                      find time to eat healthy foods and stick but
                      folowing regular meal timing is difficult.
                    </li>
                    <li>
                      <input type="radio" name="q2" /> I really
                      follow a healty diet.
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q3 (a). Do you skip any of your
                    meal?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q3" /> No
                    </li>
                    <li>
                      <input type="radio" name="q3" /> Yes,
                      sometime
                    </li>
                    <li>
                      <input type="radio" name="q3" /> Yes always
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q3 (b). if yes, specify wgith meal(s)
                    you generally skip.</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q3a" /> Breakfast
                    </li>
                    <li>
                      <input type="radio" name="q3a" /> Lunch
                    </li>
                    <li>
                      <input type="radio" name="q3a" /> Dinner
                    </li>
                    <li>
                      <input type="radio" name="q3a" /> None
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q4. Are you allergic/intolerant toward
                    any one or more of the following
                    foods?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q4" /> Milk product
                    </li>
                    <li>
                      <input type="radio" name="q4" /> Eggs
                    </li>
                    <li>
                      <input type="radio" name="q4" /> Nuts
                      (penuts, walnuts)
                    </li>
                    
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q5. Do you have any food
                    dislikes?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q5" /> No
                    </li>
                    <li>
                      <input type="radio" name="q5" /> Yes 
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q6. How frequently do you dine
                    out?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q6" /> Everyday
                    </li>
                    <li>
                      <input type="radio" name="q6" /> 3 to 4 days
                      in a week
                    </li>
                    <li>
                      <input type="radio" name="q6" /> Once or
                      twice a week
                    </li>
                    <li>
                      <input type="radio" name="q6" /> Fortnightly
                    </li>
                    <li>
                      <input type="radio" name="q6" /> Once a month
                    </li>
                    <li>
                      <input type="radio" name="q6" /> Never
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q7. How regularly do you exercise
                    (like, walking, jogging, swimming, yoga, gym,
                    workouts) for at least half hour?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q7" /> Daily
                    </li>
                    <li>
                      <input type="radio" name="q7" /> 3 to 5 days
                      in a week
                    </li>
                    <li>
                      <input type="radio" name="q7" /> Only on
                      weekends
                    </li>
                    <li>
                      <input type="radio" name="q7" /> Occasionally whenever there is free time, say once in a month
                    </li>
                    <li>
                      <input type="radio" name="q7" /> very rare,
                      almost never
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q8. Which type of physical activity do
                    you prefer?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q8" /> Walking
                    </li>
                    <li>
                      <input type="radio" name="q8" /> Other
                      aerobic activities like jogging, swimming
                    </li>
                    <li>
                      <input type="radio" name="q8" /> Yoga
                    </li>
                    <li>
                      <input type="radio" name="q8" /> Exercising
                      in the gym
                    </li>
                    <li>
                      <input type="radio" name="q8" /> Please
                      provide your preference here if it is not in
                      the list
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q9. On an average how many hour do you
                    sleep at night.</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q9" /> &lt; 4 hour
                    </li>
                    <li>
                      <input type="radio" name="q9" /> 4-5 hours
                    </li>
                    <li>
                      <input type="radio" name="q9" /> 6-8 hours
                    </li>
                    <li>
                      <input type="radio" name="q9" /> &gt; 8 hours
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q10. How would you describe your
                    sleep?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q10" /> Disturbed
                      on most of the days
                    </li>
                    <li>
                      <input type="radio" name="q10" /> Disturbed
                      when I am stressed
                    </li>
                    <li>
                      <input type="radio" name="q10" /> Sound and
                      refreshing
                    </li>
                    <li>
                      <input type="radio" name="q10" /> Combination
                      of second and third option
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q11. To which group you identify
                    to?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q11" /> Disturbed
                      on most of the days) Group A (high strung
                      i.e. rigidly organized, sensitive, impatient,
                      proactive and obsessed with time management)
                    </li>
                    <li>
                      <input type="radio" name="q11" /> Group B
                      (easy going i.e. work steadily, enjoying,
                      enjoying achievement but not becoming
                      stressed when they are not achieved
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q12. How many cups of coffee or tea
                    (excluding herbal varieties like green tea) do
                    you consume in a day?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q12" /> &lt; 2 cups
                    </li>
                    <li>
                      <input type="radio" name="q12" /> 2 to 4 cups
                    </li>
                    <li>
                      <input type="radio" name="q12" /> &gt;4 cups
                    </li>
                    <li>
                      <input type="radio" name="q12" /> I don't
                      drink coffee/tea
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q13 (a). Which among the following
                    describes your smoking habit?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q13" /> I smoke
                      everyday
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I smoke
                      often, say 3-4 days per week
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I smoke
                      less frequently, say 1-2 days per week
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I smoke
                      occasionally
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I used to
                      smoke previously, but I have stopped smoking
                      recently (less than 5 years)
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I used to
                      smoke previously, but I have stopped smoking
                      several years ago (more 5 years)
                    </li>
                    <li>
                      <input type="radio" name="q13" /> I don't
                      smoke
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q 13 (b). If you are in the habit of
                    smoking kindly specify how many cigarettes you
                    smoke in a day.</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q13b" /> &gt;31
                    </li>
                    <li>
                      <input type="radio" name="q13b" /> 21-30
                    </li>
                    <li>
                      <input type="radio" name="q13b" /> 11 to 20
                    </li>
                    <li>
                      <input type="radio" name="q13b" /> 3 to 10
                    </li>
                    <li>
                      <input type="radio" name="q13b" /> &lt; 2
                    </li>
                    <li>
                      <input type="radio" name="q13b" /> &lt; 0
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q14. Are you in the habit of chewing
                    tobacco/pan?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q14" /> Yes,
                      regularly
                    </li>
                    <li>
                      <input type="radio" name="q14" /> Yes,
                      occasionally
                    </li>
                    <li>
                      <input type="radio" name="q14" /> No
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q15. Do you have a family history of
                    one or more of the following health
                    disorders?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q15" /> Obesity
                    </li>
                    <li>
                      <input type="radio" name="q15" /> Diabetes
                    </li>
                    <li>
                      <input type="radio" name="q15" />
                      Hypertension (High blood pressure)
                    </li>
                    <li>
                      <input type="radio" name="q15" /> Heart
                      disease
                    </li>
                    <li>
                      <input type="radio" name="q15" /> None of the
                      above
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q16. Are you suffring form onr more of
                    the following medical conditions?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q16" /> Diabetes
                    </li>
                    <li>
                      <input type="radio" name="q16" /> High blood
                      cholesterol levels
                    </li>
                    <li>
                      <input type="radio" name="q16" />
                      Hypertension (High blood pressure)
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Heart
                      disease
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Arthritis
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Bone
                      Disorders
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Hormonal
                      Disorders (Like hypothyroidism or PCOD)
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Respiratory
                      Disorders
                    </li>
                    <li>
                      <input type="radio" name="q16" /> Any
                      other(s), please specify
                    </li>
                    <li>
                      <input type="radio" name="q16" /> None of the
                      above
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q17. Are you under any prescribed
                    medication?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q17" /> No
                    </li>
                    <li>
                      <input type="radio" name="q17" /> Yes
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q18. Do you go for periodic health
                    checkups (master health checkups)?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="q18" /> No
                    </li>
                    <li>
                      <input type="radio" name="q18" /> Yes
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q19. How often do you consume these
                    foods?</strong>
                  </div>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>
                          Food Type
                        </th>
                        <th>
                          Every day
                        </th>
                        <th>
                          1-2
                          times/week
                        </th>
                        <th>
                          Once in 15 days
                        </th>
                        <th>
                          Once in a month
                        </th>
                        <th>
                          Occasionally/Never
                        </th>
                      </tr>
                    </thead>
                    <tr>
                      <td>
                        Fruits and Vegetables
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Milk and milk products
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Green Tea
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Processed/readyto-eat foods (noodles,
                        pasta)
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Fried snacks (chips, samosa)
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Fast foods (pizza, burger)
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Salt preserved foods (pickle, sauce,
                        ketchup
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Aerated drinks
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                      <td align="center">
                        <input type="radio" name="q19" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q 20. How frequently do you get
                    stressed or feel tired?</strong>
                  </div>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>
                          Stress/ Tiredness
                        </th>
                        <th>
                          Most of the days during work hours
                        </th>
                        <th>
                          When there is an increased workload
                        </th>
                        <th>
                          Sometimes when there is mentionable
                          physical strain, say travel
                        </th>
                        <th>
                          Rarely
                        </th>
                      </tr>
                    </thead>
                    <tr>
                      <td>
                        Stress
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Tiredness
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                      <td>
                        <input type="radio" name="q20" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="questions">
                    <strong>Q 21. How would you describe your
                    alcohol consumption pattern?</strong>
                  </div>
                  <ul>
                    <li>
                      <input type="radio" name="qa21" /> I have a
                      drink everyday
                    </li>
                    <li>
                      <input type="radio" name="qa21" /> I drink on
                      weekends when I go out
                    </li>
                    <li>
                      <input type="radio" name="qa21" /> I drink
                      during occasionally when I go out
                    </li>
                    <li>
                      <input type="radio" name="qa21" /> I do not
                      consume alcohol
                    </li>
                  </ul>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>
                          Alcoholic beverage (Quantity)
                        </th>
                        <th>
                          Champagne (Small peg=30ml)
                        </th>
                        <th>
                          Wine (small peg=30ml)
                        </th>
                        <th>
                          Beer (1 mug or can=280ml)
                        </th>
                        <th>
                          Spirits (small peg=30ml)
                        </th>
                      </tr>
                    </thead>
                    <tr>
                      <td>
                        1
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        2
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        3
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        4
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        5
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                      <td>
                        <input type="radio" name="q21" />
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- .container -->
<?php $this->load->view('include/footer'); ?>