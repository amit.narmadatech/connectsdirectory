<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
  <div class="container inner-container">
  	<div class="row">
            <div class="col-sm-4 col-md-3">
        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
</div>
  	 <div class="col-sm-8 col-md-9">

    <div class="user-content">
	<?php $this->load->view('include/alert'); ?>
	
	<div class="row">
			<?php 
				$oldimg = (isset($user) && (!empty($user->image)))?$user->image:''; 
			?>
		<!-- START SIDEBAR -->
		<?php $this->load->view('include/user_sidebar'); ?>
		<!-- END SIDEBAR -->		
		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="page-header">
				<div class="row">
                    <div class="col-md-6">
                        <h1>Billing Address</h1>
                    </div>
                    <div class="col-md-6">
                        <a href="<?php echo base_url('billingaddress');?>" class="btn btn-primary waves-effect btn-md pull-right">Add New</a>
                    </div>
                </div>
			</div>

			<div class="row">
				<?php  
				if (!empty($addressList)) {
					$i=1;
					foreach ($addressList as $address) { 
				?>
		        <!--Card-->
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="card">
					   <h3 class="card-header primary-color white-text"><?= $address->name ?> 
						<a class="btn-floating btn-sm blue-gradient pull-right white-text" href="<?= base_url('BillingAddress/index/'.$address->id) ?>"><i class="fa fa-edit" aria-hidden="true"></i></a></h3>
						<div class="card-body">
						    <p class="card-title"><label><b>Mobile: </b></label> <?= $address->mobile1 ?>, <?= $address->mobile2 ?></p>
					        <p class="card-title"><label><b>Email:</b> </label> <?= $address->email ?></p>
					        <p class="card-text"><label><b>Address: </b></label> <?= $address->address ?></p>				<div class="form-group">
							    <input <?= ($address->is_default)?'checked':''  ?> name="is_default" type="radio" class="with-gap" id="addressID_<?php echo $i; ?>">
							    <label for="addressID_<?php echo $i; ?>">Default</label>
							</div>
						</div>
					</div>
		        </div>
				<!--/.Card-->
				<?php $i++;	}
				} ?>
			</div>
   		  </div>
		 </div>
	  </div><!-- .row -->
	</div><!-- .container -->