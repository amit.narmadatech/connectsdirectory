<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>
    invoice
  </title>
</head>
<body style="margin: 0; padding: 200px;">
    <div style="width:540px;">
        <table cellpadding="0" cellspacing="0" style="width: 540px; border-collapse: collapse; margin:0 auto; border: solid 2px #A2A2A2;">
            <thead>
                <tr>
                    <th style="background: #EEE; text-align: left; border-bottom: solid 2px #A2A2A2; vertical-align: middle;" ><br><br>
                     <img src="<?php echo base_url('/assets/images/logo.png'); ?>" width="100px" />
                    </th>               
                    <th style="background: #EEE; color: #000; font-size: 16pt; text-align: right; font-family: helvetica; border-bottom: solid 2px #A2A2A2; vertical-align: middle; line-height: 40px"><br>
                            Invoice &nbsp; 
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="3" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                            <tr>
                                <td style="font-family: helvetica; font-size:10pt;"><strong>Shipping To</strong></td>                           
                            </tr>
                            <tr>
                               <td style="font-family: helvetica; font-size:8pt; text-transform: capitalize;"><?php echo!empty($orders->name) ? $orders->name : ''; ?></td>
                            </tr>
                            <tr>
                               <td style="font-family: helvetica; font-size:8pt;"><?php echo!empty($orders->address) ? $orders->address : ''; ?>, <?php echo !empty(getDistrict($orders->district_id)) ? getDistrict($orders->district_id)->district_title : ''; ?>, <?php echo !empty(getState(getDistrict($orders->district_id)->state_id)) ? getState(getDistrict($orders->district_id)->state_id)->state_title : ''; ?>   </td>
                            </tr>                             
                            <tr>
                                <td style="font-family: helvetica; font-size:8pt;">+91 <?php echo!empty($orders->mobile1) ? $orders->mobile1 : ''; ?> </td> 
                            </tr>
                            <tr>
                                <td style="font-family: helvetica; font-size:8pt;" colspan="2"><?php echo!empty($orders->email) ? $orders->email : ''; ?> </td>  
                              </tr>
                            </tbody>
                        </table>
                    </td>   
                    <td>
                        <table cellpadding="0" cellspacing="5" style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">GSTIN : </td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">23AAECJ1917G1ZX  &nbsp;  </td>
                                </tr>
                                <tr>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">Transation Id :</td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">#13456   &nbsp;  </td>
                                </tr>
                                <tr>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;">Order No:</td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;"><?php echo!empty($orders->order_number) ? $orders->order_number : ''; ?>   &nbsp; </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right; font-family: helvetica; font-size:8pt;">Date: </td>
                                    <td style="font-family: helvetica; font-size:8pt; text-align: right;"><?php echo!empty($orders->order_date) ? date('d-m-Y', strtotime($orders->order_date)) : ''; ?>  &nbsp; </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    
                    </tr>
                    <tr>
                        <td style="width: 100%;" colspan="2"><table cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                        <tr>
                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">Plan </td>
                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">Plan Name </td>
                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">Price </td>
                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">QTY  </td>
                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; text-align: right;">Total </td>
                        </tr><?php
				$amount =0 ;
                                if (!empty($orderDetails)) {
                                    foreach ($orderDetails as $orderDetailsval) {

                                        if (file_exists("uploads/plan_img/" . $orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image)) {

                                            $img = base_url() . 'uploads/plan_img/' . $orderDetailsval->plan_image;
                                        } else {
                                            $img = base_url() . 'assets/images/diet-plan.jpg';
                                        }
					  $amount = ($amount+($orderDetailsval->quantity*$orderDetailsval->plan_price));	
									 	
                                    ?>
                                <tr>
                                   <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2;">
                                        <img src="<?php echo $img;?>"/>
                                    </td>
                                    <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">
                                        <?php echo!empty($orderDetailsval->name) ? $orderDetailsval->name : ''; ?>
                                    </td>
                                    <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">
                                         <?php echo!empty($orderDetailsval->plan_price) ? $orderDetailsval->plan_price : ''; ?>
                                    </td>
                                    <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; line-height:40px;">
                                        <?php echo!empty($orderDetailsval->quantity) ? $orderDetailsval->quantity : ''; ?>
                                   </td>
                                   <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #A2A2A2; text-align: right; line-height:40px;">
                                       <?php echo!empty($orderDetailsval->plan_price) ? ($orderDetailsval->quantity*$orderDetailsval->plan_price) : '0.0'; ?>
                                    </td>
                                </tr><?php }
                                    } ?>

				<tr>
                                   <td colspan="3" style="border-top:solid 2px #A2A2A2;"></td>
                                   <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                         Sub-Total:  </td>
                                   <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px;">
                                         <?php echo!empty($amount) ? $amount : '0.00'; ?> &nbsp;&nbsp;
                                    </td>
                                 </tr>
                                <?php if(!empty($orders->igst)) {?>
                                    <tr>
                                      <td colspan="3"></td>
                                      <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                    IGST :    </td>
                                     <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px; border-top:solid 2px #A2A2A2;">
                                       <?php echo!empty($orders->igst) ? $orders->igst : '0.00'; ?>&nbsp;&nbsp;
                                   </td>
                                   </tr>
                                <?php }else{ ?>
                                <tr>
                                    <td colspan="3"></td>
                                    <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                        CGST :   </td>
                                    <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px; border-top:solid 2px #A2A2A2;">
                                        <?php echo!empty($orders->cgst) ? $orders->cgst : '0.00'; ?> &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                    <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                        SGST :   </td>
                                    <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px; border-top:solid 2px #A2A2A2;">
                                        <?php echo!empty($orders->sgst) ? $orders->sgst : '0.00'; ?> &nbsp;&nbsp;
                                    </td>
                                </tr>
                                 <?php }?>
                                <tr>
                                    <td colspan="3"></td>
                                   <td valign="middle" style="vertical-align: middle; font-weight: bold; border-top:solid 2px #A2A2A2;  font-family: helvetica; font-size:8pt; line-height:15px; border-left: 1px solid #ddd;">
                                  Grand Total:  </td>
                                  <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px; border-top:solid 2px #A2A2A2;">
                                      <?php echo!empty($orders->total_amount) ? $orders->total_amount : ''; ?> &nbsp;&nbsp;
                                  </td>
                              </tr>
                          </table> 
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>