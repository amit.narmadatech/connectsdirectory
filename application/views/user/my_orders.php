<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>
<section class="content">
 	<div class="container inner-container">
 		<div class="row">
            <div class="col-sm-4 col-md-3">
        <!-- USER MENU -->
        <?php $this->load->view('include/user_menu'); ?>
        <!-- END USER MENU -->
    </div>
    <div class="col-sm-8 col-md-9">
   		<div class="user-content">	
			<?php $this->load->view('include/alert'); ?>
			<div class="page-header">
           		 <h1>My Orders</h1>
       		</div>
    	

		<div class="row">
			<div class="col-sm-12">
			<?php 
			   if (!empty($orders)) {
					foreach ($orders as $ordersVal) { 					
					$orderDetails = getOrderDetails($ordersVal->id);
						if(!empty($orderDetails)){  ?>
				
				<div class="order-list">
					<div class="order-unit-title">
					<p><strong>ORDER PLACED ON - </strong> <?php echo !empty($ordersVal->order_date)? date('D dS F Y', strtotime($ordersVal->order_date)) :''; ?>
 					</p>
					</div>
					<ul class="list-inline">
						<li>
							Order Id: <?php echo $ordersVal->order_number ;?> 
						</li>
						<li><?php echo !empty($ordersVal->name)?$ordersVal->name:''; ?></li>
						<li class="right-info">Total Price: <i class="fa fa-inr"></i> <strong> <?php echo $ordersVal->total_amount; ?> </strong>
						</li>
					</ul>
					<div class="order-unit-goods-info">				

					<?php 
					foreach ($orderDetails as $orderDetailsval) {
						if (file_exists("uploads/plan_img/".$orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image))
						{
											
						 $img = base_url().'uploads/plan_img/'.$orderDetailsval->plan_image;
					}else{
						 $img = base_url().'assets/images/diet-plan.jpg';
					}
					?>
                                        <div class="unit-info">
                                            <div class="left-goods-img">
                                                <a href="<?php echo base_url('user/orderdetails/' . $ordersVal->id) ?>">
                                                    <img src="<?php echo $img; ?>" class="img-responsive" width="120" height="80" style="width:80px; height: 60px; float: left; margin-right: 5px;">
                                                </a>	
                                            </div>

                                            <div class="mid-goods-name">
                                                <a href="<?php echo base_url('user/orderdetails/' . $ordersVal->id) ?>"><?php echo!empty($orderDetailsval->name) ? $orderDetailsval->name : ''; ?></a> <br>
                                                <i class="fa fa-inr"></i> <?php echo!empty($orderDetailsval->plan_price) ? $orderDetailsval->plan_price : ''; ?> X

                                                <?php echo!empty($orderDetailsval->quantity) ? $orderDetailsval->quantity : ''; ?> 
                                            </div>
                                        </div>
                                            <?php  }   ?>
                                            <div class="right-goods-block">
                                                    <a href="<?php echo base_url('user/orderdetails/'.$ordersVal->id) ?>" class="flat-btn"> Order Detail </a>
                                            </div>
					</div>
				</div>
				<?php  }
				 }
				}else{ ?>
				
				<div class="alert alert-danger">
					 No order available.
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				</div>				
				<?php }?>					      							
			</div>
		</div>
	</div>	
</div>
</div>
</div>
</section>

<?php $this->load->view('include/footer'); ?>