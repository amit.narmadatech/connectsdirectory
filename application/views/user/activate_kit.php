<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<link href="<?php echo base_url(); ?>assets/css/jquery.steps.css" rel="stylesheet">

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <style>
                    .align-center-radio tr td{
                        text-align: center;
                    }
                </style>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">	
                    <div class="page-header">
                        <h1>Activate Kit</h1>
                    </div>
                    <?php $oldimg = (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>
                    <div class="row clearfix">
                        <!-- Task Info -->

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <?php if ($this->session->userdata('success')) { ?>
                            <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
                                <?php 
                                echo $this->session->userdata('success');
                                 $this->session->unset_userdata('success');
                                ?> 
                            </div>
                         <?php } ?>
                           <?php if ($this->session->userdata('error')) { ?>
                            <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                             <?php 
                             echo $this->session->userdata('error');
                              $this->session->unset_userdata('error');
                             ?> 
                            </div>
                         <?php } ?>  
                            <form id="kitActivationfrm" method="post" action="<?php echo base_url('user/post_activate_kit'); ?>">
                                <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                                <input type="hidden" id="token" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />      
                                <div>
                                    <h3>kit </h3>   
                                    <section>
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label> Barcode <span style="color:red">*</span></label>            
                                                       <input id="barcode" type="text" name="barcode" value="" class="form-control">   
                                                </div>
                                                <span id="barcodeerror" style="color:red"><?php echo form_error('barcode'); ?></span>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>            
                                                    <button class="flat-button border form-control" type="button" id="checkBarcode"> Check <i id="barcodeBtnIcon" class="fa fa-barcode"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <h6>Who will be using this Kit</h6>
                                        <div class="row">
                                            <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="kituser" class="kituser"  value="self"> I will be using this kit.
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="kituser" class="kituser"  value="other"> Someone else will be using this kit.
                                                    </label> 
                                                </div>
                                                <span id="kitusererror"></span>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="firstname">Name</label>
                                                    <input id="name" type="text" name="name" class="form-control">   
                                                </div>
                                                <span id="nameerror"></span>
                                                <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                     <?php $end_date = date('Y-m-d'); ?>

                                                    <label for="dob">Date of Birth</label>
                                                    <input id="dob" type="date" name="dob" max="<?php echo $end_date; ?>" class="form-control">   
                                                </div>
                                                <span id="doberror"></span>
                                                <?php echo form_error('dob', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="firstname"> Email Id </label>
                                                    <input id="emailID" type="text" name="email" class="form-control">   
                                                </div>
                                                <span id="emailerror"></span>
                                                <?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="mobile"> Mobile No.</label>
                                                    <input id="mobile" type="text" name="mobile" class="form-control">   
                                                </div>
                                                <span id="mobileerror"></span>
                                                <?php echo form_error('mobile', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="firstname"> Gender </label> <br>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="gender" class="gender"  value="Male"> Male
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="gender" class="gender" value="Female"> Female
                                                    </label> 
                                                </div>
                                                <span id="gendererror"></span>
                                                <?php echo form_error('gender', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="weight">  Weight (Kg) </label>
                                                    <input type="text" name="weight" class="form-control" value="20">   
                                                </div>
                                                <?php echo form_error('weight', '<div class="text-danger">', '</div>'); ?>
                                            </div>
                                            <div class="col-sm-6">  
                                                <div class="form-group">
                                                    <label for="height">  Height (cm) </label>
                                                    <input id="height" type="text" name="height" class="form-control" value="20">   
                                                </div>
                                                <?php echo form_error('height', '<div class="text-danger">', '</div>'); ?>
                                            </div>

                                        </div>
                                    </section>
                                    <h3> Consent </h3>
                                    <section>
                                        <div class="step-term">
                                            <h4>Term of Services & Privacy Statement</h4>

                                            <ol>
                                                <li> Users are free to delete their raw data information from their account anytime after their reports are generated.&nbsp;If not, we will take due care to secure your data.</li>

                                                <li> The security measures and systems are subject to the current state of the art of data protection. However, do note that software and data breaches do occur even at highly secured IT infrastructure, including high data security government/military IT infrastructure. We will not be liable for such malicious breaches or hacking attacks beyond our control.</li>

                                                <li> Losses or damages due to obsolescence of security systems on account of technological advances does not bind us to any liability, nor does it oblige it to make any refunds, recalls or concessions.</li>

                                                <li> Access to data, be it registration information, genetic and self-reported information, automated information or data of any other kind will be shared with regulatory authorities if we are bound to comply with the requirements of the law of the land.</li>
                                            </ol>
                                        </div>
                                        <label>  You must provide legal authorization to consent for:</label>
                                        <p> <strong><span id="nameID"></span> Register</strong> , Birthdate : <span id="birthdateID"></span> , sex: <span id="sexID"></span></p> 
                                        <div class="radio" id="consentforid1">
                                            <label><input type="radio" name="consent_for"  class="consent_for" value="1"> I am this person, I have read this document and I do give consent</label>
                                        </div>
                                        <div class="radio" id="consentforid2">
                                            <label> <input type="radio" name="consent_for"  class="consent_for" value="2"> I am legally authorized to give consent for this person and I do give consent </label>
                                        </div>
                                        <span id="consenterror"></span>
                                    </section>
                                    <h3>Summary</h3>
                                    <section>
                                        <h4>Review </h4>
                                        <h6> Your kit activation is not complete until you click Finish </h6>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4"> Barcode :</label>
                                                <div class="col-sm-8" id="barcodereview">  </div>
                                            </div>    
                                            <div class="form-group">
                                                <label class="col-sm-4"> Registered to:</label>
                                                <div class="col-sm-8" id="namereview"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4"> Birth date:  </label>
                                                <div class="col-sm-8" id="dobreview"> </div>
                                            </div>    
                                            <div class="form-group">
                                                <label class="col-sm-4"> Sex:  </label>
                                                <div class="col-sm-8" id="sexreview"> </div>
                                            </div>   
                                            <div class="form-group">
                                                <label class="col-sm-4"> Consents:  </label>
                                                <div class="col-sm-8"> Accepted <br>
                                                </div>
                                            </div>     
                                        </div>  
                                    </section>
                                    <h3>Questionnaire</h3>
                                    <section>
                                        <h4>Questionnaire</h4>
                                        <p style="color: red">Note : All question requires an answer</p>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions">
                                                    <strong> Q1. Which of the following best describes your food pattern? </strong>
                                                    <span id="question_1_error"></span>
                                                </div>
                                                <ul>
                                                    <li><input type="radio" name="question[1]" value="1"> Vegetarian (plant food, dairy products)</li>
                                                    <li><input type="radio" name="question[1]" value="2"> Ova-vegetarian (plant food, dairy products, egg)</li>
                                                    <li><input type="radio" name="question[1]" value="3"> Non-vegetarian (meat,fish,chicken) </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions">
                                                    <strong> Q2. Define your eating habit by choosing an appropriate option.</strong>
                                                    <span id="question_2_error"></span>
                                                </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[2]" value="1"> I always follow proper meal timing and I choose healthy foods </li>
                                                    <li> <input type="radio" name="question[2]" value="2"> Most of the timing I eat healthy foods and stick to my regular meal timing</li>
                                                    <li> <input type="radio" name="question[2]" value="3"> I Sometimes find time to eat healthy foods and stick but following regular meal timing is difficult.</li>
                                                    <li> <input type="radio" name="question[2]" value="4"> I rarely follow a healthy diet.</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions">
                                                    <strong> Q3 (a). Do you skip any of your meal? </strong>
                                                    <span id="question_3_error"></span>
                                                </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[3]" value="1"> No </li>
                                                    <li> <input type="radio" name="question[3]" value="2"> Yes, sometime </li>
                                                    <li> <input type="radio" name="question[3]" value="3"> Yes always </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> 
                                                    <strong> Q3 (b). if yes, specify which meal(s) you generally skip.</strong>
                                                       <span id="question_3a_error"></span>
                                                </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[3a]" value="1"> Breakfast </li>
                                                    <li> <input type="radio" name="question[3a]" value="2"> Lunch </li>
                                                    <li> <input type="radio" name="question[3a]" value="3"> Dinner </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions">
                                                    <strong>  Q4. Are you allergic/intolerant towards any one or more of the followings foods? </strong>
                                                      <span id="question_4_error"></span>
                                                </div>
                                                <ul>
                                                    <li>  <input type="checkbox" class="checkbox4" name="question[4][]" value="1"> Milk product </li>
                                                    <li>  <input type="checkbox" class="checkbox4" name="question[4][]" value="2"> Eggs </li>
                                                    <li>  <input type="checkbox" class="checkbox4" name="question[4][]" value="3"> Nuts (peanuts, walnuts) </li>
                                                    <li>  <input type="checkbox" class="checkbox4" name="question[4][]" value="4">Sea Food </li>
                                                    <li>  <input type="checkbox" class="checkbox4" id="checkbox4none" name="question[4][]" value="5"> None </li>
                                                    
                                                  <?php /*?> <li>  <input type="radio" name="question[4]" value="1"> Milk product </li>
                                                    <li>  <input type="radio" name="question[4]" value="2"> Eggs </li>
                                                    <li>  <input type="radio" name="question[4]" value="3"> Nuts (peanuts, walnuts) </li>
                                                    <li>  <input type="radio" name="question[4]" value="4"> Any other </li><?php */?>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q5. Do you have any food dislikes? </strong>
                                                     <span id="question_5_error"></span>
                                                </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[5]" value="1"> No </li>
                                                    <li> <input type="radio" name="question[5]" value="2"> Yes </li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q6. How frequently do you dine out? </strong> 
                                                    <span id="question_6_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[6]" value="1"> Everyday </li>
                                                    <li> <input type="radio" name="question[6]" value="2"> 3 to 4 days in a week </li>
                                                    <li> <input type="radio" name="question[6]" value="3"> Once or twice a week </li>
                                                    <li> <input type="radio" name="question[6]" value="4"> Fortnightly </li>
                                                    <li> <input type="radio" name="question[6]" value="5"> Once a month </li>
                                                    <li> <input type="radio" name="question[6]" value="6"> Never </li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q7. How regularly do you exercise (like, walking, jogging, swimming, yoga, gym, workouts) for at least half an hour? </strong>
                                                    <span id="question_7_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[7]" value="1"> Daily </li>
                                                    <li> <input type="radio" name="question[7]" value="2"> 3 to 5 days in a week </li>
                                                    <li> <input type="radio" name="question[7]" value="3"> Only on weekends </li>
                                                    <li> <input type="radio" name="question[7]" value="4"> Occasionally whenever there is free time, say once in a month </li>
                                                    <li> <input type="radio" name="question[7]" value="5"> very rare, almost never </li>

                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>   Q8. Which type of physical activity do you prefer? </strong> <span id="question_8_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[8]" value="1"> Walking </li>
                                                    <li><input type="radio" name="question[8]" value="2"> Other aerobic activities like jogging, swimming </li>
                                                    <li><input type="radio" name="question[8]" value="3"> Yoga </li>
                                                    <li><input type="radio" name="question[8]" value="4"> Exercising in the gym</li>
                                                    <li><input type="radio" name="question[8]" value="5">None</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong> Q9. On an average how many hours do you sleep at night.</strong> <span id="question_9_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[9]" value="1"> < 4 hour </li>
                                                    <li> <input type="radio" name="question[9]" value="2"> 4-6 hours </li>
                                                    <li> <input type="radio" name="question[9]" value="3"> 6-8 hours </li>
                                                    <li> <input type="radio" name="question[9]" value="4"> > 8 hours </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q10. How would you describe your sleep? </strong> <span id="question_10_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[10]"  value="1"> Disturbed on most of the days</li>
                                                    <li><input type="radio" name="question[10]"  value="2"> Disturbed when I am stressed</li>
                                                    <li> <input type="radio" name="question[10]"  value="3"> Sound and refreshing </li>
                                                    <li> <input type="radio" name="question[10]"  value="4"> Combination of second and third option </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q11. To which group you identify to? </strong> <span id="question_11_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[11]" value="1"> Disturbed on most of the days) Group A (high strung i.e. rigidly organized, sensitive, impatient, proactive and obsessed with time management) </li>
                                                    <li><input type="radio" name="question[11]" value="2"> Group B (easy going i.e. work steadily, enjoying, enjoying achievement but not becoming stressed when they are not achieved </li>                                   
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q12. How many cups of coffee or tea (excluding herbal varieties like green tea) do you consume in a day? </strong> <span id="question_12_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[12]" value="1"> < 2 cups </li>
                                                    <li> <input type="radio" name="question[12]" value="2"> 2 to 4 cups </li>
                                                    <li> <input type="radio" name="question[12]" value="3"> >4 cups   </li>
                                                    <li> <input type="radio" name="question[12]" value="4"> I don't drink coffee/tea                       
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions"> <strong>  Q13 (a). Which among the following describes your smoking habit? </strong> <span id="question_13_error"></span> </div>
                                                <ul>
                                                    <li> <input type="radio" name="question[13]" value="1"> I smoke everyday </li>
                                                    <li> <input type="radio" name="question[13]" value="2"> I smoke often, say 3-4 days per week </li>
                                                    <li> <input type="radio" name="question[13]" value="3"> I smoke less frequently, say 1-2 days per week </li>
                                                    <li> <input type="radio" name="question[13]" value="4"> I smoke occasionally</li>
                                                    <li> <input type="radio" name="question[13]" value="5"> I used to smoke previously, but I have stopped smoking recently (less than 5 years) </li>
                                                    <li> <input type="radio" name="question[13]" value="6"> I used to smoke previously, but I have stopped smoking several years ago (more 5 years) </li>
                                                    <li><input type="radio" name="question[13]" value="7"> I don't smoke</li>
                                                </ul>
                                            </div>
                                        </div>                                  
                                        <div class="row">
                                            <div class="col-sm-12"> 
                                                <div class="questions">
                                                    <strong> Q 13 (b). If you are in the habit of smoking kindly specify how many cigarettes you smoke in a day. </strong>
                                                    <span id="question_13b_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[13b]" value="1"> >31 </li>
                                                    <li><input type="radio" name="question[13b]" value="2"> 21-30 </li>
                                                    <li><input type="radio" name="question[13b]" value="3"> 11 to 20 </li>
                                                    <li><input type="radio" name="question[13b]" value="4"> 3 to 10 </li>
                                                    <li><input type="radio" name="question[13b]" value="5"> < 2 </li>
                                                    <li><input type="radio" name="question[13b]" /> 0</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions">  <strong> Q14. Are you in the habit of chewing tobacco/pan? </strong> <span id="question_14_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[14]" value="1"> Yes, regularly</li>
                                                    <li><input type="radio" name="question[14]" value="2"> Yes, occasionally </li>
                                                    <li><input type="radio" name="question[14]" value="3"> No</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong>  Q15. Do you have a family history of one or more of the following health disorders? </strong> <span id="question_15_error"></span> </div>
                                                <ul>
                                                  <?php /* ?>  
                                                    <li><input type="radio" name="question[15]" value="1"> Obesity</li>
                                                    <li><input type="radio" name="question[15]" value="2"> Diabetes</li>
                                                    <li><input type="radio" name="question[15]" value="3"> Hypertension (High blood pressure) </li>
                                                    <li><input type="radio" name="question[15]" value="4"> Heart disease</li>
                                                    <li><input type="radio" name="question[15]" value="5"> Any other(s)</li>
                                                    <li><input type="radio" name="question[15]" value="6"> None of the above</li>
                                                   <?php */ ?>
                                                    <li><input type="checkbox" class="checkbox15" name="question[15][]" value="1"> Obesity</li>
                                                    <li><input type="checkbox" class="checkbox15" name="question[15][]" value="2"> Diabetes</li>
                                                    <li><input type="checkbox" class="checkbox15" name="question[15][]" value="3"> Hypertension (High blood pressure) </li>
                                                    <li><input type="checkbox" class="checkbox15" name="question[15][]" value="4"> Heart disease</li>
                                                    <li><input type="checkbox" class="checkbox15" name="question[15][]" value="5"> Others</li>
                                                    <li><input type="checkbox" class="checkbox15" id="checkbox15none" name="question[15][]" value="6"> None of the above</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong>  Q16. Are you suffering from one more of the following medical conditions? </strong> <span id="question_16_error"></span> </div>
                                                <ul>
                                                      <?php /* ?>  
                                                    <li><input type="radio" name="question[16]" value="1"> Diabetes</li>
                                                    <li><input type="radio" name="question[16]" value="2"> High blood cholesterol levels</li>
                                                    <li><input type="radio" name="question[16]" value="3"> Hypertension (High blood pressure)</li>
                                                    <li><input type="radio" name="question[16]" value="4"> Heart disease </li>
                                                    <li><input type="radio" name="question[16]" value="5"> Arthritis</li>
                                                    <li><input type="radio" name="question[16]" value="6"> Bone Disorders</li>
                                                    <li><input type="radio" name="question[16]" value="7"> Hormonal Disorders (Like hypothyroidism or PCOD)</li>
                                                    <li><input type="radio" name="question[16]" value="8"> Respiratory Disorders</li>
                                                    <li><input type="radio" name="question[16]" value="9"> Any other(s)</li>
                                                    <li><input type="radio" name="question[16]" value="10"> None of the above</li>
                                                    <?php */ ?>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="1"> Diabetes</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="2"> High blood cholesterol levels</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="3"> Hypertension (High blood pressure)</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="4"> Heart disease </li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="5"> Arthritis</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="6"> Bone Disorders</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="7"> Hormonal Disorders (Like hypothyroidism or PCOD)</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="8"> Respiratory Disorders</li>
                                                    <li><input type="checkbox" class="checkbox16" name="question[16][]" value="9"> Others</li>
                                                    <li><input type="checkbox" class="checkbox16" id="checkbox16none" name="question[16][]" value="10"> None of the above</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"><strong> Q17. Are you under any prescribed medication? </strong> <span id="question_17_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[17]" value="1"> No</li>
                                                    <li><input type="radio" name="question[17]" value="2"> Yes </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong> Q18. Do you go for periodic health checkups (master health checkups)? </strong> <span id="question_18_error"></span> </div>
                                                <ul>
                                                    <li><input type="radio" name="question[18]" value="1"> No</li>
                                                    <li><input type="radio" name="question[18]" value="2"> Yes</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong> Q19. How often do you consume these foods? </strong> <span id="question_19_error"></span>
                                                </div>  
                                                <div class="table-responsive">                              
                                                <table class="table table-bordered align-center-radio">
                                                    <thead>
                                                    <th>Food Type</th>
                                                    <th>Every day </th>
                                                    <th>1-2 times/week </th>
                                                    <th>Once in 15 days</th>
                                                    <th>Once in a month </th>
                                                    <th>Occasionally/Never</th>
                                                    </thead>
                                                    <tr>
                                                        <td>Fruits and Vegetables</td>
                                                        <td><input type="radio" name="question[19][fruits][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fruits][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fruits][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fruits][]"  value="4" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fruits][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Milk and milk products</td>
                                                        <td><input type="radio" name="question[19][milk][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][milk][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][milk][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][milk][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][milk][]" value="1" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Green Tea</td>
                                                        <td><input type="radio" name="question[19][tea][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][tea][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][tea][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][tea][]" value="4" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][tea][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Processed/ready-to-eat foods (noodles, pasta)</td>
                                                        <td><input type="radio" name="question[19][eatfoods][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][eatfoods][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][eatfoods][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][eatfoods][]" value="4" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][eatfoods][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Fried snacks (chips, samosa)</td>
                                                        <td><input type="radio" name="question[19][snacks][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][snacks][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][snacks][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][snacks][]" value="4" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][snacks][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Fast foods (pizza, burger)</td>
                                                        <td><input type="radio" name="question[19][fastfoods][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fastfoods][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fastfoods][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][fastfoods][]" value="4" class="group-required" ></td>
                                                        <td><input type="radio" name="question[19][fastfoods][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Salt preserved foods (pickle, sauce, ketchup) </td>
                                                        <td><input type="radio" name="question[19][saltfoods][]" value="1" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][saltfoods][]" value="2" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][saltfoods][]" value="3" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][saltfoods][]" value="4" class="group-required"></td>
                                                        <td><input type="radio" name="question[19][saltfoods][]" value="5" class="group-required"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Aerated drinks</td>
                                                        <td><input type="radio" name="question[19][drinks][]" value="1"></td>
                                                        <td><input type="radio" name="question[19][drinks][]" value="2"></td>
                                                        <td><input type="radio" name="question[19][drinks][]" value="3"></td>
                                                        <td><input type="radio" name="question[19][drinks][]" value="4"></td>
                                                        <td><input type="radio" name="question[19][drinks][]" value="5"></td>
                                                    </tr>                                   
                                                </table>
                                            </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong> Q20. How frequently do you get stressed or feel tired? </strong> <span id="question_20_error"></span> </div>
                                                <div class="table-responsive">
                                                <table class="table table-bordered align-center-radio">
                                                    <thead>
                                                    <th>Stress/ Tiredness</th>
                                                    <th>Most of the days during work hours</th>
                                                    <th>When there is an increased workload</th>
                                                    <th>Sometimes when there is mentionable physical strain, say travel</th>
                                                    <th>Rarely</th>
                                                    </thead>
                                                    </tr>
                                                    <tr>
                                                        <td> Stress</td>
                                                        <td><input type="radio" name="question[20][stress][]" value="1"></td>
                                                        <td><input type="radio" name="question[20][stress][]" value="2" ></td>
                                                        <td><input type="radio" name="question[20][stress][]" value="3"></td>
                                                        <td><input type="radio" name="question[20][stress][]" value="4"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Tiredness</td>
                                                        <td><input type="radio" name="question[20][tiredness][]" value="1"></td>
                                                        <td><input type="radio" name="question[20][tiredness][]" value="2"></td>
                                                        <td><input type="radio" name="question[20][tiredness][]" value="3"></td>
                                                        <td><input type="radio" name="question[20][tiredness][]" value="4"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="questions"> <strong>Q 21. How would you describe your alcohol consumption pattern? </strong> <span id="question_21_error"></span>
                                                </div>
                                                <ul>
                                                    <li><input type="radio" name="question[21][alcohol][]" value="1" class="group-required question21"> I have a drink everyday</li>
                                                    <li><input type="radio" name="question[21][alcohol][]" value="2" class="group-required question21"> I drink on weekends when I go out</li>
                                                    <li><input type="radio" name="question[21][alcohol][]" value="3" class="group-required question21"> I drink occasionally when I go out</li>
                                                    <li><input type="radio" name="question[21][alcohol][]" value="4" id="questionID21" class="group-required question21"> I do not consume alcohol</li>
                                                </ul>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered align-center-radio" id="questiontable21">
                                                    <tr>
                                                    <thead>
                                                    <th>Alcoholic beverage (Quantity)</th>
                                                    <th>Champagne (Small peg=30ml)</th>
                                                    <th>Wine (small peg=30ml)</th>
                                                    <th>Beer (1 mug or can=280ml)</th>
                                                    <th>Spirits (small peg=30ml)</th>
                                                    </thead>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>                                                       
                                                        <td><input type="radio" name="question[21][champagne][]" value="1"></td>
                                                        <td><input type="radio" name="question[21][wine][]" value="1"></td>
                                                        <td><input type="radio" name="question[21][beer][]" value="1"></td>
                                                        <td><input type="radio" name="question[21][spirits][]" value="1"></td>   
                                                    </tr>
                                                    <tr>
                                                      <td>2</td>                                                      
                                                        <td><input type="radio" name="question[21][champagne][]" value="2"></td>
                                                        <td><input type="radio" name="question[21][wine][]" value="2"></td>
                                                        <td><input type="radio" name="question[21][beer][]" value="2"></td>
                                                        <td><input type="radio" name="question[21][spirits][]" value="2"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td><input type="radio" name="question[21][champagne][]" value="3"></td>                                                       
                                                        <td><input type="radio" name="question[21][wine][]" value="3"></td>
                                                        <td><input type="radio" name="question[21][beer][]" value="3"></td>
                                                        <td><input type="radio" name="question[21][spirits][]" value="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td><input type="radio" name="question[21][champagne][]" value="4"></td>
                                                        <td><input type="radio" name="question[21][wine][]" value="4"></td>  
                                                        <td><input type="radio" name="question[21][beer][]" value="4"></td>
                                                        <td><input type="radio" name="question[21][spirits][]" value="4"></td>
                                                    </tr>
                                                   
                                                </table>
                                               </div>
                                            </form>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </form>                   
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->                
            </div>
        </div>
    </div>
</div><!-- .container -->

<?php $this->load->view('include/footer'); ?>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.min.js') ?>"></script>
<!--<script type="text/javascript">
    $("#example-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true
    });
</script> -->
<script type="text/javascript">
// Tooltips Initialization
    $(document).ready(function () {
        
        $(document).on('click','.checkbox4',function () {
              var checkedVal = $(this).val();
            if (checkedVal == "5") {
                $('.checkbox4').prop("checked",false);
                $("#checkbox4none").prop("checked",true);
            }else {
                 $("#checkbox4none").prop("checked",false);
            }
        });
         $(document).on('click','.checkbox15',function () {
              var checkedVal = $(this).val();
            if (checkedVal == "6") {
                $('.checkbox15').prop("checked",false);
                $("#checkbox15none").prop("checked",true);
            }else {
                 $("#checkbox15none").prop("checked",false);
            }
        });
        $(document).on('click','.checkbox16',function () {
              var checkedVal = $(this).val();
            if (checkedVal == "10") {
                $('.checkbox16').prop("checked",false);
                $("#checkbox16none").prop("checked",true);
            }else {
                 $("#checkbox16none").prop("checked",false);
            }
        });
         $(document).on('click', '.question21', function (event) {
             console.log(this.value);
            if (this.value == '4') {
                 $('#questiontable21').hide();
            }else {
                $('#questiontable21').show();
            }
        });

        $(document).on('click', '.kituser', function (event) {
            if (this.value == 'self') {
                $.post('<?php echo base_url("user/getUserDetail"); ?>', {'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function (data) {
                    $('#name').val(data.result.firstname);
                    $('#namereview,#nameID').html(data.result.firstname);
                    $('#emailID').val(data.result.email);
                    $('#mobile').val(data.result.contact1);
                }, "json");
                
                $('#consentforid2').hide();
                 $('#consentforid1').show();
            } else if (this.value == 'other') {
                $('#name').val('');
                $('#emailID').val('');
                $('#mobile').val('');
                 $('#consentforid1').hide();
                 $('#consentforid2').show();
            }
        });
        
     
    $(document).on('click', '.gender', function (event) {
        $('#sexID,#sexreview').html(this.value);
        console.log(this.value);
    });

    $(document).on('click', '#checkBarcode', function (event) {
        var barcode = $('#barcode').val();
        if (barcode != "") {
            $.post("<?php echo base_url('user/checkValidBarcode'); ?>", {barcode: barcode, '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'}, function(data) {
                if (data) {
                    $('#barcodeerror').addClass('error').html('Valid barcode');
                    $('#barcodeBtnIcon').removeClass('fa fa-barcode').addClass('fa fa-check');
                }else{
                    $('#barcodeerror').addClass('error').html('Invalid barcode');
                    $('#barcodeBtnIcon').removeClass('fa fa-check').addClass('fa fa-barcode');
                }
            },"json");
        }else{
            $('#barcodeerror').addClass('error').html('Please enter kit barcode.');

        }
    });


        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "No space please and don't leave it empty");
        
//        $.validator.addMethod('atLeastOneSeat', function(value, element, params) {
//             var seats = $('radio.group-required').filter(function() {
//            return $(this).val() != '0';
//        });
//           return seats.length > 0;
//        }, 'Please select at least one seat');
//
//        $.validator.addClassRules('group-required', {atLeastOneSeat: true});

        var form = $("#kitActivationfrm");
        form.validate({
            errorPlacement: function (error, element) {
                if (element.attr("name") == "barcode") {
                    $('#barcode').val('');
                    $('#barcodeerror').html('');
                    error.appendTo('#barcodeerror');
                    return;
                } else if (element.attr("name") == "kituser") {
                    error.appendTo('#kitusererror');
                    return;
                } else if (element.attr("name") == "gender") {
                    error.appendTo('#gendererror');
                    return;
                } else if (element.attr("name") == "email") {
                    error.appendTo('#emailerror');
                    return;
                } else if (element.attr("name") == "mobile") {
                    error.appendTo('#mobileerror');
                    return;
                } else if (element.attr("name") == "dob") {
                    error.appendTo('#doberror');
                    return;
                } else if (element.attr("name") == "consent_for") {
                    error.appendTo('#consenterror');
                    return;
                } else if (element.attr("name") == "pincode") {
                    error.appendTo('#pincodeerror');
                    return;
                }else if (element.attr("name") == "question[1]") {
                    error.appendTo('#question_1_error');
                    return;
                }else if (element.attr("name") == "question[2]") {
                    error.appendTo('#question_2_error');
                    return;
                } else if (element.attr("name") == "question[3]") {
                    error.appendTo('#question_3_error');
                    return;
                }else if (element.attr("name") == "question[3a]") {
                    error.appendTo('#question_3a_error');
                    return;
                } else if (element.attr("name") == "question[4][]") {
                    error.appendTo('#question_4_error');
                    return;
                } else if (element.attr("name") == "question[5]") {
                    error.appendTo('#question_5_error');
                    return;
                } else if (element.attr("name") == "question[6]") {
                    error.appendTo('#question_6_error');
                    return;
                } else if (element.attr("name") == "question[7]") {
                    error.appendTo('#question_7_error');
                    return;
                }  else if (element.attr("name") == "question[8]") {
                    error.appendTo('#question_8_error');
                    return;
                } else if (element.attr("name") == "question[9]") {
                    error.appendTo('#question_9_error');
                    return;
                } else if (element.attr("name") == "question[10]") {
                    error.appendTo('#question_10_error');
                    return;
                } else if (element.attr("name") == "question[11]") {
                    error.appendTo('#question_11_error');
                    return;
                } else if (element.attr("name") == "question[12]") {
                    error.appendTo('#question_12_error');
                    return;
                } else if (element.attr("name") == "question[13]") {
                    error.appendTo('#question_13_error');
                    return;
                }else if (element.attr("name") == "question[13b]") {
                    error.appendTo('#question_13b_error');
                    return;
                }else if (element.attr("name") == "question[14]") {
                    error.appendTo('#question_14_error');
                    return;
                }else if (element.attr("name") == "question[15][]") {
                    error.appendTo('#question_15_error');
                    return;
                }else if (element.attr("name") == "question[16][]") {
                    error.appendTo('#question_16_error');
                    return;
                }else if (element.attr("name") == "question[17]") {
                    error.appendTo('#question_17_error');
                    return;
                }else if (element.attr("name") == "question[18]") {
                    error.appendTo('#question_18_error');
                    return;
                }else if (element.attr("name") == "question[19]") {
                    error.appendTo('#question_19_error');
                    return;
                }else if (element.attr("name") == "question[19][fruits][]" || element.attr("name") == "question[19][milk][]") {
                     $('#question_19_error').html('');
                     error.appendTo('#question_19_error');
                    return;
                } else if (element.attr("name") == "question[20][stress][]") {
                    $('#question_20_error').html('');
                    error.appendTo('#question_20_error');
                    return;
                } else if (element.attr("name") == "question[21][alcohol][]") {
                    $('#question_21_error').html('');
                    error.appendTo('#question_21_error');
                    return;
                }else {
                    error.insertAfter(element);
                }
            },
            rules: {
                name: {
                    required: true,
                    noSpace: true,
                    minlength: 2,
                    maxlength: 16
                },
               // barcode: "required",
                barcode: {
                    required: true,
                    remote: {
                        url: '<?php echo base_url('user/checkBarcodeAvailability'); ?>',
                        type: "post",
                        data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
                    }
                },
                mobile: "required",
                pincode: "required",
                consent_for: "required",
                dob: "required",
                 'question[1]': {
                    required: true
                  },
                  'question[2]': {
                    required: true
                  },
                   'question[3]': {
                    required: true
                  },
//                   'question[3a]': {
//                    required: true
//                  },
                   'question[4][]': {
                    required: true
                  },
                   'question[5]': {
                    required: true
                  },
                   'question[6]': {
                    required: true
                  },
                  'question[7]': {
                    required: true
                  },
                  'question[8]': {
                    required: true
                  },
                  'question[9]': {
                    required: true
                  },
                  'question[10]': {
                    required: true
                  },
                  'question[11]': {
                    required: true
                  },
                  'question[12]': {
                    required: true
                  },
                  'question[13]': {
                    required: true
                  },
                  'question[13b]': {
                    required: true
                  },
                  'question[14]': {
                    required: true
                  },
                  'question[15][]': {
                    required: true
                  },
                  'question[16][]': {
                    required: true
                  },
                  'question[17]': {
                    required: true
                  },
                  'question[18]': {
                    required: true
                  },
                  'question[19][fruits][]': {
                     require_from_group: [1, ".group-required"]
                  },
                  'question[20][stress][]': {
                    require_from_group: [1, ".group-required"]
                  },
                  'question[21][alcohol][]': {
                    require_from_group: [1, ".group-required"]
                  },
                  
                kituser: {
                    required: true
                },
                gender: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: "Please enter name",
                    maxlength: "max length 16 characters",
                    minlength: "Your first name must consist of at least 2 characters"
                },
                barcode: {
                    required: "Please enter kit barcode",
                    remote: "This barcode is Invalid",
                },
              //  barcode: "Please enter kit barcode",
                mobile: "Please enter your mobile number",
                pincode: "Please enter your pincode",
                kituser: "Please select use kit",
                dob: "Date of birth field is required",
                gender: "Please select gender",
                email: {
                    required: "Please enter your email address",
                    email: "Please enter valid email address",
                },
                'question[1]': {
                    required: "This question requires an answer",
                },
                'question[2]': {
                    required: "This question requires an answer",
                },
                 'question[3]': {
                    required: "This question requires an answer",
                },
                 'question[3a]': {
                    required: "This question requires an answer",
                },
                 'question[4]': {
                    required: "This question requires an answer",
                },
                 'question[5]': {
                    required: "This question requires an answer",
                },
                 'question[6]': {
                    required: "This question requires an answer",
                },
                 'question[7]': {
                    required: "This question requires an answer",
                },
                 'question[8]': {
                    required: "This question requires an answer",
                },
                 'question[9]': {
                    required: "This question requires an answer",
                },
                 'question[10]': {
                    required: "This question requires an answer",
                },
                 'question[11]': {
                    required: "This question requires an answer",
                },
                 'question[12]': {
                    required: "This question requires an answer",
                },
                 'question[13]': {
                    required: "This question requires an answer",
                },
                 'question[14]': {
                    required: "This question requires an answer",
                },
                 'question[15]': {
                    required: "This question requires an answer",
                },
                 'question[16]': {
                    required: "This question requires an answer",
                },
                 'question[17]': {
                    required: "This question requires an answer",
                },
                 'question[18]': {
                    required: "This question requires an answer",
                },
                 'question[19][fruits][]': {
                    required: "This question requires an answer",
                },
                 'question[20][stress][]': {
                    required: "This question requires an answer",
                },
                 'question[21][alcohol][]': {
                    required: "This question requires an answer",
                },
            }
        });

        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
             
                if(currentIndex == 0 || currentIndex == 1 || currentIndex == 2){
                    var data = $('#dob').val();
                    var arr = data.split('-');
                    var str = arr[2] + "-" + arr[1]+"-"+arr[0];
                    $('#barcodereview').html($('#barcode').val());    
                    $('#birthdateID,#dobreview').html(str);    
                   // $('#sexID,#sexreview').html($('.gender').val()); 
                    $('#nameID,#namereview').html($('#name').val());
                  
                }
                
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                //alert(1);
                $('#kitActivationfrm').submit()
            }
        });

    });
</script>   
