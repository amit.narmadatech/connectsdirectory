<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>
            invoice
        </title>
    </head>
    <body style="margin: 0; padding: 200px;">
        <div style="width:540px;">
            <table cellpadding="0" cellspacing="0" style="width: 540px; border-collapse: collapse; margin:0 auto; border: solid 2px #2398CE;">
                <thead>
                    <tr>
                        <th style="background: #EEE; text-align: center;">
                            <img src="http://jcgenetics.narmadasoftech.com/assets/images/logo.png" width="100px" />
                        </th>
                    </tr>
                    <tr>
                        <th style="width:100%; background: #EEE; color: #000; font-size: 16pt; text-align: center; font-family: helvetica; border-bottom: solid 2px #2398CE;">
                            Invoice
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="5" style="width: 100%; border-collapse: collapse;">
                                <tbody>
                                    <tr>
                                <td style="font-family: helvetica; font-size:8pt;"><?php echo!empty($orders->name) ? $orders->name : ''; ?></td>
                                 <td style="text-align: right; font-family: helvetica; font-size:8pt;"><strong>Date:<?php echo!empty($orders->order_date) ? date('d-m-Y', strtotime($orders->order_date)) : ''; ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="font-family: helvetica; font-size:8pt;">Billing Address :<br />
                                            <?php echo!empty($orders->address) ? $orders->address : ''; ?><br />
                                            City:<?php echo!empty($orders->city_name) ? $orders->city_name : ''; ?><br />
                                            State:<?php echo!empty($orders->state_name) ? $orders->state_name : ''; ?><br />

                                            Contact Number:<?php echo!empty($orders->mobile1) ? $orders->mobile1 : ''; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="font-family: helvetica; font-size:8pt;">E-mail:<?php echo!empty($orders->email) ? $orders->email : ''; ?>
                                        </td>
                                        <td style="text-align: right; font-family: helvetica; font-size:8pt;">
                                            <strong>Order NO
                                                :</strong><?php echo!empty($orders->order_number) ? $orders->order_number : ''; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;"><table cellpadding="5" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td style="font-family: helvetica; font-size:9pt; vertical-align: middle; border-top:solid 1px #2398CE;">
                                        Plan
                                    </td>
                                    <td style="font-family: helvetica; font-size:9pt; vertical-align: middle; border-top:solid 1px #2398CE;">
                                        Plan Name
                                    </td>
                                    <td style="font-family: helvetica; font-size:9pt; vertical-align: middle; border-top:solid 1px #2398CE;">
                                        Price
                                    </td>
                                    <td style="font-family: helvetica; font-size:9pt;vertical-align: middle; border-top:solid 1px #2398CE;">
                                        QTY
                                    </td>
                                    <td style="font-family: helvetica; font-size:9pt; vertical-align: middle; border-top:solid 1px #2398CE; text-align: right;">
                                        Total
                                    </td>
                                </tr><?php
								$amount =0 ;
                                if (!empty($orderDetails)) {
                                    foreach ($orderDetails as $orderDetailsval) {

                                        if (file_exists("uploads/plan_img/" . $orderDetailsval->plan_image) && !empty($orderDetailsval->plan_image)) {

                                            $img = base_url() . 'uploads/plan_img/' . $orderDetailsval->plan_image;
                                        } else {
                                            $img = base_url() . 'assets/images/diet-plan.jpg';
                                        }
									  $amount = ($amount+($orderDetailsval->quantity*$orderDetailsval->plan_price));	
									 	
                                        ?>
                                        <tr>
                                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #2398CE;">
                                                <img src="<?php echo $img;?>"/>
                                            </td>
                                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #2398CE; line-height:40px;">
                                                    <?php echo!empty($orderDetailsval->name) ? $orderDetailsval->name : ''; ?>
                                            </td>
                                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #2398CE; line-height:40px;">
                                                    <?php echo!empty($orderDetailsval->plan_price) ? $orderDetailsval->plan_price : ''; ?>
                                            </td>
                                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #2398CE; line-height:40px;">
                                                    <?php echo!empty($orderDetailsval->quantity) ? $orderDetailsval->quantity : ''; ?>
                                            </td>
                                            <td style="font-family: helvetica; font-size:8pt; vertical-align: middle; border-top:solid 1px #2398CE; text-align: right; line-height:40px;">
                                                    <?php echo!empty($amount) ? $amount : '0.0'; ?>
                                            </td>
                                        </tr><?php }
                                    } ?>
                            </table> 
                        </td>
                    </tr>
                </tbody>
                <tfoot>
				    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold; border-top:solid 2px #2398CE;  font-family: helvetica; font-size:8pt; line-height:15px;">
                             Sub-Total:  <?php echo!empty($orders->base_amount) ? $orders->base_amount : '0.00'; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>

                    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold; border-top:solid 2px #2398CE;  font-family: helvetica; font-size:8pt; line-height:15px;">
                             Discount:  <?php echo!empty($orders->discount) ? '- '.$orders->discount : '0.00'; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
				    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px;">
                            Amount : <?php echo $orders->base_amount - $orders->discount ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px;">
                            CGST : <?php echo!empty($orders->cgst) ? $orders->cgst : '0.00'; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px;">
                            SGST : <?php echo!empty($orders->sgst) ? $orders->sgst : '0.00'; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px;">
                            IGST : <?php echo!empty($orders->igst) ? $orders->igst : '0.00'; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="text-align: right; vertical-align: middle; font-weight: bold;  font-family: helvetica; font-size:8pt; line-height:15px;">
                            Grand Total:<?php echo!empty($orders->total_amount) ? $orders->total_amount : ''; ?>
                            &nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>