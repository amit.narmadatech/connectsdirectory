<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('include/header'); ?>
<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">
                    <div class="page-header">
                        <h1>Create Support Ticket </h1>
                    </div>
                    <form name="createticket" id="createticket" method="POST" action="<?php echo base_url('user/saveticket') ?>" enctype="multipart/form-data">
                        <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                        <input type="hidden" name="<?php echo $csrf['name']; ?>" value="<?php echo $csrf['hash']; ?>" />
                        <div class="row">         
                            <div class="col-md-6 col-sm-6">   
                                <div class="form-group"> 
                                    <label>Subject</label>
                                    <input type="text" name="subject" class="form-control">
                                    <span style="color:red"><?php echo form_error('subject'); ?></span>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">   
                                <div class="form-group"> 
                                    <label>Priority</label>
                                    <select  name="priority" class="form-control">
                                        <option value="">Select Priority</option>
                                        <option value="1">Low</option>
                                        <option value="2">Medium</option>
                                        <option value="3">High</option>
                                    </select>
                                    <span style="color:red"><?php echo form_error('priority'); ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">         
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group"> 
                                    <label>Department</label>
                                    <select  class="form-control" name="department">
                                        <option value="">Select Department</option>
                                        <option value="Sales">Sales</option>
                                        <option value="Product Plan">Product Plan</option>
                                        <option value="Logistics">Logistics</option>
                                        <option value="Report Query">Report Query</option>
                                    </select>
                                    <span style="color:red"><?php echo form_error('department'); ?></span>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group"> 
                                    <label>Message</label>
                                    <textarea name="message" class="form-control"></textarea> 
                                    <span style="color:red"><?php echo form_error('message'); ?></span>
                                </div>
                            </div>
                        </div>               
                        <div class="form-group">
                            <label>Attachment</label> 
                            <div class="row">
                                <div class="col-sm-6 input_fields_wrap" id="">
                                    <input type="file" name="filename" class="">
                                </div>    
                              <!--  <div class="col-sm-2"><a class="flat-button border add_field_button" id="add_more">Add <i class="material-icons">add</i></a> </div>-->
                            </div>
                        </div> 

                        <div class="form-group"> 
                            <button type="submit" class="flat-button border">Submit <i class="material-icons">send</i></button>
                        </div>
                    </form>       
                </div>      
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>

<?php $this->load->view('include/footer'); ?>

<script>
    $(document).ready(function () {
        var max_fields = 10;
        var wrapper = $(".input_fields_wrap");
        var add_button = $(".add_field_button");
        var x = 1;
        $(add_button).click(function (e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div><input type="file" name="filename[]" class="form-control"><a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });
</script>