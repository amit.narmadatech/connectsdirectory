<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('include/header'); ?>

<link href="<?php echo base_url(); ?>assets/css/jquery.steps.css" rel="stylesheet">

<section class="content">
    <div class="container inner-container">
        <div class="row">
            <div class="col-sm-4 col-md-3">
                <!-- USER MENU -->
                <?php $this->load->view('include/user_menu'); ?>
                <!-- END USER MENU -->
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="user-content">	
                    <div class="page-header">
                        <h1>Activated Kit</h1>
                    </div>
                    <?php $oldimg = (isset($user) && (!empty($user->image))) ? $user->image : ''; ?>
                    <div class=" clearfix">
                                <div class="table-responsive">
                       <?php if (!empty($kitlist)) {  ?>      
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Barcode </th> 
                                            <th>Name</th>
                                            <th>Date of birth</th>
                                            <th>Gender</th>
                                            <th>Plan Name </th>                                       
                                            <th>Date</th>
                                            <th>Report</th>
                                        </tr>
                                    </thead>
                 
                          <?php   foreach ($kitlist as $kitlists) { 
                                if(!empty($kitlists->report_file) && file_exists("uploads/report_files/".$kitlists->report_file))
                                    {
                                        $rurl = base_url().'uploads/report_files/'.$kitlists->report_file;
                                    }else{
                                        $rurl = '';
                                    } 
                                ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo !empty($kitlists->barcode) ? $kitlists->barcode : ''; ?></td>
                                            <td><?php echo !empty($kitlists->name) ? $kitlists->name : ''; ?></td>
                                             <td><?php echo !empty($kitlists->dob) ? date('d-m-Y', strtotime($kitlists->dob)) : ''; ?></td>
                                            <td><?php echo !empty($kitlists->gender) ? $kitlists->gender : ''; ?></td>
                                            <td><?php echo !empty($kitlists->plan_name) ? $kitlists->plan_name : ''; ?></td>
                                            <td><?php echo !empty($kitlists->created_at) ? date('F d, Y h:i:s A', strtotime($kitlists->created_at)) : ''; ?></td>
                                            <td>
                                                <?php if (!empty($rurl)) { ?>
                                                <a href="<?php echo $rurl; ?>" download>Download Report</a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                <?php } ?>
                         
                                </table>
                                    <?php    } else { ?>
                                        <div class="alert alert-danger text-center">
                                             No record available.
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        </div>
                        <?php } ?> 
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->

<?php $this->load->view('include/footer'); ?>
<script src="<?= base_url('assets/js/additional-methods.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.steps.min.js') ?>"></script>
