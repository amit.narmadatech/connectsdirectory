<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="form-page">
 <div class="container">
	<?php $this->load->view('include/alert'); ?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 ">
				      <div class="panel  panel-default">
                <div class="panel-heading"> <h3 class="my-0 font-weight-normal text-center">Register</h3></div>
                <div class="panel-body">
                  
			 	 <form method="post" id="signupForm" action="<?= base_url('register/create_user'); ?>">
					
					<div class="form-group">
            <label for="firstname">First Name *</label>
                   		<input type="text" class="form-control" id="firstname" name="firstname"  value="<?= set_value('firstname') ?>">
                  	 	
                  	 	<?php echo form_error('firstname', '<div class="text-danger">', '</div>'); ?>
					</div>
					<span id="fnameerror"></span>

					<div class="form-group">
            <label for="lastname">Last Name *</label>
                   		<input type="text" class="form-control" id="lastname" name="lastname"  value="<?= set_value('lastname') ?>">
                  	 	
                  	 	<?php echo form_error('lastname', '<div class="text-danger">', '</div>'); ?>
					</div>
					<span id="lnameerror"></span>
				
                <div class="form-group">
                  <label for="email">Email *</label>
                  	 	<input type="email" class="form-control" id="email" name="email"  value="<?= set_value('email') ?>">
                  	 	
						<?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>                   
              		</div>
              		<span id="emailerror"></span>               
		
					 <div class="form-group">		
            <label for="password">Password *</label>		
						<input type="password" class="form-control" id="password" name="password">
					
						<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
					</div>
					<span id="passworderror"></span>

					 <div class="form-group">    
            <label for="password_confirm">Confirm password *</label>
						<input type="password" class="form-control" id="password_confirm" name="password_confirm">
						
						<?php echo form_error('password_confirm', '<div class="text-danger">', '</div>'); ?>
					</div>
					<span id="confirmpassworderror"></span>

					<div class="form-group">

            <button type="submit" class="flat-button border">Register <i class="material-icons">send</i></button>

					<!-- 	<input type="submit" class="flat-button border" value="Register"> -->
					
                    <span class="font-small pull-right grey-text">Alreday have an account? <a href="<?= base_url('login'); ?>" class="blue-text ml-1"> Sign in</a></span>
                </div>

				</form>
			</div>
		</div>
 	 </div>
	</div>
 </div>
</section>
<?php  $this->load->view('include/footer')?> 
<script>
    var validator =   $("#signupForm").validate({
    	errorPlacement: function(error, element) {
          if(element.attr("name") == "firstname"){
              error.appendTo('#fnameerror');
              return;
          }else if(element.attr("name") == "lastname"){
              error.appendTo('#lnameerror');
              return;
          }else if(element.attr("name") == "email"){
              error.appendTo('#emailerror');
              return;
          }else if(element.attr("name") == "password"){
              error.appendTo('#passworderror');
              return;
          }else if(element.attr("name") == "password_confirm"){
              error.appendTo('#confirmpassworderror');
              return;
          }else {
            error.insertAfter(element);
          }
          },
        rules: {
          firstname: "required",
          lastname: "required",
          email:{
            required: true,
            email: true 
          },
          password : {
          			required: true,
                    minlength : 8
                },
          password_confirm : {
          			required: true,
                    minlength : 8,
                    equalTo : "#password"
                }

        },
        submitHandler: function(e){
          this.submit();
        },
        messages: {
           firstname: "Please enter your first name",
           lastname: "Please enter your last name",
           password: {
				required :"Please enter your password",
             	minlength:  "The Password field must be at least 8 characters in length",	           	
           },
           password_confirm:{
				required :"Please enter your password",
             	minlength:  "The Password field must be at least 8 characters in length",	           	
             	equalTo:  "The Confirm Password field does not match the Password field",	           	
           },
           email: { 
            required :"Please enter your email address",
             email:  "Please enter valid email address",
           }
        }
    });
</script>