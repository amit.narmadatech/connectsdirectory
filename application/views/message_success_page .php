<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('include/header')?> 

<style type="text/css">
	#error-page { padding: 120px 0; text-align: center; }
	#error-page h1 { color: #E44157; font-size: 50px; margin-bottom: 35px; line-height: 1 } 
	#error-page h3 { margin-bottom: 25px; line-height: 1 }
</style>
<section id="error-page"> 
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">	
				<div class="notify successbox">			  
			 		<span class="alerticon"><img src="http://s22.postimg.org/i5iji9hv1/check.png" alt="checkmark" /></span>			  
					<h1>Success!</h1> 				
					<?php if($this->session->flashdata('success')) { ?>
				<div class="alert alert-success text-center">
				  <?php echo $this->session->flashdata('success'); ?>
					 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    </div>
			     <?php } ?>
				</div>
				<a href="<?php echo base_url('home'); ?>" class="flat-button ">Back To Home <i class="fa fa-home"> </i></a>	
			</div>
		</div>
	</div>
</section>
	<?php  $this->load->view('include/footer')?> 
</body>
</html>