<?php  $this->load->view('include/header')?> 

<section id="join_us_section">
	<div class="banneroverlay"></div>
     <div class="container">
      <div class="row">
        <div class="col-md-12 area_1">
            <h2 class="const_title">  Sports Fitness </h2>    
        </div>
        </div>
    </div>
</section>

<section class="policy">
    <div class="container">
	   <div class="row">
		    <div class="col-lg-12">
				<h2 class="heading">GET FITTER FASTER STRONGER!</h2>
				<h3 class="sub-heading">Genes play a part in who we are and how we perform</h3>
				<h4 class="mini-heading">Genes play a part in who we are and how we perform</h4>
				<p>“Sport skill acquisition does not happen without both specific genes and a specific environment, and often the genes and the environment must coincide at a specific time.”</p>
			</div>
		</div>
		<h3 class="sub-heading">OUR PRODUCTS AND THEIR BENEFITS</h3>
		<h4>Assessing parameters</h4>
		<div class="row">
			<div class="col-sm-4">
				<h4>SPORTS & FITNESS </h4>
				<ul class="list-unstyled">
					<li>Aerobic Capacity</li>
					<li>Body Fat Loss Response</li>
					<li>Endurance</li>
					<li>Exercise Response to HDL</li>
					<li>Cholesterol</li>
					<li>Exercise Responsiveness</li>
					<li>Injury Risk</li>
					<li>Muscle Performance</li>
					<li>Power</li>
					<li>Recovery from Exercise</li>
					<li>Propensity to Exercise</li>
					<li>Anaerobic Threshold</li>
					<li>Muscle Cramping</li>					
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>NUTRITION</h4>
				<ul class="list-unstyled">
					<li>Adiponectin Metabolism</li>
					<li>Alchohol Response</li>
					<li>Antioxidant Need</li>
					<li>Caffeine Sensitivity</li>
					<li>Carbohydrate Sensitivity</li>
					<li>Circardian Rhythm</li>
					<li>Detoxification</li>
					<li>Diet Type Omega-3 and 6</li>
					<li>DNA Oestrogen</li>
					<li>Fat Absorption and Metabolism (Saturated
					Fat Sensitivity)</li>
					<li>Inflammation </li>
					<li>Insulin Sensitivity</li>
				</ul>
			</div>
			<div class="col-sm-4">
			  <ul class="list-unstyled">
				<li>Intelligence IQ</li>
				<li>Iron Overload</li>
				<li>Lactose Intolerance</li>
				<li>Learning Ability (Reading)</li>
				<li>Regulation of Metabolism and</li>
				<li>Feeding Behaviour</li>
				<li>Response to Blood Pressure</li>
				<li>Salt Sensitivity</li>
				<li>Vitamin A</li>
				<li>Vitamin C</li>
				<li>Vitamin D (Bone Health)</li>
				<li>Vitmain B</li>
				<li>Vitamin</li>
				<li>Proteins</li>
			</ul>
			</div>
		</div>
		<h3>Benefits</h3>
		<div class="row">
			<div class="col-sm-6">
				<h5>SPORTS & FITNESS</h5>
				<p>Improve Performance by Reducing Injuries Optimise Your Training Time Determine Exercises Best Suited for Your Body Type
				Enhance Functional Fitness Improve Cognition and Boost Your Mood</p>
			</div>
			<div class="col-sm-6">
				<h5>NUTRITION</h5>
				<p>Design a Nutrition Plan Best Suited to Optimise Your Metabolism Achieve Optional Body Mass Index Understand What Micronutrients Your Body Needs Boost Energy Levels, Focus and Immunity</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4>Get your genetic assessment today</h4>
				<ul class="list-inline assist-list">
					<li> <span class="circule-blue">Fitness </span> </li>
					<li> +</li>
					<li> <span class="circule-blue">Nutrition</span> </li>
					<li> +</li>
					<li> <span class="circule-blue">Lifestyle </span></li>
					<li> = </li>
					<li><span class="circule-red">Your Present and Future Performance </span></li>
				</ul>
			</div>
		</div>

		<h4>Simple and Hassle Free, Non-Invasive Saliva Test</h4>

		<div class="row">
			<div class="col-sm-4">
				Collect saliva before brushing in the morning
			</div>
			<div class="col-sm-4">
				Spit directly into the funnel until the black line on the tube
			</div>
			<div class="col-sm-4">
				Twist the yellow gel tube and add the gel into the saliva tube
			</div>
			<div class="col-sm-4">
				Unscrew the funnel and close the tube with the cap
			</div>
			<div class="col-sm-4">
				Complete the lifestyle questionnaire
			</div>
			<div class="col-sm-4">
			Sign the declaration form and call +91 731 2441000 to schedule a free reverse pick up
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h4>About JCGenetics</h4>
				<p>JCGenetics is a pioneer, and a leading personal genomics company focussed on enabling personalized preventive healthcare. We are dedicated to empowering physicians, wellness professionals and customers with the most validated, accurate and actionable genomic information to positively impact and improve their health and quality of life.</p>
			</div>
		</div>
	</div>
</section>

<?php  $this->load->view('include/footer')?> 