<?php $this->load->view('include/header') ?> 
<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 area_1">
                <h1 class="const_title"> Blogs</h1>    
            </div>
        </div>
    </div>
</section>

<section id="blog-list-page">
    <div class="container">
        <div class="row">  
            <div class="col-sm-9">   
              <div class="row">  
                <?php
                    if (!empty($blog)) {
                    foreach ($blog as $blogs) {
                    if (!empty($blogs->blog_img) && file_exists("uploads/blog/" . $blogs->blog_img)) {
                       $img = base_url() . 'uploads/blog/' . $blogs->blog_img;
                    } else {
                        $img = base_url() . 'assets/images/img_not_available.jpg';
                    }
                ?>
                <div class="col-sm-6 col-md-6">
                    <div class="post entry">
                        <h2 class="entry-title"><a href="<?php echo base_url('home/blogdetails/'.$blogs->slug); ?>"><?php echo! empty($blogs->title) ? substr($blogs->title, 0,55). '..' : '' ?></a></h2>
                        <div class="blog-img">
                            <a href="<?php echo base_url('home/blogdetails/'.$blogs->slug); ?>">
                                <img src="<?php echo $img; ?>" class="img-responsive" alt="<?php echo!empty($blogs->title) ? $blogs->title : '' ?>">
                            </a>
                        </div>
                        <div class="entry-meta"> 
                            <span class="date"><?php echo!empty($blogs->created_date) ? date("D, d M Y", strtotime($blogs->created_date)) : '' ?></span>
                            <span class="author"><a href="#">Admin</a></span>
                        </div>
                    </div>
                </div>
                    <?php }
                }
                ?>  
                </div>
                <nav aria-label="Page navigation">
                <?php echo $links; ?>
                </nav>
          
                </div>
                <div class="col-sm-3">
                    <div class="bolg-sidebar">
                    <h4 class="title"><span>Search</span></h4>
                    <form name="blogserachfrm" id="blogserachfrm" method="GET" action="<?php echo base_url('blogs'); ?>">
                        <div class="input-group">
                            <input type="text" name="keyword" value="<?php echo!empty($this->input->get('keyword')) ? $this->input->get('keyword') : ''; ?>" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-warning" type="submit"><i class="material-icons">search</i></button>
                            </span>
                        </div>
                    </form>             
                    </div>
                    <div class="bolg-sidebar">
                        <h4 class="title"><span>Recent Post</span></h4>
                        <ul>
                            <?php
                                if ($recentblog) {
                                foreach ($recentblog as $recentblogs) {
                            ?>
                           <li><a href="<?php echo base_url('home/blogdetails/'.$recentblogs->slug); ?>"><?php echo!empty($recentblogs->title) ? $recentblogs->title : '' ?></a> </li>
                             <?php }
                              }
                           ?>
                        </ul>
                    </div>
                    <div class="bolg-sidebar">
                        <h4 class="title"><span>Archives</span></h4>
                        <ul>
                         <?php
                            if (!empty($archive)) { 
                                foreach ($archive as $archives) {
                          ?>
                            <li><a href="<?php echo base_url('home/blog') . '?archives=' . date('d-m-Y', strtotime($archives->created_date)); ?>"><?php echo!empty($archives->Month) ? $archives->Month . ' ' . $archives->Year : ''; ?></a> </li>
                            <?php }
                          } ?>
                        </ul>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('include/footer') ?> 