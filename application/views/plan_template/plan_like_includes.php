  <?php if(!empty($allplan)){
        foreach ($allplan as $allplans){
            
        if (file_exists("uploads/plan_img/".$allplans->plan_image) && !empty($allplans->plan_image)){

           $planimg = base_url().'uploads/plan_img/'.$allplans->plan_image;
         }else{
           $planimg = base_url().'assets/images/img_not_available.jpg';
         }
        ?>
        <div class="col-sm-4 col-md-3">
            <?php if ($allplans->is_discount) { ?>
                <div class="ribbon"><span><?php echo round($allplans->discount_prnct); ?>% off</span></div>	
            <?php } ?>
            <div class="thumbnail">
                 <?php if (($allplans->is_discount)) { ?>   
                    <a href="javascript:void(0);">
                  <?php }elseif($allplans->direct_cart){?>
                        <a href="javascript:void(0);">       
                 <?php }else{ ?>
                      <a href="<?php echo base_url("home/plandetail/$allplans->slug"); ?>">
                  <?php } ?>
                       <img src="<?php echo $planimg; ?>" alt="<?php echo !empty($allplans->name) ? $allplans->name : ''; ?>">
                      </a>
                <div class="caption">
                    <h4>
                         <?php if (($allplans->is_discount)) { ?>   
                           <a href="javascript:void(0);">
                         <?php }elseif($allplans->direct_cart){?>
                               <a href="javascript:void(0);">       
                        <?php }else{ ?>
                            <a href="<?php echo base_url("home/plandetail/$allplans->slug"); ?>">
                         <?php } ?>
                              <?php echo !empty($allplans->name) ? $allplans->name : ''; ?>
                          </a>
                    </h4>  
                    <p><?php echo!empty($allplans->sort_description) ? $allplans->sort_description : ''; ?></p> 
                    <p class="plan-price">
                        <strong><i class="fa fa-inr"></i> <?php echo!empty($allplans->price) ? $allplans->price : ''; ?> </strong> 
                          <?php if ($allplans->is_discount) { ?>
                            <span class="oldprice"><strong><i class="fa fa-inr"></i> <?php echo round($allplans->clubbed_amt); ?></strong></span>
                        <?php } ?>
                         </p>
                      
                        <?php if (($allplans->is_discount) || ($allplans->direct_cart)) { ?>
                                <form name="addtocart" method="POST" action="<?php echo base_url('cart/addtocart'); ?>">
                                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                                    <input type="hidden" name="planid" value="<?php echo!empty($allplans->id) ? $allplans->id : ''; ?>">
                                    <button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
                                </form>
                            <?php } else { ?>
                                 <a href="<?php echo base_url('plandetail/' . $allplans->slug); ?>" class="flat-btn" role="button">Buy Now</a>
                            <?php } ?>
                </div>
            </div>
        </div>
<?php }} ?>
