<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		 <div class="row">
 			<div class="col-md-12 area_1">
 				<h1 class="const_title"> Plan Detail</h1>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	
 		<div class="row">
 		  <div class="col-md-6 col-sm-6">
			  <form name="addtocart" method="POST" action="<?php echo base_url('cart/addtocart');?>">
                              <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
			  <input type="hidden" name="planid" value="<?php echo !empty($plandetails->id)? $plandetails->id : '';?>" readonly>
			   <h2><?php echo !empty($plandetails->name)? $plandetails->name : '';?></h2>	 		 	
				   <p><?php echo !empty($plandetails->description)? $plandetails->description : '';?></p>     		
				<div class="plan-pricing">
				<div class="row">
				  <div class="col-sm-7">
					<h6><?php echo !empty($plandetails->name)? $plandetails->name : '';?></h6>        
				 </div>
				  <div class="col-sm-5 text-right">
					<p class="t2_style editContent">Price: <strong><i class="fa fa-inr"></i>  <?php echo !empty($plandetails->price)? $plandetails->price : '';?></strong></p>
				  </div>
				</div>    	
				<p> Requires saliva sample for DNA analysis</p> 	
					<button type="submit" class="flat-button border">Add to cart <i class="material-icons">&#xE854;</i></button>
				</div>
			</form>	
		  </div>
	   <div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-success">
			 <img src="<?php echo base_url('assets/'); ?>images/health-service1.png" class="item-icon" alt="images">
                         <div class="panel-heading" role="tab" id="headingOne">
			    <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			           Type-2 Diabetes Mellitus
			          <p> Get a disease free life by preventing Type-2 Diabetes.</p>
			        </a>
			    </h4>
			   </div>
			   <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Lifestyle changes</li>
			        <li>Body weight management </li>
			        <li>Increased ability to use the insulin produced in the body</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  <img src="<?php echo base_url('assets/'); ?>images/health-service2.png" class="item-icon" alt="images">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			            Obesity
			           <p>Know your genes and take charge of your health today.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <ul>
			        	<li><strong>Results include:</strong> </li>
			        	<li>Improved eating and sleeping habits </li>
			       		<li>Physical exercises guidance</li>
			       		<li>Reduced BMI level </li>			       
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  <img src="<?php echo base_url('assets/'); ?>images/health-service3.png" class="item-icon" alt="images">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			        	Heart Diseases
			          <p>Test your genes to reduce your heart health risk. </p>			             
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			        <ul>
			          <li><strong>Results include:</strong> </li>
			          <li>Physical activity guidance </li>
			          <li>Healthy diet</li>
			          <li>Reduced blood homocysteine </li>	        
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-purple">
			  <img src="<?php echo base_url('assets/'); ?>images/health-service4.png" class="item-icon" alt="images">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
			              Hypertension
			           <p>Reduce your chances of hypertension today.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Maintained blood pressure </li>
			        <li> Reduced heart rate variability</li>
			        <li> Health and Diet improvement </li>			       
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 
   </div>
</section>

 <section class="plans-content">
 	<div class="container">
 	 <div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 				<h4>Know your health risks</h4>
 				<p>Most of the diseases are linked with your genes and it is better to be proactive for prevention of these diseases. We allow you to uncover the health risk factors ahead of time so that the preventive actions can be taken timely to reduce maximum risks. Reap health benefits with genetic testing. We will help you program your diet to reduce the risks to disease.</p>
 				<p>Majority of the diseases can be prevented in their initial phase, especially diseases like stroke, heart problems, hypertension, diabetes, obesity. The genetic test helps in understanding your risky gene variants. </p>
 				<p>Proactive changes in lifestyle and adequate health monitoring can reduce the health risks. The understanding of genetic profile helps you in adopting best preventive health practices that can reduce health risks and improve quality of life.  </p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			<img src="<?php echo base_url('assets/'); ?>images/horoscope-banner.png" class="img-responsive center-block" alt="images">
 		</div>
 	 </div>
 	</div>
  </div>
</section>

<section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">Genetic profile results will look like.</h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 			<h5>Average Risk</h5>
			<p> There are no exceptional readiness to the diseases. You are at baseline risk according to your gene variants.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 			<h5>Moderate Risk</h5>
			<p>You are prone to diseases as compared to the general population. It Is necessary to make changes in lifestyle choices and dietary measures. Consult your physician today. </p>
		</div>
		</div>
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 
 				<h5>High Risk</h5>
				<p>You have a high genetic risk for diseases. You need to consult specialist doctors such as cardiologist and they will guide you for necessary precautions and strategies to reduce health risks.	</p>
			</div>
		</div> 		
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block" alt="images">
 			</div>
 		</div>
	</div>
</section>
 	
 <section class="reletae-product bg-gray">
 	<div class="container">
 	 <h3 class="text-center">You may also like… </h3>
 	   <div class="row">
                   <?php $this->load->view('plan_template/plan_like_includes');?> 
 		   <?php /*if(!empty($allplan)){
				foreach ($allplan as $allplans){
					
				if (file_exists("uploads/plan_img/".$allplans->plan_image) && !empty($allplans->plan_image)){
				
				   $planimg = base_url().'uploads/plan_img/'.$allplans->plan_image;
				 }else{
				   $planimg = base_url().'assets/images/img_not_available.jpg';
				 }
				?>
				<div class="col-sm-4 col-md-3">
					<div class="thumbnail">
					  <img src="<?php echo $planimg;?>" alt="plan">
					<div class="caption">
						<h4><?php echo !empty($allplans->name)? $allplans->name : '';?></h4>  
						<p><?php echo !empty($allplans->sort_description)? $allplans->sort_description : '';?></p> 
						<p class="plan-price"><strong><i class="fa fa-inr"></i> <?php echo !empty($allplans->price)? $allplans->price : '';?> </strong> <a href="<?php  echo base_url('plandetail/'.$allplans->slug); ?>" class="flat-btn pull-right" role="button">Buy Now</a></p>   
					 </div>
					</div>
				</div>
		    <?php }} */?>
 		</div>
 </div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon" alt="images">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text">Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>
                        <div class="quote-text editContent pix_text">
                           Amit Roy
                       </div>         
                    <div class="quote-dots">. . .</div>       
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                        <div class="quote-text editContent pix_text">
                           Ritesh Jain
                      </div>         
                      <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                       
                        <div class="quote-text editContent pix_text">
                           Asha Verma
                      </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div> 
 			 </div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
	/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});

$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});
</script>


