<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		 <div class="row">
 			<div class="col-md-12 area_1">
 				<h1 class="const_title"> Plan Detail</h1>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
				  <form method="POST"  name="addtocart" action="<?php echo base_url('cart/addtocart');?>">
                                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
				  <input type="hidden" name="planid" value="<?php echo !empty($plandetails->id)? $plandetails->id : '';?>" readonly>
				   <h2><?php echo !empty($plandetails->name)? $plandetails->name : '';?></h2>	 		 	
					   <p><?php echo !empty($plandetails->description)? $plandetails->description : '';?></p>     		
					<div class="plan-pricing">
					<div class="row">
					  <div class="col-sm-7">
						<h6><?php echo !empty($plandetails->name)? $plandetails->name : '';?></h6>
					  </div>
					  <div class="col-sm-5 text-right">
						<p class="t2_style editContent">Price: <strong><i class="fa fa-inr"></i> <?php echo !empty($plandetails->price)? $plandetails->price : '';?></strong></p>
					  </div>
					</div>    	
					<p> Requires saliva sample for DNA analysis</p> 	
						<button type="submit" class="flat-button border">Add to cart <i class="material-icons">&#xE854;</i></button>
					</div>
				</form>	
 	        </div>

	<div class="col-md-6 col-sm-6">
	   	<div class="paln-right">
	   	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/allergy-service1.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			           Allergies
			          <p> Know the reasons causing allergy to you</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Dust Mites Allergy </li>
			        <li>Pets Allergy </li>
			        <li>Egg Allergy </li>
			        <li>Peanut Allergy </li>
			        <li>Pollen Allergy</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/allergy-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Intolerance and Sensitivity
			          <p>Get insights about your body intolerances and sensitivities</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Histamine intolerance </li>
			        <li>Lactose intolerance </li>
			        <li>Gluten sensitivity </li>
			        <li>Blood pressure sensitivity</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/allergy-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           Sickness
			           <p>Test your genes to know likelihood of sickness</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li> Motion sickness </li>
			        <li> Misophonia </li>			        
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
   </div>
</section>

 <section class="plans-content">
 	<div class="container">
 		<div class="bg-genedite">
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 				<h4>Know your Allergies and Sensitivities</h4>
 				<p>We use your saliva sample to learn about your body. These genes are an essential part of the body immune system, but certain forms of them could cause the body to have allergic reactions to harmless substances. Gene Allergy analyses specific genes with specific characteristics associated with allergies, intolerances and sensitivities. </p> 				

 				<p>The best way to avoid the allergy symptoms is to get clear about the causes. Most people with allergies don’t realize that eating a healthy diet, including certain specific foods, can help them manage their symptoms. We can help you understand these causes and offer you relief from most of the allergies and sensitivities.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			<img src="<?php echo base_url('assets/'); ?>images/allergy-banner.png" class="img-responsive center-block">
 		</div>
 	  </div>
 	</div>
 </div>
</section>

 <section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">Watch out for allergy reactions and take appropriate precautionary steps</h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Gluten sensitivity </h5>
				<p>Know about your sensitivity to gluten to understand about your regular issues of bloating and diarrhea.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Blood pressure sensitivity with salt intake</h5>
				<p>	Some people have tendency to increased blood pressure due to high salt consumption but blood pressure reduces on low salt diet. Know the optimized consumption for you.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 				 <h5>Lactose intolerance </h5>
				<p>Know your milk and milk products digestion capability for better body and health.</p>
			</div>
 			
		</div>
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block" alt="images">
 			</div>
 		</div>
	</div>
</section>
 	

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	<div class="row">
         
            <?php $this->load->view('plan_template/plan_like_includes');?> 
            <?php /*if(!empty($allplan)){
                          foreach ($allplan as $allplans){

                          if (file_exists("uploads/plan_img/".$allplans->plan_image) && !empty($allplans->plan_image)){

                             $planimg = base_url().'uploads/plan_img/'.$allplans->plan_image;
                           }else{
                             $planimg = base_url().'assets/images/img_not_available.jpg';
                           }
                          ?>
                          <div class="col-sm-4 col-md-3">
                                  <div class="thumbnail">
                                    <img src="<?php echo $planimg;?>" alt="plan">
                                  <div class="caption">
                                          <h4><?php echo !empty($allplans->name)? $allplans->name : '';?></h4>  
                                          <p><?php echo !empty($allplans->sort_description)? $allplans->sort_description : '';?></p> 
                                          <p class="plan-price"><strong><i class="fa fa-inr"></i> <?php echo !empty($allplans->price)? $allplans->price : '';?> </strong> <a href="<?php  echo base_url('plandetail/'.$allplans->slug); ?>" class="flat-btn pull-right" role="button">Buy Now</a></p>   
                                   </div>
                                  </div>
                          </div>
                  <?php }} */?>
 	</div>
 </div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon" alt="images">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text"> Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                        <div class="quote-text editContent pix_text">
                           Amit Roy
                       </div>         
                       <div class="quote-dots">. . .</div>        
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                    <div class="quote-text editContent pix_text">
                        Ritesh Jain
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                       
                    <div class="quote-text editContent pix_text">
                        Asha Verma
                    </div>         
                    <div class="quote-dots">. . .</div>  
   				</div> 
 			</div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */
equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}
$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});
$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});
</script>


