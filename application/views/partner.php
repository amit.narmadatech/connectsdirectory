<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
	<div class="banneroverlay"></div>
    <div class="container">
     <div class="row">
        <div class="col-md-12 area_1">
            <h1 class="const_title"> Partner with us</h1>    
        </div>
       </div>
    </div>
</section>
<section class="partner-page">
    <div class="container">
	   <div class="row">
		  <div class="col-lg-12">			
			<p>We believe that the genetic information is utilized while considering all other information about the individual such as dietary, medical, environmental, lifestyle, quality of life and many more.  JC Genetics partners with professionals in medical, diet and nutrition, skin and hair care and other domains to personalize their practices through genomics. </p> <br> 
			<p>We invite you to be a part of our worldwide partner network powering personalized and precision healthcare and wellness practices for a happier and a healthier world.</p>
			<br> 
			<ul>
			<li>You need not to worry about processes. We will train you for all the powerful genetic analysis capabilities developed by us. We offer brand name, labeling, custom report design and related support to our partners for best genetic requirements.	</li>
			<li>Don’t burden yourself with building an infrastructure, but leverage our work process, data analysis engine and overall expertise to deliver the personalized services to your clients. We compliment your genetic information practices in best possible manner.</li>
			<li>Guide your clients with clarity. We provide extensive training on the science, technology, genetic testing process, work flow, reading health reports and all other aspects. Handle all customer queries and offer best consultation to your clients.</li>
			<li>Every customer is concerned with their data privacy, especially related to health and genetics. We follow strict privacy and security policies and allow customers to delete their data anytime by requesting the same.</li>
			</ul>			
		    </div>
	    </div>  
        <div class="row">
            <div class="flat-appointment">
                <h3 class="text-center">Become A Partner</h3>
                <form id="partnerfrm" name="partnerfrm" class="appointment-form contact-form for-full-width" method="POST">
                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash());?>
                <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                    <div class="input-wrap wrap-left fullname">
                        <input type="text"  value="" tabindex="1" placeholder=" Name *" name="firstname">
                    </div>
                    <div class="input-wrap wrap-right fullname">
                        <input type="text" value="" tabindex="2" placeholder="Surname *" name="lastname">
                    </div>
                    <div class="input-wrap wrap-left email">
                        <input type="text" value="" tabindex="3" placeholder="Email *" name="email">
                    </div>
                    <div class="input-wrap wrap-right telephone">
                        <input type="text" value="" tabindex="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="10" placeholder="Phone *" name="telephone">
                    </div>
                    <div class="input-wrap wrap-left">
                        <select id="stateID" name="state">
                            <option value="">Select state</option>
                               <?php if(!empty($state)){
                                foreach ($state as $states){ ?>
                                     <option value="<?php echo $states->state_id; ?>" <?php if(!empty($userdetails->state_id) && ($userdetails->state_id==$states->state_id)) { ?> selected <?php } ?>><?php echo $states->state_title; ?></option>
                               <?php } }?>
                        </select>
                    </div>
                     <div class="input-wrap wrap-right">
                        <select id="district_id" name="district" tabindex="5">
                            <option value="">Select District</option>
                        </select>
                    </div>
                    <div class="input-wrap wrap-left subject">
                        <input type="text" value="" tabindex="6" placeholder="Company Name (If Any)" name="subject">
                    </div>
                    <div class="textarea-wrap wrap message">
                        <textarea class="" tabindex="7" placeholder="Your Message" name="message" id="message" required=""></textarea>
                    </div>									
                    <div class="send-wrap">
                        <button class="flat-button border">Submit <i class="material-icons">send</i></button>
                    </div>
                        <div class="center-on-small-only">
                            <div class="alert alert-success text-center" id="partnerNotitfy" style="display:none" >
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                        </div>
                </form><!-- /.appointment-form -->
            </div>
        </div>
    </div>
</section>
<?php  $this->load->view('include/footer')?> 
<script>
    
    jQuery.validator.addMethod("phoneno", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 && 
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    }, "<br />Please specify a valid phone number");
    $('#partnerfrm').validate({
	rules: {
		firstname: {
		  required: true,
		},
		lastname: {
			required: true,
		},
                email: {
			required: true,
		},
                state: {
			required: true,
		},
                district: {
			required: true,
		},
                telephone: {
			required: true,
                        phoneno:true,
                        minlength: 10,
                        digits: true
		}
	 },
	submitHandler: function(form){
            $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>home/savepartners',
                data: $("#partnerfrm").serialize(),
                success: function (respose) {
                    console.log(respose);
                    var obj = JSON.parse(respose);
                    if (obj.status) {
                        $('#partnerfrm')[0].reset();
                        $('#partnerNotitfy').html(obj.msg).show().delay(10000).fadeOut();
                    } else {
                        $('#partnerNotitfy').html(obj.msg).show().delay(10000).fadeOut();
                    }
                }
            });
	},
	messages:{
            firstname:{
                required: "Please enter first name",
            },
            lastname: {
                required: "Please enter last name",
            },
            email:{
                required: "Please enter email",
                email: true	
            },
             state:{
                required: "Please select state",
            },
             district:{
                required: "Please select district",
            },
            telephone:{
                required: "Please enter mobile number",
            },
            message:{
                required: "Please enter message",
            }
	},
    });

 $(document).ready(function(){
     
    $( "#stateID" ).change(function() {
        $.post("<?php echo base_url('home/getdistrict')?>",{ state_id : this.value, '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},function(response){
        if(response.status){
          $('#district_id').html('');		
          $('#district_id').html(response.result);		
        }else{
          console.log(response.status);
          console.log(response.result);
        }
        },"json");			
    }); 
 });


</script>