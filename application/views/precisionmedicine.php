<?php $this->load->view('include/header')?> 
<?php $userData = $this->session->all_userdata();?>
 
<section id="join_us_section">
 	<div class="banneroverlay"></div>
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12 area_1">
 				<h2 class="const_title"> Plan Detail</h2>
        	</div>
  	 	</div>
 	</div>
</section>

<section class="plans">
 	<div class="container">
	 <form name="addtocart">
 		<div class="row">
 		 <div class="col-md-6 col-sm-6">
	 	  <h2>Precision Medicine</h2>	
 	 	  <p>Do you know even your drug therapy can be customized? Your DNA holds the key to understand the way of processing certain medications. Drugs for diabetes, cardiology, gastrointestinal problems, and immunosuppressant can be tailored according to your genes. </p>     		
 	 	<div class="plan-pricing">
 	 	<div class="row">
 	 	  <div class="col-sm-8">
			<h6>Precision Medicine DNA kit   </h6>        
		  	  Requires saliva sample for DNA analysis
		  </div>
		  <div class="col-sm-4 text-right">
			<p class="t2_style editContent">Price: <strong>&#x20B9; 25,000</strong></p>
		  </div>
		</div>    	
			<button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
 	 	</div>
 	 </div>

	   <div class="col-md-6 col-sm-6">
	     <div class="paln-right">
	    	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-success">
			  	<img src="<?php echo base_url('assets/'); ?>images/medicine-service1.png" class="item-icon">			    
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			             Personalized drug recommendations
			          <p>We read your DNA and to provide right medications for you.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body">
			       <ul>
			        <li><strong>Results include:</strong> </li>
			        <li>Right dose </li>
			        <li>Right medications </li>
			        <li>DNA based prescriptions</li>
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-info">
			  	<img src="<?php echo base_url('assets/'); ?>images/medicine-service2.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			         	 Personalized Prescription			         
			          <p> Gene knowledge for your prescription.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body">
			        <ul>
			         <li><strong>Results include:</strong> </li>
			         <li>Genetic profile </li>
			         <li>Compatibility of prescribed drugs with your genes </li>
			         <li>Best suitable medications </li>			        
			        </ul>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-warning">
			  	<img src="<?php echo base_url('assets/'); ?>images/medicine-service3.png" class="item-icon">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			            Right Drugs, Right Dose
			           <p>Get guidance and suggestions from our team of genetic counsellors.</p>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body">
			       <ul>
			         <li><strong>Results include:</strong> </li>
			         <li>Help your physician to prescribe right drugs </li>
			         <li>Medication suggestions </li>
			         <li>Counselling support </li>			       
			        </ul>
			      </div>
			    </div>
			  </div>
			</div>
		 </div>
		</div>
 	  </div>
 	</form>
   </div>
</section>

<section class="plans-content">
 	<div class="container">
 		<div class="bg-genedite">
 		 <div class="row"> 			
 			<div class="col-md-6 col-sm-6 banner-content">
 				<h4>Individual genetic influences.</h4>
 				<p>Everyone responds differently to certain medications that affects the recovery time. The genetic makeup influences the response of your body to drug therapy. Get your precision medicine to reap the benefits of personalized medicine.</p>
 				<h4>Avoid side effects of medicines</h4>
 				<p>Most of the people are not benefitted from prescription medicines and have many side effects. Take precision medicine that enables physicians to prescribe right medicine at the right dose for you.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 			<img src="<?php echo base_url('assets/'); ?>images/medicine-banner.png" class="img-responsive center-block">
 		</div>
 	 </div>
 	</div>
 </div>
</section>

<section class="plans-content middle-content bg-gray">
 	<div class="container">
 		<h3 class="text-center">Personalize the drug therapy for right person at the right dose and right time. </h3> 		
 		<div class="row"> 			
 			<div class="col-md-6 col-sm-6"> 				
 				<h5>Benzodiazepines </h5>
				<p>Anxiety, insomnia, and muscle spasms are most common problems today. Benzodiazepines is commonly used to treat these diseases. It is mainly metabolized by CYP2C19, and many individuals have reduced CYP2C19 enzyme activity in significant manner, that leads to less or no results of the drug.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Morphine</h5>
				<p>	It is a very common drug for pain relief. The clearance and efficacy of Morphine are largely influenced by genes like OPRM1, COMT, SLC22A1*4. Genetic testing can help in determining the dosage and reduce the side effects of drug.</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6"> 		
 				<h5>Antihypertensives </h5>
				<p>	The variances of gene like CYP3A5 have direct effect on Verapamil (antihypertensive) clearance. CYP3A5 expressors show a greater oral clearance of antihypertensive as compared to non-expressors. It is an important consideration whole prescribing drugs.</p>
			</div>
 			<div class="col-md-6 col-sm-6"> 
 				<h5>Metformin</h5>
				<p>Metformin is the most commonly prescribed drug for type 2 diabetes. It is found that the patients having two copies of required variant show a two-fold drop in the glycated hemoglobin level, which is desirable for antidiabetic drugs.	</p>
			</div>
		</div>
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Antiplatelets </h5>
				<p>	The inclusion of genotyping treatment using clopidogrel or Plavix enhances the response level of patient. It is found that the patients having CYP2C19 gene are at 3 times greater risk of adverse cardiovascular incidences like stroke, heart attack and even death.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Anticoagulants</h5>
			  <p>Hemorrhage is a leading cause of death in many countries. The patients carrying genetic variants like VKORC1 or CYP2C9, it is recommended to decrease the drug dose. The patients can achieve stable INR by testing their genes and reduce adverse events.  </p>
			</div>
		</div>
 		<div class="collapse" id="collapseExample">
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 			 <h5>Beta blockers</h5>
			 <p>Beta Blockers are used for conditions like heart failure, angina, and hypertension. The genetic variation like CYP2D6 affects its results as over 70% of metabolism happens through the same pathway.</p>
			</div>
 			<div class="col-md-6 col-sm-6">
 			 <h5>Statins</h5>
			 <p>Statins lowers the ill effects of cholesterol on body but many people experience myopathy induced by statins. Gene understanding will help you choose best drug therapy without side effects.</p>
			</div>
		</div> 		
 		<div class="row">
 			<div class="col-md-6 col-sm-6">
 				<h5>Immunosuppressants</h5>
				<p>	Genes like CYP3A4*22, CYP3A5*3 strongly influences immunosuppressants like Cyclosporine and Tacrolimus. The incidences of organ toxicity and rejection can be reduced by conducting genetic test before administering immunosuppressants. </p>
			</div> 			
		 </div>
 		</div>
 		<div class="readmore-link">
			<a class="flat-button"  role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">	 More <i class="material-icons">chevron_right</i>
			</a>
		</div>
	</div>
</section>

 <section class="plans-team">
 	<div class="container">
 		<div class="row">
 			<div class="col-md-6 col-sm-6 team-content">
 				<h4>Our team is glad to support you </h4>
 				<p>JC Genetics is trained to help you with your health and lifestyle choices. Make informed decisions and change your life to better. You are unique and so is your genes. Test your genes and get a personalized road map to your better health.</p>
 			</div>
 			<div class="col-md-6 col-sm-6">
 				<img src="<?php echo base_url('assets/'); ?>images/doctor-smiling.jpg" class="img-responsive center-block">
 			</div>
 		</div>
	</div>
</section> 	

 <section class="reletae-product bg-gray">
 	<div class="container">
 		<h3 class="text-center">You may also like… </h3>
 	  <div class="row">
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing5.jpg" alt="plan">
      		<div class="caption">
        		<h4>Ancestry DNA </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing1.jpg" alt="plan">
      		<div class="caption">
        		<h4>Skin Genetics </h4>  
        		<p>Holistic approach to radiant and youthful skin  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>  
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing3.jpg" alt="plan">
      		<div class="caption">
        		<h4>Health Horoscope </h4>  
        		<p>Know your future health  </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		 </div>
 		 <div class="col-sm-3 col-md-3">
    		<div class="thumbnail">
    		  <img src="<?php echo base_url('assets/images/pricing/') ?>pricing2.jpg" alt="plan">
      		<div class="caption">
        		<h4>Gene Diet </h4>  
        		<p> Explore your origin </p> 
        		<p class="plan-price"><strong>&#x20B9; 25000 </strong> <a href="#" class="flat-btn pull-right" role="button">Buy Now</a></p>   
     		 </div>
   		    </div>
 		</div>
 	</div>
 </div>
</section>

<section id="testimonials"> 
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
         <img src="<?=base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
        <!--Carousel Wrapper-->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
 			 <div class="carousel-inner" role="listbox">
    			<div class="item active">
    				 <p class="quote-title editContent pix_text">
                        Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>
                        <div class="quote-text editContent pix_text">
                           Amit Roy
                       </div>         
                       <div class="quote-dots">. . .</div>       
    			</div>
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text"> Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                         <div class="quote-text editContent pix_text">
                           Ritesh Jain
                      </div>         
                       <div class="quote-dots">. . .</div>  
   				 </div>  
   				 <div class="item">     
    	  			<p class="quote-title editContent pix_text"> JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>                       
                    <div class="quote-text editContent pix_text">
                        Asha Verma
                     </div>         
                    <div class="quote-dots">. . .</div>  
   				</div> 
 			</div>  
		</div>
      </div>
    </div>
  </div>
</section>

<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
	/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.reletae-product .thumbnail');
});


$(window).resize(function(){
  equalheight('.reletae-product .thumbnail');
});

</script>