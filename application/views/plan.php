<?php  $this->load->view('include/header')?> 

<?php $userData = $this->session->all_userdata();?> 
<section id="join_us_section">
    <div class="banneroverlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 area_1">
                <h1 class="const_title"> Plans</h1>           
            </div>       
        </div>
    </div>
</section> 

 <section class="plans bg-gray">
 	<div class="container">
		<div class="row plan-box ">
			<?php  
 	if (!empty($planList)) { 	
 		foreach ($planList as $plan) {  		
 		?>
           <div class="col-md-4 col-sm-4">
           	<div class="plan-list">
           		<?php if ($plan->is_discount) { ?>
           			<div class="ribbon"><span><?php echo round($plan->discount_prnct); ?>% off</span></div>	
           		<?php } ?>
				<?php
				if (file_exists("uploads/plan_img/".$plan->plan_image) && !empty($plan->plan_image)){
				
				   $img = base_url().'uploads/plan_img/'.$plan->plan_image;
				 }else{
				   $img = base_url().'assets/images/img_not_available.jpg';
				 }
				?> 
				<div class="plan list-img ">
                                 <?php if (($plan->is_discount)) { ?>   
                                    <a href="javascript:void(0);">
                                  <?php }elseif($plan->direct_cart){?>
                                        <a href="javascript:void(0);">       
                                 <?php }else{ ?>
                                      <a href="<?php echo base_url("home/plandetail/$plan->slug"); ?>">
                                  <?php } ?>
                                    <img src="<?php echo $img; ?>" class="img-fluid" alt="<?php echo (!empty($plan->name))?$plan->name:''; ?>" >
                                </a>
				</div>
				<div class="plan-content">
				<h2>
                                    <?php if (($plan->is_discount)) { ?>   
                                      <a href="javascript:void(0);">
                                    <?php }elseif($plan->direct_cart){?>
                                        <a href="javascript:void(0);">
                                    <?php }else{ ?>
                                       <a href="<?php echo base_url("home/plandetail/$plan->slug"); ?>">
                                    <?php } ?>
                                       <?php echo (!empty($plan->name))?$plan->name:''; ?>
                                    </a>
                                </h2>
				<h5><?php echo !empty($plan->sort_description)? $plan->sort_description : '';?></h5>
				
				<p><?php echo !empty(strip_tags($plan->description)) ? substr(strip_tags($plan->description), 0, 80) . '...' : ''; ?> </p>

				<p>Price: <span class="new-price "><strong> <i class="fa fa-inr"></i> <?php echo !empty($plan->price)? round($plan->price) : '';?> /-</strong> </span>
					<?php if ($plan->is_discount) { ?>
					<span class="oldprice"><strong><i class="fa fa-inr"></i> <?php echo round($plan->clubbed_amt); ?></strong></span>
					<?php } ?>
						</p>
						<?php if (($plan->is_discount) || ($plan->direct_cart)) { ?>
                                                <form name="addtocart" method="POST" action="<?php echo base_url('cart/addtocart'); ?>">
                                                    <?php $csrf = array('name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()); ?>
                                                    <input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>" />
                                                    <input type="hidden" name="planid" value="<?php echo!empty($plan->id) ? $plan->id : ''; ?>">
                                                    <button type="submit" class="flat-button border">Add to cart <i class="material-icons">add_shopping_cart</i></button>
                                                </form>
						<?php }else{ ?>
						<a href="<?php echo base_url("home/plandetail/$plan->slug"); ?>" class="flat-button border">Buy Now <i class="material-icons">&#xE417;</i></a>
						<?php } ?>
			</div>
		  </div>
		</div>

<?php } 
 	}	
 ?> 
</div>
</div>
</section>  

 <section id="testimonials"> 
     <div class="container">
         <div class="row">
             <div class="col-sm-12">
                 <img src="<?= base_url('/assets/images/') ?>light-normal.png" class="q-icon">  
                 <!--Carousel Wrapper-->
                 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner" role="listbox">
                         <div class="item active">
                             <p class="quote-title editContent pix_text"> Amazing experience with JC Genetics. I have used the nutrition and skin plan. I have received the test results and must say that the report covers a lot of information and provide clear recommendations according to genetics. Thanks to the team. I am leading a way too healthy life now. Happy to Recommend.</p>                       
                             <div class="quote-text editContent pix_text">
                                 Amit Roy
                             </div>         
                             <div class="quote-dots">. . .</div>        
                         </div>
                         <div class="item">     
                             <p class="quote-title editContent pix_text">Excellent service. The test is very useful and provided great insights. I have never considered intolerances and sensitivities of daily food items. Got to know from my DNA test report that I need to modify milk and caffeine consumption. Already seeing the differences. Highly Recommended.</p>                       
                             <div class="quote-text editContent pix_text">
                                 Ritesh Jain
                             </div>         
                             <div class="quote-dots">. . .</div>  
                         </div>  
                         <div class="item">     
                             <p class="quote-title editContent pix_text">JC Genetics have changed my mindset of healthcare and helped me with better lifestyle, skincare, preventive treatment and much more. I have seen drastic change in my food choices after the report analysis and counselling session. Thanks to JC Genetics Team.</p>
                             <div class="quote-text editContent pix_text">
                                 Asha Verma
                             </div>         
                             <div class="quote-dots">. . .</div>  
                         </div> 
                     </div>  
                 </div>
             </div>
         </div>
     </div>
 </section>
<!-- Modal -->
            
<?php  $this->load->view('include/footer')?> 


<script type="text/javascript">
	/* It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){
var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.plan-box .plan-list');
});

$(window).resize(function(){
  equalheight('.plan-box .plan-list');
});
</script>