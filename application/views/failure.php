<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php  $this->load->view('include/header')?> 
<section class="sucesspage">
  <div class="container">
    <div class="row">         
      <div class="col-md-8 col-md-offset-2">
        <div class="success-content text-center">
          <div class="text-center sucess-header">
        		<i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>
        			<h4>Transaction Failed </h4>
        	</div>        		 
        		<p>	Sorry, your payment failed.plase try agian   </p>
      	</div>            
      </div>      
    </div>
  </div>
</section>
<?php  $this->load->view('include/footer')?> 