<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php  $this->load->view('include/header')?> 


<section id="join_us_section">
  <div class="banneroverlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 area_1">
        <h1 class="const_title"> How It Works</h1>
      </div>
    </div>
  </div>
</section>
<section id="how-care">
  <div class="container"> 
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 how-work"> 
        <div class="title-section desc">
          <h2 class="title" data-text="W">How it  <span>works</span></h2>
          <div class="care-head">
            <p>Testing your DNA is very simple. No Pricks. No Blood. Just a small sample of your saliva.
            <br> 
            Don’t wait and start discovering more about yourself.</p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">1</div>
               <p> Order Product Your Kit typically arrives within 3 to 5 days</p>
               <img src="<?php echo base_url();?>assets/images/hw-i1.png">
            </div>
          </div>
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">2</div>
                <p>Download Mobile App and Scan/Enter Bar Code to Activate the kit </p>
                 <img src="<?php echo base_url();?>assets/images/hw-i2.png">
            </div>
          </div>
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">3</div>
               <p>Follow Kit Instructions to Spit in the Given Tube</p>
                <img src="<?php echo base_url();?>assets/images/hw-i3.png">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">4</div>
                <p>Request pickup/mail it back to our authorized center</p>
                <img src="<?php echo base_url();?>assets/images/hw-i4.png">
            </div>
          </div>
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">5</div>
                <p>Get Reports and Discover About Yourself</p>
                <img src="<?php echo base_url();?>assets/images/hw-i5.png">
            </div>
          </div>
          <div class="col-sm-4 text-center work-process">
            <div class="work-box">
              <div class="no">6</div>
               <p>Get personalized Plan and Lead A Healthy Life</p>
               <img src="<?php echo base_url();?>assets/images/hw-i6.png">
            </div>
          </div>
        </div>
        

      <!--   <div class="row">
             
            <div class="col-sm-4 text-center work-process">
              <img src="<?php echo base_url('assets/images/'); ?>how-workv1.png"> 
          </div>
          <div class="col-sm-4 text-center work-process">                    
             <img src="<?php echo  base_url('assets/images/'); ?>how-workv2.png"> 
          </div>
          <div class="col-sm-4 text-center work-process">                    
              <img src="<?php echo  base_url('assets/images/'); ?>how-workv3.png"> 
          </div>
        </div>         
        <div class="row">
          <div class="col-sm-4 text-center work-process">                    
            <img src="<?php echo  base_url('assets/images/'); ?>how-workv4.png"> 
          </div> 
          <div class="col-sm-4 text-center">                    
            <img src="<?php echo  base_url('assets/images/'); ?>how-workv5.png"> 
          </div>
          <div class="col-sm-4 text-center">                    
            <img src="<?php echo  base_url('assets/images/'); ?>how-workv6.png"> 
          </div>            
        </div> -->            
      </div>
    </div>
  </div>
</section>
<section id="getin-kit">
  <div class="container mt-5">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-sm-12 What-getkit">
        <div class="title-section desc">
            <h2 class="title" data-text="K">What you get <span>in the kit</span></h2>
        </div>
          
          <div>
              <img src="<?= base_url('assets/images/'); ?>get_kit1.png">
          </div>
       <!--   <ul class="get-kit">
          <li><img src="<?= base_url('assets/images/'); ?>get_kit1.png">
             <!-- <p>Customer application form</p>
          </li>
        
          <li><img src="<?= base_url('assets/images/'); ?>get-kit2.png">
                <p>Tube for saliva sample collection </p> 
          </li>
          <li><img src="<?= base_url('assets/images/'); ?>get-kit3.png">
                <p>Bag for storing saliva tube</p>
          </li>
          <li><img src="<?= base_url('assets/images/'); ?>get-kit4.png">
                <p> Step-by-step instruction form</p>
          </li>
          <li><img src="<?= base_url('assets/images/'); ?>get-kit5.png">
                 <p>Packaging box with Bar code</p>
          </li>
         
        </ul>  -->
      </div>       
    </div>
  </div>
</section>
<!--
<section id="genetic-report">
  <div class="container">  
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 report-content report-section">
        <h3> Genetic Reports </h3>
        <p>All the collected saliva samples are processed in state of the art laboratory and meet the genotyping regulations. Our dedicated team of medical experts use world class illumine technology to analyze the DNA and generate reports. Personalized reports are based on the extensive scientific research and supported with well-tested system and technology. </p>
      </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
        <img src="<?= base_url('assets/images/'); ?>geneticreport.png" class="img-responsive pull-right">
      </div>           
    </div>
  </div>
</section>
<section id="privacys-practices">
  <div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
        <h3> Privacy Practices</h3>
        <p>You decide your privacy level with us and you are in complete control to decide the use of your personal genetic information. We will not share your individual data without your explicit consent.</p>
        <ul class="why-us">
         <li>We will not share your information or genetic reports with health insurance companies or other employers. </li>
         <li>We do not provide your information to law enforcement unless we need to comply with any sort of court orders. </li>
         <li>We maintain all the required security measures to prevent unauthorized access of your data and ensure appropriate use of your information.</li>
         <li>Unauthorized connection to our database is restricted by external firewalls.</li>
         <li>We use SSL (Secure Sockets Layer) technology to encrypt all connections of our website.</li>
         <li>We protect your identity by decoupling your genetic information from your personal information.</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <img src="<?= base_url('assets/images/'); ?>practices.png" class="img-responsive">
    </div>    
  </div>
</section>
<section id="promise">
  <div class="container">
    <div class="row">       
      <div class="col-md-6 col-sm-6 col-xs-12">
        <h3> We promise you</h3>
        <h6>Technology and Scientific Excellence</h6>
          <p>We use the latest science and technology to provide reliable and quality results. Your saliva samples are processed in state of the art laboratory for sequencing and genotyping tools. Our team of highly qualified and experienced technicians, scientists and medical experts ensures the processing of samples with best quality control practices.</p>
          <h6>Data Privacy</h6>
          <p>We protect your personal information and maintain high standards of privacy. Your standard of bioethics and information safety is well-maintained by us. Moreover, you can delete your data anytime and control it at all times. </p>
          <h6>Customer Support</h6>
          <p>Our dedicated team of genetic counselors are always ready to help you make informed decisions. You can contact customer care team to schedule an appointment with our genetic counselors and receive guidance to understand your personalized genetic report.</p>          
        </div>
         <div class="col-md-6 col-sm-6 col-xs-12">
          <img src="<?= base_url('assets/images/'); ?>promise.png" class="img-responsive pull-right">
      </div>     
    </div>
  </div>
</section>
-->

<?php  $this->load->view('include/footer')?> 