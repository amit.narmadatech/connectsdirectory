<?php  $this->load->view('include/header')?> 
<section id="join_us_section">
	<div class="banneroverlay"></div>
     <div class="container">
      <div class="row">
        <div class="col-md-12 area_1">
            <h2 class="const_title">  Gluten intolerance </h2>    
        </div>
      </div>
    </div>
</section>

<section class="policy">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h4>MORE THAN 10% OF INDIANS ARE GLUTEN INTOLERANT</h4>
			<span class="pull-right">- Nizam’s Institute of medical sciences</span>
			<p>Gluten intolerance is an adverse reaction to gluten present in wheat, barley and rye. Have you ever skipped wheat for a few days and found yourself feeling lighter? Maybe less bloated and more energetic? Well, chances are that you are sensitive to gluten.</p>	</div>
		</div>
		<h4>EFFECTS OF GLUTEN INSENSITIVITY</h4>
		<div class="row">				
			<div class="col-sm-3">
				<div class="bg-info four-box">
					Nutritional deficiency
				</div>
			</div>			
			<div class="col-sm-3">
			 <div class="bg-primary four-box">
				Gastrointestinal disorders
			 </div>
			</div>
			<div class="col-sm-3"> 
				<div class="bg-warning four-box">
				Headaches
		  	 </div>
			</div>
			<div class="col-sm-3">
				<div class="bg-success four-box">
				 Fatigue 
				</div>
			</div>
		</div>
		<h4>WHY YOU SHOULD TAKE THE GLUTEN GENE TEST?</h4>
		<div class="row">
			<div class="col-sm-4">
				<h3>Your genetic test analysis</h3>
			</div>
			<div class="col-sm-8">				
				<ul>
					<li> Check for gluten sensitivity </li>
					<li> Optimize your Nutrition </li>
					<li> Prevent onset of symptoms </li>
				</ul>
			</div>
		</div>
	<h4>TAKE THE JCGENETICS GLUTEN SENSITIVITY TEST TODAY TO AVOID SYMPTOMS.</h4>
	</div>
</section>
<?php  $this->load->view('include/footer')?> 