<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Notification_model class.
 * 
 * @extends CI_Model
 */
class Notification_model extends CI_Model {

     public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function add($notificationData) {

        return $this->db->insert('notification', $notificationData);
    }

    /**
     * create_user function.
     * 
     * @access public
     * @param array $notificationData
     * @return bool true on success, false on failure
     */
    public function get($user_id) {

        return $notificationData = $this->db->query("select *,DATE_FORMAT(created_at, '%d-%m-%Y %h:%i') AS formatted_date from notification where id not in(select notification_id from notification_read_status where user_id=$user_id) and type = 'admin_broadcast' and recipient_id = 0 order by created_at desc")->result();
        //print_r($notificationData);die;
    }

    /**
     * create_user function.
     * 
     * @access public
     * @param array $notificationData
     * @return bool true on success, false on failure
     */
    public function get_customer_notification($user_id) {

        return $notificationData = $this->db->query("select * from notification where id NOT IN (select notification_id from notification_read_status where user_id=$user_id) AND type ='customer_notification' AND recipient_id = $user_id order by created_at desc")->result();
        //print_r($notificationData);die;
    }

    /**
     * create_user function.
     * 
     * @access public
     * @param array $notificationData
     * @return bool true on success, false on failure
     */
    public function customer_notification_list($user_id) {

        return $notificationData = $this->db->where('recipient_id', $user_id)->where('type', 'customer_notification')->get('notification')->result();
        //echo $this->db->last_query();die;
    }

    /**
     * create_user function.
     * 
     * @access public
     * @param array $notificationData
     * @return bool true on success, false on failure
     */
    public function admin_notification_list() {

        $result = $this->db
                        ->select('notification.*')
                        ->from('notification')
                        ->where('notification.sender_id', 0)
                        ->where('notification.recipient_id', 0)
                        ->order_by('notification.created_at', 'desc')
                        ->get()->result();

        return $result;
    }

    /**
     * create_user function.
     * 
     * @access public
     * @param array $notificationData
     * @return bool true on success, false on failure
     */
    public function set_read($notification_id, $user_id) {

        $chk_exist = $this->db->where('notification_id', $notification_id)->where('user_id', $user_id)->get('notification_read_status')->num_rows();
        if ($chk_exist == 0) {

            $insertData = array('notification_id' => $notification_id, 'user_id' => $user_id);

            $notificationData = $this->db->where('id', $notification_id)->get('notification')->row();

            if ($this->db->insert('notification_read_status', $insertData)) {
                return $notificationData;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

}
