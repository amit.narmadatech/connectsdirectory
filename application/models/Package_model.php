<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Package_model class.
 * 
 * @extends CI_Model
 */
class Package_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
    private $table = 'packages';
    private $primary_id = 'id';

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}

	/**
	 * get package list function.
	 * 
	 * @access public
	 * @param array $where condition
	 * @return object list
	 */
	public function get_list($where) {
		
		return $this->db->from($this->table)
					->join('package_product','package_product.package_id = packages.id',"left")
					->join('package_incentive','package_incentive.package_id = packages.id',"left")
					->join('package_expense','package_expense.package_id = packages.id',"left")
					->join('package_commission','package_commission.package_id = packages.id',"left")
					->join('tax','tax.package_id = packages.id','left')
					->select('packages.*,packages.id as pid,packages.status as package_status,package_product.*,package_incentive.*,package_expense.*,package_commission.*,tax.*')
					->where($where)->group_by('packages.id')->order_by('packages.title')->get()->result();
	}

	/**
	 * get single package function.
	 * 
	 * @access public
	 * @param $id
	 * @return object list
	 */
	public function find($id) {
		
		return $this->db->from($this->table)
					->join('tax','tax.package_id = packages.id','left')
					->select('packages.*,packages.id as pid,packages.status as package_status,tax.*')
					->where('packages.id',$id)->get()->row();
	}

	public function get_package_product($id) {
		
		return $this->db->from('package_product')
					->join('products','products.id = packages.product_id')
					->select('package_product.*,products.name')
					->where('package_product.package_id',$id)->get()->result();
	}

	public function get_package_expense($id) {
		
		return $this->db->from('package_expense')
					->where('package_expense.package_id',$id)->get()->result();
	}

	public function get_package_incentive($id) {
		
		return $this->db->from('package_incentive')
					->where('package_incentive.package_id',$id)->get()->result();
	}

	public function get_package_commission($id) {
		
		return $this->db->from('package_commission')
					->where('package_commission.package_id',$id)->get()->result();
	}
	
	/**
	 * insert package function.
	 * 
	 * @access public
	 * @param array $packageData
	 * @return bool true on success, false on failure
	 */
	public function insert($packageData) {
		
		return $this->db->insert($this->table, $packageData);
	}


	/**
	 * update package function.
	 * 
	 * @access public
	 * @param array $packageData
	 * @return bool true on success, false on failure
	 */
	public function update($edit_id,$packageData) {

		$this->db->where($this->primary_id, $edit_id);
		return $this->db->update($this->table, $packageData);
	}
	
}
