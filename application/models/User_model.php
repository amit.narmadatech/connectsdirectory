<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class User_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($email, $password, $firstname, $lastname) {
		
		$data = array(
			'firstname'      => $firstname,
			'lastname'      => $lastname,
			'email'      => $email,
			'password'   => md5($password),
			'type'      => 4,
		);
		
		$this->db->insert('user', $data);
		return $this->db->insert_id();
	}
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($username, $password) {
		
		$this->db->select('password');
		$this->db->from('user');
		$this->db->where('email', $username);
		$this->db->where('password', md5($password));
		$this->db->where('type', 4);
		$this->db->where('status', 1);
		$hash = $this->db->get()->row();
		
		return (count($hash) > 0)?true:false;
		//return $this->verify_password_hash($password, $hash);
		
	}
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($username) {
		
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('email', $username);

		return $this->db->get()->row('id');
		
	}
	
	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) {
		
		$this->db->from('user');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
		
	}

	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}

	public function update_user($user_id, $userData) {
		unset($userData['email']);
		$this->db->where('id', $user_id);
		return $this->db->update('user', $userData);
		
	}

	public function checkUser($data = array()) {

		$this->db->select('id');
		$this->db->from('user');
		$this->db->where(array('email'=>$data['email']));
		$query = $this->db->get();
		$check = $query->num_rows();
		
		if($check > 0){
			$result = $query->row_array();
			$data['updated_at'] = date("Y-m-d H:i:s");
			$update = $this->db->update('user',$data,array('id'=>$result['id']));
			$userID = $result['id'];
		}else{
			$data['created_at'] = date("Y-m-d H:i:s");
			$data['updated_at'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert('user',$data);
			$userID = $this->db->insert_id();

			/* START - sending notification */
			$this->load->model('notification_model');

			/* sending notification to user */
			$this->notification_model->add(array(
				'sender_id' => 0,
				'type' => 'customer_notification',
				'label' => 'customer_created',
				'title' => 'WELCOME YOU HAVE SUCCESSFULLY REGISTERED',
				'body' => "Welcome ".$data['firstname']." to JC-GENOMICS.",
				'href' => '',
				'recipient_id' => $userID,
			));

			/* sending notification to admin */
			$this->notification_model->add(array(
				'sender_id' => 0,
				'type' => 'admin_broadcast',
				'label' => 'customer_created',
				'title' => 'NEW CUSTOMER REGISTERED',
				'body' => "A new customer has registered with email id - ".$data['email'],
				'href' => '',
				'recipient_id' => 0,
			));

			/* END - sending notification */
		}

		$this->db->from('user');
		$this->db->where('id', $userID);
		return $this->db->get()->row();

		//return $userID?$userID:false;
    }
	
}
