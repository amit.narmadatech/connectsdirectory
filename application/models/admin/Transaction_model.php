<?php

class Transaction_model extends CI_Model {

    public $table_name = "orders";
    public $primary_key = 'id';

    public function getall_transaction() {

        $this->db->from('orders')
                ->join('user', 'user.id = orders.user_id')
                ->select('orders.*,Concat(user.firstname," ",user.lastname) as fullname');
        $query = $this->db->get();
        return $query->result();
    }

    public function counts($filter_trans_date = false) {
        //EDIT THIS
        $this->db->from('orders')
                ->join('user', 'user.id = orders.user_id')
                ->select('orders.*,Concat(user.firstname," ",user.lastname) as fullname');
        if ($filter_trans_date) {
            $date_filter = explode("-", $filter_trans_date);
            $this->db->where('orders.order_date BETWEEN "' . date('Y-m-d h:m:s', strtotime($date_filter[0])) . '" AND "' . date('Y-m-d h:m:s', strtotime($date_filter[1])) . '"');
        }
        $query = $this->db->get();
        return count($query->result());
    }

    public function get_records($page = 0, $perpage = 10, $filter_trans_date = false) {
        $page = $page - 1;
        if ($page < 0) {
            $page = 0;
        }
        $from = $page * $perpage;
        $this->db->select('orders.*,Concat(user.firstname," ",user.lastname) as fullname');
        $this->db->join('user', 'user.id = orders.user_id');
        if ($filter_trans_date) {
            $date_filter = explode("-", $filter_trans_date);
            $this->db->where('orders.order_date BETWEEN "' . date('Y-m-d h:m:s', strtotime($date_filter[0])) . '" AND "' . date('Y-m-d h:m:s', strtotime($date_filter[1])) . '"');
        }
        $this->db->limit($perpage, $from);
      //  $this->db->group_by('user_id');
        $this->db->order_by('orders.id', "DESC");
        $query = $this->db->get("orders");
        $result = $query->result_array();
       // echo $this->db->last_query();
        return $result;
    }

    
    
     
   public function get_all_users_transaction() {
        $post = $this->input->post();
        $this->db->select('orders.*');
        if (empty($post)) {
            $this->db->where('1');
        }
        $this->db->join('user', 'user.id = orders.user_id', 'left');
        if (isset($post['date_filter']) && !empty($post['date_filter'])) {
            $date_filter = explode("-", $post['date_filter']);
            $this->db->where('orders.order_date BETWEEN "' . date('Y-m-d h:m:s', strtotime($date_filter[0])) . '" AND "' . date('Y-m-d h:m:s', strtotime($date_filter[1])) . '"');
        }
        $this->db->order_by("orders.order_date", "DESC");
        $query = $this->db->get('orders');
        $result = array();
        $results['query'] = $this->db->last_query();
        $results['totalrecords'] = $query->num_rows();
        $results['user'] = $query->result_array();
        return $results;
    }

}

?>