<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Inventory model class.
 * 
 * @extends CI_Model
 */
class Inventory_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	
	/**
	 * get request details from request id function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_request_detail($request_id) {
		
		return $this->db
		->select('purchase_request_detail.*,products.code,products.name,products.id as product_id')
		->from('purchase_request_detail')
		->join('products', 'products.id = purchase_request_detail.product_id','right')
		->where('purchase_request_detail.request_id', $request_id)
		->order_by("purchase_request_detail.id")
		->get()->result();
		
	}
	
	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) {
		
		$this->db->from('user');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
		
	}
	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}

	public function update_user($user_id, $userData) {

		$this->db->where('id', $user_id);
		return $this->db->update('user', $userData);
		
	}

	public function checkUser($data = array()) {

		$this->db->select('id');
		$this->db->from('user');
		$this->db->where(array('email'=>$data['email']));
		$query = $this->db->get();
		$check = $query->num_rows();
		
		if($check > 0){
			$result = $query->row_array();
			$data['updated_at'] = date("Y-m-d H:i:s");
			$update = $this->db->update('user',$data,array('id'=>$result['id']));
			$userID = $result['id'];
		}else{
			$data['created_at'] = date("Y-m-d H:i:s");
			$data['updated_at'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert('user',$data);
			$userID = $this->db->insert_id();

			/* START - sending notification */
			$this->load->model('notification_model');

			/* sending notification to user */
			$this->notification_model->add(array(
				'sender_id' => 0,
				'type' => 'customer_notification',
				'label' => 'customer_created',
				'title' => 'WELCOME YOU HAVE SUCCESSFULLY REGISTERED',
				'body' => "Welcome ".$data['firstname']." to JC-GENOMICS.",
				'href' => '',
				'recipient_id' => $userID,
			));

			/* sending notification to admin */
			$this->notification_model->add(array(
				'sender_id' => 0,
				'type' => 'admin_broadcast',
				'label' => 'customer_created',
				'title' => 'NEW CUSTOMER REGISTERED',
				'body' => "A new customer has registered with email id - ".$data['email'],
				'href' => '',
				'recipient_id' => 0,
			));

			/* END - sending notification */
		}

		$this->db->from('user');
		$this->db->where('id', $userID);
		return $this->db->get()->row();

		//return $userID?$userID:false;
    }
	
	function checkQrCodeExistsInKitUpload($qrcode){
		/*$this->db->select('count(*) as total');
		$this->db->from('kit_upload');
		$this->db->where('barcode',$qrcode);
		$query = $this->db->get();
		$cnt = $query->row_array();
		return $cnt['total'];*/
		return $this->db->where('barcode',$qrcode)->get('kit_upload')->num_rows();
	}

	function kitUploadGroup($data){
		$this->db->insert('kit_upload_groups',$data);
		return $this->db->insert_id(); 
	}
	function kitUploadInGroup($sql){
		$query = $this->db->query($sql);
	}
	function checkQrCodeExists($qrcode){
		$this->db->select('count(*) as total');
		$this->db->from('kit');
		$this->db->where('kit_qr_code',$qrcode);
		$query = $this->db->get();
		$cnt = $query->row_array();
		return $cnt['total'];
	}
	function update_kitGroup($group_id, $kitGroupData) {
		$this->db->where('group_id', $group_id);
		return $this->db->update('kit_upload_groups', $kitGroupData);
	}
	function getUploadKitGroup(){
		$this->db->select('*');
		$this->db->from('kit_upload_groups');
		return $this->db->get()->result();
	}
	function getUploadKitListInGroup($group_id){
		$this->db->select('*');
		$this->db->from('kit_upload');
		$this->db->join('kit_upload_groups', 'kit_upload_groups.group_id = kit_upload.group_id');
		$this->db->where('kit_upload.is_used',0);
		if(!empty($group_id)):
		$this->db->where('kit_upload.group_id',$group_id);
		endif;

		return $this->db->get()->result();
	}
	
}
