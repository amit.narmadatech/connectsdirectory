<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Billing_address class.
 * 
 * @extends CI_Model
 */
class User_order extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * get order detail function.
	 * 
	 * @access public
	 * @param int $id
	 * @return object 
	 */
	public function get_order($id) {
		
		$this->db->select('orders.*')
		//->join('billing_address', 'billing_address.user_id = orders.user_id', 'left')
		->from('orders')
		->where('orders.id', $id);

		return $this->db->get()->row();
	}

	/**
	 * get order detail function.
	 * 
	 * @access public
	 * @param int $id
	 * @return object 
	 */
	public function get_detail($id) {
		
		return $this->db
		->select('order_detail.*,plan.*')
		->from('order_detail')
		->join('plan', 'plan.id = order_detail.plan_id', 'left')
		->where('order_detail.order_id', $id)
		->order_by("order_detail.id")
		->get()->result();
	}


	/**
	 * update billing address function.
	 * 
	 * @access public
	 * @param array $billingData
	 * @return bool true on success, false on failure
	 */
	public function update($edit_id,$billingData) {

		$this->db->where('id', $edit_id);
		return $this->db->update('billing_address', $billingData);
	}
	
}
