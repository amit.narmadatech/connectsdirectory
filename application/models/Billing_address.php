<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Billing_address class.
 * 
 * @extends CI_Model
 */
class Billing_address extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * insert billing address function.
	 * 
	 * @access public
	 * @param array $billingData
	 * @return bool true on success, false on failure
	 */
	public function insert($billingData) {
		
		return $this->db->insert('billing_address', $billingData);
	}


	/**
	 * update billing address function.
	 * 
	 * @access public
	 * @param array $billingData
	 * @return bool true on success, false on failure
	 */
	public function update($edit_id,$billingData) {

		$this->db->where('id', $edit_id);
		return $this->db->update('billing_address', $billingData);
	}
	
}
