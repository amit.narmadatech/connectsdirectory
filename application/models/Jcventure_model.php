<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Package_model class.
 * 
 * @extends CI_Model
 */
class Jcventure_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
    private $table = 'packages';
    private $primary_id = 'id';

	public function __construct() {
		
		parent::__construct();
		$this->db = $this->load->database('db3', TRUE);
		
	}

	public function get_product_list($where)
    {
        return $this->db->where($where)->order_by('item_title')->get('items')->result();
    }

    public function get_designation_list($where = ['designation_parent !=' => 0,'designation_id !=' => 2])
    {
        return $this->db->where($where)->order_by('designation_title')->get('designations')->result();
    }

    public function get_expense_category_list($where)
    {
        return $this->db->where($where)->order_by('category')->get('expense_category')->result();
    }

    public function get_expense_list($where)
    {
        return $this->db->where($where)->order_by('title')->get('expense_item')->result();
    }

    public function find_product($id)
    {
        return $this->db->where('item_id',$id)->get('items')->row();
    }

    public function find_expense($id)
    {
        return $this->db->where('expense_item_id',$id)->get('expense_item')->row();
    }

    public function find_incentive($id)
    {
        return $this->db->where('designation_id',$id)->get('designations')->row();
    }

    public function getState()
    {
        return $this->db->order_by('state_title','ASC')->get('states')->result();
    }

    public function getStateById($state_id)
    {
        return $this->db->where('state_id',$state_id)->get('states')->row();
    }

    public function getDistrict($state_id)
    {
        return $this->db->where('state_id',$state_id)->order_by('district_title','ASC')->get('districts')->result();
    }

    public function getDistrictById($district_id)
    {
        return $this->db->where('district_id',$district_id)->get('districts')->row();
    }

	
	
}
