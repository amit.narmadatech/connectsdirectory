<?php

class Comman_model extends CI_Model {

    public function save($table, $data) {
        $this->db->insert($table, $data);
        return $insert_id = $this->db->insert_id();
    }

    public function update($table, $where, $data) {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete($where = array(), $tablename = '') {
        if (!empty($where)):
            $this->db->where($where);
            $this->db->delete($tablename);
        endif;
    }

    public function getRow($table, $where) {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $result = $query->row();
    }

    public function getAll($table, $where) {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by("id", "desc");
        $this->db->where($where);
        $query = $this->db->get();
        return $result = $query->result();
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function emailsend($email, $subject = "", $message = "", $files=null) {
        $config = array(
            'mailtype' => 'html',
            'newline' => '\r\n',
            'charset' => 'utf-8' //default charset
        );
        $this->email->initialize($config);
        $this->email->from('info@jcgenetics.in', 'info');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if(!empty($files)){
           $this->email->attach($files);  
        }
      
        $this->email->send();
    }

    function getBiggestId($table, $where) {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        $this->db->where($where);
        $query = $this->db->get();
        return $result = $query->result();
    }

    public function getRecordCount($table, $status) {
        return $this->db->where('status', $status)->count_all($table);
    }

    public function fetch_pagination($page, $perpage) {

        $keyword = $this->input->get('keyword');
        $archives = $this->input->get('archives');


        $page = $page - 1;
        if ($page < 0) {
            $page = 0;
        }
        $from = $page * $perpage;

        $this->db->select('blog.id,blog.title,blog.slug,blog.description,blog.blog_img,blog.created_by,blog.created_date,blog.status,Month(blog.created_date) as Month, Year(blog.created_date) as Year');
        $this->db->from('blog');
        $this->db->where('status', 1);
        if (!empty($keyword)) {

            $keyword = preg_replace('/[^A-Za-z0-9\-]/', '', $keyword);
            $this->db->like('title', $keyword);
            $this->db->or_like('description', $keyword);
        }
        if (!empty($archives)) {
           // $this->db->where('DATE_FORMAT(blog.created_date) =', DATE_FORMAT('Y-m-d', strtotime($archives))); DATE_FORMAT(date,'%Y%m')
            $this->db->where('MONTH(blog.created_date) =', date('m', strtotime($archives)));
            $this->db->where('YEAR(blog.created_date) =', date('Y', strtotime($archives)));
        }
        $this->db->order_by("created_date", "desc");

        if (!empty($archives)) {
           // $this->db->group_by(array("Month", "Year"));
        }

        $this->db->limit($perpage, $from);
        $blog = $this->db->get()->result();
       

        if (!empty($blog)) {
            return $blog;
        }
        return false;
    }

    public function ticketpagination($page, $perpage,$userid) {

        $page = $page - 1;
        if ($page <= 0) {
            $page = 0;
        }
        $from = $page * $perpage;

        $this->db->select('*');
        $this->db->from('tbl_tickets');
        $this->db->order_by("tickets_id", "desc");
        $this->db->where(array('user_id'=>$userid));
        $this->db->limit($perpage, $from);
        $result = $this->db->get()->result();

        if (!empty($result)) {
            return $result;
        }
        return false;
    }

    function ImagesUpload($pathinfo) {

        $config['upload_path'] = $pathinfo;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_filename'] = '255';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1024'; //1 MB

        if (!empty($_FILES['filename']['name'])) {

            $upload_path = FCPATH . $pathinfo;

            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('filename')) {
                return array('status' => false, 'msg' => $this->upload->display_errors());
            } else {

                $upload_data = $this->upload->data();
                return array('status' => true, 'file_name' => $upload_data['file_name']);
            }
        } else {
            return false;
        }
    }

    public function getTicketCount() {

        $sql = "SELECT 
		  (SELECT COUNT(`tickets_id`) FROM tbl_tickets WHERE tbl_tickets.status=0) as opentickets, 
		  (SELECT COUNT(`tickets_id`) FROM tbl_tickets WHERE tbl_tickets.status=1) as closetickets,
		  (SELECT COUNT(`tickets_id`) FROM tbl_tickets WHERE tbl_tickets.status=2) as inprogresstickets,
		  (SELECT COUNT(`tickets_id`) FROM tbl_tickets WHERE tbl_tickets.status=3) as pendingticltes,
		  (SELECT COUNT(`tickets_id`) FROM tbl_tickets WHERE tbl_tickets.status=4) as answerdticktes from tbl_tickets LIMIT 1";
        $result = $this->db->query($sql)->row();
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }

    public function checkRetailerServices($userid) {

        $this->db->select("tblclients.userid,companies.company_id");
        $this->db->from('narmadas_crm.tblclients');
        $this->db->join('narmadas_crm.tblretailerservices', 'tblretailerservices.retailer_id = tblclients.userid');
        $this->db->join('narmadas_hrmkiosk.companies', 'companies.company_id = tblretailerservices.company_id');
        $this->db->where('tblclients.userid', $userid);
        $this->db->where('companies.is_shipping', 1);
        $result = $this->db->get()->row();

        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }
    function get_userinfo($userId) {

    $CI = & get_instance();
    $CI->db->select('user.id,CONCAT(user.firstname," ",user.lastname) as name,user.email,billing_address.name as billing_name,billing_address.email as billing_email,billing_address.address,billing_address.mobile1,billing_address.pincode,billing_address.landmark,billing_address.state_id,billing_address.district_id,billing_address.city,billing_address.branch_id,billing_address.ref_code');
    $CI->db->from('user');
    $CI->db->join('billing_address', 'billing_address.user_id = user.id', 'left');
    $CI->db->where('user.id', $userId);
    $userdetails = $CI->db->get()->row();
    return $userdetails; 
    
}

function get_cartinfo($userId) {

    $CI = & get_instance();
    $CI->db->select('cart.*,plan.name,plan.price,plan.id as planid,plan.package_id');
    $CI->db->from('cart');
    $CI->db->join('plan', 'cart.plan_id = plan.id');
    $CI->db->where('cart.user_id', $userId);
    $cartDetails = $CI->db->get()->result();
    return $cartDetails; 
    
}

function get_jccareretailerby_id($where){
    
    $this->db->select('tblclients.userid,tblclients.ref_code,CONCAT(tblcontacts.firstname," ",tblcontacts.lastname) as name,tbllead_services.package_id,tbllead_services.package_company_id,tblclients.is_franchise,tblclients.is_retailer,tblclients.is_franchise_retailer,tblclients.fan_parent_id,tblclients.company,tblclients.address,tblclients.district,tbllead_services.status,tbllead_services.invoice_id,tblclients.userid,tblclients.addedfrom ,tblclients.ref_code,com.slug,tblclients.pan_approve')
    ->from('jccare-crm.tblclients')
    ->join('jccare-crm.tbllead_services', 'tblclients.userid = tbllead_services.customer_id')
    ->join('jcventurescmd.companies AS com', 'com.company_id = tbllead_services.package_company_id')
    ->join('jccare-crm.tblcontacts', 'tblclients.userid = tblcontacts.userid')
    //->where(array('tblclients.ref_code' => $code))
    ->where($where)
    ->group_start()
    ->where('tbllead_services.status', 1)
    ->or_where_not_in('tbllead_services.invoice_id', 0)
    ->group_end()
    ->group_start()
    ->where('com.slug','jcgenetics')
    ->or_where('com.slug','jc-venture')
    ->group_end();
    $result = $this->db->get()->result();
    return $result; 
}

    public function getJccareRetailerById($id)
    {
        return $this->db->where('tblclients.userid',$id)->get('jccare-crm.tblclients')->row();
    }
    
    public function getCompanyTdsById($company_id){
        return $this->db->where(array('tds.company_id'=>$company_id,'status'=>1))->get('jcventurescmd.tds')->row();
    }
    
    public function getKitCountJccareRetailer($jccareretailer_id="", $ref_code="") {
      $result =  $this->db->select('*')->from('orders')
               ->where('orders.order_status',1)
               ->group_start()
               ->where('orders.branch_id', $jccareretailer_id)
               ->or_where('orders.ref_code', $ref_code)
               ->group_end()
               ->get()
               ->result();
            $total_kit_dispatch=0;
            if(!empty($result)){
            
                foreach ($result as $key => $value) {
                    if(!empty($value->id)){
                        
                       $totalkit = $this->db->query("SELECT COUNT(order_detail.id) as total FROM order_detail WHERE order_detail.order_id =$value->id")->num_rows();
                       $total_kit_dispatch = ($total_kit_dispatch + $totalkit); 
                      // echo $this->db->last_query();
                    }
                }
           
            }
            return $total_kit_dispatch;
    }
    
     public function getKitStockJccareRetailer($ref_code) {
       
         $sql ="SELECT COUNT(`assign_kits`.`id`) as `stck_status` FROM `assign_kits` JOIN `assign_kit_details` ON `assign_kit_details`.`parent_id` = `assign_kits`.`id` LEFT JOIN `stock_out` ON `stock_out`.`qr_code` = `assign_kit_details`.`barcode` WHERE `assign_kits`.`reference_no` = '$ref_code' AND `stock_out`.`id`  is null GROUP BY `assign_kit_details`.`barcode`";
         $query = $this->db->query($sql);
         return $result = $query->num_rows();
    }
     public function getorderKitCountJccareRetailer($order_id) {
      
         $query = $this->db->query("SELECT COUNT(order_detail.id) as total FROM order_detail WHERE order_detail.order_id = $order_id ")->num_rows();
         $result = $query->num_rows();
         echo $this->db->last_query();
         die;
         
     }
    
    

}

?>