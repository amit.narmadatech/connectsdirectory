<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Template model class.
 * 
 * @extends CI_Model
 */
class Template_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_nf_template_by_id($id) {
		
		$this->db->select('*');
		$this->db->from('notification_templates');
		$this->db->where('id', $id);
		return $this->db->get()->row();	
	}

	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_nf_template() {
		
		$this->db->select('*');
		$this->db->from('notification_templates');
		$this->db->where('status', 1);
		return $this->db->get()->result();	
	}

	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function update_notification_template($id, $data) {
		
		return $this->db->where('id', $id)->update('notification_templates', $data);	
	}
	
}
