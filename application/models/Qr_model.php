<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * QR_model class.
 * 
 * @extends CI_Model
 */
class Qr_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $qrcode
	 * @param mixed $code
	 * @param mixed $image
	 * @param mixed $user_id
	 * @return bool true on success, false on failure
	 */
	public function create_code($qrcode, $code , $image, $user_id, $group_id) {
		$data = array(
			'group_id' => $group_id,
			'kit_qr_code' => $qrcode,
			'kit_unique_code' => $code,
			'image' => $image,
			'created_by' => $user_id,
		);
		
		return $this->db->insert('kit', $data);
		
	}

	/**
	 * check unique QR code.
	 * 
	 * @access public
	 * @param mixed $code
	 * @return bool true on success, false on failure
	 */
	public function check_unique_qr($code)
	{
		$this->db->select('id');
		$this->db->from('kit');
		$this->db->where('kit_qr_code', $code);

		return ($this->db->get()->num_rows() > 0)?true:false;
	}
	
}
