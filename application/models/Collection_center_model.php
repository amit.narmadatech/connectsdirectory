<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Collection_center_model class.
 * 
 * @extends CI_Model
 */
class Collection_center_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * insert data function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function insert($data = array()) {
		
		if (!empty($data)) {
			$this->db->insert('collection_center', $data);
			return $this->db->insert_id();
		}else
		{
			return 0;
		}
	}

	/**
	 * update data function.
	 * 
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function update($id = null,$data = array()) {
		
		if ($id) {
			$this->db->where('id', $id);
			return $this->db->update('collection_center', $data);
		}else
		{
			return false;
		}
	}
	
}
