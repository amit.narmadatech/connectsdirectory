<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH."/third_party/PHPExcel/Classes/PHPExcel.php";
require_once APPPATH."/third_party/PHPExcel/Classes/PHPExcel/IOFactory.php";
/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
}