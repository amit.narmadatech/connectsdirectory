<?php
$name='Montserrat-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 700.0,
  'XHeight' => 538.0,
  'FontBBox' => '[-882 -266 1679 1076]',
  'Flags' => 262148,
  'Ascent' => 968.0,
  'Descent' => -251.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 165.0,
  'MissingWidth' => 587.0,
);
$unitsPerEm=1000;
$up=-75;
$ut=50;
$strp=322;
$strs=50;
$ttffile='C:/xampp/htdocs/jcpay/application/libraries/mpdf60/ttfonts/Montserrat-Bold.ttf';
$TTCfontID='0';
$originalsize=262608;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='montserratB';
$panose=' 0 0 0 0 8 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=true;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 968, -251, 0
// usWinAscent/usWinDescent = 1109, -270
// hhea Ascent/Descent/LineGap = 968, -251, 0
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'DFLT' => 'DFLT ',
  'cyrl' => 'DFLT BGR  BSH  CHU  MKD  SRB  ',
  'latn' => 'DFLT AZE  CAT  CRT  KAZ  MOL  NLD  ROM  TAT  TRK  ',
);
$GSUBFeatures=array (
  'DFLT' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'BGR ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 17,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'BSH ' => 
    array (
      'locl' => 
      array (
        0 => 21,
      ),
    ),
    'CHU ' => 
    array (
      'locl' => 
      array (
        0 => 20,
      ),
    ),
    'MKD ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 18,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'SRB ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 19,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
        4 => 6,
        5 => 7,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'AZE ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 16,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'CAT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 9,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'CRT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 15,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'KAZ ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 12,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'MOL ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'NLD ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 8,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'ROM ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 10,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'TAT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 13,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'TRK ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 14,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 89904,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 3,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 91012,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 85402,
      1 => 85420,
      2 => 85440,
      3 => 85458,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 85476,
      1 => 85494,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 85512,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 85810,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86108,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86258,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86408,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86438,
      1 => 86458,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86478,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86492,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86506,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86512,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86518,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86524,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86530,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86536,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86588,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86594,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86610,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86620,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86636,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86642,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86648,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86654,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86660,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86666,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86672,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86678,
      1 => 86696,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86714,
      1 => 86732,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86750,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86770,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86776,
    ),
    'MarkFilteringSet' => '',
  ),
  34 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86782,
    ),
    'MarkFilteringSet' => '',
  ),
  35 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86788,
    ),
    'MarkFilteringSet' => '',
  ),
  36 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86794,
    ),
    'MarkFilteringSet' => '',
  ),
  37 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 87330,
    ),
    'MarkFilteringSet' => '',
  ),
  38 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 87864,
    ),
    'MarkFilteringSet' => '',
  ),
  39 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88000,
    ),
    'MarkFilteringSet' => '',
  ),
  40 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88018,
    ),
    'MarkFilteringSet' => '',
  ),
  41 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88044,
    ),
    'MarkFilteringSet' => '',
  ),
  42 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88974,
    ),
    'MarkFilteringSet' => '',
  ),
  43 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 93578,
    ),
    'MarkFilteringSet' => '',
  ),
  44 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 93672,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'DFLT' => 'DFLT ',
  'cyrl' => 'DFLT BGR  MKD  SRB  ',
  'latn' => 'DFLT AZE  CAT  CRT  KAZ  MOL  NLD  ROM  TAT  TRK  ',
);
$GPOSFeatures=array (
  'DFLT' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'BGR ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'MKD ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'SRB ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'AZE ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'CAT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'CRT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'KAZ ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'MOL ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'NLD ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'ROM ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'TAT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'TRK ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 2048,
      1 => 2098,
      2 => 3242,
      3 => 6506,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 10,
    'Subtables' => 
    array (
      0 => 9690,
      1 => 12348,
      2 => 12454,
      3 => 19310,
      4 => 22446,
      5 => 23914,
      6 => 24072,
      7 => 27868,
      8 => 31000,
      9 => 31704,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 31924,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 32678,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 38076,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 6,
    'Flag' => 256,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 56506,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 512,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 56586,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 6,
    'Flag' => 512,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 57368,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>