<?php
$name='Montserrat-Regular';
$type='TTF';
$desc=array (
  'CapHeight' => 700.0,
  'XHeight' => 525.0,
  'FontBBox' => '[-823 -262 1586 1043]',
  'Flags' => 4,
  'Ascent' => 968.0,
  'Descent' => -251.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 587.0,
);
$unitsPerEm=1000;
$up=-75;
$ut=50;
$strp=315;
$strs=50;
$ttffile='C:/xampp/htdocs/jcpay/application/libraries/mpdf60/ttfonts/Montserrat-Regular.ttf';
$TTCfontID='0';
$originalsize=264340;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='montserrat';
$panose=' 0 0 0 0 5 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=true;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 968, -251, 0
// usWinAscent/usWinDescent = 1109, -270
// hhea Ascent/Descent/LineGap = 968, -251, 0
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'DFLT' => 'DFLT ',
  'cyrl' => 'DFLT BGR  BSH  CHU  MKD  SRB  ',
  'latn' => 'DFLT AZE  CAT  CRT  KAZ  MOL  NLD  ROM  TAT  TRK  ',
);
$GSUBFeatures=array (
  'DFLT' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'BGR ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 17,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'BSH ' => 
    array (
      'locl' => 
      array (
        0 => 21,
      ),
    ),
    'CHU ' => 
    array (
      'locl' => 
      array (
        0 => 20,
      ),
    ),
    'MKD ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 18,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'SRB ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 19,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
        4 => 6,
        5 => 7,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'AZE ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 16,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'CAT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 9,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'CRT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 15,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'KAZ ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 12,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'MOL ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'NLD ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 8,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'ROM ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 10,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'TAT ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 13,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
    'TRK ' => 
    array (
      'aalt' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'ccmp' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
        3 => 5,
      ),
      'locl' => 
      array (
        0 => 14,
      ),
      'subs' => 
      array (
        0 => 22,
      ),
      'sinf' => 
      array (
        0 => 23,
      ),
      'sups' => 
      array (
        0 => 24,
      ),
      'numr' => 
      array (
        0 => 25,
      ),
      'dnom' => 
      array (
        0 => 26,
      ),
      'frac' => 
      array (
        0 => 27,
        1 => 28,
        2 => 29,
      ),
      'ordn' => 
      array (
        0 => 30,
        1 => 31,
      ),
      'lnum' => 
      array (
        0 => 32,
      ),
      'pnum' => 
      array (
        0 => 33,
      ),
      'tnum' => 
      array (
        0 => 34,
      ),
      'onum' => 
      array (
        0 => 35,
      ),
      'c2sc' => 
      array (
        0 => 36,
      ),
      'smcp' => 
      array (
        0 => 37,
      ),
      'case' => 
      array (
        0 => 38,
      ),
      'dlig' => 
      array (
        0 => 39,
      ),
      'liga' => 
      array (
        0 => 40,
      ),
      'salt' => 
      array (
        0 => 41,
      ),
      'ss01' => 
      array (
        0 => 42,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 89896,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 3,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 91004,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 85394,
      1 => 85412,
      2 => 85432,
      3 => 85450,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 85468,
      1 => 85486,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 85504,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 85802,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86100,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86250,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86400,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86430,
      1 => 86450,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86470,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86484,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86498,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86504,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86510,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86516,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86522,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86528,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86580,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86586,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86602,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86612,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86628,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86634,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86640,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86646,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86652,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86658,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86664,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86670,
      1 => 86688,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 2,
    'Subtables' => 
    array (
      0 => 86706,
      1 => 86724,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86742,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86762,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86768,
    ),
    'MarkFilteringSet' => '',
  ),
  34 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86774,
    ),
    'MarkFilteringSet' => '',
  ),
  35 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86780,
    ),
    'MarkFilteringSet' => '',
  ),
  36 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 86786,
    ),
    'MarkFilteringSet' => '',
  ),
  37 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 87322,
    ),
    'MarkFilteringSet' => '',
  ),
  38 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 87856,
    ),
    'MarkFilteringSet' => '',
  ),
  39 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 87992,
    ),
    'MarkFilteringSet' => '',
  ),
  40 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88010,
    ),
    'MarkFilteringSet' => '',
  ),
  41 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88036,
    ),
    'MarkFilteringSet' => '',
  ),
  42 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 88966,
    ),
    'MarkFilteringSet' => '',
  ),
  43 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 93570,
    ),
    'MarkFilteringSet' => '',
  ),
  44 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 93664,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'DFLT' => 'DFLT ',
  'cyrl' => 'DFLT BGR  MKD  SRB  ',
  'latn' => 'DFLT AZE  CAT  CRT  KAZ  MOL  NLD  ROM  TAT  TRK  ',
);
$GPOSFeatures=array (
  'DFLT' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'BGR ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'MKD ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'SRB ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'AZE ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'CAT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'CRT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'KAZ ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'MOL ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'NLD ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'ROM ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'TAT ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
    'TRK ' => 
    array (
      'kern' => 
      array (
        0 => 0,
        1 => 1,
      ),
      'mark' => 
      array (
        0 => 2,
        1 => 3,
        2 => 4,
      ),
      'mkmk' => 
      array (
        0 => 5,
        1 => 6,
        2 => 7,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 4,
    'Subtables' => 
    array (
      0 => 2048,
      1 => 2098,
      2 => 3242,
      3 => 6506,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 10,
    'Subtables' => 
    array (
      0 => 9690,
      1 => 12348,
      2 => 12454,
      3 => 19310,
      4 => 22446,
      5 => 23936,
      6 => 24094,
      7 => 27890,
      8 => 30984,
      9 => 31672,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 31892,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 32652,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 38068,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 6,
    'Flag' => 256,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 56492,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 512,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 56572,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 6,
    'Flag' => 512,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 57366,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>