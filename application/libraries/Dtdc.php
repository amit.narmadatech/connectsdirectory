<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dtdc
{
	private $error = array();
    private $client_id = 'CC000100132';
 	
    public function create_package($DocketList)
    {
        //$url = "http://dotzot-test.azurewebsites.net/RestService/PushOrderDataService.svc/PushOrderData_PUDO_GST";
        $url = "http://instacom.dotzot.in/restservice/pushorderdataservice.svc/pushorderdata_pudo_gst";

        $data = array (
          'Customer' => 
                  array (
                    'CUSTCD' => $this->client_id,
                  ),
          'DocketList' => $DocketList
      );
                  /*array (
                    0 => 
                    array (
                      'AgentID' => '',
                      'AwbNo' => '',
                      'Breath' => '1',
                      'CPD' => '06/60/2018',
                      'CollectableAmount' => '5000',
                      'Consg_Number' => '0012587',
                      'Consolidate_EW' => '123456',
                      'CustomerName' => 'Abhishek',
                      'Ewb_Number' => '876867',
                      'GST_REG_STATUS' => 'Y',
                      'HSN_code' => '02314h03',
                      'Height' => '1',
                      'Invoice_Ref' => 'AB123X001',
                      'IsPudo' => 'N',
                      'ItemName' => 'Iphone X 246 gb black',
                      'Length' => '1',
                      'Mode' => 'C',
                      'NoOfPieces' => '1',
                      'OrderConformation' => 'Y',
                      'OrderNo' => 'AB123X001',
                      'ProductCode' => '00123',
                      'PudoId' => '',
                      'REASON_TRANSPORT' => '',
                      'RateCalculation' => 'N',
                      'Seller_GSTIN' => '123223H2',
                      'ShippingAdd1' => 'Plot number 14 DTDC Express limited',
                      'ShippingAdd2' => 'GOregaon East',
                      'ShippingCity' => 'Mumba',
                      'ShippingEmailId' => 'Abhishek.pandey@dotzot.in',
                      'ShippingMobileNo' => '8433985578',
                      'ShippingState' => 'Maharashtra',
                      'ShippingTelephoneNo' => '0224009038',
                      'ShippingZip' => '400063',
                      'Shipping_GSTIN' => 'H212hf33',
                      'TotalAmount' => '2999',
                      'TransDistance' => '20',
                      'TransporterID' => 'TID0123',
                      'TransporterName' => 'DTDC Courier Cargo',
                      'TypeOfDelivery' => 'Home Delivery',
                      'TypeOfService' => 'Economy',
                      'UOM' => 'Per KG',
                      'VendorAddress1' => 'Plot number 98',
                      'VendorAddress2' => 'Pump House',
                      'VendorName' => 'ABC ltd',
                      'VendorPincode' => '400063',
                      'VendorTeleNo' => '8433985578',
                      'Weight' => '0.150',
                    ),
                  ),*/
                
        
        $response = $this->do_post_request($url, $data);
        return $response;

    }

    public function reverse_package($data=null)
    {
      /*$url = "http://dotzot-test.azurewebsites.net/services/InstacomCustomerServices.asmx/PushReverseOrderData_PUDO";

      $data = array(
                    'ClientId' =>'INSTACOM',
                    'UserName' => 'instauser',
                    'Password' => 'insta2013',
                    'ConsignorName' => 'vikas kanik',
                    'ConsignorAddress1' => 'ujjain',
                    'ConsignorAddress2' => 'ujjain',
                    'SkuDescription' => 'test sku',
                    'DeclaredValue' => 100,
                    'AgentId' => 'VTEST',
                    'CustomerCode' => 'CC000100132',
                    'VendorName' => 'Amit meena',
                    'VendorAddress1' => 'indore',
                    'VendorAddress2' => 'indore',
                    'VendorPincode' => '452010',
                    'VendorTeleNo' => '8871374798',
                    'TransportMode' => 'Express',
                    'ItemChecked' =>'N',
                    'DockNo' => '700004805762',
                  );*/
      
      //$soapUrl = "http://dotzot-test.azurewebsites.net/services/InstacomCustomerServices.asmx/PushReverseOrderData_PUDO";
      $soapUrl = "http://instacom.dotzot.in/services/InstacomCustomerServices.asmx/PushReverseOrderData_PUDO";

      $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <PushReverseOrderData_PUDO xmlns="http://dotzot-test.azurewebsites.net/">
      <ClientId>INSTACOM</ClientId>
      <UserName>instauser</UserName>
      <Password>insta2013</Password>
      <RequestId>R1</RequestId>
      <ConsignorName>vikas</ConsignorName>
      <ConsignorAddress1>ujjain</ConsignorAddress1>
      <ConsignorAddress2>ujjain</ConsignorAddress2>
      <MobileNo>8871374798</MobileNo>
      <Pincode>456010</Pincode>
      <SkuDescription>test </SkuDescription>
      <DeclaredValue>100</DeclaredValue>
      <AgentId>VTEST</AgentId>
      <CustomerCode>CC000100132</CustomerCode>
      <VendorName>amit</VendorName>
      <VendorAddress1>indore</VendorAddress1>
      <VendorAddress2>indore</VendorAddress2>
      <VendorPincode>452010</VendorPincode>
      <VendorTeleNo>7000091226</VendorTeleNo>
      <TransportMode>Express</TransportMode>
      <ItemChecked>N</ItemChecked>
      <DockNo>700004805762</DockNo>
    </PushReverseOrderData_PUDO>
  </soap:Body>
</soap:Envelope>';

/*$xml_post_string = '<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dot="http://dotzot-test.azurewebsites.net/">
<x:Header/>
<x:Body>
<dot:PushReverseOrderData_PUDO>
<dot:ClientId>INSTACOM</dot:ClientId>
<dot:UserName>instauser</dot:UserName>
<dot:Password>insta2013</dot:Password>
<dot:RequestId>R1</dot:RequestId>
<dot:ConsignorName>testt</dot:ConsignorName>
<dot:ConsignorAddress1>test</dot:ConsignorAddress1>
<dot:ConsignorAddress2>tset</dot:ConsignorAddress2>
<dot:MobileNo>1234567890</dot:MobileNo>
<dot:Pincode>302028</dot:Pincode>
<dot:SkuDescription>test sku</dot:SkuDescription>
<dot:DeclaredValue>100</dot:DeclaredValue>
<dot:AgentId>VTEST</dot:AgentId>
<dot:CustomerCode>CC000100132</dot:CustomerCode>
<dot:VendorName>TEST</dot:VendorName>
<dot:VendorAddress1>TEST</dot:VendorAddress1>
<dot:VendorAddress2>TEST</dot:VendorAddress2>
<dot:VendorPincode>395001</dot:VendorPincode>
<dot:VendorTeleNo>7896541230</dot:VendorTeleNo>
<dot:TransportMode>Express</dot:TransportMode>
<dot:ItemChecked>N</dot:ItemChecked>
<dot:DockNo>700004805762</dot:DockNo>
</dot:PushReverseOrderData_PUDO>
</x:Body>
</x:Envelope>';*/

      $headers = array(
      "POST /services/InstacomCustomerServices.asmx HTTP/1.1",
      "Host: dotzot-test.azurewebsites.net",
      "Content-Type: text/xml; charset=utf-8",
      "Content-Length: ".strlen($xml_post_string)
      ); 

      $url = $soapUrl;

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $response = curl_exec($ch); 
      curl_close($ch);
      print_r($response);die;
    }

    public function track($tracking_code)
    {
        //$url = "http://dotzot-test.azurewebsites.net/RestService/DocketTrackingService.svc/GetDocketTrackingDetails";
        $url = "http://dotzot.azurewebsites.net/RestService/DocketTrackingService.svc/GetDocketTrackingDetails";
        $data = array('DocketNo' => $tracking_code);
        
        $response = $this->do_post_request($url, $data);
        
        return $response;

    }

    public function do_post_request($url, $data) {
        
        $headers = array('Content-Type: application/json');
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode($data) );
        
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        return $err ? $err : $response;
    }

    
}