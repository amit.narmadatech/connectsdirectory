<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Delhivery
{
	private $error = array();
    private $api_url = 'https://track.delhivery.com/';
    private $token = '573deec75e70c68547dfc668696d26b3bfa20066';
    private $client_name = 'A2ZSALE EXPRESS';
 	
 	public function check_pincode($pincode) {
        $url = $this->api_url . "c/api/pin-codes/json/?";
        $data = array('token' => $this->token, 'filter_codes' => $pincode);
        $response = json_decode($this->do_get_request($url . http_build_query($data)));
        if(empty($response->delivery_codes)){
            return false;
        }
        $result = $response->delivery_codes[0]->postal_code;
        return ($result->pre_paid=="Y")? true:false;
    }

    public function create_waybill($count = 1) {
        $url = $this->api_url . "waybill/api/bulk/json/?";
        $data = array('token' => $this->token, 'cl' => $this->client_name, 'count' => $count);
        $waybill_number = $this->do_get_request($url . http_build_query($data));
        $waybill_number = explode(",", str_replace('"', '', $waybill_number));
        $return = is_array($waybill_number) ? $waybill_number[0] : $waybill_number;
        return $return;
    }

    public function add_warehouse($data=null) {
        $url = $this->api_url . "api/backend/clientwarehouse/create/";
       /* $data = array(
            "phone" => 919685095755,
            "city" => "Indore",
            "name" => "JCG Warehouse",
            "pin" => '452010',
            "address" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road",
            "country" => "India",
            "contact_person" => "Yashwant Sharma",
            "email" => "warehousing@jcventures.in",
            "registered_name" => $this->client_name
        );*/

        /*$data = array(
            "name" => "JCG Warehouse",
        );*/
        $recv = $this->do_post_request($url, $data);
        $response = json_decode($recv);
        print_r($response);die;
        return empty($response->success)?false:true;
    }

    public function do_get_request($url, $method = "GET", $data = array()) {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: Token " . $this->token,
                "Cache-Control: no-cache"
            )
                )
        );
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //print_r($err);die;

        return $err ? $err : $response;
    }

    public function do_post_request($url, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Authorization: Token " . $this->token,
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $err ? $err : $response;
    }

    public function packing_slip($waybill) {
        $url = $this->api_url . "api/p/packing_slip?wbns=" . $waybill;
        $response = $this->do_get_request($url);
        //echo"<prE>";
        //print_r($response);
        return $response;
    }

    public function create_pickup_request() {
        $url = $this->api_url . "fm/request/new/";
        $data = array('pickup_location' => $this->client_name, 'pickup_time' => "09:43:42", 'pickup_date' => "20180430", 'expected_package_count' => 10);
        $response = $this->do_post_request($url, $data);
        echo'<prE>';
        print_r($response);
    }

    public function invoice() {
        $data = array("md" => "E", "zn" => "A", "ss" => "DTO", "pt" => "Pre-paid", "cl" => $this->client_name, "token" => $this->token, "format" => "json");
        $url = $this->api_url . "api/kinko/v1/invoice/charges/.json?" . http_build_query($data);
        $response = json_decode($this->do_get_request($url), true);
        echo"<prE>";
        print_r($response);
    }

    public function track_order($waybill, $verbose) {
        $data = array("token" => $this->token, "waybill" => $waybill, "verbose" => $verbose);
        $url = $this->api_url . "api/packages/json/?" . http_build_query($data);
        $response = json_decode($this->do_get_request($url), true);
        echo"<prE>";
        print_r($response);
    }

    public function create_manifest() {
        $waybill = $this->create_waybill();
        //print_r($waybill);
        //die;
        $slip = $this->packing_slip($waybill);

        $slipdata = json_decode($slip, true);
        print_r($slipdata);
        die;
        $this->load->view('shipping/manifest', $slipdata);
    }

    public function pickup_request($data) {
        //$data = array("pickup_time" => '18:30:00', "pickup_date" => '20180506', "pickup_location" => "sandeep",'expected_package_count'=>3);
        $url = $this->api_url . "fm/request/new/" ;
        return $response = json_decode($this->do_post_request($url,$data), true);
    }

    protected function validateForm() {
        if (!$this->request->post['pic_date']) {
                $this->error['pic_date'] = 'pickup date required';
        }
        if (!$this->request->post['pic_time']) {
                $this->error['pic_time'] = 'pickup time required';
        }

        return !$this->error;
    }
    public function warehouse_status() {
        $data = array( "name" => 'A2ZSALE EXPRESS');
        $url = $this->api_url . "api/backend/clientwarehouse/status/" ;
        $response = json_decode($this->do_post_request($url,$data), true);
        echo"<prE>";
        print_r($response);
    }

	public function create_package($dispatch_id,$shipData,$pickupData=null) 
	{
        	$url = $this->api_url . "api/cmu/create.json";
        
            $waybill = $this->create_waybill();
            if (!$waybill) {
                die('waybill not generated.');
            }
                
            $send['format'] = "json";
            $data['dispatch_date'] = "2018-06-03T10:53:46.789382+00:00";
            $data['dispatch_id'] = $dispatch_id;

            if (!empty($pickupData)) {
            	$data['pickup_location'] = $pickupData;
            
            }else{
            	// default warehouse
	            $data['pickup_location'] = array("add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial, Scheme No. 54, A.B. Road",
                    "city" => "Indore", 
                    "country" => "India",
                    "name" => 'JCG Warehouse',
                    "phone" => "919685095755",
                    "pin" => "452010"
                );
            }


            $data['shipments'][] = array(
            	"waybill" => $waybill, # unique delhivery tracking number
                "client" => $this->client_name, # client name
                "name" => $shipData['name'], # consignee
                "order" => $shipData['order'], # client order number
                "products_desc" => 'genetic test kit', # product description
                "order_date" => $shipData['order_date'], # ISO Format
                "payment_mode" => $shipData['payment_mode'], #It can be, Pre­paid, COD, Pickup, REPL or Cash
                //"total_amount" => $order_product['total'], # in INR
                "total_amount" => $shipData['total_amount'], # in INR
                "cod_amount" => 0.0, # amount to be collected, required for COD
                "add" => $shipData['add'],
                "city" => $shipData['city'],
                "state" => $shipData['state'],
                "country" => "India",
                "phone" => $shipData['phone'],
                "pin" => $shipData['pin'],
                "return_add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial Scheme No. 54, A.B. Road",
                "return_city" => "Indore",
                "return_country" => "India",
                "return_name" => "Yashwant Sharma",
                "return_phone" => "9685095755",
                "return_pin" => "452010",
                "return_state" => "Madhya Pradesh",
                "supplier" => "JC Genetics Private Limited",
                # seller weight and dimensions
                // "extra_parameters" => array('dasd'=>1), # optional ­ Extra parameters that need to be send with packages. This should be unicoded dictionary
                "shipment_width" => "15.24", # optional width of shipment
                "shipment_height" => "5.588", # optional ­ height of shipment
                "weight" => "175.0 gm", # required
                "quantity" => $shipData['quantity'], # quantity of goods, positive integer
                # seller keys(optional)
                "seller_inv" => $shipData['order'], # seller invoice no.
                //"seller_inv_date" => "2018-04-18T09:30:SS+05:30", # ISO format
                "seller_inv_date" => $shipData['order_date'], # ISO format
                "seller_name" => "JC Genetics Private Limited", # name of seller
                "seller_add" => "7th Floor, 714-716, Princess Business Skypark, PU-3 Commercial Scheme No. 54, A.B. Road", # seller address
                "seller_cst" => "", # seller cst no
                "seller_tin" => "", # seller tin no
                # extra(optional)
                "consignee_tin" => "", # consignee tin no
                "commodity_value" => "", # commodity value
                "tax_value" => "tax value", # tax value
                "sales_tax_form_ack_no" => "", # Sale Tax Form Acknowledge No.
                "category_of_goods" => "", # Category of goods
                # GST keys
                "seller_gst_tin" => " ", #GST TIN # of the seller
                "client_gst_tin" => "", #GST TIN # of the contracting entity with Delhivery
                "consignee_gst_tin" => "", #GST tin of the consignee in case of B2B shipments
                "hsn_code" => "", #Share the Harmonized System of Nomenclature for each product in the package, you can pass more than one HSN here if the quantity is more than 1,using comma seperation.
                "invoice_reference" => "JCGNTC" #unique invoice reference number
            );

            $send['data'] = json_encode($data);
            //print_r($data);
            //die;
            $package = json_decode($this->do_post_request($url, http_build_query($send)), true);
            /*print_r($package);
            die;*/
        
        $slip = $this->packing_slip($waybill);
        $slipdata = json_decode($slip, true);
        if (empty($slipdata)) {
            return false;
        }else{
        	return array('slipdata' => $slipdata,'waybill' => $waybill);
        }
        //print_r($slipdata);
        //die;
        //$this->load->view('admin/inventory/manifest', compact("slipdata", "package","shipData"));
    }
}