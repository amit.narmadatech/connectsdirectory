<?php

function is_logged_in() {
    $CI = & get_instance();
    //print_r($CI->session->userdata);die;
    $user = $CI->session->user_id;
    $type = $CI->session->type;

    if (empty($user)) {

        return false;
    } elseif ($type != 4) {

        return false;
    } else {
        $userinfo = $CI->Comman_model->getRow('user', ['id' => $user]);
        //print_r($userinfo);die;
        if (!empty($userinfo)) {
            return true;
        }
    }
}

function get_notification($template_id = 0, $data = array()) {
    $CI = & get_instance();
    $customer_template = $CI->template_model->get_nf_template_by_id($template_id);
    $title = $customer_template->title;
    $body = $customer_template->body;
    $tokens = unserialize($customer_template->available_perameter);

    foreach ($tokens as $key => $value) {
        $title = str_replace("$key", $data[$key], $title);
        $body = str_replace("$key", $data[$key], $body);
    }

    return array('title' => $title, 'body' => $body);
}

function check_notification_isread($nfid, $user_id) {
    $CI = & get_instance();
    $results = $CI->db->where('notification_id', $nfid)->where('user_id', $user_id)->get('notification_read_status')->num_rows();
    return ($results > 0);
}

function is_admin() {
    $CI = & get_instance();
    $user = $CI->session->userdata('user_id');
    $type = $CI->session->userdata('user_type');
    //print_r($CI->session);die;
    if (empty($user)) {
        return false;
    } elseif ($type != 1) {
        return false;
    } else {
        return true;
    }
}

function set_browser_id($bid) {
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');

    if (!empty($user_id)) {
        $CI->db->where(['id' => $user_id])->update('user', ['browser_token' => $bid]);
    }
}

function check_permission() {
    return true;
    $CI = & get_instance();
    $method = $CI->input->method();
    if ($method == 'post') {
        return true;
    }
    $user_id = $CI->session->userdata('user_id');
    $CI->load->model('comman_model');

    $userData = $CI->comman_model->getRow('user', ['id' => $user_id]);
    $permissionData = $CI->comman_model->getRow('designations', ['id' => $userData->designation_id]);

    $permissions = unserialize($permissionData->permissions);
    //print_r($permissions);die;
    $segment1 = $CI->uri->segment(1);
    $segment2 = empty($CI->uri->segment(2)) ? '' : "/" . $CI->uri->segment(2);
    $requrl = $segment1 . $segment2;
    //$rqsturl = uri_string();
    $menuid = $CI->db->where('link', $requrl)->select('id')->get('menus')->row();

    //print_r($menuid->id);die;
    if (!empty($menuid->id) && in_array($menuid->id, $permissions)) {
        return true;
    } else {
        return false;
    }
}

function check_permission_by_id($id) {
    //return true;
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');
    $CI->load->model('comman_model');

    $userData = $CI->comman_model->getRow('user', ['id' => $user_id]);
    $permissionData = $CI->comman_model->getRow('designations', ['id' => $userData->designation_id]);
    //print_r($userData);die;

    $permissions = unserialize($permissionData->permissions);

    if (!empty($id) && in_array($id, $permissions)) {
        return true;
    } else {
        return false;
    }
}

function check_stockin_exists($pono) {
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');

    if (!empty($user_id)) {
        $count = $CI->db->where('po_no', $pono)->get('stock_purchase')->num_rows();
        return ($count > 0) ? true : false;
    }
}

if (!function_exists('getState')) {

    function getState($id) {
        $CI = & get_instance();
        $user_id = $CI->session->userdata('user_id');
        $CI->load->model('Jcventures_model', 'jcventure');

        return $CI->jcventure->getStateById($id);
    }

}

if (!function_exists('getDistrict')) {

    function getDistrict($id) {
        $CI = & get_instance();
        $CI->load->model('Jcventure_model', 'jcventure');

        return $CI->jcventure->getDistrictById($id);
    }

}

function count_qrcode_by_groupid($gid) {
    $CI = & get_instance();
    $user_id = $CI->session->userdata('user_id');

    return $count = $CI->db->where('group_id', $gid)->get('kit')->num_rows();
}

function check_menu_has_parent($id) {
    $CI = & get_instance();
    $menus = $CI->db->where('parent', $id)->get('menus')->num_rows();
    //print_r($menus);die;
    return ($menus > 0);
}

if (!function_exists('explode_name')) {

    function explode_name($source_image, $prifix) {
        $ext = strrchr($source_image, '.');
        $name = ($ext === FALSE) ? $source_image : substr($source_image, 0, -strlen($ext));

        return $name . $prifix . $ext;
    }

}

if (!function_exists('get_expense_category')) {

    function get_expense_category($id) {
        $CI = & get_instance();
        $CI->db = $CI->load->database('db3', TRUE);
        return $CI->db->where('expense_category_id', $id)->get('expense_category')->row();
    }

}

if (!function_exists('getCart')) {

    function getCart() {
        $CI = & get_instance();
        $userid = $CI->session->user_id;
        $CI->db->select('cart.*,plan.name,plan.price,plan.id as planid');
        $CI->db->from('cart');
        $CI->db->join('plan', 'cart.plan_id = plan.id');
        $CI->db->where('cart.user_id', $userid);
        $cart = $CI->db->get()->result();
        //echo $CI->db->last_query();die;
        if (!empty($cart)) {
            return $cart;
        } else {
            return false;
        }
    }

}

if (!function_exists('getTotalCount')) {

    function getTotalCount() {
        $CI = & get_instance();
        $userid = $CI->session->user_id;

        $CI->db->select('cart_id');
        $CI->db->from('cart');

        if (!empty($userid)) {
            $CI->db->where('cart.user_id', $userid);
        } else {

            $session_id = $CI->session->userdata('session_id');
            $CI->db->where('cart.session_id', $session_id);
        }
        return $totalcount = $CI->db->get()->num_rows();
    }

}

if (!function_exists('checkKitQuantityAlert')) {

    function checkKitQuantityAlert() {
        $CI = & get_instance();

        $count_kit = $CI->db->where('is_used', 0)->get('kit_upload')->num_rows();
        $item = $CI->db->where('id', 1)->get('products')->row();
        if (!empty($item)) {
        if ($count_kit < $item->alert_quantity) {
            $CI->session->set_flashdata('alert_non_blocking', 'Kit quantity alert ! Kit quantity is below then alert value.');
        }
        }
    }

}


if (!function_exists('curl_file_get_contents')) {

    function curl_file_get_contents($url) {
        $curl = curl_init();
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

        curl_setopt($curl, CURLOPT_URL, $url); //The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5); //The number of seconds to wait while trying to connect.	

        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE); //To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($curl, CURLOPT_TIMEOUT, 10); //The maximum number of seconds to allow cURL functions to execute.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); //To stop cURL from verifying the peer's certificate.
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
    }

}


if (!function_exists('required')) {

    function required($required) {
        $tag = true;
        foreach ($required as $req) {
            if (empty($req)) {
                $tag = false;
            }
        }
        return $tag;
    }

}

if (!function_exists('is_serial')) {

    function is_serial($string) {
        return (@unserialize($string) !== false || $string == 'b:0;');
    }

}


if (!function_exists('money_formate')) {

    function money_formate($money) {
        return CURRENCY_SIGN . ' ' . number_format(($money), 2, ".", ",");
    }

}

if (!function_exists('date_formate')) {

    function date_formate($date) {
        return date("m/d/Y", strtotime($date));
    }

}
if (!function_exists('datetime_formate')) {

    function datetime_formate($date) {
        return date("m/d/Y H:i", strtotime($date));
    }

}

if (!function_exists('get_menu_name')) {

    function get_menu_name($name) {
        $str = explode('_', $name);
        $text = '';
        foreach ($str as $tx) {
            $text .= ucfirst($tx) . ' ';
        }
        return $text;
    }

}

if (!function_exists('usershortname')) {

    function usershortname($str, $len = 15, $dot = false) {
        if (strlen($str) > $len) {
            $returstr = substr($str, 0, $len);
            if ($dot) {
                $returstr .= " ...";
            }
            return $returstr;
        } else {
            return $str;
        }
    }

}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function randomString($length, $type = '') {
    // Select which type of characters you want in your random string
    switch ($type) {
        case 'num':
            // Use only numbers
            $salt = '1234567890';
            break;
        case 'lower':
            // Use only lowercase letters
            $salt = 'abcdefghijklmnopqrstuvwxyz';
            break;
        case 'upper':
            // Use only uppercase letters
            $salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        default:
            // Use uppercase, lowercase, numbers, and symbols
            $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            break;
    }
    $rand = '';
    $i = 0;
    while ($i < $length) { // Loop until you have met the length
        $num = rand() % strlen($salt);
        $tmp = substr($salt, $num, 1);
        $rand = $rand . $tmp;
        $i++;
    }
    return $rand; // Return the random string
}

if (!function_exists('getOrderDetails')) {

    function getOrderDetails($orderid = 0) {

        $CI = & get_instance();
        $userid = $CI->session->user_id;
        $CI->db->select('order_detail.*,plan.name,plan.price,plan.id as planid,plan.plan_image');
        $CI->db->from('order_detail');
        $CI->db->join('plan', 'order_detail.plan_id = plan.id');
        $CI->db->where('order_detail.order_id', $orderid);
        $orderDetails = $CI->db->get()->result();
        if (!empty($orderDetails)) {
            return $orderDetails;
        } else {
            return false;
        }
    }

}



if (!function_exists('created_name_by_id')) {

    function created_name_by_id($created_id) {
        $CI = & get_instance();
        $CI->db->select('user.firstname,user.lastname');
        $CI->db->from('user');
        $CI->db->where('user.id', $created_id);
        $query = $CI->db->get();
        $cnt = $query->row_array();
        $name = $cnt['firstname'] . ' ' . $cnt['lastname'];
        return $name;
    }

}

if (!function_exists('uploadKitAndGroup')) {

    function uploadKitAndGroup($barcode) {
        $CI = & get_instance();
        if (!empty($barcode)):
            $dataKitGroup = array('quantity' => count($barcode),
                'created_by' => $CI->session->user_id
            );
            $CI->load->model('Inventory_model');
            $group_id = $CI->Inventory_model->kitUploadGroup($dataKitGroup);
            $values = '';
            $issuesQrcode = array();
            $kitGroupData = array();
            $totalQuantity = '';
            for ($i = 0; $i < count($barcode); $i++) {
                $sql = "insert into kit_upload(group_id,barcode,status,created_at)values";
                if ($CI->Inventory_model->checkQrCodeExistsInKitUpload($barcode[$i]) <= 0):
                    $values .= '(';
                    $values .= $group_id . ',';
                    $values .= '"' . $barcode[$i] . '",';
                    $values .= '0,';
                    $values .= '"' . date('Y-m-d H:i:s') . '"';
                    $values .= '),';
                else:
                    $issuesQrcode[] = $barcode[$i];
                endif;
            }

            $data['issuesQrcode'] = $issuesQrcode;
            if (!empty($values)):
                $values = substr($values, 0, -1);
                $sql .= $values;
                $CI->Inventory_model->kitUploadInGroup($sql);
            endif;
            $totalQuantity = count($barcode) - count($issuesQrcode);
            $kitGroupData = array('quantity' => $totalQuantity);

            if (!empty($issuesQrcode)):
                $kitGroupData['issues'] = json_encode($issuesQrcode);
                $CI->session->set_userdata('issues', $kitGroupData['issues']);
            endif;
            $CI->Inventory_model->update_kitGroup($group_id, $kitGroupData);
        endif;
    }

}

function send_android_notification($registrationIds, $msg) {

    // API access key from Google API's Console
    define('API_ACCESS_KEY', 'AIzaSyDnpcniAfth_5rNUT8rSEEQNJ4fOpLZjsE');
    $fields = array('registration_ids' => $registrationIds, 'data' => $msg);

    $headers = array('Authorization: key=' . API_ACCESS_KEY, 'Content-Type: application/json');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function getTimeAgo($ts) {

    if (!ctype_digit($ts)) {
        $ts = strtotime($ts);
    }
    $diff = time() - $ts;
    if ($diff == 0) {
        return 'now';
    } elseif ($diff > 0) {
        $day_diff = floor($diff / 86400);
        if ($day_diff == 0) {
            if ($diff < 60)
                return 'just now';
            if ($diff < 120)
                return '1 minute ago';
            if ($diff < 3600)
                return floor($diff / 60) . ' minutes ago';
            if ($diff < 7200)
                return '1 hour ago';
            if ($diff < 86400)
                return floor($diff / 3600) . ' hours ago';
        }
        if ($day_diff == 1) {
            return 'Yesterday';
        }
        if ($day_diff < 7) {
            return $day_diff . ' days ago';
        }
        if ($day_diff < 31) {
            return ceil($day_diff / 7) . ' weeks ago';
        }
        if ($day_diff < 60) {
            return 'last month';
        }
        return date('F Y', $ts);
    } else {
        $diff = abs($diff);
        $day_diff = floor($diff / 86400);
        if ($day_diff == 0) {
            if ($diff < 120) {
                return 'in a minute';
            }
            if ($diff < 3600) {
                return 'in ' . floor($diff / 60) . ' minutes';
            }
            if ($diff < 7200) {
                return 'in an hour';
            }
            if ($diff < 86400) {
                return 'in ' . floor($diff / 3600) . ' hours';
            }
        }
        if ($day_diff == 1) {
            return 'Tomorrow';
        }
        if ($day_diff < 4) {
            return date('l', $ts);
        }
        if ($day_diff < 7 + (7 - date('w'))) {
            return 'next week';
        }
        if (ceil($day_diff / 7) < 4) {
            return 'in ' . ceil($day_diff / 7) . ' weeks';
        }
        if (date('n', $ts) == date('n') + 1) {
            return 'next month';
        }
        return date('F Y', $ts);
    }
}

function getGstTax($package_id) {
    $CI = & get_instance();
    
    $CI->db->select('*');
    $CI->db->from('tax');
    $CI->db->where(array('package_id'=>$package_id));
    $gsttax = $CI->db->get()->row_array();
    if (!empty($gsttax)) {
        return $gsttax;
    } else {
        return false;
    }
}

if (!function_exists('getjccare_by_id')) {
        
        function getjccare_by_id($userid){
            
            $CI = & get_instance();
            $DB2 = $CI->load->database('db2', TRUE);
            
            $CI->db->select('tblclients.userid,tblclients.ref_code,CONCAT(tblcontacts.firstname," ",tblcontacts.lastname) as name,tbllead_services.package_id,tbllead_services.package_company_id,tblclients.is_franchise,tblclients.is_retailer,tblclients.is_franchise_retailer,tblclients.fan_parent_id,tblclients.company,tblclients.address,tblclients.district,tbllead_services.status,tbllead_services.invoice_id,tblclients.userid,tblclients.addedfrom ,tblclients.ref_code,com.slug')
            ->from('jccare-crm.tblclients')
            ->join('jccare-crm.tbllead_services', 'tblclients.userid = tbllead_services.customer_id')
            ->join('jcventurescmd.companies AS com', 'com.company_id = tbllead_services.package_company_id')
            ->join('jccare-crm.tblcontacts', 'tblclients.userid = tblcontacts.userid')
            ->where('tblclients.userid',$userid)
            ->group_start()
            ->where('tbllead_services.status', 1)
            ->or_where_not_in('tbllead_services.invoice_id', 0)
            ->group_end()
            ->group_start()
            ->where('com.slug','jcgenetics')
            ->or_where('com.slug','jc-venture')
            ->group_end();
          //  ->group_by('tblclients.userid'); 
            $result = $CI->db->get()->result();
            //echo $CI->db->last_query();
          //  die;
                
            $checkStatus = false;
            $arr = '';
            if (!empty($result)) {

                foreach ($result as $key => $value) {

                    if (!empty($value->is_franchise)) {

                        $arr = array('status' => true, "type" => 'F', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code,);
                    } else if (!empty($value->is_retailer) && $value->slug == "jcgenetics") {

                        $arr = array('status' => true, "type" => 'R', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code,'fan_parent_id' => $value->fan_parent_id,);
                    } else if (!empty($value->is_franchise_retailer) && !empty($value->fan_parent_id) && $value->slug == "jcgenetics") {

                        $arr = array('status' => true, "type" => 'FR', 'user_id' => $value->userid, 'company_id' => $value->package_company_id, 'slug' => $value->slug, 'ref_code' => $value->ref_code, 'fan_parent_id' => $value->fan_parent_id,);
                    }
                }
            }
            
            return $arr;
        }
}
if (!function_exists('getjccare_by_refcode')) {

    function getjccare_by_refcode($ref_code) {

        $CI = & get_instance();

        $DB2 = $CI->load->database('db2', TRUE);
        //   $where = "ref_code='$id' AND tblclients.isUserActive='1'";
        $result = $DB2->select('*')->from('tblclients')->where(array('ref_code' => $ref_code, 'tblclients.isUserActive' => 1))->get()->row();

        if (!empty($result->is_franchise)) {

            return array('status' => true, 'company_id' => $result->userid, 'company' => $result->company, 'ref_code' => $result->ref_code);
        } else if (!empty($result->is_retailer)) {

            $checkRetailer = $CI->Comman_model->checkRetailerServices($result->userid);
            if (!empty($checkRetailer)) {

                return array('status' => true, 'company_id' => $result->userid, 'company' => $result->company, 'ref_code' => $result->ref_code);
            }
        } else if (!empty($result->is_franchise_retailer) && !empty($result->fan_parent_id)) {

            $checkRetailer = $CI->Comman_model->checkRetailerServices($result->userid);
            if (!empty($checkRetailer)) {

                return array('status' => true, 'company_id' => $result->userid, 'company' => $result->company, 'ref_code' => $result->ref_code);
            }
        }

        return FALSE;
    }

}

function checkPlanPackage($package_id) {
    $CI = & get_instance();
    $planpackage = $CI->db->select('id,package_id')->from('plan')->where(array('package_id' => $package_id))->get()->row();
    if (!empty($planpackage)) {
        return $planpackage;
    } else {
        return false;
    }
}

function getCommission($package_id) {
    $CI = & get_instance();
    $packagecommission = $CI->db->select('*')->from('package_commission')->where(array('package_id' => $package_id))->get()->row();
    if (!empty($packagecommission)) {
        return $packagecommission;
    } else {
        return false;
    }
}

function getIncentive($package_id) {
    $CI = & get_instance();
    $packageincentive = $CI->db->select('*')->from('package_incentive')->where(array('package_id' => $package_id))->get()->row();
    if (!empty($packageincentive)) {
        return $packageincentive;
    } else {
        return false;
    }
}

// check x-code scanned and uploaded kits against PO request or not

function countScannedKit($reqid) {

    $CI = & get_instance();
    $count = $CI->db->where(array('request_id' => $reqid))->get('xcode_inventory')->num_rows();
    return ($count > 0) ? $count : 0;
}

// check x-code dispatched against PO request or not

function checkDispatchPO($reqid) {

    $CI = & get_instance();
    $count = $CI->db->where(array('request_id' => $reqid))->get('xcode_dispatch')->num_rows();
    return ($count > 0) ? false : true;
}

// check requested kit quantity in PO request

function countPORequestedKit($reqid) {

    $CI = & get_instance();
    $count = $CI->db->where(array('request_id' => $reqid))->get('purchase_request_detail')->row();
    return ($count) ? $count->quantity : 0;
}
if (!function_exists('getCountBlogComment')) {

    function getCountBlogComment($blog_id) {
        $CI = & get_instance();
        $CI->db->select('id');
        $CI->db->from('blog_comments');
        $CI->db->where('blog_comments.id',$blog_id);
        $CI->db->where('blog_comments.status',1);
        return $totalcount = $CI->db->get()->num_rows();
    }

}
function GetIP() {
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                    return $ip;
                }
            }
        }
    }
}


