<section class="content-header">
    <h1>Email Template </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url('board/dashboard'); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Add Email Template</li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-5">
            <div class="box box-primary">
                <form action="<?php echo base_url('board/settings/saveemailtemplate');?>" data-parsley-validate="" class="form-label-left"  method="post" id="form_validation" name="form_validation">
                   <?php  $csrf = array('name' => $this->security->get_csrf_token_name(),'hash' => $this->security->get_csrf_hash()); ?>
		    <input type="hidden" id="token" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />			
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Email Template</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="package_id">* Template Name<span class="required">*</span></label>
                            <input type="text" id="template_name" name="template_name" class="form-control" placeholder="Template Name" value="<?php echo !empty($email->email_temp_name)? $email->email_temp_name : '' ; ?>" >
                            <input type="hidden" id="edit_id" name="edit_id" class="form-control" value="<?php echo !empty($email->emailtemplateid)? $email->emailtemplateid : '' ; ?>">
                        </div>
                        <div class="form-group">
                            <label for="state_id">Subject <span class="required">*</span></label>
                           <input type="text" id="subject" name="subject" class="form-control" placeholder="Enter Subject" value="<?php echo !empty($email->email_temp_name)? $email->email_temp_name : '' ; ?>">
                        </div>
                        <div class="form-group">
                            <label for="sgst_tax">Email message <span class="required">*</span></label>
                            <textarea id="editContent" name="description" class="form-control ckeditor "><?php !empty($email->email_temp_name)? $email->email_temp_name : '' ; ?></textarea>
                        </div>
                          <div class="form-group">
                            <label for="sgst_tax">Status <span class="required">*</span></label>
                            <select name="status" id="status" class="form-control">
                                <option  value="1" <?php if(!empty($email->status) && $email->status == 1) {?> selected <?php }?> >Active</option>
                                <option  value="0" <?php if(empty($email->status) && $email->status == 0) {?> selected <?php }?>>Disabled</option>
                            </select>
                           
                        </div>
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="<?php echo base_url('admindashboard/department_list'); ?>" class="btn btn-danger" type="button">Cancel</a>
                            </div>
                        </div>	

                    </div>
                   </form>
            </div>	
        </div>

        <div class="col-md-7">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Email Template List</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Template Name</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($emailtemplate)) {
                                    $i = 1;
                                    foreach ($emailtemplate as $row) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?php echo!empty($row->email_temp_name) ? $row->email_temp_name : '' ?><?php echo ' - ' . $packageInfo['service']; ?></td>
                                            <td><?php echo!empty($row->subject) ? $row->subject : '' ?></td>
                                            <td><?php echo!empty($row->message) ? $row->message : '' ?></td>
                                           <td>
                                          <?php if ($row->active == 1) {
                                                echo "Active";
                                            } elseif ($row->active == 0) {
                                                echo "Disabled";
                                            } else {
                                                echo "Pending";
                                            } ?>
                                           </td>
                                           <td>
                                                <a href="<?php echo base_url('board/settings/email_setting/'.$row->emailtemplateid); ?>" class="btn btn-info btn-flat"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a> 
                                           </td>
                                            </tr>
                                     <?php }
                                 } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>	
</section>
<script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
<script>
    $(document).ready(function ($) {

        $(function () {
            $('#form_validation').validate({
                submitHandler: function (form) {
                    form.submit();
                },
                ignore: [],
                rules: {
                    state_id: "required",
                },
                messages: {
                    state_id: "The state field is required",
                },
                highlight: function (input) {
                    $(input).parents('.form-group').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-group').removeClass('error');
                },

            });

        });
    });
</script>
<script>  
    CKEDITOR.replace('editContent'); 
</script>