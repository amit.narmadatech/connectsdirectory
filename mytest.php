
<p id = "leftalign">Do you feel like scratching yourself raw every time your neighbour's pet rubs past you? Have you ever wondered why your friend gets the sniffles every year during summer? Do you know allergies are a major cause of illnesses? Nearly 50 million Americans suffer from some form of allergy. An allergy is a hypersensitive immune reaction to an allergen that enters the body or comes in contact with it.</p>

<p>Everyone with an allergy has an interesting story about how they discovered that their body reacted violently to seemingly harmless allergens. Once an allergy is triggered, the symptoms can range from being annoying to deadly. From an evolutionary angle, allergies provided an alarm that kept humans away from toxic chemicals, either from venomous animals or plants. The reaction from the body’s immune system acted as a warning system that prompted our ancestors to move to other parts of the forest.</p>

<p>The balance has shifted with modernisation where there is exposure to a lot of synthetic chemicals, leading to a rise in allergy incidences. Our ancestors moved to other parts of the forest to avoid an allergic reaction, however, we continue to be exposed due to the presence of allergens indoors.</p>

<p>Some Interesting facts about genes and allergy.</p>

<ul><li>Family and twin studies have shown that allergies are partly genetic</li>
<li>A child with a parent with allergy is 3 to 6 times more likely to have asthma</li>
 <li>Food allergies lead to 150 to 200 fatalities every year in the U.S</li>
<li>Allergies are the sixth leading cause of chronic illness in the U.S, with an annual cost of $18 million</li>
<li>Allergic rhinitis affects 30% of the Indian population</li>
<li>Nearly 80% of histamine intolerant patients are middle-aged</li>
<li>Heritability for allergic diseases is estimated to be 95% for asthma, 84% for atopic dermatitis and 91% for allergic rhinitis</li>
<li>Upto 70% of the variation in the risk for motion sickness is genetic</li></ul>

<p>Gene variations are partly responsible for the risk of allergies. Other factors include environmental triggers, stress and the presence of gut microbes.</p>

<p>In this report we profile genes that are associated with the risk for various indoor, outdoor and food allergies.</p>

<p>We hope this report helps you understand your body better and you safeguard yourself from exposure to allergens based on your genes, to lead a healthier life.</p>

<hr>
<hr>
<hr>
<hr>
<hr>

<h4 class="text-center" id = "slabo"><b>Human health is a complex interplay between genetics and the environment (lifestyle, diet, activity, stress, etc.). Your genes and the environment that you are exposed to, both play a vital role in your well-being.</b></h4>

<p>This report is presented in a user friendly language and format. The following tips will help you get the best information value out of the report.</p>

<p><b>1. The word “likely” is used often in the report. What does it mean?</b></p>
<p>People generally know that high cholesterol can lead to heart conditions. However, there are individuals with high cholesterol who do not develop heart disease. Similarly, smoking can lead to lung disease, but not always. Hence, certain genetic parameters can lead to certain outcomes but other factors may modify the outcome. “Likely” means, it is more likely that one will see the outcome, but other factors may modify it.</p>
<p><b>2. What does the term “moderate” mean in the report?</b></p>
<p>Moderate implies neither high nor low, rather an intermediate or an average outcome. For example, moderate likelihood of pets allergy is an intermediate level between high and low likelihood.</p>
<p><b>3. How do I know which result is applicable to me?</b></p>
<p>Only results with a check mark ("\u2713") are applicable to you, the others are not applicable. All possible outcomes are provided in the table to provide a context to your outcome.</p>
<p><b>4. Where did the information contained in the report come from?</b></p>
<p>The genetic markers that are used in this report are based on scientific studies published in international journals. A list of references is available for you to read on our web blog.</p>
<p><b>5. Some sentences are colored in green and others in red, why?</b></p>
<p>Low risk for a specific allergy is indicated in green while high risk is in red. Moderate or Neutral outcomes are indicated in black.<br><br>
<b>6. What is the difference between lactose intolerance and milk allergy?</b></p>
<p>Lactose intolerance occurs due to insufficient amount of lactase enzyme, resulting in the inability to digest lactose, a sugar present in milk. Milk allergy, on the other hand, is an allergic reaction to the proteins present in milk.</p>

<hr>
<hr>
<hr>
<hr>
<hr>
<hr>
<hr>

<p>Xcode provides genetic assessment services for research or investigational use and Xcode’s reports should be interpreted or used exclusively by professional practitioners including but not limited to certified physicians, dieti tians, nutritionists, sports therapists and others in similar profession (“Professional Practitioners”). Xcode does not provide any direct medical advice to individual patients and this report is to be strictly interpreted by a qualified medical or health services professionals in order to provide relevant medical or healthcare advice, diagnosis or treatment. Genetic information must always be considered in conjunction with other information about your health such as lifestyle, family history, risk factors, biomedical data, diet, nutrition and physical activity among other factors. A single gene mutation is not the only factor that influences the health conditions or outcomes and there are several factors other than your genes such as the environment and lifestyle that may influence the health outcome. You are responsible to ascertain that your Professional Practitioner is qualified to consider the genetic information indicated in this report in conjunction with all other information made available to him/her about you including your family health history, lifestyle, bio-medical data and any other information that you may provide to the Professional Practitioner. Xcode shall not be held responsible for any misinterpretation by your Professional Practitioner of this Report or for any matter arising out of this report.</p> 
      <p>Only full genome sequences are exhaustive. All other forms of genetic tests only provide a limited subset of genetic information that has been found to be relevant to specific conditions. Since this report is not generated by conducting a whole genome sequence test, the results reported are limited to a specific set of mutations known to be associated with specific conditions. Genetic information is also subject to revision based on the latest advances in scientific research. Therefore it is possible that the interpretation of results reported herein may vary or be altered subject to ongoing research. Sometimes, the interpretations may vary from company to company based on which studies are being given a higher preference compared to others.</p>
      <p>Xcode’s role is limited to providing results of genetic test and providing a broad set of general recommendations. More detailed recommendations that may be specific to you are to be made by qualified Professional Practitioners only. General guidelines provided in our report are for information purpose only and are meant to aid your Professional Practitioner to render the relevant professional or medical advice and treatment. While assessing your genetic parameters and providing the report and recommendations, we do not consider your past or existing health conditions and or any medication taken by you (either in the past or currently), even if you may have provided us with such information. Our report and the recommendations therein are to be acted upon in consultation with a medical or other health and wellness professional practitioner.</p>
      <p>Your reliance upon the report is solely at your own discretion. As with all health and medical related matters, you should exercise adequate care in using the information provided in this report or on our website. Xcode disclaims any responsibility for any errors and/or omissions by you or other persons either during collection of DNA samples or delivery of the DNA sample to Xcode. We make no warranties of any kind, either express or implied, including, without limitation, the implied warranties of merchantability, fitness for a particular purpose, accuracy and non- infringement. The information in this report is for Research Use Only (RUO) or Investigational Use Only (IUO), meant to assist in further clinical diagnosis or treatment by Professional Practitioners.</p>

      <hr>
      <hr>
      <hr>
      <hr>
      <hr>
      <hr>
      <hr>
      <p><strong>About Your Genetic Test</strong>:</p> <p>Your genetic test was performed by Xcode Life Sciences, which is a global pioneer in the field of genetic services. To generate this report by analyzing your saliva sample, Xcode employs some of the best genetic testing platforms in the world such as the Illumina, Affymetrix, Pacific Biosciences platforms. </p> <p>In this test we profiled more than <strong>600,000 genetic markers</strong>. A variety of reports such as Nutrition, Fitness, Allergy, Skin Genetics, Health predisposition, Precision Medicine etc, can be generated using this one-time test. You can avail these reports anytime without having to redo the genetic test, upon request.</p> <p><strong>About the Genetic Testing Laboratory:</strong>:</p> <p>Your DNA analysis is performed in The National Accreditation Board for Testing and Calibration Laboratories (NABL) certified Laboratories. A lab certified by NABL must meet specific standards in quality, which includes rules on qualifications of individuals who perform the test, along with measures to maintain quality standards. </p> <p>Our panel of scientists follow a rigorous approach in curating the reports by analyzing thousands of published scientific studies. Our reports are updated on a regular basis to keep current with the latest in genetic research.</p> <p><strong>About Xcode Life Sciences:</strong></p> <p>Xcode was founded by Dr. Saleem Mohammed and Dr. Abdur Rub in 2011.</p> <p>Dr. Saleem Mohammed holds a PhD in Bioinformatics from University of Nebraska, USA. Previously, he worked as a Scientist in Gene Expression Technology at Monsanto. He received the MIT TR35 award in 2013</a>.</p> <p>Dr. Abdur Rub holds a PhD in Biomedical Sciences from the University of South Florida (USF), USA. Before Xcode, he has held positions as CEO of Cell Diagnostics, Singapore, Vice President of Research, Credo Biomedical, Singapore and Head of In Vitro Diagnostics, A*STAR, Singapore. </p> <p><strong>Accessing your genetic raw data</strong>: </p> <p>Your genetic test produces a raw data file which is approximately 16 MB in size. This file is available to you upon request. </p> <p><br /><br /></p>